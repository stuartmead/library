/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#define USE_CATCH

#ifdef USE_CATCH
#define CATCH_CONFIG_MAIN
#endif
#define STRINGIFY(s) (#s)

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#ifdef USE_CATCH
#include "catch.hpp"
#endif
#include "json11.hpp"

#include "gs_solver.h"
#include "gs_raster.h"
#include "gs_vector.h"
#include "gs_geojson.h"
#include "gs_geowkt.h"
#include "gs_projection.h"
#include "gs_series.h"
#include "gs_string.h"
#include "gs_ascii.h"
#include "gs_flt.h"
#include "gs_gsr.h"

#include "gs_network_flow.h"
#include "gs_hydro_network.h"
#include "gs_level_set.h"
#include "gs_particle.h"
#include "gs_multigrid.h"
#include "gs_shallow_water.h"

using namespace Geostack;

#ifndef USE_CATCH

// Test application
int main() {

    try {

        Solver &solver = Solver::getSolver();
        solver.setVerbose(true);
        solver.getContext();
        solver.getQueue();        

    } catch (std::runtime_error &e) {

        std::cout << "ERROR: " << e.what() << std::endl;
        return 1;
    }
}

#else


TEST_CASE( "Hydro Network initialisation", "[Hydro solver]" ) {
	// Create GeoJSON network string
	std::string json = R"({"features":[
		{"geometry":{"coordinates":[0,0],"type":"Point"},"properties":{"flow":[-0.025], "head":[10.0], "type":0},"type":"Feature"},
		{"geometry":{"coordinates":[100,100],"type":"Point"},"properties":{"flow":[-0.025], "head":[10.0], "type":0},"type":"Feature"},
		{"geometry":{"coordinates":[200,0],"type":"Point"},"properties":{"flow":[-0.015], "head":[35.0], "type":0},"type":"Feature"},
		{"geometry":{"coordinates":[200,200],"type":"Point"},"properties":{"flow":[-0.025], "head":[25.0], "type":0},"type":"Feature"},
		{"geometry":{"coordinates":[[0,0],[100,100]],"type":"LineString"},"type":"Feature"},
		{"geometry":{"coordinates":[[100,100],[200,0]],"type":"LineString"},"type":"Feature"},
		{"geometry":{"coordinates":[[200,0],[300,100]],"type":"LineString"},"type":"Feature"},
		{"geometry":{"coordinates":[[100,100],[200,200]],"type":"LineString"},"type":"Feature"},
		{"geometry":{"coordinates":[[200,200],[300,100]],"type":"LineString"},"type":"Feature"}
	],"type":"FeatureCollection"})";
	
		/* {"geometry":{"coordinates":[[200,200],[300,100]],"type":"LineString"},"type":"Feature"} */
	// Parse GeoJSON string
	auto v = GeoJson<REAL>::geoJsonToVector(json, false);

	//Set distance and elevation vectors
	std::vector<REAL> distances = { 0.0, 0.0, 300.0, 300.0 };
	std::vector<REAL> elevations = { 50.0, 0.0, 0.0, 50.0 };

	v.setProperty(0, "dists", distances);
	v.setProperty(0, "elevs", elevations);
	v.setProperty(1, "dists", distances);
	v.setProperty(1, "elevs", elevations);
	v.setProperty(2, "dists", distances);
	v.setProperty(2, "elevs", elevations);
	v.setProperty(3, "dists", distances);
	v.setProperty(3, "elevs", elevations);

	// Create flow solver instance
	HydroNetworkFlowSolver<REAL> hydroNetworkFlowSolver;
	bool initSuccess = hydroNetworkFlowSolver.init(v);
	REQUIRE(initSuccess == true);
    // Run flow solver
    /* bool runSuccess = hydroNetworkFlowSolver.run(100); */
    /* REQUIRE( runSuccess == true ); */

}


TEST_CASE("Hydro Network property calculations with rectangular channel", "Hydro solver") {
	// Create GeoJSON network string
	std::string json = R"({"features":[
		{"geometry":{"coordinates":[-120,0],"type":"Point"},"properties":{"flow":[-0.025], "head":[25.0], "type":0},"type":"Feature"},
		{"geometry":{"coordinates":[[-120,0], [0,0]],"type":"LineString"},"type":"Feature"}
	],"type":"FeatureCollection"})";
	
	//{"geometry":{"coordinates":[[100,140],[100,0]],"type":"LineString"},"properties":{"constant":100,"dists" : [0.0, 0.0, 300.0, 300.0],"diameter":0.3,"type":1},"type":"Feature"},

	// Parse GeoJSON string
	auto v = GeoJson<REAL>::geoJsonToVector(json, false);

	//Set distance and elevation vectors
	std::vector<REAL> distances = { 0.0, 0.0, 300.0, 300.0 };
	std::vector<REAL> elevations = { 50.0, 0.0, 0.0, 50.0 };

	v.setProperty(0, "dists", distances);
	v.setProperty(0, "elevs", elevations);

	// Create flow solver instance
	HydroNetworkFlowSolver<REAL> hydroNetworkFlowSolver;
	bool initSuccess = hydroNetworkFlowSolver.init(v);
	REQUIRE(initSuccess == true);
	
	//Check area
	double area = hydroNetworkFlowSolver.getArea(0);
	REQUIRE(area == 7500.0);

	//Check wet perimeter
	double perimeter = hydroNetworkFlowSolver.getPerimeter(0);
	REQUIRE(perimeter == 350.0);

	//Check flow width
	double flowWidth = hydroNetworkFlowSolver.getChannelWidth(0);
	REQUIRE(flowWidth == 300.0);

    // Check conveyance (using mannings = 0.023)
    double K = hydroNetworkFlowSolver.getConveyance(0);
	//REAL K_err = fabs(K - 2515719.75);
    //REQUIRE(K_err < 1.0);
	REQUIRE(K == 2515719.75);

    double partialKh = hydroNetworkFlowSolver.getPartialKh(0);
    REQUIRE(partialKh == 158130.0);
    //std::cout << "partialKh: " << partialKh << std::endl;
}

TEST_CASE("Hydro Network property calculations with rectangular channel (JSON)", "Hydro solver") {
	// Create GeoJSON network string
	std::string json = R"({"features":[
		{"geometry":{"coordinates":[-120,0],"type":"Point"},"properties":{"flow":[-0.025],"dists" : [0.0, 0.0, 300.0, 300.0],"elevs": [50.0, 0.0, 0.0, 50.0], "head":[25.0], "type":0},"type":"Feature"},
		{"geometry":{"coordinates":[[-120,0], [0,0]],"type":"LineString"},"type":"Feature"}
	],"type":"FeatureCollection"})";

	//{"geometry":{"coordinates":[[100,140],[100,0]],"type":"LineString"},"properties":{"constant":100,"dists" : [0.0, 0.0, 300.0, 300.0],"diameter":0.3,"type":1},"type":"Feature"},

	// Parse GeoJSON string
	auto v = GeoJson<REAL>::geoJsonToVector(json, false);

	
	// Create flow solver instance
	HydroNetworkFlowSolver<REAL> hydroNetworkFlowSolver;
	bool initSuccess = hydroNetworkFlowSolver.init(v);
	REQUIRE(initSuccess == true);

	//Check area
	double area = hydroNetworkFlowSolver.getArea(0);
	REQUIRE(area == 7500.0);

	//Check wet perimeter
	double perimeter = hydroNetworkFlowSolver.getPerimeter(0);
	REQUIRE(perimeter == 350.0);

	//Check flow width
	double flowWidth = hydroNetworkFlowSolver.getChannelWidth(0);
	REQUIRE(flowWidth == 300.0);
    
    // Check conveyance (using mannings = 0.023)
    double K = hydroNetworkFlowSolver.getConveyance(0);
	//REAL K_err = fabs(K - 2515719.75);
    //REQUIRE(K_err < 1.0);
	REQUIRE(K == 2515719.75);

    double partialKh = hydroNetworkFlowSolver.getPartialKh(0);
    REQUIRE(partialKh == 158130.0);
    //std::cout << "partialKh: " << partialKh << std::endl;
}

TEST_CASE("Hydro Network property calculations with trapezoidal channel", "Hydro solver") {
	// Create GeoJSON network string
	std::string json = R"({"features":[
		{"geometry":{"coordinates":[-120,0],"type":"Point"},"properties":{"flow":[-0.025], "head":[25.0], "type":0},"type":"Feature"},
		{"geometry":{"coordinates":[[-120,0], [0,0]],"type":"LineString"},"type":"Feature"}
	],"type":"FeatureCollection"})";
	
	//{"geometry":{"coordinates":[[100,140],[100,0]],"type":"LineString"},"properties":{"constant":100,"dists" : [0.0, 0.0, 300.0, 300.0],"diameter":0.3,"type":1},"type":"Feature"},

	// Parse GeoJSON string
	auto v = GeoJson<REAL>::geoJsonToVector(json, false);

	//Set distance and elevation vectors for trapezoidal channel
	std::vector<REAL> distances = { 0.0, 100.0, 200.0, 300.0 };
	std::vector<REAL> elevations = { 50.0, 0.0, 0.0, 50.0 };

	v.setProperty(0, "dists", distances);
	v.setProperty(0, "elevs", elevations);

	// Create flow solver instance
	HydroNetworkFlowSolver<REAL> hydroNetworkFlowSolver;
	bool initSuccess = hydroNetworkFlowSolver.init(v);
	REQUIRE(initSuccess == true);
	
	//Check area
	double area = hydroNetworkFlowSolver.getArea(0);
	REQUIRE(area == 3750.0);

	//Check wet perimeter
	double perimeter = hydroNetworkFlowSolver.getPerimeter(0);
    double real_perimeter = 100.0 + 2.0 * sqrt(pow(50.0, 2.0) + pow(25.0, 2.0));
    double perimeter_error = abs(perimeter - real_perimeter);
	REQUIRE(perimeter_error < 0.1);

	//Check flow width
	double flowWidth = hydroNetworkFlowSolver.getChannelWidth(0);
	REQUIRE(flowWidth == 200.0);

    // Check conveyance (using mannings = 0.023)
    double K = hydroNetworkFlowSolver.getConveyance(0);
	REAL K_err = fabs(K - 1.10756e+06);
    REQUIRE(K_err < 10.0);

    double partialKh = hydroNetworkFlowSolver.getPartialKh(0);
    REQUIRE(partialKh == 82860.0);
}

TEST_CASE("Hydro Network property calculations with complex channel", "Hydro solver") {
	// Create GeoJSON network string
	std::string json = R"({"features":[
		{"geometry":{"coordinates":[-120,0],"type":"Point"},"properties":{"flow":[-0.025], "head":[25.0], "type":0},"type":"Feature"},
		{"geometry":{"coordinates":[[-120,0], [0,0]],"type":"LineString"},"type":"Feature"}
	],"type":"FeatureCollection"})";
	
	//{"geometry":{"coordinates":[[100,140],[100,0]],"type":"LineString"},"properties":{"constant":100,"dists" : [0.0, 0.0, 300.0, 300.0],"diameter":0.3,"type":1},"type":"Feature"},

	// Parse GeoJSON string
	auto v = GeoJson<REAL>::geoJsonToVector(json, false);

	//Set distance and elevation vectors for trapezoidal channel
	std::vector<REAL> distances = { 0.0, 100.0, 150.0, 200.0, 300.0 };
	std::vector<REAL> elevations = { 50.0, 0.0, 10.0, 0.0, 50.0 };

	v.setProperty(0, "dists", distances);
	v.setProperty(0, "elevs", elevations);

	// Create flow solver instance
	HydroNetworkFlowSolver<REAL> hydroNetworkFlowSolver;
	bool initSuccess = hydroNetworkFlowSolver.init(v);
	REQUIRE(initSuccess == true);
	
	//Check area
	double area = hydroNetworkFlowSolver.getArea(0);
	REQUIRE(area == 3250.0);

	//Check wet perimeter
	double perimeter = hydroNetworkFlowSolver.getPerimeter(0);
    double real_perimeter = 2.0 * sqrt(pow(50.0, 2.0) + pow(10.0, 2.0)) + 2.0 * sqrt(pow(50.0, 2.0) + pow(25.0, 2.0));
    double perimeter_error = abs(perimeter - real_perimeter);
	REQUIRE(perimeter_error < 0.1);

	//Check flow width
	double flowWidth = hydroNetworkFlowSolver.getChannelWidth(0);
	REQUIRE(flowWidth == 200.0);

    // Check conveyance (using mannings = 0.023)
    double K = hydroNetworkFlowSolver.getConveyance(0);
    /* std::cout << "K: " << K << std::endl; */
	REAL K_err = fabs(K - 867151);
    REQUIRE(K_err < 10.0);

    double partialKh = hydroNetworkFlowSolver.getPartialKh(0);
    /* std::cout << "partialKh: " << partialKh << std::endl; */
	REAL partialKh_err = fabs(partialKh - 76845.3);
    REQUIRE(partialKh_err < 0.1);
}

TEST_CASE("Constant test", "Hydro solver") {

	std::cout << "\t \t *******CONSTANT TEST********* \t \t" << std::endl;

	//Constants for creating profile */
	REAL slope = 0.01; //Bed slope 
	REAL dx = 50.0; //Cell width 
	REAL length = 200.0; //Channel length 
	REAL q = 3000.0; //Flow rate 
	REAL mannings = 0.04;
	//Set distance vectors */
	std::vector<REAL> distances = { 0.0, 0.0, 300.0, 300.0 };
	//Init vector
	Vector<REAL> network;
	CoordinateList<REAL> cs;

	network.addProperty("mannings");
	network.addProperty("dists");
	network.addProperty("elevs");
	network.addProperty("head");
	network.addProperty("flow");
	network.addProperty("type");

	REAL l = length;
	while (l >= 0.0) {
		//Add point
		auto id = network.addPoint(Coordinate<REAL>(l, 0.0));
		cs.push_back(Coordinate<REAL>(l, 0.0));

		std::vector<REAL> elevs = { (REAL)50.0 + (l*slope), (REAL)l*slope, (REAL)l*slope, (REAL)50.0 + (l*slope) };
        std::vector<REAL> head = {(REAL)10.0 + (l*slope)};
        std::vector<REAL> flow = {q};
		network.setProperty(id, "dists", distances);
		network.setProperty(id, "elevs", elevs);
		network.setProperty(id, "mannings", mannings);
		network.setProperty(id, "head", head);//Init height as 5 m?
		network.setProperty(id, "flow", flow); //Maybe only for the first one

		network.setProperty(id, "type", (int)HydroNetworkNodeType::Junction);		
		
		//network.setProperty(id, "length", dx);//Maybe
		l -= dx;
	}
	std::cout << std::endl;

	//Create links - loop from begining to 1 from end
	for (int it = 0; it < cs.size() - 1; ++it) {
		network.addLineString({ cs[it], cs[it+1]});
	}

	//Set last point to terminator
	auto last = network.getPointIndexes().back();
	network.setProperty(last, "type", (int)HydroNetworkNodeType::Terminator);
    
	//Set first point to inflow
	auto first = network.getPointIndexes().front();
	network.setProperty(first, "type", (int)HydroNetworkNodeType::Inflow);
    std::vector<REAL> new_head = { (REAL)10.0 };
	std::vector<cl_uint> ids = network.getPointIndexes();
	network.setProperty(ids.back(), "head", new_head);
	// Create flow solver instance */
	HydroNetworkFlowSolver<REAL> hydroNetworkFlowSolver;
	bool initSuccess = hydroNetworkFlowSolver.init(network);
	REQUIRE(initSuccess == true);
	bool runSuccess = hydroNetworkFlowSolver.run(600.0, 60.0);
	REQUIRE(runSuccess == true);
}


TEST_CASE("Steady backwater impoundment", "Hydro solver") { 

	std::cout << "\t \t *******STEADY BACKWATER IMPOUNDMENT TEST********* \t \t" << std::endl;

	//Constants for creating profile */
	REAL slope = 0.002; // 0.01; //Bed slope 
	REAL dx = 100.0; //Cell length 
	REAL length = 4000.0; //Channel length 
	REAL q = 4000.0; //Flow rate 
	REAL mannings = 0.04;
    //Set distance vectors */
	std::vector<REAL> distances = { 0.0, 0.0, (REAL)300.0, (REAL)300.0};
	//Init vector
	Vector<REAL> network;
	CoordinateList<REAL> cs;
	
	network.addProperty("mannings");
	network.addProperty("dists");
	network.addProperty("elevs");
	network.addProperty("head");
	network.addProperty("flow");
	network.addProperty("type");

	REAL l = length;
	while (l >= 0.0) {
		//Add point
		auto id = network.addPoint(Coordinate<REAL>(l, 0.0));
		cs.push_back(network.getPointCoordinate(id));

		std::vector<REAL> elevs = { (REAL)50.0 + (l*slope), (REAL)l*slope, (REAL)l*slope, (REAL)50.0 + (l*slope) };
        std::vector<REAL> head = { std::max((REAL)1.0 + (l*slope), (REAL)25.0) };
        std::vector<REAL> flow = { q };
		network.setProperty(id, "dists", distances);
		network.setProperty(id, "elevs", elevs);
		network.setProperty(id, "mannings", mannings);
		network.setProperty(id, "head", head);//Init height as 5 m?
        network.setProperty(id, "flow", flow); //Maybe only for the first one

		network.setProperty(id, "type", (int)HydroNetworkNodeType::Junction);

		l -= dx;		
	}
	std::cout << std::endl;

	//Create links - loop from begining to 1 from end
	for (int it = 0; it < cs.size() - 1; ++it) {
		network.addLineString({ cs[it], cs[it + 1] });
	}

	//Set last point to terminator
	auto last = network.getPointIndexes().back();
	network.setProperty(last, "type", (int)HydroNetworkNodeType::Terminator);

	//Set first point to inflow
    std::vector<REAL> inflow = { q };
	auto first = network.getPointIndexes().front();
	network.setProperty(first, "type", (int)HydroNetworkNodeType::Inflow);
	network.setProperty(first, "flow", inflow);

	//std::vector<cl_uint> ids = network.getPointIndexes();
	
	//Set the last value (outlet) height to 25 */
	//network.setProperty(last, "head", (REAL)25.0);// +(length*slope));

	// Create flow solver instance */
	HydroNetworkFlowSolver<REAL> hydroNetworkFlowSolver;
	bool initSuccess = hydroNetworkFlowSolver.init(network);
	REQUIRE(initSuccess == true);

	bool runSuccess = hydroNetworkFlowSolver.run(6000.0, 60.0, 1);
	REQUIRE(runSuccess == true);
	
    std::vector<REAL> times = hydroNetworkFlowSolver.getTimeArray();

	Vector<REAL> networkOut = hydroNetworkFlowSolver.getNetwork();
		
    auto &properties = networkOut.getProperties();
    auto ids = networkOut.getPointIndexes();

	
    auto &headVector = properties.template getPropertyRef<std::vector<std::vector<REAL>>>("head");
    auto &flowVector = properties.template getPropertyRef<std::vector<std::vector<REAL>>>("flow");
	/*
    std::cout << "Time:\tHead:\tFlow:" << std::endl;
    std::cout << "=====================" << std::endl;
    
	for(int i = 0; i < ids.size(); ++i) {
        std::cout << "Point: " << ids[i] << std::endl;
        std::cout << "---------------------" << std::endl;
        auto flowValues = flowVector[i];
        auto headValues = headVector[i];
        for(int j = 0; j < times.size(); ++j) {
			std::cout << times[j] << "\t" << headValues[j] << "\t" << flowValues[j] << std::endl;
        }
    }
	*/


}


#endif
