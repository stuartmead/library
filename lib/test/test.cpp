/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#define USE_CATCH

#ifdef USE_CATCH
#define CATCH_CONFIG_MAIN
#endif
#define STRINGIFY(s) (#s)

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#ifdef USE_CATCH
#include "catch.hpp"
#endif
#include "json11.hpp"

#include "gs_solver.h"
#include "gs_raster.h"
#include "gs_vector.h"
#include "gs_geojson.h"
#include "gs_geowkt.h"
#include "gs_projection.h"
#include "gs_series.h"
#include "gs_string.h"
#include "gs_ascii.h"
#include "gs_flt.h"
#include "gs_gsr.h"

#include "gs_network_flow.h"
#include "gs_level_set.h"
#include "gs_particle.h"
#include "gs_multigrid.h"
#include "gs_shallow_water.h"

using namespace Geostack;

#ifndef USE_CATCH

// Test application
int main() {

    try {

        Solver &solver = Solver::getSolver();
        solver.setVerbose(true);
        solver.getContext();
        solver.getQueue();        

    } catch (std::runtime_error &e) {

        std::cout << "ERROR: " << e.what() << std::endl;
        return 1;
    }
}

#else

TEST_CASE( "OpenCL initialisation", "[core]" ) {

    // Get solver singleton
    Solver &solver = Solver::getSolver();
    solver.setVerbose(true);
    solver.getContext();

    // Test initialisation
    REQUIRE( solver.openCLInitialised() == true );
}

TEST_CASE( "Property map", "[core]" ) {

        auto p = PropertyMap();
        p.setProperty("A", "10");
        p.setProperty("B", 20);
        p.setProperty("C", 30.0F);
        p.setProperty("D", 40.0);
        p.setProperty("E", (cl_uint)50);

        p.setProperty("Av", std::vector<std::string>( { "11", "12", "13", "14" } ));
        p.setProperty("Bv", std::vector<int>( { 21, 22, 23, 24 } ));
        p.setProperty("Cv", std::vector<float>( { 31.0F, 32.0F, 33.0F, 34.0F } ));
        p.setProperty("Dv", std::vector<double>( { 41.0, 42.0, 43.0, 44.0 } ));
        p.setProperty("Ev", std::vector<cl_uint>( { 51, 52, 53, 54 } ));
        p.setProperty("Fv", std::vector<std::vector<float> >( {
            { 101.0F, 102.0F, 103.0F, 104.0F },
            { 111.0F, 112.0F, 113.0F, 114.0F },
            { 121.0F, 122.0F, 123.0F, 124.0F },
            { 131.0F, 132.0F, 133.0F, 134.0F } } ));
        p.setProperty("Gv", std::vector<std::vector<double> >( {
            { 101.0, 102.0, 103.0, 104.0 },
            { 111.0, 112.0, 113.0, 114.0 },
            { 121.0, 122.0, 123.0, 124.0 },
            { 131.0, 132.0, 133.0, 134.0 } } ));

        std::string expected = 
            R"({"A": "10", "Av": ["11", "12", "13", "14"], "B": 20, "Bv": [21, 22, 23, 24], "C": 30, "Cv": [31, 32, 33, 34],)"
            R"( "D": 40, "Dv": [41, 42, 43, 44], "E": 50, "Ev": [51, 52, 53, 54],)"
            R"( "Fv": [[101, 102, 103, 104], [111, 112, 113, 114], [121, 122, 123, 124], [131, 132, 133, 134]],)"
            R"( "Gv": [[101, 102, 103, 104], [111, 112, 113, 114], [121, 122, 123, 124], [131, 132, 133, 134]]})";
        REQUIRE( p.toJsonString() == expected );

        auto A_type = p.getPropertyType("A");
        auto B_type = p.getPropertyType("B");
        auto C_type = p.getPropertyType("C");
        auto D_type = p.getPropertyType("D");
        auto E_type = p.getPropertyType("E");

        auto Av_type = p.getPropertyType("Av");
        auto Bv_type = p.getPropertyType("Bv");
        auto Cv_type = p.getPropertyType("Cv");
        auto Dv_type = p.getPropertyType("Dv");
        auto Ev_type = p.getPropertyType("Ev");
        auto Fv_type = p.getPropertyType("Fv");
        auto Gv_type = p.getPropertyType("Gv");

        auto A_structure = p.getPropertyStructure("A");
        auto B_structure = p.getPropertyStructure("B");
        auto C_structure = p.getPropertyStructure("C");
        auto D_structure = p.getPropertyStructure("D");
        auto E_structure = p.getPropertyStructure("E");

        auto Av_structure = p.getPropertyStructure("Av");
        auto Bv_structure = p.getPropertyStructure("Bv");
        auto Cv_structure = p.getPropertyStructure("Cv");
        auto Dv_structure = p.getPropertyStructure("Dv");
        auto Ev_structure = p.getPropertyStructure("Ev");
        auto Fv_structure = p.getPropertyStructure("Fv");
        auto Gv_structure = p.getPropertyStructure("Gv");

        REQUIRE( A_type == PropertyType::String );
        REQUIRE( B_type == PropertyType::Integer );
        REQUIRE( C_type == PropertyType::Float );
        REQUIRE( D_type == PropertyType::Double );
        REQUIRE( E_type == PropertyType::Index );

        REQUIRE( Av_type == PropertyType::String );
        REQUIRE( Bv_type == PropertyType::Integer );
        REQUIRE( Cv_type == PropertyType::Float );
        REQUIRE( Dv_type == PropertyType::Double );
        REQUIRE( Ev_type == PropertyType::Index );
        REQUIRE( Fv_type == PropertyType::FloatVector );
        REQUIRE( Gv_type == PropertyType::DoubleVector );

        REQUIRE( A_structure == PropertyStructure::Scalar );
        REQUIRE( B_structure == PropertyStructure::Scalar );
        REQUIRE( C_structure == PropertyStructure::Scalar );
        REQUIRE( D_structure == PropertyStructure::Scalar );
        REQUIRE( E_structure == PropertyStructure::Scalar );

        REQUIRE( Av_structure == PropertyStructure::Vector );
        REQUIRE( Bv_structure == PropertyStructure::Vector );
        REQUIRE( Cv_structure == PropertyStructure::Vector );
        REQUIRE( Dv_structure == PropertyStructure::Vector );
        REQUIRE( Ev_structure == PropertyStructure::Vector );
        REQUIRE( Fv_structure == PropertyStructure::Vector );
        REQUIRE( Gv_structure == PropertyStructure::Vector );

        REQUIRE( p.template getProperty<std::string>("A") == "10" );
        REQUIRE( p.template getProperty<std::string>("B") == std::to_string(20) );
        REQUIRE( p.template getProperty<std::string>("C") == std::to_string(30.0F) );
        REQUIRE( p.template getProperty<std::string>("D") == std::to_string(40.0) );
        REQUIRE( p.template getProperty<std::string>("E") == std::to_string((cl_uint)50) );

        REQUIRE( p.template getProperty<int>("A") == 10 );
        REQUIRE( p.template getProperty<int>("B") == 20 );
        REQUIRE( p.template getProperty<int>("C") == 30 );
        REQUIRE( p.template getProperty<int>("D") == 40 );
        REQUIRE( p.template getProperty<int>("E") == 50 );

        REQUIRE( p.template getProperty<float>("A") == 10.0F );
        REQUIRE( p.template getProperty<float>("B") == 20.0F );
        REQUIRE( p.template getProperty<float>("C") == 30.0F );
        REQUIRE( p.template getProperty<float>("D") == 40.0F );
        REQUIRE( p.template getProperty<float>("E") == 50.0F );

        REQUIRE( p.template getProperty<double>("A") == 10.0 );
        REQUIRE( p.template getProperty<double>("B") == 20.0 );
        REQUIRE( p.template getProperty<double>("C") == 30.0 );
        REQUIRE( p.template getProperty<double>("D") == 40.0 );
        REQUIRE( p.template getProperty<double>("E") == 50.0 );

        REQUIRE( p.template getPropertyFromVector<cl_uint>("Av", 1) == (cl_uint)12 );
        REQUIRE( p.template getPropertyFromVector<cl_uint>("Bv", 1) == (cl_uint)22 );
        REQUIRE( p.template getPropertyFromVector<cl_uint>("Cv", 1) == (cl_uint)32 );
        REQUIRE( p.template getPropertyFromVector<cl_uint>("Dv", 1) == (cl_uint)42 );
        REQUIRE( p.template getPropertyFromVector<cl_uint>("Ev", 1) == (cl_uint)52 );

        REQUIRE( p.template getPropertyFromVector<int>("Av", 1) == 12 );
        REQUIRE( p.template getPropertyFromVector<int>("Bv", 1) == 22 );
        REQUIRE( p.template getPropertyFromVector<int>("Cv", 1) == 32 );
        REQUIRE( p.template getPropertyFromVector<int>("Dv", 1) == 42 );
        REQUIRE( p.template getPropertyFromVector<int>("Ev", 1) == 52 );

        REQUIRE( p.template getPropertyFromVector<float>("Av", 1) == 12.0F );
        REQUIRE( p.template getPropertyFromVector<float>("Bv", 1) == 22.0F );
        REQUIRE( p.template getPropertyFromVector<float>("Cv", 1) == 32.0F );
        REQUIRE( p.template getPropertyFromVector<float>("Dv", 1) == 42.0F );
        REQUIRE( p.template getPropertyFromVector<float>("Ev", 1) == 52.0F );

        REQUIRE( p.template getPropertyFromVector<double>("Av", 1) == 12.0 );
        REQUIRE( p.template getPropertyFromVector<double>("Bv", 1) == 22.0 );
        REQUIRE( p.template getPropertyFromVector<double>("Cv", 1) == 32.0 );
        REQUIRE( p.template getPropertyFromVector<double>("Dv", 1) == 42.0 );
        REQUIRE( p.template getPropertyFromVector<double>("Ev", 1) == 52.0 );

        REQUIRE( p.template getPropertyFromVector<cl_uint>("Av", 1) == (cl_uint)12 );
        REQUIRE( p.template getPropertyFromVector<cl_uint>("Bv", 1) == (cl_uint)22 );
        REQUIRE( p.template getPropertyFromVector<cl_uint>("Cv", 1) == (cl_uint)32 );
        REQUIRE( p.template getPropertyFromVector<cl_uint>("Dv", 1) == (cl_uint)42 );
        REQUIRE( p.template getPropertyFromVector<cl_uint>("Ev", 1) == (cl_uint)52 );

        REQUIRE( p.template getPropertyFromVector<std::vector<float> >("Fv", 1)[2] == 113.0F );
        REQUIRE( p.template getPropertyFromVector<std::vector<double> >("Gv", 1)[2] == 113.0 );

        std::string &Ar = p.template getPropertyRef<std::string>("A");
        REQUIRE( Ar == "10" );

        int &Br = p.template getPropertyRef<int>("B");
        REQUIRE( Br == 20 );

        float &Cr = p.template getPropertyRef<float>("C");
        REQUIRE( Cr == 30.0F );

        double &Dr = p.template getPropertyRef<double>("D");
        REQUIRE( Dr == 40.0 );

        cl_uint &Er = p.template getPropertyRef<cl_uint>("E");
        REQUIRE( Er == (cl_uint)50 );

        {
        std::vector<std::string> &Avr = p.template getPropertyRef<std::vector<std::string> >("Av");
        REQUIRE( Avr[1] == "12" );

        std::vector<int> &Bvr = p.template getPropertyRef<std::vector<int> >("Bv");
        REQUIRE( Bvr[1] == 22 );

        std::vector<float> &Cvr = p.template getPropertyRef<std::vector<float> >("Cv");
        REQUIRE( Cvr[1] == 32.0F );

        std::vector<double> &Dvr = p.template getPropertyRef<std::vector<double> >("Dv");
        REQUIRE( Dvr[1] == 42.0 );

        std::vector<cl_uint> &Evr = p.template getPropertyRef<std::vector<cl_uint> >("Ev");
        REQUIRE( Evr[1] == (cl_uint)52 );

        std::vector<std::vector<float> > &Fvr = p.template getPropertyRef<std::vector<std::vector<float> > >("Fv");
        REQUIRE( Fvr[1][2] == 113.0F );

        std::vector<std::vector<double> > &Gvr = p.template getPropertyRef<std::vector<std::vector<double> > >("Gv");
        REQUIRE( Gvr[1][2] == 113.0 );

        Avr[1] = "120";
        std::vector<std::string> &Avr2 = p.template getPropertyRef<std::vector<std::string> >("Av");
        REQUIRE( Avr2[1] == "120" );

        Bvr[1] = 220;
        std::vector<int> &Bvr2 = p.template getPropertyRef<std::vector<int> >("Bv");
        REQUIRE( Bvr2[1] == 220 );

        Cvr[1] = 320.0F;
        std::vector<float> &Cvr2 = p.template getPropertyRef<std::vector<float> >("Cv");
        REQUIRE( Cvr2[1] == 320.0F );

        Dvr[1] = 420.0;
        std::vector<double> &Dvr2 = p.template getPropertyRef<std::vector<double> >("Dv");
        REQUIRE( Dvr2[1] == 420.0 );

        Evr[1] = (cl_uint)520;
        std::vector<cl_uint> &Evr2 = p.template getPropertyRef<std::vector<cl_uint> >("Ev");
        REQUIRE( Evr2[1] == (cl_uint)520 );

        Fvr[1][2] = 620.0F;
        std::vector<std::vector<float> > &Fvr2 = p.template getPropertyRef<std::vector<std::vector<float> > >("Fv");
        REQUIRE( Fvr2[1][2] == 620.0F );

        Gvr[1][2] = (cl_uint)720.0;
        std::vector<std::vector<double> > &Gvr2 = p.template getPropertyRef<std::vector<std::vector<double> > >("Gv");
        REQUIRE( Gvr2[1][2] == 720.0 );
        }

        auto q = PropertyMap();
        q.setProperty("Av", std::vector<std::string>( { "21", "22", "23", "24" } ));
        q.setProperty("Bv", std::vector<int>( { 31, 32, 33, 34 } ));
        q.setProperty("Cv", std::vector<float>( { 41.0F, 42.0F, 43.0F, 44.0F } ));
        q.setProperty("Dv", std::vector<double>( { 51.0, 52.0, 53.0, 54.0 } ));
        q.setProperty("Ev", std::vector<cl_uint>( { 61, 62, 63, 64 } ));
        q.setProperty("Fv", std::vector<std::vector<float> >( {
            { 201.0F, 202.0F, 203.0F, 204.0F },
            { 211.0F, 212.0F, 213.0F, 214.0F },
            { 221.0F, 222.0F, 223.0F, 224.0F },
            { 231.0F, 232.0F, 233.0F, 234.0F } } ));
        q.setProperty("Gv", std::vector<std::vector<double> >( {
            { 201.0, 202.0, 203.0, 204.0 },
            { 211.0, 212.0, 213.0, 214.0 },
            { 221.0, 222.0, 223.0, 224.0 },
            { 231.0, 232.0, 233.0, 234.0 } } ));

        p+=q;

        {
        std::vector<std::string> &Avr = p.template getPropertyRef<std::vector<std::string> >("Av");
        REQUIRE( Avr.size() == 8 );
        REQUIRE( Avr[7] == "24" );

        std::vector<int> &Bvr = p.template getPropertyRef<std::vector<int> >("Bv");
        REQUIRE( Bvr.size() == 8 );
        REQUIRE( Bvr[7] == 34 );

        std::vector<float> &Cvr = p.template getPropertyRef<std::vector<float> >("Cv");
        REQUIRE( Cvr.size() == 8 );
        REQUIRE( Cvr[7] == 44.0F );

        std::vector<double> &Dvr = p.template getPropertyRef<std::vector<double> >("Dv");
        REQUIRE( Dvr.size() == 8 );
        REQUIRE( Dvr[7] == 54.0 );

        std::vector<cl_uint> &Evr = p.template getPropertyRef<std::vector<cl_uint> >("Ev");
        REQUIRE( Evr.size() == 8 );
        REQUIRE( Evr[7] == (cl_uint)64 );

        std::vector<std::vector<float> > &Fvr = p.template getPropertyRef<std::vector<std::vector<float> > >("Fv");
        REQUIRE( Fvr.size() == 8 );
        REQUIRE( Fvr[7][2] == 233.0F );

        std::vector<std::vector<double> > &Gvr = p.template getPropertyRef<std::vector<std::vector<double> > >("Gv");
        REQUIRE( Gvr.size() == 8 );
        REQUIRE( Gvr[7][2] == 233.0 );
        }
}

TEST_CASE( "Raster initialisation 2D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.max() == (REAL)99.9 );
    REQUIRE( testRasterA.min() == (REAL)1.0 );
}

TEST_CASE( "Raster resize 2D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    // Resize
    testRasterA.resize2D(3, 3, 1, 1);

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(2, 2) == (REAL)99.9 );
    REQUIRE( testRasterA.max() == (REAL)99.9 );
    REQUIRE( testRasterA.min() == (REAL)1.0 );
}

TEST_CASE( "Raster variable script 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setVariableData<REAL>("varA", (REAL)99.9);
    testRasterA.setVariableData<REAL>("varB", (REAL)88.8);
    testRasterA.setVariableData<REAL>("varC", (REAL)77.7);

    // Run script
    runScript<REAL>("testRasterA = testRasterA::varA;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)99.9 );

    // Run script
    runScript<REAL>("testRasterA = testRasterA::varB;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)88.8 );

    // Run script
    runScript<REAL>("testRasterA = testRasterA::varC;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)77.7 );

    // Run script
    runScript<REAL>(
        "testRasterA = testRasterA::varA+testRasterA::varB+testRasterA::varC;",
        {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)99.9+(REAL)88.8+(REAL)77.7 );
}

TEST_CASE( "Raster float script 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(15, 15, 1.0, 1.0, -5, -5);
    testRasterB.setAllCellValues((REAL)2.0);

    makeRaster(testRasterC, REAL, REAL)
    testRasterC.init2D(15, 15, 1.0, 1.0, -5, -5);

    // Run script
    std::string script = "testRasterC = 100.0 + testRasterA * testRasterB;";
    runScript<REAL>(script, {testRasterC, testRasterA, testRasterB} );

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.max() == (REAL)299.8 );
    REQUIRE( testRasterC.min() == (REAL)102.0 );
    REQUIRE( testRasterC.getCellValue(7, 7) == (REAL)299.8 );
    REQUIRE( testRasterC.getNearestValue(2.5, 2.5) == (REAL)299.8 );
    REQUIRE( testRasterC.getBilinearValue(2.5, 2.5) == (REAL)299.8 );
}

TEST_CASE( "Raster float output script 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(15, 15, 1.0, 1.0, -5, -5);
    testRasterB.setAllCellValues((REAL)2.0);

    // Run script
    std::string script = "output = 100.0 + testRasterA * testRasterB;";
    auto testRasterC = runScript<REAL, REAL>(script, {testRasterA, testRasterB} );

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.max() == (REAL)299.8 );
    REQUIRE( testRasterC.min() == (REAL)102.0 );
    REQUIRE( testRasterC.getCellValue(7, 7) == (REAL)299.8 );
    REQUIRE( testRasterC.getNearestValue(2.5, 2.5) == (REAL)299.8 );
    REQUIRE( testRasterC.getBilinearValue(2.5, 2.5) == (REAL)299.8 );
}

TEST_CASE( "Raster integer output script 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, uint32_t, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues(1);
    testRasterA.setCellValue(99, 2, 2);

    makeRaster(testRasterB, uint32_t, REAL)
    testRasterB.init2D(15, 15, 1.0, 1.0, -5, -5);
    testRasterB.setAllCellValues(2);

    // Run script
    std::string script = "output = 100.0 + testRasterA * testRasterB;";
    auto testRasterC = runScript<uint32_t, REAL>(script, {testRasterA, testRasterB}, RasterNullValue::Zero);

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.max() == (uint32_t)298 );
    REQUIRE( testRasterC.min() == (uint32_t)100 );
    REQUIRE( testRasterC.getCellValue(7, 7) == (uint32_t)298 );
    REQUIRE( testRasterC.getNearestValue(2.5, 2.5) == (uint32_t)298 );
}

TEST_CASE( "Raster integer float output script 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(50, 50, (REAL)0.1, (REAL)0.1);
    testRasterA.setAllCellValues(0.0);

    makeRaster(testRasterB, uint32_t, REAL)
    testRasterB.init2D(50, 50, (REAL)0.1, (REAL)0.1);
    testRasterB.setAllCellValues(0);

    // Create gradient
    testRasterA = runScript<REAL, REAL>("output = x*y;", {testRasterA} );

    // Run script
    std::string script = "output = testRasterA;";
    auto testRasterC = runScript<uint32_t, REAL>(script, {testRasterB, testRasterA});

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getCellValue(26, 14) == (REAL)2.65*(REAL)1.45 );
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.getCellValue(26, 14) == (uint32_t)3 );
}

TEST_CASE( "Raster float area script 2D", "[raster]" ) {


    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(21, 21, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)0.0);
    testRasterA.setCellValue((REAL)99.9, 10, 10);

    // Run script
    std::string script = R"(

    // Average
    if (isValid_REAL(testRasterA)) {
        output += testRasterA;
        sum+=1.0;
    }

    )";
    auto output = runAreaScript<REAL, REAL>(script, testRasterA, 3);

    // Test values
    REQUIRE( output.hasData() == true );
    REQUIRE( output.getCellValue(10, 10) == ((REAL)99.9/(REAL)49.0) );
}

TEST_CASE( "Raster reduction script 2D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(500, 500, 1.0, 1.0);

    // Set values
    testRasterA.setAllCellValues((REAL)-99.9);
    testRasterA.setCellValue((REAL)99.9, 250, 250);

    // Test maximum reduction
    testRasterA.setReductionType(Reduction::Maximum);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)99.9);

    // Test minimum reduction
    testRasterA.setReductionType(Reduction::Minimum);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)-99.9);

    // Test maximum reduction
    testRasterA.setReductionType(Reduction::Maximum);
    runScript<REAL>("testRasterA = x*y;", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)249500.25);

    // Test count reduction
    testRasterA.setReductionType(Reduction::Count);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)500*500);

    // Test sum reduction
    testRasterA.setReductionType(Reduction::Sum);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)15625000000);

    // Test mean reduction
    testRasterA.setReductionType(Reduction::Mean);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)62500);

    // Test copy and maximum reduction
    auto testRasterB = runScript<REAL, REAL>("output = testRasterA;", { testRasterA }, Reduction::Maximum );
    REQUIRE(testRasterB.reduce() == (REAL)249500.25);
}

TEST_CASE( "Raster random script 2D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(100, 100, 1.0, 1.0);

    testRasterA.setReductionType(Reduction::Mean);
    runScriptNoOut<REAL>("testRasterA = random;", { testRasterA });

    REQUIRE( fabs(testRasterA.reduce()-(REAL)0.5) < 0.1 );

    // Create raster
    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(100, 100, 1.0, 1.0);

    testRasterB.setReductionType(Reduction::Mean);
    runScriptNoOut<REAL>("testRasterB = randomNormal(5.0, 1.0);", { testRasterB });

    REQUIRE( fabs(testRasterB.reduce()-(REAL)5.0) < 0.1 );
}

TEST_CASE( "Raster boundary script 2D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(1000, 1000, 1.0, 1.0);

    runScriptNoOut<REAL>("testRasterA = isBoundary_N || isBoundary_S || isBoundary_W || isBoundary_E;", { testRasterA });

    REQUIRE( fabs(testRasterA.getCellValue(0, 0)) == 1 );
    REQUIRE( fabs(testRasterA.getCellValue(999, 999)) == 1 );
    REQUIRE( fabs(testRasterA.getCellValue(999, 0)) == 1 );
    REQUIRE( fabs(testRasterA.getCellValue(0, 999)) == 1 );
    REQUIRE( fabs(testRasterA.getCellValue(1, 1)) == 0 );
    REQUIRE( fabs(testRasterA.getCellValue(499, 499)) == 0 );
}


TEST_CASE( "Raster debug script", "[raster]" ) {

    makeRaster(rA, REAL, REAL)
    makeRaster(rB, REAL, REAL)

    rA.init2D(100, 100, 1, 1);
    rB.init2D(100, 100, 1, 1);
    rA.setVariableData("A", (REAL)11.1);
    rB.setVariableData("B", (REAL)22.2);

    runScriptNoOut<REAL>("rA = x+rB::B; rB = x+rA::A; rA::A = 44.4; rB::B = 55.5", {rA, rB}, RasterDebug::Enable);

    REQUIRE(rA.getCellValue(0, 0) == (REAL)22.7);
    REQUIRE(rB.getCellValue(0, 0) == (REAL)11.6);
    REQUIRE(rA.getCellValue(1, 1) != rA.getCellValue(1, 1));
    REQUIRE(rB.getCellValue(1, 1) != rA.getCellValue(1, 1));
    
    REQUIRE(rA.getVariableData<REAL>("A") == (REAL)44.4);
    REQUIRE(rB.getVariableData<REAL>("B") == (REAL)55.5);
}

TEST_CASE( "Raster neighbours 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(5, 5, 1.0, 1.0);
    runScript<REAL>("testRasterA = ipos+jpos*5;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getCellValue(3, 3) == (REAL)18 );

    // Map north neighbours
    runScript<REAL>("testRasterB = testRasterA_N;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)23 );
    REQUIRE( testRasterB.getCellValue(4, 4) != testRasterB.getCellValue(4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_NE;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)24 );
    REQUIRE( testRasterB.getCellValue(4, 4) != testRasterB.getCellValue(4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_E;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)19 );
    REQUIRE( testRasterB.getCellValue(4, 4) != testRasterB.getCellValue(4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_SE;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)14 );
    REQUIRE( testRasterB.getCellValue(4, 4) != testRasterB.getCellValue(4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_S;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)13 );
    REQUIRE( testRasterB.getCellValue(0, 0) != testRasterB.getCellValue(0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_SW;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)12 );
    REQUIRE( testRasterB.getCellValue(0, 0) != testRasterB.getCellValue(0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_W;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)17 );
    REQUIRE( testRasterB.getCellValue(0, 0) != testRasterB.getCellValue(0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_NW;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)22 );
    REQUIRE( testRasterB.getCellValue(0, 0) != testRasterB.getCellValue(0, 0) );
}

TEST_CASE( "Raster sampling 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(20, 20, 0.25, 0.25);

    // Run script
    //   This will default to use the minimum resolution of the two rasters
    std::string script = "output = testRasterA;";
    testRasterB = runScript<REAL, REAL>(script, {testRasterA, testRasterB} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(2.5, 2.5) == testRasterB.getNearestValue(2.5, 2.5) );
}

TEST_CASE( "Raster intersection 2D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(20, 20, 0.25, 0.25, -4, -4);
    testRasterB.setAllCellValues((REAL)-1.0);

    // Run script
    //   This will use zero as the null value and default
    //   to use the minimum resolution of the two rasters
    std::string script = "output = testRasterA + testRasterB;";
    auto testRasterC = runScript<REAL, REAL>(script, {testRasterA, testRasterB}, RasterNullValue::Zero);

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.getCellValue(17, 17) == (REAL)0.0 );
}

TEST_CASE( "Raster properties 2D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);

    // Set properties
    testRasterA.setProperty("property0", 99);
    testRasterA.setProperty("property1", "rstr");

    // Test values
    REQUIRE( testRasterA.template getProperty<int>("property0") == 99 );
    REQUIRE( testRasterA.template getProperty<std::string>("property1") == "rstr" );
}

TEST_CASE( "Raster float script 3D", "[raster]" ) {

    // Create rasters
    makeRaster(testRaster2D, REAL, REAL)
    testRaster2D.init2D(5, 5, 1.0, 1.0);
    makeRaster(testRaster3D_A, REAL, REAL)
    testRaster3D_A.init(5, 5, 5, 1.0, 1.0, 1.0);
    makeRaster(testRaster3D_B, REAL, REAL)
    testRaster3D_B.init(5, 5, 5, 1.0, 1.0, 1.0);

    // Create 3D raster
    runScript<REAL>("testRaster3D_A = x*y*z;", { testRaster3D_A } );
    REQUIRE( testRaster3D_A.hasData() == true );
    REQUIRE( testRaster3D_A.getCellValue(2, 2, 2) == (REAL)15.625 );

    // Sum 3D raster into 3D raster
    std::string scriptA = "output = 0; for (uint k = 0; k < testRaster3D_A_layers; k++) output += testRaster3D_A[k];";
    auto output = runScript<REAL, REAL>(scriptA, { testRaster3D_A } );
    REQUIRE( output.hasData() == true );
    REQUIRE( output.getCellValue(1, 1, 0) == (REAL)28.125 );
    REQUIRE( output.getCellValue(1, 1, 2) == (REAL)28.125 );
    REQUIRE( output.getCellValue(1, 1, 4) == (REAL)28.125 );

    // Sum 3D raster into 2D raster
    std::string scriptB = "testRaster2D = 0; for (uint k = 0; k < testRaster3D_A_layers; k++) testRaster2D += testRaster3D_A[k];";
    runScript<REAL>(scriptB, { testRaster2D, testRaster3D_A } );
    REQUIRE( testRaster2D.hasData() == true );
    REQUIRE( testRaster2D.getCellValue(1, 1) == (REAL)28.125 );

    // Map 2D raster into 3D raster
    std::string scriptC = "for (uint k = 0; k < testRaster3D_B_layers; k++) testRaster3D_B[k] = testRaster2D+k;";
    runScript<REAL>(scriptC, { testRaster2D, testRaster3D_B } );
    REQUIRE( testRaster3D_B.hasData() == true );
    REQUIRE( testRaster3D_B.getCellValue(1, 1, 1) == (REAL)29.125 );

    // Map 3D raster into 3D raster
    std::string scriptD = "testRaster3D_B = testRaster3D_A+1.0;";
    runScript<REAL>(scriptD, { testRaster3D_B, testRaster3D_A } );
    REQUIRE( testRaster3D_B.getCellValue(2, 2, 2) == (REAL)16.625 );

    // Run script
    std::string scriptE = "testRaster3D_B = testRaster2D+1.0;";
    runScript<REAL>(scriptE, { testRaster3D_B, testRaster2D } );
    REQUIRE( testRaster3D_B.getCellValue(1, 1, 0) == (REAL)29.125 );
    REQUIRE( testRaster3D_B.getCellValue(1, 1, 2) == (REAL)29.125 );
    REQUIRE( testRaster3D_B.getCellValue(1, 1, 4) == (REAL)29.125 );
}

TEST_CASE( "Raster float sort 3D", "[raster]" ) {

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init(5, 5, 5, 1.0, 1.0, 1.0);

    REAL kVals[] = { 9.0, 7.5, 22.2, getNullValue<REAL>(), 3.7 };
    for (int k = 0; k < 5; k++) {
        for (int j = 0; j < 5; j++) {
            for (int i = 0; i < 5; i++) {
                testRasterA.setCellValue(kVals[k], i, j, k);
            }
        }
    }

    // Run sort
    sortColumns<REAL>(testRasterA);

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getCellValue(2, 2, 0) == (REAL)3.7 );
    REQUIRE( testRasterA.getCellValue(2, 2, 3) == (REAL)22.2 );
    REQUIRE( testRasterA.getCellValue(2, 2, 4) != testRasterA.getCellValue(2, 2, 4) );
}

TEST_CASE( "Raster neighbours 3D", "[raster]" ) {

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init(5, 5, 5, 1.0, 1.0, 1.0);
    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init(5, 5, 5, 1.0, 1.0, 1.0);
    runScript<REAL>("testRasterA = ipos+(jpos+kpos*5)*5;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getCellValue(3, 3, 3) == (REAL)93 );

    // Map north neighbours
    runScript<REAL>("testRasterB = testRasterA_N;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)98 );
    REQUIRE( testRasterB.getCellValue(4, 4, 4) != testRasterB.getCellValue(4, 4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_NE;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)99 );
    REQUIRE( testRasterB.getCellValue(4, 4, 4) != testRasterB.getCellValue(4, 4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_E;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)94 );
    REQUIRE( testRasterB.getCellValue(4, 4, 4) != testRasterB.getCellValue(4, 4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_SE;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)89 );
    REQUIRE( testRasterB.getCellValue(4, 4, 4) != testRasterB.getCellValue(4, 4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_S;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)88 );
    REQUIRE( testRasterB.getCellValue(0, 0, 0) != testRasterB.getCellValue(0, 0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_SW;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)87 );
    REQUIRE( testRasterB.getCellValue(0, 0, 0) != testRasterB.getCellValue(0, 0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_W;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)92 );
    REQUIRE( testRasterB.getCellValue(0, 0, 0) != testRasterB.getCellValue(0, 0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_NW;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)97 );
    REQUIRE( testRasterB.getCellValue(0, 0, 0) != testRasterB.getCellValue(0, 0, 0) );
}

TEST_CASE( "Raster distance", "[raster]" ) {

    Vector<REAL> vector;
    vector.addProperty("level");
    vector.setProperty(vector.addPoint( {  1,  1} ), "level", (uint32_t)0);
    vector.setProperty(vector.addPoint( { -1,  1} ), "level", (uint32_t)0);
    vector.setProperty(vector.addPoint( { -1, -1} ), "level", (uint32_t)0);

    Vector<REAL> vector2;
    vector2.addProperty("level");
    vector2.setProperty(vector2.addPoint( {  1,  1} ), "level", (uint32_t)1);
    vector2.setProperty(vector2.addPoint( {  1, -1} ), "level", (uint32_t)1);
    vector2.setProperty(vector2.addPoint( { -1, -1} ), "level", (uint32_t)1);

    REAL delta = 0.01;
    auto bounds = BoundingBox<REAL>( { {-2, -2}, {2, 2} });

    // Test distance map
    auto distance = vector.mapDistance<REAL>(delta, GeometryType::Point, bounds);
    REQUIRE( fabs(distance.getNearestValue( 0,  0)-sqrt(2.0)) < delta );
    REQUIRE( fabs(distance.getNearestValue( 1,  0)-1.0) < delta );
    REQUIRE( fabs(distance.getNearestValue( 0, -1)-1.0) < delta );
    REQUIRE( fabs(distance.getNearestValue( 1,  1)) < delta );

    // Test distance map on second layer
    auto dim = distance.getRasterDimensions();
    makeRaster (distance2, REAL, REAL)
    distance2.init(dim.d.nx, dim.d.ny, 2, dim.d.hx, dim.d.hy, 1.0, dim.d.ox, dim.d.oy, 0.0);
    distance2.mapVector(vector, GeometryType::Point, "", "level");
    distance2.mapVector(vector2, GeometryType::Point, "", "level");
    REQUIRE( fabs(distance2.getNearestValue( 0,  0,  0)-sqrt(2.0)) < delta );
    REQUIRE( fabs(distance2.getNearestValue( 1,  0,  0)-1.0) < delta );
    REQUIRE( fabs(distance2.getNearestValue( 0, -1,  0)-1.0) < delta );
    REQUIRE( fabs(distance2.getNearestValue(-1,  1,  0)) < delta );
    REQUIRE( fabs(distance2.getNearestValue( 0,  0,  1)-sqrt(2.0)) < delta );
    REQUIRE( fabs(distance2.getNearestValue( 1,  0,  1)-1.0) < delta );
    REQUIRE( fabs(distance2.getNearestValue( 0, -1,  1)-1.0) < delta );
    REQUIRE( fabs(distance2.getNearestValue( 1, -1,  1)) < delta );

    // Test radial basis map
    auto rb = vector.mapDistance<REAL>(delta,
        "REAL rb = exp(-2.0*dot(d, d)); output = isValid_REAL(output) ? output+rb : rb;", GeometryType::Point, bounds);
    REQUIRE( fabs(rb.getNearestValue( 0,  0)-3.0*exp(-4.0)) < delta );
    REQUIRE( fabs(rb.getNearestValue( 1,  0)-exp(-2.0)) < delta );
    REQUIRE( fabs(rb.getNearestValue( 0, -1)-exp(-2.0)) < delta );
    REQUIRE( fabs(rb.getNearestValue( 1,  1)-1.0) < delta );

}

TEST_CASE( "Vector initialisation", "[vector]" ) {

    // Create vector data
    Vector<REAL> vector;
    Coordinate<REAL> c = { (REAL)144.9631, (REAL)-37.8136 };
    auto newPointIndex = vector.addPoint(c);
    vector.setProperty(newPointIndex, "newproperty", std::string("newstr"));

    // Test values
    REQUIRE( vector.getPointCoordinate(newPointIndex) == c );
    REQUIRE( vector.template getProperty<std::string>(newPointIndex, "newproperty") == "newstr" );
}

TEST_CASE( "Vector subregion", "[vector]" ) {

    // Create GeoJSON string
    std::string inputGeoJSON = R"({"features": [
        {"geometry": {"coordinates": [143, -36], "type": "Point"}, "properties": {}, "type": "Feature"},
        {"geometry": {"coordinates": [144, -37, 1], "type": "Point"}, "properties": {}, "type": "Feature"},
        {"geometry": {"coordinates": [145, -38, 1], "type": "Point"}, "properties": {"time": 10}, "type": "Feature"},
        {"geometry": {"coordinates": [146, -39, 2], "type": "Point"}, "properties": {"time": 10}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    // Parse GeoJSON string
    auto vector = GeoJson<REAL>::geoJsonToVector(inputGeoJSON);

    // Test p, q slice
    auto r0 = vector.region({ {(REAL)143, (REAL)-36, (REAL)0}, {(REAL)143, (REAL)-36, (REAL)0}});
    auto &p0 = r0.getPointIndexes();
    REQUIRE( p0.size() == 1 );
    REQUIRE( r0.getPointCoordinate(p0[0]) == Coordinate<REAL>((REAL)143, (REAL)-36, (REAL)0) );

    // Test r slice
    auto r1 = vector.region({ {(REAL)143, (REAL)-36, (REAL)0.5}, {(REAL)146, (REAL)-39, (REAL)1.5}});
    auto &p1 = r1.getPointIndexes();
    REQUIRE( p1.size() == 1 );
    REQUIRE( r1.getPointCoordinate(p1[0]) == Coordinate<REAL>((REAL)144, (REAL)-37, (REAL)1, (REAL)0) );

    // Test s slice
    auto r2 = vector.region({ {(REAL)143, (REAL)-36, (REAL)0, (REAL)10}, {(REAL)146, (REAL)-39, (REAL)2, (REAL)10}});
    auto &p2 = r2.getPointIndexes();
    REQUIRE( p2.size() == 2 );
    REQUIRE( r2.getPointCoordinate(p2[0]) == Coordinate<REAL>((REAL)145, (REAL)-38, (REAL)1, (REAL)10) );
    REQUIRE( r2.getPointCoordinate(p2[1]) == Coordinate<REAL>((REAL)146, (REAL)-39, (REAL)2, (REAL)10) );
}

TEST_CASE( "Vector rasterisation", "[vector]" ) {

    // Create GeoJSON string
    std::string json = R"({"features": [
        {"geometry": {"coordinates": [0, 1.5], "type": "Point"},
            "properties": {"A": 1}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"A": 2}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"A": 3}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 4}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 5}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    // Parse GeoJSON string
    auto v = GeoJson<REAL>::geoJsonToVector(json);

    // No script, default to mask of 1
    auto testRasterA = v.rasterise<REAL>(0.02);
    REQUIRE( testRasterA.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(25, 25) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(12, 12) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(45, 46) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(70, 51) == (REAL)1.0 );

    // No processing specified, A is order-specific
    auto testRasterB = v.rasterise<REAL>(0.02, "output = A;");
    REQUIRE( testRasterB.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterB.getCellValue(25, 25) == (REAL)2.0 );
    REQUIRE( testRasterB.getCellValue(12, 12) == (REAL)3.0 );
    REQUIRE( testRasterB.getCellValue(45, 46) == (REAL)5.0 );
    REQUIRE( testRasterB.getCellValue(70, 51) == (REAL)5.0 );

    // Minimum specified
    auto testRasterC = v.rasterise<REAL>(0.02, "output = min(A, output);");
    REQUIRE( testRasterC.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterC.getCellValue(25, 25) == (REAL)2.0 );
    REQUIRE( testRasterC.getCellValue(12, 12) == (REAL)3.0 );
    REQUIRE( testRasterC.getCellValue(45, 46) == (REAL)3.0 );
    REQUIRE( testRasterC.getCellValue(70, 51) == (REAL)4.0 );

    // Maximum specified
    auto testRasterD = v.rasterise<REAL>(0.02, "output = max(A, output);");
    REQUIRE( testRasterD.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterD.getCellValue(25, 25) == (REAL)2.0 );
    REQUIRE( testRasterD.getCellValue(12, 12) == (REAL)3.0 );
    REQUIRE( testRasterD.getCellValue(45, 46) == (REAL)5.0 );
    REQUIRE( testRasterD.getCellValue(70, 51) == (REAL)5.0 );
}

TEST_CASE( "Vector script", "[vector]") {

    // Create GeoJSON string
    std::string json = R"({"features": [
        {"geometry": {"coordinates": [0, 1.5], "type": "Point"},
            "properties": {"C": 10}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"C": 20}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"C": 30}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"C": 40}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"C": 50}, "type": "Feature"},
        {"geometry": {"coordinates": [2, 0.75], "type": "Point"},
            "properties": {"C": 60}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    // Parse GeoJSON string
    auto v = GeoJson<REAL>::geoJsonToVector(json);
    v.setProjectionParameters(ProjectionParameters<double>());
    v.template setProperty<REAL>(0, "A", (REAL)1.0);
    v.template setProperty<REAL>(1, "A", (REAL)1.0);
    v.template setProperty<std::vector<REAL> >("B", { 99.0, 99.0, 0.0, 0.0, 0.0, 0.0 } );

    runVectorScript<REAL, REAL>("A += B-C;", v);
    REQUIRE(v.template getProperty<REAL>(0, "A") == (REAL)90.0);
    REQUIRE(v.template getProperty<REAL>(1, "A") == (REAL)80.0);
    REQUIRE(isInvalid<REAL>(v.template getProperty<REAL>(2, "A")));
    REQUIRE(isInvalid<REAL>(v.template getProperty<REAL>(3, "A")));
    REQUIRE(isInvalid<REAL>(v.template getProperty<REAL>(4, "A")));
    REQUIRE(isInvalid<REAL>(v.template getProperty<REAL>(5, "A")));

    REAL delta = 0.02;
    REAL delta2 = delta/2.0;

    makeRaster(r, REAL, REAL)
    r.init2D(150, 150, delta, delta);
    runScriptNoOut<REAL>("r = x;", { r } );
    r.setVariableData<REAL>("var", 11);

    makeRaster(s, REAL, REAL)
    s.init2D(150, 150, delta, delta);
    runScriptNoOut<REAL>("s = 2;", { s } );
    s.setVariableData<REAL>("var", 22);

    runVectorScript<REAL, REAL>("A = r::var;", v, { r }, Reduction::Maximum);
    REQUIRE(v.template getProperty<REAL>(0, "A") == (11));
    runVectorScript<REAL, REAL>("A = r::var+s::var;", v, { r, s }, Reduction::Maximum);
    REQUIRE(v.template getProperty<REAL>(0, "A") == (11+22));

    runVectorScript<REAL, REAL>("A = r; B += 1.0;", v, { r }, Reduction::Maximum);
    REAL A2_max = v.template getProperty<REAL>(2, "A");
    REQUIRE(fabs(v.template getProperty<REAL>(0, "A") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(1, "A") - (3.0-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(2, "A") - (1.0-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(3, "A") - (1.5-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(4, "A") - (1.5-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(5, "A") - (2.0+delta2)) < 1.0E-3);
    REQUIRE(v.template getProperty<REAL>(0, "B") == 100.0);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 10.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 60.0);

    runVectorScript<REAL, REAL>("A = r; B += 1.0;", v, { r }, Reduction::Minimum);
    REAL A2_min = v.template getProperty<REAL>(2, "A");
    REQUIRE(fabs(v.template getProperty<REAL>(0, "A") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(1, "A") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(2, "A") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(3, "A") - (0.5+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(4, "A") - (0.5+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(5, "A") - (2.0+delta2)) < 1.0E-3);
    REQUIRE(v.template getProperty<REAL>(0, "B") == 101.0);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 10.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 60.0);

    runVectorScript<REAL, REAL>("A = r;", v, { r }, Reduction::Count);
    REQUIRE(v.template getProperty<int>(0, "A") == 1);
    REQUIRE(v.template getProperty<int>(1, "A") == 150);
    //REQUIRE(v.template getProperty<int>(2, "A") == 1875);
    REQUIRE(v.template getProperty<int>(3, "A") == 2500);
    REQUIRE(v.template getProperty<int>(4, "A") == 2500);
    REQUIRE(v.template getProperty<int>(5, "A") == 1);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 10.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 60.0);

    runVectorScript<REAL, REAL>("A = r; B += 1.0;", v, { r }, Reduction::Mean);
    REQUIRE(fabs(v.template getProperty<REAL>(0, "A") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(1, "A") - (1.5)) < 1.0E-3);
    //REQUIRE(fabs(v.template getProperty<REAL>(2, "A") - (0.5)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(3, "A") - (1.0)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(4, "A") - (1.0)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(5, "A") - (2.0+delta2)) < 1.0E-3);
    REQUIRE(v.template getProperty<REAL>(0, "B") == 102.0);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 10.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 60.0);

    runVectorScript<REAL, REAL>("A = s; B += 1.0;", v, { s }, Reduction::Sum);
    REQUIRE(v.template getProperty<int>(0, "A") == 2);
    REQUIRE(v.template getProperty<int>(1, "A") == 300);
    //REQUIRE(v.template getProperty<int>(2, "A") == 3750);
    REQUIRE(v.template getProperty<int>(3, "A") == 5000);
    REQUIRE(v.template getProperty<int>(4, "A") == 5000);
    REQUIRE(v.template getProperty<int>(5, "A") == 2);
    REQUIRE(v.template getProperty<REAL>(0, "B") == 103.0);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 10.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 60.0);

    runVectorScript<REAL, REAL>("A = r*s; B = r; C += 10.0;", v, { r, s }, Reduction::Maximum);
    REQUIRE(fabs(v.template getProperty<REAL>(0, "A") - 2.0*(0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(1, "A") - 2.0*(3.0-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(2, "A") - 2.0*(1.0-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(3, "A") - 2.0*(1.5-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(4, "A") - 2.0*(1.5-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(5, "A") - 2.0*(2.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(0, "B") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(1, "B") - (3.0-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(2, "B") - (1.0-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(3, "B") - (1.5-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(4, "B") - (1.5-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(5, "B") - (2.0+delta2)) < 1.0E-3);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 60.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 70.0);

    v.addProperty("D");
    v.addProperty("E");
    runVectorScript<REAL, REAL>("D = r; E = s;", v, { r, s }, Reduction::None);
    {
    auto D2_vec = v.template getProperty<std::vector<REAL> >(2, "D");
    REQUIRE(D2_vec[0] == A2_min);
    REQUIRE(D2_vec[D2_vec.size()-1] == A2_max);

    auto E2_vec = v.template getProperty<std::vector<REAL> >(2, "E");
    REQUIRE(E2_vec[0] == (REAL)2);
    }
    v.removeProperty("D");
    REQUIRE( v.hasProperty("D") == false );
    v.removeProperty("E");
    REQUIRE( v.hasProperty("E") == false );

    v.removeProperty("A");
    v.removeProperty("B");
    v.addProperty("A");
    v.addProperty("B");
    runVectorScript<REAL, REAL>("A = r; B = s;", v, { r, s }, Reduction::None);
    {
    auto A2_vec = v.template getProperty<std::vector<REAL> >(2, "A");
    REQUIRE(A2_vec[0] == A2_min);
    REQUIRE(A2_vec[A2_vec.size()-1] == A2_max);

    auto B2_vec = v.template getProperty<std::vector<REAL> >(2, "B");
    REQUIRE(B2_vec[0] == (REAL)2);
    }

    runVectorScript<REAL, REAL>("A = s; B = r;", v, { r, s }, Reduction::None);
    {
    auto B2_vec = v.template getProperty<std::vector<REAL> >(2, "B");
    REQUIRE(B2_vec[0] == A2_min);
    REQUIRE(B2_vec[B2_vec.size()-1] == A2_max);

    auto A2_vec = v.template getProperty<std::vector<REAL> >(2, "A");
    REQUIRE(A2_vec[0] == (REAL)2);
    }

}

TEST_CASE( "Vector addition", "[vector]") {

    // Create GeoJSON string
    std::string json1 = R"({"features": [
        {"geometry": {"coordinates": [0, 1.5], "type": "Point"},
            "properties": {"A": 10}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"A": 20}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"A": 30}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 40}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 50}, "type": "Feature"},
        {"geometry": {"coordinates": [2, 0.75], "type": "Point"},
            "properties": {"A": 60}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    std::string json2 = R"({"features": [
        {"geometry": {"coordinates": [5, 6.5], "type": "Point"},
            "properties": {"B": 15}, "type": "Feature"},
        {"geometry": {"coordinates": [[5, 5], [6, 6], [7, 5], [8, 6]], "type": "LineString"},
            "properties": {"B": 25}, "type": "Feature"},
        {"geometry": {"coordinates": [[[5, 5], [6, 5], [6, 6], [5, 6], [5, 5]], [[5.25, 5.25], [5.25, 5.75], [5.75, 5.75], [5.75, 5.25], [5.25, 5.25]]], "type": "Polygon"},
            "properties": {"B": 35}, "type": "Feature"},
        {"geometry": {"coordinates": [[[5.5, 5.5], [6.5, 5.5], [6.5, 6.5], [5.5, 6.5], [5.5, 5.5]]], "type": "Polygon"},
            "properties": {"B": 45}, "type": "Feature"},
        {"geometry": {"coordinates": [[[5.5, 5.5], [6.5, 5.5], [6.5, 6.5], [5.5, 6.5], [5.5, 5.5]]], "type": "Polygon"},
            "properties": {"B": 55}, "type": "Feature"},
        {"geometry": {"coordinates": [7, 5.75], "type": "Point"},
            "properties": {"B": 65}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    // Parse GeoJSON string
    auto v1 = GeoJson<REAL>::geoJsonToVector(json1);
    auto v2 = GeoJson<REAL>::geoJsonToVector(json2);

    // Add vectors
    v1+=v2;

    // Test values
    REQUIRE(v1.template getProperty<int>( 0, "A") == 10);
    REQUIRE(v1.template getProperty<int>( 5, "A") == 60);
    REQUIRE(isInvalid<int>(v1.template getProperty<int>( 6, "A")));
    REQUIRE(isInvalid<int>(v1.template getProperty<int>(11, "A")));

    REQUIRE(isInvalid<int>(v1.template getProperty<int>( 0, "B")));
    REQUIRE(isInvalid<int>(v1.template getProperty<int>( 5, "B")));
    REQUIRE(v1.template getProperty<int>( 6, "B") == 15);
    REQUIRE(v1.template getProperty<int>(11, "B") == 65);

    REQUIRE( v1.getPointCoordinate(v1.getPointIndexes()[0]) == Coordinate<REAL>({ 0, 1.5 }) );
    REQUIRE( v1.getPointCoordinate(v1.getPointIndexes()[1]) == Coordinate<REAL>({ 2, 0.75 }) );
    REQUIRE( v1.getPointCoordinate(v1.getPointIndexes()[2]) == Coordinate<REAL>({ 5, 6.5 }) );
    REQUIRE( v1.getPointCoordinate(v1.getPointIndexes()[3]) == Coordinate<REAL>({ 7, 5.75 }) );

}

TEST_CASE( "Geohash", "[vector]" ) {

    // Create vector data
    Coordinate<double> c( { 144.9631, -37.8136 } );
    std::string geoHash = c.getGeoHash();

    // Test values
    REQUIRE( geoHash == "r1r0fsnzv41c" );
}

TEST_CASE( "GeoJSON", "[vector]" ) {

    // Create GeoJSON string
    std::string inputGeoJSON = R"({"features": [
        {"geometry": {"coordinates": [0, 0.5], "type": "Point"},
            "properties": {"p0": "pstr", "p1": 1, "p2": 1.1000000000000001}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"l0": "lstr", "l1": 2, "l2": 2.2000000000000002}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"y0": "ystr", "y1": 3, "y2": 3.2999999999999998}, "type": "Feature"}], "type": "FeatureCollection", "bbox":[0,0,3,1]})";

    // Parse GeoJSON string
    auto vector = GeoJson<double>::geoJsonToVector(inputGeoJSON);

    // Convert Vector to GeoJSON
    std::string outputGeoJSON = GeoJson<double>::vectorToGeoJson(vector);

    // Test strings
    REQUIRE( Strings::removeWhitespace(outputGeoJSON) == Strings::removeWhitespace(inputGeoJSON) );

    // Test properties
    auto &pointIndexes = vector.getPointIndexes();
    auto &lineStringIndexes = vector.getLineStringIndexes();
    auto &polygonIndexes = vector.getPolygonIndexes();
    REQUIRE( vector.template getProperty<std::string>(pointIndexes[0], "p0") == "pstr" );
    REQUIRE( vector.template getProperty<int>(lineStringIndexes[0], "l1") == 2 );
    REQUIRE( vector.template getProperty<double>(polygonIndexes[0], "y2") == (double)3.2999999999999998 );
}


TEST_CASE( "GeoWKT", "[vector]" ) {

    // Create GeoWKT string
    std::string inputGeoWKT = R"(GEOMETRYCOLLECTION (POINT (0.000000 0.500000), LINESTRING (0.000000 0.000000, 1.000000 1.000000, 2.000000 0.000000, 3.000000 1.000000), POLYGON ((0.000000 0.000000, 1.000000 0.000000, 1.000000 1.000000, 0.000000 1.000000, 0.000000 0.000000), (0.250000 0.250000, 0.250000 0.750000, 0.750000 0.750000, 0.750000 0.250000, 0.250000 0.250000))))";

    // Convert Vector to GeoWKT
    auto v = GeoWKT<double>::geoWKTToVector(inputGeoWKT);
    std::string outputGeoWKT = GeoWKT<double>::vectorToGeoWKT(v);
    REQUIRE( inputGeoWKT == outputGeoWKT );

    auto v0 = Vector<double>();
    v0.addPoint( { 0.0, 0.0 } );
    std::string outputGeoWKTPoint = GeoWKT<double>::vectorToGeoWKT(v0);
    REQUIRE( outputGeoWKTPoint == std::string("POINT (0.000000 0.000000)") );

    auto v1 = Vector<double>();
    v1.addLineString( { { 0.0, 0.0 }, { 1.0, 1.0 } } );
    std::string outputGeoWKTLineString = GeoWKT<double>::vectorToGeoWKT(v1);
    REQUIRE( outputGeoWKTLineString == std::string("LINESTRING (0.000000 0.000000, 1.000000 1.000000)") );

    auto v2 = Vector<double>();
    v2.addPolygon( { { { 0.0, 0.0 }, { 1.0, 1.0 }, { 2.0, 2.0 } } } );
    std::string outputGeoWKTPolygon = GeoWKT<double>::vectorToGeoWKT(v2);
    REQUIRE( outputGeoWKTPolygon == std::string("POLYGON ((0.000000 0.000000, 1.000000 1.000000, 2.000000 2.000000, 0.000000 0.000000))") );

    auto v3 = Vector<double>();
    Coordinate<double> c0, c1;

    std::vector<cl_uint> ids;
    ids = GeoWKT<double>::parseString(v3, "POINT (0.0 0.5)");
    REQUIRE( ids.size() == 1 );
    REQUIRE( ids[0] == 0 );
    c0 = v3.getPointCoordinate(ids[0]);
    REQUIRE( c0 == Coordinate<double>( { 0.0, 0.5 } ) );

    ids = GeoWKT<double>::parseString(v3, "POINT (1.0 1.5)");
    REQUIRE( ids.size() == 1 );
    REQUIRE( ids[0] == 1 );
    c0 = v3.getPointCoordinate(ids[0]);
    REQUIRE( c0 == Coordinate<double>( { 1.0, 1.5 } ) );

    ids = GeoWKT<double>::parseStrings(v3, { "POINT (2.0 2.5)", "POINT (3.0 3.5)" } );
    REQUIRE( ids.size() == 2 );
    REQUIRE( ids[0] == 2 );
    REQUIRE( ids[1] == 3 );
    c0 = v3.getPointCoordinate(ids[0]);
    c1 = v3.getPointCoordinate(ids[1]);
    REQUIRE( c0 == Coordinate<double>( { 2.0, 2.5 } ) );
    REQUIRE( c1 == Coordinate<double>( { 3.0, 3.5 } ) );
}

TEST_CASE( "Vector projection from WKT", "[projection]" ) {

    // Create projections
    std::string projWKT_EPSG4326 = R"(GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]])";
    auto proj_EPSG4326 = Projection::parseWKT(projWKT_EPSG4326);

    std::string projWKT_EPSG3111 = R"(PROJCS["GDA94 / Vicgrid94",GEOGCS["GDA94",DATUM["Geocentric_Datum_of_Australia_1994",
        SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6283"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],
        PROJECTION["Lambert_Conformal_Conic_2SP"],PARAMETER["standard_parallel_1",-36],PARAMETER["standard_parallel_2",-38],PARAMETER["latitude_of_origin",-37],PARAMETER["central_meridian",145],PARAMETER["false_easting",2500000],PARAMETER["false_northing",2500000],AUTHORITY["EPSG","3111"],AXIS["Easting",EAST],AXIS["Northing",NORTH]])";
    auto proj_EPSG3111 = Projection::parseWKT(projWKT_EPSG3111);

    std::string projWKT_EPSG3577 = R"(PROJCS["GDA94 / Australian Albers",GEOGCS["GDA94",DATUM["Geocentric_Datum_of_Australia_1994",
        SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6283"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],
        PROJECTION["Albers_Conic_Equal_Area"],PARAMETER["standard_parallel_1",-18],PARAMETER["standard_parallel_2",-36],PARAMETER["latitude_of_center",0],PARAMETER["longitude_of_center",132],PARAMETER["false_easting",0],PARAMETER["false_northing",0],AUTHORITY["EPSG","3577"],AXIS["Easting",EAST],AXIS["Northing",NORTH]])";
    auto proj_EPSG3577 = Projection::parseWKT(projWKT_EPSG3577);

    std::string projWKT_EPSG28355 = R"(PROJCS["GDA94 / MGA zone 55",GEOGCS["GDA94",DATUM["Geocentric_Datum_of_Australia_1994",
        SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6283"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],
        PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",147],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",10000000],AUTHORITY["EPSG","28355"],AXIS["Easting",EAST],AXIS["Northing",NORTH]])";
    auto proj_EPSG28355 = Projection::parseWKT(projWKT_EPSG28355);

    std::string projWKT_EPSG3832 = R"(PROJCS["WGS 84 / PDC Mercator",GEOGCS["WGS 84",DATUM["World Geodetic System 1984",
        SPHEROID["WGS 84",6378137.0,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0.0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.017453292519943295],AXIS["Geodetic latitude",NORTH],AXIS["Geodetic longitude",EAST],AUTHORITY["EPSG","4326"]],
        PROJECTION["Mercator_1SP"],PARAMETER["latitude_of_origin",0.0],PARAMETER["central_meridian",150.0],PARAMETER["scale_factor",1.0],PARAMETER["false_easting",0.0],PARAMETER["false_northing",0.0],UNIT["m",1.0],AXIS["Easting",EAST],AXIS["Northing",NORTH],AUTHORITY["EPSG","3832"]])";
    auto proj_EPSG3832 = Projection::parseWKT(projWKT_EPSG3832);

    std::string projWKT_EPSG3857 = R"(PROJCS["WGS 84 / Pseudo-Mercator",GEOGCS["WGS 84",DATUM["WGS_1984",
        SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],
        PROJECTION["Mercator_1SP"],PARAMETER["central_meridian",0],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["X",EAST],AXIS["Y",NORTH],
        EXTENSION["PROJ4","+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs"],AUTHORITY["EPSG","3857"]])";
    auto proj_EPSG3857 = Projection::parseWKT(projWKT_EPSG3857);

    // Test coordinate
    Coordinate<double> c_Elliptical = { 144.9631, -37.8136 };

    // Test Lambert conformal conic
    Coordinate<double> c_Lambert = c_Elliptical;
    Coordinate<double> c_Lambert_Target = { 2496750.96316546, 2409712.42995386 };
    Projection::convert(c_Lambert, proj_EPSG3111, proj_EPSG4326);
    double delta_Lambert = std::hypot(c_Lambert.p-c_Lambert_Target.p, c_Lambert.q-c_Lambert_Target.q);
    REQUIRE( delta_Lambert < 1.0E-3 );

    Projection::convert(c_Lambert, proj_EPSG4326, proj_EPSG3111);
    delta_Lambert = std::hypot(c_Lambert.p-c_Elliptical.p, c_Lambert.q-c_Elliptical.q);
    REQUIRE( delta_Lambert < 1.0E-6 );

    // Test Albers conic
    Coordinate<double> c_Albers = c_Elliptical;
    Coordinate<double> c_Albers_Target = { 1146469.071166, -4192119.31810244 };
    Projection::convert(c_Albers, proj_EPSG3577, proj_EPSG4326);
    double delta_Albers = std::hypot(c_Albers.p-c_Albers_Target.p, c_Albers.q-c_Albers_Target.q);
    REQUIRE( delta_Albers < 1.0E-3 );

    Projection::convert(c_Albers, proj_EPSG4326, proj_EPSG3577);
    delta_Albers = std::hypot(c_Albers.p-c_Elliptical.p, c_Albers.q-c_Elliptical.q);
    REQUIRE( delta_Albers < 1.0E-6 );

    // Test Transverse Mercator
    Coordinate<double> c_TM = c_Elliptical;
    Coordinate<double> c_TM_Target = { 320704.446321103, 5812911.69953155 };
    Projection::convert(c_TM, proj_EPSG28355, proj_EPSG4326);
    double delta_TM = std::hypot(c_TM.p-c_TM_Target.p, c_TM.q-c_TM_Target.q);
    REQUIRE( delta_TM < 1.0E-3 );

    Projection::convert(c_TM, proj_EPSG4326, proj_EPSG28355);
    delta_TM = std::hypot(c_TM.p-c_Elliptical.p, c_TM.q-c_Elliptical.q);
    REQUIRE( delta_TM < 1.0E-6 );

    // Test Mercator
    Coordinate<double> c_Mercator = c_Elliptical;
    Coordinate<double> c_Mercator_Target = { -560705.14, -4526927.40 };
    Projection::convert(c_Mercator, proj_EPSG3832, proj_EPSG4326);
    double delta_Mercator = std::hypot(c_Mercator.p-c_Mercator_Target.p, c_Mercator.q-c_Mercator_Target.q);
    REQUIRE( delta_Mercator < 1.0E-2 );

    Projection::convert(c_Mercator, proj_EPSG4326, proj_EPSG3832);
    delta_Mercator = std::hypot(c_Mercator.p-c_Elliptical.p, c_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Mercator < 1.0E-6 );

    // Test Web Mercator
    Coordinate<double> c_Web_Mercator = c_Elliptical;
    Coordinate<double> c_Web_Mercator_Target = { 16137218.48, -4553127.11 };
    Projection::convert(c_Web_Mercator, proj_EPSG3857, proj_EPSG4326);
    double delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Web_Mercator_Target.p, c_Web_Mercator.q-c_Web_Mercator_Target.q);
    REQUIRE( delta_Web_Mercator < 1.0E-2 );

    Projection::convert(c_Web_Mercator, proj_EPSG4326, proj_EPSG3857);
    delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Elliptical.p, c_Web_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Web_Mercator < 1.0E-6 );

    // Test multi Lambert conformal conic
    std::vector<Coordinate<REAL> > c_Lambert_multi;
    Coordinate<REAL> c_Lambert_Target_REAL = { (REAL)2496750.96316546, (REAL)2409712.42995386 };
    for (int i = 0; i < 10; i++) {
        c_Lambert_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Lambert_multi, proj_EPSG3111, proj_EPSG4326);
    REAL delta_Lambert_REAL = std::hypot(c_Lambert_multi[0].p-c_Lambert_Target_REAL.p, c_Lambert_multi[0].q-c_Lambert_Target_REAL.q);
    REQUIRE( delta_Lambert_REAL < 1.0 );

    // Test multi Albers
    std::vector<Coordinate<REAL> > c_Albers_multi;
    Coordinate<REAL> c_Albers_Target_REAL = { (REAL)1146469.071166, (REAL)-4192119.31810244 };
    for (int i = 0; i < 10; i++) {
        c_Albers_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Albers_multi, proj_EPSG3577, proj_EPSG4326);
    REAL delta_Albers_REAL = std::hypot(c_Albers_multi[0].p-c_Albers_Target_REAL.p, c_Albers_multi[0].q-c_Albers_Target_REAL.q);
    REQUIRE( delta_Albers_REAL < 10.0 );

    // Test multi Transverse Mercator
    std::vector<Coordinate<REAL> > c_TM_multi;
    Coordinate<REAL> c_TM_Target_REAL = { (REAL)320704.446321103, (REAL)5812911.69953155 };
    for (int i = 0; i < 10; i++) {
        c_TM_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_TM_multi, proj_EPSG28355, proj_EPSG4326);
    REAL delta_UTM_REAL = std::hypot(c_TM_multi[0].p-c_TM_Target_REAL.p, c_TM_multi[0].q-c_TM_Target_REAL.q);
    REQUIRE( delta_UTM_REAL < 1.0 );

    // Test multi Mercator
    std::vector<Coordinate<REAL> > c_Mercator_multi;
    Coordinate<REAL> c_Mercator_Target_REAL = { (REAL)-560705.14, (REAL)-4526927.40 };
    for (int i = 0; i < 10; i++) {
        c_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Mercator_multi, proj_EPSG3832, proj_EPSG4326);
    REAL delta_Mercator_REAL = std::hypot(c_Mercator_multi[0].p-c_Mercator_Target_REAL.p, c_Mercator_multi[0].q-c_Mercator_Target_REAL.q);
    REQUIRE( delta_Mercator_REAL < 10.0 );

    // Test multi Web Mercator
    std::vector<Coordinate<REAL> > c_Web_Mercator_multi;
    Coordinate<REAL> c_Web_Mercator_Target_REAL = { (REAL)16137218.48, (REAL)-4553127.11 };
    for (int i = 0; i < 10; i++) {
        c_Web_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Web_Mercator_multi, proj_EPSG3857, proj_EPSG4326);
    REAL delta_Web_Mercator_REAL = std::hypot(c_Web_Mercator_multi[0].p-c_Web_Mercator_Target_REAL.p, c_Web_Mercator_multi[0].q-c_Web_Mercator_Target_REAL.q);
    REQUIRE( delta_Web_Mercator_REAL < 10.0 );
}

TEST_CASE( "Vector projection from PROJ4", "[projection]" ) {

    // Create projections
    std::string projPROJ4_EPSG4326 = R"(+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs)";
    auto proj_EPSG4326 = Projection::parsePROJ4(projPROJ4_EPSG4326);

    std::string projPROJ4_EPSG3111 = R"(+proj=lcc +lat_1=-36 +lat_2=-38 +lat_0=-37 +lon_0=145 +x_0=2500000 +y_0=2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)";
    auto proj_EPSG3111 = Projection::parsePROJ4(projPROJ4_EPSG3111);

    std::string projPROJ4_EPSG3577 = R"(+proj=aea +lat_1=-18 +lat_2=-36 +lat_0=0 +lon_0=132 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)";
    auto proj_EPSG3577 = Projection::parsePROJ4(projPROJ4_EPSG3577);

    std::string projPROJ4_UTM_EPSG28355 = R"(+proj=utm +zone=55 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)";
    auto proj_UTM_EPSG28355 = Projection::parsePROJ4(projPROJ4_UTM_EPSG28355);

    std::string projPROJ4_TM_EPSG28355 = R"(+proj=tmerc +lat_0=0 +lon_0=147 +k=0.9995999932289124 +x_0=500000 +y_0=10000000 +a=6378137 +b=6356752.314475018 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)";
    auto proj_TM_EPSG28355 = Projection::parsePROJ4(projPROJ4_TM_EPSG28355);

    std::string projPROJ4_EPSG3832 = R"(+proj=merc +lon_0=150 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs)";
    auto proj_EPSG3832 = Projection::parsePROJ4(projPROJ4_EPSG3832);

    std::string projPROJ4_EPSG3857 = R"(+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +no_defs)";
    auto proj_EPSG3857 = Projection::parsePROJ4(projPROJ4_EPSG3857);

    std::string projPROJ4_SRORG6974 = R"(+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs)";
    auto proj_SRORG6974 = Projection::parsePROJ4(projPROJ4_SRORG6974);

    // Test coordinate
    Coordinate<double> c_Elliptical = { 144.9631, -37.8136 };

    // Test Lambert conformal conic
    Coordinate<double> c_Lambert = c_Elliptical;
    Coordinate<double> c_Lambert_Target = { 2496750.96316546, 2409712.42995386 };
    Projection::convert(c_Lambert, proj_EPSG3111, proj_EPSG4326);
    double delta_Lambert = std::hypot(c_Lambert.p-c_Lambert_Target.p, c_Lambert.q-c_Lambert_Target.q);
    REQUIRE( delta_Lambert < 1.0E-3 );

    Projection::convert(c_Lambert, proj_EPSG4326, proj_EPSG3111);
    delta_Lambert = std::hypot(c_Lambert.p-c_Elliptical.p, c_Lambert.q-c_Elliptical.q);
    REQUIRE( delta_Lambert < 1.0E-6 );

    // Test Albers conic
    Coordinate<double> c_Albers = c_Elliptical;
    Coordinate<double> c_Albers_Target = { 1146469.071166, -4192119.31810244 };
    Projection::convert(c_Albers, proj_EPSG3577, proj_EPSG4326);
    double delta_Albers = std::hypot(c_Albers.p-c_Albers_Target.p, c_Albers.q-c_Albers_Target.q);
    REQUIRE( delta_Albers < 1.0E-3 );

    Projection::convert(c_Albers, proj_EPSG4326, proj_EPSG3577);
    delta_Albers = std::hypot(c_Albers.p-c_Elliptical.p, c_Albers.q-c_Elliptical.q);
    REQUIRE( delta_Albers < 1.0E-6 );

    // Test Transverse Mercator
    Coordinate<double> c_TM = c_Elliptical;
    Coordinate<double> c_TM_Target = { 320704.447532, 5812911.72786 };
    Projection::convert(c_TM, proj_TM_EPSG28355, proj_EPSG4326);
    double delta_TM = std::hypot(c_TM.p-c_TM_Target.p, c_TM.q-c_TM_Target.q);
    REQUIRE( delta_TM < 1.0E-3 );

    Projection::convert(c_TM, proj_EPSG4326, proj_TM_EPSG28355);
    delta_TM = std::hypot(c_TM.p-c_Elliptical.p, c_TM.q-c_Elliptical.q);
    REQUIRE( delta_TM < 1.0E-6 );

    // Test Universal Transverse Mercator
    Coordinate<double> c_UTM = c_Elliptical;
    Coordinate<double> c_UTM_Target = { 320704.446321103, 5812911.69953155 };
    Projection::convert(c_UTM, proj_UTM_EPSG28355, proj_EPSG4326);
    double delta_UTM = std::hypot(c_UTM.p-c_UTM_Target.p, c_UTM.q-c_UTM_Target.q);
    REQUIRE( delta_UTM < 1.0E-3 );

    Projection::convert(c_UTM, proj_EPSG4326, proj_UTM_EPSG28355);
    delta_UTM = std::hypot(c_UTM.p-c_Elliptical.p, c_UTM.q-c_Elliptical.q);
    REQUIRE( delta_UTM < 1.0E-6 );

    // Test Mercator
    Coordinate<double> c_Mercator = c_Elliptical;
    Coordinate<double> c_Mercator_Target = { -560705.14, -4526927.40 };
    Projection::convert(c_Mercator, proj_EPSG3832, proj_EPSG4326);
    double delta_Mercator = std::hypot(c_Mercator.p-c_Mercator_Target.p, c_Mercator.q-c_Mercator_Target.q);
    REQUIRE( delta_Mercator < 1.0E-2 );

    Projection::convert(c_Mercator, proj_EPSG4326, proj_EPSG3832);
    delta_Mercator = std::hypot(c_Mercator.p-c_Elliptical.p, c_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Mercator < 1.0E-6 );

    // Test Web Mercator
    Coordinate<double> c_Web_Mercator = c_Elliptical;
    Coordinate<double> c_Web_Mercator_Target = { 16137218.48, -4553127.11 };
    Projection::convert(c_Web_Mercator, proj_EPSG3857, proj_EPSG4326);
    double delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Web_Mercator_Target.p, c_Web_Mercator.q-c_Web_Mercator_Target.q);
    REQUIRE( delta_Web_Mercator < 1.0E-2 );

    Projection::convert(c_Web_Mercator, proj_EPSG4326, proj_EPSG3857);
    delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Elliptical.p, c_Web_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Web_Mercator < 1.0E-6 );

    // Test Sinusoidal
    Coordinate<double> c_Sinusoidal = c_Elliptical;
    Coordinate<double> c_Sinusoidal_Target = { 12764626.0593, -4186808.60447 };
    Projection::convert(c_Sinusoidal, proj_SRORG6974, proj_EPSG4326);
    double delta_Sinusoidal = std::hypot(c_Sinusoidal.p-c_Sinusoidal_Target.p, c_Sinusoidal.q-c_Sinusoidal_Target.q);
    REQUIRE( delta_Sinusoidal < 1.0E-2 );

    Projection::convert(c_Sinusoidal, proj_EPSG4326, proj_SRORG6974);
    delta_Sinusoidal = std::hypot(c_Sinusoidal.p-c_Elliptical.p, c_Sinusoidal.q-c_Elliptical.q);
    REQUIRE( delta_Sinusoidal < 1.0E-6 );

    // Test multi Lambert conformal conic
    std::vector<Coordinate<REAL> > c_Lambert_multi;
    Coordinate<REAL> c_Lambert_Target_REAL = { (REAL)2496750.96316546, (REAL)2409712.42995386 };
    for (int i = 0; i < 10; i++) {
        c_Lambert_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Lambert_multi, proj_EPSG3111, proj_EPSG4326);
    REAL delta_Lambert_REAL = std::hypot(c_Lambert_multi[0].p-c_Lambert_Target_REAL.p, c_Lambert_multi[0].q-c_Lambert_Target_REAL.q);
    REQUIRE( delta_Lambert_REAL < 1.0 );

    // Test multi Albers
    std::vector<Coordinate<REAL> > c_Albers_multi;
    Coordinate<REAL> c_Albers_Target_REAL = { (REAL)1146469.071166, (REAL)-4192119.31810244 };
    for (int i = 0; i < 10; i++) {
        c_Albers_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Albers_multi, proj_EPSG3577, proj_EPSG4326);
    REAL delta_Albers_REAL = std::hypot(c_Albers_multi[0].p-c_Albers_Target_REAL.p, c_Albers_multi[0].q-c_Albers_Target_REAL.q);
    REQUIRE( delta_Albers_REAL < 10.0 );

    // Test multi Universal Transverse Mercator
    std::vector<Coordinate<REAL> > c_UTM_multi;
    Coordinate<REAL> c_UTM_Target_REAL = { (REAL)320704.446321103, (REAL)5812911.69953155 };
    for (int i = 0; i < 10; i++) {
        c_UTM_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_UTM_multi, proj_UTM_EPSG28355, proj_EPSG4326);
    REAL delta_UTM_REAL = std::hypot(c_UTM_multi[0].p-c_UTM_Target_REAL.p, c_UTM_multi[0].q-c_UTM_Target_REAL.q);
    REQUIRE( delta_UTM_REAL < 1.0 );

    // Test multi Mercator
    std::vector<Coordinate<REAL> > c_Mercator_multi;
    Coordinate<REAL> c_Mercator_Target_REAL = { (REAL)-560705.14, (REAL)-4526927.40 };
    for (int i = 0; i < 10; i++) {
        c_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Mercator_multi, proj_EPSG3832, proj_EPSG4326);
    REAL delta_Mercator_REAL = std::hypot(c_Mercator_multi[0].p-c_Mercator_Target_REAL.p, c_Mercator_multi[0].q-c_Mercator_Target_REAL.q);
    REQUIRE( delta_Mercator_REAL < 10.0 );

    // Test multi Web Mercator
    std::vector<Coordinate<REAL> > c_Web_Mercator_multi;
    Coordinate<REAL> c_Web_Mercator_Target_REAL = { (REAL)16137218.48, (REAL)-4553127.11 };
    for (int i = 0; i < 10; i++) {
        c_Web_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Web_Mercator_multi, proj_EPSG3857, proj_EPSG4326);
    REAL delta_Web_Mercator_REAL = std::hypot(c_Web_Mercator_multi[0].p-c_Web_Mercator_Target_REAL.p, c_Web_Mercator_multi[0].q-c_Web_Mercator_Target_REAL.q);
    REQUIRE( delta_Web_Mercator_REAL < 10.0 );

    // Test multi Sinusoidal
    std::vector<Coordinate<REAL> > c_Sinusoidal_multi;
    Coordinate<REAL> c_Sinusoidal_Target_REAL = { (REAL)12764626.0593, (REAL)-4186808.60447 };
    for (int i = 0; i < 10; i++) {
        c_Sinusoidal_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Sinusoidal_multi, proj_SRORG6974, proj_EPSG4326);
    REAL delta_Sinusoidal_REAL = std::hypot(c_Sinusoidal_multi[0].p-c_Sinusoidal_Target_REAL.p, c_Sinusoidal_multi[0].q-c_Sinusoidal_Target_REAL.q);
    REQUIRE( delta_Sinusoidal_REAL < 10.0 );

}

TEST_CASE( "Time parsing", "[time]" ) {

    // Create vector data
    auto timeSinceEpoch = Strings::iso8601toEpoch("2010-01-10T08:05:10Z");
    auto failTest = Strings::iso8601toEpoch("FAIL_TEST");

    // Test values
    REQUIRE( timeSinceEpoch == 1263110710000 );
    REQUIRE( failTest == getNullValue<int64_t>() );
}

TEST_CASE( "Series", "[series]" ) {

    // Test constant series
    Series<double, double> s0;
    s0.addValue(1.0, 2.0);

    REQUIRE( s0.isInitialised() == true );
    REQUIRE( s0.isConstant() == true );
    REQUIRE( s0(1.0) == 2.0 );

    // Test series
    Series<double, double> s1;
    s1.addValues( { { 1.0, 20.0 }, { 0.0, 10.0 }, { 10.0, 0.0 }, { 2.0, 30.0 } } );

    REQUIRE( s1.isInitialised() == true );
    REQUIRE( s1.isConstant() == false );
    REQUIRE( s1.get_xMin() == 0.0 );
    REQUIRE( s1.get_xMax() == 10.0 );
    REQUIRE( s1.get_yMin() == 0.0 );
    REQUIRE( s1.get_yMax() == 30.0 );
    REQUIRE( s1(0.5) == 15.0 );
    REQUIRE( s1(1.5) == 25.0 );

    // Test series monotone interpolation
    s1.setInterpolation(SeriesInterpolation::MonotoneCubic);
    REQUIRE( s1(6.0) == 18.75 );

    // Test bounded linear interpolation
    s1.setInterpolation(SeriesInterpolation::BoundedLinear);
    s1.setBounds(0.0, 40.0);
    REQUIRE( s1(6.0) == 35.0 );

    // Test string-based series
    Series<double, double> s2;
    s2.addValues( { { "1.0", 20.0 }, { "0.0", 10.0 }, { "10.0", 0.0 }, { "2.0", 30.0 } } );

    REQUIRE( s2.isInitialised() == true );
    REQUIRE( s2.isConstant() == false );
    REQUIRE( s2.get_xMin() == 0.0 );
    REQUIRE( s2.get_xMax() == 10.0 );
    REQUIRE( s2.get_yMin() == 0.0 );
    REQUIRE( s2.get_yMax() == 30.0 );
    REQUIRE( s2(0.5) == 15.0 );
    REQUIRE( s2(1.5) == 25.0 );

    // Test long series
    Series<int64_t, double> s3;
    s3.addValues( { { 1, 20.0 }, { 0, 10.0 }, { 10, 0.0 }, { 2, 30.0 } } );

    REQUIRE( s3.isInitialised() == true );
    REQUIRE( s3.isConstant() == false );
    REQUIRE( s3.get_xMin() == 0.0 );
    REQUIRE( s3.get_xMax() == 10.0 );
    REQUIRE( s3.get_yMin() == 0.0 );
    REQUIRE( s3.get_yMax() == 30.0 );
    REQUIRE( s3(6) == 15.0 );
}

TEST_CASE( "Time series", "[series]" ) {

    // Test string-based series
    Series<int64_t, double> s0;
    s0.addValues( {
        { "2010-01-10T08:06:10Z", 20.0 },
        { "2010-01-10T08:05:10Z", 10.0 },
        { "2010-01-10T08:15:10Z", 0.0 },
        { "2010-01-10T08:07:10Z", 30.0 }
    } );

    REQUIRE( s0.isInitialised() == true );
    REQUIRE( s0.isConstant() == false );
    REQUIRE( s0.get_xMin() == Strings::iso8601toEpoch("2010-01-10T08:05:10Z") );
    REQUIRE( s0.get_xMax() == Strings::iso8601toEpoch("2010-01-10T08:15:10Z") );
    REQUIRE( s0.get_yMin() == 0.0 );
    REQUIRE( s0.get_yMax() == 30.0 );
    REQUIRE( s0(Strings::iso8601toEpoch("2010-01-10T08:05:40Z")) == 15.0 );
    REQUIRE( s0(Strings::iso8601toEpoch("2010-01-10T08:06:40Z")) == 25.0 );
}


TEST_CASE( "Network", "[solver]" ) {

    // Create GeoJSON network string
    std::string json = R"({"features":[
    {"geometry":{"coordinates":[[100,140],[100,0]],"type":"LineString"},"properties":{"constant":100,"diameter":0.3,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[100,0],[100,-165.818]],"type":"LineString"},"properties":{"constant":100,"diameter":0.3,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[100,0],[-10,0]],"type":"LineString"},"properties":{"constant":100,"diameter":0.2,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[100,-165.818],[-10,-120]],"type":"LineString"},"properties":{"constant":100,"diameter":0.18,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[-10,0],[-10,-120]],"type":"LineString"},"properties":{"constant":100,"diameter":0.18,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[-10,0],[-120,0]],"type":"LineString"},"properties":{"constant":100,"diameter":0.2,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[-10,-120],[-120,-120]],"type":"LineString"},"properties":{"constant":100,"diameter":0.18,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[100,140],[-60,140]],"type":"LineString"},"properties":{"constant":100,"diameter":0.2,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[-60,140],[-120,0]],"type":"LineString"},"properties":{"constant":100,"diameter":0.18,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[-120,0],[-120,-120]],"type":"LineString"},"properties":{"constant":100,"diameter":0.18,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[100,140],"type":"Point"},"properties":{"flow":0.126},"type":"Feature"},
    {"geometry":{"coordinates":[100,0],"type":"Point"},"properties":{"flow":-0.025},"type":"Feature"},
    {"geometry":{"coordinates":[100,-165.818],"type":"Point"},"properties":{"flow":-0.05},"type":"Feature"},
    {"geometry":{"coordinates":[-10,0],"type":"Point"},"properties":{"flow":0},"type":"Feature"},
    {"geometry":{"coordinates":[-10,-120],"type":"Point"},"properties":{"flow":0},"type":"Feature"},
    {"geometry":{"coordinates":[-60,140],"type":"Point"},"properties":{"flow":0},"type":"Feature"},
    {"geometry":{"coordinates":[-120,0],"type":"Point"},"properties":{"flow":-0.025},"type":"Feature"}],"type":"FeatureCollection"})";

    // Parse GeoJSON string
    auto v = GeoJson<REAL>::geoJsonToVector(json, false);

    // Create flow solver instance
    NetworkFlowSolver<REAL> networkFlowSolver;
    bool initSuccess = networkFlowSolver.init(v);
    REQUIRE( initSuccess == true );

    // Run flow solver
    bool runSuccess = networkFlowSolver.run();
    REQUIRE( runSuccess == true );

    // Get values
    auto &vn = networkFlowSolver.getNetwork();
    auto &li = vn.getLineStringIndexes();
    auto &properties = vn.getProperties();
    auto &flow = properties.template getPropertyRef<std::vector<REAL> >("flow");

    // Get flow rates, compare to 4 loop analysis iterations:
    REAL flow_err_0 = fabs(flow[li[0]]-0.10190);
    REAL flow_err_1 = fabs(flow[li[1]]-0.02410);
    REAL flow_err_2 = fabs(flow[li[2]]-0.05511);
    REAL flow_err_3 = fabs(flow[li[3]]-0.02179);
    REAL flow_err_4 = fabs(flow[li[4]]-0.00511);
    REAL flow_err_5 = fabs(flow[li[5]]-0.00841);
    REAL flow_err_6 = fabs(flow[li[6]]-0.01338);
    REAL flow_err_7 = fabs(flow[li[7]]-0.01352);
    REAL flow_err_8 = fabs(flow[li[8]]-0.02410);
    REAL flow_err_9 = fabs(flow[li[9]]-0.01248);

    REQUIRE( flow_err_0 < 1.0E-3 );
    REQUIRE( flow_err_1 < 1.0E-3 );
    REQUIRE( flow_err_2 < 1.0E-3 );
    REQUIRE( flow_err_3 < 1.0E-3 );
    REQUIRE( flow_err_4 < 1.0E-3 );
    REQUIRE( flow_err_5 < 1.0E-3 );
    REQUIRE( flow_err_6 < 1.0E-3 );
    REQUIRE( flow_err_7 < 1.0E-3 );
    REQUIRE( flow_err_8 < 1.0E-3 );
    REQUIRE( flow_err_9 < 1.0E-3 );
}

TEST_CASE( "Level set", "[solver]" ) {

    // Create starting conditions
    auto v = Vector<REAL>();
    v.addPoint( { 0.0, 0.0 } );
    v.setProperty(0, "radius", 10);

    // Create input raster list
    auto pType = std::make_shared<Raster<REAL, REAL> >("type");
    std::vector<RasterBasePtr<REAL> > inputLayers = { pType };

    // Create output raster list
    auto pOutput = std::make_shared<Raster<REAL, REAL> >("output");
    std::vector<RasterBasePtr<REAL> > outputLayers = { pOutput };

    // Create variables
    std::shared_ptr<Variables<REAL, std::string> > variables;
    variables = std::make_shared<Variables<REAL, std::string> >();
    variables->set("varA", 77.7);
    variables->set("varB", 88.8);

    // Create type raster
    Raster<REAL, REAL> &type = *pType;
    type.init2D(1000, 1000, 1.0, 1.0, -500, -500);
    runScript<REAL>("type = x > 0 && x < 100 && y > 0 && y < 100 ? 2 : 1;", { type } );

    // Create solver configuration
    std::string config = R"({
        "resolution": 0.5,
        "initialisationScript": "class = type;",
        "buildScript": "if (class == 1) { speed = 0.5; } else if (class == 2) { speed = 2.0; }",
        "updateScript": "output = class*varA+varB;"
    })";

    // Initialise solver
    auto solver = LevelSet<REAL>();
    bool initSuccess = solver.init(config, v, variables, inputLayers, outputLayers);
    REQUIRE( initSuccess == true );

    // Run solver
    bool runSuccess = true;
    while (runSuccess && solver.getParameters().time < 100.0) {
        runSuccess &= solver.step();
    }
    REQUIRE( runSuccess == true );

    // Check values
    REQUIRE( fabs(solver.getArrival().getBilinearValue(0, -50.0)-80.0) < 1.0 );
    REQUIRE( fabs(solver.getArrival().getBilinearValue(-50.0, 0)-80.0) < 1.0 );
    REQUIRE( fabs(solver.getArrival().getBilinearValue(-50.0*M_SQRT1_2, -50.0*M_SQRT1_2)-80.0) < 1.0 );
    REQUIRE( fabs(solver.getArrival().getBilinearValue(50.0*M_SQRT1_2, 50.0*M_SQRT1_2)-20.0) < 1.0 );

    Raster<REAL, REAL> &output = *pOutput;
    REQUIRE( output.getNearestValue(0, -50.0) == (REAL)(1*77.7+88.8) );
    REQUIRE( output.getNearestValue(-50.0, 0) == (REAL)(1*77.7+88.8) );
    REQUIRE( output.getNearestValue(-50.0*M_SQRT1_2, -50.0*M_SQRT1_2) == (REAL)(1*77.7+88.8) );
    REQUIRE( output.getNearestValue(50.0*M_SQRT1_2, 50.0*M_SQRT1_2) == (REAL)(2*77.7+88.8) );

}

TEST_CASE( "Particle", "[solver]" ) {

    // Create particles
    auto p = Vector<REAL>();
    p.addProperty("test");
    for (int c = 0; c < 10; c++) {
        auto id = p.addPoint( {0, 0, 0} );
        p.setProperty(id, "test", (REAL)c);
    }

    // Create solver configuration
    std::string config = R"({
        "dt": 0.001,
        "initialisationScript": "radius = 1.0; velocity.x = sqrt(9.8/4.0); velocity.y = sqrt(9.8/4.0); velocity.z = sqrt(9.8/2.0);",
        "advectionScript": "time += dt; test += 1.0; if (position.z < 0.0) { radius = -1.0; }",
        "updateScript": "acceleration.z = -9.8;"
    } )";

    // Initialise solver
    Particle<REAL> solver;
    bool initSuccess = solver.init(config, p);

    // Run solver
    bool runSuccess = true;
    for (int i = 0; runSuccess && i < 1000; i++) {
        runSuccess &= solver.step();
    }
    REQUIRE( runSuccess == true );

    REAL test = p.template getProperty<REAL>(0, "test");
    REQUIRE( test == 1000.0 );

    REAL x = p.getCoordinate(0).p;
    REAL y = p.getCoordinate(0).q;
    REQUIRE( fabs(sqrt(x*x+y*y)-1.0 < 1.0E-3) );
}

TEST_CASE( "Multigrid", "[solver]" ) {

    // Create forcing grid
    auto pf = std::make_shared<Raster<REAL, REAL> >("f");
    pf->init2D(500, 500, 1.0, 1.0);
    pf->setAllCellValues(0.0);
    pf->setCellValue(100.0, 250, 250);
    std::vector<RasterBasePtr<REAL> > inputLayers = { pf };

    // Create solver configuration
    std::string config = R"({
        "initialisationScript" : "b = f;"
    } )";

    // Initialise solver
    Multigrid<REAL> solver;
    bool initSuccess = solver.init(config, inputLayers);

    // Run solver
    bool runSuccess = solver.step();
    REQUIRE( runSuccess == true );

    // Get solution Raster
    auto &u = solver.getSolution();

    // Test two points on the Raster for zero second derivative
    REAL d_50_50 =
        u.getCellValue(49, 50)+
        u.getCellValue(51, 50)+
        u.getCellValue(50, 49)+
        u.getCellValue(50, 51)-
        4.0*u.getCellValue(50, 50);
    REQUIRE( fabs(d_50_50) < 1.0E-3 );

    REAL d_400_100 =
        u.getCellValue(399, 100)+
        u.getCellValue(401, 100)+
        u.getCellValue(400,  99)+
        u.getCellValue(400, 101)-
        4.0*u.getCellValue(400, 100);
    REQUIRE( fabs(d_400_100) < 1.0E-3 );
}
#endif
