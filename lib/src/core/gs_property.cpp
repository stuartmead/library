/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <sstream>
#include <typeinfo>

#include "gs_solver.h"
#include "gs_property.h"

using namespace json11;

namespace Geostack
{

    static const std::vector<std::string> PropertyTypeNames ( {
       "Undefined",
       "String",
       "Signed integer",
       "Single precision float",
       "Double precision float",
       "Unsigned integer",
       "Vector of single precision floats",
       "Vector of double precision floats"
    } ); 

    /**
    * Conversion matrix
    */
    template <> std::string PropertyBase::convert<std::string, std::string>(const std::string v) {
        return v;
    }
    template <> std::string PropertyBase::convert<int32_t, std::string>(const int32_t v) {
        return std::to_string(v);
    }
    template <> std::string PropertyBase::convert<float, std::string>(const float v) {
        return std::to_string(v);
    }
    template <> std::string PropertyBase::convert<double, std::string>(const double v) {
        return std::to_string(v);
    }
    template <> std::string PropertyBase::convert<cl_uint, std::string>(const cl_uint v) {
        return std::to_string(v);
    }

    template <> int32_t PropertyBase::convert<std::string, int32_t>(const std::string v) {
        return (int32_t)std::stol(v);
    }
    template <> int32_t PropertyBase::convert<int32_t, int32_t>(const int32_t v) {
        return v;
    }
    template <> int32_t PropertyBase::convert<float, int32_t>(const float v) {
        return (int32_t)v;
    }
    template <> int32_t PropertyBase::convert<double, int32_t>(const double v) {
        return (int32_t)v;
    }
    template <> int32_t PropertyBase::convert<cl_uint, int32_t>(const cl_uint v) {
        return (int32_t)v;
    }

    template <> float PropertyBase::convert<std::string, float>(const std::string v) {
        return std::stof(v);
    }
    template <> float PropertyBase::convert<int32_t, float>(const int32_t v) {
        return (float)v;
    }
    template <> float PropertyBase::convert<float, float>(const float v) {
        return v;
    }
    template <> float PropertyBase::convert<double, float>(const double v) {
        return (float)v;
    }
    template <> float PropertyBase::convert<cl_uint, float>(const cl_uint v) {
        return (float)v;
    }

    template <> double PropertyBase::convert<std::string, double>(const std::string v) {
        return std::stod(v);
    }
    template <> double PropertyBase::convert<int32_t, double>(const int32_t v) {
        return (double)v;
    }
    template <> double PropertyBase::convert<float, double>(const float v) {
        return (double)v;
    }
    template <> double PropertyBase::convert<double, double>(const double v) {
        return v;
    }
    template <> double PropertyBase::convert<cl_uint, double>(const cl_uint v) {
        return (double)v;
    }

    template <> cl_uint PropertyBase::convert<std::string, cl_uint>(const std::string v) {
        return (cl_uint)std::stol(v);
    }
    template <> cl_uint PropertyBase::convert<int32_t, cl_uint>(const int32_t v) {
        return (cl_uint)v;
    }
    template <> cl_uint PropertyBase::convert<float, cl_uint>(const float v) {
        return (cl_uint)v;
    }
    template <> cl_uint PropertyBase::convert<double, cl_uint>(const double v) {
        return (cl_uint)v;
    }
    template <> cl_uint PropertyBase::convert<cl_uint, cl_uint>(const cl_uint v) {
        return v;
    }

    /**
    * Static %Property types
    */
    template <>
    PropertyType::Type PropertyBase::getPropertyType<std::string>() {
        return PropertyType::String;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<int32_t>() {
        return PropertyType::Integer;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<float>() {
        return PropertyType::Float;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<double>() {
        return PropertyType::Double;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<cl_uint>() {
        return PropertyType::Index;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<std::vector<std::string> >() {
        return PropertyType::String;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<std::vector<int32_t> >() {
        return PropertyType::Integer;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<std::vector<float> >() {
        return PropertyType::Float;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<std::vector<double> >() {
        return PropertyType::Double;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<std::vector<cl_uint> >() {
        return PropertyType::Index;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<std::vector<PropertyMap> >() {
        return PropertyType::Map;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<std::vector<std::vector<float> > >() {
        return PropertyType::FloatVector;
    }

    template <>
    PropertyType::Type PropertyBase::getPropertyType<std::vector<std::vector<double> > >() {
        return PropertyType::DoubleVector;
    }

    /**
    * Static %Property structures
    */
    template <>
    PropertyStructure::Type PropertyBase::getPropertyStructure<std::string>() {
        return PropertyStructure::Scalar;
    }

    template <>
    PropertyStructure::Type PropertyBase::getPropertyStructure<int32_t>() {
        return PropertyStructure::Scalar;
    }

    template <>
    PropertyStructure::Type PropertyBase::getPropertyStructure<float>() {
        return PropertyStructure::Scalar;
    }

    template <>
    PropertyStructure::Type PropertyBase::getPropertyStructure<double>() {
        return PropertyStructure::Scalar;
    }

    template <>
    PropertyStructure::Type PropertyBase::getPropertyStructure<cl_uint>() {
        return PropertyStructure::Scalar;
    }

    template <>
    PropertyStructure::Type PropertyBase::getPropertyStructure<PropertyMap>() {
        return PropertyStructure::Scalar;
    }

    template <>
    PropertyStructure::Type PropertyBase::getPropertyStructure<std::vector<std::string> >() {
        return PropertyStructure::Vector;
    }

    template <>
    PropertyStructure::Type PropertyBase::getPropertyStructure<std::vector<int32_t> >() {
        return PropertyStructure::Vector;
    }

    template <>
    PropertyStructure::Type PropertyBase::getPropertyStructure<std::vector<float> >() {
        return PropertyStructure::Vector;
    }

    template <>
    PropertyStructure::Type PropertyBase::getPropertyStructure<std::vector<double> >() {
        return PropertyStructure::Vector;
    }

    template <>
    PropertyStructure::Type PropertyBase::getPropertyStructure<std::vector<cl_uint> >() {
        return PropertyStructure::Vector;
    }

    /**
    * Default %Property constructors
    */
    template <typename PTYPE>
    Property<PTYPE>::Property(PTYPE v_): v(v_) { };

    /**
    * %Property copy constructors
    */
    template <typename PTYPE>
    Property<PTYPE>::Property(const Property<PTYPE> &r): v(r.v) { };

    /**
    * %Property assignment operator
    */
    template <typename PTYPE>
    Property<PTYPE> &Property<PTYPE>::operator=(const Property<PTYPE> &r) {
        v = r.v;
        return *this;
    };

    /**
    * Clone %Property
    */
    template <typename PTYPE>
    PropertyBase *Property<PTYPE>::clone() {
        return new Property<PTYPE>(v);
    }

    /**
    * %Property handling
    */
    template <typename PTYPE>
    template <typename RTYPE>
    RTYPE Property<PTYPE>::get() const {
        return PropertyBase::convert<PTYPE, RTYPE>(v);
    }

    template <typename PTYPE>
    template <typename RTYPE>
    void Property<PTYPE>::set(const RTYPE v_) {
        v = PropertyBase::convert<RTYPE, PTYPE>(v_);
    }

    /**
    * %Property deletion
    */
    template <>
    void Property<std::string>::clear() {
        v = getNullValue<std::string>();
    }

    template <>
    void Property<int32_t>::clear() {
        v = getNullValue<int32_t>();
    }

    template <>
    void Property<float>::clear() {
        v = getNullValue<float>();
    }

    template <>
    void Property<double>::clear() {
        v = getNullValue<double>();
    }

    template <>
    void Property<cl_uint>::clear() {
        v = getNullValue<cl_uint>();
    }

    template <>
    void Property<PropertyMap>::clear() {
        v.clear();
    }

    /**
    * %Property types
    */
    template <>
    PropertyType::Type Property<std::string>::getType() {
        return PropertyType::String;
    }

    template <>
    PropertyType::Type Property<int32_t>::getType() {
        return PropertyType::Integer;
    }

    template <>
    PropertyType::Type Property<float>::getType() {
        return PropertyType::Float;
    }

    template <>
    PropertyType::Type Property<double>::getType() {
        return PropertyType::Double;
    }

    template <>
    PropertyType::Type Property<cl_uint>::getType() {
        return PropertyType::Index;
    }

    template <>
    PropertyType::Type Property<PropertyMap>::getType() {
        return PropertyType::Map;
    }

    /**
    * %Property OpenCL types
    */
    template <>
    std::string Property<std::string>::getOpenCLTypeString() {
        throw std::runtime_error("Invalid OpenCL type of string");
    }

    template <>
    std::string Property<int32_t>::getOpenCLTypeString() {
        return Solver::getOpenCLTypeString<int32_t>();
    }

    template <>
    std::string Property<float>::getOpenCLTypeString() {
        return Solver::getOpenCLTypeString<float>();
    }

    template <>
    std::string Property<double>::getOpenCLTypeString() {
        return Solver::getOpenCLTypeString<double>();
    }

    template <>
    std::string Property<cl_uint>::getOpenCLTypeString() {
        return Solver::getOpenCLTypeString<cl_uint>();
    }

    template <>
    std::string Property<PropertyMap>::getOpenCLTypeString() {
        throw std::runtime_error("Invalid OpenCL type of map");
    }

    template <>
    std::string Property<std::vector<float> >::getOpenCLTypeString() {
        throw std::runtime_error("Invalid OpenCL type of float vector");
    }

    template <>
    std::string Property<std::vector<double> >::getOpenCLTypeString() {
        throw std::runtime_error("Invalid OpenCL type of double vector");
    }

    /**
    * %PropertyVector constructors from std::vector
    */
    template <typename PTYPE>
    PropertyVectorUnbuffered<PTYPE>::PropertyVectorUnbuffered(PTYPE v_): v(v_) { };

    template <typename PTYPE>
    PropertyVectorBuffered<PTYPE>::PropertyVectorBuffered(PTYPE v_) {
        v.getData() = v_;
    };

    /**
    * %PropertyVector copy constructors
    */
    template <typename PTYPE>
    PropertyVectorUnbuffered<PTYPE>::PropertyVectorUnbuffered(const PropertyVectorUnbuffered<PTYPE> &r): v(r.v) { };

    template <typename PTYPE>
    PropertyVectorBuffered<PTYPE>::PropertyVectorBuffered(const PropertyVectorBuffered<PTYPE> &r): v(r.v) { };

    /**
    * %PropertyVector assignment operator
    */
    template <typename PTYPE>
    PropertyVectorUnbuffered<PTYPE> &PropertyVectorUnbuffered<PTYPE>::operator=(const PropertyVectorUnbuffered<PTYPE> &r) {
        if (this != &r) {
            v = r.v;
        }
        return *this;
    };

    template <typename PTYPE>
    PropertyVectorBuffered<PTYPE> &PropertyVectorBuffered<PTYPE>::operator=(const PropertyVectorBuffered<PTYPE> &r) {
        if (this != &r) {
            v = r.v;
        }
        return *this;
    };

    /**
    * Clone %PropertyVector
    */
    template <typename PTYPE>
    PropertyBase *PropertyVectorUnbuffered<PTYPE>::clone() {
        return new PropertyVectorUnbuffered<PTYPE>(*this);
    }

    template <typename PTYPE>
    PropertyBase *PropertyVectorBuffered<PTYPE>::clone() {
        return new PropertyVectorBuffered<PTYPE>(*this);
    }

    /**
    * PropertyVector type
    */
    template <>
    PropertyType::Type PropertyVector<std::vector<std::string> >::getType() {
        return PropertyType::String;
    }

    template <>
    PropertyType::Type PropertyVector<std::vector<int32_t> >::getType() {
        return PropertyType::Integer;
    }

    template <>
    PropertyType::Type PropertyVector<std::vector<float> >::getType() {
        return PropertyType::Float;
    }

    template <>
    PropertyType::Type PropertyVector<std::vector<double> >::getType() {
        return PropertyType::Double;
    }

    template <>
    PropertyType::Type PropertyVector<std::vector<cl_uint> >::getType() {
        return PropertyType::Index;
    }

    template <>
    PropertyType::Type PropertyVector<std::vector<std::vector<float> > >::getType() {
        return PropertyType::FloatVector;
    }

    template <>
    PropertyType::Type PropertyVector<std::vector<std::vector<double> > >::getType() {
        return PropertyType::DoubleVector;
    }

    /**
    * %PropertyVector OpenCL types
    */
    template <>
    std::string PropertyVector<std::vector<std::string> >::getOpenCLTypeString() {
        throw std::runtime_error("Invalid OpenCL type of string");
    }

    template <>
    std::string PropertyVector<std::vector<int32_t> >::getOpenCLTypeString() {
        return Solver::getOpenCLTypeString<int32_t>();
    }

    template <>
    std::string PropertyVector<std::vector<float> >::getOpenCLTypeString() {
        return Solver::getOpenCLTypeString<float>();
    }

    template <>
    std::string PropertyVector<std::vector<double> >::getOpenCLTypeString() {
        return Solver::getOpenCLTypeString<double>();
    }

    template <>
    std::string PropertyVector<std::vector<cl_uint> >::getOpenCLTypeString() {
        return Solver::getOpenCLTypeString<cl_uint>();
    }

    template <>
    std::string PropertyVector<std::vector<std::vector<float> > >::getOpenCLTypeString() {
        throw std::runtime_error("Invalid OpenCL type of float vector");
    }

    template <>
    std::string PropertyVector<std::vector<std::vector<double> > >::getOpenCLTypeString() {
        throw std::runtime_error("Invalid OpenCL type of double vector");
    }

    /**
    * @PropertyMap destructor.
    */
    PropertyMap::~PropertyMap() {

        // Remove all property data
        for (auto &it : properties) {
            delete it.second;
            it.second = nullptr;
        }
    }

    /**
    * @PropertyMap copy constructor.
    */
    PropertyMap::PropertyMap(const PropertyMap &r) {

        // Clear existing properties
        for (auto pb : properties) {
            delete pb.second;
            pb.second = nullptr;
        }
        properties.clear();

        // Copy map
        for (auto pb : r.properties) {

            // Get property
            auto p = pb.second;
            properties[pb.first] = p->clone();
        }
    }

    /**
    * @PropertyMap assignment operator.
    * @param r %PropertyMap to assign from.
    */
    PropertyMap &PropertyMap::operator=(const PropertyMap &r) {

        if (this != &r) {

            // Clear existing properties
            for (auto pb : properties) {
                delete pb.second;
                pb.second = nullptr;
            }
            properties.clear();

            // Copy map
            for (auto pb : r.properties) {

                // Get property
                auto p = pb.second;
                properties[pb.first] = p->clone();
            }
        }
        return *this;
    }

    /**
    * Addition operator. Scalar properties are not overwritten,
    * vector properties are appended.
    * @param r %PropertyMap to add.
    */
    PropertyMap &PropertyMap::operator+=(const PropertyMap &r) {

        if (this != &r) {

            // Copy map
            for (auto &rit : r.properties) {

                auto rp = rit.second;

                // Search for property
                auto pit = properties.find(rit.first);
                if (pit == properties.end()) {

                    // Copy property
                    properties[rit.first] = rp->clone();

                } else if (rp->getStructure() == PropertyStructure::Vector)  {

                    auto pp = pit->second;

                    // Append property
                    if (pp->getType() == rp->getType()) {

                        switch(pp->getType()) {

                            case PropertyType::String: {
                                auto &pvec = static_cast<PropertyVectorUnbuffered<std::vector<std::string> > *>(pp)->getRef();
                                auto &rvec = static_cast<PropertyVectorUnbuffered<std::vector<std::string> > *>(rp)->getRef();
                                pvec.insert(pvec.end(), rvec.begin(), rvec.end());
                                } break;

                            case PropertyType::Integer: {
                                auto &pvec = static_cast<PropertyVectorBuffered<std::vector<int32_t> > *>(pp)->getRef();
                                auto &rvec = static_cast<PropertyVectorBuffered<std::vector<int32_t> > *>(rp)->getRef();
                                pvec.insert(pvec.end(), rvec.begin(), rvec.end());
                                } break;

                            case PropertyType::Float: {
                                auto &pvec = static_cast<PropertyVectorBuffered<std::vector<float> > *>(pp)->getRef();
                                auto &rvec = static_cast<PropertyVectorBuffered<std::vector<float> > *>(rp)->getRef();
                                pvec.insert(pvec.end(), rvec.begin(), rvec.end());
                                } break;

                            case PropertyType::Double: {
                                #if defined(REAL_DOUBLE)
                                auto &pvec = static_cast<PropertyVectorBuffered<std::vector<double> > *>(pp)->getRef();
                                auto &rvec = static_cast<PropertyVectorBuffered<std::vector<double> > *>(rp)->getRef();
                                #else
                                auto &pvec = static_cast<PropertyVectorUnbuffered<std::vector<double> > *>(pp)->getRef();
                                auto &rvec = static_cast<PropertyVectorUnbuffered<std::vector<double> > *>(rp)->getRef();
                                #endif
                                pvec.insert(pvec.end(), rvec.begin(), rvec.end());
                                } break;

                            case PropertyType::Index: {
                                auto &pvec = static_cast<PropertyVectorBuffered<std::vector<cl_uint> > *>(pp)->getRef();
                                auto &rvec = static_cast<PropertyVectorBuffered<std::vector<cl_uint> > *>(rp)->getRef();
                                pvec.insert(pvec.end(), rvec.begin(), rvec.end());
                                } break;

                            case PropertyType::FloatVector: {
                                auto &pvec = static_cast<PropertyVectorUnbuffered<std::vector<std::vector<float> > > *>(pp)->getRef();
                                auto &rvec = static_cast<PropertyVectorUnbuffered<std::vector<std::vector<float> > > *>(rp)->getRef();
                                pvec.insert(pvec.end(), rvec.begin(), rvec.end());
                                } break;

                            case PropertyType::DoubleVector: {
                                auto &pvec = static_cast<PropertyVectorUnbuffered<std::vector<std::vector<double> > > *>(pp)->getRef();
                                auto &rvec = static_cast<PropertyVectorUnbuffered<std::vector<std::vector<double> > > *>(rp)->getRef();
                                pvec.insert(pvec.end(), rvec.begin(), rvec.end());
                                } break;
                        }
                    }
                }
            }
        }
        return *this;
    }

    /**
    * Add entry to @PropertyMap.
    * @param name @PropertyMap name.
    */
    void PropertyMap::addProperty(std::string name) {

        // Set property
        if (!hasProperty(name)) {
            properties[name] = new PropertyUndefined();
        }
    }

    /**
    * Set @PropertyMap
    */
    template <>
    void PropertyMap::setProperty(std::string name, const char *v) {

        // Set property
        removeProperty(name);
        properties[name] = new Property<std::string>(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, std::string v) {

        // Set property
        removeProperty(name);
        properties[name] = new Property<std::string>(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, int32_t v) {

        // Set property
        removeProperty(name);
        properties[name] = new Property<int32_t>(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, float v) {

        // Set property
        removeProperty(name);
        properties[name] = new Property<float>(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, double v) {

        // Set property
        removeProperty(name);
        properties[name] = new Property<double>(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, cl_uint v) {

        // Set property
        removeProperty(name);
        properties[name] = new Property<cl_uint>(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, std::vector<std::string> v) {

        // Set property
        removeProperty(name);
        properties[name] = new PropertyVectorUnbuffered<std::vector<std::string> >(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, std::vector<int32_t> v) {

        // Set property
        removeProperty(name);
        properties[name] = new PropertyVectorBuffered<std::vector<int32_t> >(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, std::vector<float> v) {

        // Set property
        removeProperty(name);
        properties[name] = new PropertyVectorBuffered<std::vector<float> >(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, std::vector<double> v) {

        // Set property
        removeProperty(name);

#if defined(REAL_DOUBLE)
        properties[name] = new PropertyVectorBuffered<std::vector<double> >(v);
#else
        properties[name] = new PropertyVectorUnbuffered<std::vector<double> >(v);
#endif
    }

    template <>
    void PropertyMap::setProperty(std::string name, std::vector<cl_uint> v) {

        // Set property
        removeProperty(name);
        properties[name] = new PropertyVectorBuffered<std::vector<cl_uint> >(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, std::vector<std::vector<float> > v) {

        // Set property
        removeProperty(name);
        properties[name] = new PropertyVectorUnbuffered<std::vector<std::vector<float> > >(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, std::vector<std::vector<double> > v) {

        // Set property
        removeProperty(name);
        properties[name] = new PropertyVectorUnbuffered<std::vector<std::vector<double> > >(v);
    }

    template <>
    void PropertyMap::setProperty(std::string name, PropertyMap v) {

        // Set property
        removeProperty(name);
        properties[name] = new Property<PropertyMap>(v);
    }

    /**
    * Get a map of all properties of certain type.
    * @return map of names and values.
    */
    template <typename PTYPE>
    std::map<std::string, PTYPE> PropertyMap::getProperties() const {

        // Create map
        std::map<std::string, PTYPE> pm;
        for (auto pb : properties) {
            auto p = pb.second;
            if (PropertyBase::getPropertyType<PTYPE>() == p->getType()) {
                pm[pb.first] = static_cast<Property<PTYPE> *>(p)->template get<PTYPE>();
            }
        }
        return pm;
    }

    /**
    * Get a set of all undefined properties.
    * @return set of names.
    */
    std::set<std::string> PropertyMap::getUndefinedProperties() const {

        // Create map
        std::set<std::string> ps;
        for (auto pb : properties) {
            auto p = pb.second;
            if (p->getType() == PropertyType::Undefined) {
                ps.insert(pb.first);
            }
        }
        return ps;
    }

    /**
    * Get a map of all property references of certain type.
    * @return map of names and references.
    */
    template <typename PTYPE>
    std::map<std::string, std::reference_wrapper<PTYPE> > PropertyMap::getPropertyRefs() const {

        // Create map
        std::map<std::string, std::reference_wrapper<PTYPE> > pm;
        for (auto pb : properties) {
            auto p = pb.second;
            if (PropertyBase::getPropertyType<PTYPE>() == p->getType()) {
                pm.emplace(pb.first, std::ref(static_cast<PropertyVector<PTYPE> *>(p)->getRef()));
            }
        }
        return pm;
    }

    /**
    * Get property with conversion.
    * @return string value.
    */
    template <typename PTYPE>
    PTYPE PropertyMap::getProperty(std::string name) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {

            // Get property
            auto p = it->second;
            if (p->getStructure() == PropertyStructure::Scalar) {
                switch (p->getType()) {

                    case PropertyType::Undefined: {
                        std::stringstream err;
                        err << "Property '" << name << "' is undefined";
                        throw std::runtime_error(err.str());
                        } break;

                    case PropertyType::String:
                        return static_cast<Property<std::string> *>(p)->template get<PTYPE>();
                        break;

                    case PropertyType::Integer:
                        return static_cast<Property<int32_t> *>(p)->template get<PTYPE>();
                        break;

                    case PropertyType::Float:
                        return static_cast<Property<float> *>(p)->template get<PTYPE>();
                        break;

                    case PropertyType::Double:
                        return static_cast<Property<double> *>(p)->template get<PTYPE>();
                        break;

                    case PropertyType::Index:
                        return static_cast<Property<cl_uint> *>(p)->template get<PTYPE>();
                        break;

                    default: {
                        std::stringstream err;
                        err << "Cannot get property type " << PropertyTypeNames[PropertyBase::getPropertyType<PTYPE>()]
                            << " for property '" << name << "'";
                        throw std::runtime_error(err.str());
                        } break;
                }
            }
        }

        // No property
        std::stringstream err;
        err << "Cannot find scalar property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    template <>
    std::vector<float> PropertyMap::getProperty(std::string name) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {

            // Get property
            auto p = it->second;
            if (p->getStructure() == PropertyStructure::Scalar) {
                switch (p->getType()) {

                    case PropertyType::Undefined: {
                        std::stringstream err;
                        err << "Property '" << name << "' is undefined";
                        throw std::runtime_error(err.str());
                        } break;

                    case PropertyType::FloatVector:
                        return static_cast<Property<std::vector<float> > *>(p)->getRef();
                        break;

                    default: {
                        std::stringstream err;
                        err << "Cannot convert property '" << name << "'";
                        throw std::runtime_error(err.str());
                        } break;
                }
            }
        }

        // No property
        std::stringstream err;
        err << "Cannot find scalar property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    template <>
    std::vector<double> PropertyMap::getProperty(std::string name) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {

            // Get property
            auto p = it->second;
            if (p->getStructure() == PropertyStructure::Scalar) {
                switch (p->getType()) {

                    case PropertyType::Undefined: {
                        std::stringstream err;
                        err << "Property '" << name << "' is undefined";
                        throw std::runtime_error(err.str());
                        } break;

                    case PropertyType::DoubleVector:
                        return static_cast<Property<std::vector<double> > *>(p)->getRef();
                        break;

                    default: {
                        std::stringstream err;
                        err << "Cannot convert property '" << name << "'";
                        throw std::runtime_error(err.str());
                        } break;
                }
            }
        }

        // No property
        std::stringstream err;
        err << "Cannot find scalar property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    /**
    * Get property with conversion.
    * @return string value.
    */
    template <typename PTYPE>
    PTYPE PropertyMap::getPropertyFromVector(std::string name, std::size_t index) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {

            // Get property
            auto p = it->second;
            if (p->getStructure() == PropertyStructure::Vector) {
                switch (p->getType()) {

                    case PropertyType::Undefined: {
                        std::stringstream err;
                        err << "Property '" << name << "' is undefined";
                        throw std::runtime_error(err.str());
                        } break;

                    case PropertyType::String: {
                        auto &pr = static_cast<PropertyVectorUnbuffered<std::vector<std::string> > *>(p)->getRef();
                        if (index < pr.size()) {
                            return PropertyBase::convert<std::string, PTYPE>(pr[index]);
                        } else {
                            throw std::runtime_error("Index out of range for string vector");
                        }
                        } break;

                    case PropertyType::Integer: {
                        auto &pr = static_cast<PropertyVectorBuffered<std::vector<int32_t> > *>(p)->getRef();
                        if (index < pr.size()) {
                            return PropertyBase::convert<int32_t, PTYPE>(pr[index]);
                        } else {
                            throw std::runtime_error("Index out of range for int vector");
                        }
                        } break;

                    case PropertyType::Float: {
                        auto &pr = static_cast<PropertyVectorBuffered<std::vector<float> > *>(p)->getRef();
                        if (index < pr.size()) {
                            return PropertyBase::convert<float, PTYPE>(pr[index]);
                        } else {
                            throw std::runtime_error("Index out of range for double vector");
                        }
                        } break;

                    case PropertyType::Double: {
                        #if defined(REAL_DOUBLE)
                        auto &pr = static_cast<PropertyVectorBuffered<std::vector<double> > *>(p)->getRef();
                        #else
                        auto &pr = static_cast<PropertyVectorUnbuffered<std::vector<double> > *>(p)->getRef();
                        #endif
                        if (index < pr.size()) {
                            return PropertyBase::convert<double, PTYPE>(pr[index]);
                        } else {
                            throw std::runtime_error("Index out of range for double vector");
                        }
                        } break;

                    case PropertyType::Index: {
                        auto &pr = static_cast<PropertyVectorBuffered<std::vector<cl_uint> > *>(p)->getRef();
                        if (index < pr.size()) {
                            return PropertyBase::convert<cl_uint, PTYPE>(pr[index]);
                        } else {
                            throw std::runtime_error("Index out of range for index vector");
                        }
                        } break;

                    default: {
                        std::stringstream err;
                        err << "Cannot get vector property type " << PropertyTypeNames[PropertyBase::getPropertyType<PTYPE>()]
                            << " for property '" << name << "'";
                        throw std::runtime_error(err.str());
                        } break;
                }
            }
        }

        // No property
        std::stringstream err;
        err << "Cannot find vector property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    template <>
    std::vector<float> PropertyMap::getPropertyFromVector(std::string name, std::size_t index) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {

            // Get property
            auto p = it->second;
            if (p->getStructure() == PropertyStructure::Vector) {
                switch (p->getType()) {

                    case PropertyType::Undefined: {
                        std::stringstream err;
                        err << "Property '" << name << "' is undefined";
                        throw std::runtime_error(err.str());
                        } break;

                    case PropertyType::FloatVector: {
                        auto &pr = static_cast<PropertyVectorUnbuffered<std::vector<std::vector<float> > > *>(p)->getRef();
                        if (index < pr.size()) {
                            return pr[index];
                        } else {
                            throw std::runtime_error("Index out of range for float vector");
                        }
                        } break;

                    default: {
                        std::stringstream err;
                        err << "Cannot convert property '" << name << "'";
                        throw std::runtime_error(err.str());
                        } break;
                }
            }
        }

        // No property
        std::stringstream err;
        err << "Cannot find vector property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    template <>
    std::vector<double> PropertyMap::getPropertyFromVector(std::string name, std::size_t index) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {

            // Get property
            auto p = it->second;
            if (p->getStructure() == PropertyStructure::Vector) {
                switch (p->getType()) {

                    case PropertyType::Undefined: {
                        std::stringstream err;
                        err << "Property '" << name << "' is undefined";
                        throw std::runtime_error(err.str());
                        } break;

                    case PropertyType::DoubleVector: {
                        auto &pr = static_cast<PropertyVectorUnbuffered<std::vector<std::vector<double> > > *>(p)->getRef();
                        if (index < pr.size()) {
                            return pr[index];
                        } else {
                            throw std::runtime_error("Index out of range for float vector");
                        }
                        } break;

                    default: {
                        std::stringstream err;
                        err << "Cannot convert property '" << name << "'";
                        throw std::runtime_error(err.str());
                        } break;
                }
            }
        }

        // No property
        std::stringstream err;
        err << "Cannot find vector property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    /**
    * Get property reference.
    * @return reference.
    */
    template <>
    std::string &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefScalar<std::string>(name);
    }

    template <>
    int32_t &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefScalar<int32_t>(name);
    }

    template <>
    float &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefScalar<float>(name);
    }

    template <>
    double &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefScalar<double>(name);
    }

    template <>
    cl_uint &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefScalar<cl_uint>(name);
    }

    template <>
    std::vector<std::string> &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefVectorUnbuffered<std::vector<std::string> >(name);
    }

    template <>
    std::vector<int32_t> &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefVectorBuffered<std::vector<int32_t> >(name);
    }

    template <>
    std::vector<float> &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefVectorBuffered<std::vector<float> >(name);
    }

    template <>
    std::vector<double> &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefVectorUnbuffered<std::vector<double> >(name);
    }

    template <>
    std::vector<cl_uint> &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefVectorBuffered<std::vector<cl_uint> >(name);
    }

    template <>
    std::vector<std::vector<float> > &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefVectorUnbuffered<std::vector<std::vector<float> > >(name);
    }

    template <>
    std::vector<std::vector<double> > &PropertyMap::getPropertyRef(std::string name) {
        return getPropertyRefVectorUnbuffered<std::vector<std::vector<double> > >(name);
    }

    template <typename PTYPE>
    PTYPE &PropertyMap::getPropertyRefScalar(std::string name) {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {

            // Get property
            auto p = it->second;
            if (PropertyBase::getPropertyType<PTYPE>() == p->getType()) {

                // Return reference
                return static_cast<Property<PTYPE> *>(p)->getRef();

            } else if (p->getType() == PropertyType::Undefined) {

                // Create property
                auto *pp = new Property<PTYPE>();
                properties[name] = pp;

                // Return reference
                return pp->getRef();

            } else {

                // Type is different
                std::stringstream err;
                err << "Requested reference type '" << PropertyTypeNames[PropertyBase::getPropertyType<PTYPE>()] 
                    << "' differs from property '" << name << "' type '" << PropertyTypeNames[p->getType()]  << "'";
                throw std::runtime_error(err.str());
            }
        }

        // No property
        std::stringstream err;
        err << "Cannot find property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    template <typename PTYPE>
    PTYPE &PropertyMap::getPropertyRefVectorUnbuffered(std::string name) {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {

            // Get property
            auto p = it->second;
            if (PropertyBase::getPropertyType<PTYPE>() == p->getType()) {

                // Return reference
                return static_cast<PropertyVector<PTYPE> *>(p)->getRef();

            } else if (p->getType() == PropertyType::Undefined) {

                auto *pp = new PropertyVectorUnbuffered<PTYPE>();
                properties[name] = pp;

                // Return reference
                return pp->getRef();

            } else {

                // Type is different
                std::stringstream err;
                err << "Requested reference type '" << PropertyTypeNames[PropertyBase::getPropertyType<PTYPE>()] 
                    << "' differs from property '" << name << "' type '" << PropertyTypeNames[p->getType()]  << "'";
                throw std::runtime_error(err.str());
            }
        }

        // No property
        std::stringstream err;
        err << "Cannot find property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    template <typename PTYPE>
    PTYPE &PropertyMap::getPropertyRefVectorBuffered(std::string name) {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {

            // Get property
            auto p = it->second;
            if (PropertyBase::getPropertyType<PTYPE>() == p->getType()) {

                // Return reference
                return static_cast<PropertyVector<PTYPE> *>(p)->getRef();

            } else if (p->getType() == PropertyType::Undefined) {

                auto *pp = new PropertyVectorBuffered<PTYPE>();
                properties[name] = pp;

                // Return reference
                return pp->getRef();

            } else {

                // Type is different
                std::stringstream err;
                err << "Requested reference type '" << PropertyTypeNames[PropertyBase::getPropertyType<PTYPE>()] 
                    << "' differs from property '" << name << "' type";
                throw std::runtime_error(err.str());
            }
        }

        // No property
        std::stringstream err;
        err << "Cannot find property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    /**
    * Get property buffer.
    * @return OpenCL buffer.
    */
    cl::Buffer const &PropertyMap::getPropertyBuffer(std::string name) {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {

            // Get property buffer
            auto p = it->second;
            return p->getBuffer();
        }

        // No property
        std::stringstream err;
        err << "Cannot find property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    /**
    * Get %PropertyType in %PropertyMap.
    * @return %PropertyType
    */
    PropertyType::Type PropertyMap::getPropertyType(std::string name) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {
            return it->second->getType();
        }

        std::stringstream err;
        err << "Cannot find property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    /**
    * Get OpenCL type string of property.
    * @return OpenCL type string
    */
    std::string PropertyMap::getOpenCLTypeString(std::string name) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {
            return it->second->getOpenCLTypeString();
        }

        std::stringstream err;
        err << "Cannot find property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    /**
    * Get %PropertyStructure in %PropertyMap.
    * @return %PropertyStructure
    */
    PropertyStructure::Type PropertyMap::getPropertyStructure(std::string name) const {

        // Look up value
        auto it = properties.find(name);
        if (it != properties.end()) {
            return it->second->getStructure();
        }

        std::stringstream err;
        err << "Cannot find property '" << name << "'";
        throw std::runtime_error(err.str());
    }

    /**
    * Remove property.
    */
    void PropertyMap::removeProperty(std::string name) {

        // Remove existing property data
        auto it = properties.find(name);
        if (it != properties.end()) {
            delete it->second;
            it->second = nullptr;
        }

        // Remove map item
        properties.erase(name);
    }

    /**
    * Convert vector property.
    */
    template <typename PTYPE>
    void PropertyMap::convertProperty(std::string name) {

        // Remove existing property data
        auto it = properties.find(name);
        if (it != properties.end()) {

            // Get property
            auto p = it->second;

            if (p->getType() == PropertyType::Undefined) {
                std::stringstream err;
                err << "Property '" << name << "' is undefined";
                throw std::runtime_error(err.str());
            }

            if (p->getStructure() != PropertyStructure::Vector) {
                std::stringstream err;
                err << "Property '" << name << "' must be vector type for conversion";
                throw std::runtime_error(err.str());
            }

            // Check for same type
            if (p->getType() == PropertyBase::getPropertyType<PTYPE>())
                return;

            // Create new property vector
            PTYPE pn;

            // Covert properties
            switch (p->getType()) {

                case PropertyType::String: {
                    auto &pr = static_cast<PropertyVectorUnbuffered<std::vector<std::string> > *>(p)->getRef();
                    for (auto &p : pr) {
                        pn.push_back(PropertyBase::convert<std::string, typename PTYPE::value_type>(p));
                    }
                    } break;

                case PropertyType::Integer: {
                    auto &pr = static_cast<PropertyVectorBuffered<std::vector<int32_t> > *>(p)->getRef();
                    for (auto &p : pr) {
                        pn.push_back(PropertyBase::convert<int32_t, typename PTYPE::value_type>(p));
                    }
                    } break;

                case PropertyType::Float: {
                    auto &pr = static_cast<PropertyVectorBuffered<std::vector<float> > *>(p)->getRef();
                    for (auto &p : pr) {
                        pn.push_back(PropertyBase::convert<float, typename PTYPE::value_type>(p));
                    }
                    } break;

                case PropertyType::Double: {
                    #if defined(REAL_DOUBLE)
                    auto &pr = static_cast<PropertyVectorBuffered<std::vector<double> > *>(p)->getRef();
                    #else
                    auto &pr = static_cast<PropertyVectorUnbuffered<std::vector<double> > *>(p)->getRef();
                    #endif
                    for (auto &p : pr) {
                        pn.push_back(PropertyBase::convert<double, typename PTYPE::value_type>(p));
                    }
                    } break;

                case PropertyType::Index: {
                    auto &pr = static_cast<PropertyVectorBuffered<std::vector<cl_uint> > *>(p)->getRef();
                    for (auto &p : pr) {
                        pn.push_back(PropertyBase::convert<cl_uint, typename PTYPE::value_type>(p));
                    }
                    } break;

                default: {
                    std::stringstream err;
                    err << "Cannot get vector property type " << PropertyTypeNames[PropertyBase::getPropertyType<PTYPE>()]
                        << " for property '" << name << "'";
                    throw std::runtime_error(err.str());
                    } break;
            }
                
            // Update property
            setProperty<PTYPE>(name, pn);

        } else {

            std::stringstream err;
            err << "Cannot find property '" << name << "'";
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Clear properties.
    */
    void PropertyMap::clear() {

        // Remove all property data
        for (auto &it : properties) {
            delete it.second;
            it.second = nullptr;
        }

        // Clear map
        properties.clear();
    }

    /**
    * Resize all property vectors
    */
    void PropertyMap::resize(std::size_t size) {

        // Find and resize all vectors
        for (auto &it : properties) {

            // Get property
            auto p = it.second;
            if (p->getStructure() == PropertyStructure::Vector) {
                switch (p->getType()) {

                    case PropertyType::String:
                        static_cast<PropertyVector<std::vector<std::string> > *>(p)->getRef().resize(size, getNullValue<std::string>());
                        break;

                    case PropertyType::Integer:
                        static_cast<PropertyVector<std::vector<int32_t> > *>(p)->getRef().resize(size, getNullValue<int32_t>());
                        break;

                    case PropertyType::Float:
                        static_cast<PropertyVector<std::vector<float> > *>(p)->getRef().resize(size, getNullValue<float>());
                        break;

                    case PropertyType::Double:
                        static_cast<PropertyVector<std::vector<double> > *>(p)->getRef().resize(size, getNullValue<double>());
                        break;

                    case PropertyType::Index:
                        static_cast<PropertyVector<std::vector<cl_uint> > *>(p)->getRef().resize(size, getNullValue<cl_uint>());
                        break;

                    case PropertyType::FloatVector:
                        static_cast<PropertyVector<std::vector<std::vector<float> > > *>(p)->getRef().resize(size, std::vector<float>());
                        break;

                    case PropertyType::DoubleVector:
                        static_cast<PropertyVector<std::vector<std::vector<double> > > *>(p)->getRef().resize(size, std::vector<double>());
                        break;
                }
            }
        }
    }

    /**
    * Get set of property names.
    * @return set of property names.
    */
    std::set<std::string> PropertyMap::getPropertyNames() const {
        std::set<std::string> ps;
        for (auto &it : properties) {
            ps.insert(it.first);
        }
        return ps;
    }

    /**
    * Get set of vector property names.
    * @return set of vector property names.
    */
    std::set<std::string> PropertyMap::getPropertyVectorNames() const {
        std::set<std::string> ps;
        for (auto &it : properties) {
            if (it.second->getStructure() == PropertyStructure::Vector) {
                ps.insert(it.first);
            }
        }
        return ps;
    }
    
    /**
    * Write all properties to Json.
    * @return Json.
    */
    Json PropertyMap::toJson() {

        // Create json object
        Json json = Json::object();
        Json::object jsonObj = json.object_items();

        for (auto &it : properties) {

            // Get property
            auto p = it.second;
            if (p->getStructure() == PropertyStructure::Scalar) {

                switch (p->getType()) {
                
                    case PropertyType::String: {
                        auto &d = static_cast<Property<std::string> *>(p)->getRef();
                        jsonObj[it.first] = isValid<std::string>(d) ? d : Json();
                        } break;

                    case PropertyType::Integer: {
                        auto &d = static_cast<Property<int32_t> *>(p)->getRef();
                        jsonObj[it.first] = isValid<int32_t>(d) ? d : Json();
                        } break;

                    case PropertyType::Float: {
                        auto &d = static_cast<Property<float> *>(p)->getRef();
                        jsonObj[it.first] = isValid<float>(d) ? d : Json();
                        } break;

                    case PropertyType::Double: {
                        auto &d = static_cast<Property<double> *>(p)->getRef();
                        jsonObj[it.first] = isValid<double>(d) ? d : Json();
                        } break;

                    case PropertyType::Index: {
                        auto &d = static_cast<Property<cl_uint> *>(p)->getRef();
                        jsonObj[it.first] = isValid<double>((double)d) ? (double)d : Json();
                        } break;

                    case PropertyType::FloatVector: {
                        auto &data = static_cast<Property<std::vector<float> > *>(p)->getRef();
                        auto arr = Json::array();
                        for (auto &d : data) {
                            arr.push_back(isValid<float>(d) ? d : Json());
                        }
                        jsonObj[it.first] = arr;
                        } break;

                    case PropertyType::DoubleVector: {
                        auto &data = static_cast<Property<std::vector<double> > *>(p)->getRef();
                        auto arr = Json::array();
                        for (auto &d : data) {
                            arr.push_back(isValid<double>(d) ? d : Json());
                        }
                        jsonObj[it.first] = arr;
                        } break;

                    case PropertyType::Map: {
                        auto &data = static_cast<Property<PropertyMap> *>(p)->getRef();
                        jsonObj[it.first] = data.toJson();
                        } break;
                }

            } else if (p->getStructure() == PropertyStructure::Vector) {
                switch (p->getType()) {

                    case PropertyType::String: {
                        auto &data = static_cast<PropertyVector<std::vector<std::string> > *>(p)->getRef();
                        auto arr = Json::array();
                        for (auto &d : data) {
                            arr.push_back(isValid<std::string>(d) ? d : Json());
                        }
                        jsonObj[it.first] = arr;
                        } break;

                    case PropertyType::Integer: {
                        auto &data = static_cast<PropertyVector<std::vector<int32_t> > *>(p)->getRef();
                        auto arr = Json::array();
                        for (auto &d : data) {
                            arr.push_back(isValid<int32_t>(d) ? d : Json());
                        }
                        jsonObj[it.first] = arr;
                        } break;

                    case PropertyType::Float: {
                        auto &data = static_cast<PropertyVector<std::vector<float> > *>(p)->getRef();
                        auto arr = Json::array();
                        for (auto &d : data) {
                            arr.push_back(isValid<float>(d) ? d : Json());
                        }
                        jsonObj[it.first] = arr;
                        } break;

                    case PropertyType::Double: {
                        auto &data = static_cast<PropertyVector<std::vector<double> > *>(p)->getRef();
                        auto arr = Json::array();
                        for (auto &d : data) {
                            arr.push_back(isValid<double>(d) ? d : Json());
                        }
                        jsonObj[it.first] = arr;
                        } break;

                    case PropertyType::Index: {
                        auto &data = static_cast<PropertyVector<std::vector<cl_uint> > *>(p)->getRef();
                        auto arr = Json::array();
                        for (auto &d : data) {
                            arr.push_back(isValid<double>((double)d) ? (double)d : Json());
                        }
                        jsonObj[it.first] = arr;
                        } break;

                    case PropertyType::FloatVector: {
                        auto &data = static_cast<PropertyVector<std::vector<std::vector<float> > > *>(p)->getRef();
                        auto arr_out = Json::array();
                        for (auto &d_in : data) {
                            auto arr = Json::array();
                            for (auto d : d_in) {
                                arr.push_back(isValid<float>(d) ? d : Json());
                            }
                            arr_out.push_back(arr);
                        }
                        jsonObj[it.first] = arr_out;
                        } break;

                    case PropertyType::DoubleVector: {
                        auto &data = static_cast<PropertyVector<std::vector<std::vector<double> > > *>(p)->getRef();
                        auto arr_out = Json::array();
                        for (auto &d_in : data) {
                            auto arr = Json::array();
                            for (auto d : d_in) {
                                arr.push_back(isValid<double>(d) ? d : Json());
                            }
                            arr_out.push_back(arr);
                        }
                        jsonObj[it.first] = arr_out;
                        } break;
                }
            }
        }

        // Remap object to json
        json = jsonObj;
        return json;
    }

    /**
    * Write all properties to Json.
    * @return Json string.
    */
    std::string PropertyMap::toJsonString() {
        return toJson().dump();
    }

    // Template definitions
    template Property<std::string>::Property(std::string);
    template Property<int32_t>::Property(int32_t);
    template Property<double>::Property(double);
    template Property<cl_uint>::Property(cl_uint);
    template Property<PropertyMap>::Property(PropertyMap);
    template PropertyVectorUnbuffered<std::vector<std::string> >::PropertyVectorUnbuffered(std::vector<std::string>);
    template PropertyVectorBuffered<std::vector<int32_t> >::PropertyVectorBuffered(std::vector<int32_t>);
    template PropertyVectorBuffered<std::vector<float> >::PropertyVectorBuffered(std::vector<float>);
    #if defined(REAL_DOUBLE)
    template PropertyVectorBuffered<std::vector<double> >::PropertyVectorBuffered(std::vector<double>);
    #else
    template PropertyVectorUnbuffered<std::vector<double> >::PropertyVectorUnbuffered(std::vector<double>);
    #endif
    template PropertyVectorBuffered<std::vector<cl_uint> >::PropertyVectorBuffered(std::vector<cl_uint>);
    template PropertyVectorUnbuffered<std::vector<std::vector<float> > >::PropertyVectorUnbuffered(std::vector<std::vector<float> >);
    template PropertyVectorUnbuffered<std::vector<std::vector<double> > >::PropertyVectorUnbuffered(std::vector<std::vector<double> >);

    template Property<std::string>::Property(const Property<std::string> &);
    template Property<int32_t>::Property(const Property<int32_t> &);
    template Property<float>::Property(const Property<float> &);
    template Property<double>::Property(const Property<double> &);
    template Property<cl_uint>::Property(const Property<cl_uint> &);
    template Property<PropertyMap>::Property(const Property<PropertyMap> &);
    template PropertyVectorUnbuffered<std::vector<std::string> >::PropertyVectorUnbuffered(const PropertyVectorUnbuffered<std::vector<std::string> > &);
    template PropertyVectorBuffered<std::vector<int32_t> >::PropertyVectorBuffered(const PropertyVectorBuffered<std::vector<int32_t> > &);
    template PropertyVectorBuffered<std::vector<float> >::PropertyVectorBuffered(const PropertyVectorBuffered<std::vector<float> > &);
    #if defined(REAL_DOUBLE)
    template PropertyVectorBuffered<std::vector<double> >::PropertyVectorBuffered(const PropertyVectorBuffered<std::vector<double> > &);
    #else
    template PropertyVectorUnbuffered<std::vector<double> >::PropertyVectorUnbuffered(const PropertyVectorUnbuffered<std::vector<double> > &);
    #endif
    template PropertyVectorBuffered<std::vector<cl_uint> >::PropertyVectorBuffered(const PropertyVectorBuffered<std::vector<cl_uint> > &);
    template PropertyVectorUnbuffered<std::vector<std::vector<float> > >::PropertyVectorUnbuffered(const PropertyVectorUnbuffered<std::vector<std::vector<float> > > &);
    template PropertyVectorUnbuffered<std::vector<std::vector<double> > >::PropertyVectorUnbuffered(const PropertyVectorUnbuffered<std::vector<std::vector<double> > > &);

    template Property<std::string> &Property<std::string>::operator=(const Property<std::string> &);
    template Property<int32_t> &Property<int32_t>::operator=(const Property<int32_t> &);
    template Property<float> &Property<float>::operator=(const Property<float> &);
    template Property<double> &Property<double>::operator=(const Property<double> &);
    template Property<cl_uint> &Property<cl_uint>::operator=(const Property<cl_uint> &);
    template Property<PropertyMap> &Property<PropertyMap>::operator=(const Property<PropertyMap> &);
    template PropertyVectorUnbuffered<std::vector<std::string> > &PropertyVectorUnbuffered<std::vector<std::string> >::operator=(const PropertyVectorUnbuffered<std::vector<std::string> > &);
    template PropertyVectorBuffered<std::vector<int32_t> > &PropertyVectorBuffered<std::vector<int32_t> >::operator=(const PropertyVectorBuffered<std::vector<int32_t> > &);
    template PropertyVectorBuffered<std::vector<float> > &PropertyVectorBuffered<std::vector<float> >::operator=(const PropertyVectorBuffered<std::vector<float> > &);
    #if defined(REAL_DOUBLE)
    template PropertyVectorBuffered<std::vector<double> > &PropertyVectorBuffered<std::vector<double> >::operator=(const PropertyVectorBuffered<std::vector<double> > &);
    #else
    template PropertyVectorUnbuffered<std::vector<double> > &PropertyVectorUnbuffered<std::vector<double> >::operator=(const PropertyVectorUnbuffered<std::vector<double> > &);
    #endif
    template PropertyVectorBuffered<std::vector<cl_uint> > &PropertyVectorBuffered<std::vector<cl_uint> >::operator=(const PropertyVectorBuffered<std::vector<cl_uint> > &);
    template PropertyVectorUnbuffered<std::vector<std::vector<float> > > &PropertyVectorUnbuffered<std::vector<std::vector<float> > >::operator=(const PropertyVectorUnbuffered<std::vector<std::vector<float> > > &);
    template PropertyVectorUnbuffered<std::vector<std::vector<double> > > &PropertyVectorUnbuffered<std::vector<std::vector<double> > >::operator=(const PropertyVectorUnbuffered<std::vector<std::vector<double> > > &);

    template std::string Property<std::string>::get() const;
    template std::string Property<int32_t>::get() const;
    template std::string Property<float>::get() const;
    template std::string Property<double>::get() const;
    template std::string Property<cl_uint>::get() const;
    template int32_t Property<std::string>::get() const;
    template int32_t Property<int32_t>::get() const;
    template int32_t Property<float>::get() const;
    template int32_t Property<double>::get() const;
    template int32_t Property<cl_uint>::get() const;
    template float Property<std::string>::get() const;
    template float Property<int32_t>::get() const;
    template float Property<float>::get() const;
    template float Property<double>::get() const;
    template float Property<cl_uint>::get() const;
    template double Property<std::string>::get() const;
    template double Property<int32_t>::get() const;
    template double Property<float>::get() const;
    template double Property<double>::get() const;
    template double Property<cl_uint>::get() const;
    template cl_uint Property<std::string>::get() const;
    template cl_uint Property<int32_t>::get() const;
    template cl_uint Property<float>::get() const;
    template cl_uint Property<double>::get() const;
    template cl_uint Property<cl_uint>::get() const;

    template void Property<std::string>::set(const std::string);
    template void Property<int32_t>::set(const std::string);
    template void Property<float>::set(const std::string);
    template void Property<double>::set(const std::string);
    template void Property<cl_uint>::set(const std::string);
    template void Property<std::string>::set(const int32_t);
    template void Property<int32_t>::set(const int32_t);
    template void Property<float>::set(const int32_t);
    template void Property<double>::set(const int32_t);
    template void Property<cl_uint>::set(const int32_t);
    template void Property<std::string>::set(const float);
    template void Property<int32_t>::set(const float);
    template void Property<float>::set(const float);
    template void Property<double>::set(const float);
    template void Property<cl_uint>::set(const float);
    template void Property<std::string>::set(const double);
    template void Property<int32_t>::set(const double);
    template void Property<float>::set(const double);
    template void Property<double>::set(const double);
    template void Property<cl_uint>::set(const double);
    template void Property<std::string>::set(const cl_uint);
    template void Property<int32_t>::set(const cl_uint);
    template void Property<float>::set(const cl_uint);
    template void Property<double>::set(const cl_uint);
    template void Property<cl_uint>::set(const cl_uint);

    template PropertyBase *Property<std::string>::clone();
    template PropertyBase *Property<int32_t>::clone();
    template PropertyBase *Property<float>::clone();
    template PropertyBase *Property<double>::clone();
    template PropertyBase *Property<cl_uint>::clone();
    template PropertyBase *Property<PropertyMap>::clone();

    template PropertyBase *PropertyVectorUnbuffered<std::vector<std::string> >::clone();
    template PropertyBase *PropertyVectorBuffered<std::vector<int32_t> >::clone();
    template PropertyBase *PropertyVectorBuffered<std::vector<float> >::clone();
    #if defined(REAL_DOUBLE)
    template PropertyBase *PropertyVectorBuffered<std::vector<double> >::clone();
    #else
    template PropertyBase *PropertyVectorUnbuffered<std::vector<double> >::clone();
    #endif
    template PropertyBase *PropertyVectorBuffered<std::vector<cl_uint> >::clone();
    template PropertyBase *PropertyVectorUnbuffered<std::vector<std::vector<float> > >::clone();

    template std::map<std::string, std::string> PropertyMap::getProperties<std::string>() const;
    template std::map<std::string, int32_t> PropertyMap::getProperties<int32_t>() const;
    template std::map<std::string, float> PropertyMap::getProperties<float>() const;
    template std::map<std::string, double> PropertyMap::getProperties<double>() const;
    template std::map<std::string, cl_uint> PropertyMap::getProperties<cl_uint>() const;

    template std::map<std::string, std::reference_wrapper<std::vector<std::string> > > PropertyMap::getPropertyRefs<std::vector<std::string> >() const;
    template std::map<std::string, std::reference_wrapper<std::vector<int32_t> > > PropertyMap::getPropertyRefs<std::vector<int32_t> >() const;
    template std::map<std::string, std::reference_wrapper<std::vector<float> > > PropertyMap::getPropertyRefs<std::vector<float> >() const;
    template std::map<std::string, std::reference_wrapper<std::vector<double> > > PropertyMap::getPropertyRefs<std::vector<double> >() const;
    template std::map<std::string, std::reference_wrapper<std::vector<cl_uint> > > PropertyMap::getPropertyRefs<std::vector<cl_uint> >() const;

    template std::string PropertyMap::getProperty<std::string>(std::string) const;
    template int32_t PropertyMap::getProperty<int32_t>(std::string) const;
    template float PropertyMap::getProperty<float>(std::string) const;
    template double PropertyMap::getProperty<double>(std::string) const;
    template cl_uint PropertyMap::getProperty<cl_uint>(std::string) const;
    template std::vector<float> PropertyMap::getProperty<std::vector<float> >(std::string) const;
    template std::vector<double> PropertyMap::getProperty<std::vector<double> >(std::string) const;

    template std::string PropertyMap::getPropertyFromVector<std::string>(std::string, std::size_t) const;
    template int32_t PropertyMap::getPropertyFromVector<int32_t>(std::string, std::size_t) const;
    template float PropertyMap::getPropertyFromVector<float>(std::string, std::size_t) const;
    template double PropertyMap::getPropertyFromVector<double>(std::string, std::size_t) const;
    template cl_uint PropertyMap::getPropertyFromVector<cl_uint>(std::string, std::size_t) const;
    template std::vector<float> PropertyMap::getPropertyFromVector<std::vector<float> >(std::string, std::size_t) const;
    template std::vector<double> PropertyMap::getPropertyFromVector<std::vector<double> >(std::string, std::size_t) const;

    template void PropertyMap::convertProperty<std::vector<std::string> >(std::string);
    template void PropertyMap::convertProperty<std::vector<int32_t> >(std::string);
    template void PropertyMap::convertProperty<std::vector<float> >(std::string);
    template void PropertyMap::convertProperty<std::vector<double> >(std::string);
    template void PropertyMap::convertProperty<std::vector<cl_uint> >(std::string);
}
