/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include "gs_geometry.h"
#include "gs_variables.h"

namespace Geostack
{
    /**
    * Default %Variables constructor
    */
    template <typename RTYPE, typename KTYPE>
    Variables<RTYPE, KTYPE>::Variables():
        bufferMapPtr(nullptr),
        bufferOnDevice(false) {
    }

    /**
    * %Variables Destructor
    */
    template <typename RTYPE, typename KTYPE>
    Variables<RTYPE, KTYPE>::~Variables() {
    }
    
    /**
    * Set variable value in map
    * @param name the variable name.
    * @param value the variable value to set.
    */
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::set(KTYPE key, RTYPE value) {

        // Ensure data is on host
        ensureDataOnHost();

        // Set value
        dataMap[key] = value;
    }

    /**
    * Get variable value from map
    * @param name the variable name.
    * @param value the variable value to set.
    */
    template <typename RTYPE, typename KTYPE>
    RTYPE Variables<RTYPE, KTYPE>::get(KTYPE key) {
    
        // Ensure data is on host
        ensureDataOnHost();

        // Get value
        auto it = dataMap.find(key);
        if (it == dataMap.end())
            return getNullValue<RTYPE>();
        return it->second;
    }

    /**
    * Get variable map
    * @param name the variable name.
    * @param value the variable value to set.
    */
    template <typename RTYPE, typename KTYPE>
    const std::map<KTYPE, std::size_t> &Variables<RTYPE, KTYPE>::getIndexes() {    

        // Copy data from map to vector
        refreshData();

        // Return map
        return dataIndexes;
    }

    /**
    * Map OpenCL variable buffer to host, this gives exclusive access to the host
    */    
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::mapBuffer(cl::CommandQueue &queue) {

        if (hasData()) {

            // Map buffer
            if (bufferOnDevice) {
                bufferMapPtr = queue.enqueueMapBuffer(buffer, CL_TRUE, CL_MAP_WRITE, 0, dataVec.size()*sizeof(RTYPE));
            }
            bufferOnDevice = false;
        }
    }

    /**
    * Unmap OpenCL variable buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::unmapBuffer(cl::CommandQueue &queue) {

        if (hasData()) {

            // Unmap buffer
            if (!bufferOnDevice) {
                queue.enqueueUnmapMemObject(buffer, bufferMapPtr);
            }
            bufferOnDevice = true;
        }
    }
    
    /**
    * Copy data from map to vector and create buffer
    */
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::refreshData() {
    
        // Ensure data is on host
        ensureDataOnHost();

        // Check data sizes
        if (dataMap.size() != dataVec.size()) {        

            // Get solver handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();

            // Initialise data
            dataVec.resize(dataMap.size(), getNullValue<RTYPE>());

            // Create buffers
            cl_int err = CL_SUCCESS;
            auto &context = solver.getContext();
            buffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, dataVec.size()*sizeof(RTYPE), dataVec.data(), &err);
            if (err != CL_SUCCESS) {
                throw std::range_error("Cannot allocate memory for variable buffer");
            }

            // Initialise buffer
            bufferOnDevice = true;
            mapBuffer(queue);

            // Create data indexes
            std::size_t i = 0;
            dataIndexes.clear();
            for (auto it : dataMap) {
                dataIndexes[it.first] = i;
                i++;
            }
        }

        // Copy data into vector
        std::size_t i = 0;
        for (auto it : dataMap) {
            dataVec[i] = it.second;
            i++;
        }
    }

    /**
    * Ensure variable data is on the host
    */
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::ensureDataOnHost() {
    
        // Check if data is on device
        if (bufferOnDevice) {

            // Map data from device to host
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapBuffer(queue);

            // Copy data into map
            for (auto it : dataIndexes)
                dataMap[it.first] = dataVec[it.second];
        }
    }

    /**
    * Get %Variables buffer.
    * @return reference to %Variables buffer.
    */
    template <typename RTYPE, typename KTYPE>
    cl::Buffer &Variables<RTYPE, KTYPE>::getBuffer() {
    
        // Check data
        if (hasData()) {

            // Ensure reduction buffer is on device
            if (!bufferOnDevice) {

                // Get solver handles
                Geostack::Solver &solver = Geostack::Solver::getSolver();
                auto &queue = solver.getQueue();

                // Copy data from map to vector
                refreshData();

                // Map to device
                unmapBuffer(queue);
            }
        
            // Return reference to buffer
            return buffer;

        } else {

            // Throw exception if Variable has no data
            throw std::range_error("Variable has no data");
        }
    }

    /**
    * %VariablesVector constructor
    */
    template <typename RTYPE>
    VariablesVector<RTYPE>::VariablesVector():
        bufferOnDevice(false),
        bufferSize(0) {
    }

    /**
    * %VariablesVector copy constructor
    */
    template <typename RTYPE>
    VariablesVector<RTYPE>::VariablesVector(VariablesVector const &r):
        bufferOnDevice(false),
        bufferSize(0) {

        r.ensureBufferOnHost();
        data = r.data;
    }
    
    /**
    * %VariablesVector assignment operator
    */
    template <typename RTYPE>
    VariablesVector<RTYPE> &VariablesVector<RTYPE>::operator=(VariablesVector const &r) {

        if (this != &r) {

            ensureBufferOnHost();
            r.ensureBufferOnHost();
            data = r.data;
        }
        return *this;
    }

    /**
    * Clone VariablesVector data
    * Makes a copy of the %VariablesVector
    */
    template <typename RTYPE>
    VariablesVector<RTYPE> *VariablesVector<RTYPE>::clone() {

        // Create new VariablesVector
        VariablesVector<RTYPE> *r = new VariablesVector<RTYPE>();

        // Copy data
        r->data = data;

        return r;
    }

    /**
    * Get buffer from %VariablesVector.
    * @return %VariablesVector buffer
    */
    template <typename RTYPE>
    const cl::Buffer &VariablesVector<RTYPE>::getBuffer() {
    
        // Check if data has been created
        if (size() == 0)
            throw std::range_error("Array has no data");

        // Ensure data buffer is on device
        if (!bufferOnDevice) {
        
            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();

            // Check data size
            if (size() != bufferSize) {

                // Update buffer
                cl_int err = CL_SUCCESS;
                auto &context = solver.getContext();
                buffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, size()*sizeof(RTYPE), data.data(), &err);
                if (err != CL_SUCCESS) {
                    throw std::range_error("Cannot allocate memory for vertex buffer");
                }

                // Map to host
                bufferOnDevice = true;
                mapBuffer(queue);
            }

            // Unmap buffer
            unmapBuffer(queue);
        }
        
        // Return reference to data buffer
        return buffer;
    }

    /**
    * Map OpenCL data buffer to host, this gives exclusive access to the host
    */    
    template <typename RTYPE>
    void VariablesVector<RTYPE>::mapBuffer(cl::CommandQueue &queue) const {
        if (bufferOnDevice) {
            void *bufferPtr = queue.enqueueMapBuffer(buffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, size()*sizeof(RTYPE));
            if (bufferPtr != static_cast<const void *>(data.data()))
                throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
        }
        bufferOnDevice = false;
    }

    /**
    * Unmap OpenCL data buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE>
    void VariablesVector<RTYPE>::unmapBuffer(cl::CommandQueue &queue) {
        if (!bufferOnDevice) {
            queue.enqueueUnmapMemObject(buffer, data.data());
        }
        bufferOnDevice = true;
    }

    /**
    * Ensure data is on the host
    */
    template <typename RTYPE>
    void VariablesVector<RTYPE>::ensureBufferOnHost() const {

        // Check and move data to host
        if (bufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapBuffer(queue);
        }
    }

    // Variable definitions
    template class Variables<cl_uint, uint32_t>;
    template class Variables<float, std::string>;
    template class Variables<double, std::string>;
    
    // Variable array definitions
    template class VariablesVector<cl_int>;
    template class VariablesVector<cl_uint>;
    template class VariablesVector<float>;
    template class VariablesVector<double>;
    template class VariablesVector<Coordinate<float> >;
    template class VariablesVector<Coordinate<double> >;
}
