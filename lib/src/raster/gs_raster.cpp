/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <limits>
#include <cmath>
#include <cstring>
#include <sstream>
#include <numeric>
#include <regex>
#include <set>
#include <iomanip>
#include <cstdio>

#include "gs_models.h"
#include "gs_solver.h"
#include "gs_raster.h"
#include "gs_vector.h"
#include "gs_string.h"
#include "gs_ascii.h"
#include "gs_flt.h"
#include "gs_geotiff.h"
#include "gs_netcdf.h"
#include "gs_gsr.h"
#include "gs_png.h"

#include "miniz.h"

namespace Geostack
{
    /**
    * 2D dimensions equality check.
    * @param l input dimensions.
    * @param r input dimensions.
    * @return true if dimensions match
    */
    template <typename CTYPE>
    bool equalSpatialMetrics2D(const Dimensions<CTYPE> l, const Dimensions<CTYPE> r) {
    
        return 
            l.nx == r.nx && l.ny == r.ny &&
            l.hx == r.hx && l.hy == r.hy &&
            l.ox == r.ox && l.oy == r.oy;
    }

    /**
    * 3D dimensions equality check.
    * @param l input dimensions.
    * @param r input dimensions.
    * @return true if dimensions match
    */
    template <typename CTYPE>
    bool equalSpatialMetrics(const Dimensions<CTYPE> l, const Dimensions<CTYPE> r) {
    
        return 
            l.nx == r.nx && l.ny == r.ny && l.nz == r.nz &&
            l.hx == r.hx && l.hy == r.hy && l.hz == r.hz &&
            l.ox == r.ox && l.oy == r.oy && l.oz == r.oz;
    }

    /**
    * %Raster dimension structure output stream
    * @param os output stream.
    * @param r %RasterDimensions structure.
    * @return updated ostream.
    */
    template <typename CTYPE>
    std::ostream &operator<<(std::ostream &os, const RasterDimensions<CTYPE> &r) {

        // Write data
        return os
            << sizeof(r) << ", " << 
            r.d.nx << ", " <<
            r.d.ny << ", " << 
            r.d.nz << ", " << 
            r.d.hx << ", " << 
            r.d.hy << ", " << 
            r.d.hz << ", " << 
            r.d.ox << ", " << 
            r.d.oy << ", " << 
            r.d.oz << ", " << 
            r.ex << ", " << 
            r.ey << ", " << 
            r.ez << ", " << 
            r.tx << ", " << 
            r.ty;
    }
    
    /**
    * Singleton instance of KernelCache
    */
    KernelCache& KernelCache::getKernelCache() {

        static KernelCache kernelCache;
        return kernelCache;
    }

    void KernelCache::setKernelGenerator(std::size_t scriptHash, KernelGenerator kernelGenerator) {

        // Check for generator
        generatorMap[scriptHash] = kernelGenerator;
    }

    KernelGenerator KernelCache::getKernelGenerator(std::size_t scriptHash) {

        // Check for generator
        auto it = generatorMap.find(scriptHash);
        if (it != generatorMap.end()) {
            return it->second;
        }
        return KernelGenerator();
    }

#ifdef USE_TILE_CACHING

    /**
    * %TileCacheManager constructor.
    */
    template <typename RTYPE, typename CTYPE>
    TileCacheManager<RTYPE, CTYPE>::TileCacheManager():
        cacheHostAllocationBytes(0), maxHostAllocationBytes(0), 
        cacheDeviceAllocationBytes(0), maxDeviceAllocationBytes(0) {
    
        // Output
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        uint8_t verbose = solver.getVerboseLevel();
        if (verbose <= Geostack::Verbosity::Info) {
            std::cout << "TileCacheManager: Using tile cache" << std::endl;
        }

        // Get temporary directory
        char *cacheDirectoryStr = std::getenv("GEOSTACK_TEMP_DIR");
        if (cacheDirectoryStr != NULL) {
            cacheDirectory = std::string(cacheDirectoryStr);
        } else {
            cacheDirectoryStr = std::getenv("GEOSTACK_TMP_DIR");
            if (cacheDirectoryStr != NULL) {
                cacheDirectory = std::string(cacheDirectoryStr);
            }
        }

        // Get allocation limits
        maxHostAllocationBytes = solver.getHostMemoryLimit();
        maxDeviceAllocationBytes = (uint64_t)(0.75*solver.getDeviceMemoryLimit());
    }

    /**
    * Singleton instance of %TileCacheManager.
    */
    template <typename RTYPE, typename CTYPE>
    TileCacheManager<RTYPE, CTYPE> &TileCacheManager<RTYPE, CTYPE>::instance() {

        static TileCacheManager tileManager;
        return tileManager;
    }
    
    /**
    * TileCacheManager destructor
    */
    template <typename RTYPE, typename CTYPE>
    TileCacheManager<RTYPE, CTYPE>::~TileCacheManager() {
    }
    
    /**
    * Update tile cache entries and write tile data to cache if required
    */
    template <typename RTYPE, typename CTYPE>
    void TileCacheManager<RTYPE, CTYPE>::update(Tile<RTYPE, CTYPE> *tile) {

        auto dim = tile->getDimensions();
        uint64_t tileMemorySize = (dim.mx+1)*dim.my*dim.nz*sizeof(RTYPE);

        if (maxDeviceAllocationBytes > 0) {

            // Check for tile entry
            if (cacheDeviceMap.find(tile) == cacheDeviceMap.end()) {

                // Check cache size
                if (cacheDeviceAllocationBytes > maxDeviceAllocationBytes) {

                    // delete least recently used element
                    Tile<RTYPE, CTYPE> *cacheTile = cacheDeviceList.back();

                    // Clear buffers
                    cacheTile->clearDataBuffer();
 
                    // Remove last list element
                    cacheDeviceList.pop_back();
 
                    // Erase the last
                    cacheDeviceMap.erase(cacheTile);

                } else {

                    // Update cache size
                    cacheDeviceAllocationBytes += tileMemorySize;
                }
 
                // Update cache
                cacheDeviceList.push_front(tile);
                cacheDeviceMap[tile] = cacheDeviceList.begin();

            } else {
 
                // Swap to front
                cacheDeviceList.splice(cacheDeviceList.begin(), cacheDeviceList, cacheDeviceMap[tile]);
            }
        }
        
        if (maxHostAllocationBytes > 0) {

            // Check for tile entry
            if (cacheHostMap.find(tile) == cacheHostMap.end()) {

                // Check cache size
                if (cacheHostAllocationBytes > maxHostAllocationBytes) {

                    // delete least recently used element
                    Tile<RTYPE, CTYPE> *cacheTile = cacheHostList.back();

                    // Clear buffers
                    cacheTile->writeToCache();
 
                    // Remove last list element
                    cacheHostList.pop_back();
 
                    // Erase the last
                    cacheHostMap.erase(cacheTile);

                } else {

                    // Update cache size
                    cacheHostAllocationBytes += tileMemorySize;
                }

                // Update cache
                cacheHostList.push_front(tile);
                cacheHostMap[tile] = cacheHostList.begin();

            } else {
 
                // Swap to front
                cacheHostList.splice(cacheHostList.begin(), cacheHostList, cacheHostMap[tile]);
            }
 
        }
    }

    /**
    * Remove tile cache entries
    */
    template <typename RTYPE, typename CTYPE>
    void TileCacheManager<RTYPE, CTYPE>::remove(Tile<RTYPE, CTYPE> *tile) {
    
        auto dim = tile->getDimensions();
        uint64_t tileMemorySize = (dim.mx+1)*dim.my*dim.nz*sizeof(RTYPE);

        // Remove existing map entries
        auto itd = cacheDeviceMap.find(tile);
        if (itd != cacheDeviceMap.end()) {
            cacheDeviceList.erase(itd->second);
            cacheDeviceMap.erase(itd);
            cacheDeviceAllocationBytes -= tileMemorySize;
        }

        auto ith = cacheHostMap.find(tile);
        if (ith != cacheHostMap.end()) {
            cacheHostList.erase(ith->second);
            cacheHostMap.erase(ith);
            cacheHostAllocationBytes -= tileMemorySize;
        }
    }

#endif

    /**
    * Default %Tile constructor
    * dimensions will be POD initialised to zero
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::Tile():
        dim(), 
        dataBufferMapPtr(nullptr),
        reduceBufferMapPtr(nullptr),
        statusBufferMapPtr(nullptr),
        dataBufferOnDevice(false),
        reduceBufferOnDevice(false),
        statusBufferOnDevice(false),
        dataInitialised(false),
        status(0) { }
     
    /**
    * %Tile Destructor
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::~Tile() {

#ifdef USE_TILE_CACHING

        // Update cache
        TileCacheManager<RTYPE, CTYPE>::instance().remove(this);

#endif

    }

    /**
    * %Tile Comparison operator
    * @param l left-hand Raster to compare.
    * @param r right-hand Raster to compare.
    */
    template <typename RTYPE, typename CTYPE>
    bool operator==(const Tile<RTYPE, CTYPE> &l, const Tile<RTYPE, CTYPE> &r) { 

        // Compare objects
        if (&l == &r)
            return true;

        // Ensure buffers are on host
        l.ensureDataBufferOnHost();
        r.ensureDataBufferOnHost();
         
        // Compare values
        if (l.v != r.v)
            return false;

        return true; 
    }

    /**
    * %Tile Comparison operator.
    * @param l left-hand Raster to compare.
    * @param r right-hand Raster to compare.
    */
    template <typename RTYPE, typename CTYPE>
    bool operator!=(const Tile<RTYPE, CTYPE> &l, const Tile<RTYPE, CTYPE> &r) { 
        return !operator==(l, r);
    }
        
#ifdef USE_TILE_CACHING
    /**
    * %TileCache constructor
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::TileCache::TileCache():
        isCached(false),
        hasCacheFile(false) {
    }

    /**
    * %TileCache destructor
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::TileCache::~TileCache() {
        
        // Delete cache file
        if (hasCacheFile) {
            std::remove(cacheName.c_str());
        }
    }

    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::TileCache::read(Tile<RTYPE, CTYPE> &tile) {
    
        // Check cache
        if (!hasCacheFile) {
            throw std::runtime_error("No cache file");
        }

        // Open file
        std::FILE *pf = std::fopen(cacheName.c_str(), "rb");

        // Check file
        if (!pf) {
            std::stringstream err;
            err << "Cannot open cache file '" << cacheName << "' for writing";
            throw std::runtime_error(err.str());
        }

        // Create compressed vector        
        std::vector<unsigned char> compressedData(tile.dataVec.size()*sizeof(RTYPE));

        // Read data length
        uint32_t dataLength = 0;
        std::fread(reinterpret_cast<char *>(&dataLength), sizeof(uint32_t), 1, pf);

        // Read tile data
        mz_ulong compressedDataLen = (mz_ulong)dataLength;
        std::fread(reinterpret_cast<char *>(compressedData.data()), sizeof(unsigned char), compressedDataLen, pf);
        if (std::feof(pf)) {
            throw std::runtime_error("Unexpected end of file while reading data");
        }

        // Close file
        std::fclose(pf);

        // Uncompress
        mz_ulong len = (mz_ulong)compressedData.size();
        int status = uncompress(reinterpret_cast<unsigned char *>(tile.dataVec.data()), &len, 
            reinterpret_cast<const unsigned char *>(compressedData.data()), compressedDataLen);

        if (status != Z_OK) {
            std::stringstream err;
            err << "Uncompression failed '" << mz_error(status) << "'";
            throw std::runtime_error(err.str());
        } 

        // Check size
        if (len != (mz_ulong)compressedData.size()) {
            throw std::runtime_error("Unexpected data size during uncompression");
        }
        
        // Set cache flag
        isCached = false;
    }

    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::TileCache::write(Tile<RTYPE, CTYPE> &tile) {
    
        // Generate cache name
        if (cacheName.empty()) {
            const auto &cacheDirectory = TileCacheManager<RTYPE, CTYPE>::instance().getCacheDirectory();
            if (cacheDirectory.empty()) {
                cacheName = std::string(std::tmpnam(nullptr))+".tile";
            } else {
                auto fileName = std::string(std::tmpnam(nullptr));
                auto split = fileName.find_last_of("/\\");
                cacheName = cacheDirectory+fileName.substr(split)+".tile";
            }
        }

        // Open file
        std::FILE *pf = std::fopen(cacheName.c_str(), "wb");

        // Check file
        if (!pf) {
            std::stringstream err;
            err << "Cannot open cache file '" << cacheName << "' for writing";
            throw std::runtime_error(err.str());
        }

        // Set data sizes
        uint32_t dataLength = (uint32_t)tile.dataVec.size()*sizeof(RTYPE);
        mz_ulong compressedDataLen = compressBound(dataLength);

        // Compress
        std::vector<unsigned char> compressedData(compressedDataLen);
        mz_ulong len = compressedDataLen;
        int status = compress2(compressedData.data(), &len, 
            reinterpret_cast<const unsigned char *>(tile.dataVec.data()), (mz_ulong)dataLength, MZ_BEST_SPEED);

        if (status != Z_OK) {
            std::stringstream err;
            err << "Compression failed '" << mz_error(status) << "'";
            throw std::runtime_error(err.str());
        }
        
        // Write data length
        uint32_t writeDataLength = (uint32_t)len;
        std::fwrite(reinterpret_cast<const char *>(&writeDataLength), sizeof(uint32_t), 1, pf);

        // Write tile data
        std::fwrite(reinterpret_cast<const char *>(compressedData.data()), sizeof(unsigned char), len, pf);

        // Close file
        std::fclose(pf);
        
        // Set cache flag
        hasCacheFile = true;
        isCached = true;
    }
#endif

    /**
    * Create data %Tile data.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::createData() {
        
        if (!dataInitialised) {

            // Get handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();

            // Check data size
            size_t dataSize = getDataSize();
            if (dataSize*sizeof(RTYPE) >= solver.getMaxAllocSizeBytes()) {
                std::stringstream err;
                err << "Cannot allocate OpenCL memory for tile of size " 
                    << (dataSize>>20) << " Mb, maximum allowable size is " 
                    << (solver.getMaxAllocSizeBytes()>>20) << " Mb";
                throw std::runtime_error(err.str());
            }
        
            // Allocate data buffer
            dataVec.resize(dataSize);
        
            // Allocate reduction buffer
            reduceVec.resize(dim.d.my*dim.d.nz, getNullValue<RTYPE>());

            // Reset status
            status = 0;

            // Map to host
            dataBufferOnDevice = false;
            reduceBufferOnDevice = false;
            statusBufferOnDevice = false;

            // Set initialisation flag
            dataInitialised = true;
        }
    }

    /**
    * Reset %Tile to null.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::clearData() {
                
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Check and move data to host
        if (dataBufferOnDevice) {

            // Reset device cell values
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            queue.enqueueFillBuffer<RTYPE>(dataBuffer, getNullValue<RTYPE>(), 0, getDataSize()*sizeof(RTYPE));

        } else {
        
            // Reset host cell values
            dataVec.assign(getDataSize(), getNullValue<RTYPE>());
        }
    }

    /**
    * Read %Tile data from handler.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::readData(Raster<RTYPE, CTYPE> &r, dataHandlerReadFunction<RTYPE, CTYPE> readDataHandler) {
    
        // Check data handler
        if (readDataHandler) {        

            // Read from file
            readDataHandler(dim, dataVec, r);

            // Check data on return
            if (dataVec.size() != getDataSize()) {
                throw std::runtime_error("Tile data from handler is invalid");
            }
        }
    }

    /**
    * Delete %Tile data.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::deleteData(bool clearCacheFlag) {
    
        // Map to host
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        mapDataBuffer(queue);
        mapReduceBuffer(queue);
        mapStatusBuffer(queue);

        // Clear host data
        std::vector<RTYPE>().swap(dataVec);
        std::vector<RTYPE>().swap(reduceVec);
        status = 0;

        // Clear buffers
        dataBuffer = cl::Buffer();
        reduceBuffer = cl::Buffer();
        statusBuffer = cl::Buffer();

        // Clear pointers
        dataBufferMapPtr = nullptr;
        reduceBufferMapPtr = nullptr;
        statusBufferMapPtr = nullptr;

        // Set initialisation flag
        dataInitialised = false;
        
#ifdef USE_TILE_CACHING

        // Clear cache flag
        if (clearCacheFlag)
            tileCache.setAsUncached();

#endif
    }

    /**
    * %Tile initialisation.
    * @param nz_ the number of cells in the z dimension.
    */
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::init(
        uint32_t ti_, 
        uint32_t tj_, 
        RasterDimensions<CTYPE> rdim_) {

        // Ensure all data is on host
        if (dataInitialised) {
            ensureDataBufferOnHost();
            ensureReduceBufferOnHost();
            ensureStatusBufferOnHost();
        }

        // Set tile index
        dim.ti = ti_;
        dim.tj = tj_;
        
        // Set raster offset
        dim.d.ox = rdim_.d.ox+(CTYPE)(dim.ti*TileMetrics::tileSize)*rdim_.d.hx;
        dim.d.oy = rdim_.d.oy+(CTYPE)(dim.tj*TileMetrics::tileSize)*rdim_.d.hy; 
        dim.d.oz = rdim_.d.oz;
        
        // Set size
        dim.d.nx = std::min(rdim_.d.nx-TileMetrics::tileSize*dim.ti, TileMetrics::tileSize);
        dim.d.ny = std::min(rdim_.d.ny-TileMetrics::tileSize*dim.tj, TileMetrics::tileSize);
        dim.d.nz = rdim_.d.nz;

        // Set memory size
        dim.d.mx = TileMetrics::tileSize;
        dim.d.my = TileMetrics::tileSize;

        // Set spacing
        dim.d.hx = rdim_.d.hx;
        dim.d.hy = rdim_.d.hy;
        dim.d.hz = rdim_.d.hz;

        // Calculate end points
        dim.ex = dim.d.ox+(CTYPE)dim.d.nx*dim.d.hx;
        dim.ey = dim.d.oy+(CTYPE)dim.d.ny*dim.d.hy;
        dim.ez = dim.d.oz+(CTYPE)dim.d.nz*dim.d.hz;

        // Set buffer flags
        dataBufferOnDevice = false;
        reduceBufferOnDevice = false;
        statusBufferOnDevice = false;

        return true;
    }

    /**
    * Get %Tile OpenCL data buffer.
    * @return Reference %Tile data buffer.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Tile<RTYPE, CTYPE>::getDataBuffer() {

        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Ensure data buffer is on device
        if (!dataBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            unmapDataBuffer(queue);
        }
        
        // Return reference to data buffer
        return dataBuffer;
    }

    /**
    * Get %Tile OpenCL reduction buffer.
    * @return Reference %Tile reduction buffer.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Tile<RTYPE, CTYPE>::getReduceBuffer() {
    
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Ensure reduction buffer is on device
        if (!reduceBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            unmapReduceBuffer(queue);
        }
        
        // Return reference to reduce buffer
        return reduceBuffer;
    }

    /**
    * Get %Tile OpenCL status buffer.
    * @return Reference %Tile status buffer.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Tile<RTYPE, CTYPE>::getStatusBuffer() {
    
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Ensure reduction buffer is on device
        if (!statusBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            unmapStatusBuffer(queue);
        }
        
        // Return reference to status buffer
        return statusBuffer;
    }

    /**
    * Ensure tile data is on the host
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::ensureDataBufferOnHost() {
    
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Check and move data to host
        if (dataBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapDataBuffer(queue);
        }
    }

    /**
    * Ensure reduction data is on the host
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::ensureReduceBufferOnHost() {
    
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Check and move data to host
        if (reduceBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapReduceBuffer(queue);
        }
    }

    /**
    * Ensure status data is on the host
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::ensureStatusBufferOnHost() {
    
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Check and move data to host
        if (statusBufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapStatusBuffer(queue);
        }
    }

    /**
    * Handle to %Tile cell.
    * @param i x cell index.
    * @param j y cell index.
    * @param k z cell index.
    * @return Reference to cell data.
    */
    template <typename RTYPE, typename CTYPE>
    inline RTYPE &Tile<RTYPE, CTYPE>::operator()(uint32_t i, uint32_t j, uint32_t k) { 

        // Ensure data is available
        ensureDataBufferOnHost();

        // Return reference to value
        return dataVec[i+((j+(k<<TileMetrics::tileSizePower))<<TileMetrics::tileSizePower)];
    }

    /**
    * Check tile for data.
    * @return true is %Tile contains data.
    */
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::hasData() {
    
        // Check vector
        return !dataVec.empty();
    }

    /**
    * Copy data from %Tile.
    * @param data vector to copy into.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::getData(tIterator iTo, size_t start, size_t end) {

        // Ensure data is available
        ensureDataBufferOnHost();

        // Copy data
        std::copy(dataVec.begin()+start, dataVec.begin()+end, iTo);
    }
    
    /**
    * Copy data to %Tile.
    * @param data vector to copy into.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setData(tIterator iFromStart, tIterator iFromEnd, size_t start) {
    
        // Ensure data is available
        ensureDataBufferOnHost();

        // Copy data
        std::copy(iFromStart, iFromEnd, dataVec.begin()+start);
    }

    /**
    * Set %Tile index.
    * @param ti %Tile x index.
    * @param tj %Tile y index.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setIndex(uint32_t ti_, uint32_t tj_) {

        // Update indexes
        dim.ti = ti_;
        dim.tj = tj_;
    }
    
    /**
    * Set value in all %Tile cells.
    * Sets entire raster to specified value
    * @param v Value to set.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setAllCellValues(RTYPE val) {
                    
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Check and move data to host
        if (dataBufferOnDevice) {

            // Set device cell values
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            queue.enqueueFillBuffer<RTYPE>(dataBuffer, val, 0, getDataSize()*sizeof(RTYPE));

        } else {
        
            // Set host cell values
            tIterator iStart = dataVec.begin();
            tIterator iEnd = dataVec.begin()+dim.d.nx;
            for (uint32_t k = 0; k < dim.d.mx*dim.d.my*dim.d.nz; k+=dim.d.mx*dim.d.my)
                for (uint32_t j = 0; j < dim.d.mx*dim.d.ny; j+=dim.d.mx)
                    std::fill(iStart+j+k, iEnd+j+k, val); 
        }
    }

    /**
    * Maximum operator.
    * @return maximum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Tile<RTYPE, CTYPE>::max() {
    
        // Return null for non-initialised tiles
        if (!dataInitialised)
            return getNullValue<RTYPE>();

        // Ensure data is available
        ensureDataBufferOnHost();

        // Run through tile
        RTYPE max = std::numeric_limits<RTYPE>::lowest();
        for (auto val : dataVec)
            max = Geostack::max<RTYPE>(val, max);
            
        // Return null if limits are not found    
        return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
    }

    /**
    * Minimum operator.
    * @return minimum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Tile<RTYPE, CTYPE>::min() {

        // Return null for non-initialised tiles
        if (!dataInitialised)
            return getNullValue<RTYPE>();

        // Ensure data is available
        ensureDataBufferOnHost();

        // Run through tile
        RTYPE min = std::numeric_limits<RTYPE>::max();
        for (auto val : dataVec)
            min = Geostack::min<RTYPE>(val, min);
            
        // Return null if limits are not found    
        return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;    
    }

    /**
    * Reduction operator.
    * @return reduction value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Tile<RTYPE, CTYPE>::reduce(Reduction::Type type) {

        // Reduce values
        switch (type) {

            case(Reduction::Maximum): {

                // Ensure data is available
                ensureReduceBufferOnHost();
                
                // Get data
                RTYPE max = std::numeric_limits<RTYPE>::lowest();
                for (auto v : reduceVec)
                    max = Geostack::max<RTYPE>(v, max);
            
                // Return null if limits are not found    
                return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
            }
            
            case(Reduction::Minimum): {

                // Ensure data is available
                ensureReduceBufferOnHost();
                
                // Get data
                RTYPE min = std::numeric_limits<RTYPE>::max();
                for (auto v : reduceVec)
                    min = Geostack::min<RTYPE>(v, min);
            
                // Return null if limits are not found    
                return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;    
            }
            
            case(Reduction::Sum):
            case(Reduction::SumSquares): {

                // Ensure data is available
                ensureReduceBufferOnHost();

                // Get data
                RTYPE sum = getNullValue<RTYPE>();
                for (auto v : reduceVec) {

                    // Check for no-data
                    if (isValid<RTYPE>(v)) {
                        sum = isValid<RTYPE>(sum) ? sum + v : v;
                    }
                }
                return sum;    
            }
            
            case(Reduction::Count): {

                // Ensure data is available
                ensureStatusBufferOnHost();
                
                // Get data
                return (RTYPE)status;    
            }
        }

        // Return null value
        return getNullValue<RTYPE>();
    }
    
    /**
    * Clear reduction buffer
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::resetReduction() {
                
        // Check if data has been created
        if (!dataInitialised)
            throw std::range_error("Tile has no data");

        // Check and move data to host
        if (reduceBufferOnDevice) {

            // Reset device cell values
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            queue.enqueueFillBuffer<RTYPE>(reduceBuffer, getNullValue<RTYPE>(), 0, dim.d.my*dim.d.nz*sizeof(RTYPE));

        } else {
        
            // Reset host cell values
            reduceVec.assign(dim.d.my*dim.d.nz, getNullValue<RTYPE>());
        }
    }    

    /**
    * Get status.
    * @return status bits.
    */  
    template <typename RTYPE, typename CTYPE>
    cl_uint Tile<RTYPE, CTYPE>::getStatus() {

        // Ensure data is available
        ensureStatusBufferOnHost();

        // Return status
        return status;
    }

    /**
    * Set status.
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setStatus(cl_uint newStatus) {

        // Ensure data is available
        ensureStatusBufferOnHost();

        // Set status
        status = newStatus;
    }

    /**
    * Get status and reset.
    * @param newStatus new value for status.
    * @return status bits.
    */  
    template <typename RTYPE, typename CTYPE>
    cl_uint Tile<RTYPE, CTYPE>::getResetStatus(cl_uint newStatus) {

        // Ensure data is available
        ensureStatusBufferOnHost();

        // Return status
        cl_uint lastStatus = status;
        status = newStatus;
        return lastStatus;
    }

#ifdef USE_TILE_CACHING

    /**
    * Check whether %Tile data is cached
    * @return true if %Tile is cached, false otherwise
    */  
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::isCached()  {
    
        // Return cache status
        return tileCache.getIsCached();
    }

    /**
    * Write %Tile data to cache
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::writeToCache() {

        // Check for cached data
        if (tileCache.getIsCached()) {
            std::cout << "WARNING: Tile " << dim.ti << ", " << dim.tj << " is already cached" << std::endl;
            return;
        }

        // Ensure data is on host
        ensureDataBufferOnHost();

        // Write data to cache
        tileCache.write(*this);

        // Clear data
        deleteData();
    }

    /**
    * Clear data buffer
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::clearDataBuffer() {

        if (dataInitialised) {

            // Ensure data is on host
            ensureDataBufferOnHost();

            // Clear buffer
            dataBuffer = cl::Buffer();

            // Clear pointers
            dataBufferMapPtr = nullptr;
        }
    }

    /**
    * Read %Tile data from cache
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::readFromCache() {

        // Check for cached data
        if (!tileCache.getIsCached()) {
            std::cout << "WARNING: Tile " << dim.ti << ", " << dim.tj << " is not cached" << std::endl;
            return;
        }

        // Write data to cache
        tileCache.read(*this);
    }

#endif

    /**
    * Get bounds of tile.
    * @return %Tile %BoundingBox
    */
    template <typename RTYPE, typename CTYPE>
    BoundingBox<CTYPE> Tile<RTYPE, CTYPE>::getBounds() const { 
        return { { dim.d.ox, dim.d.oy }, { dim.ex, dim.ey} }; 
    }
    
    /**
    * Map OpenCL data buffer to host, this gives exclusive access to the host
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::mapDataBuffer(cl::CommandQueue &queue) const {
        if (dataBufferOnDevice) {
            dataBufferMapPtr = queue.enqueueMapBuffer(dataBuffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, getDataSize()*sizeof(RTYPE));
        }
        dataBufferOnDevice = false;
    }

    /**
    * Unmap OpenCL data buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::unmapDataBuffer(cl::CommandQueue &queue) {
        if (!dataBufferOnDevice) {

            // Create buffer if needed
            if (dataBufferMapPtr == nullptr) {

                // Create buffer
                auto &solver = Geostack::Solver::getSolver();
                auto &context = solver.getContext();
                cl_int err = CL_SUCCESS;
                dataBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, getDataSize()*sizeof(RTYPE), dataVec.data(), &err);
                if (err != CL_SUCCESS) {
                    throw std::range_error("Cannot allocate memory for tile buffer");
                }

                // Map to populate pointer
                dataBufferOnDevice = true;
                mapDataBuffer(queue);
            }

            // Unmap buffer
            queue.enqueueUnmapMemObject(dataBuffer, dataBufferMapPtr);
        }
        dataBufferOnDevice = true;
    }
    
    /**
    * Map OpenCL reduction buffer to host, this gives exclusive access to the host
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::mapReduceBuffer(cl::CommandQueue &queue) {

        // request mapping to host
        if (reduceBufferOnDevice) {
            reduceBufferMapPtr = queue.enqueueMapBuffer(reduceBuffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, dim.d.my*dim.d.nz*sizeof(RTYPE));
        }
        reduceBufferOnDevice = false;
    }

    /**
    * Unmap OpenCL reduction buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::unmapReduceBuffer(cl::CommandQueue &queue) {

        // Map to device
        if (!reduceBufferOnDevice) {

            // Create buffer if needed
            if (reduceBufferMapPtr == nullptr) {

                // Create buffer
                auto &solver = Geostack::Solver::getSolver();
                auto &context = solver.getContext();
                cl_int err = CL_SUCCESS;
                reduceBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, dim.d.my*dim.d.nz*sizeof(RTYPE), reduceVec.data(), &err);
                if (err != CL_SUCCESS) {
                    throw std::range_error("Cannot allocate memory for reduction buffer");
                }

                // Map to populate pointer
                reduceBufferOnDevice = true;
                mapReduceBuffer(queue);
            }

            // Unmap buffer
            queue.enqueueUnmapMemObject(reduceBuffer, reduceBufferMapPtr);
        }
        reduceBufferOnDevice = true;
    }
    
    /**
    * Map OpenCL status buffer to host, this gives exclusive access to the host
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::mapStatusBuffer(cl::CommandQueue &queue) const {
        if (statusBufferOnDevice) {
            statusBufferMapPtr = queue.enqueueMapBuffer(statusBuffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(cl_uint));
        }
        statusBufferOnDevice = false;
    }

    /**
    * Unmap OpenCL status buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::unmapStatusBuffer(cl::CommandQueue &queue) {
        if (!statusBufferOnDevice) {

            // Create buffer if needed
            if (statusBufferMapPtr == nullptr) {

                // Create buffer
                auto &solver = Geostack::Solver::getSolver();
                auto &context = solver.getContext();
                cl_int err = CL_SUCCESS;
                statusBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), &status, &err);
                if (err != CL_SUCCESS) {
                    throw std::range_error("Cannot allocate memory for status buffer");
                }

                // Map to populate pointer
                statusBufferOnDevice = true;
                mapStatusBuffer(queue);
            }

            // Unmap buffer
            queue.enqueueUnmapMemObject(statusBuffer, statusBufferMapPtr);
        }
        statusBufferOnDevice = true;
    }

    /**
    * Map %Vector distances into %Tile
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param widthPropertyName name of feature width property
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::mapVector(Vector<CTYPE> &v, std::size_t clHash, ProjectionParameters<double> rproj, 
        size_t geometryTypes, std::string widthPropertyName, std::string levelPropertyName) {

        // Check geometry types
        if (!(geometryTypes & (GeometryType::Point | GeometryType::LineString | GeometryType::Polygon))) {
            throw std::runtime_error("No requested geometry types to map");
        }

        try { 
            
            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();

            // Get projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (rproj != vproj);

            // Get bounds and expand to cover all time
            BoundingBox<CTYPE> b({ 
                { dim.d.ox, dim.d.oy, 0.0, std::numeric_limits<CTYPE>::lowest() }, 
                { dim.ex, dim.ey, 0.0, std::numeric_limits<CTYPE>::max() } } );

            // Reproject bounds
            if (usingProjection) {
                b = b.convert(vproj, rproj);
            }

            // Get geometry nearest to tile
            auto vTile = v.nearest(b, geometryTypes);

            // Map points
            if (geometryTypes & GeometryType::Point) {            
                
                // Get points
                const auto &pointIndexes = vTile.getPointIndexes();
                    
                // Check size
                if (pointIndexes.size() > 0) {

                    // Point data
                    CoordinateList<CTYPE> coords; // Coordinate vector
                    std::vector<CTYPE> width;     // Geometry width vector
                    std::vector<uint32_t> level;  // Geometry level vector
                
                    // Build coordinate list
                    for (auto &p : pointIndexes) {
                        coords.push_back(vTile.getPointCoordinate(p));
                    }

                    // Get geometry width
                    if (widthPropertyName.length() == 0) {

                        // Set width to zero
                        width.assign(coords.size(), 0.0);

                    } else {

                        // Get width
                        for (auto &p : pointIndexes) {
                            width.push_back((CTYPE)vTile.template getProperty<RTYPE>(p, widthPropertyName));
                        }
                    }
                    
                    // Get geometry level
                    if (levelPropertyName.length() == 0 || !vTile.hasProperty(levelPropertyName)) {

                        // Set level to zero
                        level.assign(coords.size(), 0);

                    } else {

                        // Get level
                        for (auto &p : pointIndexes) {
                            level.push_back(vTile.template getProperty<uint32_t>(p, levelPropertyName));
                        }
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<CTYPE> widthBuffer(context, queue, width);
                    AutoBuffer<uint32_t> levelBuffer(context, queue, level);

                    // Populate points kernel arguments
                    auto map2DPointDistanceKernel = solver.getKernel(clHash, "map2DPointDistance");
                    map2DPointDistanceKernel.setArg(0, getDataBuffer());
                    map2DPointDistanceKernel.setArg(1, dim.d);
                    map2DPointDistanceKernel.setArg(2, coordsBuffer.getBuffer());
                    map2DPointDistanceKernel.setArg(3, widthBuffer.getBuffer()); 
                    map2DPointDistanceKernel.setArg(4, levelBuffer.getBuffer());
                    map2DPointDistanceKernel.setArg(5, (cl_uint)coords.size()); 
                    if (usingProjection) {
                        map2DPointDistanceKernel.setArg(6, ProjectionParameters<CTYPE>(rproj));
                        map2DPointDistanceKernel.setArg(7, ProjectionParameters<CTYPE>(vproj));
                    }

                    // Execute points kernel
                    queue.enqueueNDRangeKernel(map2DPointDistanceKernel, 
                        cl::NullRange, 
                        cl::NDRange(TileMetrics::tileSize, dim.d.ny, dim.d.nz), 
                        cl::NDRange(TileMetrics::tileSize, 1, 1));
                }
            }
                
            // Map line strings
            if (geometryTypes & GeometryType::LineString) {
                
                // Get line strings
                const auto &lineStringIndexes = vTile.getLineStringIndexes();
                    
                // Check size
                if (lineStringIndexes.size() > 0) {
            
                    // Line string data
                    CoordinateList<CTYPE> coords; // Coordinate vector
                    std::vector<uint32_t> offset; // Offset vector
                    std::vector<CTYPE> width;     // Geometry width vector
                    std::vector<uint32_t> level;  // Geometry level vector
                    
                    // Build coordinate list
                    for (auto &l : lineStringIndexes) {

                        // Get line string coordinates
                        auto lc = vTile.getLineStringCoordinates(l);

                        // Append coordinates
                        coords.insert(coords.end(), lc.begin(), lc.end());

                        // Update offset list
                        offset.push_back((uint32_t)coords.size()-1);
                    }
                    
                    // Get geometry width
                    if (widthPropertyName.length() == 0) {

                        // Set width to zero
                        width.assign(offset.size(), 0.0);

                    } else {

                        // Get width
                        for (auto &l : lineStringIndexes) {
                            width.push_back((CTYPE)vTile.template getProperty<RTYPE>(l, widthPropertyName));
                        }
                    }
                    
                    // Get geometry level
                    if (levelPropertyName.length() == 0 || !vTile.hasProperty(levelPropertyName)) {

                        // Set level to zero
                        level.assign(offset.size(), 0);

                    } else {

                        // Get level
                        for (auto &l : lineStringIndexes) {
                            level.push_back(vTile.template getProperty<uint32_t>(l, levelPropertyName));
                        }
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<uint32_t> offsetBuffer(context, queue, offset);
                    AutoBuffer<CTYPE> widthBuffer(context, queue, width);
                    AutoBuffer<uint32_t> levelBuffer(context, queue, level);
                
                    // Populate line string kernel arguments
                    auto map2DLineStringDistanceKernel = solver.getKernel(clHash, "map2DLineStringDistance");
                    map2DLineStringDistanceKernel.setArg(0, getDataBuffer());
                    map2DLineStringDistanceKernel.setArg(1, dim.d);
                    map2DLineStringDistanceKernel.setArg(2, coordsBuffer.getBuffer());
                    map2DLineStringDistanceKernel.setArg(3, widthBuffer.getBuffer());
                    map2DLineStringDistanceKernel.setArg(4, offsetBuffer.getBuffer());
                    map2DLineStringDistanceKernel.setArg(5, levelBuffer.getBuffer());
                    map2DLineStringDistanceKernel.setArg(6, (cl_uint)offset.size());
                    if (usingProjection) {
                        map2DLineStringDistanceKernel.setArg(7, ProjectionParameters<CTYPE>(rproj));
                        map2DLineStringDistanceKernel.setArg(8, ProjectionParameters<CTYPE>(vproj));
                    }

                    // Execute line string kernel
                    queue.enqueueNDRangeKernel(map2DLineStringDistanceKernel, 
                        cl::NullRange, 
                        cl::NDRange(TileMetrics::tileSize, dim.d.ny, dim.d.nz), 
                        cl::NDRange(TileMetrics::tileSize, 1, 1));
                }
            }

            // Map polygons
            if (geometryTypes & GeometryType::Polygon) {            

                // Get polygon indexes
                const auto &polygonIndexes = vTile.getPolygonIndexes();
                    
                // Check size
                if (polygonIndexes.size() > 0) {
                
                    // Polygon data
                    CoordinateList<CTYPE> coords; // Coordinate vector
                    std::vector<uint32_t> offset; // Offset vector
                    CoordinateList<CTYPE> bounds; // Bounding box vector
                    std::vector<uint32_t> level;  // Geometry level vector

                    // Build coordinate list
                    for (auto &p : polygonIndexes) {

                        // Get polygon coordinates
                        auto pc = vTile.getPolygonCoordinates(p);

                        // Get polygon sub-indexes
                        uint32_t currentOffset = (uint32_t)coords.size();
                        for (const auto &subIndex : vTile.getPolygonSubIndexes(p)) {
                            currentOffset += (uint32_t)subIndex;
                            offset.push_back(currentOffset-1);
                        }

                        // Append coordinates
                        coords.insert(coords.end(), pc.begin(), pc.end());

                        // Append bounds
                        for (const auto &subBounds : vTile.getPolygonSubBounds(p)) {
                            bounds.push_back(subBounds.min);
                            bounds.push_back(subBounds.max);
                        }
                    }
                    
                    // Get geometry level
                    if (levelPropertyName.length() == 0 || !vTile.hasProperty(levelPropertyName)) {

                        // Set level to zero
                        level.assign(offset.size(), 0);

                    } else {

                        // Get level
                        for (auto &p : polygonIndexes) {
                            uint32_t l = vTile.template getProperty<uint32_t>(p, levelPropertyName);
                            for (const auto &subIndex : vTile.getPolygonSubIndexes(p)) {
                                level.push_back(l);
                            }
                        }
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<uint32_t> offsetBuffer(context, queue, offset);
                    AutoBuffer<Coordinate<CTYPE> > boundsBuffer(context, queue, bounds);
                    AutoBuffer<uint32_t> levelBuffer(context, queue, level);

                    // Populate line string kernel arguments
                    auto map2DPolygonDistanceKernel = solver.getKernel(clHash, "map2DPolygonDistance");
                    map2DPolygonDistanceKernel.setArg(0, getDataBuffer());
                    map2DPolygonDistanceKernel.setArg(1, dim.d);
                    map2DPolygonDistanceKernel.setArg(2, coordsBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(3, offsetBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(4, boundsBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(5, levelBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(6, (cl_uint)offset.size());
                    if (usingProjection) {
                        map2DPolygonDistanceKernel.setArg(7, ProjectionParameters<CTYPE>(rproj));
                        map2DPolygonDistanceKernel.setArg(8, ProjectionParameters<CTYPE>(vproj));
                    }

                    // Execute line string kernel
                    queue.enqueueNDRangeKernel(map2DPolygonDistanceKernel, 
                        cl::NullRange,
                        cl::NDRange(TileMetrics::tileSize, dim.d.ny, dim.d.nz), 
                        cl::NDRange(TileMetrics::tileSize, 1, 1));

                }
            }

            // Flush queue
            queue.flush();

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Rasterise %Vector onto %Tile
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param script script to run for rasterisation
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::rasterise(Vector<CTYPE> &v, std::size_t clHash, 
        std::vector<std::string> fields, ProjectionParameters<double> rproj, size_t geometryTypes) {

        try { 
            
            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            size_t clWorkgroupSize, clPaddedWorkgroupSize;

            // Get projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (rproj != vproj);

            // Get bounds and expand to cover all time
            BoundingBox<CTYPE> b({ 
                { dim.d.ox, dim.d.oy, 0.0, std::numeric_limits<CTYPE>::lowest() }, 
                { dim.ex, dim.ey, 0.0, std::numeric_limits<CTYPE>::max() } } );

            // Reproject bounds
            if (usingProjection) {
                b = b.convert(vproj, rproj);
            }
            
            // Get geometry within tile
            Vector<CTYPE> vTile = v.region(b, geometryTypes);

            // Create fields
            std::vector<RTYPE> field;  // Geometry field vector

            // Rasterise points
            if (geometryTypes & GeometryType::Point) {
            
                // Point data
                CoordinateList<CTYPE> coords; // Coordinate vector
                
                // Get points
                const auto &pointIndexes = vTile.getPointIndexes();
                    
                // Check size
                if (pointIndexes.size() > 0) {

                    // Build coordinate list
                    for (auto &p : pointIndexes) {
                        coords.push_back(vTile.getPointCoordinate(p));
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);

                    // Get kernel
                    auto rasterise2DPointKernel = solver.getKernel(clHash, "rasterise2DPoint"); 

                    // Populate points kernel arguments
                    cl_uint arg = 0;
                    rasterise2DPointKernel.setArg(arg++, getDataBuffer());
                    rasterise2DPointKernel.setArg(arg++, getDimensions());
                    rasterise2DPointKernel.setArg(arg++, coordsBuffer.getBuffer());
                    rasterise2DPointKernel.setArg(arg++, (cl_uint)coords.size());     
                    if (usingProjection) {
                        rasterise2DPointKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                        rasterise2DPointKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                    }
                
                    // Get fields
                    std::vector<AutoBuffer<RTYPE> *> autoBuffers;
                    for (auto &fieldName : fields) {
                        field.clear();

                        // Populate field array
                        for (auto &p : pointIndexes) {
                            field.push_back((RTYPE)vTile.template getProperty<RTYPE>(p, fieldName)); // TODO UPDATE COPY POINT
                        }

                        // Create buffers
                        autoBuffers.push_back(new AutoBuffer<RTYPE>(context, queue, field));

                        // Add to arguments
                        rasterise2DPointKernel.setArg(arg++, autoBuffers.back()->getBuffer());         
                    }
                    
                    // Execute point kernel
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(rasterise2DPointKernel);
                    clPaddedWorkgroupSize = (size_t)(coords.size()+((clWorkgroupSize-(coords.size()%clWorkgroupSize))%clWorkgroupSize));
                    queue.enqueueNDRangeKernel(rasterise2DPointKernel, cl::NullRange, 
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                    // Clear buffers
                    for (auto autoBuffer : autoBuffers)
                        delete autoBuffer;
                }
            }
                
            // Rasterise line strings
            if (geometryTypes & GeometryType::LineString) {
            
                // Line string data
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<uint32_t> offset; // Offset vector
                
                // Get line strings
                const auto &lineStringIndexes = vTile.getLineStringIndexes();
                    
                // Check size
                if (lineStringIndexes.size() > 0) {
                    
                    // Build coordinate list
                    for (auto &l : lineStringIndexes) {

                        // Get line string coordinates
                        auto lc = vTile.getLineStringCoordinates(l);

                        // Append coordinates
                        coords.insert(coords.end(), lc.begin(), lc.end());

                        // Update offset list
                        offset.push_back((uint32_t)coords.size()-1);
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<uint32_t> offsetBuffer(context, queue, offset);

                    // Get kernel
                    auto rasterise2DLineStringKernel = solver.getKernel(clHash, "rasterise2DLineString"); 
                
                    // Populate line string kernel arguments
                    cl_uint arg = 0;
                    rasterise2DLineStringKernel.setArg(arg++, getDataBuffer());
                    rasterise2DLineStringKernel.setArg(arg++, getDimensions());
                    rasterise2DLineStringKernel.setArg(arg++, coordsBuffer.getBuffer());
                    rasterise2DLineStringKernel.setArg(arg++, offsetBuffer.getBuffer());
                    rasterise2DLineStringKernel.setArg(arg++, (cl_uint)offset.size());
                    if (usingProjection) {
                        rasterise2DLineStringKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                        rasterise2DLineStringKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                    }
                    
                    // Get fields
                    std::vector<AutoBuffer<RTYPE> *> autoBuffers;
                    for (auto &fieldName : fields) {
                        field.clear();

                        // Populate field array
                        for (auto &l : lineStringIndexes) {
                            field.push_back((RTYPE)vTile.template getProperty<RTYPE>(l, fieldName));
                        }

                        // Create buffers
                        autoBuffers.push_back(new AutoBuffer<RTYPE>(context, queue, field));

                        // Add to arguments
                        rasterise2DLineStringKernel.setArg(arg++, autoBuffers.back()->getBuffer());         
                    }

                    // Execute line string kernel
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(rasterise2DLineStringKernel);
                    clPaddedWorkgroupSize = (size_t)(offset.size()+((clWorkgroupSize-(offset.size()%clWorkgroupSize))%clWorkgroupSize));
                    queue.enqueueNDRangeKernel(rasterise2DLineStringKernel, cl::NullRange, 
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                    // Clear buffers
                    for (auto autoBuffer : autoBuffers)
                        delete autoBuffer;
                }
            }
            
            // Rasterise polygons
            if (geometryTypes & GeometryType::Polygon) {
            
                // Polygon data
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<int32_t> offset;  // Offset vector
                CoordinateList<CTYPE> bounds; // Bounding box vector

                // Get polygon indexes
                const auto &polygonIndexes = vTile.getPolygonIndexes();
                    
                // Check size
                if (polygonIndexes.size() > 0) {

                    // Build geometry list
                    for (auto &p : polygonIndexes) {
                    
                        // Get polygon coordinates
                        auto pc = vTile.getPolygonCoordinates(p);

                        // Append coordinates
                        coords.insert(coords.end(), pc.begin(), pc.end());

                        // Get polygon sub-indexes
                        auto &si = vTile.getPolygonSubIndexes(p);

                        // Add sub-indexes, sub-polygons are marked as negative
                        offset.push_back((int32_t)si[0]);
                        for (cl_uint s = 1; s < si.size(); s++) {
                            offset.push_back(-(int32_t)si[s]);
                        }

                        // Append bounds
                        for (const auto &subBounds : vTile.getPolygonSubBounds(p)) {
                            bounds.push_back(subBounds.min);
                            bounds.push_back(subBounds.max);
                        }
                    }

                    // Add terminator to offset
                    offset.push_back(0);

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<int32_t> offsetBuffer(context, queue, offset);
                    AutoBuffer<Coordinate<CTYPE> > boundsBuffer(context, queue, bounds);

                    // Get kernel
                    auto rasterise2DPolygonKernel = solver.getKernel(clHash, "rasterise2DPolygon"); 

                    // Populate line string kernel arguments
                    cl_uint arg = 0;
                    rasterise2DPolygonKernel.setArg(arg++, getDataBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, getDimensions());
                    rasterise2DPolygonKernel.setArg(arg++, coordsBuffer.getBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, offsetBuffer.getBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, boundsBuffer.getBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, (cl_uint)(offset.size()-1));
                    if (usingProjection) {
                        rasterise2DPolygonKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                        rasterise2DPolygonKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                    }

                    // Get fields
                    std::vector<AutoBuffer<RTYPE> *> autoBuffers;
                    for (auto &fieldName : fields) {
                        field.clear();

                        // Populate field array
                        for (auto &p : polygonIndexes) {
                            field.push_back((RTYPE)vTile.template getProperty<RTYPE>(p, fieldName));
                        }

                        // Create buffers
                        autoBuffers.push_back(new AutoBuffer<RTYPE>(context, queue, field));

                        // Add to arguments
                        rasterise2DPolygonKernel.setArg(arg++, autoBuffers.back()->getBuffer());         
                    }

                    // Execute polygon kernel
                    queue.enqueueNDRangeKernel(rasterise2DPolygonKernel, cl::NullRange, 
                        TileMetrics::rangeTile2DGlobal, TileMetrics::rangeTile2DLocal);

                    // Clear buffers
                    for (auto autoBuffer : autoBuffers)
                        delete autoBuffer;
                }
            }

            // Flush queue
            queue.flush();

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }
    
    /**
    * Index %Vector geometry and weighting from %Tile
    * @param v the %Vector to map.
    * @param rasterIndexes reference to vector of %RasterIndex
    * @param clHash hash of OpenCL program
    * @param rproj %Tile projection
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::index(Vector<CTYPE> &v, std::vector<RasterIndex<CTYPE> > &rasterIndexes,
        std::size_t clHash, ProjectionParameters<double> rproj) {

        try { 

            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            size_t clWorkgroupSize, clPaddedWorkgroupSize;

            // Get projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (rproj != vproj);

            // Get bounds and expand to cover all time
            BoundingBox<CTYPE> b({ 
                { dim.d.ox, dim.d.oy, 0.0, std::numeric_limits<CTYPE>::lowest() }, 
                { dim.ex, dim.ey, 0.0, std::numeric_limits<CTYPE>::max() } } );

            // Reproject bounds
            if (usingProjection) {
                b = b.convert(vproj, rproj);
            }
            
            // Get geometry within tile
            Vector<CTYPE> vTile = v.region(b);

            // Create raster indexes
            std::size_t rasterIndexTotalSize = rasterIndexes.size();

            // Get points
            const auto &pointIndexes = vTile.getPointIndexes();
            if (pointIndexes.size() > 0) {

                // Build coordinate list
                CoordinateList<CTYPE> coords; // Coordinate vector
                for (auto &p : pointIndexes) {
                    coords.push_back(vTile.getPointCoordinate(p));
                }

                // Update index vector
                std::size_t rasterIndexSize = pointIndexes.size();
                rasterIndexes.resize(rasterIndexes.size()+rasterIndexSize);
                auto pRasterIndex = rasterIndexes.data()+rasterIndexTotalSize;
                rasterIndexTotalSize += rasterIndexSize;
                
                // Create raster index buffer
                auto rasterIndexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                    rasterIndexSize*sizeof(RasterIndex<CTYPE>), pRasterIndex);

                void *rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, rasterIndexSize*sizeof(RasterIndex<CTYPE>));
                if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

                queue.enqueueUnmapMemObject(rasterIndexBuffer, rasterIndexBufferPtr);

                // Create buffers
                AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                AutoBuffer<cl_uint> geometryIndexBuffer(context, queue, pointIndexes);

                // Get kernel
                auto indexPointsKernel = solver.getKernel(clHash, "indexPoints"); 

                // Populate points kernel arguments
                cl_uint arg = 0;
                indexPointsKernel.setArg(arg++, getDataBuffer());
                indexPointsKernel.setArg(arg++, getDimensions());
                indexPointsKernel.setArg(arg++, coordsBuffer.getBuffer());
                indexPointsKernel.setArg(arg++, geometryIndexBuffer.getBuffer());
                indexPointsKernel.setArg(arg++, rasterIndexBuffer);
                indexPointsKernel.setArg(arg++, (cl_uint)pointIndexes.size());
                if (usingProjection) {
                    indexPointsKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                    indexPointsKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                }
                    
                // Execute point kernel
                clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(indexPointsKernel);
                clPaddedWorkgroupSize = (size_t)(pointIndexes.size()+((clWorkgroupSize-(pointIndexes.size()%clWorkgroupSize))%clWorkgroupSize));
                queue.enqueueNDRangeKernel(indexPointsKernel, cl::NullRange, 
                    cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                // Map index data
                rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, pointIndexes.size()*sizeof(RasterIndex<CTYPE>));
                if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
            }

            // Get line strings
            const auto &lineStringIndexes = vTile.getLineStringIndexes();
            if (lineStringIndexes.size() > 0) {
            
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<uint32_t> offset; // Offset vector

                // Build coordinate list
                CTYPE length = 0.0;
                for (auto &l : lineStringIndexes) {

                    // Get line string coordinates
                    auto lc = vTile.getLineStringCoordinates(l);

                    // Calculate length
                    for (std::size_t i = 0; i < lc.size()-1; i++) {
                        length += std::hypot(
                            1.0+fabs((lc[i].p-lc[i+1].p)/dim.d.hx), 1.0+fabs((lc[i].q-lc[i+1].q)/dim.d.hy));
                    }
                    length += std::hypot(
                        1.0+fabs((lc[lc.size()-1].p-lc[0].p)/dim.d.hx), 1.0+fabs((lc[lc.size()-1].q-lc[0].q)/dim.d.hy));

                    // Append coordinates
                    coords.insert(coords.end(), lc.begin(), lc.end());

                    // Update offset list
                    offset.push_back((uint32_t)coords.size()-1);
                }
                
                // Index cells
                uint32_t cellCount = (uint32_t)length;
                if (cellCount > 0) {

                    // Create array index buffer
                    cl_uint globalIndex = 0;
                    cl::Buffer globalIndexBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), &globalIndex);
                    void *globalIndexBufferPtr = queue.enqueueMapBuffer(globalIndexBuffer, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
                    if (globalIndexBufferPtr != static_cast<void *>(&globalIndex))
                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                    queue.enqueueUnmapMemObject(globalIndexBuffer, globalIndexBufferPtr);

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<cl_uint> offsetBuffer(context, queue, offset);
                    AutoBuffer<cl_uint> geometryIndexBuffer(context, queue, lineStringIndexes);
                    
                    // Update index vector
                    rasterIndexes.resize(rasterIndexTotalSize+cellCount);
                    auto pRasterIndex = rasterIndexes.data()+rasterIndexTotalSize;
                
                    // Create raster index buffer
                    auto rasterIndexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                        cellCount*sizeof(RasterIndex<CTYPE>), pRasterIndex);
                    void *rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, cellCount*sizeof(RasterIndex<CTYPE>));
                    if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                    queue.enqueueUnmapMemObject(rasterIndexBuffer, rasterIndexBufferPtr);     
                
                    // Get line string index kernel
                    auto indexLineStringKernel = solver.getKernel(clHash, "indexLineStrings");

                    cl_uint arg = 0;
                    indexLineStringKernel.setArg(arg++, getDataBuffer());
                    indexLineStringKernel.setArg(arg++, getDimensions());
                    indexLineStringKernel.setArg(arg++, coordsBuffer.getBuffer());
                    indexLineStringKernel.setArg(arg++, offsetBuffer.getBuffer());
                    indexLineStringKernel.setArg(arg++, geometryIndexBuffer.getBuffer());
                    indexLineStringKernel.setArg(arg++, rasterIndexBuffer);
                    indexLineStringKernel.setArg(arg++, globalIndexBuffer);
                    indexLineStringKernel.setArg(arg++, (cl_uint)offset.size());  
                    if (usingProjection) {
                        indexLineStringKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                        indexLineStringKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                    }                
                
                    // Get optimal workgroup size
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(indexLineStringKernel);
                    clPaddedWorkgroupSize = (size_t)(offset.size()+((clWorkgroupSize-(offset.size()%clWorkgroupSize))%clWorkgroupSize));

                    // Execute line string index kernel
                    queue.enqueueNDRangeKernel(indexLineStringKernel, cl::NullRange, 
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));     

                    // Map index data
                    rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, cellCount*sizeof(RasterIndex<CTYPE>));
                    if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");         
                    
                    // Get linestring cell count
                    globalIndexBufferPtr = queue.enqueueMapBuffer(globalIndexBuffer, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
                    if (globalIndexBufferPtr != static_cast<void *>(&globalIndex))
                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

                    // Adjust vector
                    rasterIndexes.resize(rasterIndexTotalSize+globalIndex);
                    rasterIndexTotalSize += globalIndex;
                }
            }

            // Get polygon indexes
            const auto &polygonIndexes = vTile.getPolygonIndexes();
            if (polygonIndexes.size() > 0) {
            
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<int32_t> offset;  // Offset vector
                CoordinateList<CTYPE> bounds; // Bounding box vector
                uint32_t cellCount = 0;       // Number of cells for current geometry

                // Build geometry list
                CTYPE area = 0.0;
                for (auto &p : polygonIndexes) {
                    
                    // Get polygon coordinates
                    auto pc = vTile.getPolygonCoordinates(p);

                    // Append coordinates
                    coords.insert(coords.end(), pc.begin(), pc.end());

                    // Get polygon sub-indexes
                    auto &si = vTile.getPolygonSubIndexes(p);

                    // Add sub-indexes, sub-polygons are marked as negative
                    offset.push_back((int32_t)si[0]);
                    for (cl_uint s = 1; s < si.size(); s++) {
                        offset.push_back(-(int32_t)si[s]);
                    }

                    // Append bounds
                    for (const auto &subBounds : vTile.getPolygonSubBounds(p)) {
                        bounds.push_back(subBounds.min);
                        bounds.push_back(subBounds.max);
                    }

                    // Reproject bounds
                    if (usingProjection) {
                    
                        // Estimate area from bounds
                        auto pb = vTile.getGeometry(p)->getBounds();
                        pb = pb.intersect(b);
                        pb = pb.convert(rproj, vproj);

                        // Add 10% 'safety margin' to area
                        CTYPE pArea = 1.1*pb.area2D()/(dim.d.hx*dim.d.hy);

                        pArea = std::min(pArea, (CTYPE)TileMetrics::tileSizeSquared);
                        area += pArea;

                    } else {

                        // Calculate area
                        CTYPE pArea = 0.0;
                        for (std::size_t i = 0; i < si[0]-1; i++) {
                            pArea += (pc[i].p*pc[i+1].q)-(pc[i+1].p*pc[i].q);
                        }
                        pArea += (pc[si[0]-1].p*pc[0].q)-(pc[0].p*pc[si[0]-1].q);
                        area += fabs(pArea)/(dim.d.hx*dim.d.hy);
                    }
                }
                cellCount = 1+(uint32_t)(area);

                // Add terminator to offset
                offset.push_back(0);

                // Create array index buffer
                cl_uint globalIndex = 0;
                cl::Buffer globalIndexBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), &globalIndex);
                void *globalIndexBufferPtr = queue.enqueueMapBuffer(globalIndexBuffer, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
                if (globalIndexBufferPtr != static_cast<void *>(&globalIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                queue.enqueueUnmapMemObject(globalIndexBuffer, globalIndexBufferPtr);

                // Create buffers
                AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                AutoBuffer<int32_t> offsetBuffer(context, queue, offset);
                AutoBuffer<Coordinate<CTYPE> > boundsBuffer(context, queue, bounds);
                AutoBuffer<cl_uint> geometryIndexBuffer(context, queue, polygonIndexes);                
                
                // Update index vector
                rasterIndexes.resize(rasterIndexTotalSize+cellCount);
                auto pRasterIndex = rasterIndexes.data()+rasterIndexTotalSize;
                
                // Create raster index buffer
                auto rasterIndexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                    cellCount*sizeof(RasterIndex<CTYPE>), pRasterIndex);
                void *rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, cellCount*sizeof(RasterIndex<CTYPE>));
                if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                queue.enqueueUnmapMemObject(rasterIndexBuffer, rasterIndexBufferPtr); 

                // Get polygon index kernel
                auto indexPolygonKernel = solver.getKernel(clHash, "indexPolygons"); 

                // Populate polygon kernel arguments
                cl_uint arg = 0;
                indexPolygonKernel.setArg(arg++, getDataBuffer());
                indexPolygonKernel.setArg(arg++, getDimensions());
                indexPolygonKernel.setArg(arg++, coordsBuffer.getBuffer());
                indexPolygonKernel.setArg(arg++, offsetBuffer.getBuffer());
                indexPolygonKernel.setArg(arg++, boundsBuffer.getBuffer());
                indexPolygonKernel.setArg(arg++, geometryIndexBuffer.getBuffer());
                indexPolygonKernel.setArg(arg++, rasterIndexBuffer);
                indexPolygonKernel.setArg(arg++, globalIndexBuffer);
                indexPolygonKernel.setArg(arg++, (cl_uint)(offset.size()-1));
                if (usingProjection) {
                    indexPolygonKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                    indexPolygonKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                }

                // Execute polygon count kernel
                queue.enqueueNDRangeKernel(indexPolygonKernel, cl::NullRange, 
                    TileMetrics::rangeTile2DGlobal, TileMetrics::rangeTile2DLocal);

                // Map index data
                rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, cellCount*sizeof(RasterIndex<CTYPE>));
                if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                        
                // Get polygon cell count
                globalIndexBufferPtr = queue.enqueueMapBuffer(globalIndexBuffer, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
                if (globalIndexBufferPtr != static_cast<void *>(&globalIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

                // Adjust vector
                rasterIndexes.resize(rasterIndexTotalSize+globalIndex);
                rasterIndexTotalSize += globalIndex;
            }

            // Flush queue
            queue.flush(); 

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }

    /**
    * %RasterFileHandler constructor.
    */
    template <typename RTYPE, typename CTYPE>
    RasterFileHandler<RTYPE, CTYPE>::RasterFileHandler() {

        // Create file stream
        fileStream = std::make_shared<std::fstream>();
    }

    /**
    * RasterFileHandler copy constructor.
    * @param rf %RasterFileHandler to copy.
    */
    template <typename RTYPE, typename CTYPE>
    RasterFileHandler<RTYPE, CTYPE>::RasterFileHandler(const RasterFileHandler<RTYPE, CTYPE> &rf): 
        readDataHandler(rf.readDataHandler), writeDataHandler(rf.writeDataHandler) {

        // Set file stream
        if (fileStream == nullptr)
            fileStream = std::make_shared<std::fstream>();
        else
            fileStream = rf.fileStream;
    }

    /**
    * %RasterFileHandler destructor.
    */
    template <typename RTYPE, typename CTYPE>
    RasterFileHandler<RTYPE, CTYPE>::~RasterFileHandler() {
    }

    /**
    * Register function to read tile data to %Raster.
    * @param dataHandler_ function for data handler.
    */
    template <typename RTYPE, typename CTYPE>
    void RasterFileHandler<RTYPE, CTYPE>::registerReadDataHandler(dataHandlerReadFunction<RTYPE, CTYPE> dataHandler_) {

        // Move function handle
        readDataHandler = std::move(dataHandler_);
    }

    /**
    * Register function to write tile data from %Raster.
    * @param dataHandler_ function for data handler.
    */
    template <typename RTYPE, typename CTYPE>
    void RasterFileHandler<RTYPE, CTYPE>::registerWriteDataHandler(dataHandlerWriteFunction<RTYPE, CTYPE> dataHandler_) {

        // Move function handle
        writeDataHandler = std::move(dataHandler_);
    }

    /**
    * Default %RasterBase constructor
    * dimensions will be POD initialised to zero
    */
    template <typename CTYPE>
    RasterBase<CTYPE>::RasterBase():
        PropertyMap(),
        dim(),
        proj(),
        interpolation(RasterInterpolation::Nearest),
        hashString() {

        // Create variable data
        vars = std::make_shared<Variables<CTYPE, std::string> >();
    }
        
    /**
    * Destructor
    */
    template <typename CTYPE>
    RasterBase<CTYPE>::~RasterBase() {

        // Ensure queue operations are finished to prevent buffer deallocation before kernel execution
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        queue.finish();
    }

    /**
    * Copy constructor
    * Creates shallow copy
    */
    template <typename CTYPE>
    RasterBase<CTYPE>::RasterBase(const RasterBase<CTYPE> &rb):
        PropertyMap(rb),
        vars(rb.vars),
        proj(rb.proj),
        dim(rb.dim),
        interpolation(rb.interpolation),
        hashString(rb.hashString) {

        // Copy data
        tree = rb.tree;
    }

    /**
    * Set %RasterBase %ProjectionParameters.
    */
    template <typename CTYPE>
    void RasterBase<CTYPE>::setProjectionParameters(ProjectionParameters<double> proj_) {

        // Set projection
        proj = proj_;

        // Update hash
        createHashString();
        hashString += "proj [" + proj.str() + "] ";
    }

    /**
    * Return variable data for %RasterBase.
    * @return copy of variable data vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    RTYPE RasterBase<CTYPE>::getVariableData(std::string name) {

        // Check data
        if (vars != nullptr) {
            return vars->get(name);
        } else {
            return getNullValue<RTYPE>();
        }
    }

    /**
    * Set variable data for %RasterBase.
    * @param dataVec_ vector data to set.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    void RasterBase<CTYPE>::setVariableData(std::string name, RTYPE value) {

        // Check data
        if (vars != nullptr) {
            vars->set(name, value);
        }
    }
    
    /**
    * Return variable indexes for %RasterBase.
    * @return index map of variables.
    */
    template <typename CTYPE>
    const std::map<std::string, std::size_t> &RasterBase<CTYPE>::getVariablesIndexes() {

        // Check data
        if (vars != nullptr) {
            return vars->getIndexes();
        } else {
            throw std::runtime_error("RasterBase has no variables");
        }
    }
    
    /**
    * Return variable buffer for %RasterBase.
    * @return reference to variable buffer.
    */
    template <typename CTYPE>
    cl::Buffer &RasterBase<CTYPE>::getVariablesBuffer() {

        // Check data
        if (vars != nullptr) {
            return vars->getBuffer();
        } else {
            throw std::runtime_error("RasterBase has no variables");
        }
    }

    /**
    * Return variable type for %RasterBase.
    * @return string type of variables.
    */
    template <typename CTYPE>
    std::string RasterBase<CTYPE>::getVariablesType() {

        // Check data
        if (vars != nullptr) {
            return vars->getOpenCLTypeString();
        } else {
            throw std::runtime_error("RasterBase has no variables");
        }
    }
    
    /**
    * Return cache buffer of given size for %RasterBase.
    * @param dataSize required data size.
    * @return Buffer of given size.
    */
    template <typename CTYPE>
    cl::Buffer &RasterBase<CTYPE>::getCacheBuffer(std::size_t dataSize) {

        auto it = dataCache.find(dataSize);
        if (it == dataCache.end()) {

            // Create buffer
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            it = dataCache.insert( { dataSize, cl::Buffer(context, CL_MEM_READ_ONLY, dataSize) } ).first;
        }

        // Return reference to buffer
        return (*it).second;
    }

    /**
    * Get hash string.
    * @return hash string for %RasterBase.
    */
    template <typename CTYPE>
    std::string RasterBase<CTYPE>::getHashString() {
        std::string hs;
        hs += "name [" + PropertyMap::template getProperty<std::string>("name") + "] ";
        hs += "nbr [" + std::to_string(getRequiredNeighbours()) + "] ";
        hs += "red [" + std::to_string(getReductionType()) + "] ";
        hs += "stat [" + std::to_string(getNeedsStatus()) + "] ";
        hs += "read [" + std::to_string(getNeedsRead()) + "] ";
        hs += "wrt [" + std::to_string(getNeedsWrite()) + "] ";
        hs += hashString;
        return hs;
    }

    /**
    * Create string hash for RasterBase
    */
    template <typename CTYPE>
    void RasterBase<CTYPE>::createHashString() {

        // Create hash
        hashString += "type [" + getOpenCLTypeString() + "] ";
        hashString += "dims [";
        hashString += std::to_string(dim.d.nx) + " ";
        hashString += std::to_string(dim.d.ny) + " ";
        hashString += std::to_string(dim.d.nz) + " ";
        hashString += std::to_string(dim.d.hx) + " ";
        hashString += std::to_string(dim.d.hy) + " ";
        hashString += std::to_string(dim.d.hz) + " ";
        hashString += std::to_string(dim.d.ox) + " ";
        hashString += std::to_string(dim.d.oy) + " ";
        hashString += std::to_string(dim.d.oz) + " ";
        hashString += std::to_string(dim.ex) + " ";
        hashString += std::to_string(dim.ey) + " ";
        hashString += std::to_string(dim.ez) + " ";
        hashString += std::to_string(dim.tx) + " ";
        hashString += std::to_string(dim.ty) + " ";
        hashString += "] ";
    }

    /**
    * Default %Raster constructor
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::Raster():
        requiredNeighbours(Neighbours::None),
        reductionType(Reduction::None),
        needsStatus(false),
        needsRead(true),
        needsWrite(true) {

        // Set empty name
        RasterBase<CTYPE>::template setProperty<std::string>("name", "");
    }

    /**
    * Named %Raster constructor
    * dimensions will be POD initialised to zero
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::Raster(std::string name_):
        requiredNeighbours(Neighbours::None),
        reductionType(Reduction::None),
        needsStatus(false),
        needsRead(true),
        needsWrite(true) {

        // Set name
        RasterBase<CTYPE>::template setProperty<std::string>("name", name_);
    }
        
    /**
    * Destructor
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::~Raster() {
    }

    /**
    * Copy constructor
    * Creates shallow copy
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::Raster(const Raster<RTYPE, CTYPE> &r):
        RasterBase<CTYPE>::RasterBase(r),
        requiredNeighbours(r.requiredNeighbours),
        reductionType(r.reductionType),
        needsStatus(r.needsStatus),
        needsRead(r.needsRead),
        needsWrite(r.needsWrite) {

        // Copy data
        tiles.clear();
        tiles = r.tiles;
        fileHandlerIn = r.fileHandlerIn;
        fileHandlerOut = r.fileHandlerOut;

        // Copy buffers
        nullBuffer = r.nullBuffer;
        randomBuffer = r.randomBuffer;
    }

    /**
    * Assignment operator
    * Creates shallow copy
    * @param r %Raster to assign.
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> &Raster<RTYPE, CTYPE>::operator=(const Raster<RTYPE, CTYPE> &r) { 

        if (this != &r) {

            // Get name
            auto name = PropertyMap::template getProperty<std::string>("name");
            if (name.empty()) {

                // Get name from input raster
                name = r.template getProperty<std::string>("name");
            }

            // Copy base data
            PropertyMap::operator=(r);
            vars = r.vars;
            proj = r.proj;
            tree = r.tree;
            dim = r.dim;
            interpolation = r.interpolation;
            hashString = r.hashString;

            // Copy data
            tiles.clear();
            tiles = r.tiles;
            requiredNeighbours = r.requiredNeighbours;
            reductionType = r.reductionType;
            needsStatus = r.needsStatus;
            needsRead = r.needsRead;
            needsWrite = r.needsWrite;
            fileHandlerIn = r.fileHandlerIn;
            fileHandlerOut = r.fileHandlerOut;

            // Copy buffers
            nullBuffer = r.nullBuffer;
            randomBuffer = r.randomBuffer;

            // Set name
            RasterBase<CTYPE>::template setProperty<std::string>("name", name);
        }

        return *this; 
    }
    
    /**
    * Get type string for RTYPE data.
    * @return data type string.
    */
    template <typename RTYPE, typename CTYPE>
    std::string Raster<RTYPE, CTYPE>::getDataTypeString() {
        return Solver::getTypeString<RTYPE>();
    }
    
    /**
    * Get type string for RTYPE data.
    * @return RTYPE as OpenCL compatible type string.
    */
    template <typename RTYPE, typename CTYPE>
    std::string Raster<RTYPE, CTYPE>::getOpenCLTypeString() {
        return Solver::getOpenCLTypeString<RTYPE>();
    }

    /**
    * %Raster comparison operator
    * @param l left-hand Raster to compare.
    * @param r right-hand Raster to compare.
    */
    template <typename RTYPE, typename CTYPE>
    bool operator==(const Raster<RTYPE, CTYPE> &l, const Raster<RTYPE, CTYPE> &r) { 

        // Compare objects
        if (&l == &r)
            return true;

        // Compare metrics
        if (!equalSpatialMetrics(l.dim.d, r.dim.d))
            return false;
        
        // Compare number of tiles
        auto &ltiles = l.tiles;
        auto &rtiles = r.tiles;
        if (ltiles.size() != rtiles.size())
            return false;
            return false;
        
        // Compare tiles
        for (std::size_t c = 0; c < ltiles.size(); c++) {
            
            // Compare tile values
            if (ltiles[c] != rtiles[c])
                return false;
        }

        return true; 
    }
        
    /**
    * %Raster %Tile creation.
    * Creates %Tile at index ti and tj
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE> &Raster<RTYPE, CTYPE>::getTile(uint32_t ti, uint32_t tj) {
    
        // Check bounds
        if (ti >= dim.tx || tj >= dim.ty) {
            throw std::range_error("Requested tile out of range in raster");
        }

        // Get pointer
        TilePtr<RTYPE, CTYPE> &tp = tiles[ti+tj*dim.tx];
        if (tp == nullptr) {
            throw std::range_error("Requested tile in raster is empty");
        }
            
        // Get tile
        Tile<RTYPE, CTYPE> &tile = *tp;
            
#ifdef USE_TILE_CACHING

        // Populate tile
        if (!tile.hasData()) {

            // Create data
            tile.createData();

            if (tile.isCached()) {

                // Read from cache
                tile.readFromCache();

            } else {
                    
                // Populate data
                if (fileHandlerIn != nullptr) {

                    // The file handler may not write to tile edges, so clear boundary tiles
                    if (ti == (dim.tx-1) || tj == (dim.ty-1)) {
                        tile.clearData();
                    }

                    // Read from file
                    tile.readData(*this, fileHandlerIn->getReadDataHandler());

                } else {

                    // Set to null
                    tile.clearData();
                }
            }
        }           

        // Update cache - TODO: this can invalidate neighbours if the number of cache tiles is too small
        TileCacheManager<RTYPE, CTYPE>::instance().update(tp.get());

#else

        // Populate tile
        if (!tile.hasData()) {

            // Create data
            tile.createData();
                    
            // Populate data
            if (fileHandlerIn != nullptr) {

                // The file handler may not write to tile edges, so clear boundary tiles
                if (ti == (dim.tx-1) || tj == (dim.ty-1)) {
                    tile.clearData();
                }

                // Read from file
                tile.readData(*this, fileHandlerIn->getReadDataHandler());

            } else {

                // Set to null
                tile.clearData();
            }
        }           

#endif

        // Return tile pointer
        return tile;
    }  

    /**
    * %Raster null %Tile buffer.
    * @return Buffer from Tile containing null data
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Raster<RTYPE, CTYPE>::getNullBuffer() {
    
        // Check for tile
        if (!nullBuffer) {
        
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();

            // Create buffer
            std::size_t bufferSize = dim.d.nz*TileMetrics::tileSizeSquared*sizeof(RTYPE);
            nullBuffer = std::make_shared<cl::Buffer>(context, CL_MEM_READ_WRITE, bufferSize);
            queue.enqueueFillBuffer<RTYPE>(*nullBuffer, getNullValue<RTYPE>(), 0, bufferSize);
        }
        
        // Return buffer
        return *nullBuffer;
    }  

    /**
    * %Raster random %Tile buffer.
    * @return Buffer from Tile containing random data
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Raster<RTYPE, CTYPE>::getRandomBuffer() {
    
        // Check for tile
        if (!randomBuffer) {
        
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();

            // Create buffer
            std::size_t bufferSize = dim.d.nz*TileMetrics::tileSizeSquared*sizeof(RandomState);
            randomBuffer = std::make_shared<cl::Buffer>(context, CL_MEM_READ_WRITE, bufferSize);

            // Initialise buffer
            std::size_t clHash = solver.buildProgram(Models::cl_random_c);
            if (clHash == solver.getNullHash()) {
                throw std::runtime_error("Cannot build program");
            }

            auto initRandomBufferKernel = solver.getKernel(clHash, "initRandomBuffer");
            initRandomBufferKernel.setArg(0, *randomBuffer);
            initRandomBufferKernel.setArg(1, dim.d.nz*TileMetrics::tileSizeSquared);
            initRandomBufferKernel.setArg(2, 58936695); // Random seed TODO set from input
            
            // Execute vector kernel
            queue.enqueueNDRangeKernel(initRandomBufferKernel, cl::NullRange, 
                cl::NDRange(dim.d.nz*TileMetrics::tileSizeSquared), cl::NDRange(TileMetrics::tileSize));
        }
        
        // Return buffer
        return *randomBuffer;
    }
            
    /**
    * %Raster initialisation.
    * Initialises %Raster, creating tiles and internal raster variables.
    * @param nx_ the number of cells in the x dimension.
    * @param ny_ the number of cells in the y dimension.
    * @param nz_ the number of cells in the z dimension.
    * @param hx_ the spacing in the x direction.
    * @param hy_ the spacing in the y direction.
    * @param hz_ the spacing in the z direction.
    * @param ox_ the origin in the x direction.
    * @param oy_ the origin in the y direction.
    * @param oz_ the origin in the z direction.
    * @return \b true if successfully initialised, \b false otherwise.
    */     
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init(
        uint32_t nx_, 
        uint32_t ny_, 
        uint32_t nz_, 
        CTYPE hx_, 
        CTYPE hy_, 
        CTYPE hz_, 
        CTYPE ox_, 
        CTYPE oy_,  
        CTYPE oz_) {

        // Clear any existing data
        tiles.clear();
        tree.clear();
              
        // Check bounds
        if (hx_ < 0.0) return false;
        if (hy_ < 0.0) return false;
        if (hz_ < 0.0) return false;
            
        if (nx_ <= 0) nx_ = 1;
        if (ny_ <= 0) ny_ = 1;
        if (nz_ <= 0) nz_ = 1;
                            
        // Set variables
        dim.d.nx = nx_; dim.d.ny = ny_; dim.d.nz = nz_;
        dim.d.hx = hx_; dim.d.hy = hy_; dim.d.hz = hz_;
        dim.d.ox = ox_; dim.d.oy = oy_; dim.d.oz = oz_;
        dim.d.mx = nx_; dim.d.my = ny_; 

        // Calculate end points
        dim.ex = dim.d.ox+(CTYPE)dim.d.nx*dim.d.hx;
        dim.ey = dim.d.oy+(CTYPE)dim.d.ny*dim.d.hy;
        dim.ez = dim.d.oz+(CTYPE)dim.d.nz*dim.d.hz;

        // Calculate number of tiles needed
        dim.tx = (uint32_t)ceil((CTYPE)nx_/(CTYPE)TileMetrics::tileSize);
        dim.ty = (uint32_t)ceil((CTYPE)ny_/(CTYPE)TileMetrics::tileSize);

        // Initialise tiles
        for (uint32_t tj = 0; tj < dim.ty; tj++)
            for (uint32_t ti = 0; ti < dim.tx; ti++) {
            
                // Create empty tile
                TilePtr<RTYPE, CTYPE> tp = std::make_shared<Tile<RTYPE, CTYPE> >();
                if (!tp->init(ti, tj, dim))
                    return false;

                // Add to tiles
                tiles.push_back(tp);

                // Add to tree
                tree.insert(tp);
            }

        // Create hash
        RasterBase<CTYPE>::createHashString();

        return true;
    }

    /**
    * Two-dimensional raster initialisation.
    * Initialises raster, creating tiles and internal raster variables.
    * @param nx_ the number of cells in the x dimension.
    * @param ny_ the number of cells in the y dimension.
    * @param hx_ the spacing in the x direction.
    * @param hy_ the spacing in the y direction.
    * @param ox_ the origin in the x direction.
    * @param oy_ the origin in the y direction.
    * @return \b true if successfully initialised, \b false otherwise.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init2D(
        uint32_t nx_, 
        uint32_t ny_, 
        CTYPE hx_, 
        CTYPE hy_, 
        CTYPE ox_, 
        CTYPE oy_, 
        CTYPE oz_) {
            return init(nx_, ny_, 0, hx_, hy_, 1.0, ox_, oy_, oz_);
    }

    /**
    * Raster initialisation from dimensions.
    * Initialises %Raster, creating tiles and internal raster variables.
    * @param dim %Raster dimensions.
    * @return \b true if successfully initialised, \b false otherwise.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init(const Dimensions<CTYPE> &dim) {
        return init(dim.nx, dim.ny, dim.nz, dim.hx, dim.hy, dim.hz, dim.ox, dim.oy, dim.oz);
    }

    /**
    * Raster initialisation from bounding box.
    * Initialises raster, creating tiles and internal raster variables.
    * @param bounds %Raster bounds.
    * @param hx_ %Raster x-spacing.
    * @param hy_ %Raster y-spacing.
    * @param hz_ %Raster z-spacing.
    * @return \b true if successfully initialised, \b false otherwise.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init(const BoundingBox<CTYPE> bounds, CTYPE hx_, CTYPE hy_, CTYPE hz_) {

        // Check resolution
        if (hx_ > 0.0) {

            // Set any null spacings from x-spacing
            if (hy_ <= 0.0) {
                hy_ = hx_;
            }
            if (hz_ <= 0.0) {
                hz_ = hx_;
            }

            // Calculate dimensions
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/hx_);
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/hy_);
            uint32_t nz = (uint32_t)ceil((bounds.max.r-bounds.min.r)/hz_);

            // Initialise raster
            return init(nx, ny, nz, hx_, hy_, hz_, bounds.min.p, bounds.min.q, bounds.min.r);

        } else {

            // Exception for invalid raster resolutions
            throw std::runtime_error("Raster resolution must be positive");
        }

        return false;
    }
    
    /**
    * Two-dimensional raster resize.
    * Resizes raster to new number of tile dimensions.
    * @param tnx_ the new number of tiles in the x dimension.
    * @param tny_ the new number of tiles in the y dimension.
    * @param tox_ the tile x origin.
    * @param toy_ the tile y origin.
    * @return set of new tiles as pairs of tile indexes.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::resize2D(uint32_t tnx_, uint32_t tny_, uint32_t tox_, uint32_t toy_) {
                  
        // Check bounds
        if (tnx_ == 0 || tny_ == 0) return false;
        if (tox_ > tnx_-1 || toy_ > tny_-1) return false;

        // Store current tiles
        auto oldTiles = tiles;
        uint32_t otx = dim.tx;
        uint32_t oty = dim.ty;

        // Clear any existing data
        tiles.clear();
        tree.clear();
                            
        // Set variables
        dim.d.nx = tnx_*TileMetrics::tileSize; 
        dim.d.ny = tny_*TileMetrics::tileSize;

        dim.d.mx = dim.d.nx; 
        dim.d.my = dim.d.ny; 

        dim.d.ox -= (CTYPE)(tox_*TileMetrics::tileSize)*dim.d.hx;
        dim.d.oy -= (CTYPE)(toy_*TileMetrics::tileSize)*dim.d.hy;

        // Calculate end points
        dim.ex = dim.d.ox+(CTYPE)dim.d.nx*dim.d.hx;
        dim.ey = dim.d.oy+(CTYPE)dim.d.ny*dim.d.hy;
        dim.ez = dim.d.oz+(CTYPE)dim.d.nz*dim.d.hz;

        // Set new number of tiles
        dim.tx = tnx_;
        dim.ty = tny_;

        // Initialise tiles
        TilePtr<RTYPE, CTYPE> tp;
        for (uint32_t tj = 0; tj < tny_; tj++) {
            for (uint32_t ti = 0; ti < tnx_; ti++) {
            
                // Check for old tiles
                if (ti >= tox_ && tj >= toy_ && ti < tox_+otx  && tj < toy_+oty) {

                    // Use existing tile
                    tp = oldTiles[(ti-tox_)+(tj-toy_)*otx];
                    tp->setIndex(ti, tj);

                } else {

                    // Create new empty tile
                    tp = std::make_shared<Tile<RTYPE, CTYPE> >();
                }

                // Update tile dimensions
                if (!tp->init(ti, tj, dim))
                    return false;

                // Add to tiles
                tiles.push_back(tp);

                // Add to tree
                tree.insert(tp);
            }
        }

        // Update hash
        RasterBase<CTYPE>::createHashString();

        return true;
    }

    /**
    * Indexes for two-dimensional raster resize.
    * @param tnx_ the new number of tiles in the x dimension.
    * @param tny_ the new number of tiles in the y dimension.
    * @param tox_ the tile x origin.
    * @param toy_ the tile y origin.
    * @return set of pairs of new tile indexes.
    */  
    template <typename RTYPE, typename CTYPE>
    tileIndexSet Raster<RTYPE, CTYPE>::resize2DIndexes(uint32_t tnx_, uint32_t tny_, uint32_t tox_, uint32_t toy_) {
    
        // Create return set
        tileIndexSet newTiles;
              
        // Check bounds
        if (tnx_ == 0 || tny_ == 0) return newTiles;
        if (tox_ > tnx_-1 || toy_ > tny_-1) return newTiles;

        // Store current tiles
        uint32_t otx = dim.tx;
        uint32_t oty = dim.ty;

        // Initialise tiles
        TilePtr<RTYPE, CTYPE> tp;
        for (uint32_t tj = 0; tj < tny_; tj++) {
            for (uint32_t ti = 0; ti < tnx_; ti++) {
            
                // Check for old tiles
                if (ti < tox_ || tj < toy_ || ti >= tox_+otx  || tj >= toy_+oty) {
                    newTiles.insert( { ti, tj } );
                }
            }
        }

        return newTiles;
    }

    /**
    * Maximum operator.
    * @return maximum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::max() const {
    
        // Run through raster
        RTYPE max = std::numeric_limits<RTYPE>::lowest();
        for (auto &tp : tiles) {
            if (tp != nullptr)
                max = Geostack::max<RTYPE>(tp->max(), max);
        }    

        // Return null if limits are not found    
        return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
    }

    /**
    * Minimum operator.
    * @return minimum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::min() const {    

        // Run through raster
        RTYPE min = std::numeric_limits<RTYPE>::max();
        for (auto &tp : tiles) {
            if (tp != nullptr)
                min = Geostack::min<RTYPE>(tp->min(), min);
        }

        // Return null if limits are not found
        return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;
    }

    /**
    * Raster reduction operator.
    * @return reduction value for %Raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::reduce() const {
    
        // Reduce values
        switch (reductionType) {

            case(Reduction::Maximum): {
                RTYPE max = std::numeric_limits<RTYPE>::lowest();
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        max = Geostack::max<RTYPE>(tp->reduce(reductionType), max);
                    }
                }

                // Return null if limits are not found    
                return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
            }
            
            case(Reduction::Minimum): {
                RTYPE min = std::numeric_limits<RTYPE>::max();
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        min = Geostack::min<RTYPE>(tp->reduce(reductionType), min);
                    }
                }

                // Return null if limits are not found
                return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;
            }
            
            case(Reduction::Sum):
            case(Reduction::SumSquares): {
                RTYPE sum = getNullValue<RTYPE>();
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        RTYPE v = tp->reduce(reductionType);
                        sum = isValid<RTYPE>(sum) ? sum + v : v;
                    }
                }
                return sum;
            }
            
            case(Reduction::Count): {
                RTYPE count = 0.0;
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        count += tp->reduce(reductionType);
                    }
                }
                return count;
            }
            
            case(Reduction::Mean): {
                RTYPE sum = 0.0;
                RTYPE count = 0.0;
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        sum += tp->reduce(Reduction::Sum);
                        count += tp->reduce(Reduction::Count);
                    }
                }
                return count == 0.0 ? getNullValue<RTYPE>() : sum/count;
            }
        }

        // Return null value
        return getNullValue<RTYPE>();
    }
    
    /**
    * Gets %Tile data buffer from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param dataBuffer a reference to the data buffer to return.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Raster<RTYPE, CTYPE>::getTileDataBuffer(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);
        
        // Return buffer
        return t.getDataBuffer();
    }
    
    /**
    * Gets %Tile reduction buffer from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param reductionBuffer a reference to the data buffer to return.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Raster<RTYPE, CTYPE>::getTileReduceBuffer(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);
        
        // Return buffer
        return t.getReduceBuffer();
    }
    
    /**
    * Gets %Tile reduction value from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @return reduction value.
    */
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::getTileReduction(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);
        
        // Return buffer
        return t.reduce(reductionType);
    }

    /**
    * Resets reduction buffer of %Tile in %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::resetTileReduction(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);

        // Reset reduction buffer
        t.resetReduction();
    }    

    /**
    * Gets %Tile status buffer from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param statusBuffer a reference to the data buffer to return.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Raster<RTYPE, CTYPE>::getTileStatusBuffer(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);

        // Return buffer
        return t.getStatusBuffer();
    }
    
    /**
    * Gets %Dimensions for a %Tile from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @return %TileDimensions of %Tile.
    */
    template <typename RTYPE, typename CTYPE>
    TileDimensions<CTYPE> Raster<RTYPE, CTYPE>::getTileDimensions(uint32_t ti, uint32_t tj) {
    
        // Check bounds
        if (ti >= dim.tx || tj >= dim.ty) {
            throw std::range_error("Requested tile out of range in raster");
        }

        // Get pointer
        TilePtr<RTYPE, CTYPE> &tp = tiles[ti+tj*dim.tx];
        if (tp == nullptr) {
            throw std::range_error("Requested tile in raster is empty");
        }
            
        // Get tile
        return tp->getTileDimensions();
    }
    
    /**
    * Gets %BoundingBox for a %Tile from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @return %BoundingBox of %Tile.
    */
    template <typename RTYPE, typename CTYPE>
    BoundingBox<CTYPE> Raster<RTYPE, CTYPE>::getTileBounds(uint32_t ti, uint32_t tj) {
    
        // Check bounds
        if (ti >= dim.tx || tj >= dim.ty) {
            throw std::range_error("Requested tile out of range in raster");
        }

        // Get pointer
        TilePtr<RTYPE, CTYPE> &tp = tiles[ti+tj*dim.tx];
        if (tp == nullptr) {
            throw std::range_error("Requested tile in raster is empty");
        }
            
        // Get tile
        return tp->getBounds();
    }

    /**
    * Get copy of tile data from %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param data handle to vector of type RTYPE to copy raster data into.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::getTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE> &tileData) {

        // Get tile handle
        auto &t = getTile(ti, tj);

        // Set tile to nodata
        size_t dataSize = t.getDataSize();
        tileData.resize(dataSize);
        tileData.assign(dataSize, getNullValue<RTYPE>());
        
        // Copy tile data
        t.getData(tileData.begin(), 0, dataSize);
    }

    /**
    * Get copy of tile data from %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param z slice index.
    * @param data handle to vector of type RTYPE to copy raster data into.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::getTileDataSlice(uint32_t ti, uint32_t tj, uint32_t k, std::vector<RTYPE> &tileData) {

        if (k < dim.d.nz) {

            // Get tile handle
            auto &t = getTile(ti, tj);

            // Set tile to nodata
            tileData.resize(TileMetrics::tileSizeSquared);
            
            // Copy tile data
            t.getData(tileData.begin(), k*TileMetrics::tileSizeSquared, (k+1)*TileMetrics::tileSizeSquared);

        } else {
            throw std::runtime_error("Slice index out of bounds");
        }
    }

    /**
    * Set tile data in %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param data handle to vector of type CTYPE to copy raster data from.
    * @return true if data is successfully set, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::setTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE>& data) {

        // Get tile
        auto &t = getTile(ti, tj);

        // Check size
        size_t dataSize = t.getDataSize();
        if (data.size() != dataSize) {
            throw std::runtime_error("Tile data size is different from requested write size");
        }

        // Copy tile data
        t.setData(data.begin(), data.begin()+dataSize, 0);
    }

    /**
    * Get tile status in %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param reference to status.
    */
    template <typename RTYPE, typename CTYPE>
    cl_uint Raster<RTYPE, CTYPE>::getTileStatus(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);

        // Return status
        return t.getStatus();
    }

    /**
    * Set tile status in %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param newStatus new status value.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::setTileStatus(uint32_t ti, uint32_t tj, cl_uint newStatus) {

        // Get tile handle
        auto &t = getTile(ti, tj);

        // Return status
        t.setStatus(newStatus);
    }

    /**
    * Get tile status in %Raster and reset to new status.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param reference to status.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::getResetTileStatus(uint32_t ti, uint32_t tj, cl_uint &rStatus, cl_uint newStatus) {

        // Get tile handle
        auto &t = getTile(ti, tj);

        // Return status
        rStatus = t.getResetStatus(newStatus);
        return true;
    }

    /**
    * Set tile values in raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param val value to set.
    * @return true if data is successfully set, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::setAllTileCellValues(uint32_t ti, uint32_t tj, RTYPE val) {

        // Get tile
        auto &t = getTile(ti, tj);
        
        // Set all values in tile
        t.setAllCellValues(val);

        return true;
    }

    /**
    * Get value from raster cell.
    * @param i x cell index.
    * @param j y cell index.
    * @param k z cell index.
    */
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::getCellValue(uint32_t i, uint32_t j, uint32_t k) {
        
        // Check bounds
        if (i > dim.d.nx-1 || j > dim.d.ny-1 || k > dim.d.nz-1)
            return getNullValue<RTYPE>();

        // Get tile, tile index is quotient (using bitshift)
        uint32_t ti = i>>TileMetrics::tileSizePower;
        uint32_t tj = j>>TileMetrics::tileSizePower;
        auto &t = getTile(ti, tj);

        // Return value in tile, tile cell index is remainder (using mask)
        return t(i & TileMetrics::tileSizeMask, j & TileMetrics::tileSizeMask, k);
    }

    /**
    * Get nearest-neighbour value at position in raster coordinates. f
    * @param[in] x x-position in world coordinates.
    * @param[in] y y-position in world coordinates.
    * @param[in] z z-position in world coordinates.
    * @return Nearest-neighbour cell-centred value.
    */
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::getNearestValue(CTYPE x, CTYPE y, CTYPE z) {
    
        // Convert to raster coordinates
        CTYPE rx = (x-dim.d.ox)/dim.d.hx;
        CTYPE ry = (y-dim.d.oy)/dim.d.hy;
        CTYPE rz = (z-dim.d.oz)/dim.d.hz;
        
        // Check bounds
        if (rx < 0.0 || ry < 0.0 || rz < 0.0 || 
            rx >= (CTYPE)dim.d.nx || ry >= (CTYPE)dim.d.ny || rz >= (CTYPE)dim.d.nz) {
            return getNullValue<RTYPE>();
        } else {
        
            // Convert to cell coordinates
            uint32_t i = (uint32_t)rx;
            uint32_t j = (uint32_t)ry;
            uint32_t k = (uint32_t)rz;

            return getCellValue(i, j, k);
        }
    }

    /**
    * Get bilinearly interpolated value at position in raster coordinates. 
    * @param[in] x x-position in world coordinates.
    * @param[in] y y-position in world coordinates.
    * @param[in] z z-position in world coordinates.
    * @return Bilinearly interpolated cell-centred value.
    */
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::getBilinearValue(CTYPE x, CTYPE y, CTYPE z) {
        
        // Convert to raster coordinates
        double rx = (double)(x-dim.d.ox)/dim.d.hx;
        double ry = (double)(y-dim.d.oy)/dim.d.hy;
        double rz = (double)(z-dim.d.oz)/dim.d.hz;
        
        // Check bounds
        if (rx < 0.0 || ry < 0.0 || rz < 0.0 || 
            rx > (double)dim.d.nx || ry > (double)dim.d.ny || rz > (double)dim.d.nz) {
            return getNullValue<RTYPE>();
        } else {        
        
            // Offset to cell centre for interpolation
            rx-=0.5;
            ry-=0.5;
            rz-=0.5;
            
            // Offset within grid cell
            double a = 0.0;
            double b = 0.0;
            double c = 0.0;
            
            uint32_t i, ip;
            if (rx < 0.0) {
                i = 0;
                ip = 0;
                a = 0.0;
            } else if (rx < (double)(dim.d.nx-1)) {
                i = (uint32_t)rx;
                ip = i+1;
                a = rx-(double)i;
            } else {
                i = dim.d.nx-1;
                ip = dim.d.nx-1;
                a = 0.0;
            }
            
            uint32_t j, jp;
            if (ry < 0.0) {
                j = 0;
                jp = 0;
                b = 0.0;
            } else if (ry < (double)(dim.d.ny-1)) {
                j = (uint32_t)ry;
                jp = j+1;
                b = ry-(double)j;
            } else {
                j = dim.d.ny-1;
                jp = dim.d.ny-1;
                b = 0.0;
            }
            
            uint32_t k, kp;
            if (rz < 0.0) {
                k = 0;
                kp = 0;
                c = 0.0;
            } else if (rz < (double)(dim.d.nz-1)) {
                k = (uint32_t)rz;
                kp = k+1;
                c = rz-(double)k;
            } else {
                k = dim.d.nz-1;
                kp = dim.d.nz-1;
                c = 0.0;
            }

            double u000 = (double)getCellValue(i , j , k );
            double u100 = (double)getCellValue(ip, j , k );
            double u010 = (double)getCellValue(i , jp, k );
            double u110 = (double)getCellValue(ip, jp, k );
            double u001 = (double)getCellValue(i , j , kp);
            double u101 = (double)getCellValue(ip, j , kp);
            double u011 = (double)getCellValue(i , jp, kp);
            double u111 = (double)getCellValue(ip, jp, kp);
            
            // Calculate interpolated value
            double v = 0.0;
            v = (double)(u111*     a *     b *     c );
            v+= (double)(u011*(1.0-a)*     b *     c );
            v+= (double)(u101*     a *(1.0-b)*     c );
            v+= (double)(u110*     a *     b *(1.0-c));
            v+= (double)(u001*(1.0-a)*(1.0-b)*     c );
            v+= (double)(u010*(1.0-a)*     b *(1.0-c));
            v+= (double)(u100*     a *(1.0-b)*(1.0-c));
            v+= (double)(u000*(1.0-a)*(1.0-b)*(1.0-c));

            return (RTYPE)v;
        }
    }

    /**
    * Set value in raster cell.
    * This is an expensive operation.
    * @param val value to set.
    * @param i x cell index.
    * @param j y cell index.
    * @param k z cell index.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::setCellValue(RTYPE val, uint32_t i, uint32_t j, uint32_t k) {
    
        // Check bounds
        if (i > dim.d.nx-1 || j > dim.d.ny-1 || k > dim.d.nz-1)
            return;
        
        // Get tile, tile index is quotient (using bitshift)
        uint32_t ti = i>>TileMetrics::tileSizePower;
        uint32_t tj = j>>TileMetrics::tileSizePower;
        
        // Get tile handle
        auto &t = getTile(ti, tj);

        // Return value in tile, tile cell index is remainder (using mask)
        t(i & TileMetrics::tileSizeMask, j & TileMetrics::tileSizeMask, k) = val;
    }

    /**
    * Set value in all raster cells.
    * Sets entire raster to specified value
    * @param val value to set.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::setAllCellValues(RTYPE val) { 
    
        // Set value
        for (uint32_t tj = 0; tj < dim.ty; tj++) {
            for (uint32_t ti = 0; ti < dim.tx; ti++) {
        
                // Create new tile if no tile currently exists
                auto &t = getTile(ti, tj);

                // Set all values in tile
                t.setAllCellValues(val);
            }
        }
    }
    
    /**
    * Deletes all %Raster data in tiles
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::deleteRasterData() {
    
        // Set value
        for (uint32_t tj = 0; tj < dim.ty; tj++) {
            for (uint32_t ti = 0; ti < dim.tx; ti++) {

                // Get tile pointer
                TilePtr<RTYPE, CTYPE> tp = tiles[ti+tj*dim.tx];
                if (tp != nullptr) {

                    // Delete tile data
                    tp->deleteData(true);
                }
            }
        }
    }
    
    /**
    * Get %Raster region from %Raster.
    * Returns all tiles intersecting region.
    * @param bounds Bounds of region
    * @return %Raster of geometry within region.
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> Raster<RTYPE, CTYPE>::region(BoundingBox<CTYPE> bounds) {

        // Get region
        std::vector<GeometryBasePtr<CTYPE> > tileList;
        tree.search(bounds, tileList, GeometryType::Tile);
        
        // Create return raster
        Raster<RTYPE, CTYPE> r;
        r.setProjectionParameters(proj);

        // Check for null result
        if (tileList.size() == 0)
            return r;

        // Set data
        r.fileHandlerIn = fileHandlerIn;
        r.fileHandlerOut = fileHandlerOut;

        // Set cell spacing
        r.dim.d.hx = dim.d.hx; 
        r.dim.d.hy = dim.d.hy; 
        r.dim.d.hz = dim.d.hz;

        // Create bounds
        r.dim.d.ox = std::numeric_limits<CTYPE>::max();
        r.dim.d.oy = std::numeric_limits<CTYPE>::max();
        r.dim.d.oz = dim.d.oz;

        // Index ranges
        uint32_t ti_min = std::numeric_limits<uint32_t>::max();
        uint32_t ti_max = 0;
        uint32_t tj_min = std::numeric_limits<uint32_t>::max();
        uint32_t tj_max = 0;
                
        // Loop through tiles
        for (auto &g : tileList) {
        
            // Get tile pointer
            auto tp = std::static_pointer_cast<Tile<RTYPE, CTYPE> >(g);
            
            // Update bounds
            auto tDim = tp->getTileDimensions();
            r.dim.d.ox = std::min(r.dim.d.ox, tDim.d.ox);
            r.dim.d.oy = std::min(r.dim.d.oy, tDim.d.oy);
            ti_min = std::min(tDim.ti, ti_min);
            ti_max = std::max(tDim.ti, ti_max);
            tj_min = std::min(tDim.tj, tj_min);
            tj_max = std::max(tDim.tj, tj_max);
        }

        // Set tile count
        r.dim.tx = ti_max-ti_min+1;
        r.dim.ty = tj_max-tj_min+1;

        // Set memory size
        r.dim.d.mx = r.dim.tx*TileMetrics::tileSize;
        r.dim.d.my = r.dim.ty*TileMetrics::tileSize;

        // Calculate end offsets
        uint32_t dx = std::max((int64_t)(ti_max+1)*TileMetrics::tileSize-(int64_t)dim.d.nx, (int64_t)0);
        uint32_t dy = std::max((int64_t)(tj_max+1)*TileMetrics::tileSize-(int64_t)dim.d.ny, (int64_t)0);

        // Set cell counts
        r.dim.d.nx = r.dim.d.mx-dx; 
        r.dim.d.ny = r.dim.d.my-dy; 
        r.dim.d.nz = dim.d.nz;

        // Set end limit
        r.dim.ex = r.dim.d.ox+r.dim.d.hx*(CTYPE)r.dim.d.nx;
        r.dim.ey = r.dim.d.oy+r.dim.d.hy*(CTYPE)r.dim.d.ny;
        r.dim.ez = dim.ez;

        // Tile addition table
        std::map<std::pair<uint32_t, uint32_t>, TilePtr<RTYPE, CTYPE> > tileTable;

        // Add tiles
        for (auto &g : tileList) {

            // Get tile pointer
            auto tp = std::static_pointer_cast<Tile<RTYPE, CTYPE> >(g);
            
            // Tile relative position
            auto tDim = tp->getTileDimensions();
            uint32_t tpx = tDim.ti-ti_min;
            uint32_t tpy = tDim.tj-tj_min;

            tileTable[std::make_pair(tpx, tpy)] = tp;
        }
        
        // Create or add tiles
        for (uint32_t j = 0; j < r.dim.ty; j++) {
            for (uint32_t i = 0; i < r.dim.tx; i++) {

                // Search for tile
                TilePtr<RTYPE, CTYPE> tp = nullptr;
                auto it = tileTable.find(std::make_pair(i, j));
                if (it != tileTable.end()) {

                    // Get tile pointer
                    tp = std::static_pointer_cast<Tile<RTYPE, CTYPE> >(it->second);

                    // Add to tree
                    r.tree.insert(tp);
                }

                // Add to vector
                r.tiles.push_back(tp);
            }
        }

        return r;
    }
    
    /**
    * Map %Vector distances into %Raster
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param widthPropertyName name of feature width property
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::mapVector(Vector<CTYPE> &v, std::string script, size_t geometryTypes, 
        std::string widthPropertyName, std::string levelPropertyName) {
    
        try {

            // Check projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (proj != vproj);
            if (usingProjection) {

                // Check projections are valid
                if (proj.type == 0 || vproj.type == 0) {
                    throw std::runtime_error("One or more projections is undefined for distance mapping");
                }

                // Check projections are the same type
                if ((proj.type == 1 && vproj.type == 2) || (proj.type == 2 && vproj.type == 1)) {
                    throw std::runtime_error("Projections must have the same length units");
                }
            }
            
            // Check for empty script
            if (script.length() == 0) {

                // Default script
                script = "output = fmin(output, length(d)-radius);";
            }

            // Parse script
            script = Solver::processScript(script);

            // Build kernel code
            std::string kernelStr;
            if (usingProjection) {

                // Update projection definitions
                auto projStr = Models::cl_projection_head_c;
                if (proj.type == 1) {
                    std::string projDef = "USE_PROJ_TYPE_" + std::to_string(proj.cttype);
                    projStr = std::regex_replace(projStr, std::regex(std::string("__") + projDef), projDef);
                }
                if (vproj.type == 1) {
                    std::string projDef = "USE_PROJ_TYPE_" + std::to_string(vproj.cttype);
                    projStr = std::regex_replace(projStr, std::regex(std::string("__") + projDef), projDef);
                }

                kernelStr = projStr + Models::cl_map_distance_c;
            
                // Create argument list
                std::string argsList;
                argsList += ", const ProjectionParameters _rproj";
                argsList += ", const ProjectionParameters _vproj";
            
                // Parse script
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__PROJECT__|__PROJECT__\\*\\/"), std::string(""));

            } else {

                // Create program string without projection
                kernelStr = Models::cl_map_distance_c;
            }

            // Patch script
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__CODE__\\*\\/"), script);

            // Build program
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            std::size_t clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {
                throw std::runtime_error("Cannot build program");
            }

            // Loop through tiles
            for (uint32_t tj = 0; tj < dim.ty; tj++) {
                for (uint32_t ti = 0; ti < dim.tx; ti++) {

                    // Get handle to tile
                    auto &t = getTile(ti, tj);

                    // Map vector to tile
                    t.mapVector(v, clHash, proj, geometryTypes, widthPropertyName, levelPropertyName);
                }
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }
    
    /**
    * Map %Vector distances into %Raster %Tile at index ti and tj
    * Raster and Vector projections must be the same
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param widthPropertyName name of feature width property
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::mapTileVector(uint32_t ti, uint32_t tj, Vector<CTYPE> &v, std::string script, 
        size_t geometryTypes, std::string widthPropertyName, std::string levelPropertyName) {

            // Check projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (proj != vproj);
            if (usingProjection) {

                // Check projections are valid
                if (proj.type == 0 || vproj.type == 0) {
                    throw std::runtime_error("One or more projections is undefined for distance mapping");
                }

                // Check projections are the same type
                if ((proj.type == 1 && vproj.type == 2) || (proj.type == 2 && vproj.type == 1)) {
                    throw std::runtime_error("Projections must have the same length units");
                }
            }
            
            // Check for empty script
            if (script.length() == 0) {

                // Default script
                script = "output = fmin(output, length(d)-radius);";
            }

            // Parse script
            script = Solver::processScript(script);

            // Build kernel code
            std::string kernelStr;
            if (usingProjection) {

                // Update projection definitions
                auto projStr = Models::cl_projection_head_c;
                if (proj.type == 1) {
                    std::string projDef = "USE_PROJ_TYPE_" + std::to_string(proj.cttype);
                    projStr = std::regex_replace(projStr, std::regex(std::string("__") + projDef), projDef);
                }
                if (vproj.type == 1) {
                    std::string projDef = "USE_PROJ_TYPE_" + std::to_string(vproj.cttype);
                    projStr = std::regex_replace(projStr, std::regex(std::string("__") + projDef), projDef);
                }

                kernelStr = projStr + Models::cl_map_distance_c;
                
                // Create argument list
                std::string argsList;
                argsList += ", const ProjectionParameters _rproj";
                argsList += ", const ProjectionParameters _vproj";
            
                // Parse script
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__PROJECT__|__PROJECT__\\*\\/"), std::string(""));

            } else {

                // Create program string without projection
                kernelStr = Models::cl_map_distance_c;
            }

            // Patch script
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__CODE__\\*\\/"), script);

            // Build program
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            std::size_t clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {
                throw std::runtime_error("Cannot build program");
            }

        try { 
        
            // Get handle to tile
            auto &t = getTile(ti, tj);

            // Map vector to tile
            t.mapVector(v, clHash, proj, geometryTypes, widthPropertyName, levelPropertyName);

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }
    
    /**
    * Rasterise %Vector onto %Raster
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param script script to run for rasterisation
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::rasterise(Vector<CTYPE> &v, std::string script, size_t geometryTypes) {
    
        try {

            // Check geometry types
            if (!(geometryTypes & (GeometryType::Point | GeometryType::LineString | GeometryType::Polygon))) {
                throw std::runtime_error("No requested geometry types to map");
            }

            // Check for underscores in script, these are reserved for internal variables
            if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
            }

            // Check for empty script
            if (script.length() == 0) {

                // Default script
                script = "output = 1.0;";
            }

            // Check script
            if (!std::regex_search(script, std::regex("\\boutput\\b|\\batomic_\\w*\\("))) {
                throw std::runtime_error("Rasterise kernel code must contain 'output' variable or atomic function");
            }

            // Parse script
            script = Solver::processScript(script);

            // Check projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (proj != vproj);
            if (usingProjection && (proj.type == 0 || vproj.type == 0)) {
                throw std::runtime_error("One or more projections is undefined for rasterisation");
            }

            // Build kernel code
            std::string kernelStr;
            if (usingProjection) {

                // Update projection definitions
                auto projStr = Models::cl_projection_head_c;
                if (proj.type == 1) {
                    std::string projDef = "USE_PROJ_TYPE_" + std::to_string(proj.cttype);
                    projStr = std::regex_replace(projStr, std::regex(std::string("__") + projDef), projDef);
                }
                if (vproj.type == 1) {
                    std::string projDef = "USE_PROJ_TYPE_" + std::to_string(vproj.cttype);
                    projStr = std::regex_replace(projStr, std::regex(std::string("__") + projDef), projDef);
                }

                kernelStr = projStr + Models::cl_rasterise_c;

            } else {
                kernelStr = Models::cl_rasterise_c;
            }

            // Replace type
            kernelStr = std::regex_replace(kernelStr, std::regex("TYPE"), Solver::getOpenCLTypeString<RTYPE>());
            
            // Create argument list
            std::string argsList;
            if (usingProjection) {
                argsList += ", const ProjectionParameters _rproj";
                argsList += ", const ProjectionParameters _vproj";
            }

            // Search for fields
            std::vector<std::string> fields;
            auto &properties = v.getProperties();
            for (auto &pi : properties.template getPropertyRefs<std::vector<int> >()) { // TODO reduce into one function
                if (std::regex_search(script, std::regex("\\b" + pi.first + "\\b"))) {
                    fields.push_back(pi.first);
                }
            }
            for (auto &pi : properties.template getPropertyRefs<std::vector<RTYPE> >()) {
                if (std::regex_search(script, std::regex("\\b" + pi.first + "\\b"))) {
                    fields.push_back(pi.first);
                }
            }
            for (auto &pi : properties.template getPropertyRefs<std::vector<cl_uint> >()) {
                if (std::regex_search(script, std::regex("\\b" + pi.first + "\\b"))) {
                    fields.push_back(pi.first);
                }
            }

            for (std::size_t i = 0; i < fields.size(); i++) {

                // Replace field
                std::string fieldName = std::string("_field_") + std::to_string(i);
                script = std::regex_replace(script, std::regex("\\b" + fields[i] + "\\b"), 
                    std::string("*(") + fieldName + std::string("+index)"));

                // Add to argument list
                argsList += ",\n__global REAL *" + fieldName;
            }

            // Search for output pointer
            if (std::regex_search(script, std::regex("\\batomic_\\w*\\("))) {

                // Replace atomic functions
                script = std::regex_replace(script, std::regex("\\batomic_(inc|dec)\\(\\)"), 
                    std::string("atomic_$1(_pval2D_a(_u, _i, _j))"));
                script = std::regex_replace(script, std::regex("\\batomic_(add|sub|xchg|cmpxchg|min|max|and|or)\\((.*)\\)"), 
                    std::string("atomic_$1(_pval2D_a(_u, _i, _j), $2)"));

            } else {

                // Enable write
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__WRITE__|__WRITE__\\*\\/"), std::string(""));
            }

            // Parse script
            script = Solver::processScript(script);
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__CODE__\\*\\/"), script);
            if (usingProjection) {
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__PROJECT__|__PROJECT__\\*\\/"), std::string(""));
            }
                        
            // Build program
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            std::size_t clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {
                throw std::runtime_error("Cannot build program");
            }

            // Loop through tiles
            for (uint32_t tj = 0; tj < dim.ty; tj++) {
                for (uint32_t ti = 0; ti < dim.tx; ti++) {

                    // Get handle to tile
                    auto &t = getTile(ti, tj);

                    // Map vector to tile
                    t.rasterise(v, clHash, fields, proj, geometryTypes);
                }
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }    
    
    /**
    * Index %Vector geometry from %Raster
    * @param v the %Vector to map.
    */
    template <typename RTYPE, typename CTYPE>
    std::vector<RasterIndex<CTYPE> > Raster<RTYPE, CTYPE>::getVectorTileIndexes(Vector<CTYPE> &v, 
        uint32_t ti, uint32_t tj, std::size_t clHash) {

        // Create return vector
        std::vector<RasterIndex<CTYPE> > rasterIndexes;

        // Get handle to tile
        auto &t = getTile(ti, tj);

        // Map vector to tile
        t.index(v, rasterIndexes, clHash, proj);

        return rasterIndexes;
    }

    /**
    * Create %Vector of cell centres from %Raster data.
    * @return %Vector containing points of cell centres.
    */
    template <typename RTYPE, typename CTYPE>
    Vector<CTYPE> Raster<RTYPE, CTYPE>::cellCentres() {
        
        // Create vector
        Vector<CTYPE> v;
        
        // Create points
        CTYPE py = dim.d.oy+0.5*dim.d.hy;
        for (uint32_t j = 0; j < dim.d.ny; j++) {
            CTYPE px = dim.d.ox+0.5*dim.d.hx;
            for (uint32_t i = 0; i < dim.d.nx; i++) {
                v.addPoint( { px, py } );
                px += dim.d.hx;
            }
            py += dim.d.hy;
        }

        // Set projection
        v.setProjectionParameters(proj);
        
        return v;
    }

    /**
    * Vectorise %Raster data, returning a closed contour.
    * @return contour %Vector of %Raster.
    */
    template <typename RTYPE, typename CTYPE>
    Vector<CTYPE> Raster<RTYPE, CTYPE>::vectorise(std::vector<RTYPE> countourValues, RTYPE noDataValue) {
        
        // Create vector
        Vector<CTYPE> v;

        // Check contour values
        if (countourValues.size() == 0) {
            std::cout << "WARNING: No contour values to vectorise" << std::endl;
            return v;
        }

        try {

            // Get OpenCL handles
            cl_int err = CL_SUCCESS;
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();

            // Build kernel code
            std::string kernelStr = Models::cl_vectorise_c;

            // Build program
            std::size_t clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {
                throw std::runtime_error("Cannot build vectorise program");
            }

            // Get kernel
            auto vectoriseKernel = solver.getKernel(clHash, "vectorise");

            // Create index and vertex buffers
            cl::Buffer bi = cl::Buffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_uint), NULL, &err);
            cl::Buffer bc = cl::Buffer(context, CL_MEM_WRITE_ONLY, 
                4*TileMetrics::tileSizeSquared*sizeof(Coordinate<RTYPE>), NULL, &err);
            if (err != CL_SUCCESS) {
                throw std::runtime_error("Cannot create vectorise buffers");
            }
            
            // Loop over contour values
            for (auto contourValue : countourValues) {

                // Loop through tiles
                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {
                    
                        // Get tile handle
                        auto &t = getTile(ti, tj);
                        
                        // Clear index buffer
                        queue.enqueueFillBuffer<cl_uint>(bi, 0, 0 , sizeof(cl_uint));

                        // Vectorise tile
                        vectoriseKernel.setArg( 0, contourValue);
                        vectoriseKernel.setArg( 1, t.getDataBuffer());

                        // Add north buffer
                        if (tj < dim.ty-1) {
                            Tile<RTYPE, CTYPE> &tN = getTile(ti, tj+1);
                            vectoriseKernel.setArg( 2, tN.getDataBuffer());
                        } else {
                            vectoriseKernel.setArg( 2, getNullBuffer());
                        }

                        // Add east buffer
                        if (ti < dim.tx-1) {
                            Tile<RTYPE, CTYPE> &tE = getTile(ti+1, tj);
                            vectoriseKernel.setArg( 3, tE.getDataBuffer());
                        } else {
                            vectoriseKernel.setArg( 3, getNullBuffer());
                        }

                        // Add north east buffer
                        if (ti < dim.tx-1 && tj < dim.ty-1) {
                            Tile<RTYPE, CTYPE> &tNE = getTile(ti+1, tj+1);
                            vectoriseKernel.setArg( 4, tNE.getDataBuffer());
                        } else {
                            vectoriseKernel.setArg( 4, getNullBuffer());
                        }

                        vectoriseKernel.setArg( 5, t.getDimensions());
                        vectoriseKernel.setArg( 6, noDataValue);
                        vectoriseKernel.setArg( 7, bc);
                        vectoriseKernel.setArg( 8, bi);
                        queue.enqueueNDRangeKernel(vectoriseKernel, cl::NullRange, 
                            TileMetrics::rangeTile2DGlobal, TileMetrics::rangeTile2DLocal);

                        // Flush queue
                        queue.flush();

                        // Read number of coordinates
                        cl_uint nc = 0;
                        queue.enqueueReadBuffer(bi, CL_TRUE, 0, sizeof(cl_uint), &nc);
            
                        if (nc > 0) {

                            // Read coordinates
                            CoordinateList<CTYPE> c;
                            c.resize(nc);
                            queue.enqueueReadBuffer(bc, CL_TRUE, 0, nc*sizeof(Coordinate<CTYPE>), c.data());
            
                            // Add line segments to Vector
                            for (cl_uint i = 0; i < nc; i+=2) {
                                auto id = v.addLineString( { c[i], c[i+1] } );
                                v.template setProperty<RTYPE>(id, "value", contourValue);
                            }
                        }
                    }
                }
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }

        // De-duplicate
        v.deduplicateVertices();

        // Set projection
        v.setProjectionParameters(proj);
        
        return v;
    }
    
    /**
    * Creates an OpenCL buffer from a %Raster region.
    * @param bounds the %BoundingBox of the region.
    * @param regionBuffer a reference to a region buffer to return.
    * @param rasterRegionDim a reference to a %RasterDimensions object to return.
    * @return true if the raster overlaps the region, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::createRegionBuffer(
        BoundingBox<CTYPE> bounds, 
        cl::Buffer &rasterRegionBuffer, 
        RasterDimensions<CTYPE> &rasterRegionDim) {

        try {

            // Check raster
            if (hasData()) {
            
                // Get region
                std::vector<GeometryBasePtr<CTYPE> > tileList;
                tree.search(bounds, tileList, GeometryType::Tile);

                // Check for null result
                if (tileList.size() != 0) {

                    // Get tile index bounds
                    uint32_t ti_min = std::numeric_limits<uint32_t>::max();
                    uint32_t ti_max = 0;
                    uint32_t tj_min = std::numeric_limits<uint32_t>::max();
                    uint32_t tj_max = 0;
                    for (auto &g : tileList) {

                        // Get tile pointer
                        auto tp = std::static_pointer_cast<Tile<RTYPE, CTYPE> >(g);
                        
                        // Tile relative position
                        auto tDim = tp->getTileDimensions();
                        ti_min = std::min(tDim.ti, ti_min);
                        ti_max = std::max(tDim.ti, ti_max);
                        tj_min = std::min(tDim.tj, tj_min);
                        tj_max = std::max(tDim.tj, tj_max);
                    }
                    uint32_t ntx = ti_max-ti_min+1;
                    uint32_t nty = tj_max-tj_min+1;
                    
                    // Calculate end offsets
                    uint32_t dx = std::max((int64_t)(ti_max+1)*TileMetrics::tileSize-(int64_t)dim.d.nx, (int64_t)0);
                    uint32_t dy = std::max((int64_t)(tj_max+1)*TileMetrics::tileSize-(int64_t)dim.d.ny, (int64_t)0);

                    // Set dimensions
                    rasterRegionDim.d.hx = dim.d.hx;
                    rasterRegionDim.d.hy = dim.d.hy;
                    rasterRegionDim.d.hz = dim.d.hz;
                    rasterRegionDim.d.nx = ntx*TileMetrics::tileSize-dx;
                    rasterRegionDim.d.ny = nty*TileMetrics::tileSize-dy;
                    rasterRegionDim.d.nz = dim.d.nz;
                    rasterRegionDim.d.ox = dim.d.ox+dim.d.hx*ti_min*TileMetrics::tileSize;
                    rasterRegionDim.d.oy = dim.d.oy+dim.d.hy*tj_min*TileMetrics::tileSize;
                    rasterRegionDim.d.oz = dim.d.oz;
                    rasterRegionDim.d.mx = ntx*TileMetrics::tileSize;
                    rasterRegionDim.d.my = nty*TileMetrics::tileSize;
                    rasterRegionDim.ex = dim.d.ox+dim.d.hx*(ti_max+1)*TileMetrics::tileSize;
                    rasterRegionDim.ey = dim.d.oy+dim.d.hy*(tj_max+1)*TileMetrics::tileSize;
                    rasterRegionDim.ez = dim.ez;
                    rasterRegionDim.tx = ntx;
                    rasterRegionDim.ty = nty;

                    // Return tile buffer if only one tile is found
                    if (ntx == 1 && nty == 1) {
                        rasterRegionBuffer = getTile(ti_min, tj_min).getDataBuffer();
                        return true;
                    }

                    // Get OpenCL handles
                    cl_int err = CL_SUCCESS;
                    Geostack::Solver &solver = Geostack::Solver::getSolver();
                    auto &context = solver.getContext();
                    auto &queue = solver.getQueue();

                    // Set block sizes
                    cl::detail::size_t_array srcOrigin;
                    srcOrigin[0] = 0;
                    srcOrigin[1] = 0;
                    srcOrigin[2] = 0;

                    cl::detail::size_t_array region;
                    region[0] = TileMetrics::tileSize*sizeof(RTYPE);
                    region[1] = TileMetrics::tileSize;
                    region[2] = dim.d.nz;

                    cl::detail::size_t_array dstOrigin;
                    dstOrigin[2] = 0;

                    // Get buffer
                    std::size_t dataSize = (std::size_t)ntx*nty*TileMetrics::tileSizeSquared*dim.d.nz*sizeof(RTYPE);
                    rasterRegionBuffer = RasterBase<CTYPE>::getCacheBuffer(dataSize);
                    queue.enqueueFillBuffer(rasterRegionBuffer, getNullValue<RTYPE>(), 0, dataSize);

                    // Loop over tiles
                    for (uint32_t rtj = 0; rtj < nty; rtj++) {
                        for (uint32_t rti = 0; rti < ntx; rti++) {
                                        
                            // Set destination block
                            dstOrigin[0] = (cl::size_type)rti*TileMetrics::tileSize*sizeof(RTYPE);
                            dstOrigin[1] = (cl::size_type)rtj*TileMetrics::tileSize;

                            // Copy tiles into buffer
                            queue.enqueueCopyBufferRect(getTile(ti_min+rti, tj_min+rtj).getDataBuffer(), 
                                rasterRegionBuffer, srcOrigin, dstOrigin, region, 
                                (cl::size_type)TileMetrics::tileSize*sizeof(RTYPE), 
                                (cl::size_type)TileMetrics::tileSizeSquared*sizeof(RTYPE), 
                                (cl::size_type)ntx*TileMetrics::tileSize*sizeof(RTYPE), 
                                (cl::size_type)ntx*nty*TileMetrics::tileSizeSquared*sizeof(RTYPE));
                        }
                    }
                    return true;
                }
            }
            return false;

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Read file to %Raster.
    * @param fileName name of file to read.
    * @return true on successful read.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::read(std::string fileName, std::string jsonConfig) {

        // Get filename extension
        auto split = Strings::splitPath(fileName);

        if (split[2] == "asc" || split[2] == "ascii") {

            // Create handler
            fileHandlerIn = std::make_shared<AsciiHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "flt") {

            // Create handler
            fileHandlerIn = std::make_shared<FltHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "tif" || split[2] == "tiff"|| split[2] == "gtiff" || split[2] == "geotiff") {

            // Create handler
            fileHandlerIn = std::make_shared<GeoTIFFHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "nc" || split[2] == "nc3" || split[2] == "netcdf") {

            // Create handler
            fileHandlerIn = std::make_shared<NetCDFHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "gsr") {

            // Create handler
            fileHandlerIn = std::make_shared<GsrHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "png") {

            // Create handler
            fileHandlerIn = std::make_shared<PngHandler<RTYPE, CTYPE> >();

        } else {
            std::stringstream err;
            err << "File name has unrecognised extension '" << split[2] << "'";
            throw std::runtime_error(err.str());
        }

        // Process file
        fileHandlerIn->read(fileName, *this, jsonConfig);
    }

    /**
    * Write file to %Raster.
    * @param fileName name of file to read.
    * @return true on successful write.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::write(std::string fileName, std::string jsonConfig) {
    
        // Check for data
        if (!hasData()) {
            throw std::runtime_error("Raster has no data to write");
        }
    
        // Get filename extension
        auto split = Strings::splitPath(fileName);

        if (split[2] == "asc") {

            // Create handler
            fileHandlerOut = std::make_shared<AsciiHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "flt") {

            // Create handler
            fileHandlerOut = std::make_shared<FltHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "tif" || split[2] == "tiff") {

            // Create handler
            fileHandlerOut = std::make_shared<GeoTIFFHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "gsr") {

            // Create handler
            fileHandlerOut = std::make_shared<GsrHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "png") {

            // Create handler
            fileHandlerOut = std::make_shared<PngHandler<RTYPE, CTYPE> >();

        } else {
        
            std::stringstream err;
            err << "File name has unrecognised extension '" << split[2] << "'";
            throw std::runtime_error(err.str());
        }

        // Process file
        fileHandlerOut->write(fileName, *this, jsonConfig);
    }
    
    /**
    * Create script hash.
    * @param script OpenCL script to run.
    * @param kernelBlock OpenCL code block.
    * @param rasterBaseRefs list of input raster references.
    * @return script hash.
    */
    template <typename CTYPE>
    std::size_t createScriptHash(
        std::string script, 
        std::string kernelBlock, 
        RasterBaseRefs<CTYPE> rasterBaseRefs) {

        // Build library lookup
        std::string scriptLookup;
        for (int i = 0; i < rasterBaseRefs.size(); i++) {
            RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();
            scriptLookup += rasterBase.getHashString();
        }
        scriptLookup+=script;
        scriptLookup+=kernelBlock;

        // Search for program
        std::hash<std::string> hasher;
        return hasher(scriptLookup);
    }

    /**
    * Generates OpenCL kernel.
    * @param script OpenCL script to run.
    * @param kernelBlock OpenCL code block.
    * @param rasterBaseRefs list of input raster references.
    * @param rasterNullValue handling type for null values.
    * @param variables named Variables to use in the kernel.
    * @param useConst force all input variables to be const.
    * @return processed OpenCL code block, null script on error.
    */
    template <typename CTYPE>
    KernelGenerator generateKernel(
        std::string script, 
        std::string kernelBlock, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        RasterNullValue::Type rasterNullValue,
        VariablesBasePtr<std::string> variables,
        bool useConst) {

        // Create variable strings
        std::string argsList;
        std::string variableListHi;
        std::string variableListLow;
        std::string reduceHead;
        std::string reduceCode;
        std::string reducePost;
        std::string post;
        std::set<std::string> names;

        // Create generator        
        KernelGenerator kernelGenerator;
        kernelGenerator.reqs.rasterRequirements.resize(rasterBaseRefs.size());

        // Strip all single-line comments from strings
        script = std::regex_replace(script, std::regex("\\s*//.*"), std::string(""));
        kernelBlock = std::regex_replace(kernelBlock, std::regex("\\s*//.*"), std::string(""));

        // Create cache data
        std::map<std::string, std::pair<uint32_t, std::string> > cacheItems;

        // Update variable list
        std::map<std::string, std::string> variableTable;
        if (variables != nullptr && variables->hasData()) {

            // Get type
            auto vType = variables->getOpenCLTypeString();

            // Add to globals
            argsList += "__global " + vType + " *_vars,\n";

            // Loop over variables
            for (auto vi : variables->getIndexes()) {

                // Check for duplicate names
                std::string vName = vi.first;
                if (names.find(vName) != names.end()) {
                    throw std::runtime_error("raster name '" + vName + "' is not unique");
                }
                names.insert(vName);

                // Add to kernel variable list
                variableListHi += (useConst ? "const " : "") + vType + " " + 
                    vName + " = *(_vars+" + std::to_string(vi.second) + ");\n";
                if (!useConst) {
                    post += "*(_vars+" + std::to_string(vi.second) + ") = " + vName + ";\n";
                }
            }
        }

        bool needsReduction = false;

        // Parse rasters
        if (rasterBaseRefs.size() > 0) {

            bool needsNeighbours = false;
            bool hasProjection = false;
            bool needsProjectionCoordinate = false;

            // Create full script
            std::string fullScript = kernelBlock+script;

            // Add variable for valid raster count
            if (rasterNullValue != RasterNullValue::Null) {
                variableListHi += "uint __layer_count = 0;\n";
            }

            // Get handle to anchor layer
            RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
            auto dimFirst = rasterBaseFirst.getRasterDimensions();            
            auto projFirst = rasterBaseFirst.getProjectionParameters();

            // Add random buffer
            if (std::regex_search(fullScript, std::regex("\\brandom\\b")) || std::regex_search(fullScript, std::regex("\\brandomNormal\\b"))) {

                // Add random code
                kernelGenerator.reqs.usesRandom = true;

                // Add random variables
                argsList += "__global RandomState *_rs,\n";
                bool is2D = (dimFirst.d.nz == 1);
                if (is2D) {
                    variableListHi += "RandomState _rsl = _val2D_a(_rs, _i, _j);\n";
                    post += "_val2D_a(_rs, _i, _j) = _rsl;\n";
                } else {
                    variableListHi += "RandomState _rsl = _val3D_a(_rs, _i, _j, _k);\n";
                    post += "_val3D_a(_rs, _i, _j, _k) = _rsl;\n";
                }
                variableListHi += "RandomState *_prsl = &_rsl;\n";
                script = std::regex_replace(script, std::regex("\\brandom\\b"), std::string("random_REAL(_prsl)"));
                script = std::regex_replace(script, std::regex("\\brandomNormal\\("), std::string("randomNormal_REAL(_prsl, "));
            }

            // Add random buffer
            bool needsBoundary = false;
            if (std::regex_search(fullScript, std::regex("\\bisBoundary(_N|_E|_S|_W)\\b"))) {
                needsBoundary = true;
                kernelGenerator.reqs.usesBoundary = true;
            }

            // Loop over rasters
            for (int i = 0; i < rasterBaseRefs.size(); i++) {
                    
                // Get raster handle
                RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();

                // Check Raster is initialised
                if (!rasterBase.hasData()) {
                    throw std::runtime_error("Kernel cannot use uninitialised raster");
                }
                
                // Get and check name
                if (!rasterBase.hasProperty("name")) {
                    throw std::runtime_error("Raster in kernel has no name");
                }
                std::string rName = rasterBase.template getProperty<std::string>("name");                
                if (rName.empty()) {
                    throw std::runtime_error("Raster in kernel has empty name");
                }
                if (names.find(rName) != names.end()) {
                    throw std::runtime_error("raster name '" + rName + "' is not unique");
                }

                // Check for name in script
                auto regName = std::regex("\\b_?" + rName + "(_layers|_N|_E|_S|_W|_NE|_SE|_SW|_NW)?\\b");
                if (!std::regex_search(script, regName) && 
                    !std::regex_search(kernelBlock, regName) &&
                    rasterBase.getReductionType() == Reduction::None) {

                    // Skip processing
                    kernelGenerator.reqs.rasterRequirements[i].used = false;
                    continue;
                }

                // Process raster
                kernelGenerator.reqs.rasterRequirements[i].used = true;
                names.insert(rName);

                // Check projection
                auto rProj = rasterBase.getProjectionParameters();
                if (i == 0 && rProj.type != 0) {
                    hasProjection = true;
                }
                if (rProj.type == 0 && hasProjection) {
                    throw std::runtime_error("One or more raster projections is undefined");
                }

                // Check projection type
                if (hasProjection && !Projection::checkProjection(rProj)) {
                    throw std::runtime_error("Invalid projection type for Raster '" + rName + "'");
                }

                // Update variable list
                if (rasterBase.hasVariables()) {
                
                    // Get type
                    auto vType = rasterBase.getVariablesType();

                    // Add variable
                    std::string rasterVarName = "_vars_" + rName;
                    argsList += "__global " + vType + " *" + rasterVarName + ",\n";

                    // Loop over variables
                    for (auto vi : rasterBase.getVariablesIndexes()) {
                    
                        // Check for duplicate names
                        std::string vName = rName + "_" + vi.first;
                        if (names.find(vName) != names.end()) {
                            throw std::runtime_error("raster name '" + vName + "' is not unique");
                        }
                        names.insert(vName);

                        // Add to kernel variable list
                        variableListHi += (useConst ? "const " : "") + vType + " " + vName + 
                            " = *(" + rasterVarName + "+" + std::to_string(vi.second) + ");\n";
                        if (!useConst) {
                            post += "*(" + rasterVarName + "+" + std::to_string(vi.second) + ") = " + vName + ";\n";
                        }

                        // Add variable to lookup table
                        variableTable[rName + "::" + vi.first] = vName;
                    }
                }

                // Get raster information
                auto rType = rasterBase.getOpenCLTypeString();
                auto rasterDim = rasterBase.getRasterDimensions();
                bool is2D = (rasterDim.d.nz == 1);
            
                // Check for aligned layers these are be optimised for access
                if (equalSpatialMetrics2D(rasterDim.d, dimFirst.d)) {

                    // Update parameter list
                    argsList += "__global " + rType + " *_" + rName + ",\n";

                    // Update script for raster value
                    if (rasterBase.getNeedsRead()) {
                        if (is2D) {
                            variableListHi += rType + " " + rName + " = _val2D_a(_" + rName + ", _i, _j);\n";
                        } else {
                    
                            if (rasterDim.d.nz == dimFirst.d.nz) {

                                // Use aligned value
                                variableListHi += rType + " " + rName + " = _val3D_a(_" + rName + ", _i, _j, _k);\n";

                            } else {
                                
                                // Check for 3D references
                                if (!script.empty() && !std::regex_search(script, std::regex("\\b" + rName + "\\s*\\["))) {
                                    throw std::runtime_error("3D raster '" + rName + "' has no layer specified when referenced by 2D raster");
                                }
                            }

                            // Use square brackets for k-index
                            if (!script.empty()) {

                                if (dimFirst.d.nz == 1) {

                                    // Allow writes to 3D layers if anchor layer is 2D                                
                                    script = std::regex_replace(script, std::regex("\\b" + rName + "\\s*\\[\\s*(.+?)\\s*\\]"), 
                                        std::string("_val3D_a(_") + rName + ", _i, _j, min_uint_DEF(($1), " + std::to_string(rasterDim.d.nz-1) + "))");

                                } else {

                                    // Otherwise disable writes to 3D layers
                                    script = std::regex_replace(script, std::regex("\\b" + rName + "\\s*\\[\\s*(.+?)\\s*\\]"), 
                                        std::string("getZValue" + rType + "(_") + rName + ", _i, _j, ($1), _dim.mx, _dim.my, " + std::to_string(rasterDim.d.nz) + ")");
                                }
                            }

                            // Add range variable                        
                            variableListHi += "const uint " + rName + "_layers = " + std::to_string(rasterDim.d.nz) +";\n";
                        }
                    } else {
                        variableListHi += rType + " " + rName + " = noData_" + rType + ";\n";
                    }

                    // Check for neighbours
                    std::vector<std::string> neighbourSuffix = { "N", "NE", "E", "SE", "S", "SW", "W", "NW" };
                    uint8_t requiredNeighbours = rasterBase.getRequiredNeighbours();
                    if (std::regex_search(fullScript, std::regex("\\b" + rName + "(_N|_E|_S|_W|_NE|_SE|_SW|_NW)\\b"))) {
                        for (uint32_t i = 0; i < 8; i++) {
                            if (std::regex_search(fullScript, std::regex("\\b" + rName + "(_" + neighbourSuffix[i] + ")\\b"))) {
                                requiredNeighbours |= (uint8_t)(1 << i);
                            }
                        }
                    }

                    // If diagonal neighbours are required, adjacent neighbours must also be added
                    if (requiredNeighbours & Neighbours::NE) {
                        requiredNeighbours |= Neighbours::N;
                        requiredNeighbours |= Neighbours::E;
                    }
                    if (requiredNeighbours & Neighbours::SE) {
                        requiredNeighbours |= Neighbours::S;
                        requiredNeighbours |= Neighbours::E;
                    }
                    if (requiredNeighbours & Neighbours::SW) {
                        requiredNeighbours |= Neighbours::S;
                        requiredNeighbours |= Neighbours::W;
                    }
                    if (requiredNeighbours & Neighbours::NW) {
                        requiredNeighbours |= Neighbours::N;
                        requiredNeighbours |= Neighbours::W;
                    }

                    // Check for operators
                    auto rGrad = std::regex("\\bgrad\\(\\s*" + rName + "\\s*\\)");
                    if (std::regex_search(script, rGrad)) {
                        script = std::regex_replace(script, rGrad, std::string("_grad_a(") + rName + ")");
                        requiredNeighbours = Neighbours::Queen;
                    } else if (std::regex_search(script, std::regex("\\b" + rName + "\\b\\("))) {

                        // General offset operator
                        requiredNeighbours = Neighbours::Queen;
                        if (is2D) {
                            script = std::regex_replace(script, std::regex("\\b" + rName + "\\b\\("), 
                                std::string("_getValue2D(_") + rName + ","
                                "_" + rName + "_N, " +
                                "_" + rName + "_E, " +
                                "_" + rName + "_S, " +
                                "_" + rName + "_W, " +
                                "_" + rName + "_NE, " +
                                "_" + rName + "_SE, " +
                                "_" + rName + "_SW, " +
                                "_" + rName + "_NW, " +
                                "_dim, _i, _j, ");
                        }
                    }

                    // Update raster list
                    kernelGenerator.reqs.rasterRequirements[i].requiredNeighbours = requiredNeighbours;
                                
                    // Add neighbours to arguments
                    for (uint32_t i = 0; i < 8; i++) {
                        if (requiredNeighbours & (uint8_t)(1 << i)) {
                            argsList += "__global " + rType + " *_" + rName + "_" + neighbourSuffix[i] + ",\n";
                            if (is2D) {
                                variableListHi += rType + " " + rName + "_" + neighbourSuffix[i] + " = _val2D_a_" + neighbourSuffix[i] + "(_" + rName + ", _i, _j);\n";
                            } else {
                                variableListHi += rType + " " + rName + "_" + neighbourSuffix[i] + " = _val3D_a_" + neighbourSuffix[i] + "(_" + rName + ", _i, _j, _k);\n";
                            }
                            needsNeighbours = true;
                        }
                    }

                    // Check whether reductions are required on raster
                    auto reduction = rasterBase.getReductionType();
                    if (reduction != Reduction::None) {

                        // Update reduction parameter list
                        argsList += "__global " + rType + " *_" + rName + "_reduce,\n";
                        std::string reduceVar = "_" + rName + "_cache+_lid";

                        // Set reduction function
                        if (reduction == Reduction::Maximum) {
                            post += "*(" + reduceVar + ") = " + rName + ";\n";
                            reduceHead += "__local " + rType + " _" + rName + "_cache[" + std::to_string(TileMetrics::tileReduceSize) + "];\n";
                            reduceHead += "*(" + reduceVar + ") = noData_" + rType + ";\n";
                            reduceCode += "if (isValid_" + rType + "(*(" + reduceVar + "+_step))) { *(" + reduceVar + ") = fmax(*(" + reduceVar + "), *(" + reduceVar + "+_step)); }\n";
                            reducePost += "*(_" + rName + "_reduce+gid) = *_" + rName + "_cache;\n";
                        }
                    
                        if (reduction == Reduction::Minimum) {
                            post += "*(" + reduceVar + ") = " + rName + ";\n";
                            reduceHead += "__local " + rType + " _" + rName + "_cache[" + std::to_string(TileMetrics::tileReduceSize) + "];\n";
                            reduceHead += "*(" + reduceVar + ") = noData_" + rType + ";\n";
                            reduceCode += "if (isValid_" + rType + "(*(" + reduceVar + "+_step))) { *(" + reduceVar + ") = fmin(*(" + reduceVar + "), *(" + reduceVar + "+_step)); }\n";
                            reducePost += "*(_" + rName + "_reduce+gid) = *_" + rName + "_cache;\n";
                        }
                    
                        if (reduction == Reduction::Sum || reduction == Reduction::SumSquares || reduction == Reduction::Mean) {
                            if (reduction == Reduction::SumSquares) {
                                post += "*(" + reduceVar + ") = isValid_" + rType + "(" + rName + ") ? " + rName + "*" + rName + " : 0.0;\n";
                            } else {
                                post += "*(" + reduceVar + ") = isValid_" + rType + "(" + rName + ") ? " + rName + " : 0.0;\n";
                            }
                            reduceHead += "__local " + rType + " _" + rName + "_cache[" + std::to_string(TileMetrics::tileReduceSize) + "];\n";
                            reduceHead += "*(" + reduceVar + ") = noData_" + rType + ";\n";
                            reduceCode += "if (isValid_" + rType + "(*(" + reduceVar + "+_step))) { *(" + reduceVar + ") += *(" + reduceVar + "+_step); }\n";
                            reducePost += "*(_" + rName + "_reduce+gid) = *_" + rName + "_cache;\n";
                        }
                    
                        if (reduction == Reduction::Count || reduction == Reduction::Mean) {    
                            post += "if (isValid_" + rType + "(" + rName + ")) { atomic_inc(&_" + rName + "_count); } \n";                
                            reduceHead += "__local uint _" + rName + "_count; if (_lid == 0) { _" + rName + "_count = 0; } \n";
                            reducePost += "atomic_add(_" + rName + "_status, _" + rName + "_count);\n";                            
                        }

                        // Set flag to enable reduction code
                        needsReduction = true;
                    }

                    // Check whether status bits are required on raster
                    if (rasterBase.getNeedsStatus() || 
                        reduction == Reduction::Count ||
                        reduction == Reduction::Mean) {

                        // Update reduction parameter list
                        argsList += "__global uint *_" + rName + "_status,\n";
                    }

                    if (rasterBase.getNeedsWrite()) {

                        // Update script for writing aligned layers
                        if (is2D) {
                            post += "_val2D_a(_" + rName + ", _i, _j) = " + rName + ";\n";
                        } else if (rasterDim.d.nz == dimFirst.d.nz) {
                            post += "_val3D_a(_" + rName + ", _i, _j, _k) = " + rName + ";\n";
                        }
                    }

                } else {

                    // Check for operators
                    auto rGrad = std::regex("\\bgrad\\(\\s*" + rName + "\\s*\\)");
                    if (std::regex_search(script, rGrad)) {
                        script = std::regex_replace(script, rGrad, std::string("_grad(") + rName + ")");
                    }

                    // Update parameter list
                    argsList += "__global " + rType + " *_" + rName + ",\n";
                    argsList += (useConst ? "const Dimensions _dim_" : "Dimensions _dim_") + rName + ",\n";

                    // Update script for raster value                
                    if (rasterBase.getNeedsRead()) {
                        if (projFirst == rProj) {

                            // Use same projection
                            auto interpolation = rasterBase.getInterpolationType();
                            if (interpolation == RasterInterpolation::Nearest || rType == "UINT") {

                                // Nearest neighbour interpolation 
                                if (is2D) {
                                    variableListHi += rType + " " + rName + " = getNearestValue2D" + rType + 
                                        "(_" + rName + ", x, y, _dim_" + rName + ");\n";
                                } else {
                                    variableListHi += rType + " " + rName + " = getNearestValue3D" + rType + 
                                        "(_" + rName + ", x, y, z, _dim_" + rName + ");\n";
                                }
                            } else if (interpolation == RasterInterpolation::Bilinear) {

                                // Bilinear interpolation 
                                if (is2D) {
                                    variableListHi += rType + " " + rName + " = getBilinearValue2D" + rType + 
                                        "(_" + rName + ", x, y, _dim_" + rName + ");\n";
                                } else {
                                    variableListHi += rType + " " + rName + " = getBilinearValue3D" + rType + 
                                        "(_" + rName + ", x, y, z, _dim_" + rName + ");\n";
                                }
                            } else {

                                // Error
                                std::string err = std::string("Invalid interpolation rType '") + std::to_string(interpolation) + "'";
                                throw std::runtime_error(err);
                            }

                        } else {

                            // Add projection information
                            if (projFirst.type == 1)
                                kernelGenerator.projectionTypes.insert(projFirst.cttype);
                            if (rProj.type == 1)
                                kernelGenerator.projectionTypes.insert(rProj.cttype);
                            needsProjectionCoordinate = true;
                                
                            // Add projection code
                            argsList += (useConst ? "const ProjectionParameters _proj_" : "ProjectionParameters _proj_") + rName + ",\n";
                            auto interpolation = rasterBase.getInterpolationType();
                            if (interpolation == RasterInterpolation::Nearest || rType == "UINT") {
                                if (is2D) {
                                    variableListHi += rType + " " + rName + " = getNearestProjectedValue2D" + rType + 
                                        "(_" + rName + ", x, y, _dim_" + rName + ", _proj_" + rName + ", _proj);\n";
                                } else {
                                    variableListHi += rType + " " + rName + " = getNearestProjectedValue3D" + rType + 
                                        "(_" + rName + ", x, y, z, _dim_" + rName + ", _proj_" + rName + ", _proj);\n";
                                }
                    
                            } else if (interpolation == RasterInterpolation::Bilinear) {

                                // Bilinear interpolation 
                                if (is2D) {
                                    variableListHi += rType + " " + rName + " = getBilinearProjectedValue2D" + rType + 
                                        "(_" + rName + ", x, y, _dim_" + rName + ", _proj_" + rName + ", _proj);\n";
                                } else {
                                    variableListHi += rType + " " + rName + " = getBilinearProjectedValue3D" + rType + 
                                        "(_" + rName + ", x, y, z, _dim_" + rName + ", _proj_" + rName + ", _proj);\n";
                                }
                            } else {

                                // Error
                                std::string err = std::string("Invalid interpolation type '") + std::to_string(interpolation) + "'";
                                throw std::runtime_error(err);
                            }
                        }
                    } else {
                        variableListHi += rType + " " + rName + " = noData_" + rType + ";\n";
                    }
                }

                if (rasterNullValue != RasterNullValue::Null) {

                    // Update script count for non-null values
                    variableListHi += "if (isValid_" + rType + "(" + rName + ")) { __layer_count++; }\n";

                    // Update script processing for fill values
                    if (rasterNullValue == RasterNullValue::Zero)
                        variableListLow += "if (__layer_count > 0 && !isValid_" + rType + "(" + rName + ")) " + rName + " = ("+ rType +")0;\n";
                    else if (rasterNullValue == RasterNullValue::One)
                        variableListLow += "if (__layer_count > 0 && !isValid_" + rType + "(" + rName + ")) " + rName + " = ("+ rType +")1;\n";
                }

            }                     
            
            // Add temporary coordinate for any reprojection operations
            if (needsProjectionCoordinate) {
                variableListHi = "Coordinate _tc = {0.0, 0.0, 0.0, 0.0};\n" + variableListHi;
            }

            // Add neighbour memory boundary variable
            if (needsNeighbours) {
                std::string boundaryScript;
                boundaryScript += "const bool _boundary_W = (_i == 0);\n";
                boundaryScript += "const bool _boundary_S = (_j == 0);\n";
                boundaryScript += "const bool _boundary_E = (_i == _dim.mx-1);\n";
                boundaryScript += "const bool _boundary_N = (_j == _dim.my-1);\n";
                variableListHi = boundaryScript + variableListHi;
            }

            // Add variable for valid raster count
            if (rasterNullValue != RasterNullValue::Null) {
                variableListLow += "const uint _layer_count = __layer_count;";
            }
        
            // Add additional parameters
            if (!kernelGenerator.projectionTypes.empty()) {
                argsList += (useConst ? "const ProjectionParameters _proj,\n" : "ProjectionParameters _proj,\n");
            }
            argsList += (useConst ? "const Dimensions _dim,\n" : "Dimensions _dim,\n");

            // Add boundary flags
            if (needsBoundary) {
                std::string boundaryScript;
                boundaryScript += "const bool isBoundary_W = ((_i == 0) && (_isBoundaryTile&(uchar)0x01));\n";
                boundaryScript += "const bool isBoundary_S = ((_j == 0) && (_isBoundaryTile&(uchar)0x02));\n";
                boundaryScript += "const bool isBoundary_E = ((_i == _dim.nx-1) && (_isBoundaryTile&(uchar)0x04));\n";
                boundaryScript += "const bool isBoundary_N = ((_j == _dim.ny-1) && (_isBoundaryTile&(uchar)0x08));\n";
                variableListHi = variableListHi + boundaryScript;
                argsList += "uchar _isBoundaryTile,\n";
            }
        }

        // Remove trailing comma and newline
        argsList.pop_back();
        argsList.pop_back();
                    
        // Load kernel codes
        kernelGenerator.script = kernelBlock;
        
        // Check if cache is required
        if (cacheItems.size() != 0) {
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__CACHE__|__CACHE__\\*\\/"), std::string(""));

            std::string cacheHead;
            std::string cacheVars;
            for (auto &cacheItem : cacheItems) {
                
                cacheHead += "__local " + cacheItem.first + " _cache_" + cacheItem.first + "[" + std::to_string(cacheItem.second.first) + "];\n";
                cacheVars += cacheItem.second.second;
            }
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("__CACHE_HEAD__"), cacheHead);
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("__CACHE_VARS__"), cacheVars);
        }

        // Check if reduction is required
        if (needsReduction) {
            
            // Enable reduction code
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__REDUCE__|__REDUCE__\\*\\/"), std::string(""));
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("__REDUCE_HEAD__"), reduceHead);
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("__REDUCE_CODE__"), reduceCode);
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("__REDUCE_POST__"), reducePost);
        }

        // Patch kernel template with code
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__VARS__\\*\\/"), variableListHi + variableListLow);
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__POST__\\*\\/"), post);

        // Patch kernel template with script
        if (!script.empty()) {
            script = Solver::processScript(script);
        }
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__CODE__\\*\\/"), script);

        // Find all variables in string
        std::smatch variableMatch;
        std::string::const_iterator searchStart(kernelGenerator.script.cbegin());
        std::set<std::string> replacementVariables;
        while (std::regex_search(searchStart, kernelGenerator.script.cend(), variableMatch, std::regex("\\w+:{2}\\w+"))) {

            // Check if variable is valid
            if (variableTable.find(variableMatch[0]) == variableTable.end()) {
                std::string err = std::string("Invalid variable name '") + variableMatch[0].str() + "'";
                throw std::runtime_error(err);
            }

            // Add to replacement set
            replacementVariables.insert(variableMatch[0]);

            // Update search position
            searchStart = variableMatch.suffix().first;
        }

        // Patch kernel script with variable names
        for (auto &replacement : replacementVariables) {
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, 
                std::regex("\\b" + replacement + "\\b"), variableTable[replacement]);
        }

        // Replace spatial indexes
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\bipos\\b"), std::string("_i"));
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\bjpos\\b"), std::string("_j"));
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\bkpos\\b"), std::string("_k"));

        //// Output script
        //std::cout << kernelGenerator.script << std::endl; // TEST

        // Return processed script
        return kernelGenerator;
    }

    /**
    * Generates OpenCL kernel.
    * @param kernelGenerator processed OpenCL code block.
    * @return hash of program string
    */
    std::size_t buildRasterKernel(KernelGenerator &kernelGenerator) {
        
        // Get solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();

        // Create hash
        std::size_t clHash = solver.getNullHash();

        try {

            // Load kernel codes
            std::string kernelStr = Models::cl_raster_head_c;
            if (!kernelGenerator.projectionTypes.empty()) {
            
                // Update projection definitions
                auto projStr = Models::cl_projection_head_c;
                for (auto pid : kernelGenerator.projectionTypes) {
                    std::string projDef = "USE_PROJ_TYPE_" + std::to_string(pid);
                    projStr = std::regex_replace(projStr, std::regex(std::string("__") + projDef), projDef);
                }
                kernelStr += projStr;
            }

            if (kernelGenerator.reqs.usesRandom) {
                kernelStr += Models::cl_random_c;
            }

            kernelStr += kernelGenerator.script;
                        
            // Build program
            clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {

                // Output kernel code
                //std::cout << "ERROR: Cannot build program" << std::endl;
                //std::cout << kernelStr << std::endl;

                throw std::runtime_error("Cannot build program");
            }

            // Set generator hash
            kernelGenerator.programHash = clHash;

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        } catch(std::regex_error e) {
            std::stringstream err;
            err << "Regex exception '" << e.what();
            throw std::runtime_error(err.str());
        }
        return clHash;
    }
    
    /**
    * Sets kernel arguments for a %Raster %Tile based on generated script
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param kernel OpenCL kernel to run.
    * @param rasterBaseRefs list of input raster references.
    * @param startArg the index of the first argument to set.
    * @param variables pointer to a general %Variables object for the kernel.
    * @return true if kernel arguments are correctly set, false otherwise
    */
    template <typename CTYPE>
    bool setTileKernelArguments(
        uint32_t ti, 
        uint32_t tj, 
        cl::Kernel &kernel, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        KernelRequirements reqs, 
        int startArg,
        VariablesBasePtr<std::string> variables,
        bool useDimensions) {

        // Check references against requirements
        if (rasterBaseRefs.size() != reqs.rasterRequirements.size()) {
            throw std::runtime_error("Kernel rasters and requirements are different lengths");
        }

        try {
            
            // Get OpenCL solver handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();

            if (solver.openCLInitialised()) {

                // Populate variable argument
                int arg = startArg;
                if (variables != nullptr && variables->hasData()) {
                    kernel.setArg(arg++, variables->getBuffer());
                }

                if (rasterBaseRefs.size() > 0) {

                    // Get handle to anchor layer
                    RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
                    auto dimFirst = rasterBaseFirst.getRasterDimensions();            
                    auto projFirst = rasterBaseFirst.getProjectionParameters();

                    // Add random buffer
                    if (reqs.usesRandom) {
                        kernel.setArg(arg++, rasterBaseFirst.getRandomBuffer());
                    }

                    // Get tile dimensions
                    TileDimensions<CTYPE> tDimFirst = rasterBaseFirst.getTileDimensions(ti, tj);

                    // Populate kernel arguments
                    bool needsProjection = false;
                    for (int i = 0; i < rasterBaseRefs.size(); i++) {

                        // Skip unused Rasters
                        if (!reqs.rasterRequirements[i].used) {
                            continue;
                        }

                        // Get Raster handle
                        auto &rasterBase = rasterBaseRefs[i].get();
                        
                        // Reset variables for reduction operations
                        auto reduction = rasterBase.getReductionType();
                        if (reduction != Reduction::None) {
                            rasterBase.resetTileReduction(ti, tj);
                            if (reduction == Reduction::Count || reduction == Reduction::Mean)
                                rasterBase.setTileStatus(ti, tj, 0);
                        }

                        // Update variable list
                        if (rasterBase.hasVariables()) {
                            kernel.setArg(arg++, rasterBase.getVariablesBuffer());
                        }
                    
                        // Check if raster has equal dimensions to this raster
                        if (equalSpatialMetrics2D(rasterBase.getRasterDimensions().d, dimFirst.d)) {

                            // Get required neighbours
                            auto requiredNeighbours = reqs.rasterRequirements[i].requiredNeighbours;

                            // Add buffer and dimensions to kernel
                            kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti, tDimFirst.tj));

                            // Check and add neighbours
                            if (requiredNeighbours & Neighbours::N) {
                                if (tDimFirst.tj < dimFirst.ty-1) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti, tDimFirst.tj+1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }
                            
                            if (requiredNeighbours & Neighbours::NE) {
                                if (tDimFirst.ti < dimFirst.tx-1 && tDimFirst.tj < dimFirst.ty-1) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti+1, tDimFirst.tj+1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            if (requiredNeighbours & Neighbours::E) {
                                if (tDimFirst.ti < dimFirst.tx-1) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti+1, tDimFirst.tj));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }
                            
                            if (requiredNeighbours & Neighbours::SE) {
                                if (tDimFirst.ti < dimFirst.tx-1 && tDimFirst.tj > 0) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti+1, tDimFirst.tj-1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            if (requiredNeighbours & Neighbours::S) {
                                if (tDimFirst.tj > 0) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti, tDimFirst.tj-1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            if (requiredNeighbours & Neighbours::SW) {
                                if (tDimFirst.ti > 0 && tDimFirst.tj > 0) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti-1, tDimFirst.tj-1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            if (requiredNeighbours & Neighbours::W) {
                                if (tDimFirst.ti > 0) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti-1, tDimFirst.tj));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            if (requiredNeighbours & Neighbours::NW) {
                                if (tDimFirst.ti > 0 && tDimFirst.tj < dimFirst.ty-1) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti-1, tDimFirst.tj+1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            // Check whether reductions are required on raster
                            if (reduction != Reduction::None) {
                        
                                // Add reduction buffer
                                kernel.setArg(arg++, rasterBase.getTileReduceBuffer(tDimFirst.ti, tDimFirst.tj));
                            }

                            // Check whether status bits are required on raster
                            if (rasterBase.getNeedsStatus() || 
                                reduction == Reduction::Count ||
                                reduction == Reduction::Mean) {

                                // Add status buffer
                                kernel.setArg(arg++, rasterBase.getTileStatusBuffer(tDimFirst.ti, tDimFirst.tj));
                            }

                        } else {

                            // Get raster projection
                            auto rasterProj = rasterBase.getProjectionParameters();
                            bool differentProjections = (projFirst != rasterProj);

                            // Get bounds and reproject if necessary
                            auto tileBounds = rasterBaseFirst.getTileBounds(ti, tj);
                            if (differentProjections) {

                                // Reproject bounds
                                tileBounds = tileBounds.convert(rasterProj, projFirst);
                                needsProjection = true;
                            }
                        
                            // Get raster region intersecting current tile
                            RasterDimensions<CTYPE> rasterDim;
                            cl::Buffer kernelBuffer;
                            if (rasterBase.createRegionBuffer(tileBounds, kernelBuffer, rasterDim)) {
                        
                                // Add buffer and dimensions to kernel
                                kernel.setArg(arg++, kernelBuffer);
                                kernel.setArg(arg++, rasterDim.d);
                                if (differentProjections)
                                    kernel.setArg(arg++, ProjectionParameters<CTYPE>(rasterProj));

                            } else {

                                // Add null buffer and dimensions to kernel, this will be detected in the kernel
                                kernel.setArg(arg++, rasterBase.getNullBuffer());
                                kernel.setArg(arg++, Dimensions<CTYPE>());
                                if (differentProjections)
                                    kernel.setArg(arg++, ProjectionParameters<CTYPE>()); // TODO
                            }
                        }
                    }
                    
                    if (needsProjection)
                        kernel.setArg(arg++, ProjectionParameters<CTYPE>(projFirst));
                    if (useDimensions)
                        kernel.setArg(arg++, tDimFirst.d);

                    // Add boundary flag
                    if (reqs.usesBoundary) {
                        kernel.setArg(arg++, (cl_uchar)(
                            (cl_uchar)(tDimFirst.ti == 0) | 
                            ((cl_uchar)(tDimFirst.tj == 0)<<1) | 
                            ((cl_uchar)(tDimFirst.ti == dimFirst.tx-1)<<2) |
                            ((cl_uchar)(tDimFirst.tj == dimFirst.ty-1)<<3)));
                    }

                    return true;
                }

                // Return false if rasterBaseRefs is empty
                return false;
            }
        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
        return false;
    }

    /**
    * Runs a kernel on the %Tile at index ti and tj in the first %Raster 
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param kernel OpenCL kernel to run.
    * @param rasterBaseRefs list of input raster references.
    * @param startArg the index of the first argument to set.
    * @param variables pointer to a general %Variables object for the kernel.
    * @return true if kernel executes, false otherwise.
    */
    template <typename CTYPE>
    bool runTileKernel(
        uint32_t ti, 
        uint32_t tj, 
        cl::Kernel &kernel, 
        RasterBaseRefs<CTYPE> rasterBaseRefs,
        KernelRequirements reqs,
        int startArg,
        VariablesBasePtr<std::string> variables,
        size_t debug) {

        // Check references against requirements
        if (rasterBaseRefs.size() != reqs.rasterRequirements.size()) {
            throw std::runtime_error("Kernel rasters and requirements are different lengths");
        }
    
        try {
            
            // Get OpenCL solver handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();

            if (solver.openCLInitialised()) {

                if (rasterBaseRefs.size() > 0) {

                    // Set arguments
                    if (!setTileKernelArguments<CTYPE>(ti, tj, kernel, rasterBaseRefs, reqs, startArg, variables))
                        return false;

                    // Get handle to anchor layer
                    RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();

                    // Get tile dimensions
                    TileDimensions<CTYPE> tDimFirst = rasterBaseFirst.getTileDimensions(ti, tj);

                    // Execute kernel
                    if (debug == RasterDebug::Enable) {

                        // Run over 1 cell only
                        queue.enqueueNDRangeKernel(kernel, 
                            cl::NullRange, 
                            cl::NDRange(1, 1, tDimFirst.d.nz), 
                            cl::NDRange(1, 1, 1));
                    } else {

                        // Run over range
                        queue.enqueueNDRangeKernel(kernel, 
                            cl::NullRange, 
                            cl::NDRange(TileMetrics::tileSize, tDimFirst.d.ny, tDimFirst.d.nz), 
                            cl::NDRange(TileMetrics::tileSize, 1, 1));
                    }

                    // Flush queue
                    //queue.flush();

                    return true;
                }

                // Return false if rasterBaseRefs is empty
                return false;
            }
        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
        return false;
    }

    /**
    * Run script on %Raster
    * @param script OpenCL script to run.
    * @param rasterBaseRefs list of input raster references.
    * @param parameters parameter flags for combination and aliasing.
    */
    template <typename CTYPE>
    void runScriptNoOut(
        std::string script, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        size_t parameters) {

        // Check for rasters
        if (rasterBaseRefs.size() == 0) {
            throw std::runtime_error("No rasters for script");
        }
            
        // Get OpenCL handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {

            // Get paramaters
            RasterDebug::Type rasterDebug = static_cast<RasterDebug::Type>(parameters & 0x3000);

            // Get generator
            auto scriptHash = createScriptHash(script, Models::cl_raster_block_c, rasterBaseRefs);
            auto kernelGenerator = KernelCache::getKernelCache().getKernelGenerator(scriptHash);
            if (kernelGenerator.programHash == Solver::getNullHash()) {

                // Check for underscores in script, these are reserved for internal variables
                if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                    throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                }

                // Check if script has an output variable
                if (std::regex_search(script, std::regex("\\boutput\\b"))) {  
                    throw std::runtime_error("Script cannot contain 'output' definition");   
                }

                // Generate kernel
                RasterNullValue::Type rasterNullValue = static_cast<RasterNullValue::Type>(parameters & 0xC0);
                kernelGenerator = generateKernel<CTYPE>(script, Models::cl_raster_block_c, 
                    rasterBaseRefs, rasterNullValue, nullptr, rasterDebug != RasterDebug::Enable);
                    
                // Check script
                if (kernelGenerator.script.size() == 0) {
                    throw std::runtime_error("Cannot generate script");
                }

                // Build script
                if (buildRasterKernel(kernelGenerator) == solver.getNullHash()) {
                    throw std::runtime_error("Cannot get kernel for script");
                }

                // Register kernel in cache
                KernelCache::getKernelCache().setKernelGenerator(scriptHash, kernelGenerator);
            }

            // Get kernel
            auto &rasterKernel = solver.getKernel(kernelGenerator.programHash, "raster"); 

            // Loop through tiles
            uint8_t verbose = solver.getVerboseLevel();
            auto dimFirst = rasterBaseRefs[0].get().getRasterDimensions();
            if (rasterDebug == RasterDebug::Enable) {

                // Run over single tile only
                if (verbose <= Geostack::Verbosity::Info) {
                    std::cout << "Debug mode" << std::endl;
                }
                runTileKernel<CTYPE>(0, 0, rasterKernel, rasterBaseRefs, kernelGenerator.reqs, 0, nullptr, rasterDebug);

            } else {

                // Run over all tiles
                for (uint32_t tj = 0; tj < dimFirst.ty; tj++) {
                    for (uint32_t ti = 0; ti < dimFirst.tx; ti++) {
                    
                        // Run kernel
                        runTileKernel<CTYPE>(ti, tj, rasterKernel, rasterBaseRefs, kernelGenerator.reqs, 0, nullptr, rasterDebug);
                    }

                    // Output progress
                    if (verbose <= Geostack::Verbosity::Info && (tj%10) == 0) {
                        std::cout << std::setprecision(2) << 100.0*(double)tj/(double)dimFirst.ty << "%" << std::endl;
                    }
                }
                if (verbose <= Geostack::Verbosity::Info) {
                    std::cout << "100%" << std::endl;
                }
            }

        } else {
            throw std::runtime_error("OpenCL not initialised");
        }
    }

    /**
    * Run script on %Raster
    * @param script OpenCL script to run.
    * @param rasterBaseRefs list of input raster references.
    * @param parameters parameter flags for combination and aliasing.
    * @return output %Raster
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runScript(
        std::string script, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        size_t parameters) {

        // Check raster list is populated
        if (rasterBaseRefs.size() == 0) {
            throw std::runtime_error("Scripts must use one or more Rasters");
        }

        // Get OpenCL handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {

            // Run simplified script if there is no output specified
            if (!std::regex_search(script, std::regex("\\boutput\\b"))) {
                runScriptNoOut<CTYPE>(script, rasterBaseRefs, parameters);
                return Raster<RTYPE, CTYPE>();
            }

            // Get parameters
            RasterCombination::Type rasterCombination = static_cast<RasterCombination::Type>(parameters & 0x03);
            RasterResolution::Type rasterResolution = static_cast<RasterResolution::Type>(parameters & 0x0C);
            RasterNullValue::Type rasterNullValue = static_cast<RasterNullValue::Type>(parameters & 0xC0);
            Reduction::Type rasterOutputReduction = static_cast<Reduction::Type>(parameters & 0xF00);
            RasterDebug::Type rasterDebug = static_cast<RasterDebug::Type>(parameters & 0x3000);

            // Initialise bounds of output
            RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
            auto dimFirst = rasterBaseFirst.getRasterDimensions();
            auto projFirst = rasterBaseFirst.getProjectionParameters();
            uint32_t nx = dimFirst.d.nx;
            uint32_t ny = dimFirst.d.ny;
            uint32_t nz = dimFirst.d.nz;
            CTYPE hx = dimFirst.d.hx;
            CTYPE hy = dimFirst.d.hy;
            CTYPE hz = dimFirst.d.hz;
            CTYPE xMin = dimFirst.d.ox;
            CTYPE yMin = dimFirst.d.oy;
            CTYPE zMin = dimFirst.d.oz;
            CTYPE xMax = dimFirst.ex;
            CTYPE yMax = dimFirst.ey;
            CTYPE zMax = dimFirst.ez;

            // Check rasters all have a projection, or none have a projection
            bool hasProjection = projFirst.type != 0;

            // Check projection type
            if (hasProjection && !Projection::checkProjection(projFirst)) {
                throw std::runtime_error("Invalid projection type");
            }
                    
            // Parse rasters
            for (int i = 1; i < rasterBaseRefs.size(); i++) {     
                    
                // Get raster handle
                RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();
                auto dim = rasterBase.getRasterDimensions();
                auto proj = rasterBase.getProjectionParameters();

                // Check raster
                if (!rasterBase.hasData()) {
                    std::stringstream err;
                    err << "Raster index '" << i << "' in script has no data";
                    throw std::runtime_error(err.str());
                }

                // Check projection
                if (proj.type == 0 && hasProjection) {
                    throw std::runtime_error("One or more raster projections is undefined");
                }

                // Check projection type
                if (hasProjection && !Projection::checkProjection(proj)) {
                    throw std::runtime_error("Invalid projection type");
                }

                // Get bounds
                auto bounds = rasterBase.getBounds();
                auto rhx = dim.d.hx;
                auto rhy = dim.d.hy;
                auto rhz = dim.d.hz;
                if (projFirst != proj) {
                
                    // Convert distance
                    auto centroid = bounds.centroid();
                    auto offsetCentroid = centroid;
                    offsetCentroid.p += rhx;
                    offsetCentroid.q += rhy;
                    Projection::convert(centroid, projFirst, proj);
                    Projection::convert(offsetCentroid, projFirst, proj);
                    auto offset = centroid-offsetCentroid;
                    CTYPE rh = 0.5*(fabs(offset.p)+fabs(offset.q));
                    rhx = rh;
                    rhy = rh;

                    // Re-project and process bounds
                    bounds = bounds.convert(projFirst, proj);
                }

                // Process resolution
                hx = (rasterResolution == RasterResolution::Maximum) ? std::max(hx, rhx) : std::min(hx, rhx);
                hy = (rasterResolution == RasterResolution::Maximum) ? std::max(hy, rhy) : std::min(hy, rhy);
                hz = (rasterResolution == RasterResolution::Maximum) ? std::max(hz, rhz) : std::min(hz, rhz);

                // Process bounds
                xMin = (rasterCombination == RasterCombination::Union) ? std::min(xMin, bounds.min.p) : std::max(xMin, bounds.min.p);
                yMin = (rasterCombination == RasterCombination::Union) ? std::min(yMin, bounds.min.q) : std::max(yMin, bounds.min.q);
                zMin = (rasterCombination == RasterCombination::Union) ? std::min(zMin, bounds.min.r) : std::max(zMin, bounds.min.r);
                xMax = (rasterCombination == RasterCombination::Union) ? std::max(xMax, bounds.max.p) : std::min(xMax, bounds.max.p);
                yMax = (rasterCombination == RasterCombination::Union) ? std::max(yMax, bounds.max.q) : std::min(yMax, bounds.max.q);
                zMax = (rasterCombination == RasterCombination::Union) ? std::max(zMax, bounds.max.r) : std::min(zMax, bounds.max.r);
            }

            // Check difference from base dimensions
            if (xMin != dimFirst.d.ox || yMin != dimFirst.d.oy || zMin != dimFirst.d.oz || 
                xMax != dimFirst.ex || yMax != dimFirst.ey || zMax != dimFirst.ez || 
                hx != dimFirst.d.hx || hy != dimFirst.d.hy || hz != dimFirst.d.hz) {

                // Calculate extent
                CTYPE lx = xMax-xMin;
                CTYPE ly = yMax-yMin;
                CTYPE lz = zMax-zMin;
                if (lx <= 0.0 || ly <= 0.0 || lz <= 0.0) {
                    throw std::runtime_error("Output raster size from script is zero");
                }

                // Calculate number of cells
                nx = (uint32_t)std::ceil(lx/hx);
                ny = (uint32_t)std::ceil(ly/hy);
                nz = (uint32_t)std::ceil(lz/hz);
            }

            // Create output
            Raster<RTYPE, CTYPE> rasterOut;
            rasterOut.init(nx, ny, nz, hx, hy, hz, xMin, yMin, zMin);
            rasterOut.template setProperty<std::string>("name", "output");
            rasterOut.setProjectionParameters(projFirst);
            rasterOut.setReductionType(rasterOutputReduction);

            // The output must be the anchor layer at the front of the vector
            RasterBaseRefs<CTYPE> rasterBaseRefsWithOutput;
            rasterBaseRefsWithOutput.push_back(rasterOut);
            rasterBaseRefsWithOutput.insert(rasterBaseRefsWithOutput.end(), rasterBaseRefs.begin(), rasterBaseRefs.end());

            // Get generator
            auto scriptHash = createScriptHash(script, Models::cl_raster_block_c, rasterBaseRefsWithOutput);
            auto kernelGenerator = KernelCache::getKernelCache().getKernelGenerator(scriptHash);
            if (kernelGenerator.programHash == Solver::getNullHash()) {

                // Check for underscores in script, these are reserved for internal variables
                if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                    throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                }

                // Generate kernel
                kernelGenerator = generateKernel<CTYPE>(
                    script, Models::cl_raster_block_c, rasterBaseRefsWithOutput, 
                        rasterNullValue, nullptr, rasterDebug != RasterDebug::Enable);

                // Check script
                if (kernelGenerator.script.size() == 0) {
                    throw std::runtime_error("Cannot generate script");
                }

                // Build kernel
                if (buildRasterKernel(kernelGenerator) == solver.getNullHash()) {
                    throw std::runtime_error("Cannot get kernel for script");
                }

                // Register kernel in cache
                KernelCache::getKernelCache().setKernelGenerator(scriptHash, kernelGenerator);
            }

            // Get kernel
            cl::Kernel &rasterKernel = solver.getKernel(kernelGenerator.programHash, "raster");        

            // Loop through tiles
            uint8_t verbose = solver.getVerboseLevel();
            auto dimOut = rasterOut.getRasterDimensions();
            if (rasterDebug == RasterDebug::Enable) {

                // Run over single tile only
                if (verbose <= Geostack::Verbosity::Info) {
                    std::cout << "Debug mode" << std::endl;
                }
                runTileKernel<CTYPE>(0, 0, rasterKernel, rasterBaseRefsWithOutput, kernelGenerator.reqs, 0, nullptr, rasterDebug);

            } else {

                // Run over all tiles
                for (uint32_t tj = 0; tj < dimOut.ty; tj++) {
                    for (uint32_t ti = 0; ti < dimOut.tx; ti++) {

                        // Run kernel
                        runTileKernel<CTYPE>(ti, tj, rasterKernel, rasterBaseRefsWithOutput, kernelGenerator.reqs, 0, nullptr, rasterDebug);
                    }

                    // Output progress
                    if (verbose <= Geostack::Verbosity::Info && (tj%10) == 0) {
                        std::cout << std::setprecision(2) << 100.0*(double)tj/(double)dimOut.ty << "%" << std::endl;
                    }
                }
                if (verbose <= Geostack::Verbosity::Info) {
                    std::cout << "100%" << std::endl;
                }
            }

            // Return raster
            return rasterOut;

        } else {
            throw std::runtime_error("OpenCL not initialised");
        }

        // Return empty raster
        return Raster<RTYPE, CTYPE>();
    }
    
    /**
    * Runs a script on the raster, returning a resulting raster.
    * @param script OpenCL script to run.
    * @param rasterBaseRef input raster.
    * @param parameters parameters for sampling and output raster.
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runAreaScript(
        std::string script, 
        RasterBase<CTYPE> &rasterBase, 
        cl_int width) {

        // Check for underscores in script, these are reserved for internal variables
        if (std::regex_search(script, std::regex("\\b_\\w*"))) {
            throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
        }

        // Parse script
        script = Solver::processScript(script);

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {

            // Get raster properties
            std::string name = rasterBase.template getProperty<std::string>("name");
            auto dim = rasterBase.getRasterDimensions();
            auto proj = rasterBase.getProjectionParameters();

            // Create output
            Raster<RTYPE, CTYPE> rasterOut;
            rasterOut.init(dim.d);
            rasterOut.template setProperty<std::string>("name", "output");
            rasterOut.setProjectionParameters(proj);
            rasterOut.setNeedsWrite(false);       
            
            // Replace variable name in script
            std::string kernelBlock = Models::cl_raster_area_op2D_c;
            kernelBlock = std::regex_replace(kernelBlock, std::regex("__VARIABLE__"), name);
            
            // Store and set raster properties
            bool rasterBaseNeedsWrite = rasterBase.getNeedsWrite();
            uint8_t rasterBaseNeighbours = rasterBase.getRequiredNeighbours();
            rasterBase.setNeedsWrite(false);
            rasterBase.setRequiredNeighbours(Neighbours::Queen);
            
            // Generate kernel
            RasterBaseRefs<CTYPE> rasterBaseRefs;            
            rasterBaseRefs.push_back(rasterBase);
            rasterBaseRefs.push_back(rasterOut);

            // Get generator
            auto scriptHash = createScriptHash(script, kernelBlock, rasterBaseRefs);
            auto kernelGenerator = KernelCache::getKernelCache().getKernelGenerator(scriptHash);
            if (kernelGenerator.programHash == Solver::getNullHash()) {

                // Generate kernel
                kernelGenerator = generateKernel<CTYPE>(
                    script, kernelBlock, rasterBaseRefs, RasterNullValue::Null);
                
                // Check script
                if (kernelGenerator.script.size() == 0) {
                    throw std::runtime_error("Cannot generate script");
                }
                
                // Build kernel
                std::size_t clRasterHash = buildRasterKernel(kernelGenerator);
                if (clRasterHash == solver.getNullHash()) {
                    throw std::runtime_error("Cannot get kernel for script");
                }

                // Register kernel in cache
                KernelCache::getKernelCache().setKernelGenerator(scriptHash, kernelGenerator);
            }

            // Get kernel
            cl::Kernel &rasterKernel = solver.getKernel(kernelGenerator.programHash, "rasterArea2D");        

            // Loop through tiles
            uint8_t verbose = solver.getVerboseLevel();
            auto dimOut = rasterOut.getRasterDimensions();
            for (uint32_t tj = 0; tj < dimOut.ty; tj++) {
                for (uint32_t ti = 0; ti < dimOut.tx; ti++) {

                    // Run kernel
                    rasterKernel.setArg(0, width);
                    runTileKernel<CTYPE>(ti, tj, rasterKernel, rasterBaseRefs, kernelGenerator.reqs, 1);
                }

                // Output progress
                if (verbose <= Geostack::Verbosity::Info && (tj%10) == 0) {
                    std::cout << std::setprecision(2) << 100.0*(double)tj/(double)dimOut.ty << "%" << std::endl;
                }
            }
            if (verbose <= Geostack::Verbosity::Info) {
                std::cout << "100%" << std::endl;
            }

            // Preserve original properties
            rasterOut.setNeedsWrite(true);
            rasterBase.setNeedsWrite(rasterBaseNeedsWrite);
            rasterBase.setRequiredNeighbours(rasterBaseNeighbours);

            return rasterOut;

        } else {
            throw std::runtime_error("OpenCL not initialised");
        }

        // Return empty raster
        return Raster<RTYPE, CTYPE>();
    }
    
    /**
    * Stipple %Raster data, creating a set of new Points
    * @return %Vector of %Raster.
    */
    template <typename RTYPE, typename CTYPE>
    Vector<CTYPE> stipple(
        std::string script, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        std::vector<std::string> fields, 
        uint32_t nPerCell) {

        // Create vector
        Vector<CTYPE> v;
        
        try {

            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            size_t clWorkgroupSize, clPaddedWorkgroupSize;
            if (solver.openCLInitialised()) {

                // Check number of points per cell
                if (nPerCell == 0) {
                    throw std::runtime_error("Number of points per cell must be greater than zero");
                }

                // Reduce Rasters
                if (rasterBaseRefs.size() > 0) {

                    // Check dimensions, this is currently restricted to 2D anchor layers
                    RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
                    auto dimFirst = rasterBaseFirst.getRasterDimensions();                    
                    if (dimFirst.d.nz > 1) {
                        throw std::runtime_error("Only 2D raster layers are supported for stippling");
                    }

                    // Create coordinate buffer
                    std::size_t nCoords = TileMetrics::tileSizeSquared*nPerCell;
                    auto &coordsCache = rasterBaseFirst.getCoordinateCache();
                    if (coordsCache.size() != nCoords)
                        coordsCache.getData().resize(nCoords);
                    
                    // Create fields
                    std::vector<std::string> fieldNames;
                    std::set<std::string> fieldsCheck;
                    for (auto &fieldName : fields) {

                        if (std::regex_search(script, std::regex("\\b" + fieldName + "\\b"))) {
                    
                            // Check for duplicate names
                            if (fieldsCheck.find(fieldName) != fieldsCheck.end()) {
                                throw std::runtime_error("Field name '" + fieldName + "' is not unique");
                            }
                            fieldsCheck.insert(fieldName);
                            fieldNames.push_back(fieldName);
                        }
                    }
                    auto nFields = fieldNames.size();

                    // Get generator
                    auto scriptHash = createScriptHash(script, Models::cl_stipple_c, rasterBaseRefs);
                    auto stippleGenerator = KernelCache::getKernelCache().getKernelGenerator(scriptHash);
                    if (stippleGenerator.programHash == Solver::getNullHash()) {
            
                        // Check for underscores in script, these are reserved for internal variables
                        if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                            throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                        }

                        // Add fields
                        std::string fieldArgsList;
                        std::string variableList;
                        std::string post;
                        if (nFields > 0) {

                            // Add to argument list
                            fieldArgsList += ",\n__global REAL *_fields";
                        }
                        for (std::size_t i = 0; i < nFields; i++) {

                            auto fieldName = fieldNames[i];
                            if (std::regex_search(script, std::regex("\\b" + fieldName + "\\b"))) {
                            
                                // Update script for field value
                                variableList += "REAL " + fieldName + " = noData_REAL;\n";
                                post += "*(_fields+" + std::to_string(i) + "+_idx*" + std::to_string(nFields) + ") = " + fieldName + ";\n";
                            }
                        }

                        // Generate raster kernel script
                        stippleGenerator = Geostack::generateKernel<CTYPE>(script, Models::cl_stipple_c, rasterBaseRefs);
                        if (stippleGenerator.script.size() == 0) {
                            throw std::runtime_error("Cannot generate script");
                        }

                        // Patch raster kernel template with additional code
                        fieldArgsList += ",";
                        stippleGenerator.script = std::regex_replace(stippleGenerator.script, std::regex("\\/\\*__ARGS2__\\*\\/"), fieldArgsList);
                        stippleGenerator.script = std::regex_replace(stippleGenerator.script, std::regex("\\/\\*__VARS2__\\*\\/"), variableList);
                        stippleGenerator.script = std::regex_replace(stippleGenerator.script, std::regex("\\/\\*__POST2__\\*\\/"), post);

                        // Build raster kernel
                        if (buildRasterKernel(stippleGenerator) == solver.getNullHash()) {
                            throw std::runtime_error("Cannot get kernel for script");
                        }

                        // Register kernel in cache
                        KernelCache::getKernelCache().setKernelGenerator(scriptHash, stippleGenerator);
                    }
                    auto &stippleKernel = solver.getKernel(stippleGenerator.programHash, "stipple"); 

                    // Loop over tiles
                    for (uint32_t tj = 0; tj < dimFirst.ty; tj++) {
                        for (uint32_t ti = 0; ti < dimFirst.tx; ti++) {

                            // Set kernel arguments
                            cl_uint arg = 0;
                            stippleKernel.setArg(arg++, coordsCache.getBuffer());
                            stippleKernel.setArg(arg++, rasterBaseFirst.getTileStatusBuffer(ti, tj));
                            stippleKernel.setArg(arg++, nPerCell);
                            if (nFields > 0) {
                                std::size_t dataSize = nCoords*nFields*sizeof(RTYPE);
                                cl::Buffer &fieldBuffer = rasterBaseFirst.getCacheBuffer(dataSize);
                                stippleKernel.setArg(arg++, fieldBuffer);
                            }

                            // Run kernel
                            runTileKernel(ti, tj, stippleKernel, rasterBaseRefs, stippleGenerator.reqs, arg);

                            // Get count
                            cl_uint count = rasterBaseFirst.getTileStatus(ti, tj);

                            // Populate Vector
                            if (count > 0) {
                    
                                // Unmap fields
                                std::vector<RTYPE> fieldValues;
                                if (nFields > 0) {
                                    fieldValues.resize(count*nFields);
                                    std::size_t dataSize = nCoords*nFields*sizeof(RTYPE);
                                    cl::Buffer &fieldBuffer = rasterBaseFirst.getCacheBuffer(dataSize);
                                    queue.enqueueReadBuffer(fieldBuffer, CL_TRUE, 0, count*nFields*sizeof(RTYPE), fieldValues.data());
                                }                        

                                // Update Vector
                                auto &coords = coordsCache.getData();
                                for (std::size_t i = 0; i < count; i++) {
                                    auto id = v.addPoint(coords[i]);
                                    for (std::size_t f = 0; f < nFields; f++) {
                                        v.template setProperty<RTYPE>(id, fieldNames[f], fieldValues[f+i*nFields]);
                                    }
                                }
                            }

                            // Reset count
                            rasterBaseFirst.setTileStatus(ti, tj, 0);
                        }
                    }

                    // Set projection
                    v.setProjectionParameters(rasterBaseFirst.getProjectionParameters());

                } else {
                    throw std::runtime_error("No rasters in stipple script");
                }

            } else {
                throw std::runtime_error("OpenCL not initialised");
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }

        return v;
    }

    /**
    * Sort column values in z-direction.
    * @param rasterBaseRef input raster.
    */
    template <typename CTYPE>
    void sortColumns(RasterBase<CTYPE> &rasterBase) {
    
        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {

            // Get raster properties
            std::string name = rasterBase.template getProperty<std::string>("name");
            auto dim = rasterBase.getRasterDimensions();
            
            // Store and set raster properties
            bool rasterBaseNeedsWrite = rasterBase.getNeedsWrite();
            rasterBase.setNeedsWrite(true);
            
            // Generate references
            RasterBaseRefs<CTYPE> rasterBaseRefs;            
            rasterBaseRefs.push_back(rasterBase);
            
            // Get generator
            auto scriptHash = createScriptHash("", Models::cl_sort_c, rasterBaseRefs);
            auto sortGenerator = KernelCache::getKernelCache().getKernelGenerator(scriptHash);
            if (sortGenerator.programHash == Solver::getNullHash()) {

                // Generate kernel script
                sortGenerator = Geostack::generateKernel<CTYPE>("", Models::cl_sort_c, rasterBaseRefs);
                if (sortGenerator.script.size() == 0) {
                    throw std::runtime_error("Cannot generate script");
                }

                // Force first Raster to be used
                sortGenerator.reqs.rasterRequirements[0].used = true;

                // Build raster kernel
                if (buildRasterKernel(sortGenerator) == solver.getNullHash()) {
                    throw std::runtime_error("Cannot get kernel for script");
                }

                // Register kernel in cache
                KernelCache::getKernelCache().setKernelGenerator(scriptHash, sortGenerator);
            }
            auto &sortKernel = solver.getKernel(sortGenerator.programHash, "sortREAL"); 

            // Loop through tiles
            uint8_t verbose = solver.getVerboseLevel();
            try {

                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {

                        // Set arguments
                        setTileKernelArguments<CTYPE>(ti, tj, sortKernel, rasterBaseRefs, sortGenerator.reqs);

                        // Execute kernel
                        queue.enqueueNDRangeKernel(sortKernel, 
                            cl::NullRange, 
                            cl::NDRange(TileMetrics::tileSize, dim.d.ny, 1), 
                            cl::NDRange(TileMetrics::tileSize, 1, 1));

                    }

                    // Output progress
                    if (verbose <= Geostack::Verbosity::Info && (tj%10) == 0) {
                        std::cout << std::setprecision(2) << 100.0*(double)tj/(double)dim.ty << "%" << std::endl;
                    }
                }

            } catch (cl::Error e) {
                std::stringstream err;
                err << "OpenCL exception '" << e.what() << "': " << e.err();
                throw std::runtime_error(err.str());
            }

            if (verbose <= Geostack::Verbosity::Info) {
                std::cout << "100%" << std::endl;
            }

            // Preserve original properties
            rasterBase.setNeedsWrite(rasterBaseNeedsWrite);

        } else {
            throw std::runtime_error("OpenCL not initialised");
        }
    }

    // CTYPE float definitions
    template bool equalSpatialMetrics2D(const Dimensions<float> l, const Dimensions<float> r);
    template bool equalSpatialMetrics(const Dimensions<float> l, const Dimensions<float> r);
    template class RasterBase<float>;
    template std::ostream &operator<<(std::ostream &, const RasterDimensions<float> &);
    template void runScriptNoOut<float>(std::string, RasterBaseRefs<float>, size_t);
    template KernelGenerator generateKernel(std::string, std::string, RasterBaseRefs<float>, RasterNullValue::Type, VariablesBasePtr<std::string>, bool);
    template bool setTileKernelArguments<float>(uint32_t, uint32_t, cl::Kernel &, RasterBaseRefs<float>, KernelRequirements, int, VariablesBasePtr<std::string>, bool);
    template bool runTileKernel<float>(uint32_t, uint32_t, cl::Kernel &, RasterBaseRefs<float>, KernelRequirements, int, VariablesBasePtr<std::string>, size_t);
    template void sortColumns<float>(RasterBase<float> &);

    // CTYPE double definitions
    template bool equalSpatialMetrics2D(const Dimensions<double> l, const Dimensions<double> r);
    template bool equalSpatialMetrics(const Dimensions<double> l, const Dimensions<double> r);
    template class RasterBase<double>;
    template std::ostream &operator<<(std::ostream &, const RasterDimensions<double> &);
    template void runScriptNoOut<double>(std::string, RasterBaseRefs<double>, size_t);
    template KernelGenerator generateKernel(std::string, std::string, RasterBaseRefs<double>, RasterNullValue::Type, VariablesBasePtr<std::string>, bool);
    template bool setTileKernelArguments<double>(uint32_t, uint32_t, cl::Kernel &, RasterBaseRefs<double>, KernelRequirements, int, VariablesBasePtr<std::string>, bool);
    template bool runTileKernel<double>(uint32_t, uint32_t, cl::Kernel &, RasterBaseRefs<double>, KernelRequirements, int, VariablesBasePtr<std::string>, size_t);
    template void sortColumns<double>(RasterBase<double> &);

    // RTYPE float, CTYPE float definitions
    template class Tile<float, float>;
    template class RasterFileHandler<float, float>;
    template class Raster<float, float>;
    
    template float RasterBase<float>::getVariableData<float>(std::string);
    template void RasterBase<float>::setVariableData<float>(std::string, float);
    template bool operator==(const Raster<float, float> &, const Raster<float, float> &);
    template Raster<float, float> runScript<float, float>(std::string, RasterBaseRefs<float>, size_t);
    template Raster<float, float> runAreaScript<float, float>(std::string, RasterBase<float> &, cl_int);
    template Vector<float> stipple<float, float>(std::string, RasterBaseRefs<float>, std::vector<std::string>, uint32_t);
    
    // RTYPE uint, CTYPE float definitions
    template class Tile<uint32_t, float>;
    template class RasterFileHandler<uint32_t, float>;
    template class Raster<uint32_t, float>;
    
    template uint32_t RasterBase<float>::getVariableData<uint32_t>(std::string);
    template void RasterBase<float>::setVariableData<uint32_t>(std::string, uint32_t);
    template bool operator==(const Raster<uint32_t, float> &, const Raster<uint32_t, float> &);
    template Raster<uint32_t, float> runScript<uint32_t, float>(std::string, RasterBaseRefs<float>, size_t);
    template Raster<uint32_t, float> runAreaScript<uint32_t, float>(std::string, RasterBase<float> &, cl_int);
    template Vector<float> stipple<uint32_t, float>(std::string, RasterBaseRefs<float>, std::vector<std::string>, uint32_t);
    
    // RTYPE double, CTYPE double definitions
    template class Tile<double, double>;
    template class RasterFileHandler<double, double>;
    template class Raster<double, double>;
        
    template double RasterBase<double>::getVariableData<double>(std::string);
    template void RasterBase<double>::setVariableData<double>(std::string, double);
    template bool operator==(const Raster<double, double> &, const Raster<double, double> &);
    template Raster<double, double> runScript<double, double>(std::string, RasterBaseRefs<double>, size_t);
    template Raster<double, double> runAreaScript<double, double>(std::string, RasterBase<double> &, cl_int);
    template Vector<double> stipple<double, double>(std::string, RasterBaseRefs<double>, std::vector<std::string>, uint32_t);

    // RTYPE uint, CTYPE double definitions
    template class Tile<uint32_t, double>;
    template class RasterFileHandler<uint32_t, double>;
    template class Raster<uint32_t, double>;
    
    template uint32_t RasterBase<double>::getVariableData<uint32_t>(std::string);
    template void RasterBase<double>::setVariableData<uint32_t>(std::string, uint32_t);
    template bool operator==(const Raster<uint32_t, double> &, const Raster<uint32_t, double> &);
    template Raster<uint32_t, double> runScript<uint32_t, double>(std::string, RasterBaseRefs<double>, size_t);
    template Raster<uint32_t, double> runAreaScript<uint32_t, double>(std::string, RasterBase<double> &, cl_int);
    template Vector<double> stipple<uint32_t, double>(std::string, RasterBaseRefs<double>, std::vector<std::string>, uint32_t);
}
