/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef GEOSTACK_NETWORK_FLOW_SOLVER_H
#define GEOSTACK_NETWORK_FLOW_SOLVER_H

#include <string>
#include <vector>

#include "gs_vector.h"

namespace Geostack
{
    /**
     * %Flow network node types.
     */
    namespace NetworkNodeType {
        enum Type { 
            Junction,
            Terminator, 
        };
    }
    
    /**
     * %Flow network segment types.
     */
    namespace NetworkSegmentType {
        enum Type { 
            Undefined, 
            HazenWilliams, 
            ManningOpenChannel, 
            Logarithmic, 
            SqrtExp, 
        };
    }

    /**
    * %NetworkFlowSolver class for solution of flow over a network. 
    */
    template <typename TYPE>
    class NetworkFlowSolver {

        public:
            bool init(Vector<TYPE> &network_, std::string jsonConfig = std::string());
            bool run();
            Vector<TYPE> &getNetwork() { return network; }

        private:

            Vector<TYPE> network; ///< Internal network
            std::vector<std::pair<cl_uint, cl_uint> > linkPoints; ///< Lookup table of links to points
    };
}

#endif
