/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/* SRM: */
 /* Just to be safe, creating a hydro network flow solver as separate */
  
#ifndef GEOSTACK_HYDRO_NETWORK_FLOW_SOLVER_H
#define GEOSTACK_HYDRO_NETWORK_FLOW_SOLVER_H

#include <string>
#include <vector>

#include "gs_vector.h"

namespace Geostack
{
    /**
     * %Flow network node types.
     * Added inflow 13/01/21
     */
    namespace HydroNetworkNodeType {
        enum Type { 
            Junction,
            Terminator, 
            Inflow,
        };
    }
    
    /**
     * %Flow network segment types.
     * Added dynamic wave segment type
     */
    namespace HydroNetworkSegmentType {
        enum Type { 
            Undefined, 
            HazenWilliams, 
            ManningOpenChannel, 
            Logarithmic, 
            SqrtExp, 
            DynamicWave,
        };
    }

	/**
	* %Flow network solution mode.
	* Can be auto (adjust segments based on Fr), diffusive or dynamic
	*/

	namespace HydroNetworkSolverMode {
		enum Type {
			Auto,
			Dynamic,
			Diffusive,
		};
	}

    /**
    * %HydroNetworkFlowSolver class for solution of flow over a network. 
    */
    template <typename T>
    class HydroNetworkFlowSolver {

        public:
            bool init(Vector<T> &network_, std::string jsonConfig = std::string());
            bool run(T runtime, T writeStep, uint32_t mode = 0);
            bool run_hydro();
			T getArea(uint32_t id);
			T getPerimeter(uint32_t id);
			T getChannelWidth(uint32_t id);
			T getConveyance(uint32_t id);
			T getPartialKh(uint32_t id);
            Vector<T> &getNetwork() { return network; }
            std::vector<T> &getTimeArray() { return timeArray; }

        private:

            Vector<T> network; ///< Internal network
            T time;
            std::vector<T> timeArray;
            std::vector<std::pair<cl_uint, cl_uint> > linkPoints; ///< Lookup table of links to points
            T getArea_(std::vector<T> distArray, std::vector<T> elevArray, T head);
            T getPerimeter_(std::vector<T> distArray, std::vector<T> elevArray, T head);
            T getChannelWidth_(std::vector<T> distArray, std::vector<T> elevArray, T head);
            T getConveyance_(T area, T mannings, T perimeter);
            T getPartialKh_(std::vector<T> distArray, std::vector<T> elevArray, T head, T mannings, T dz);
    };
}

#endif
