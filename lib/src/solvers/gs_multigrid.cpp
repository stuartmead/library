/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <iostream>
#include <sstream>
#include <regex>

#include "json11/json11.hpp"

#include "gs_solver.h"
#include "gs_multigrid.h"

namespace Geostack
{ 
    using namespace json11;
    
    template <typename TYPE>
    uint32_t Multigrid<TYPE>::factor(uint32_t size) {
        uint32_t factor = 16;
        while (size & (factor-1)) {
            factor >>= 1;
        }
        return factor;
    }

    /**
    * Multigrid resize
    * @param rDim dimensions to resize to
    */
    template <typename TYPE>
    void Multigrid<TYPE>::resizeTiles(RasterDimensions<TYPE> rDim) {
    
        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();

        // Get tile sizes
        uint32_t tileSize = TileMetrics::tileSize;
        auto dataSize = TileMetrics::tileSizeSquared*sizeof(TYPE);

        // Calculate new dimensions
        Dimensions<TYPE> dim;
        dim.nx = rDim.tx*tileSize;
        dim.ny = rDim.ty*tileSize;
        dim.nz = 1;
        dim.hx = rDim.d.hx;
        dim.hy = rDim.d.hy;
        dim.hz = 1.0;
        dim.ox = rDim.d.ox;
        dim.oy = rDim.d.oy;
        dim.oz = 0.0;

        uint32_t levels = 8; // TODO update
        for (uint32_t l = 0; l < levels; l++) {

            // Create rasters
            ll0[l].init(dim);            
            ll1[l].init(dim);         
            lb[l].init(dim);

            // Set data to zero
            auto newDim = lb[l].getRasterDimensions();
            for (uint32_t tj = 0; tj < newDim.ty; tj++) {
                for (uint32_t ti = 0; ti < newDim.tx; ti++) {
                    queue.enqueueFillBuffer(lb[l].getTileDataBuffer(ti, tj), 0, 0, dataSize);
                    queue.enqueueFillBuffer(ll0[l].getTileDataBuffer(ti, tj), 0, 0, dataSize);
                    queue.enqueueFillBuffer(ll1[l].getTileDataBuffer(ti, tj), 0, 0, dataSize);
                }
            }
            
            // Fill null tile to zero rather than no-data
            queue.enqueueFillBuffer(lb[l].getNullBuffer(), 0, 0, dataSize);
            queue.enqueueFillBuffer(ll0[l].getNullBuffer(), 0, 0, dataSize);
            queue.enqueueFillBuffer(ll1[l].getNullBuffer(), 0, 0, dataSize);

            // Modify dimensions for next level
            dim.nx = dim.nx>>1;
            dim.ny = dim.ny>>1;
            dim.hx *= 2.0;
            dim.hy *= 2.0;
        }
    }

    /**
    * Multigrid initialisation
    * @param jsonConfig json configuration for solver
    * @param inputLayers_ set of Raster input layers
    */
    template <typename TYPE>
    bool Multigrid<TYPE>::init(
        std::string jsonConfig,
        std::vector<RasterBasePtr<TYPE> > inputLayers_) {
        
        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();

        // Copy start conditions and layers
        inputLayers = inputLayers_;

        // Check inputs
        if (inputLayers.size() == 0) {
            throw std::runtime_error("No input layers for multigrid solver");
        }

        // Parse configuration
        std::string jsonErr;
        auto config = Json::parse(jsonConfig, jsonErr);
        if (!jsonErr.empty()) {
            throw std::runtime_error(jsonErr);
        }
        
        // Get and create simulation projection
        auto proj = ProjectionParameters<double>();
        if (config["projection"].is_string()) {
            proj = Projection::parsePROJ4(config["projection"].string_value());
        }

        // Get and check scripts
        std::string initScript;
        if (config["initialisationScript"].is_string()) {
            initScript = config["initialisationScript"].string_value();
        }
        if (initScript.length() == 0) {
            std::cout << "WARNING: No 'initialisationScript' value supplied." << std::endl;
        }

        needsUpdate = false;
        std::string updateScript;
        if (config["updateScript"].is_string()) {
            updateScript = config["updateScript"].string_value();
            needsUpdate = true;
        }

        cycles = 3;
        if (config["cycles"].is_number()) {
            if (config["cycles"].number_value() <= 0) {
                std::stringstream err;
                err << "Requested cycles " << cycles << " must be positive.";
                throw std::runtime_error(err.str());
            }
            cycles = (uint32_t)config["cycles"].number_value();
        }

        verbose = false;
        if (config["verbose"].is_bool()) {
            verbose = (bool)config["verbose"].bool_value();
        }
        
        // Create rasters
        uint32_t levels = 8;
        for (uint32_t l = 0; l < levels; l++) {

            ll0.push_back(Raster<TYPE, TYPE>("l0"));
            ll1.push_back(Raster<TYPE, TYPE>("l1"));
            lb.push_back(Raster<TYPE, TYPE>("b"));
        }

        // Initialise rasters
        resizeTiles(inputLayers[0]->getRasterDimensions());
        
        // Set reference requirements for relaxation kernel
        relaxReq = KernelRequirements( { Neighbours::None, Neighbours::Rook, Neighbours::None } );

        // Set projection and create references
        for (uint32_t l = 0; l < levels; l++) {

            // Set projection
            ll0[l].setProjectionParameters(proj);
            ll1[l].setProjectionParameters(proj);
            lb[l].setProjectionParameters(proj);

            // Create references
            relaxRefs.push_back( { lb[l], ll0[l], ll1[l] } );
            relaxFlipRefs.push_back( { lb[l], ll1[l], ll0[l] } );
        }
        
        // Create reference list
        scriptRefs = {  lb[0], ll0[0] };        
        for (std::size_t i = 0; i < inputLayers.size(); i++) 
            scriptRefs.push_back(*inputLayers[i]);  
        
        // Create initialisation kernel  
        initGenerator = Geostack::generateKernel<TYPE>(initScript, std::string(R_cl_multigrid_init_c), scriptRefs);
        
        // Create update kernel    
        updateGenerator = Geostack::generateKernel<TYPE>(updateScript, std::string(R_cl_multigrid_update_c), scriptRefs);

        // Create error kernel    
        lb[0].setNeedsWrite(false);
        ll0[0].setNeedsWrite(false);
        ll1[0].setNeedsRead(false);       
        ll1[0].setReductionType(Reduction::SumSquares);

        auto errorScriptBlock = std::string(R_cl_multigrid_error_c);
        errorGenerator = Geostack::generateKernel<TYPE>("", errorScriptBlock, { 
                lb[0], 
                ll0[0], 
                ll1[0]
            }, RasterNullValue::Null, nullptr, false);

        lb[0].setNeedsWrite(true);
        ll0[0].setNeedsWrite(true);
        ll1[0].setNeedsRead(true);
        ll1[0].setReductionType(Reduction::None);

        // Build kernels
        KernelGenerator multigridGenerator;
        multigridGenerator.script = 
            initGenerator.script + 
            updateGenerator.script + 
            std::string(R_cl_multigrid_head_c) +
            std::string(R_cl_multigrid_c) + 
            std::string(R_cl_multigrid_restrict_c) + 
            std::string(R_cl_multigrid_interpolate_c) + 
            errorGenerator.script;
            
        // Replace constant values        
        multigridGenerator.script = std::regex_replace(multigridGenerator.script, 
            std::regex("\\/\\*__MSIZE__\\*\\/"), std::to_string(TileMetrics::tileSize));
        multigridGenerator.script = std::regex_replace(multigridGenerator.script, 
            std::regex("\\/\\*__ROW_BSHIFT__\\*\\/"), std::to_string(TileMetrics::tileSizePower));

        auto multigridHash = buildRasterKernel(multigridGenerator);
        if (multigridHash == solver.getNullHash())
            return false;
        
        // Get kernels
        initKernel = solver.getKernel(multigridHash, "init");
        updateKernel = solver.getKernel(multigridHash, "update");
        relaxKernel = solver.getKernel(multigridHash, "relax");
        residualKernel = solver.getKernel(multigridHash, "residual");
        restrictKernel = solver.getKernel(multigridHash, "rest");
        interpolateKernel = solver.getKernel(multigridHash, "interpolate");
        errorKernel = solver.getKernel(multigridHash, "error");

        // Check local memory, requires (2*16+1)^2 REAL values for cache
        uint32_t cacheSize = (uint32_t)pow(2*16+1, 2)*sizeof(TYPE);
        if (solver.getLocalMemorySizeBytes() < cacheSize) {  // TODO fixed
            throw std::runtime_error("Insufficient OpenCL local memory for multigrid, requires: " +
                std::to_string(cacheSize) + " b, available: " +
                std::to_string(solver.getLocalMemorySizeBytes()) + " b");
        }

        // Set initialisation flag
        initialised = true;

        return true;
    }
    
    /**
    * Multigrid solution step
    */
    template <typename TYPE>
    bool Multigrid<TYPE>::step() { 

        // Check initialisation
        if (!initialised) {
            throw std::runtime_error("Solver not initialised.");
        }
        if (cycles == 0) {
            std::stringstream err;
            err << "Requested cycles " << cycles << " must be greater than zero.";
            throw std::runtime_error(err.str());
        }

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        
        // Check for domain size changes
        auto rDim = lb[0].getRasterDimensions();
        auto rDim_in = inputLayers[0]->getRasterDimensions();
        if (!equalSpatialMetrics2D(rDim.d, rDim_in.d)) {

            // Resize
            resizeTiles(rDim_in);

            // Reset dimensions
            rDim = lb[0].getRasterDimensions();
        }

        // Initialise
        for (uint32_t tj = 0; tj < rDim.ty; tj++) {
            for (uint32_t ti = 0; ti < rDim.tx; ti++) {
                runTileKernel<TYPE>(ti, tj, initKernel, scriptRefs, initGenerator.reqs);
            }
        }

        // Calculate initial error
        TYPE e0 = 0.0;
        if (verbose) {
            e0 = error(queue);
            std::cout << "Initial error: " << e0 << std::endl;
        }

        int levels = 8;
        for (uint32_t c = 0; c < cycles; c++) {

            // Relax
            relax(0, queue);

            for (int v = 0; v < levels-1; v++) {

                // Restrict
                rest(v, queue);                

                // Relax
                relax(v+1, queue);
            }

            // F-cycle
            for (int f = levels-3; f >= 0; f--) {
                for (int vf = levels-2; vf >= f; vf--) {
                
                    // Interpolate
                    interpolate(vf, queue);

                    // Relax
                    relax(vf, queue);
                }
                for (int vf = f; vf < levels-1; vf++) {

                    // Restrict
                    rest(vf, queue);      

                    // Relax
                    relax(vf+1, queue);
                }
            }

            for (int v = levels-2; v >= 0; v--) {

                // Interpolate
                interpolate(v, queue);

                // Relax
                relax(v, queue);
            }

            // Output relative error
            if (verbose) {
                auto e = error(queue);
                std::cout << "Error: " << e << ", relative: " << e/e0 << std::endl;
            }
        }

        // Update
        if (needsUpdate) {
            for (uint32_t tj = 0; tj < rDim.ty; tj++) {
                for (uint32_t ti = 0; ti < rDim.tx; ti++) {
                    runTileKernel<TYPE>(ti, tj, updateKernel, scriptRefs, updateGenerator.reqs);
                }
            }
        }

        return true;
    }
    
    /**
    * Multigrid relaxation step
    * @param level current level to relax, 0 is top level
    * @param dim dimensions of current Raster
    * @param queue OpenCL command queue
    */
    template <typename TYPE>
    void Multigrid<TYPE>::relax(uint32_t level, cl::CommandQueue &queue) { 

        // Create ranges
        auto dim = lb[level].getRasterDimensions();
        auto globalRange = cl::NDRange(std::min(TileMetrics::tileSize, dim.d.nx), std::min(TileMetrics::tileSize, dim.d.ny));        
        auto localRange = cl::NDRange(factor(dim.d.nx), factor(dim.d.ny));

        // Calculate spacing
        double ihx2 = 1.0/dim.d.hx; ihx2 *= ihx2;
        double ihy2 = 1.0/dim.d.hy; ihy2 *= ihy2;

        // Relaxation
        for (uint32_t tj = 0; tj < dim.ty; tj++) {
            for (uint32_t ti = 0; ti < dim.tx; ti++) {
                cl_uint tileBoundary = (cl_uint)(ti == 0) | ((cl_uint)(tj == 0) << 1);

                relaxKernel.setArg(0, tileBoundary);
                relaxKernel.setArg(1, (TYPE)ihx2);
                relaxKernel.setArg(2, (TYPE)ihy2);
                Geostack::setTileKernelArguments<TYPE>(ti, tj, relaxKernel, relaxRefs[level], relaxReq, 3, nullptr, false);
                queue.enqueueNDRangeKernel(relaxKernel, cl::NullRange, globalRange, localRange);
            }
        }
        for (uint32_t tj = 0; tj < dim.ty; tj++) {
            for (uint32_t ti = 0; ti < dim.tx; ti++) {
                cl_uint tileBoundary = (cl_uint)(ti == 0) | ((cl_uint)(tj == 0) << 1);

                relaxKernel.setArg(0, tileBoundary);
                relaxKernel.setArg(1, (TYPE)ihx2);
                relaxKernel.setArg(2, (TYPE)ihy2);
                Geostack::setTileKernelArguments<TYPE>(ti, tj, relaxKernel, relaxFlipRefs[level], relaxReq, 3, nullptr, false);
                queue.enqueueNDRangeKernel(relaxKernel, cl::NullRange, globalRange, localRange);
            }
        }
    }

    /**
    * Multigrid residual and restriction step, restricts from level to level+1
    * @param level current level for residual, 0 is top level
    * @param dim dimensions of current Raster
    * @param queue OpenCL command queue
    */
    template <typename TYPE>
    void Multigrid<TYPE>::rest(uint32_t level, cl::CommandQueue &queue) { 
    
        // Create ranges
        auto dimf = lb[level].getRasterDimensions();
        auto globalRange = cl::NDRange(std::min(TileMetrics::tileSize, dimf.d.nx), std::min(TileMetrics::tileSize, dimf.d.ny));
        auto localRange = cl::NDRange(factor(dimf.d.nx), factor(dimf.d.ny));

        // Calculate spacing
        double ihx2 = 1.0/dimf.d.hx; ihx2 *= ihx2;
        double ihy2 = 1.0/dimf.d.hy; ihy2 *= ihy2;

        // Residual
        for (uint32_t tj = 0; tj < dimf.ty; tj++) {
            for (uint32_t ti = 0; ti < dimf.tx; ti++) {

                // Set bit 1 if tile is west edge of raster, bit 2 if south edge, bit 3 if east edge is subtile and bit 4 if north edge is subtile
                cl_uint tileBoundary = (cl_uint)(ti == 0) | ((cl_uint)(tj == 0) << 1);

                residualKernel.setArg(0, tileBoundary);
                residualKernel.setArg(1, (TYPE)ihx2);
                residualKernel.setArg(2, (TYPE)ihy2);
                Geostack::setTileKernelArguments<TYPE>(ti, tj, residualKernel, relaxRefs[level], relaxReq, 3, nullptr, false);
                queue.enqueueNDRangeKernel(residualKernel, cl::NullRange, globalRange, localRange);
            }
        }
        
        // Create ranges
        auto dimc = lb[level+1].getRasterDimensions();
        globalRange = cl::NDRange(std::min(TileMetrics::tileSize, dimc.d.nx), std::min(TileMetrics::tileSize, dimc.d.ny));
        localRange = cl::NDRange(factor(dimc.d.nx), factor(dimc.d.ny));

        // Calculate spacing
        ihx2 = 1.0/dimc.d.hx; ihx2 *= ihx2;
        ihy2 = 1.0/dimc.d.hy; ihy2 *= ihy2;
        
        // Restrict residual
        cl_uint arg = 0;
        restrictKernel.setArg(arg++, (TYPE)ihx2);
        restrictKernel.setArg(arg++, (TYPE)ihy2);
        for (uint32_t tjc = 0; tjc < dimc.ty; tjc++) {
            for (uint32_t tic = 0; tic < dimc.tx; tic++) {

                uint32_t tif = tic<<1;
                uint32_t tjf = tjc<<1;
            
                arg = 2;
                restrictKernel.setArg(arg++, lb[level+1].getTileDataBuffer(tic, tjc));
                restrictKernel.setArg(arg++, ll0[level+1].getTileDataBuffer(tic, tjc));
                restrictKernel.setArg(arg++, ll1[level].getTileDataBuffer(tif, tjf));

                if (tjf < dimf.ty-1) {
                    restrictKernel.setArg(arg++, ll1[level].getTileDataBuffer(tif, tjf+1));
                } else {
                    restrictKernel.setArg(arg++, ll1[level].getNullBuffer());
                }
                            
                if (tif < dimf.tx-1 && tjf < dimf.ty-1) {
                    restrictKernel.setArg(arg++, ll1[level].getTileDataBuffer(tif+1, tjf+1));
                } else {
                    restrictKernel.setArg(arg++, ll1[level].getNullBuffer());
                }

                if (tif < dimf.tx-1) {
                    restrictKernel.setArg(arg++, ll1[level].getTileDataBuffer(tif+1, tjf));
                } else {
                    restrictKernel.setArg(arg++, ll1[level].getNullBuffer());
                }

                if (tif < dimf.tx-1 && tjf > 0) {
                    restrictKernel.setArg(arg++, ll1[level].getTileDataBuffer(tif+1, tjf-1));
                } else {
                    restrictKernel.setArg(arg++, ll1[level].getNullBuffer());
                }

                if (tjf > 0) {
                    restrictKernel.setArg(arg++, ll1[level].getTileDataBuffer(tif, tjf-1));
                } else {
                    restrictKernel.setArg(arg++, ll1[level].getNullBuffer());
                }

                if (tif > 0 && tjf > 0) {
                    restrictKernel.setArg(arg++, ll1[level].getTileDataBuffer(tif-1, tjf-1));
                } else {
                    restrictKernel.setArg(arg++, ll1[level].getNullBuffer());
                }

                if (tif > 0) {
                    restrictKernel.setArg(arg++, ll1[level].getTileDataBuffer(tif-1, tjf));
                } else {
                    restrictKernel.setArg(arg++, ll1[level].getNullBuffer());
                }

                if (tif > 0 && tjf < dimf.ty-1) {
                    restrictKernel.setArg(arg++, ll1[level].getTileDataBuffer(tif-1, tjf+1));
                } else {
                    restrictKernel.setArg(arg++, ll1[level].getNullBuffer());
                }

                queue.enqueueNDRangeKernel(restrictKernel, cl::NullRange, globalRange, localRange);
            }
        }        
    }
    
    /**
    * Multigrid interpolation step, interpolates from level+1 to level
    * @param level current level for interpolation, 0 is top level
    * @param dim dimensions of current Raster
    * @param queue OpenCL command queue
    */
    template <typename TYPE>
    void Multigrid<TYPE>::interpolate(uint32_t level, cl::CommandQueue &queue) { 

        // Create ranges
        auto dimc = lb[level+1].getRasterDimensions();
        auto dimf = lb[level].getRasterDimensions();
        auto localRange = cl::NDRange(factor(dimc.d.nx), factor(dimc.d.ny));

        // Interpolate residual
        bool isOddx = (dimc.d.nx & (TileMetrics::tileSize-1)) != 0;
        bool isOddy = (dimc.d.ny & (TileMetrics::tileSize-1)) != 0;
        for (uint32_t tjc = 0; tjc < dimc.ty; tjc++) {
            for (uint32_t tic = 0; tic < dimc.tx; tic++) {  
            
                // If dimensions are an odd multiple of tileSize, run over half the domain for the last tile
                uint32_t rx = TileMetrics::tileSize;
                if (dimc.d.nx < TileMetrics::tileSize) {
                    rx = dimc.d.nx;
                } else if (isOddx && (tic == dimc.tx-1)) {
                    rx = TileMetrics::tileSize>>1;
                }
                uint32_t ry = TileMetrics::tileSize;
                if (dimc.d.ny < TileMetrics::tileSize) {
                    ry = dimc.d.ny;
                } else if (isOddy && (tjc == dimc.ty-1)) {
                    ry = TileMetrics::tileSize>>1;
                }
                auto globalRange = cl::NDRange(rx, ry);

                uint32_t tif = tic<<1;
                uint32_t tjf = tjc<<1;
                
                cl_uint arg = 0;

                interpolateKernel.setArg(arg++, ll0[level+1].getTileDataBuffer(tic, tjc));

                if (tjc < dimc.ty-1) {
                    interpolateKernel.setArg(arg++, ll0[level+1].getTileDataBuffer(tic, tjc+1));
                } else {
                    interpolateKernel.setArg(arg++, ll0[level+1].getNullBuffer());
                }
                            
                if (tic < dimc.tx-1 && tjc < dimc.ty-1) {
                    interpolateKernel.setArg(arg++, ll0[level+1].getTileDataBuffer(tic+1, tjc+1));
                } else {
                    interpolateKernel.setArg(arg++, ll0[level+1].getNullBuffer());
                }

                if (tic < dimc.tx-1) {
                    interpolateKernel.setArg(arg++, ll0[level+1].getTileDataBuffer(tic+1, tjc));
                } else {
                    interpolateKernel.setArg(arg++, ll0[level+1].getNullBuffer());
                }

                interpolateKernel.setArg(arg++, ll0[level].getTileDataBuffer(tif, tjf));

                if (tjf < dimf.ty-1) {
                    interpolateKernel.setArg(arg++, ll0[level].getTileDataBuffer(tif, tjf+1));
                } else {
                    interpolateKernel.setArg(arg++, ll0[level].getNullBuffer());
                }
                            
                if (tif < dimf.tx-1 && tjf < dimf.ty-1) {
                    interpolateKernel.setArg(arg++, ll0[level].getTileDataBuffer(tif+1, tjf+1));
                } else {
                    interpolateKernel.setArg(arg++, ll0[level].getNullBuffer());
                }

                if (tif < dimf.tx-1) {
                    interpolateKernel.setArg(arg++, ll0[level].getTileDataBuffer(tif+1, tjf));
                } else {
                    interpolateKernel.setArg(arg++, ll0[level].getNullBuffer());
                }

                queue.enqueueNDRangeKernel(interpolateKernel, cl::NullRange, globalRange, localRange);
            }
        }        
    }

    /**
    * Multigrid error of top-level residual
    * @param dim dimensions of current Raster
    * @param queue OpenCL command queue
    * @return RMS of top-level residual
    */
    template <typename TYPE>
    TYPE Multigrid<TYPE>::error(cl::CommandQueue &queue) {     
    
        // Create ranges
        auto dim = lb[0].getRasterDimensions();
        auto globalRange = cl::NDRange(TileMetrics::tileSize, TileMetrics::tileSize);
        auto localRange = cl::NDRange(16, 16);

        // Calculate spacing
        double ihx2 = 1.0/(dim.d.hx); ihx2 *= ihx2;
        double ihy2 = 1.0/(dim.d.hy); ihy2 *= ihy2;

        // Calculate RMS error of top-level residual
        ll1[0].setReductionType(Reduction::SumSquares);
        for (uint32_t tj = 0; tj < dim.ty; tj++) {
            for (uint32_t ti = 0; ti < dim.tx; ti++) {
                cl_uint tileBoundary = (cl_uint)(ti == 0) | ((cl_uint)(tj == 0) << 1);

                errorKernel.setArg(0, tileBoundary);
                errorKernel.setArg(1, (TYPE)ihx2);
                errorKernel.setArg(2, (TYPE)ihy2);
                Geostack::setTileKernelArguments<TYPE>(ti, tj, errorKernel, relaxRefs[0], relaxReq, 3);
                queue.enqueueNDRangeKernel(errorKernel, cl::NullRange, globalRange, localRange);
            }
        }

        // Reduce
        TYPE error = sqrt(ll1[0].reduce());
        ll1[0].setReductionType(Reduction::None);

        return error;
    }

    // Float type definitions
    template class Multigrid<float>;

    // Double type definitions
    template class Multigrid<double>;
}
