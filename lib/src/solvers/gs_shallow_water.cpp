/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <iostream>
#include <iomanip>

#include "json11/json11.hpp"

#include "gs_solver.h"
#include "gs_vector.h"
#include "gs_geojson.h"
#include "gs_projection.h"
#include "gs_string.h"
#include "gs_shallow_water.h"

// Lattice stencil and indexes
//
// NW (6)  N (2)  NE (5)
//       \   |   /
//        \  |  /
//         \ | /
//          \|/
// W (3) ----0---- E (1)
//          /|\
//         / | \
//        /  |  \
//       /   |   \
// SW (7)  S (4)  SW (8)
//

namespace Geostack
{ 
    using namespace json11; 
    
    template <typename TYPE>
    bool ShallowWater<TYPE>::init(
        std::string jsonConfig,
        Raster<TYPE, TYPE> &h_in,
        Raster<TYPE, TYPE> &b_in) {    

        // Parse configuration
        std::string jsonErr;
        auto config = Json::parse(jsonConfig, jsonErr);
        if (!jsonErr.empty()) {
            throw std::runtime_error(jsonErr);
        }
        
        // Get and check timestep
        dt = 0.0;
        if (config["dt"].is_number()) {
            dt = (TYPE)config["dt"].number_value();
        }
        if (dt <= 0.0) {
            throw std::runtime_error("Time step must be positive");
        }    
        
        // Get and check drag
        cd = 0.01;
        if (config["dragCoefficientManning"].is_number()) {
            cd = (TYPE)config["dragCoefficientManning"].number_value();
        }
        if (cd <= 0.0) {
            throw std::runtime_error("Manning drag coefficient must be positive");
        }

        // Get and check relaxation time
        tau = 0.9;
        if (config["tau"].is_number()) {
            tau = (TYPE)config["tau"].number_value();
        }
        if (tau <= 0.0) {
            throw std::runtime_error("Relaxation time must be positive");
        }

        // Check inputs
        if (!h_in.hasData()) {
            throw std::runtime_error("Shallow water height input must be initialised");
        }
        if (!b_in.hasData()) {
            throw std::runtime_error("Shallow water base input must be initialised");
        }
        auto dim_h = h_in.getRasterDimensions();
        auto dim_b = b_in.getRasterDimensions();
        if (!equalSpatialMetrics2D(dim_h.d, dim_b.d)) {
            throw std::runtime_error("Shallow water base and height inputs must have same spatial dimensions");
        }

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Clear status
        statusVector.clear();

        // Create internal rasters
        h = h_in;
        b = b_in;

        // Set active tiles
        activeTiles.clear();
        for (uint32_t tj = 0; tj < dim_h.ty; tj++) {
            for (uint32_t ti = 0; ti < dim_h.tx; ti++) {
                activeTiles.insert( { ti, tj } );
            }
        }

        auto proj = h.getProjectionParameters();
        if (b.getProjectionParameters() != proj) {
            throw std::runtime_error("Shallow water base and height inputs must have same projection");
        }

        uh.init2D(dim_h.d.nx, dim_h.d.ny, dim_h.d.hx, dim_h.d.hy, dim_h.d.ox, dim_h.d.oy);
        uh.setProperty("name", "uh");
        uh.setProjectionParameters(proj);

        vh.init2D(dim_h.d.nx, dim_h.d.ny, dim_h.d.hx, dim_h.d.hy, dim_h.d.ox, dim_h.d.oy);
        vh.setProperty("name", "vh");
        vh.setProjectionParameters(proj);

        fi.init(dim_h.d.nx, dim_h.d.ny, 9, dim_h.d.hx, dim_h.d.hy, 1.0, dim_h.d.ox, dim_h.d.oy, dim_h.d.oz);
        fi.setProperty("name", "_fi");
        fi.setProjectionParameters(proj);

        fe.init(dim_h.d.nx, dim_h.d.ny, 9, dim_h.d.hx, dim_h.d.hy, 1.0, dim_h.d.ox, dim_h.d.oy, dim_h.d.oz);
        fe.setProperty("name", "_fe");
        fe.setProjectionParameters(proj);

        ft.init(dim_h.d.nx, dim_h.d.ny, 9, dim_h.d.hx, dim_h.d.hy, 1.0, dim_h.d.ox, dim_h.d.oy, dim_h.d.oz);
        ft.setProperty("name", "_ft");
        ft.setProjectionParameters(proj);
        
        // Create initialisation kernel
        initRefs = { h, uh, vh, fi };
        initKernelGenerator = generateKernel<TYPE>("", std::string(R_cl_shallow_water_init_c), initRefs);

        // Create collision kernel
        h.setReductionType(Reduction::Sum);
        h.setNeedsWrite(false);
        b.setNeedsWrite(false);
        uh.setNeedsWrite(false);
        vh.setNeedsWrite(false);
        collisionRefs = { h, b, uh, vh, fi, fe, ft };
        collisionKernelGenerator = generateKernel<TYPE>("", std::string(R_cl_shallow_water_collision_c), collisionRefs);
        h.setReductionType(Reduction::None);
        h.setNeedsWrite(true);  
        b.setNeedsWrite(true);  
        uh.setNeedsWrite(true);
        vh.setNeedsWrite(true);

        // Create streaming kernel
        b.setNeedsWrite(false);
        streamingRefs = { h, uh, vh, fi, fe, ft };
        streamingKernelGenerator = generateKernel<TYPE>("", std::string(R_cl_shallow_water_streaming_c), streamingRefs);
        streamingKernelGenerator.reqs.rasterRequirements[4] = Neighbours::Queen;
        streamingKernelGenerator.reqs.rasterRequirements[5] = Neighbours::Queen;
        b.setNeedsWrite(true);

        // Build script and get kernels
        KernelGenerator shallowWaterGenerator;
        shallowWaterGenerator.script = 
            initKernelGenerator.script + 
            collisionKernelGenerator.script +
            streamingKernelGenerator.script;

        shallowWaterGenerator.reqs.usesRandom = 
            initKernelGenerator.reqs.usesRandom;

        shallowWaterGenerator.projectionTypes.insert(
            initKernelGenerator.projectionTypes.begin(), initKernelGenerator.projectionTypes.end());
        shallowWaterGenerator.projectionTypes.insert(
            collisionKernelGenerator.projectionTypes.begin(), collisionKernelGenerator.projectionTypes.end());
        shallowWaterGenerator.projectionTypes.insert(
            streamingKernelGenerator.projectionTypes.begin(), streamingKernelGenerator.projectionTypes.end());

        auto shallowWaterHash = buildRasterKernel(shallowWaterGenerator);
        if (shallowWaterHash == solver.getNullHash())
            return false;
            
        initKernel = solver.getKernel(shallowWaterHash, "init");
        collisionKernel = solver.getKernel(shallowWaterHash, "collision");
        streamingKernel = solver.getKernel(shallowWaterHash, "streaming");

        // Initialise
        TYPE c = (dim_h.d.hx+dim_h.d.hy)/(2.0*dt);
        TYPE ic2 = 1.0/(c*c);

        initKernel.setArg(0, ic2);
        for (uint32_t tj = 0; tj < dim_h.ty; tj++) {
            for (uint32_t ti = 0; ti < dim_h.tx; ti++) {
                runTileKernel<TYPE>(ti, tj, initKernel, initRefs, initKernelGenerator.reqs, 1);
            }
        }

        // Set initialisation flag
        initialised = true;

        return true;
    }

    template <typename TYPE>
    bool ShallowWater<TYPE>::step() {
    
        // Check initialisation
        if (!initialised) {
            throw std::runtime_error("Solver not initialised.");
        }

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Check and update status variable
        if (statusVector.size() != activeTiles.size()) {

            // Resize data and create buffer
            statusVector.resize(activeTiles.size());
            statusBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                statusVector.size()*sizeof(cl_uint), statusVector.data());
            auto statusBufferPtr = queue.enqueueMapBuffer(statusBuffer, CL_TRUE, CL_MAP_READ, 0, statusVector.size()*sizeof(cl_uint));

            // Check pointer
            if (statusBufferPtr != static_cast<void *>(statusVector.data()))
                throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
        }
        queue.enqueueUnmapMemObject(statusBuffer, static_cast<void *>(statusVector.data()));
        queue.enqueueFillBuffer(statusBuffer, 0, 0, statusVector.size()*sizeof(cl_uint));
        
        // Simulation variables
        auto dim_h = h.getRasterDimensions();
        TYPE c = (dim_h.d.hx+dim_h.d.hy)/(2.0*dt);
        TYPE cs = c/sqrt(3.0); // Sound speed
        TYPE ic2 = 1.0/(c*c);
        TYPE ics2 = 1.0/(cs*cs);

        // Collision step
        cl_uint tid = 0;
        h.setReductionType(Reduction::Sum);
        collisionKernel.setArg(0, c);
        collisionKernel.setArg(1, ic2);
        collisionKernel.setArg(2, ics2);
        collisionKernel.setArg(3, (REAL)(1.0/tau));
        collisionKernel.setArg(5, statusBuffer);  
        for (auto &tile : activeTiles) {
            collisionKernel.setArg(4, tid++);  
            runTileKernel<TYPE>(tile.first, tile.second, collisionKernel, collisionRefs, collisionKernelGenerator.reqs, 6);
        }
        h.setReductionType(Reduction::None);

        // Map to host and check pointer
        auto statusBufferPtr = queue.enqueueMapBuffer(statusBuffer, CL_TRUE, CL_MAP_READ, 0, statusVector.size()*sizeof(cl_uint)); 
        if (statusBufferPtr != static_cast<void *>(statusVector.data()))
            throw std::runtime_error("OpenCL mapped pointer differs from data pointer"); 

        // Create new active tile set
        tid = 0;
        std::set<std::pair<uint32_t, uint32_t> > newActiveTiles;
        for (auto &tile : activeTiles) {

            auto status = statusVector[tid++];
            auto ti = tile.first;
            auto tj = tile.second;

            if (status & 0x10) {
                newActiveTiles.insert( { ti, tj } );
            }
            if (status & 0x01 && tj < dim_h.ty-1) {
                newActiveTiles.insert( { ti, tj+1 } );
                if (status & 0x02 && ti < dim_h.tx-1) {
                    newActiveTiles.insert( { ti+1, tj+1 } );
                }
                if (status & 0x08 && ti > 0) {
                    newActiveTiles.insert( { ti-1, tj+1 } );
                }
            }
            if (status & 0x04 && tj > 0) {
                newActiveTiles.insert( { ti, tj-1 } );
                if (status & 0x02 && ti < dim_h.tx-1) {
                    newActiveTiles.insert( { ti+1, tj-1 } );
                }
                if (status & 0x08 && ti > 0) {
                    newActiveTiles.insert( { ti-1, tj-1 } );
                }
            }
            if (status & 0x02 && ti < dim_h.tx-1) {
                newActiveTiles.insert( { ti+1, tj } );
            }
            if (status & 0x08 && ti > 0) {
                newActiveTiles.insert( { ti-1, tj } );
            }
        }
        activeTiles = newActiveTiles;
        
        // Check for no water
        if (activeTiles.size() == 0) {
            throw std::runtime_error("Water level is zero everywhere");
        }    

        // Streaming step
        streamingKernel.setArg(0, c);
        streamingKernel.setArg(1, tau);
        streamingKernel.setArg(2, (REAL)(9.81*cd*cd*dt));
        for (auto &tile : activeTiles) {
            runTileKernel<TYPE>(tile.first, tile.second, streamingKernel, streamingRefs, streamingKernelGenerator.reqs, 3);
        }

        return true;
    }

    // Float type definitions
    template class ShallowWater<float>;

    // Double type definitions
    template class ShallowWater<double>;
}