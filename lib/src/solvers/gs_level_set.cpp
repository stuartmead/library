/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <iostream>
#include <sstream>
#include <regex>

#include "json11/json11.hpp"

#include "gs_solver.h"
#include "gs_vector.h"
#include "gs_geojson.h"
#include "gs_projection.h"
#include "gs_string.h"
#include "gs_level_set.h"

// Include code for projection
#include "cl_date_head.c"

namespace Geostack
{ 
    using namespace json11;      
        
    template <typename TYPE>
    std::string LevelSet<TYPE>::buildNormal = "REALVEC2 normal_vector = normalize((REALVEC2)(0.5*(_distance_E-_distance_W)/_dim.hx, 0.5*(_distance_N-_distance_S)/_dim.hy)); \n";

    template <typename TYPE>
    std::string LevelSet<TYPE>::buildAdvectNormal = "REALVEC2 advect_normal_vector = (REALVEC2)(_dx, _dy)/hypot(_dx, _dy); \n";

    template <typename TYPE>
    std::string LevelSet<TYPE>::buildAdvectMag = "REAL advect_mag = hypot(advect_x, advect_y); \n";      

    template <typename TYPE>
    std::string LevelSet<TYPE>::buildAdvectVector = "REALVEC2 advect_vector = (REALVEC2)(advect_x, advect_y); \n";

    template <typename TYPE>
    std::string LevelSet<TYPE>::buildAdvectDotNormal = "REAL advect_dot_normal = fmax(dot(advect_vector, advect_normal_vector), (REAL)0.0); \n";        

    // Set static members
    template <typename TYPE>
    const TYPE LevelSet<TYPE>::CFL = (TYPE)0.75;

    template <typename TYPE>
    const TYPE LevelSet<TYPE>::timeStepMax = (TYPE)60.0;

    template <typename TYPE>
    const TYPE LevelSet<TYPE>::narrowBandCells = (TYPE)3.0;

    // Find dimensions from bounds
    template <typename TYPE>
    Dimensions<TYPE> LevelSet<TYPE>::boundDimensions(Vector<TYPE> &v) {
    
        const uint32_t tileSize = TileMetrics::tileSize;

        // Get distance map bounds
        auto bounds = v.getBounds();

        // Add buffer of narrow band
        bounds.extend((TYPE)2.0*p.bandWidth);

        // Calculate distance map cells
        auto boundsExtent = bounds.extent();
        uint32_t nx = (uint32_t)ceil(boundsExtent.p/resolution);
        uint32_t ny = (uint32_t)ceil(boundsExtent.q/resolution);

        // Calculate number of tiles
        uint32_t ntx = (nx+((tileSize-(nx%tileSize))%tileSize))/tileSize;
        uint32_t nty = (ny+((tileSize-(ny%tileSize))%tileSize))/tileSize;

        // Calculate offset
        auto boundsCentre = bounds.centroid();        
        TYPE ox = boundsCentre.p-(TYPE)0.5*resolution*ntx*tileSize;
        TYPE oy = boundsCentre.q-(TYPE)0.5*resolution*nty*tileSize;
                    
        // Snap to multiple of global resolution
        ox = floor(ox/resolution)*resolution;
        oy = floor(oy/resolution)*resolution;

        // Create raster dimensions
        Dimensions<TYPE> dim;
        dim.nx = ntx*tileSize;
        dim.ny = nty*tileSize;
        dim.nz = levels;
        dim.hx = resolution;
        dim.hy = resolution;
        dim.hz = 1.0;
        dim.ox = ox;
        dim.oy = oy;
        dim.oz = 0.0;

        return dim;
    }
    
    // Resize solver
    template <typename TYPE>
    void LevelSet<TYPE>::resizeTiles(uint32_t tx, uint32_t ty, uint32_t ox, uint32_t oy) {

        // Get resizing indexes
        newTileIndexes = classbits.resize2DIndexes(tx, ty, ox, oy);
        if (newTileIndexes.size() > 0) {

            // Resize rasters
            classbits.resize2D(tx, ty, ox, oy);
            for (auto &l : layers)
                l.resize2D(tx, ty, ox, oy);
            for (auto &pl : outputLayers)
                pl->resize2D(tx, ty, ox, oy);
        }
    }
    
    template <typename TYPE>
    bool LevelSet<TYPE>::init(
        std::string jsonConfig,
        Vector<TYPE> sources_,
        std::shared_ptr<Variables<TYPE, std::string> > variables_,
        std::vector<RasterBasePtr<TYPE> > inputLayers_,
        std::vector<RasterBasePtr<TYPE> > outputLayers_) {

        const uint32_t tileSize = TileMetrics::tileSize;
        
        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Reset iterations
        iters = 0;

        // Copy start conditions and layers
        sources = sources_;
        variables = variables_;
        inputLayers = inputLayers_;
        outputLayers = outputLayers_;

        // Check inputs
        if (!sources.hasData() && inputLayers.size() == 0) {
            throw std::runtime_error("No sources or input layers for level set solver");
        }

        // Parse configuration
        std::string jsonErr;
        auto config = Json::parse(jsonConfig, jsonErr);
        if (!jsonErr.empty()) {
            throw std::runtime_error(jsonErr);
        }

        // Get and create simulation projection
        auto proj = ProjectionParameters<double>();
        if (config["projection"].is_string()) {
            proj = Projection::parsePROJ4(config["projection"].string_value());
        }

        // Get and check levels
        levels = 1;
        if (config["levels"].is_number()) {
            levels = (uint32_t)config["levels"].number_value();
        }
        if (levels == 0) {
            std::stringstream err;
            err << "Requested levels " << levels << " must be greater than zero.";
            throw std::runtime_error(err.str());
        }
        
        // Get and check target
        timeMultiple = 3600.0;
        if (config["timeMultiple"].is_number()) {
            timeMultiple = (TYPE)config["timeMultiple"].number_value();
        }
        if (timeMultiple < 0.0) {
            throw std::runtime_error("Time multiple must be positive");
        }

        // Get and check scripts
        std::string initScript;
        if (config["initialisationScript"].is_string()) {
            initScript = config["initialisationScript"].string_value();
        }
        if (initScript.length() == 0) {
            std::cout << "WARNING: No 'initialisationScript' value supplied." << std::endl;
        }

        std::string advectScript;
        if (config["advectionScript"].is_string()) {
            advectScript = config["advectionScript"].string_value();
        }
        if (advectScript.length() == 0) {
            std::cout << "WARNING: No 'advectionScript' value supplied, default advection vector of (0, 0) applied." << std::endl;
            advectScript = "advect_x = 0.0; advect_y = 0.0;";
        }

        std::string buildScript;
        if (config["buildScript"].is_string()) {
            buildScript = config["buildScript"].string_value();
        }
        if (buildScript.length() == 0) {
            std::cout << "WARNING: No 'buildScript' value supplied." << std::endl;
        }

        std::string updateScript;
        if (config["updateScript"].is_string()) {
            updateScript = config["updateScript"].string_value();
        }
        if (updateScript.length() == 0) {
            std::cout << "WARNING: No 'updateScript' value supplied." << std::endl;
        }

        // Process start date and time
        hasStartDate = false;
        currentEpochMilliseconds = 0;
        localZoneOffsetMinutes = 0;
        startTime = date::sys_time<std::chrono::milliseconds>();
        startDay = date::sys_time<std::chrono::milliseconds>();
        if (config["startDateISO8601"].is_string()) {

            // Get date
            auto dateString = config["startDateISO8601"].string_value();
            if (dateString.length() > 0) {
            
                // Parse date
                std::istringstream in(dateString);
                if (dateString.back() == 'Z') {

                    // Try to parse Zulu format
                    in >> date::parse("%FT%TZ", startTime);

                } else {

                    // Try to parse time zone
                    in >> date::parse("%FT%T%Ez", startTime);

                    // Parse time zone
                    localZoneOffsetMinutes = Strings::iso8601toTimeZoneOffset(dateString)/60000;
                }

                // Get start day
                if (!in.fail()) {
                    startDay = date::floor<date::days>(startTime);
                    hasStartDate = true;
                }
            }
        }

        // Initialise parameters
        p.time = 0.0;
        p.maxSpeed = 0.0;
        p.area = 0.0;
        p.bandWidth = 0.0;
        lastTime = 0.0;

        // Set time to lower bound of source times
        if (sources.hasData()) {
            p.time = sources.getBounds().min.s;
            lastTime = p.time;
        }

        // Set minimum time step to smallest possible positive value
        TYPE timeStepMin = std::numeric_limits<TYPE>::epsilon();
        if (p.time > timeStepMin) 
            timeStepMin = std::nextafter(p.time, std::numeric_limits<TYPE>::max())-p.time;
        p.dt = timeStepMin;
        
        // Update times
        if (hasStartDate) {
            auto currentTime = startTime+std::chrono::seconds((uint64_t)((double)p.time));
            currentEpochMilliseconds = currentTime.time_since_epoch().count();
            p.JulianFraction = (TYPE)((double)(currentTime-startDay).count()/(double)86400000.0);
            p.JulianDate = (TYPE)DateCalculation::JulianDateFromEpoch<double>(currentEpochMilliseconds/1000.0);
        } else {
            currentEpochMilliseconds = (uint64_t)((double)p.time*1000.0);
            p.JulianFraction = (TYPE)((double)currentEpochMilliseconds/(double)86400000.0);
            p.JulianDate = (TYPE)DateCalculation::JulianDateFromEpoch<double>(currentEpochMilliseconds/1000.0);
        }

        // Re-project input layers
        if (sources.hasData() && sources.getProjectionParameters() != proj)
            sources = sources.convert(proj);
            
        // Calculate initial extent
        Dimensions<TYPE> dim;
        bool foundInitialDistanceLayer = false;
        std::size_t initialDistanceIndex = 0;
        std::string initialDistanceLayerName;

        // Find sources in timeslice
        auto bounds = sources.getBounds();
        currentSources = sources.region(BoundingBox<TYPE>( 
            { { bounds.min.p, bounds.min.q, 0.0, p.time }, { bounds.max.p, bounds.max.q, 0.0, p.time } } ) );
        
        if (currentSources.hasData()) {
        
            // Create layers
            layers.resize(LevelSetLayers::LevelSetLayers_END+inputLayers.size());
            
            // Get and check resolution
            resolution = 0.0;
            if (config["resolution"].is_number()) {
                resolution = (TYPE)config["resolution"].number_value();
            } else {
                throw std::runtime_error("No 'resolution' value supplied.");
            }
            if (resolution <= 0.0) {
                std::stringstream err;
                err << "Requested resolution " << resolution << " must be positive.";
                throw std::runtime_error(err.str());
            }

            // Set bandwidth
            p.bandWidth = (TYPE)narrowBandCells*resolution;

            // Get bounds
            dim = boundDimensions(currentSources);

        } else {

            // Get and check resolution
            if (config["initialDistanceLayer"].is_string()) {

                // Get distance layer name
                initialDistanceLayerName = config["initialDistanceLayer"].string_value();

                // Search for distance layer
                for (initialDistanceIndex = 0; initialDistanceIndex < inputLayers.size(); initialDistanceIndex++) {

                    // Get input name and compare
                    std::string name = inputLayers[initialDistanceIndex]->template getProperty<std::string>("name");
                    if (name == initialDistanceLayerName) {

                        // Set flag
                        foundInitialDistanceLayer = true;
                        break;
                    }
                }

                if (foundInitialDistanceLayer) {
                    
                    // Create layers
                    layers.resize(LevelSetLayers::LevelSetLayers_END+inputLayers.size()-1);

                    // Dimensions from layer
                    auto rasterDimensions = inputLayers[initialDistanceIndex]->getRasterDimensions();

                    // Use only 2D values
                    dim.nx = rasterDimensions.tx*tileSize;
                    dim.ny = rasterDimensions.ty*tileSize;
                    dim.nz = levels;
                    dim.hx = rasterDimensions.d.hx;
                    dim.hy = rasterDimensions.d.hy;
                    dim.hz = 1.0;
                    dim.ox = rasterDimensions.d.ox;
                    dim.oy = rasterDimensions.d.oy;
                    dim.oz = 0.0;

                } else {
                    std::stringstream err;
                    err << "Initial distance map '" << initialDistanceLayerName << "' not found in input layer list.";
                    throw std::runtime_error(err.str());
                }

            } else {
                throw std::runtime_error("No sources or initial distance map provided.");
            }
        }

        // Populate layers
        if (!classbits.init(dim)) 
            return false;
        classbits.setProjectionParameters(proj);

        for (auto &l : layers) {
            if (!l.init(dim)) 
                return false;
            l.setProjectionParameters(proj);
        }

        for (auto &pl : outputLayers) {
            if (!pl->init(dim)) 
                return false;
            pl->setProjectionParameters(proj);
        }

        // Set base names
        classbits.setProperty("name", "classbits");
        layers[LevelSetLayers::Distance].setProperty("name", "_distance");
        layers[LevelSetLayers::DistanceUpdate].setProperty("name", "_distance_update");
        layers[LevelSetLayers::Rate].setProperty("name", "_rate");
        layers[LevelSetLayers::Speed].setProperty("name", "_speed");
        layers[LevelSetLayers::Arrival].setProperty("name", "_arrival");
        layers[LevelSetLayers::Advect_x].setProperty("name", "advect_x");
        layers[LevelSetLayers::Advect_y].setProperty("name", "advect_y");
        //layers[LevelSetLayers::StartTime].setProperty("name", "_start_time");
        //layers[LevelSetLayers::StartTimeUpdate].setProperty("name", "_start_time_update");

        if (foundInitialDistanceLayer) {

            // Copy data
            runScript<TYPE>("_distance = " + initialDistanceLayerName + ";", 
                { layers[LevelSetLayers::Distance], *inputLayers[initialDistanceIndex] } );

            // Erase input layer
            inputLayers.erase(inputLayers.begin()+initialDistanceIndex);
        }

        // Set input layer names
        std::string initScriptPrefix;
        for (std::size_t i = 0; i < inputLayers.size(); i++) {

            // Get input name and set layer name
            std::string name = inputLayers[i]->template getProperty<std::string>("name");
            layers[LevelSetLayers::LevelSetLayers_END+i].setProperty("name", name);

            // Copy unaligned input data to aligned data
            initScriptPrefix += name + " = " + name + "_INPUT;\n";
        }

        // Set hour script
        std::string hourScript = std::string("uint hour = (uint)fmod(_p.Julian_fraction*24.0+")+std::to_string((double)localZoneOffsetMinutes/60.0)+", 24.0); \n";

        // Parse initialisation script
        initScript = std::regex_replace(initScript, std::regex("\\bstate\\b"), std::string("_class_state"));
        initScript = std::regex_replace(initScript, std::regex("\\bclass\\b"), std::string("_class_lo"));
        initScript = std::regex_replace(initScript, std::regex("\\bsubclass\\b"), std::string("_class_sub_lo"));
        initScript = std::regex_replace(initScript, std::regex("\\bJulian_date\\b"), std::string("_p.Julian_date"));
        initScript = initScriptPrefix+initScript;

        // Parse advect script
        advectScript = std::regex_replace(advectScript, std::regex("\\bstate\\b"), std::string("_class_state"));
        advectScript = std::regex_replace(advectScript, std::regex("\\bclass\\b"), std::string("_class_lo"));
        advectScript = std::regex_replace(advectScript, std::regex("\\bsubclass\\b"), std::string("_class_sub_lo"));
        advectScript = std::regex_replace(advectScript, std::regex("\\badvect_x\\b"), std::string("advect_x"));
        advectScript = std::regex_replace(advectScript, std::regex("\\badvect_y\\b"), std::string("advect_y"));
        advectScript = std::regex_replace(advectScript, std::regex("\\btime\\b"), std::string("_p.time"));
        advectScript = std::regex_replace(advectScript, std::regex("\\btime_step\\b"), std::string("_p.dt"));
        advectScript = std::regex_replace(advectScript, std::regex("\\bJulian_date\\b"), std::string("_p.Julian_date"));
        
        // Parse build script
        bool needsHour = false;
        bool needsNormal = false;
        bool needsAdvectNormal = false;
        bool needsAdvectVector = false;
        bool needsAdvectDotNormal = false;

        if (std::regex_search(buildScript, std::regex("\\bhour\\b"))) {
            needsHour = true;
        }
        if (std::regex_search(buildScript, std::regex("\\bnormal_vector\\b"))) {
            needsNormal = true;
        }
        if (std::regex_search(buildScript, std::regex("\\badvect_normal_vector\\b"))) {
            needsAdvectNormal = true;
        }
        if (std::regex_search(buildScript, std::regex("\\badvect_mag\\b"))) {
            buildScript = buildAdvectMag + buildScript;
        }
        if (std::regex_search(buildScript, std::regex("\\badvect_vector\\b"))) {
            needsAdvectVector = true;
        }
        if (std::regex_search(buildScript, std::regex("\\badvect_dot_normal\\b"))) {
            needsAdvectNormal = true;
            needsAdvectVector = true;
            needsAdvectDotNormal = true;
        }
        
        if (needsHour)
            buildScript = hourScript + buildScript;
        if (needsAdvectDotNormal)
            buildScript = buildAdvectDotNormal + buildScript;
        if (needsAdvectVector)
            buildScript = buildAdvectVector + buildScript;
        if (needsAdvectNormal)
            buildScript = buildAdvectNormal + buildScript;
        if (needsNormal)
            buildScript = buildNormal + buildScript;

        buildScript = std::regex_replace(buildScript, std::regex("\\bstate\\b"), std::string("_class_state"));
        buildScript = std::regex_replace(buildScript, std::regex("\\bclass\\b"), std::string("_class_lo"));
        buildScript = std::regex_replace(buildScript, std::regex("\\bsubclass\\b"), std::string("_class_sub_lo"));
        buildScript = std::regex_replace(buildScript, std::regex("\\btime\\b"), std::string("_p.time"));
        buildScript = std::regex_replace(buildScript, std::regex("\\btime_step\\b"), std::string("_p.dt"));
        buildScript = std::regex_replace(buildScript, std::regex("\\bJulian_date\\b"), std::string("_p.Julian_date"));
        buildScript = std::regex_replace(buildScript, std::regex("\\barrival\\b"), std::string("_arrival"));
        buildScript = std::regex_replace(buildScript, std::regex("\\bdistance\\b"), std::string("_distance"));
        //buildScript = std::regex_replace(buildScript, std::regex("\\bstart_time\\b"), std::string("_start_time"));
        
        // Parse update script
        needsHour = false;
        needsAdvectVector = false;

        if (std::regex_search(updateScript, std::regex("\\bhour\\b"))) {
            needsHour = true;
        }
        if (std::regex_search(updateScript, std::regex("\\badvect_mag\\b"))) {
            updateScript = buildAdvectMag + updateScript;
        }
        if (std::regex_search(updateScript, std::regex("\\badvect_vector\\b"))) {
            needsAdvectVector = true;
        }
        
        if (needsHour)
            updateScript = hourScript + updateScript;
        if (needsAdvectVector)
            updateScript = buildAdvectVector + updateScript;

        updateScript = std::regex_replace(updateScript, std::regex("\\bstate\\b"), std::string("_class_state"));
        updateScript = std::regex_replace(updateScript, std::regex("\\bclass\\b"), std::string("_class_lo"));
        updateScript = std::regex_replace(updateScript, std::regex("\\bsubclass\\b"), std::string("_class_sub_lo"));
        updateScript = std::regex_replace(updateScript, std::regex("\\btime\\b"), std::string("_p.time"));
        updateScript = std::regex_replace(updateScript, std::regex("\\btime_step\\b"), std::string("_p.dt"));
        updateScript = std::regex_replace(updateScript, std::regex("\\bJulian_date\\b"), std::string("_p.Julian_date"));
        updateScript = std::regex_replace(updateScript, std::regex("\\barrival\\b"), std::string("_arrival"));
        //updateScript = std::regex_replace(updateScript, std::regex("\\bstart_time\\b"), std::string("_start_time_update"));

        // ---------------------------------------------------------------------
        // Create initialisation kernel
        // Requires:
        //   classbits            - land classification layer (aligned)
        //   distance             - distance from perimeter (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)
        
        layers[LevelSetLayers::Distance].setNeedsWrite(false);
        layers[LevelSetLayers::Advect_x].setNeedsWrite(false);
        layers[LevelSetLayers::Advect_y].setNeedsWrite(false);

        initRefs = { 
            classbits, 
            layers[LevelSetLayers::Distance], 
            layers[LevelSetLayers::Advect_x], 
            layers[LevelSetLayers::Advect_y]
        };

        for (std::size_t i = 0; i < inputLayers.size(); i++)  {

            // Get input name and set layer name
            std::string name = inputLayers[i]->template getProperty<std::string>("name");

            // Prefix name of unaligned raster layer
            name += "_INPUT";
            inputLayers[i]->setProperty("name", name);

            initRefs.push_back(layers[i+LevelSetLayers::LevelSetLayers_END]);
        }
            
        for (std::size_t i = 0; i < inputLayers.size(); i++) 
            initRefs.push_back(*inputLayers[i]);

        for (std::size_t i = 0; i < outputLayers.size(); i++) 
            initRefs.push_back(*outputLayers[i]);
        
        // Generate kernel
        initKernelGenerator = Geostack::generateKernel<TYPE>(
            initScript, std::string(R_cl_level_set_init_c), initRefs, RasterNullValue::Null, variables);

        layers[LevelSetLayers::Distance].setNeedsWrite(true);
        layers[LevelSetLayers::Advect_x].setNeedsWrite(true);
        layers[LevelSetLayers::Advect_y].setNeedsWrite(true);
        
        // Set all input layers as read-only outside initialisation kernel
        for (std::size_t i = 0; i < inputLayers.size(); i++) {
            layers[i+LevelSetLayers::LevelSetLayers_END].setNeedsWrite(false);

            // Get input name and set layer name
            std::string name = inputLayers[i]->template getProperty<std::string>("name");

            // Remove prefix from name of unaligned raster layer
            name.erase(name.end()-6, name.end());
            inputLayers[i]->setProperty("name", name);
        }
        
        // ---------------------------------------------------------------------
        // Create advection kernel
        // Requires:
        //   classbits            - land classification layer (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)

        classbits.setNeedsWrite(false);
        layers[LevelSetLayers::Distance].setNeedsWrite(false);

        advectRefs = { 
            classbits, 
            layers[LevelSetLayers::Distance], 
            layers[LevelSetLayers::DistanceUpdate],
            layers[LevelSetLayers::Rate], 
            layers[LevelSetLayers::Speed],
            layers[LevelSetLayers::Advect_x], 
            layers[LevelSetLayers::Advect_y]
        };
        
        for (std::size_t i = 0; i < inputLayers.size(); i++) 
            advectRefs.push_back(layers[i+LevelSetLayers::LevelSetLayers_END]);

        for (std::size_t i = 0; i < outputLayers.size(); i++) 
            advectRefs.push_back(*outputLayers[i]);
        
        // Generate kernel
        advectKernelGenerator = Geostack::generateKernel<TYPE>(
            advectScript, std::string(R_cl_level_set_advect_c), advectRefs, RasterNullValue::Null, variables);

        classbits.setNeedsWrite(true);
        layers[LevelSetLayers::Distance].setNeedsWrite(true);
        
        // ---------------------------------------------------------------------
        // Create build kernel
        // Requires:
        //   classbits            - land classification layer (aligned, neighbours)
        //   distance             - distance from perimeter (aligned, neighbours)
        //   distance update      - updated distance from perimeter (aligned)
        //   rate                 - time derivative of update (aligned)
        //   speed                - speed of perimeter (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)

        classbits.setNeedsWrite(false);
        layers[LevelSetLayers::Distance].setNeedsWrite(false);
        layers[LevelSetLayers::Advect_x].setNeedsWrite(false);
        layers[LevelSetLayers::Advect_y].setNeedsWrite(false);

        buildRefs = { 
            classbits, 
            layers[LevelSetLayers::Distance], 
            layers[LevelSetLayers::DistanceUpdate],
            layers[LevelSetLayers::Rate], 
            layers[LevelSetLayers::Speed],
            layers[LevelSetLayers::Advect_x], 
            layers[LevelSetLayers::Advect_y] 
            //layers[LevelSetLayers::StartTime], 
            //layers[LevelSetLayers::StartTimeUpdate]
        };
        
        for (std::size_t i = 0; i < inputLayers.size(); i++) {
            buildRefs.push_back(layers[i+LevelSetLayers::LevelSetLayers_END]);
        }

        for (std::size_t i = 0; i < outputLayers.size(); i++) 
            buildRefs.push_back(*outputLayers[i]);
        
        // Generate kernel
        buildKernelGenerator = Geostack::generateKernel<TYPE>(
            buildScript, std::string(R_cl_level_set_build_c), buildRefs, RasterNullValue::Null, variables);

        classbits.setNeedsWrite(true);
        layers[LevelSetLayers::Distance].setNeedsWrite(true);
        layers[LevelSetLayers::Advect_x].setNeedsWrite(true);
        layers[LevelSetLayers::Advect_y].setNeedsWrite(true);
        
        // ---------------------------------------------------------------------
        // Create update kernel
        // Requires:
        //   classbits            - land classification layer (aligned)
        //   distance             - distance from perimeter (aligned)
        //   distance update      - updated distance from perimeter (aligned, neighbours)
        //   rate                 - time derivative of update (aligned)
        //   speed                - speed of perimeter (aligned)
        //   arrival              - arrival time (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)

        layers[LevelSetLayers::DistanceUpdate].setNeedsWrite(false);
        layers[LevelSetLayers::Rate].setNeedsWrite(false);
        layers[LevelSetLayers::Speed].setNeedsWrite(false);
        layers[LevelSetLayers::Advect_x].setNeedsWrite(false);
        layers[LevelSetLayers::Advect_y].setNeedsWrite(false);

        updateRefs = {
            classbits, 
            layers[LevelSetLayers::Distance], 
            layers[LevelSetLayers::DistanceUpdate], 
            layers[LevelSetLayers::Rate], 
            layers[LevelSetLayers::Speed], 
            layers[LevelSetLayers::Arrival],
            layers[LevelSetLayers::Advect_x], 
            layers[LevelSetLayers::Advect_y]
            //layers[LevelSetLayers::StartTime], 
            //layers[LevelSetLayers::StartTimeUpdate]
        };
        
        for (std::size_t i = 0; i < inputLayers.size(); i++) 
            updateRefs.push_back(layers[i+LevelSetLayers::LevelSetLayers_END]);

        for (std::size_t i = 0; i < outputLayers.size(); i++) 
            updateRefs.push_back(*outputLayers[i]);
        
        // Generate kernel
        updateKernelGenerator = Geostack::generateKernel<TYPE>(
            updateScript, std::string(R_cl_level_set_update_c), updateRefs, RasterNullValue::Null, variables);

        layers[LevelSetLayers::DistanceUpdate].setNeedsWrite(true);
        layers[LevelSetLayers::Rate].setNeedsWrite(true);
        layers[LevelSetLayers::Speed].setNeedsWrite(true);
        layers[LevelSetLayers::Advect_x].setNeedsWrite(true);
        layers[LevelSetLayers::Advect_y].setNeedsWrite(true);
        
        // ---------------------------------------------------------------------
        // Create reinitialisation kernel
        // Requires:
        //   distance             - distance from perimeter (aligned, neighbours)
        //   distance update      - updated distance from perimeter (aligned)
        //   speed                - speed of perimeter (aligned, neighbours)

        layers[LevelSetLayers::Distance].setNeedsWrite(false);
        layers[LevelSetLayers::Speed].setNeedsWrite(false);
        
        reinitReq = KernelRequirements( { Neighbours::Rook, Neighbours::None, Neighbours::Rook } );

        reinitRefs = { 
            layers[LevelSetLayers::Distance],
            layers[LevelSetLayers::DistanceUpdate], 
            layers[LevelSetLayers::Speed]
        };
        reinitFlipRefs = { 
            layers[LevelSetLayers::DistanceUpdate], 
            layers[LevelSetLayers::Distance], 
            layers[LevelSetLayers::Speed]
        };
        
        // Generate kernel
        reinitKernelGenerator = Geostack::generateKernel<TYPE>(
            "", std::string(R_cl_level_set_reinit_c), { 
                layers[LevelSetLayers::Distance], 
                layers[LevelSetLayers::DistanceUpdate], 
                layers[LevelSetLayers::Speed]
            });

        layers[LevelSetLayers::Distance].setNeedsWrite(true);
        layers[LevelSetLayers::Speed].setNeedsWrite(true);
        
        // ---------------------------------------------------------------------

        // Build script and get kernels
        KernelGenerator levelSetGenerator;
        levelSetGenerator.script = 
            std::string(R_cl_date_head_c) +
            std::string(R_cl_level_set_head_c) +
            advectKernelGenerator.script + 
            initKernelGenerator.script + 
            buildKernelGenerator.script + 
            updateKernelGenerator.script + 
            reinitKernelGenerator.script;

        levelSetGenerator.reqs.usesRandom = 
            advectKernelGenerator.reqs.usesRandom ||
            initKernelGenerator.reqs.usesRandom ||
            buildKernelGenerator.reqs.usesRandom ||
            updateKernelGenerator.reqs.usesRandom;

        levelSetGenerator.projectionTypes.insert(
            advectKernelGenerator.projectionTypes.begin(), advectKernelGenerator.projectionTypes.end());
        levelSetGenerator.projectionTypes.insert(
            initKernelGenerator.projectionTypes.begin(), initKernelGenerator.projectionTypes.end());
        levelSetGenerator.projectionTypes.insert(
            buildKernelGenerator.projectionTypes.begin(), buildKernelGenerator.projectionTypes.end());
        levelSetGenerator.projectionTypes.insert(
            updateKernelGenerator.projectionTypes.begin(), updateKernelGenerator.projectionTypes.end());
        levelSetGenerator.projectionTypes.insert(
            reinitKernelGenerator.projectionTypes.begin(), reinitKernelGenerator.projectionTypes.end());

        auto levelSetHash = buildRasterKernel(levelSetGenerator);
        if (levelSetHash == solver.getNullHash())
            return false;
            
        advectKernel = solver.getKernel(levelSetHash, "advect");
        advectInactiveKernel = solver.getKernel(levelSetHash, "advectInactive");
        initKernel = solver.getKernel(levelSetHash, "init");
        buildKernel = solver.getKernel(levelSetHash, "build");
        updateKernel = solver.getKernel(levelSetHash, "update");
        updateInactiveKernel = solver.getKernel(levelSetHash, "updateInactive");
        reinitKernel = solver.getKernel(levelSetHash, "reinitWeighted");
        buildKernelMultiple = solver.getKernelPreferredWorkgroupMultiple(buildKernel);
        
        // Clear internal variables
        activeTiles.clear();
        inactiveTiles.clear();
        newTileIndexes.clear();
        statusVector.clear();

        // Activate all tiles
        auto rDim = classbits.getRasterDimensions();
        for (uint32_t tj = 0; tj < rDim.ty; tj++) {
            for (uint32_t ti = 0; ti < rDim.tx; ti++) {

                // Add to active tile list
                activeTiles.insert( { ti, tj } );
                newTileIndexes.insert( { ti, tj } );
            }
        }

        // Set initialisation flag
        initialised = true;

        // Set internal rasters as non-writeable in scripts
        classbits.setNeedsWrite(false);
        layers[LevelSetLayers::Distance].setNeedsWrite(false);
        layers[LevelSetLayers::DistanceUpdate].setNeedsWrite(false);
        layers[LevelSetLayers::Rate].setNeedsWrite(false);
        layers[LevelSetLayers::Speed].setNeedsWrite(false);
        layers[LevelSetLayers::Arrival].setNeedsWrite(false);

        return true;
    }

    template <typename TYPE>
    bool LevelSet<TYPE>::step() {
    
        const uint32_t tileSize = TileMetrics::tileSize;

        // Check initialisation
        if (!initialised) {
            throw std::runtime_error("Solver not initialised.");
        }

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Get dimensions
        auto rDim = classbits.getRasterDimensions();

        // Update new tiles
        if (!newTileIndexes.empty()) {
            initKernel.setArg(0, p);
            for (auto &t : newTileIndexes) {
        
                // Get indexes
                uint32_t ti = t.first;
                uint32_t tj = t.second;                

                // Set new distance tile to twice the narrow band width
                layers[LevelSetLayers::Distance].setAllTileCellValues(ti, tj, (TYPE)2.0*p.bandWidth);
                layers[LevelSetLayers::DistanceUpdate].setAllTileCellValues(ti, tj, (TYPE)2.0*p.bandWidth);

                // Run initialisation kernel
                Geostack::runTileKernel<TYPE>(ti, tj, initKernel, initRefs, initKernelGenerator.reqs, 1, variables);
            }
            newTileIndexes.clear();
        }

        // Map sources
        if (currentSources.hasData()) {
            for (auto &t : activeTiles) {

                // Map sources
                layers[LevelSetLayers::Distance].mapTileVector(t.first, t.second, currentSources, "",
                    GeometryType::Point | GeometryType::LineString | GeometryType::Polygon, "radius", "level");
            }
        }

        // Set minimum time step to smallest possible positive value
        TYPE timeStepMin = std::numeric_limits<TYPE>::epsilon();
        if (p.time > timeStepMin) 
            timeStepMin = std::nextafter(p.time, std::numeric_limits<TYPE>::max())-p.time;

        // Update time step
        p.dt = std::min(CFL*resolution/(p.maxSpeed+1.0E-6), 2.0*p.dt);
        p.dt = std::max(p.dt, timeStepMin);
        p.dt = std::min(p.dt, timeStepMax);
            
        // Modify time step to lock to target multiples
        if (timeMultiple > 0.0 && fmod(p.time, timeMultiple) != 0.0) {
            TYPE dTarget = ceil(p.time/timeMultiple)*timeMultiple;
            TYPE tGap = dTarget-p.time;
            if (p.dt > tGap) {

                // If next time is greater than target set time step to match target
                p.dt = tGap;

            } else if (p.dt > 0.9*tGap) {
                
                // If next time step is close to target, reduce time step to 'soften' approach
                p.dt *= 0.75;
            }
        }

        // Change max speed to take timestep into account
        p.maxSpeed = resolution/p.dt;

        // Run reinitialisation kernel
        reinitKernel.setArg(0, p);
        reinitKernel.setArg(1, (TYPE)CFL);
        for (auto &t : activeTiles) {

            // Run reinitialisation kernel
            Geostack::runTileKernel<TYPE>(t.first, t.second, reinitKernel, reinitRefs, reinitKernelGenerator.reqs, 2);
        }
            
        for (auto &t : activeTiles) {
            
            // Run reinitialisation kernel
            Geostack::runTileKernel<TYPE>(t.first, t.second, reinitKernel, reinitFlipRefs, reinitKernelGenerator.reqs, 2);
        }

        // Check and update index variable
        if (rasterIndexCount.size() != activeTiles.size()+1) {

            // Resize data and create buffer
            rasterIndexCount.resize(activeTiles.size()+1);
            rasterIndexCountBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                rasterIndexCount.size()*sizeof(cl_uint), rasterIndexCount.data());
            auto rasterCountPtr = queue.enqueueMapBuffer(rasterIndexCountBuffer, CL_TRUE, CL_MAP_READ, 0, rasterIndexCount.size()*sizeof(cl_uint));

            // Check pointer
            if (rasterCountPtr != static_cast<void *>(rasterIndexCount.data()))
                throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

            // Resize index buffer
            rasterIndexBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, 
                (cl::size_type)levels*TileMetrics::tileSizeSquared*sizeof(LevelSetRasterIndex<TYPE>)*activeTiles.size());
        }

        // Build tile indexes
        uint32_t  tileCount = 0;
        queue.enqueueUnmapMemObject(rasterIndexCountBuffer, static_cast<void *>(rasterIndexCount.data()));
        queue.enqueueFillBuffer(rasterIndexCountBuffer, 0, 0, rasterIndexCount.size()*sizeof(cl_uint));
        advectKernel.setArg(0, p);
        advectKernel.setArg(2, rasterIndexCountBuffer);  
        advectKernel.setArg(3, rasterIndexBuffer);  
        for (auto &t : activeTiles) {

            // Run advection kernel for active tiles
            advectKernel.setArg(1, tileCount++);  
            Geostack::runTileKernel<TYPE>(t.first, t.second, advectKernel, advectRefs, advectKernelGenerator.reqs, 4, variables);
        }

        advectInactiveKernel.setArg(0, p);
        for (auto &t : inactiveTiles) {

            // Run advection kernel for inactive tiles
            Geostack::runTileKernel<TYPE>(t.first, t.second, advectInactiveKernel, advectRefs, advectKernelGenerator.reqs, 1, variables);
        }

        // Map to host and check pointer
        auto rasterCountPtr = queue.enqueueMapBuffer(rasterIndexCountBuffer, CL_FALSE, CL_MAP_READ, 0, rasterIndexCount.size()*sizeof(cl_uint), 0, &rasterIndexMapEvent); 
        if (rasterCountPtr != static_cast<void *>(rasterIndexCount.data()))
            throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
        
        // Check and update status variable
        if (statusVector.size() != activeTiles.size()+2) {

            // Resize data and create buffer
            statusVector.resize(activeTiles.size()+2);
            statusBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                statusVector.size()*sizeof(cl_uint), statusVector.data());
            auto statusBufferPtr = queue.enqueueMapBuffer(statusBuffer, CL_TRUE, CL_MAP_READ, 0, statusVector.size()*sizeof(cl_uint));

            // Check pointer
            if (statusBufferPtr != static_cast<void *>(statusVector.data()))
                throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
        }
        queue.enqueueUnmapMemObject(statusBuffer, static_cast<void *>(statusVector.data()));
        queue.enqueueFillBuffer(statusBuffer, 0, 0, statusVector.size()*sizeof(cl_uint));

        // Build first numerical integration stage
        tileCount = 0;
        cl_uint startIndex = 0;
        rasterIndexMapEvent.wait();
        buildKernel.setArg(0, p);
        buildKernel.setArg(3, rasterIndexBuffer);  
        buildKernel.setArg(4, levels);
        for (auto &t : activeTiles) {
        
            auto size = rasterIndexCount[tileCount+1];
            if (size > 0) {

                // Set build kernel arguments
                buildKernel.setArg(1, startIndex);
                buildKernel.setArg(2, size);
                Geostack::setTileKernelArguments<TYPE>(t.first, t.second, buildKernel, buildRefs, buildKernelGenerator.reqs, 5, variables);
            
                // Run build kernel
                std::size_t paddedSize = (std::size_t)(size+((buildKernelMultiple-(size%buildKernelMultiple))%buildKernelMultiple));
                queue.enqueueNDRangeKernel(buildKernel, cl::NullRange, cl::NDRange(paddedSize), cl::NDRange(buildKernelMultiple));
            }
            tileCount++;
            startIndex += size;
        }
        
        // Run second numerical integration stage and update
        tileCount = 0;
        updateKernel.setArg(0, p); 
        updateKernel.setArg(2, statusBuffer);  
        for (auto &t : activeTiles) {

            updateKernel.setArg(1, tileCount++);         
            Geostack::runTileKernel<TYPE>(t.first, t.second, updateKernel, updateRefs, updateKernelGenerator.reqs, 3, variables);
        }

        updateInactiveKernel.setArg(0, p);  
        for (auto &t : inactiveTiles) {      
            Geostack::runTileKernel<TYPE>(t.first, t.second, updateInactiveKernel, updateRefs, updateKernelGenerator.reqs, 1, variables);
        }
        
        // Map to host and check pointer
        auto statusBufferPtr = queue.enqueueMapBuffer(statusBuffer, CL_FALSE, CL_MAP_READ, 0, statusVector.size()*sizeof(cl_uint), 0, &statusMapEvent); 
        if (statusBufferPtr != static_cast<void *>(statusVector.data()))
            throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

        // Update time
        lastTime = p.time;
        p.time += p.dt;
        
        // Update times
        if (hasStartDate) {
            auto currentTime = startTime+std::chrono::seconds((uint64_t)((double)p.time));
            currentEpochMilliseconds = currentTime.time_since_epoch().count();
            p.JulianFraction = (TYPE)((double)(currentTime-startDay).count()/(double)86400000.0);
            p.JulianDate = (TYPE)DateCalculation::JulianDateFromEpoch<double>(currentEpochMilliseconds/1000.0);
        } else {
            currentEpochMilliseconds = (uint64_t)((double)p.time*1000.0);
            p.JulianFraction = (TYPE)((double)currentEpochMilliseconds/(double)86400000.0);
            p.JulianDate = (TYPE)DateCalculation::JulianDateFromEpoch<double>(currentEpochMilliseconds/1000.0);
        }
        
        // Skip sources at first time
        auto bounds = sources.getBounds();
        if (lastTime != bounds.min.s) {

            // Decrement next time by smallest possible increment to ensure sources are not applied twice
            TYPE timeMinusDelta = std::nextafter(p.time, lastTime);

            // Find sources in current time slice
            currentSources = sources.region(BoundingBox<TYPE>( 
                { { bounds.min.p, bounds.min.q, 0.0, lastTime }, { bounds.max.p, bounds.max.q, 0.0, timeMinusDelta } } ) );

        } else {

            // Clear any sources from first time step
            currentSources = Vector<TYPE>();
        }

        // Wait for mapping to complete
        statusMapEvent.wait();

        // Get minimum distance to boundary
        p.area = (TYPE)(statusVector[0]*resolution*resolution); // First status integer is count

        // Get maximum speed
        p.maxSpeed = (TYPE)statusVector[1]/65535.0; // Second status integer is fixed-point maximum speed

        // Check maximum speed
        if (p.maxSpeed == 0.0) {
            std::cout << "WARNING: Speed is zero everywhere." << std::endl;
            return false;
        } else if (p.maxSpeed == 65535.0) {
            std::cout << "WARNING: Maximum speed exceeded." << std::endl;
            return false;
        }

        // Build active tile map
        tileCount = 0;
        cl_uint resizeBits = 0;
        for (auto &t : activeTiles) {

            // Status integers from 1 onwards are tile boundary flags
            cl_uint tileStatus = statusVector[2+tileCount++];

            if (tileStatus & 0xF) {

                // Get indexes
                uint32_t ti = t.first;
                uint32_t tj = t.second;
            
                // North boundary
                if (tileStatus & 0x01 && tj >= rDim.ty-1) {
                    resizeBits |= 0x01;
                }
                
                // East boundary
                if (tileStatus & 0x02 && ti >= rDim.tx-1) {
                    resizeBits |= 0x02;
                }
                
                // South boundary
                if (tileStatus & 0x04 && tj <= 0) {
                    resizeBits |= 0x04;
                }
                
                // West boundary
                if (tileStatus & 0x08 && ti <= 0) {
                    resizeBits |= 0x08;
                }
            }
        }

        // Expand raster if perimeter is approaching boundary
        if (resizeBits != 0) {
            
            // Set new size
            uint32_t ox = 0;
            uint32_t oy = 0;
            if (resizeBits & 0x01) {
                rDim.ty+=1;
            }
            if (resizeBits & 0x02) {
                rDim.tx+=1;
            }
            if (resizeBits & 0x04) {
                rDim.ty+=1;
                oy = 1;
            }
            if (resizeBits & 0x08) {
                rDim.tx+=1;
                ox = 1;
            }

            // Resize domain
            resizeTiles(rDim.tx, rDim.ty, ox, oy);

            // Update tile indexes
            std::set<std::pair<uint32_t, uint32_t> > updatedTiles;
            for (auto &t : activeTiles) {
                updatedTiles.insert( { t.first+ox, t.second+oy } );
            }
            activeTiles = updatedTiles;

            updatedTiles.clear();
            for (auto &t : inactiveTiles) {
                updatedTiles.insert( { t.first+ox, t.second+oy } );
            }
            inactiveTiles = updatedTiles;
        }

        // Rebuild active tile map
        tileCount = 0;
        tileIndexSet newActiveTiles;
        for (auto &t : activeTiles) {

            // Status integers from 1 onwards are tile boundary flags
            cl_uint tileStatus = statusVector[2+tileCount++];

            if (tileStatus != 0) {
            
                // Get indexes
                uint32_t ti = t.first;
                uint32_t tj = t.second;

                // Add tile
                newActiveTiles.insert( { ti, tj } );
                    
                // North boundary
                if (tileStatus & 0x01 && tj < rDim.ty-1) {
                    newActiveTiles.insert( { ti, tj+1 } );
                }
                    
                // East boundary
                if (tileStatus & 0x02 && ti < rDim.tx-1) {
                    newActiveTiles.insert( { ti+1, tj } );
                }
                    
                // South boundary
                if (tileStatus & 0x04 && tj > 0) {
                    newActiveTiles.insert( { ti, tj-1 } );
                }
                    
                // West boundary
                if (tileStatus & 0x08 && ti > 0) {
                    newActiveTiles.insert( { ti-1, tj } );
                }
            }
        }

        // Check for changes in active tiles
        if (activeTiles != newActiveTiles) {

            // Update inactive tiles
            for (auto &t : activeTiles) {
                inactiveTiles.insert(t);
            }
            for (auto &t : newActiveTiles) {
                inactiveTiles.erase(t);
            }

            // Set change flag and update active tiles
            activeTiles = newActiveTiles; 
        }
        
        if (currentSources.hasData()) {
        
            // Get distance map bounds
            auto bounds = currentSources.getBounds();
            bounds.extend((TYPE)2.0*p.bandWidth);

            // Calculate distances
            uint32_t nxp = (uint32_t)ceil(std::max((TYPE)0.0, bounds.max.p-rDim.ex)/resolution);
            uint32_t nyp = (uint32_t)ceil(std::max((TYPE)0.0, bounds.max.q-rDim.ey)/resolution);
            uint32_t nxm = (uint32_t)ceil(std::max((TYPE)0.0, rDim.d.ox-bounds.min.p)/resolution);
            uint32_t nym = (uint32_t)ceil(std::max((TYPE)0.0, rDim.d.oy-bounds.min.q)/resolution);

            // Calculate number of tiles
            uint32_t ntxp = (nxp+((tileSize-(nxp%tileSize))%tileSize))/tileSize;
            uint32_t ntyp = (nyp+((tileSize-(nyp%tileSize))%tileSize))/tileSize;
            uint32_t ntxm = (nxm+((tileSize-(nxm%tileSize))%tileSize))/tileSize;
            uint32_t ntym = (nym+((tileSize-(nym%tileSize))%tileSize))/tileSize;
            
            // Resize domain
            rDim.tx+=ntxp+ntxm;
            rDim.ty+=ntyp+ntym;
            resizeTiles(rDim.tx, rDim.ty, ntxm, ntym);

            // Update dimensions
            rDim = classbits.getRasterDimensions();

            // Update tile indexes
            std::set<std::pair<uint32_t, uint32_t> > updatedTiles;
            for (auto &t : activeTiles) {
                updatedTiles.insert( { t.first+ntxm, t.second+ntym } );
            }
            activeTiles = updatedTiles;

            updatedTiles.clear();
            for (auto &t : inactiveTiles) {
                updatedTiles.insert( { t.first+ntxm, t.second+ntym } );
            }
            inactiveTiles = updatedTiles;

            // Activate all tiles within new source bounds
            uint32_t si = (uint32_t)((bounds.min.p-rDim.d.ox)/((TYPE)tileSize*resolution));
            uint32_t sj = (uint32_t)((bounds.min.q-rDim.d.oy)/((TYPE)tileSize*resolution));
            uint32_t ei = (uint32_t)((bounds.max.p-rDim.d.ox)/((TYPE)tileSize*resolution));
            uint32_t ej = (uint32_t)((bounds.max.q-rDim.d.oy)/((TYPE)tileSize*resolution));

            for (uint32_t tj = sj; tj <= ej; tj++) {
                for (uint32_t ti = si; ti <= ei; ti++) {
                    std::pair<uint32_t, uint32_t> t = { ti, tj };
                    activeTiles.insert(t);
                    inactiveTiles.erase(t);
                }
            }
        }

        // Increment iteration count
        iters++;

        return true;
    }
   
    template <typename TYPE>
    void LevelSet<TYPE>::addSource(const Vector<TYPE> &v) {

        // Check projection
        auto proj = sources.getProjectionParameters();
        if (v.getProjectionParameters() != proj) {
            sources += v.convert(proj);
        } else {
            sources += v;
        }
    }

    // Float type definitions
    template class LevelSet<float>;

    // Double type definitions
    template class LevelSet<double>;
}
