/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <iostream>
#include <sstream>
#include <regex>

#include "json11/json11.hpp"

#include "gs_solver.h"
#include "gs_property.h"
#include "gs_particle.h"

namespace Geostack
{
    using namespace json11;

    template <typename TYPE>
    bool Particle<TYPE>::init(
        std::string jsonConfig,
        Vector<TYPE> particles_,
        std::shared_ptr<Variables<TYPE, std::string> > variables_,
        std::vector<RasterBasePtr<TYPE> > inputLayers_) {

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Reset iterations
        iters = 0;

        // Copy start conditions and layers
        particles = particles_;
        variables = variables_;
        inputLayers = inputLayers_;

        // Parse configuration
        std::string jsonErr;
        auto config = Json::parse(jsonConfig, jsonErr);
        if (!jsonErr.empty()) {
            throw std::runtime_error(jsonErr);
        }

        // Get and check parameters
        dt = 0.0;
        if (config["dt"].is_number()) {
            dt = (REAL)config["dt"].number_value();
        }
        if (dt <= 0.0) {
            throw std::runtime_error("Time step must be greater than zero.");
        }

        uint32_t nsteps = 10;
        if (config["adaptiveMaximumSteps"].is_number()) {
            nsteps = (uint32_t)config["adaptiveMaximumSteps"].number_value();
        }
        if (nsteps == 0 || nsteps > 16) {
            throw std::runtime_error("Maximum adaptive steps must be between 1 and 2^16.");
        }

        uint32_t adaptiveMaximumSteps = 10;
        if (config["adaptiveMaximumSteps"].is_number()) {
            adaptiveMaximumSteps = (uint32_t)config["adaptiveMaximumSteps"].number_value();
        }
        if (adaptiveMaximumSteps == 0 || adaptiveMaximumSteps > 16) {
            throw std::runtime_error("Maximum adaptive steps must be between 1 and 2^16.");
        }

        double adaptiveTolerance = 1.0E-3;
        if (config["adaptiveTolerance"].is_number()) {
            adaptiveTolerance = (double)config["adaptiveTolerance"].number_value();
        }
        if (adaptiveTolerance < 0.0) {
            throw std::runtime_error("Adaptive tolerance must be greater than zero.");
        }        

        // Get scripts
        std::string initScript;
        if (config["initialisationScript"].is_string()) {
            initScript = config["initialisationScript"].string_value();
        }
        if (initScript.length() == 0) {
            std::cout << "WARNING: No 'initialisationScript' value supplied." << std::endl;
        }
        
        std::string advectScript;
        if (config["advectionScript"].is_string()) {
            advectScript = config["advectionScript"].string_value();
        }
        if (advectScript.length() == 0) {
            std::cout << "WARNING: No 'advectionScript' value supplied, default advection vector of (0, 0, 0) applied." << std::endl;
        }
        
        std::string postUpdateScript;
        if (config["postUpdateScript"].is_string()) {
            postUpdateScript = config["postUpdateScript"].string_value();
        }
        
        pcx = 0.0; pcy = 0.0; pcz = 0.0;
        pnx = 0.0; pny = 0.0; pnz = 0.0;
        bool useSamplePlane = false;
        if (config["samplingPlane"].is_object()) {
            auto samplingPlane = config["samplingPlane"];

            // Get plane point
            if (samplingPlane["point"].is_array()) {
                auto point = samplingPlane["point"].array_items();
                if (point.size() == 3) {
                    pcx = (TYPE)point[0].number_value();
                    pcy = (TYPE)point[1].number_value();
                    pcz = (TYPE)point[2].number_value();
                    
                    // Get plane normal
                    if (samplingPlane["normal"].is_array()) {
                        auto normal = samplingPlane["normal"].array_items();
                        if (normal.size() == 3) {
                            pnx = (TYPE)normal[0].number_value();
                            pny = (TYPE)normal[1].number_value();
                            pnz = (TYPE)normal[2].number_value();

                            // Ensure normal is normalised
                            TYPE norm = pnx*pnx+pny*pny+pnz*pnz;
                            if (norm != 0.0) {
                                pnx/=norm;
                                pny/=norm;
                                pnz/=norm;
                    
                                // Set sample plane flag
                                useSamplePlane = true;
                            }
                        }
                    }
                }
            }
        }

        // Search for fields
        fields.clear();
        auto &properties = particles.getProperties();
        for (auto n: properties.getPropertyNames()) {
            if (std::regex_search(initScript, std::regex("\\b" + n + "\\b")) || 
                std::regex_search(advectScript, std::regex("\\b" + n + "\\b")) || 
                std::regex_search(postUpdateScript, std::regex("\\b" + n + "\\b"))) {
                auto type = properties.getPropertyType(n);
                if (type == PropertyType::Undefined ||
                    #if defined(REAL_FLOAT)
                    type == PropertyType::Float) {
                    #elif defined(REAL_DOUBLE)
                    type == PropertyType::Double) {
                    #endif

                    // Add index, integer and float fields, assume undefined will be populated
                    fields.push_back(n);
                }
            }
        }

        // Set variables
        std::string fieldArgsList, variableList, postList;
        for (std::size_t i = 0; i < fields.size(); i++) {
                
            std::string &fieldName = fields[i];
            
            // Add to argument list
            fieldArgsList += ",\n__global REAL *_" + fieldName;

            // Update script for field value
            variableList += "REAL " + fieldName + " = *(_" + fieldName + "+index);\n";
            
            // Update script to write field value
            postList += "*(_" + fieldName + "+index) = " + fieldName + ";\n";
        }

        // Parse scripts
        advectScript = Solver::processScript(advectScript);
        advectScript = std::regex_replace(advectScript, std::regex("\\brandom\\b"), std::string("random_REAL(_prsl)"));
        advectScript = std::regex_replace(advectScript, std::regex("\\brandomNormal\\("), std::string("randomNormal_REAL(_prsl, "));

        postUpdateScript = Solver::processScript(postUpdateScript);
        postUpdateScript = std::regex_replace(postUpdateScript, std::regex("\\brandom\\b"), std::string("random_REAL(_prsl)"));
        postUpdateScript = std::regex_replace(postUpdateScript, std::regex("\\brandomNormal\\("), std::string("randomNormal_REAL(_prsl, "));
        
        std::vector<std::string> updateScripts;
        if (config["updateScript"].is_array()) {
            for (auto script : config["updateScript"].array_items()) {
                updateScripts.push_back(script.string_value());
            }
        } else if (config["updateScript"].is_string()) {
            updateScripts.push_back(config["updateScript"].string_value());
        }
        if (updateScripts.size() == 0 || updateScripts[0].length() == 0) {
            std::cout << "WARNING: No 'updateScript' value supplied." << std::endl;
            updateScripts.push_back("");
        }
        for (auto &script : updateScripts) {
            script = Solver::processScript(script);
        }

        // Check for stochastic solve
        if (updateScripts.size() >= 2) {
            
            // Check for individual scripts
            std::vector<std::string> stochasticUpdateScripts(3);
            auto mu_regex = std::regex("\\bmu\\b");
            auto sigma_regex = std::regex("\\bsigma\\b");
            auto dW_regex = std::regex("\\bdW\\b");
            for (auto &script : updateScripts) {
                if (std::regex_search(script, mu_regex)) {
                    stochasticUpdateScripts[0] = script;
                } else if (std::regex_search(script, sigma_regex)) {
                    stochasticUpdateScripts[1] = script;
                } else if (std::regex_search(script, dW_regex)) {
                    stochasticUpdateScripts[2] = script;
                }
            }

            // Check required scripts are present
            if (stochasticUpdateScripts[0].size() != 0 && stochasticUpdateScripts[1].size() != 0) {
                
                // Add default uncorrelated Wiener process
                if (stochasticUpdateScripts[2].size() == 0) {                
                    stochasticUpdateScripts[2] = R"(
                    REAL sqrt_dt = sqrt(dt);
                    dW = (REALVEC3)(
                        sigma.x != 0.0 ? randomNormal_REAL(_prsl, 0.0, sqrt_dt) : 0.0,
                        sigma.y != 0.0 ? randomNormal_REAL(_prsl, 0.0, sqrt_dt) : 0.0,
                        sigma.z != 0.0 ? randomNormal_REAL(_prsl, 0.0, sqrt_dt) : 0.0);
                    )";
                }
                
                // Update scripts
                isStochastic = true;
                updateScripts = stochasticUpdateScripts;
                
                // Replace random definitions
                for (auto &script : updateScripts) {
                    script = std::regex_replace(script, std::regex("\\brandom\\b"), std::string("random_REAL(_prsl)"));
                    script = std::regex_replace(script, std::regex("\\brandomNormal\\("), std::string("randomNormal_REAL(_prsl, "));
                }
            }
        }
        
        // Create initialisation kernel
        initScript = Solver::processScript(initScript);
        initScript = std::regex_replace(initScript, std::regex("\\brandom\\b"), std::string("random_REAL(_prsl)"));
        initScript = std::regex_replace(initScript, std::regex("\\brandomNormal\\("), std::string("randomNormal_REAL(_prsl, "));
        std::string initKernelStr = std::string(R_cl_random_c)+std::string(R_cl_particle_init_c);
        initKernelStr = std::regex_replace(initKernelStr, std::regex("\\/\\*__CODE__\\*\\/"), initScript);
        initKernelStr = std::regex_replace(initKernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), fieldArgsList);
        initKernelStr = std::regex_replace(initKernelStr, std::regex("\\/\\*__VARS__\\*\\/"), variableList);
        
        auto initHash = solver.buildProgram(initKernelStr);
        if (initHash == solver.getNullHash()) {
            throw std::runtime_error("Cannot build program");
        }            
        initKernel = solver.getKernel(initHash, "init");
        initWorkitemMultiple = solver.getKernelPreferredWorkgroupMultiple(initKernel);
        initWorkgroupSize = solver.getKernelWorkgroupSize(initKernel);

        // Create update kernel 
        std::string updateKernelStr = std::string(R_cl_random_c)+std::string(R_cl_particle_update_c);
        updateKernelStr = std::regex_replace(updateKernelStr, std::regex("__MAX_STEPS__"), std::to_string(adaptiveMaximumSteps));
        updateKernelStr = std::regex_replace(updateKernelStr, std::regex("__TOLERANCE__"), std::to_string(adaptiveTolerance));
        if (useSamplePlane) {
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__SAMPLE__|__SAMPLE__\\*\\/"), std::string(""));
        }
        if (isStochastic) {
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__INIT2__\\*\\/"), advectScript);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__CODE2__\\*\\/"), updateScripts[0]);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__CODE3__\\*\\/"), updateScripts[1]);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__CODE4__\\*\\/"), updateScripts[2]);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), fieldArgsList);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__VARS__\\*\\/"), variableList);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__POST1__\\*\\/"), postUpdateScript);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__POST2__\\*\\/"), postList);
        } else {
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__INIT1__\\*\\/"), advectScript);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__CODE1__\\*\\/"), updateScripts[0]);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), fieldArgsList);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__VARS__\\*\\/"), variableList);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__POST1__\\*\\/"), postUpdateScript);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__POST2__\\*\\/"), postList);
        }

        auto updateHash = solver.buildProgram(updateKernelStr);
        if (updateHash == solver.getNullHash()) {
            throw std::runtime_error("Cannot build program");
        }
        if (isStochastic) {
            updateKernel = solver.getKernel(updateHash, "updateStochastic");
        } else {
            updateKernel = solver.getKernel(updateHash, "update");
        }
        updateWorkitemMultiple = solver.getKernelPreferredWorkgroupMultiple(updateKernel);
        updateWorkgroupSize = solver.getKernelWorkgroupSize(updateKernel);

        // Create sample plane buffer
        pci = 0;
        bci = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), &pci);
        pbci = queue.enqueueMapBuffer(bci, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
        if (pbci != static_cast<void *>(&pci))
            throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

        return true;
    }

    template <typename TYPE>
    bool Particle<TYPE>::step() {

        // Check size
        if (particles.hasData()) {

            // Get OpenCL solver handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            
            // Get particle count
            auto nParticles = particles.getVertexSize();

            // Set field kernel arguments
            std::vector<TYPE> field;
            const auto &pointIndexes = particles.getPointIndexes();
            if (nParticles != pointIndexes.size()) {
                throw std::runtime_error("Particles must be point geometry");
            }

            // Check dynamics vector size
            if (dynamics.size() != nParticles) {

                // Resize vectors
                auto lastSize = dynamics.size();
                dynamics.getData().resize(nParticles);
                randomState.getData().resize(nParticles*4);

                // Populate initialisation kernel
                cl_uint arg = 0;
                initKernel.setArg(arg++, particles.getVertexBuffer());
                initKernel.setArg(arg++, dynamics.getBuffer());
                initKernel.setArg(arg++, (cl_uint)lastSize);
                initKernel.setArg(arg++, (cl_uint)nParticles);
                initKernel.setArg(arg++, (cl_uint)1418072); // Random seed
                initKernel.setArg(arg++, randomState.getBuffer());

                // Add fields
                for (auto &fieldName : fields) {
                    initKernel.setArg(arg++, particles.getPropertyBuffer(fieldName));   
                }                          

                // Execute initialisation kernel
                auto size = nParticles-lastSize;
                size_t particlePaddedWorkgroupSize = 
                    (size_t)(size+((initWorkitemMultiple-(size%initWorkitemMultiple))%initWorkitemMultiple));
                queue.enqueueNDRangeKernel(initKernel, cl::NullRange, 
                    cl::NDRange(particlePaddedWorkgroupSize), cl::NDRange(initWorkitemMultiple));
            }
            
            // Check sample plane vector size
            pi.getData().assign(nParticles, getNullValue<cl_uint>());
            pci = 0;
            queue.enqueueUnmapMemObject(bci, pbci);

            // Populate update kernel arguments
            cl_uint arg = 0;
            if (isStochastic) {
                updateKernel.setArg(arg++, particles.getVertexBuffer());
                updateKernel.setArg(arg++, (cl_uint)nParticles);
                updateKernel.setArg(arg++, dt);
                updateKernel.setArg(arg++, randomState.getBuffer());
            } else {
                updateKernel.setArg(arg++, particles.getVertexBuffer());
                updateKernel.setArg(arg++, dynamics.getBuffer());
                updateKernel.setArg(arg++, (cl_uint)nParticles);
                updateKernel.setArg(arg++, dt);
                updateKernel.setArg(arg++, randomState.getBuffer());
                updateKernel.setArg(arg++, pi.getBuffer());
                updateKernel.setArg(arg++, bci);
                updateKernel.setArg(arg++, pcx);
                updateKernel.setArg(arg++, pcy);
                updateKernel.setArg(arg++, pcz);
                updateKernel.setArg(arg++, pnx);
                updateKernel.setArg(arg++, pny);
                updateKernel.setArg(arg++, pnz);
            }

            // Add fields
            for (auto &fieldName : fields) {
                updateKernel.setArg(arg++, particles.getPropertyBuffer(fieldName));   
            }                

            // Execute update kernel
            size_t particlePaddedWorkgroupSize = 
                (size_t)(nParticles+((updateWorkitemMultiple-(nParticles%updateWorkitemMultiple))%updateWorkitemMultiple));
            queue.enqueueNDRangeKernel(updateKernel, cl::NullRange, 
                cl::NDRange(particlePaddedWorkgroupSize), cl::NDRange(updateWorkitemMultiple));

            // Map sample count buffer
            pbci = queue.enqueueMapBuffer(bci, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
            if (pbci != static_cast<void *>(&pci))
                throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
        }

        return true;
    }

    template <typename TYPE>
    void Particle<TYPE>::setTimeStep(TYPE dt_) {

        // Check time step
        if (dt_ <= 0.0) {
            throw std::runtime_error("Time step must be greater than zero.");
        }

        // Update time step
        dt = dt_;
    }

    template <typename TYPE>
    void Particle<TYPE>::addParticles(Vector<TYPE> particles_) {

        if (particles_.hasData()) {

            // Add particles to solver
            auto proj = particles.getProjectionParameters();
            if (particles.getProjectionParameters() != proj) {

                // Convert projection
                particles += particles_.convert(proj);
            } else {
                particles += particles_;
            }
        }
    }
    
    template <typename TYPE>
    cl_uint Particle<TYPE>::getSamplePlaneIndexCount() {
        return pci;
    }
    
    template <typename TYPE>
    std::vector<cl_uint> Particle<TYPE>::getSamplePlaneIndexes() {

        // Get sample plane indexes
        std::vector<cl_uint> r;
        if (!isStochastic) {
            if (pci > 0) {
                auto &iv = pi.getData();
                r.assign(iv.begin(), iv.begin()+std::min((std::size_t)pci, iv.size()));
            }
        }
        return r;
    }
    
    template <typename TYPE>
    Vector<TYPE> &Particle<TYPE>::getParticles() {
        return particles;
    }

    // Float type definitions
    template class Particle<float>;

    // Double type definitions
    template class Particle<double>;
}
