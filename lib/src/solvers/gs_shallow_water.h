/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef GEOSTACK_SW_LB_SOLVER_H
#define GEOSTACK_SW_LB_SOLVER_H

#include <string>
#include <vector>
#include <set>

#include "date.h"
#include "gs_raster.h"
#include "gs_variables.h"

// Link to external resources
extern const char R_cl_shallow_water_init_c[];
extern const uint32_t R_cl_shallow_water_init_size;
extern const char R_cl_shallow_water_collision_c[];
extern const uint32_t R_cl_shallow_water_collision_size;
extern const char R_cl_shallow_water_streaming_c[];
extern const uint32_t R_cl_shallow_water_streaming_size;

namespace Geostack
{
    /**
    * %ShallowWater class for shallow water flow. 
    */
    template <typename TYPE>
    class ShallowWater {

        public:
        
            /**
            * ShallowWater constructor
            */
            ShallowWater():initialised(false), dt(0.0), cd(0.0) { }

            // Initialisation
            bool init(std::string jsonConfig, Raster<TYPE, TYPE> &h_in, Raster<TYPE, TYPE> &b_in);

            // Execution
            bool step();

        private:

            // Internal variables
            volatile bool initialised; ///< Solver initialised
            TYPE dt;                   ///< Solver timestep
            TYPE tau;                  ///< Relaxation time
            TYPE cd;                   ///< Drag coefficient
            std::set<std::pair<uint32_t, uint32_t> > activeTiles;

            // Internal generators
            KernelGenerator initKernelGenerator;        ///< Initialisation generator
            KernelGenerator collisionKernelGenerator;   ///< Collision generator
            KernelGenerator streamingKernelGenerator;   ///< Streaming generator

            RasterBaseRefs<TYPE> initRefs;        ///< Initialisation kernel rasters
            RasterBaseRefs<TYPE> collisionRefs;   ///< Collision kernel rasters
            RasterBaseRefs<TYPE> streamingRefs;   ///< Streaming kernel rasters
            
            // Internal kernels
            cl::Kernel initKernel;        ///< Initialisation kernel
            cl::Kernel collisionKernel;   ///< Collision kernel
            cl::Kernel streamingKernel;   ///< Streaming kernel

            // Status data
            std::vector<cl_uint> statusVector; ///< Vector to hold boundary approach flags
            cl::Buffer statusBuffer;           ///< Buffer to hold boundary approach flags

            // Internal rasters
            Raster<TYPE, TYPE> h;  // Height raster
            Raster<TYPE, TYPE> b;  // Base raster
            Raster<TYPE, TYPE> uh; // Unit discharge x component
            Raster<TYPE, TYPE> vh; // Unit discharge y component
            Raster<TYPE, TYPE> fe; // Population equilibrium
            Raster<TYPE, TYPE> fi; // Population grid at current timestep
            Raster<TYPE, TYPE> ft; // Population grid at next timestep
    };
}

#endif