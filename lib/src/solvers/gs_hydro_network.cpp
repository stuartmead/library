/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <set>
#include <iostream>
#include <sstream>
#include <eigen/Dense>
#include <eigen/Sparse>

#include "json11.hpp"

#include "gs_property.h"
#include "gs_hydro_network.h"

namespace Geostack
{

    using namespace json11;

	//SRM not really needed - but will compile wrong right now
    typedef Eigen::Triplet<double> Et;
    
    
    template <typename T>
    bool HydroNetworkFlowSolver<T>::init(Vector<T> &networkIn, std::string jsonConfig) {
        // Clear internal copy of network
        network.clear();
        linkPoints.clear();
        timeArray.clear();
    
        // Copy network projection
        network.setProjectionParameters(networkIn.getProjectionParameters());
    
        // Parse config
        Json config;
        if (!jsonConfig.empty()) {
                std::string err;
                config = Json::parse(jsonConfig, err);
                if (!err.empty()) {
                        throw std::runtime_error(err);
                }
        }
    
        /*
           double constant = 1.0;
           if (config["constant"].is_number()) {
           constant = config["constant"].number_value();
           } else {
           std::cout << "WARNING: No 'constant' value supplied, using default value of " << constant << std::endl;
           }
    
           int defaultSegmentType = static_cast<int>(HydroNetworkSegmentType::DynamicWave);
           if (config["defaultLinkType"].is_number()) {
           defaultSegmentType = config["defaultLinkType"].int_value();
           } else {
           std::cout << "WARNING: No 'default link type' value supplied, using default value of " << defaultSegmentType << std::endl;
           }
           */
    
        /* SRM elevation and distance arrays should not be default config
           std::vector<json11::Json> elevArray;
           if (config["elevs"].is_array()) {
           elevArray = config["elevs"].array_items();
           }
    
           std::vector<json11::Json> distArray;
           if (config["dists"].is_array()) {
           distArray = config["dists"].array_items();
           }
    
    */
        /* SRM dont need this 
           double defaultDiameter = 0.0;
           if (config["defaultDiameter"].is_number()) {
           defaultDiameter = config["defaultDiameter"].number_value();
           }
    
           double defaultArea = 0.0;
           if (config["defaultArea"].is_number()) {
           defaultArea = config["defaultArea"].number_value();
           }
           */
    
        T defaultMannings = 0.023;
        if (config["defaultMannings"].is_number()) {
                defaultMannings = config["defaultMannings"].number_value();
        }
    
        // TODO: Allow config override of default values. <07-10-21, NJM> //
        // Below doesn't work. 
        std::vector<T> defaultHead = {0.0};
        // if (config["defaultHead"].is_array()) {
        //         defaultHead = config["defaultHead"].array_items();
        // }
        std::vector<T> defaultFlow = {0.0};
        // if (config["defaultFlow"].is_array()) {
        //         defaultFlow = config["defaultFlow"].array_items();
        // }
    
        //Get properties - only type, head, and flow should be defined at T=0
        PropertyMap &properties = network.getProperties();
        PropertyMap &propertiesIn = networkIn.getProperties();
        bool hasType = propertiesIn.hasProperty("type");
        bool hasHead = propertiesIn.hasProperty("head");
        bool hasFlow = propertiesIn.hasProperty("flow");
        bool hasMannings = propertiesIn.hasProperty("mannings");
        bool hasElevs = propertiesIn.hasProperty("elevs");
        bool hasDists = propertiesIn.hasProperty("dists");
        //SRM - template for other propes we need
        //bool hasDiameter = propertiesIn.hasProperty("diameter");
    
        // Copy points
        auto bounds = networkIn.getBounds();
        auto pointIndexes = networkIn.getPointIndexes();
        auto linkIndexes = networkIn.getLineStringIndexes();
        std::size_t nDuplicatePoints = 0;
        std::multimap<uint64_t, std::size_t> zIndexes;
        std::cout << "Simulation will run over " << pointIndexes.size() << " points and " << linkIndexes.size() << " links." << std::endl;
    
        for (std::size_t k = 0; k < pointIndexes.size(); k++) {
                // Get coordinate
                //std::cout << "working on point" << k << std::endl;
                auto c = networkIn.getPointCoordinate(pointIndexes[k]);
                // Get z-index of coordinate
                uint64_t zIndex = bounds.createZIndex2D(c);
    
                // Check for duplicate points
                bool noDuplicates = true;
                auto indexRange = zIndexes.equal_range(zIndex);
                for (auto it = indexRange.first; it != indexRange.second; ++it) {
                        if (c == network.getPointCoordinate(it->second)) {
                                noDuplicates = false;
                        }
                }
    
                if (noDuplicates) {
                        auto id = network.addPoint(c);
                        zIndexes.insert( { zIndex, id } );
                        network.template setProperty<cl_uint>(id, "id", id);
    
                       // Copy data
                       if (hasHead) {
                                /*
                                   network.template setProperty<double>(id, "head",
                                   networkIn.template getProperty<double>(pointIndexes[k], "head"));
                                   */
                                std::vector<T> head = propertiesIn.template getPropertyFromVector<std::vector<T>>("head", pointIndexes[k]);
                                network.template setProperty<std::vector<T>>(id, "head", head);					
                        }
                        else { //SRM terrible default
                                std::cout << "WARNING: No head value supplied for point " << k << ", using default 0.0" << std::endl;
                                network.template setProperty<std::vector<T>>(id, "head", defaultHead);
                        }
    
                        if (hasFlow) {
                                /*
                                   network.template setProperty<double>(id, "flow",
                                   networkIn.template getProperty<double>(pointIndexes[k], "flow"));
                                   */
                                std::vector<T> flow = propertiesIn.template getPropertyFromVector<std::vector<T>>("flow", pointIndexes[k]);
                                network.template setProperty<std::vector<T>>(id, "flow", flow);
                        }
                        else {
                                std::cout << "WARNING: No flow value supplied for point " << k << ", using default 0.0" << std::endl;
                                network.template setProperty<std::vector<T>>(id, "flow", defaultFlow);
    
                        }
    
                        if (hasMannings) {
    
                                // Get mannings
                                T mannings = propertiesIn.template getPropertyFromVector<T>("mannings", pointIndexes[k]);
                                network.template setProperty<T>(id, "mannings", mannings);
                        }
                        else {
                                std::cout << "WARNING: No mannings values supplied for point " << k << ", using default " << defaultMannings << std::endl;
                                network.template setProperty<T>(id, "mannings", defaultMannings);
                        }
    
                        //network.template setProperty<double> (id, "area",
                        //    networkIn.template getProperty<double>(pointIndexes[k], "area"));
    
                        /*
                        //SRM: Distance array this is older
                        network.template setProperty < std::vector<json11::Json>>(id, "dists",
                        networkIn.template getProperty < std::vector<json11::Json>>(pointIndexes[k], "dists", distArray));
    
                        network.template setProperty < std::vector<json11::Json>>(id, "elevs",
                        networkIn.template getProperty < std::vector<json11::Json>>(pointIndexes[k], "elevs", elevArray));
                        */
    
                        //Copy array data
                        if (hasDists) {
                            std::vector<T> dists = propertiesIn.template getPropertyFromVector<std::vector<T>>("dists", pointIndexes[k]);
                            network.template setProperty<std::vector<T>>(id, "dists", dists);
                        } else {
                                std::stringstream err;
                                err << "Network segment " << id << " has no distance array";
                                throw std::runtime_error(err.str());
                        }
    
                        if (hasElevs) {
                            std::vector<T> elevs = propertiesIn.template getPropertyFromVector<std::vector<T>>("elevs", pointIndexes[k]);
                            network.template setProperty<std::vector<T>>(id, "elevs", elevs);
                        } else {
                                std::stringstream err;
                                err << "Network segment " << id << " has no elevation array";
                                throw std::runtime_error(err.str());
                        }
    
                        //Set type
                        int tp = propertiesIn.template getPropertyFromVector<int>("type", pointIndexes[k]);
                        network.template setProperty<int>(id, "type", tp);
    
    
                        // Get perimeter
                        double perimeter = getPerimeter(id);
                        //std::cout << "perimeter" << perimeter << std::endl;
                        if (perimeter <= 0.0) {
                                std::stringstream err;
                                err << "Network segment " << id << " has zero or negative diameter";
                                throw std::runtime_error(err.str());
                        }
                        network.template setProperty<T>(id, "perimeter", perimeter);
    
    
                        // Get area
                        double area = getArea(id);
                        //std::cout << "area" << area << std::endl;
                        if (area <= 0.0) {
                                std::stringstream err;
                                err << "Network segment " << id << " has zero or negative area";
                                throw std::runtime_error(err.str());
                        }
                        network.template setProperty<T>(id, "area", area);
    
                        //std::cout << "got here" << std::endl;
                        // Get channel top surface width
                        double width = getChannelWidth(id);
                        //std::cout << "width" << width << std::endl;
                        if (width <= 0.0) {
                                std::stringstream err;
                                err << "Network segment " << id << " has zero or negative channel width";
                                throw std::runtime_error(err.str());
                        }
                        network.template setProperty<T>(id, "width", width);
    
                } else {
    
                        std::cout << "doops" << std::endl;
                        nDuplicatePoints++;
                }
        }
    
        if (nDuplicatePoints > 0) {
                std::cout << "WARNING: " << nDuplicatePoints << " duplicate point" << 
                        (nDuplicatePoints > 1 ? "s ignored" : " ignored") << std::endl;
        }
    
        // Copy line strings
        // This takes line strings, finds its linked coordinates in the point geometry
        std::size_t nDuplicateLinks = 0;
        std::map<uint64_t, uint32_t> validLines;
        for (const auto &l : networkIn.getLineStringIndexes()) {
    
                //Get coordinates
                const auto &c = networkIn.getLineStringCoordinates(l);
    
                // Check number of points
                if (c.size() > 2) {
                        throw std::runtime_error("Network must be segments composed of two vertices");
                }
    
                // Update table of links to points
                uint32_t id[2];
                for (int i = 0; i < 2; i++) {
    
                        // Set id to null value
                        id[i] = getNullValue<uint32_t>();
    
                        // Get z-index of coordinate
                        uint64_t zIndex = bounds.createZIndex2D(c[i]);
    
                        // Loop through geometry
                        auto indexRange = zIndexes.equal_range(zIndex);
                        for (auto it = indexRange.first; it != indexRange.second; ++it) {
                                if (c[i] == network.getPointCoordinate(it->second)) {
                                        id[i] = it->second;
                                        break;
                                }
                        }
    
                        // Add any missing points at the end of segments
                        if (id[i] == getNullValue<uint32_t>()) {
                                id[i] = network.addPoint(c[i]);
                                zIndexes.insert( { zIndex, id[i] } );
                                network.template setProperty<cl_uint>(id[i], "id", id[i]);
    
                                std::cout << "WARNING: Did not find a point for link " << l << std::endl;
                                // Get handle and set point
                                //network.template setProperty<int>(id[i], "type", (int)HydroNetworkNodeType::Terminator);
    
                        }
                }
    
                // Check for duplicate lines
                uint64_t lineKey01 = (((uint64_t)id[0])<<32) | (uint64_t)id[1];
                uint64_t lineKey10 = (((uint64_t)id[1])<<32) | (uint64_t)id[0];
                if (validLines.find(lineKey01) == validLines.end() && validLines.find(lineKey10) == validLines.end()) {
    
                        // Add line
                        validLines.insert( { lineKey01, l } );
    
                } else {
                        nDuplicateLinks++;
                }
        }
    
        if (nDuplicateLinks > 0) {
                std::cout << "WARNING: " << nDuplicateLinks << " duplicate line" << 
                        (nDuplicateLinks > 1 ? "s ignored" : " ignored") << std::endl;
        }
    
        // Add non-duplicate lines
        for (auto &l : validLines) {
    
                auto id = network.addLineString(networkIn.getLineStringCoordinates(l.second));
    
                // Copy data
                //network.template setProperty<double> (id, "diameter",
                //    networkIn.template getProperty<double>(l.second, "diameter", defaultDiameter));
    
                //SRM: Maybe need area, but shouldn't
                //network.template setProperty<double> (id, "area",
                //    networkIn.template getProperty<double>(l.second, "area", defaultArea));
    
    
                // Update id to new map
                l.second = id;
        }
    
        // Parse line strings
        for (auto &l : validLines) {
                const auto &c = network.getLineStringCoordinates(l.second);
    
                // Update table of links to points
                linkPoints.push_back( { (uint32_t)(l.first>>32), (uint32_t)(l.first&(uint64_t)0xFFFFFFFF) } );
    
    
                // Calculate length (deltaX)
                T length = std::hypot(c[0].p-c[1].p, c[0].q-c[1].q);
                network.template setProperty<T>(l.first, "length", length); //SRM this is held in the first vertex, so the outlet won't have it
                //std::cout << length << std::endl;	
        }
    
        //Set time
        time = { 0.0 };
    
        return true;

    }


	//Calculates flow area
	template <typename T>
	T HydroNetworkFlowSolver<T>::getArea(uint32_t id) {
		std::vector<T> distArray = network.template getProperty<std::vector<T>>(id, "dists");
		std::vector<T> elevArray = network.template getProperty<std::vector<T>>(id, "elevs");
		std::vector<T> headArray = network.template getProperty<std::vector<T>>(id, "head");
		//Head is the absolute elevation of the water. Subtract 'elevs' to get water height.
		T head = headArray.back();

		if (distArray.size() != elevArray.size()) {
			std::stringstream err;
			err << "Unknown distance and elevation arrays not the same length for point " << id;
			throw std::runtime_error(err.str());
		}

		T area = getArea_(distArray, elevArray, head);
		return area;
	}

	//Calculates flow area
	template <typename T>
	T HydroNetworkFlowSolver<T>::getArea_(std::vector<T> distArray, std::vector<T> elevArray, T head) {

		T area = 0.0;
        T l = 0.0; //length along surface

        //NJM Replaced alpha method (eq 5.11 of Wu Comp. River Dynamics, p. 179)
        //because it assumes that the dist array starts and ends directly
        //on the waterline. 

		//Iterate to 1 before last
		for (int it = 0; it != distArray.size() - 1; ++it) {
			//area += std::min<double>(0.0, 0.5 * (distArray.at(it + 1) - distArray.at(it)) * ((head - elevArray.at(it)) + (head - elevArray.at(it + 1))));
			//area += std::min<double>(0.0, (head - 0.5 * (elevArray.at(it) + elevArray.at(it + 1))) * std::abs(distArray.at(it) - distArray.at(it + 1)));
			/* area += (head - 0.5 * (elevArray.at(it) + elevArray.at(it + 1))) * std::abs(distArray.at(it) - distArray.at(it + 1)); */
            if ((head - elevArray.at(it) >= 0) && (head - elevArray.at(it+1) >= 0)) {
                // Both points wet
                /* std::cout << "Both points wet" << std::endl; */
                l = distArray.at(it + 1) - distArray.at(it);
                area += l * (head - std::max<T>(elevArray.at(it), elevArray.at(it + 1)))
                        + ((abs(elevArray.at(it) - elevArray.at(it + 1)) * l) / 2);
                /* std::cout << "Area: " << area << std::endl; */
            } else if ((head - elevArray.at(it) < 0) || (head - elevArray.at(it + 1) < 0)) {
                // One point wet, one dry. 
                /* std::cout << "One point wet" << std::endl; */
                l = abs((distArray.at(it + 1) - distArray.at(it)) / (elevArray.at(it + 1) - elevArray.at(it)))
                    * (head - std::min<T>(elevArray.at(it), elevArray.at(it + 1)));
                area += (l * (head - std::min<T>(elevArray.at(it), elevArray.at(it + 1))))/2;
                /* std::cout << "Area: " << area << std::endl; */
            }
		}
				
		return area;
	}

	//Calculates wetted perimeter (Xi)
	template <typename T>
	T HydroNetworkFlowSolver<T>::getPerimeter(uint32_t id) {

		std::vector<T> distArray = network.template getProperty<std::vector<T>>(id, "dists");
		std::vector<T> elevArray = network.template getProperty<std::vector<T>>(id, "elevs");
		std::vector<T> headArray = network.template getProperty<std::vector<T>>(id, "head");
		//Head is the absolute elevation of the water. Subtract 'elevs' to get water height.
		T head = headArray.back();

		if (distArray.size() != elevArray.size()) {
			std::stringstream err;
			err << "Unknown distance and elevation arrays not the same length for point " << id;
			throw std::runtime_error(err.str());
		}

        //std::cout << "In perimeter" << std::endl;
        T perimeter = getPerimeter_(distArray, elevArray, head);
		return perimeter;
	}

	//Calculates wetted perimeter (Xi)
	template <typename T>
    T HydroNetworkFlowSolver<T>::getPerimeter_(std::vector<T> distArray, std::vector<T> elevArray, T head) {

		//Using alpha method - eq 5.12 of Wu Comp. River Dynamics, p. 179
		//Xj = sqrt((elev_i - elev_i+1)^2 + dy^2)
		T perimeter = 0.0;
		T d2, e2; //d2 is distance square, e2 is elevation square

		//Iterate to 1 before last
		for (int it = 0; it != distArray.size() - 1; ++it) {
            //std::cout << "elev " << elevArray.size() << std::endl;
			d2 = 0.0;
			e2 = 0.0;
			//If both are below head, it is all OK to add
			if ((head - elevArray.at(it)) >= 0.0 && (head - elevArray.at(it + 1)) >= 0.0) {
                //std::cout << "Both wet" << std::endl;
				//Both points wet
				d2 = pow(distArray.at(it + 1) - distArray.at(it), 2.0);
				e2 = pow(elevArray.at(it + 1) - elevArray.at(it), 2.0);				
			}
			else if ((head - elevArray.at(it) >= 0.0) && (head - elevArray.at(it + 1) < 0.0)) {
                //std::cout << "Left wet" << std::endl;
				//Left wet, right dry
				// Looks like this, so take x(i) + height-e(i) * run/rise
				// |-/
				// |/
				//Slope times distance
				T xr = ((distArray.at(it) - distArray.at(it + 1)) / (elevArray.at(it) - elevArray.at(it + 1))) * (head - elevArray.at(it));
				//d2 = pow(xr - distArray.at(it), 2.0);
				d2 = pow(xr, 2.0);
				e2 = pow(head - elevArray.at(it), 2.0);
			}
			else if ((head - elevArray.at(it + 1) >= 0.0) && (head - elevArray.at(it) < 0.0)) {
                //std::cout << "Right wet" << std::endl;
				//Right wet, left dry
				// Looks like this, so take x(i+1) - height-e(i) * run/rise
				// \-|
				//  \|
				T xl = ((distArray.at(it) - distArray.at(it + 1)) / (elevArray.at(it) - elevArray.at(it + 1))) * (head - elevArray.at(it + 1));
				//d2 = pow(distArray.at(it + 1) - xl, 2.0);
				d2 = pow(xl, 2.0);
				e2 = pow(head - elevArray.at(it + 1), 2.0);
			}
			//Calculate perimeter - doesn't include top as Wu.
			perimeter += sqrt(d2 + e2);
		}

		return perimeter;
	}


	//Calculate channel width at water surface (B)
	template <typename T>
	T HydroNetworkFlowSolver<T>::getChannelWidth(uint32_t id) {
		std::vector<T> distArray = network.template getProperty<std::vector<T>>(id, "dists");
		std::vector<T> elevArray = network.template getProperty<std::vector<T>>(id, "elevs");
		std::vector<T> headArray = network.template getProperty<std::vector<T>>(id, "head");
		//Head is the absolute elevation of the water. Subtract 'elevs' to get water height.
		T head = headArray.back();

		if (distArray.size() != elevArray.size()) {
			std::stringstream err;
			err << "Unknown distance and elevation arrays not the same length for point " << id;
			throw std::runtime_error(err.str());
		}
	    T width = getChannelWidth_(distArray, elevArray, head);	
		return width;
	}

	//Calculate channel width at water surface (B)
	template <typename T>
	T HydroNetworkFlowSolver<T>::getChannelWidth_(std::vector<T> distArray, std::vector<T> elevArray, T head) {

		T width = 0.0;

		//Iterate to 1 before last
		for (int it = 0; it != distArray.size() - 1; ++it) {
			//If both are below head, it is all OK to add
			if ((head - elevArray.at(it)) >= 0.0 && (head - elevArray.at(it + 1)) >= 0.0) {
				width += (distArray.at(it + 1) - distArray.at(it));
			}
			else if ((head - elevArray.at(it) >= 0.0) && (head - elevArray.at(it + 1) < 0.0)) { 
				//Left wet, right dry
				// Looks like this, so take x(i) + height-e(i) * run/rise
				// |-/
				// |/
				T xr = abs((distArray.at(it) - distArray.at(it + 1)) / (elevArray.at(it) - elevArray.at(it + 1))) * (head - elevArray.at(it));
				//width += (xr - distArray.at(it)); XR is the fractional distance
				width += xr;
			}
			else if ((head - elevArray.at(it + 1) >= 0.0) && (head - elevArray.at(it) < 0.0)) {
				//Right wet, left dry
				// Looks like this, so take x(i+1) - height-e(i) * run/rise
				// \-|
				//  \|
				T xl = ((distArray.at(it + 1) - distArray.at(it)) / (elevArray.at(it) - elevArray.at(it + 1))) * (head - elevArray.at(it + 1));
				//width += (distArray.at(it + 1) - xl); xl is the fractinal distance
				width += xl;
			}
		
		}
		
		return width;
	}

	// Calculate Conveyance K
	template <typename T>
	T HydroNetworkFlowSolver<T>::getConveyance(uint32_t id) {
		std::vector<T> distArray = network.template getProperty<std::vector<T>>(id, "dists");
		std::vector<T> elevArray = network.template getProperty<std::vector<T>>(id, "elevs");
		std::vector<T> headArray = network.template getProperty<std::vector<T>>(id, "head");
		T head = headArray.back();
		//Head is the absolute elevation of the water. Subtract 'elevs' to get water height.
		T mannings = network.template getProperty<T>(id, "mannings");

		if (distArray.size() != elevArray.size()) {
			std::stringstream err;
			err << "Unknown distance and elevation arrays not the same length for point " << id;
			throw std::runtime_error(err.str());
		}

		T area = getArea_(distArray, elevArray, head);
        T perimeter = getPerimeter_(distArray, elevArray, head);

        T K = getConveyance_(area, mannings, perimeter);
		return K;
	}

	//Calculate channel width at water surface (B)
	template <typename T>
	T HydroNetworkFlowSolver<T>::getConveyance_(T area, T mannings, T perimeter) {
        T K;
        K = pow(area, 5.0 / 3.0) / (mannings * pow(perimeter, 2.0 / 3.0));
		return K;
	}

	// Calculate partialKh
	template <typename T>
	T HydroNetworkFlowSolver<T>::getPartialKh(uint32_t id) {
		std::vector<T> distArray = network.template getProperty<std::vector<T>>(id, "dists");
		std::vector<T> elevArray = network.template getProperty<std::vector<T>>(id, "elevs");
		std::vector<T> headArray = network.template getProperty<std::vector<T>>(id, "head");
		//Head is the absolute elevation of the water. Subtract 'elevs' to get water height.
        //Using the head value at the current timestep
        T head = headArray.back();
		T mannings = network.template getProperty<T>(id, "mannings");

		if (distArray.size() != elevArray.size()) {
			std::stringstream err;
			err << "Unknown distance and elevation arrays not the same length for point " << id;
			throw std::runtime_error(err.str());
		}

        //Maybe this should be scaled by (head - min(elevs)) or something? 
        T dz = 0.1;
        
        T partialKh = getPartialKh_(distArray, elevArray, head, mannings, dz);
		return partialKh;
	}

	//Calculate channel width at water surface (B)
	template <typename T>
	T HydroNetworkFlowSolver<T>::getPartialKh_(std::vector<T> distArray, std::vector<T> elevArray, T head, T mannings, T dz) {
        // Perturbing height up and down by dz 
        // and calculating the effect on area and perimeter
		T areaUp = getArea_(distArray, elevArray, head + dz);
		T areaDown = getArea_(distArray, elevArray, head - dz);
        T perimeterUp = getPerimeter_(distArray, elevArray, head + dz);
        T perimeterDown = getPerimeter_(distArray, elevArray, head - dz);

        T K_up = getConveyance_(areaUp, mannings, perimeterUp);
        T K_down = getConveyance_(areaDown, mannings, perimeterDown);

        // Central difference
        T partialKh = (K_up - K_down)/(2.0 * dz); 

		return partialKh;
	}


    //NJM Eigen doesn't support templated typedefs. 
    template <typename Type, int Size> using Array = Eigen::Array<Type, Size, 1>;

    //Leave but do not use
    template <typename T>
    bool HydroNetworkFlowSolver<T>::run(T runtime, T writeStep, uint32_t mode) {

		//Get handles to point and lines
		auto pointIndexes = network.getPointIndexes();
		auto lineStringIndexes = network.getLineStringIndexes();

		//Some constants that are needed
		std::size_t N = pointIndexes.size(); //Size of the network (number of links)
		
		//Initialise time
		
		//Create storage matrices, which are last time step (sup(n))
		//Eigen::Matrix<double, Eigen::Dynamic, 1> A(N), B(N), h(N), Q(N), beta(N), S(N), K(N);
		Array<T, Eigen::Dynamic> A(N); //Area
		Array<T, Eigen::Dynamic> B(N); //Top surface width
		Array<T, Eigen::Dynamic> h(N); //Height *head*
		Array<T, Eigen::Dynamic> Q(N); //Flow
		Array<T, Eigen::Dynamic> beta(N); //Momentum correction factor
		Array<T, Eigen::Dynamic> Sf(N); //Friction slope
		Array<T, Eigen::Dynamic> K(N); //Conveyance
		Array<T, Eigen::Dynamic> dX(N - 1); //Length

        Array<T, Eigen::Dynamic> manningsArray(N); //Mannings coefficient
        Array<T, Eigen::Dynamic> perimeterArray(N); //Perimiter
        Array<T, Eigen::Dynamic> lengthArray(N); //Length? Added as property for now. 
        

		//Get property handles
		PropertyMap &properties = network.getProperties();
		//Reference to the types
		auto &typeVector = properties.template getPropertyRef<std::vector<int>>("type");
		auto &areaVector = properties.template getPropertyRef<std::vector<T>>("area");
		auto &widthVector = properties.template getPropertyRef<std::vector<T>>("width");
		auto &headVector = properties.template getPropertyRef<std::vector<std::vector<T>>>("head");
		auto &flowVector = properties.template getPropertyRef<std::vector<std::vector<T>>>("flow");
		// auto &headVector = properties.template getPropertyRef<std::vector<T>>("head");
		// auto &flowVector = properties.template getPropertyRef<std::vector<T>>("flow");
		auto &elevVector = properties.template getPropertyRef<std::vector<std::vector<T>>>("elevs");
		auto &distVector = properties.template getPropertyRef<std::vector<std::vector<T>>>("dists");
		auto &perimeterVector = properties.template getPropertyRef<std::vector<T>>("perimeter");
		auto &manningsVector = properties.template getPropertyRef<std::vector<T>>("mannings");
		auto &lengthVector = properties.template getPropertyRef<std::vector<T>>("length");


        //NJM this doesn't work because N is not known at compile time, and this can only map static size to static size. 
        //So the first loop is necessary. 
        /* A = Eigen::Map<Array<T, N>>(areaVector.data()); */
            
		//Initialise the Matrices
		for (std::size_t k = 0; k < N; k++) {
		    	
			A[k] = (T)areaVector[k];
			B[k] = (T)widthVector[k];

            // Using the head and flow arrays in the last timestep
			h[k] = (T)headVector[k].back();
			Q[k] = (T)flowVector[k].back();
           
			manningsArray[k] = (T)manningsVector[k];
			perimeterArray[k] = (T)perimeterVector[k];
						
			if (typeVector[k] != (int)HydroNetworkNodeType::Terminator) {
                dX[k] = lengthVector[k+1]; //SRM THIS IS SUSPECT				
				//std::cout << "Added length " << lengthVector[k+1] << std::endl;
            }
			
		}
		

		//Validate network starts with inflow and ends with terminator
		if (typeVector.front() != (int)HydroNetworkNodeType::Inflow) {
			std::stringstream err;
			err << "Network does not start with inflow!" << std::endl << "It is " << typeVector.front();
			throw std::runtime_error(err.str());
		}

		if (typeVector[N-1] != (int)HydroNetworkNodeType::Terminator) {
			std::stringstream err;
			err << "Network does not end with terminator!" << std::endl << "It is " << typeVector[N-1];
			throw std::runtime_error(err.str());
		}

				
		
		/* Debug outputs
		std::cout << "Area: \n" << A << std::endl;
		std::cout << "Perimeter: \n" << perimeterArray << std::endl;
		std::cout << "Manningns: \n" << manningsArray << std::endl;
		std::cout << "Q: \n" << Q << std::endl;
		std::cout << "Width: \n" << B << std::endl;
		std::cout << "dx: \n" << dX << std::endl;
		std::cout << "Head: \n" << h << std::endl;
		*/

		std::cout << "Running simulation, starting parameters are:" << std::endl;

        //Conveyance K - equation 2.1 CCHE - this should be calculated in init to help out 
		// THis is eq 5.24 in comp. river dynamics
        K = A.pow(5.0 / 3.0) / (manningsArray * perimeterArray.pow(2.0 / 3.0));
		std::cout << "Max Conveyance: " << K.maxCoeff() << std::endl;


        //Friction slope Sf - equation 3.3
        Sf = (Q * abs(Q)) / (K * K);
		std::cout << "Max Friction: " << Sf.maxCoeff() << std::endl;

        //Beta (momentum correction) - equation 3.55, currently not using left/right banks. This is one according to that?
        //Note also comp. river dynamics equation 5.54 and text suggests a different equation (5.41)
        // THis should be 1 until we create our Left, right banks.
		beta = ((A * A) / (K * K * K)) *((K * K * K) / (A * A));
		std::cout << "Max Beta: " << beta.maxCoeff() << std::endl;

		//Partial k/partial h
		Array<T, Eigen::Dynamic> partialKh = Array<T, Eigen::Dynamic>::Zero(N); 

        //head perturbation for the central difference estimation of Partial K/partial h
		T dz = 0.1; 
		for (std::size_t k = 0; k < N; k++) { //SRM k < N 
	    	partialKh[k] = getPartialKh_(distVector[k], elevVector[k], h[k], manningsArray[k], dz);
        }
		std::cout << "Max Partial Kh:" << partialKh.maxCoeff() << std::endl;

		//Create the star matrices, which are last iteration step (sup(*)), initialised is a copy of the n step
		Array<T, Eigen::Dynamic> A_star = A;
		Array<T, Eigen::Dynamic> B_star = B;
		Array<T, Eigen::Dynamic> h_star = h;
		Array<T, Eigen::Dynamic> Q_star = Q;
		Array<T, Eigen::Dynamic> beta_star = beta;
		Array<T, Eigen::Dynamic> Sf_star = Sf;
		Array<T, Eigen::Dynamic> K_star = K;
		Array<T, Eigen::Dynamic> Perimeter_star = perimeterArray;
		Array<T, Eigen::Dynamic> partialKh_star = partialKh;

			
		//Create matrices for calculation
		//Continuity coefficients
		Array<T, Eigen::Dynamic> a(N);
		Array<T, Eigen::Dynamic> b(N);
		Array<T, Eigen::Dynamic> c(N);
		Array<T, Eigen::Dynamic> d(N);
		Array<T, Eigen::Dynamic> p(N);
		//Momentum coefficents
		Array<T, Eigen::Dynamic> e(N);
		Array<T, Eigen::Dynamic> f(N);
		Array<T, Eigen::Dynamic> g(N);
		Array<T, Eigen::Dynamic> w(N);
		Array<T, Eigen::Dynamic> r(N);
		
		Array<T, Eigen::Dynamic> dh = Array<T, Eigen::Dynamic>::Zero(N); //Delta h
		Array<T, Eigen::Dynamic> dQ = Array<T, Eigen::Dynamic>::Zero(N); //Delta Q

		//Create a froude matrix
		Array<T, Eigen::Dynamic> Fr(N);

		//Create the crucial S and T matrices
		Array<T, Eigen::Dynamic> s = Array<T, Eigen::Dynamic>::Zero(N);
		Array<T, Eigen::Dynamic> t = Array<T, Eigen::Dynamic>::Zero(N);
		
		T eps = 1.0e-4; //Convergence criteria
		
		 //Calculate deltaT, not really sure if this is correct:
		// celerity (wave speed) should be beta(Q/A)
		Array<T, Eigen::Dynamic> waveSpeed = 3.0 * (Q/ A);
		std::cout << "Wavespeed: " << waveSpeed.maxCoeff() << std::endl;

		//std::cout << "dx: " << dX << std::endl;

		//Calculate the delta T (divide wave by deltax)
		Array<T, Eigen::Dynamic> deltaT = dX.minCoeff() / waveSpeed;
		//Get our timestep
		T timestep = deltaT.minCoeff();
		std::cout << "Timestep: " << timestep << std::endl;
		
		//Set the sub steps - these should be constant at ~0.5
		double Xi = 0.5; //Spatial step - must be 0.5
		double theta = 0.55; //Time step - must be >= 0.5 to be unconditionally stable
		double grav = 9.81;
		double Xi_r = 0.05; //Small flow depth eq 3.56

		//This is where BC's are set at the moment Q is the inflow at 0 and H is at outflow
		//IMPORTANT SET INFLOW properly
		double q_n1 = Q[0];
		double outH = h[N - 1];
		std::cout << "Flow: " << q_n1 << std::endl;
		std::cout << "Head: " << outH << std::endl;

		//Iterate until time is less than the runtime specified
		std::cout <<std::endl << "Stepping: " << std::endl;

		while (time < runtime) {
            std::cout << time << std::endl;
			//Iterate while dh.sum() and dQ.sum() greater than eps
			// Iteration counter
			int iter = 1;
			do {

				//Init matrices:
				//Do array/matrix maths ratehr than iteratively (should be 0)


				a = ((1.0 - Xi)*B_star) / timestep; //a
				b = -theta / dX; //b, dX as the distance between k-1 and k nodes
				d = theta / dX; //d

				if (mode == 1) {//Dynamic wave
					//Momentum coeffs e f g w r
					f = ((1.0 - Xi) / timestep) * (1.0 / A_star) -
						theta / dX * (beta_star * Q_star / (A_star * A_star)) +
						2.0 * theta*(1.0 - Xi_r)*grav*(Q_star.abs() / (K_star * K_star));
					//std::cout << "f: " << f.maxCoeff() << std::endl;
					e = (-(1.0 - Xi) / timestep) * ((Q_star * B_star) / (A_star * A_star)) +
						theta / dX * ((beta_star * Q_star * Q_star * B_star) / (A_star * A_star * A_star)) -
						theta * grav / dX - 2.0 * theta * (1.0 - Xi_r) * grav * (Sf_star / K_star)*partialKh_star;
				}
				else if (mode == 2) {//Diffusive wave
					e = -((theta * grav) / dX) - 2.0 * theta * (1.0 - Xi_r) * grav * (Sf_star / K_star)*partialKh_star;
					f = 2.0 * theta*(1.0 - Xi_r)*grav*(Q_star.abs() / (K_star * K_star));
				}
				else { //"Auto" mode, calculate Froude
					//THis is actually Fr^2 - John fenton quick trick
					Fr = ((Q_star / A_star) * (Q_star / A_star)) / (grav*(A_star / B_star));
				}

				//

				//All of these have [k+1] indices. Would need to think a bit to get them out of here. 				
				for (std::size_t k = 0; k < N - 1; k++) {
					//Continutity coeffs a b c d p
					c[k] = (Xi*B_star[k + 1]) / timestep; //c

					p[k] = -Xi / timestep * (A_star[k + 1] - A[k + 1]) -
						(1.0 - Xi) / timestep * (A_star[k] - A[k]) -
						theta / dX[k] * (Q_star[k + 1] - Q_star[k]) -
						(1.0 - theta) / dX[k] * (Q[k + 1] - Q[k]);
					//To add (for inflows): + theta*(Xi*q(n+1,k+1) + (1-Xi)*q(n+1,j)) + (1-theta) * (Xi*q(n, k+1) + (1-Xi)*q(n,k))
				
				//Momentum coeffs e f g w r
					if (mode == 1) {//Dynamic wave

						
						g[k] = -Xi / timestep * (Q_star[k + 1] * B_star[k + 1]) / (A_star[k + 1] * A_star[k + 1]) -
							theta / dX[k] * (beta_star[k + 1] * Q_star[k + 1] * Q_star[k + 1] * B_star[k + 1]) / (A_star[k + 1] * A_star[k + 1] * A_star[k + 1]) +
							theta * grav / dX[k] - 2.0 * theta*Xi_r*grav*(Sf_star[k + 1] / K_star[k + 1])*partialKh_star[k + 1];
						w[k] = Xi / timestep * (1.0 / A_star[k + 1]) +
							theta / dX[k] * ((beta_star[k + 1] * Q_star[k + 1]) / (A_star[k + 1] * A_star[k + 1])) +
							2.0 * theta*Xi_r*grav*(fabs(Q_star[k + 1]) / (K_star[k + 1] * K_star[k + 1]));

						r[k] = -Xi / timestep * (Q_star[k + 1] / A_star[k + 1] - Q[k + 1] / A[k + 1]) -
							(1.0 - Xi) / timestep * (Q_star[k] / A_star[k] - Q[k] / A[k]) -
							theta / dX[k] * (0.5*beta_star[k + 1] * (Q_star[k + 1] / A_star[k + 1])*(Q_star[k + 1] / A_star[k + 1]) -
								0.5*beta_star[k] * (Q_star[k] / A_star[k])*(Q_star[k] / A_star[k])) -
								(1.0 - theta) / dX[k] * (0.5*beta[k + 1] * (Q[k + 1] / A[k + 1])*(Q[k + 1] / A[k + 1]) -
									0.5*beta[k] * (Q[k] / A[k])*(Q[k] / A[k])) -
							theta * grav / dX[k] * (h_star[k + 1] - h_star[k]) -
							(1.0 - theta) * grav / dX[k] * (h[k + 1] - h[k]) -
							theta * grav * (Xi_r*Sf_star[k + 1] + (1.0 - Xi_r)*Sf_star[k]) -
							(1.0 - theta) * grav * (Xi_r*Sf[k + 1] + (1.0 - Xi_r)*Sf[k]);
						//To add (for inflows)
					}
					else if (mode == 2) {//Diffusive wave
						g[k] = theta * grav / dX[k] - 2.0 * theta*Xi_r*grav*(Sf_star[k + 1] / K_star[k + 1])*partialKh_star[k + 1];
						w[k] = 2.0 * theta*Xi_r*grav*(fabs(Q_star[k + 1]) / (K_star[k + 1] * K_star[k + 1]));
						r[k] = -theta * grav / dX[k] * (h_star[k + 1] - h_star[k]) -
							(1.0 - theta) * grav / dX[k] * (h[k + 1] - h[k]) -
							theta * grav * (Xi_r*Sf_star[k + 1] + (1.0 - Xi_r)*Sf_star[k]) -
							(1.0 - theta) * grav * (Xi_r*Sf[k + 1] + (1.0 - Xi_r)*Sf[k]);
					}
					else { //'Auto' mode - check froude
						if (Fr[k] < 0.9*0.9) {
							//Utilise dynamic wave
							e[k] = (-(1.0 - Xi) / timestep) * ((Q_star[k] * B_star[k]) / (A_star[k] * A_star[k])) +
								theta / dX[k] * ((beta_star[k] * Q_star[k] * Q_star[k] * B_star[k]) / (A_star[k] * A_star[k] * A_star[k])) -
								theta * grav / dX[k] - 2.0 * theta * (1.0 - Xi_r) * grav * (Sf_star[k] / K_star[k])*partialKh_star[k];
							f[k] = ((1.0 - Xi) / timestep) * (1.0 / A_star[k]) -
								theta / dX[k] * (beta_star[k] * Q_star[k] / (A_star[k] * A_star[k])) +
								2.0 * theta*(1.0 - Xi_r)*grav*(fabs(Q_star[k]) / (K_star[k] * K_star[k]));
							g[k] = -Xi / timestep * (Q_star[k + 1] * B_star[k + 1]) / (A_star[k + 1] * A_star[k + 1]) -
								theta / dX[k] * (beta_star[k + 1] * Q_star[k + 1] * Q_star[k + 1] * B_star[k + 1]) / (A_star[k + 1] * A_star[k + 1] * A_star[k + 1]) +
								theta * grav / dX[k] - 2.0 * theta*Xi_r*grav*(Sf_star[k + 1] / K_star[k + 1])*partialKh_star[k + 1];
							w[k] = Xi / timestep * (1.0 / A_star[k + 1]) +
								theta / dX[k] * ((beta_star[k + 1] * Q_star[k + 1]) / (A_star[k + 1] * A_star[k + 1])) +
								2.0 * theta*Xi_r*grav*(fabs(Q_star[k + 1]) / (K_star[k + 1] * K_star[k + 1]));

							r[k] = -Xi / timestep * (Q_star[k + 1] / A_star[k + 1] - Q[k + 1] / A[k + 1]) -
								(1.0 - Xi) / timestep * (Q_star[k] / A_star[k] - Q[k] / A[k]) -
								theta / dX[k] * (0.5*beta_star[k + 1] * (Q_star[k + 1] / A_star[k + 1])*(Q_star[k + 1] / A_star[k + 1]) -
									0.5*beta_star[k] * (Q_star[k] / A_star[k])*(Q_star[k] / A_star[k])) -
									(1.0 - theta) / dX[k] * (0.5*beta[k + 1] * (Q[k + 1] / A[k + 1])*(Q[k + 1] / A[k + 1]) -
										0.5*beta[k] * (Q[k] / A[k])*(Q[k] / A[k])) -
								theta * grav / dX[k] * (h_star[k + 1] - h_star[k]) -
								(1.0 - theta) * grav / dX[k] * (h[k + 1] - h[k]) -
								theta * grav * (Xi_r*Sf_star[k + 1] + (1.0 - Xi_r)*Sf_star[k]) -
								(1.0 - theta) * grav * (Xi_r*Sf[k + 1] + (1.0 - Xi_r)*Sf[k]);
						}
						else {
							//Utilise diffusive wave
							e[k] = -theta * grav / dX[k] - 2.0 * theta * (1.0 - Xi_r) * grav * (Sf_star[k] / K_star[k])*partialKh_star[k];
							f[k] = 2.0 * theta*(1.0 - Xi_r)*grav*(fabs(Q_star[k]) / (K_star[k] * K_star[k]));
							g[k] = theta * grav / dX[k] - 2.0 * theta*Xi_r*grav*(Sf_star[k + 1] / K_star[k + 1])*partialKh_star[k + 1];
							w[k] = 2.0 * theta*Xi_r*grav*(fabs(Q_star[k + 1]) / (K_star[k + 1] * K_star[k + 1]));
							r[k] = -theta * grav / dX[k] * (h_star[k + 1] - h_star[k]) -
								(1.0 - theta) * grav / dX[k] * (h[k + 1] - h[k]) -
								theta * grav * (Xi_r*Sf_star[k + 1] + (1.0 - Xi_r)*Sf_star[k]) -
								(1.0 - theta) * grav * (Xi_r*Sf[k + 1] + (1.0 - Xi_r)*Sf[k]);
						}

					}
					
				}
				
				/*
				std::cout << "A_star: \n" << A_star << std::endl;
				std::cout << "B_star: \n" << B_star << std::endl;
				std::cout << "h_star: \n" << h_star << std::endl;
				std::cout << "Q_star: \n" << Q_star << std::endl;
				std::cout << "beta_star: \n" << beta_star << std::endl;
				std::cout << "Sf_star: \n" << Sf_star << std::endl;
				std::cout << "K_star: \n" << K_star << std::endl;
				std::cout << "PartialKh_star: \n" << partialKh_star << std::endl;
				std::cout << "Perimenter_star: \n" << Perimeter_star << std::endl;
				std::cout << "w: \n" << w << std::endl;
				*/
				if (Eigen::isnan(a).any() || Eigen::isnan(b).any() || Eigen::isnan(c).any() || Eigen::isnan(d).any() || Eigen::isnan(p).any()) {
					std::stringstream err;
					err << "Got NANs in continuity" << std::endl;
									
					err << "a: \n" << a << std::endl;
					err << "b: \n" << b << std::endl;
					err << "c: \n" << c << std::endl;
					err << "d: \n" << d << std::endl;
					err << "p: \n" << p << std::endl;

					err << "B_star: \n" << B_star << std::endl;
					throw std::runtime_error(err.str());
				}

				if (Eigen::isnan(g).any() || Eigen::isnan(w).any() || Eigen::isnan(r).any()) {
					std::stringstream err;
					err << "Got NANs in momentum" << std::endl;

					err << "e: \n" << e << std::endl;
					err << "f: \n" << f << std::endl;
					err << "g: \n" << g << std::endl;
					err << "w: \n" << w << std::endl;
					err << "r: \n" << r << std::endl;

					err << "Sf_star: \n" << Sf_star << std::endl;
					err << "PartialKh_star: \n" << partialKh_star << std::endl;
					err << "A_star: \n" << A_star << std::endl;
					err << "B_star: \n" << B_star << std::endl;
					err << "Q_star: \n" << Q_star << std::endl;
					err << "K_star: \n" << K_star << std::endl;
					err << "beta_star: \n" << beta_star << std::endl;
					throw std::runtime_error(err.str());
				}
								
				/*
				std::cout << "a: \n" << a << std::endl;
				std::cout << "b: \n" << b << std::endl;
				std::cout << "c: \n" << c << std::endl;
				std::cout << "d: \n" << d << std::endl;
				std::cout << "p: \n" << p << std::endl;

				std::cout << "\t \t ****Momentum coeffs****" << std::endl;
				std::cout << "e: \n" << e << std::endl;
				std::cout << "f: \n" << f << std::endl;
				std::cout << "g: \n" << g << std::endl;
				std::cout << "w: \n" << w << std::endl;
				std::cout << "r: \n" << r << std::endl;
				*/
				//Run double sweep, calc S and T from 1 to N-1

				//Set s and T0 because inlet is q_n1
				s[0] = 0.0;
				t[0] = q_n1 - Q_star[0];
				//std::cout << "t:" << t[0] << std::endl;

				//std::cout << "N " << N << std::endl;

				//Sweep 1 (s and T) from 0 to 
				for (std::size_t k = 0; k < N - 1; k++) {
					s[k + 1] = (-((a[k] + b[k] * s[k])*g[k]) + ((e[k] + f[k] * s[k])*c[k])) / ((a[k] + b[k] * s[k])*w[k] - (e[k] + f[k] * s[k])*d[k]);
					t[k + 1] = ((a[k] + b[k] * s[k])*(r[k] - f[k] * t[k]) - (e[k] + f[k] * s[k])*(p[k] - b[k] * t[k])) / ((a[k] + b[k] * s[k])*w[k] - (e[k] + f[k] * s[k])*d[k]);
					//std::cout << "sweep 1 for setting elem " << k + 1 << "from elem " << k << std::endl;
				}
				/*
				std::cout << "\t \t ****T and S coeffs****" << std::endl;
				std::cout << "t: \n" << t << std::endl;
				std::cout << "s: \n" << f << std::endl;
				*/
				//Reverse sweep (dh dQ)
				//Set dh at n-1
				dh[N - 1] = outH - h_star[N-1];
				//dh[N - 1] = std::min(outH, h[N - 1] + ((outH / 300.0)*timestep)) - h_star[N - 1];
				dQ[N - 1] = s[N - 1] * dh[N - 1] + t[N - 1];
				//Loop from N-2 to 0
				for (std::size_t k = N - 2; k < -1; k--) {
					dh[k] = ((p[k] - b[k] * t[k]) - (c[k] * dh[k + 1] + d[k] * dQ[k + 1])) / (a[k] + b[k] * s[k]);
					dQ[k] = s[k] * dh[k] + t[k];
					//std::cout << "sweep 2 for setting elem " << k << "from elem " << k+1 << std::endl;
				}
				/*
				std::cout << "\t \t ****Q and H coeffs****" << std::endl;
				std::cout << "dh: \n" << dh << std::endl;
				std::cout << "dQ: \n" << dQ << std::endl;
				*/

				//std::cout << " . iter: " << iter << " dh:" << dh.sum() << " dq:" << dQ.maxCoeff();

				//std::cout << std::endl << "dh eps " << (abs(dh) > eps).any() << " dQ eps " << (abs(dQ) > eps).any() << " output: " << ((abs(dh) > eps).any() || (abs(dQ) > eps).any());

				std::cout << " .";

				/*Update stars*/
				h_star = h_star + dh;
				Q_star = Q_star + dQ;


				if ((h_star < 0.0).any()) {

					std::stringstream err;
					err << "Got height less than elevation" << std::endl;
										
					err << "h_star: \n" << h_star << std::endl;
					err << "dh: \n" << dh << std::endl;

					err << "p: \n" << p << std::endl;
					err << "A_star - A: \n" << A_star - A << std::endl;
					err << "Q: \n" << Q << std::endl;
					err << "Q_star: \n" << Q_star << std::endl;

					//err << "b: \n" << b << std::endl;
					err << "t: \n" << t << std::endl;
					//err << "c: \n" << c << std::endl;
					//err << "d: \n" << d << std::endl;
					err << "dq: \n" << dQ << std::endl;
					//err << "a: \n" << a << std::endl;
					err << "s: \n" << s << std::endl;

					throw std::runtime_error(err.str());

				}

				/* do A* B* sf* K**/
				for (std::size_t k = 0; k < N; k++) {
					A_star[k] = getArea_(distVector[k], elevVector[k], h_star[k]);
					B_star[k] = getChannelWidth_(distVector[k], elevVector[k], h_star[k]);
					Perimeter_star[k] = getPerimeter_(distVector[k], elevVector[k], h_star[k]);
	    	        partialKh_star[k] = getPartialKh_(distVector[k], elevVector[k], h_star[k], manningsArray[k], dz);
				}
				K_star = A_star.pow(5.0 / 3.0) / (manningsArray * Perimeter_star.pow(2.0 / 3.0));
				Sf_star = (Q_star * abs(Q_star)) / (K_star * K_star);
				
				
				

				//std::cout << "dh:" << dh << std::endl;
				//std::cout << "dQ:" << dQ << std::endl;

				++iter;
				/*
				if (iter > 100) {
					std::stringstream err;
					err << "Not converged after 100 iterations" << std::endl;
					err << "dQ: \n" << dQ << std::endl;
					err << "dH: \n" << dh << std::endl;
					throw std::runtime_error(err.str());
				}
				*/

			} while ((abs(dh) > eps).any() || (abs(dQ) > 1.0e-2).any());//(((abs(dh) > eps).any() || (abs(dQ) > eps).any()) && iter < 20); //Iteration break needed for debug, abs() may not be correct

			time = time + timestep;
			
			std::cout << "/ "; //Just to show next iteration

            //SRM make time output a bit more easier to read - reporting every step is too much
			//std::cout << std::endl << "Time: " << time << std::endl;
			//std::cout << "(" << time << "s)";

			/* Debug outputs
			
			std::cout << std::endl << "Dh " << dh.maxCoeff() << std::endl;
			std::cout << "Dq " << dQ.maxCoeff() << std::endl;
			*/
			

			//Iteration done, set A,B,h,beta,S,K to stars
			A = A_star;
			B = B_star;
			beta = beta_star;
			Q = Q_star;
			h = h_star;
			K = K_star;
			Sf = Sf_star;

			//Recalculate wavespeed
			waveSpeed = 3.0 * (Q / A);
			deltaT = dX.minCoeff() / waveSpeed.abs();
			timestep = deltaT.minCoeff();
			
            // If the time is closer than one timestep to a writestep, write.  
            if (fmod(time, writeStep) < timestep) {
                timeArray.push_back(time);
				for (std::size_t k = 0; k < N; k++) {
					headVector[k].push_back(h[k]);
                    flowVector[k].push_back(Q[k]);
                }
            } 	
            
			//Next DT
		} //End time loop
		std::cout << std::endl << "Simulation completed!" << std::endl;
		std::cout << "Q: \n" << Q_star << std::endl;
		std::cout << "H: \n" << h_star << std::endl;
		
		
		std::cout << "Elevs: \n" << std::endl;
		for (std::size_t k = 0; k < N; k++) {
			auto elem = std::min_element(elevVector[k].begin(), elevVector[k].end());
			std::cout << h_star[k] - *elem << std::endl;
		}
		
        return true;
    }

    template <typename T>
    bool HydroNetworkFlowSolver<T>::run_hydro() {
		/*
        // Get data handles        
        auto pointIndexes = network.getPointIndexes();
        auto lineStringIndexes = network.getLineStringIndexes();

        //SRM: Leaving these for the time being
        // Create auxillary vectors
        std::size_t N = pointIndexes.size();
        std::vector<double> b0(N);         // Initial forcing vector
        std::vector<double> d(N);          // Diagonal vector
        std::set<std::size_t> terminators; // Termination points

        // Create vectors and matrices
        std::vector<Et> c;
        Eigen::SparseMatrix<double> A(N, N);
        Eigen::Matrix<double, Eigen::Dynamic, 1> b(N);  // Forcing vector
        Eigen::Matrix<double, Eigen::Dynamic, 1> h(N);  // Pressure vector
        Eigen::Matrix<double, Eigen::Dynamic, 1> dh(N); // Pressure update vector
        

        // Set data
        double inflow = 0.0;
        for (std::size_t k = 0; k < N; k++) {
            
            // Get point index
            auto &p = pointIndexes[k];

            // Set flow and pressure
            h(k) = 0.0;
            b0[k] = 0.0;
            if (network.template getProperty<int>(p, "type") == HydroNetworkNodeType::Terminator) {

                // Get pressure from terminators
                h(k) = network.template getProperty<double>(p, "head");
                terminators.insert(k);

            } else if (network.template getProperty<int>(p, "type") == HydroNetworkNodeType::Inflow) {
                inflow = network.template getProperty<double>(p, "inflow", inflow); //Will need to be an array later
                if (inflow == 0.0) {
                    std::cout << "WARNING: Inflow node " << p << "is 0.0, this probably shouldnt be the case" << std::endl;
                }
            }
            else {

                // Get inflow
                double flow = network.template getProperty<double>(p, "flow");

                // Update initial forcing vector
                b0[k] = flow;
                inflow += flow;
            }
            
        }

        // Iterate
        std::size_t iter = 0;
        double qval, dval;
        double eps = 1.0E-12;
        double dh_dot, dh0_dot = 1.0;
        do {

            // Clear data
            c.clear();
            for (std::size_t k = 0; k < N; k++) {

                // Set forcing
                b(k) = b0[k];

                // Reset diagonal
                d[k] = 0.0;
            }

            // Loop over links
            for (std::size_t k = 0; k < lineStringIndexes.size(); k++) {
                
                // Get link index
                auto &l = lineStringIndexes[k];

                // Get link parameters
                HydroNetworkSegmentType::Type type = 
                    static_cast<HydroNetworkSegmentType::Type>(network.template getProperty<int>(l, "type"));
                double lK = network.template getProperty<double>(l, "K");
                double lk = network.template getProperty<double>(l, "k");

                // Get indexes
                cl_uint i = linkPoints[k].first;
                cl_uint j = linkPoints[k].second;

                // Get h values from link
                double hi = h(i);
                double hj = h(j);
                double hval = fabs(hi-hj);
                double hsign = hi-hj > 0.0 ? 1.0 : -1.0;

                // Calculate matrix entries
                qval = 0.0;
                dval = 0.0;
                switch (type) {
                    //SRM: Add Dynamic wave type
                    case HydroNetworkSegmentType::DynamicWave: {
                        //TODO: calcs in here
                    } break;
                            
                    default:
                        throw std::runtime_error("No other channel type accepted yet");
                }

                // Set matrix entries
                bool ti = terminators.find(i) == terminators.end();
                bool tj = terminators.find(j) == terminators.end();
                if (ti && tj) {
                    c.push_back(Et(i, j, -dval));
                    c.push_back(Et(j, i, -dval));
                }
                if (ti) {
                    d[i] += dval;
                    b(i) -= qval;
                }
                if (tj) {
                    d[j] += dval;
                    b(j) += qval;
                }
            }

            // Write to diagonal
            for (int i = 0; i < (int)d.size(); i++)
                c.push_back(Et(i, i, d[i] == 0.0 ? 1.0 : d[i]));
                    
            // Create matrix
            A.setFromTriplets(c.begin(), c.end());
            A.makeCompressed();

            // Solve matrix
            Eigen::SparseLU<Eigen::SparseMatrix<double> > solver(A);            
            solver.compute(A);

            if(solver.info() != Eigen::Success) {
                
                // Solver failed
                throw std::runtime_error("Flow network solver failed");
            }
            dh = solver.solve(b);

            // Update h
            h += dh;

            // Check convergence
            dh_dot = dh.dot(dh);
            if (iter == 0) {
                dh0_dot = dh_dot;

                // Check if any flow is present
                if (dh0_dot == 0.0) {
                    break;
                }
            }
            iter++;

            //std::cout << "Flow network convergence: " << dh_dot/dh0_dot << std::endl; // TEST

        } while(dh_dot/dh0_dot > 1.0E-3 && iter < 100);
                
        //std::cout << "Flow network took " << iter << " iterations" << std::endl; // TEST
        if (dh0_dot == 0.0) {
            throw std::runtime_error("Flow network has no sources");
        }
        if (iter == 100) {
            throw std::runtime_error("Flow network has not converged");
        }
        
        // Store pressure
        for (std::size_t k = 0; k < N; k++)
            network.template setProperty<double>(pointIndexes[k], "head", h(k));
        
        // Store flow
        double outflow = 0.0;
        double Qmax = 0.0;
        for (std::size_t k = 0; k < lineStringIndexes.size(); k++) {
                
            // Get link index
            auto &l = lineStringIndexes[k];

            // Get indexes
            cl_uint i = linkPoints[k].first;
            cl_uint j = linkPoints[k].second;
            
            // Get link parameters
            HydroNetworkSegmentType::Type type = 
                static_cast<HydroNetworkSegmentType::Type>(network.template getProperty<int>(l, "type"));
            double lK = network.template getProperty<double>(l, "K");
            double lk = network.template getProperty<double>(l, "k");

            // Get h values from link
            double hi = h(i);
            double hj = h(j);

            // Calculate flow   
            double Q = 0.0;
            switch (type) {
                //SRM: Add Dynamic wave type
                case HydroNetworkSegmentType::DynamicWave: {
                    //TODO: calcs in here
                    } break;
                            
                default:
                    throw std::runtime_error("No other channel type accepted yet");
                 
            }

            // Find maximum Q value
            Qmax = std::max(Qmax, fabs(Q));

            // Update network
            network.template setProperty<double>(l, "flow", Q);

            // Set flow at terminators
            if (terminators.find(i) != terminators.end()) {
                network.template setProperty<double>(pointIndexes[i], "flow", Q);
                outflow += Q;
            } 
            if (terminators.find(j) != terminators.end()) {
                network.template setProperty<double>(pointIndexes[j], "flow", -Q);
                outflow -= Q;
            }
        }

        // Check sum of flow, this should be relatively close to zero
        if (fabs(outflow+inflow)/Qmax > 1.0E-3)
            std::cout << "WARNING: Flow does not balance over network" << std::endl;
			*/
        return true;
    }



    // Float type definitions
    template bool HydroNetworkFlowSolver<float>::init(Vector<float> &, std::string);
    template bool HydroNetworkFlowSolver<float>::run(float runtime, float writeStep, uint32_t mode);
    template bool HydroNetworkFlowSolver<float>::run_hydro();
	template float HydroNetworkFlowSolver<float>::getArea(uint32_t id);
	template float HydroNetworkFlowSolver<float>::getPerimeter(uint32_t id);
	template float HydroNetworkFlowSolver<float>::getChannelWidth(uint32_t id);
	template float HydroNetworkFlowSolver<float>::getConveyance(uint32_t id);
	template float HydroNetworkFlowSolver<float>::getPartialKh(uint32_t id);
	template float HydroNetworkFlowSolver<float>::getArea_(std::vector<float> distArray, std::vector<float> elevArray, float head);
	template float HydroNetworkFlowSolver<float>::getPerimeter_(std::vector<float> distArray, std::vector<float> elevArray, float head);
	template float HydroNetworkFlowSolver<float>::getChannelWidth_(std::vector<float> distArray, std::vector<float> elevArray, float head);
	template float HydroNetworkFlowSolver<float>::getConveyance_(float area, float mannings, float perimeter);
	template float HydroNetworkFlowSolver<float>::getPartialKh_(std::vector<float> distArray, std::vector<float> elevArray, float head, float mannings, float dz);

    // Double type definitions
    template bool HydroNetworkFlowSolver<double>::init(Vector<double> &, std::string);
    template bool HydroNetworkFlowSolver<double>::run(double runtime, double writeStep, uint32_t mode);
    template bool HydroNetworkFlowSolver<double>::run_hydro();
	template double HydroNetworkFlowSolver<double>::getArea(uint32_t id);
	template double HydroNetworkFlowSolver<double>::getPerimeter(uint32_t id);
	template double HydroNetworkFlowSolver<double>::getChannelWidth(uint32_t id);
	template double HydroNetworkFlowSolver<double>::getConveyance(uint32_t id);
	template double HydroNetworkFlowSolver<double>::getPartialKh(uint32_t id);
	template double HydroNetworkFlowSolver<double>::getArea_(std::vector<double> distArray, std::vector<double> elevArray, double head);
	template double HydroNetworkFlowSolver<double>::getPerimeter_(std::vector<double> distArray, std::vector<double> elevArray, double head);
	template double HydroNetworkFlowSolver<double>::getChannelWidth_(std::vector<double> distArray, std::vector<double> elevArray, double head);
	template double HydroNetworkFlowSolver<double>::getConveyance_(double area, double mannings, double perimeter);
	template double HydroNetworkFlowSolver<double>::getPartialKh_(std::vector<double> distArray, std::vector<double> elevArray, double head, double mannings, double dz);

}
