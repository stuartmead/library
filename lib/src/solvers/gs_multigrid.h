/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef GEOSTACK_MULTIGRID_SOLVER_H
#define GEOSTACK_MULTIGRID_SOLVER_H

#include <string>
#include <vector>
#include <set>

#include "gs_raster.h"

// Link to external resources
extern const char R_cl_multigrid_head_c[];
extern const uint32_t R_cl_multigrid_head_c_size;
extern const char R_cl_multigrid_init_c[];
extern const uint32_t R_cl_multigrid_init_c_size;
extern const char R_cl_multigrid_update_c[];
extern const uint32_t R_cl_multigrid_update_c_size;
extern const char R_cl_multigrid_c[];
extern const uint32_t R_cl_multigrid_c_size;
extern const char R_cl_multigrid_restrict_c[];
extern const uint32_t R_cl_multigrid_restrict_c_size;
extern const char R_cl_multigrid_interpolate_c[];
extern const uint32_t R_cl_multigrid_interpolate_c_size;
extern const char R_cl_multigrid_error_c[];
extern const uint32_t R_cl_multigrid_error_c_size;

namespace Geostack
{
    /**
    * %Multigrid layer types.
    */
    namespace MultigridLayers {
        enum Type {
            b,      ///< Forcing layer
            l0,     ///< Solution layer 0
            l1,     ///< Solution layer 1
            bm,     ///< Forcing multi layers
            lm0,    ///< Solution multi layers 0
            lm1,    ///< Solution multi layers 1
            MultigridLayers_END ///< Placeholder for count of layers
        };
    }	

    /**
    * %Multigrid class for perimeter growth. 
    */
    template <typename TYPE>
    class Multigrid {

        public:
        
            /**
            * Multigrid constructor
            */
            Multigrid():initialised(false), verbose(false), cycles(0), needsUpdate(false) { }

            // Initialisation
            bool init(
                std::string jsonConfig,
                std::vector<RasterBasePtr<TYPE> > inputLayers_ = std::vector<RasterBasePtr<TYPE> >());

            // Execution
            bool step();

            /**
            * Return multigrid forcing %Raster
            */
            Raster<TYPE, TYPE> &getForcing() { 
                return lb[0];
            }
            Raster<TYPE, TYPE> &getForcingLevel(int level) { 
                return lb[level];
            }

            /**
            * Return multigrid solution %Raster
            */
            Raster<TYPE, TYPE> &getSolution() { 
                return ll0[0];
            }
            Raster<TYPE, TYPE> &getSolutionLevel(int level) { 
                return ll0[level];
            }
            Raster<TYPE, TYPE> &getSolutionTempLevel(int level) { 
                return ll1[level];
            }

        private:  
            
            // Input data
            std::vector<RasterBasePtr<TYPE> > inputLayers;    ///< User-defined input layers

            // Internal Raster lists
            KernelRequirements initReq;                        ///< Initialisation kernel raster requirements     
            KernelRequirements relaxReq;                       ///< Relaxation kernel raster requirements          
            RasterBaseRefs<TYPE> scriptRefs;                   ///< Script kernel rasters
            std::vector<RasterBaseRefs<TYPE> > relaxRefs;      ///< Relaxation kernel rasters
            std::vector<RasterBaseRefs<TYPE> > relaxFlipRefs;  ///< Relaxation kernel flipped rasters

            // Generators
            KernelGenerator initGenerator;   ///< Initialisation kernel generator
            KernelGenerator updateGenerator; ///< Update kernel generator
            KernelGenerator errorGenerator;  ///< Residual error generator

            // Internal raster layers
            std::vector<Raster<TYPE, TYPE> > ll0; ///< Solution layers 0
            std::vector<Raster<TYPE, TYPE> > ll1; ///< Solution layers 1
            std::vector<Raster<TYPE, TYPE> > lb;  ///< Forcing layers
            
            // Internal variables
            volatile bool initialised;    ///< Solver initialised
            volatile bool verbose;        ///< Verbose output
            uint32_t cycles;              ///< Number of multigrid cycles
            bool needsUpdate;             ///< Requires update kernel to be run

            // Internal kernels
            cl::Kernel initKernel;        ///< Initialisation kernel
            cl::Kernel updateKernel;      ///< Update kernel
            cl::Kernel relaxKernel;       ///< Relaxation kernel
            cl::Kernel residualKernel;    ///< Residual kernel
            cl::Kernel restrictKernel;    ///< Restriction kernel
            cl::Kernel interpolateKernel; ///< Interpolation kernel
            cl::Kernel errorKernel;       ///< Residual error kernel

            // Multigrid functions
            void relax(uint32_t level, cl::CommandQueue &queue);
            void rest(uint32_t level, cl::CommandQueue &queue);
            void interpolate(uint32_t level, cl::CommandQueue &queue);
            TYPE error(cl::CommandQueue &queue);

            // Member functions
            uint32_t factor(uint32_t size);
            void resizeTiles(RasterDimensions<TYPE> rDim);
    };
}

#endif