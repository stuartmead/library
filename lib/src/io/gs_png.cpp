/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <iomanip>

#include "json11.hpp"
#include "miniz.h"

#include "gs_solver.h"
#include "gs_string.h"
#include "gs_png.h"

using namespace json11;

namespace Geostack
{
    /**
    * Read PNG image to raster.
    * @param fileName input file name.
    * @return true if file is read, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void PngHandler<RTYPE, CTYPE>::read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {
        
        // TODO
        throw std::runtime_error("PNG read functions are not yet available");
    }

    /**
    * Write raster to PNG image.
    * @param fileName output file name.
    * @return true if file is written, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void PngHandler<RTYPE, CTYPE>::write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // Check for data
        if (!r.hasData()) {
            throw std::runtime_error("Raster has no data to write to float file");
        }

        // Set config defaults
        uint32_t k = 0;
        RTYPE vMax = 0.0;
        RTYPE vMin = 0.0;

        // Parse config
        if (!jsonConfig.empty()) {
        
            // Parse Json
            std::string err;
            auto config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                throw std::runtime_error(err);
            }
              
            // Set no data type
            if (config["slice"].is_number()) {
                k = (uint32_t)config["slice"].number_value();
            }
              
            // Set ranges
            if (config["vmin"].is_number()) {
                vMin = (RTYPE)config["vmin"].number_value();
            }
            if (config["vmax"].is_number()) {
                vMax = (RTYPE)config["vmax"].number_value();
            }
        }

        // Get raster dimensions
        const uint32_t tileSize = TileMetrics::tileSize;
        auto dim = r.getRasterDimensions();

        // Check ranges
        if (vMax <= vMin) {
            vMax = r.max();
            vMin = r.min();
        }
        RTYPE iRange = 255.0/(vMax-vMin);
        
        // Create data buffers
        std::vector<RTYPE> dataCache;
        std::vector<uint8_t> imageData;
        imageData.resize(dim.d.nx*dim.d.ny*3);

        // Read data
        uint8_t verbose = Geostack::Solver::getSolver().getVerboseLevel();
        for (int32_t tj = 0; tj < dim.ty; tj++) {

            // Populate tiles
            for (uint32_t ti = 0; ti < dim.tx; ti++) {

                // Get data
                r.getTileDataSlice(ti, tj, k, dataCache);

                // Convert to pixel
                for (uint32_t j = 0; j < (tj == dim.ty-1 ? dim.d.ny-(dim.ty-1)*tileSize : tileSize); j++) {
                    for (uint32_t i = 0; i < (ti == dim.tx-1 ? dim.d.nx-(dim.tx-1)*tileSize : tileSize); i++) {

                        RTYPE v = std::min(std::max(*(dataCache.data()+i+j*tileSize), vMin), vMax);
                        uint8_t pixelValue = (uint8_t)(iRange*(v-vMin));
                        auto *pImageData = imageData.data()+((ti*tileSize+i)+(tj*tileSize+j)*dim.d.nx)*3;
                        *(pImageData+0) = pixelValue;
                        *(pImageData+1) = pixelValue;
                        *(pImageData+2) = pixelValue;
                    }
                }
            }

            // Output progress
            if (verbose <= Geostack::Verbosity::Info && (tj%10) == 0) {
                std::cout << std::setprecision(2) << 100.0*(double)tj/(double)dim.ty << "%" << std::endl;
            }
        }

        // Compress data
        size_t pngDataDize = 0;
        void *data = tdefl_write_image_to_png_file_in_memory_ex(imageData.data(), dim.d.nx, dim.d.ny, 3, &pngDataDize, 6, MZ_FALSE);
        if (!data) {
            throw std::runtime_error("Cannot compress png data");
        }

        // Write file
        std::ofstream fileStream(fileName, std::ios::binary);
        if (!fileStream) {
            std::stringstream err;
            err << "Cannot open '" << fileName << "' for writing";
            throw std::runtime_error(err.str());
        }
        fileStream.write(reinterpret_cast<const char *>(data), pngDataDize);

        // Flush stream
        fileStream.flush();
        mz_free(data);

        if (verbose <= Geostack::Verbosity::Info) {
            std::cout << "100%" << std::endl;
        }
    }    
    
    // RTYPE float, CTYPE float definitions
    template class PngHandler<float, float>;
    
    // RTYPE uint, CTYPE float definitions
    template class PngHandler<uint32_t, float>;
    
    // RTYPE double, CTYPE double definitions
    template class PngHandler<double, double>;
    
    // RTYPE uint, CTYPE double definitions
    template class PngHandler<uint32_t, double>;
}
