/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>
#include <algorithm>
#include <string>
#include <cmath>
#include <regex>
#include <sstream>
#include <iomanip>

#include "gs_models.h"
#include "gs_string.h"
#include "gs_vector.h"
#include "gs_projection.h"

namespace Geostack
{
    /**
    * %Point construction.
    * Creates %Point from a %Vertex.
    * @param pointVertex_ %Vertex to use for point.
    */
    template <typename CTYPE>
    void Point<CTYPE>::addVertex(cl_uint pointVertex_) {
        pointVertex = pointVertex_;
    }

    /**
    * Clone Vertex data
    * Makes a copy of the %Point vertex
    */
    template <typename CTYPE>
    VectorGeometry<CTYPE> *Point<CTYPE>::clone() {

        // Create new point
        Point<CTYPE> *r = new Point<CTYPE>();

        // Set ID
        r->setID(VectorGeometry<CTYPE>::id);

        // Copy geometry
        r->pointVertex = pointVertex;

        return r;
    }

    /**
    * Get %Point bounding box.
    * @return pair of co-located coordinates.
    */
    template <typename CTYPE>
    BoundingBox<CTYPE> Point<CTYPE>::getBounds() const {
        return BoundingBox<CTYPE> { bc, bc };
    }

    /**
    * Update %Point bounding box.
    */
    template <typename CTYPE>
    void Point<CTYPE>::updateBounds(const Vector<CTYPE> &v) {

        // Store coordinate for bounds
        bc = v.getCoordinate(pointVertex);
    }

    /**
    * %LineString construction.
    * Adds a %Vertex to a line string
    * @param pointVertex_ %Vertex to add to line.
    */
    template <typename CTYPE>
    void LineString<CTYPE>::addVertex(cl_uint lineVertex_) {

        // Add to list
        lineVertices.push_back(lineVertex_);
    }

    /**
    * Clone Vertex data
    * Makes a copy of the %LineString vertices
    */
    template <typename CTYPE>
    VectorGeometry<CTYPE> *LineString<CTYPE>::clone() {

        // Create new linestring
        LineString<CTYPE> *r = new LineString<CTYPE>();

        // Set ID
        r->setID(VectorGeometry<CTYPE>::id);

        // Copy geometry
        r->lineVertices = lineVertices;

        // Copy bounds
        r->bounds = bounds;

        return r;
    }

    /**
    * Update %LineString bounding box.
    */
    template <typename CTYPE>
    void LineString<CTYPE>::updateBounds(const Vector<CTYPE> &v) {

        // Reset bounds
        bounds.reset();

        // Update bounds
        for (auto &index : lineVertices)
            bounds.extend(v.getCoordinate(index));
    }

    /**
    * %Polygon construction.
    * Adds a %Polygon to a line string
    * @param pointVertex_ %Polygon to add to line.
    * @param index sub-polygon index.
    */
    template <typename CTYPE>
    void Polygon<CTYPE>::addVertex(cl_uint polygonVertex_) {

        // Add to list
        polygonVertices.push_back(polygonVertex_);
    }

    /**
    * Clone Vertex data
    * Makes a copy of the %Polygon vertices
    */
    template <typename CTYPE>
    VectorGeometry<CTYPE> *Polygon<CTYPE>::clone() {

        // Create new polygon
        Polygon<CTYPE> *r = new Polygon<CTYPE>();

        // Set ID
        r->setID(VectorGeometry<CTYPE>::id);

        // Copy geometry
        r->polygonVertices = polygonVertices;
        r->polygonSubIndexes = polygonSubIndexes;
        r->polygonBounds = polygonBounds;

        return r;
    }

    /**
    * %Polygon construction.
    * Adds a sub-polygon to a %Polygon.
    * The first sub-polygon contains vertices for the external polygon.
    * Subsequent sub-polygon contain vertices for holes in the polygon.
    * @param length number of vertices in sub-polygon.
    */
    template <typename CTYPE>
    void Polygon<CTYPE>::addSubPolygon(cl_uint length) {
        polygonSubIndexes.push_back(length);
    }

    /**
    * Update %Polygon bounding box.
    */
    template <typename CTYPE>
    void Polygon<CTYPE>::updateBounds(const Vector<CTYPE> &v) {

        // Clear list
        polygonBounds.clear();

        // Update bounds
        cl_uint index = 0;
        for (auto &len : polygonSubIndexes) {

            // Find sub-polygon bounds
            BoundingBox<CTYPE> subPolygonBounds;
            for (cl_uint i = index; i < index+len; i++) {
                subPolygonBounds.extend(v.getCoordinate(polygonVertices[i]));
            }

            // Add bounds to list
            polygonBounds.push_back(subPolygonBounds);

            // Update index
            index+=len;
        }
    }

    /**
    * Default %Vector constructor
    * dimensions will be POD initialised to zero
    */
    template <typename CTYPE>
    Vector<CTYPE>::Vector():
        proj() {

        // Create data
        pVertices = std::make_shared<VariablesVector<Coordinate<CTYPE> > >();
        pGeometry = std::make_shared<std::vector<VectorGeometryPtr<CTYPE> > >();
        pProperties = std::make_shared<PropertyMap>();
    }

    /**
    * Destructor
    */
    template <typename CTYPE>
    Vector<CTYPE>::~Vector() {
    }

    /**
    * Copy constructor
    */
    template <typename CTYPE>
    Vector<CTYPE>::Vector(const Vector<CTYPE> &v):
        proj(v.proj),
        pVertices(v.pVertices),
        pGeometry(v.pGeometry),
        geometryIndexes(v.geometryIndexes),
        pointIndexes(v.pointIndexes),
        lineStringIndexes(v.lineStringIndexes),
        polygonIndexes(v.polygonIndexes),
        pProperties(v.pProperties) {

        // Copy data
        tree = v.tree;
    }

    /**
    * Clear %Vector data
    */
    template <typename CTYPE>
    void Vector<CTYPE>::clear() {

        // Clear projection
        proj = ProjectionParameters<double>();

        // Clear geometry
        tree.clear();
        pVertices->clear();
        pGeometry->clear();
        geometryIndexes.clear();
        pointIndexes.clear();
        lineStringIndexes.clear();
        polygonIndexes.clear();

        // Clear properties
        pProperties->clear();
    }

    /**
    * Clear and build RTree
    */
    template <typename CTYPE>
    void Vector<CTYPE>::buildTree() {

        // Clear tree
        tree.clear();

        // Build tree
        auto &geometry = *pGeometry;
        for (auto &g : geometry) {

            // Update bounding box
            g->updateBounds(*this);

            // Update tree
            tree.insert(g);
        }
    }

    /**
    * Assignment operator
    * @param v %Vector to assign.
    */
    template <typename CTYPE>
    Vector<CTYPE> &Vector<CTYPE>::operator=(const Vector<CTYPE> &v) {

        if (this != &v) {

            // Copy projection
            proj = v.proj;

            // Copy geometry
            tree = v.tree;
            pVertices = v.pVertices;
            pGeometry = v.pGeometry;
            geometryIndexes = v.geometryIndexes;
            pointIndexes = v.pointIndexes;
            lineStringIndexes = v.lineStringIndexes;
            polygonIndexes = v.polygonIndexes;

            // Copy properties
            pProperties = v.pProperties;
        }

        return *this;
    }

    /**
    * Addition operator
    * @param v %Vector to add.
    */
    template <typename CTYPE>
    Vector<CTYPE> &Vector<CTYPE>::operator+=(const Vector<CTYPE> &v) {

        if (this != &v) {

            // Null operation
            if (!v.hasData())
                return *this;

            // Check projection
            if (proj != v.proj) {
                throw std::length_error("Projections do not match in Vector addition");
            }

            // Get data handles
            std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
            PropertyMap &properties = *pProperties;

            // Get original sizes
            auto initialVerticesSize = pVertices->size();
            auto initialGeometrySize = geometry.size();

            // Append vertices
            auto &vertices = pVertices->getData();
            auto &addVertices = v.pVertices->getData();
            vertices.insert(vertices.end(), addVertices.begin(), addVertices.end());

            // Append geometry
            for (auto &g : *v.pGeometry) {

                auto ag = std::shared_ptr<VectorGeometry<CTYPE> >(g->clone());
                ag->shiftVertexIndex(initialVerticesSize);
                add(ag);
            }

            // Get properties
            auto &vProperties = v.getProperties();

            auto stringProperties = vProperties.template getPropertyRefs<std::vector<std::string> >(); // TODO reduce into one function
            auto intProperties = vProperties.template getPropertyRefs<std::vector<int> >();
            auto floatProperties = vProperties.template getPropertyRefs<std::vector<float> >();
            auto doubleProperties = vProperties.template getPropertyRefs<std::vector<double> >();
            auto indexProperties = vProperties.template getPropertyRefs<std::vector<cl_uint> >();

            // Add new properties
            for (auto &pi : stringProperties) {
                properties.addProperty(pi.first);
            }
            for (auto &pi : intProperties) {
                properties.addProperty(pi.first);
            }
            for (auto &pi : floatProperties) {
                properties.addProperty(pi.first);
            }
            for (auto &pi : doubleProperties) {
                properties.addProperty(pi.first);
            }
            for (auto &pi : indexProperties) {
                properties.addProperty(pi.first);
            }

            // Resize properties
            properties.resize(geometry.size());

            // Copy properties
            for (auto &pi : stringProperties) {
                std::vector<std::string> &p = properties.template getPropertyRef<std::vector<std::string> >(pi.first);
                p.resize(initialGeometrySize, getNullValue<std::string>());
                std::vector<std::string> &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
            for (auto &pi : intProperties) {
                std::vector<int> &p = properties.template getPropertyRef<std::vector<int> >(pi.first);
                p.resize(initialGeometrySize, getNullValue<int>());
                std::vector<int> &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
            for (auto &pi : floatProperties) {
                std::vector<float> &p = properties.template getPropertyRef<std::vector<float> >(pi.first);
                p.resize(initialGeometrySize, getNullValue<float>());
                std::vector<float> &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
            for (auto &pi : doubleProperties) {
                std::vector<double> &p = properties.template getPropertyRef<std::vector<double> >(pi.first);
                p.resize(initialGeometrySize, getNullValue<double>());
                std::vector<double> &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
            for (auto &pi : indexProperties) {
                std::vector<cl_uint> &p = properties.template getPropertyRef<std::vector<cl_uint> >(pi.first);
                p.resize(initialGeometrySize, getNullValue<cl_uint>());
                std::vector<cl_uint> &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
        }

        return *this;
    }

    /**
    * Add @VectorGeometry to %Vector
    * @param g @VectorGeometry to add.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::add(VectorGeometryPtr<CTYPE> g) {

        // Update bounds
        g->updateBounds(*this);

        // Update tree
        tree.insert(g);

        // Update vector
        pGeometry->push_back(g);

        // Update geometry index
        cl_uint index = pGeometry->size()-1;
        geometryIndexes.push_back(index);
        g->setID(index);
        g->updateVector(*this, index);

        // Return index
        return index;
    }

    /**
    * Add %Point to %Vector.
    * @param c_ Coordinate pair.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::addPoint(Coordinate<CTYPE> c_) {

        // Create point
        PointPtr<CTYPE> p = std::make_shared<Point<CTYPE> >();

        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getData();

        // Create vertex
        cl_uint index = vertices.size();
        vertices.push_back(c_);

        // Associate vertex with point
        p->addVertex(index);

        // Add and return index of geometry
        return add(p);
    }

    /**
    * Add %LineString to %Vector
    * @param cs_ List of coordinate pairs.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::addLineString(CoordinateList<CTYPE> cs_) {

        // Check size
        if (cs_.size() == 0) {
            throw std::length_error("No coordinates provided for line string creation");
        }

        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getData();

        // Create new line string
        LineStringPtr<CTYPE> l = std::make_shared<LineString<CTYPE> >();
        for (auto &c : cs_) {

            // Add vertices
            cl_uint index = vertices.size();
            vertices.push_back(c);

            // Add to line
            l->addVertex(index);
        }

        // Add and return index of geometry
        return add(l);
    }

    /**
    * Add %Polygon to %Vector
    * @param pcs_ List of list of coordinate pairs.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::addPolygon(std::vector<CoordinateList<CTYPE> > pcs_) {

        // Check size
        if (pcs_.size() == 0 || pcs_[0].size() == 0)
            throw std::length_error("No coordinates provided for polygon creation");

        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getData();

        // Create new polygon
        PolygonPtr<CTYPE> p = std::make_shared<Polygon<CTYPE> >();
        for (auto &cs : pcs_) {

            // Polygon bounds
            BoundingBox<CTYPE> bounds = { cs[0], cs[0] };

            // Check first and last coordinates are the same
            if (cs.front() != cs.back()) {
                cs.push_back(cs.front());
            }

            // Add vertices
            for (auto &c : cs) {

                // Update bounds
                bounds.extend(c);

                // Add vertices
                cl_uint index = vertices.size();
                vertices.push_back(c);

                // Add to polygon
                p->addVertex(index);
            }

            // Create new sub polygon
            p->addSubPolygon(cs.size());
        }

        // Add and return index of geometry
        return add(p);
    }

    /**
    * Get %Point %Coordinate from %Vector.
    * @return %Point coordinate
    */
    template <typename CTYPE>
    Coordinate<CTYPE> Vector<CTYPE>::getPointCoordinate(const cl_uint index) const {

        // Get geometry references
        auto &c = pVertices->getData();
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

        // Get point reference
        auto &point = *std::static_pointer_cast<Point<CTYPE> >(geometry[index]);

        // Return coordinate
        return c[point.pointVertex];
    }

    /**
    * Get %LineString coordinates from %Vector.
    * @return %LineString coordinates
    */
    template <typename CTYPE>
    CoordinateList<CTYPE> Vector<CTYPE>::getLineStringCoordinates(const cl_uint index) const {

        // Get geometry references
        auto &c = pVertices->getData();
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

        // Get line string reference
        auto &lineString = *std::static_pointer_cast<LineString<CTYPE> >(geometry[index]);

        // Create coordinate list
        CoordinateList<CTYPE> cl;
        for (auto li : lineString.lineVertices)
            cl.push_back(c[li]);

        // Return coordinate list
        return cl;
    }

    /**
    * Get %Polygon coordinates from %Vector.
    * @return %Polygon coordinates
    */
    template <typename CTYPE>
    CoordinateList<CTYPE> Vector<CTYPE>::getPolygonCoordinates(const cl_uint index) const {

        // Get geometry references
        auto &c = pVertices->getData();
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

        // Get polygon reference
        auto &polygon = *std::static_pointer_cast<Polygon<CTYPE> >(geometry[index]);

        // Create coordinate list
        CoordinateList<CTYPE> cl;
        for (auto pi : polygon.polygonVertices)
            cl.push_back(c[pi]);

        // Return coordinate list
        return cl;
    }

    /**
    * Get %Polygon indexes from %Vector.
    * @return index list
    */
    template <typename CTYPE>
    const std::vector<cl_uint> &Vector<CTYPE>::getPolygonVertexIndexes(const cl_uint index) const {
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        return std::static_pointer_cast<Polygon<CTYPE> >(geometry[index])->getVertexIndexes();
    }

    /**
    * Get %Polygon sub-indexes from %Vector.
    * @return index list
    */
    template <typename CTYPE>
    const std::vector<cl_uint> &Vector<CTYPE>::getPolygonSubIndexes(const cl_uint index) const {
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        return std::static_pointer_cast<Polygon<CTYPE> >(geometry[index])->getSubIndexes();
    }

    /**
    * Get %Polygon sub-bounds from %Vector.
    * @return %BoundingBox list
    */
    template <typename CTYPE>
    const std::vector<BoundingBox<CTYPE> > &Vector<CTYPE>::getPolygonSubBounds(const cl_uint index) const {
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        return std::static_pointer_cast<Polygon<CTYPE> >(geometry[index])->getSubBounds();
    }

    /**
    * Check property name in %Vector.
    * @param name @Property name.
    * @return true if found, false otherwise
    */
    template <typename CTYPE>
    bool Vector<CTYPE>::hasProperty(std::string name) {

        // Get data handles
        PropertyMap &properties = *pProperties;
        return properties.hasProperty(name);
    }

    /**
    * Add property to %Vector.
    * @param name @Property name.
    */
    template <typename CTYPE>
    void Vector<CTYPE>::addProperty(std::string name) {

        // Get data handle
        PropertyMap &properties = *pProperties;

        // Add property
        properties.addProperty(name);
    }

    /**
    * Convert property type in %Vector.
    * @param name @Property name.
    */
    template <typename CTYPE>
    template <typename PTYPE>
    void Vector<CTYPE>::convertProperty(std::string name) {

        // Get data handle
        PropertyMap &properties = *pProperties;

        // Add property
        properties.convertProperty<std::vector<PTYPE> >(name);
    }

    /**
    * Remove property from %Vector.
    * @param name @Property name.
    */
    template <typename CTYPE>
    void Vector<CTYPE>::removeProperty(std::string name) {

        // Get data handle
        PropertyMap &properties = *pProperties;

        // Add property
        properties.removeProperty(name);
    }

    /**
    * Set property for %VectorGeometry in %Vector.
    * @param index %VectorGeometry item.
    * @param name @Property name.
    * @param v @Property value.
    */
    template <typename CTYPE>
    template <typename PTYPE>
    void Vector<CTYPE>::setProperty(cl_uint index, std::string name, PTYPE v) {

        // Get data handles
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        PropertyMap &properties = *pProperties;

        // Create property
        properties.addProperty(name);

        // Set property
        if (index < geometry.size()) {

            // Get property vector
            std::vector<PTYPE> &p = properties.template getPropertyRef<std::vector<PTYPE> >(name);

            // Resize vector
            properties.resize(geometry.size());

            // Set vector
            p[index] = v;

        } else {

            // Index out of range
            std::stringstream err;
            err << "Vector index " << index << " out of range for property '" << name << "'";
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Set property for %VectorGeometry in %Vector.
    * @param index %VectorGeometry item.
    * @param name @Property name.
    * @param v @Property value.
    */
    template <typename CTYPE>
    template <typename PTYPE>
    void Vector<CTYPE>::setProperty(std::string name, PTYPE v) {

        // Get data handles
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        PropertyMap &properties = *pProperties;

        // Check data size
        if (v.size() != geometry.size()) {
            std::stringstream err;
            err << "Invalid vector size for setting property '" << name << "'";
            throw std::runtime_error(err.str());
        }

        // Create property
        properties.addProperty(name);

        // Get property vector
        PTYPE &p = properties.template getPropertyRef<PTYPE>(name);

        // Resize vector
        properties.resize(geometry.size());

        // Set property vector
        p = v;
    }

    /**
    * Get property for %VectorGeometry in %Vector.
    * @param index %VectorGeometry item.
    * @param name @Property name.
    * @return @Property value
    */
    template <typename CTYPE>
    template <typename PTYPE>
    PTYPE Vector<CTYPE>::getProperty(cl_uint index, std::string name) {

        // Get data handles
        PropertyMap &properties = *pProperties;
        return properties.template getPropertyFromVector<PTYPE>(name, index);
    }

    /**
    * Get property for %VectorGeometry in %Vector.
    * @param name @Property name.
    * @return @Property value
    */
    template <typename CTYPE>
    template <typename PTYPE>
    PTYPE &Vector<CTYPE>::getPropertyVectors(std::string name) {

        // Get data handles
        PropertyMap &properties = *pProperties;
        return properties.template getPropertyRef<PTYPE>(name);
    }

    /**
    * Get property for %VectorGeometry in %Vector.
    * @param index %VectorGeometry item.
    * @param name @Property name.
    * @return @Property value
    */
    template <typename CTYPE>
    template <typename PTYPE>
    PTYPE &Vector<CTYPE>::getPropertyVector(cl_uint index, std::string name) {
        auto &pv = this->template getPropertyVectors<std::vector<std::vector<CTYPE> > >(name);
        return pv[index];
    }

    /**
    * Get property buffer in %Vector.
    * @param name @Property name.
    * @return OpenCL buffer
    */
    template <typename CTYPE>
    cl::Buffer const &Vector<CTYPE>::getPropertyBuffer(std::string name) {

        // Get data handles
        PropertyMap &properties = *pProperties;
        return properties.getPropertyBuffer(name);
    }

    /**
    * Convert %Vector projection
    * @param to projection to convert to
    * @return %Vector converted into new projection
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::convert(ProjectionParameters<double> to) const {

        Vector<CTYPE> v;

        // Check for data
        if (hasData()) {

            // Clone vertices
            v.pVertices = std::shared_ptr<VariablesVector<Coordinate<CTYPE> > >(pVertices->clone());

            // Copy geometry data
            v.geometryIndexes = geometryIndexes;
            v.pointIndexes = pointIndexes;
            v.lineStringIndexes = lineStringIndexes;
            v.polygonIndexes = polygonIndexes;

            // Copy property data
            v.pProperties = pProperties;

            // Clone data
            auto &geometry = *pGeometry;
            auto &rGeometry = *v.pGeometry;
            for (auto &g : geometry) {
                rGeometry.push_back(std::shared_ptr<VectorGeometry<CTYPE> >(g->clone()));
            }

            // Return empty Vector if there are no coordinates or projection fails
            if (v.pVertices->size() == 0) {
                return v;
            }

            // Convert projection
            Projection::convert(*v.pVertices, to, proj);

            // Build tree
            v.buildTree();

            // Update projection
            v.proj = to;
        }

        return v;
    }

    /**
    * Get %Vector region from %Vector.
    * Returns all geometry intersecting region.
    * @param bounds Bounds of region
    * @param geometryTypes the types of geometry to include in resulting %Vector
    * @return %Vector of geometry within region.
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::region(BoundingBox<CTYPE> bounds, size_t geometryTypes) {

        // Get region
        std::vector<GeometryBasePtr<CTYPE> > geometryList;
        tree.search(bounds, geometryList, geometryTypes);

        // Create return vector
        Vector<CTYPE> v;

        // Set projection
        v.setProjectionParameters(proj);

        // Sort by ID
        std::sort(geometryList.begin(), geometryList.end(),
            [](const GeometryBasePtr<CTYPE> &l, const GeometryBasePtr<CTYPE> &r) { return l->getID() < r->getID(); });

        // Copy geometry
        v.pVertices = pVertices;
        v.pGeometry = pGeometry;
        for (auto &g : geometryList) {

            // Add geometry
            v.tree.insert(g);

            // Update vector
            v.geometryIndexes.push_back(g->getID());
            std::static_pointer_cast<VectorGeometry<CTYPE> >(g)->updateVector(v, g->getID());
        }

        // Copy properties
        v.pProperties = pProperties;

        return v;
    }

    /**
    * Return nearest %VectorGeometry to bounds
    * @param bounds Bounds of region
    * @param geometryTypes the types of geometry to include in resulting %Vector
    * @return %Vector containing geometry nearest to region.
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::nearest(BoundingBox<CTYPE> bounds, size_t geometryTypes) {

        // Find nearest
        std::vector<GeometryBasePtr<CTYPE> > geometryList;
        tree.nearest(bounds, geometryList, geometryTypes);

        // Create return vector
        Vector<CTYPE> v;

        // Set projection
        v.setProjectionParameters(proj);

        // Sort by ID
        std::sort(geometryList.begin(), geometryList.end(),
            [](const GeometryBasePtr<CTYPE> &l, const GeometryBasePtr<CTYPE> &r) { return l->getID() < r->getID(); });

        // Copy geometry
        v.pVertices = pVertices;
        v.pGeometry = pGeometry;
        for (auto &g : geometryList) {

            // Add geometry
            v.tree.insert(g);

            // Update vector
            v.geometryIndexes.push_back(g->getID());
            std::static_pointer_cast<VectorGeometry<CTYPE> >(g)->updateVector(v, g->getID());
        }

        // Copy properties
        v.pProperties = pProperties;

        return v;
    }

    /**
    * Get list of geometry attached to coordinate.
    * @param %Coordinate to use.
    * @param geometryTypes the types of geometry to include in list.
    * @return list of geometry within region.
    */
    template <typename CTYPE>
    std::vector<GeometryBasePtr<CTYPE> > Vector<CTYPE>::attached(Coordinate<CTYPE> c, size_t geometryTypes) {

        // Get attached geometry
        std::vector<GeometryBasePtr<CTYPE> > geometryList;
        tree.search({c, c}, geometryList, geometryTypes);
        return geometryList;
    }

    /**
    * Remove all coincident vertices from @Vector
    */
    template <typename CTYPE>
    void Vector<CTYPE>::deduplicateVertices() {

        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getData();

        // Check size
        if (vertices.size() == 0)
            return;

        // Create z-index list of vertices
        std::vector<std::pair<uint64_t, std::size_t> > zIndexes;
        for (std::size_t i = 0; i < vertices.size(); i++) {
            zIndexes.push_back ( { getBounds().createZIndex2D(vertices[i]), i } );
        }

        // Sort z-indexes
        std::sort(zIndexes.begin(), zIndexes.end(),
            [](const std::pair<uint64_t, std::size_t> &l, const std::pair<uint64_t, std::size_t> &r) { return l.first < r.first; });

        // Find coincident vertices
        std::vector<std::size_t> duplicateMap;
        duplicateMap.resize(vertices.size());
        auto zIndex = zIndexes.begin();
        do {
            // Set first index
            auto currentIndex = zIndex->first;
            auto currentVertex = zIndex->second;
            duplicateMap[currentVertex] = currentVertex;

            // Map all coincident vertices to first index
            while (++zIndex != zIndexes.end() && zIndex->first == currentIndex && vertices[zIndex->second] == vertices[currentVertex]) {
                duplicateMap[zIndex->second] = currentVertex;
            }

        } while (zIndex != zIndexes.end());

        // Build duplicate map and update vertices
        std::size_t newIndex = 0;
        std::map<std::size_t, std::size_t> newIndexes;
        for (std::size_t i = 0; i < vertices.size(); i++) {

            // Check for duplicate
            if (i == duplicateMap[i]) {

                // Update map
                newIndexes.insert( { i, newIndex } );

                // Update vertex
                vertices[newIndex] = vertices[i];

                // Increment index
                newIndex++;
            }
        }

        // Exit if there are no duplicates
        if (vertices.size() == newIndexes.size())
            return;

        // Resize vertices
        vertices.resize(newIndexes.size());

        // Update duplicate indexes
        for (std::size_t i = 0; i < duplicateMap.size(); i++) {
            duplicateMap[i] = newIndexes[duplicateMap[i]];
        }

        // Get geometry handle
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

        // Update all geometry
        for (const auto &gi : geometryIndexes) {
            auto &g = geometry[gi];

            // Process type
            if (g->isType(GeometryType::Point)) {

                // Update point vertex index
                auto &p = *std::static_pointer_cast<Point<CTYPE> >(g);
                p.pointVertex = duplicateMap[p.pointVertex];

            } else if (g->isType(GeometryType::LineString)) {

                // Update line string vertex indexes
                auto &lineString = *std::static_pointer_cast<LineString<CTYPE> >(g);
                for (std::size_t i = 0; i < lineString.lineVertices.size(); i++)
                    lineString.lineVertices[i] = duplicateMap[lineString.lineVertices[i]];

            } else if (g->isType(GeometryType::Polygon)) {

                // Update polygon vertex indexes
                auto &polygon = *std::static_pointer_cast<Polygon<CTYPE> >(g);
                for (std::size_t i = 0; i < polygon.polygonVertices.size(); i++)
                    polygon.polygonVertices[i] = duplicateMap[polygon.polygonVertices[i]];
            }
        }

    }

    /**
    * Map distance from geometry, returning a resulting raster.
    * @param resolution resolution of resulting %Raster
    * @param geometryTypes the types of geometry to include in resulting %Raster
    * @param bounds %BoundingBox of resulting %Raster, defaults to %Vector bounding box
    * @return %Raster of %Vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    Raster<RTYPE, CTYPE> Vector<CTYPE>::mapDistance(CTYPE resolution, std::string script, size_t geometryTypes, BoundingBox<CTYPE> bounds) {

        // Create raster
        Raster<RTYPE, CTYPE> r;

        // Check resolution
        if (resolution <= 0.0) {
            throw std::runtime_error("Raster resolution must be positive");
        }

        // Check bounds
        if (bounds.extent().p == 0.0 && bounds.extent().q == 0.0) {

            // Set to vector bounds
            bounds = getBounds();

            // Calculate dimensions with 1 cell boundary
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution)+2;
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution)+2;

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p-resolution, bounds.min.q-resolution);

        } else {

            // Calculate dimensions
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution);
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution);

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p, bounds.min.q);
        }

        // Set name and projection
        r.setProperty("name", "output");
        r.setProjectionParameters(proj);

        // Map vector to raster
        r.mapVector(*this, script, geometryTypes);

        return r;
    }

    /**
    * Map distance from geometry, returning a resulting raster based on the dimensions of an input Raster
    * @param rasterBase %RasterBase to use for dimensions
    * @param geometryTypes the types of geometry to include in resulting %Raster
    * @return %Raster of %Vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    Raster<RTYPE, CTYPE> Vector<CTYPE>::mapDistance(const RasterBase<CTYPE> &rasterBase, std::string script, size_t geometryTypes) {

        // Create raster
        Raster<RTYPE, CTYPE> r;
        r.init(rasterBase.getRasterDimensions().d);

        // Set name and projection
        r.setProperty("name", "output");
        r.setProjectionParameters(proj);

        // Map vector to raster
        r.mapVector(*this, script, geometryTypes);

        return r;
    }

    /**
    * Rasterize vector data, returning a resulting raster.
    * @param resolution resolution of resulting %Raster
    * @param geometryTypes the types of geometry to include in resulting %Raster
    * @param bounds %BoundingBox of resulting %Raster, defaults to %Vector bounding box
    * @return %Raster of %Vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    Raster<RTYPE, CTYPE> Vector<CTYPE>::rasterise(CTYPE resolution, std::string script, size_t geometryTypes, BoundingBox<CTYPE> bounds) {

        // Create raster
        Raster<RTYPE, CTYPE> r;

        // Check resolution
        if (resolution <= 0.0) {
            throw std::runtime_error("Raster resolution must be positive");
        }

        // Check bounds
        uint32_t buffer = 0;
        if (bounds.extent().p == 0.0 && bounds.extent().q == 0.0) {

            // Set to vector bounds
            bounds = getBounds();

            // Calculate dimensions with 1 cell boundary
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution)+2;
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution)+2;

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p-resolution, bounds.min.q-resolution);

        } else {

            // Calculate dimensions
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution);
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution);

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p, bounds.min.q);
        }

        // Set name and projection
        r.setProperty("name", "output");
        r.setProjectionParameters(proj);

        // Map vector to raster
        r.rasterise(*this, script, geometryTypes);

        return r;
    }

    /**
    * Sample raster at %Point locations, writing values to %Point property
    * @param r %Raster to sample
    */
    template <typename CTYPE>
    template <typename RTYPE>
    void Vector<CTYPE>::pointSample(Raster<RTYPE, CTYPE> &r) {

        // Check coordinate systems are the same
        if (r.getProjectionParameters() != proj) {
            throw std::runtime_error("Vector and raster projections must be the same for sampling");
        }

        // Check name
        auto name = r.template getProperty<std::string>("name");
        if (name.length() != 0) {

            // Get geometry handle
            std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

            // Sample at points
            for (auto i : pointIndexes) {

                // Get handle to point
                auto &p = *std::static_pointer_cast<Point<CTYPE> >(geometry[i]);
                auto c = getPointCoordinate(i);

                // Get raster value
                RTYPE v = r.getNearestValue(c.p, c.q);

                // Write values to Point
                setProperty<RTYPE>(geometry[i]->getID(), name, v);
            }
        } else {

            // Error if Raster has no name
            throw std::runtime_error("Sampled raster has no name");
        }
    }

    /**
    * Run script on only %Vector object
    * @param script OpenCL script to run.
    * @param v %Vector to run script over.
    */
    template <typename CTYPE>
    void Vector<CTYPE>::runScript(std::string script) {

        // Check for no data
        if (!hasData()) {
            return;
        }

        try {

            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            size_t clWorkgroupSize, clPaddedWorkgroupSize;
            if (solver.openCLInitialised()) {

                std::string fieldArgsList;
                std::string variableList;
                std::string post;

                // Check for underscores in script, these are reserved for internal variables
                if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                    throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                }

                // Search for fields
                PropertyMap &properties = *pProperties;
                std::vector<std::string> fields;
                for (auto n: properties.getPropertyNames()) {
                    if (std::regex_search(script, std::regex("\\b" + n + "\\b"))) {
                        auto type = properties.getPropertyType(n);

                        // Add fields for scalar reductions
                        if (type == PropertyType::Index ||
                            type == PropertyType::Integer ||
                            #if defined(REAL_FLOAT)
                            type == PropertyType::Float) {
                            #elif defined(REAL_DOUBLE)
                            type == PropertyType::Double) {
                            #endif

                            // Add index, integer and float fields
                            fields.push_back(n);

                        } else if (type == PropertyType::Undefined) {

                            // Create any undefined fields
                            setProperty<CTYPE>(0, n, 0.0);
                            fields.push_back(n);

                        } else {

                            // Throw error if property cannot be mapped
                            throw std::runtime_error(std::string("Property '") + n +
                                std::string("' cannot be used in script, this must be a integer or float type"));
                        }
                    }
                }

                for (std::size_t i = 0; i < fields.size(); i++) {

                    std::string &fieldName = fields[i];

                    // Get type
                    auto type = properties.getOpenCLTypeString(fieldName);

                    // Add to argument list
                    fieldArgsList += std::string(",\n__global ") + type + " *_" + fieldName;

                    // Update script for field value
                    variableList += type + " " + fieldName + " = *(_" + fieldName + "+_index);\n";
                    post += "*(_" + fieldName + "+_index) = " + fieldName + ";\n";
                }

                // Create script
                script = Solver::processScript(script);
                script = std::regex_replace(Models::cl_vector_block_c, std::regex("\\/\\*__CODE__\\*\\/"), script);
                script = std::regex_replace(script, std::regex("\\/\\*__ARGS__\\*\\/"), fieldArgsList);
                script = std::regex_replace(script, std::regex("\\/\\*__VARS__\\*\\/"), variableList);
                script = std::regex_replace(script, std::regex("\\/\\*__POST__\\*\\/"), post);

                // Build kernel
                std::size_t vectorHash = solver.buildProgram(script);
                if (vectorHash == solver.getNullHash()) {
                    throw std::runtime_error("Cannot build program");
                }
                auto &vectorKernel = solver.getKernel(vectorHash, "vector");

                // Get geometry
                std::size_t gSize = geometryIndexes.size();

                // Set kernel arguments
                cl_uint arg = 0;
                vectorKernel.setArg(arg++, (cl_uint)gSize);

                // Create and add index buffer
                auto indexBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, gSize*sizeof(cl_uint), geometryIndexes.data());
                void *indexBufferPtr = queue.enqueueMapBuffer(indexBuffer, CL_TRUE, CL_MAP_WRITE, 0, gSize*sizeof(cl_uint));
                if (indexBufferPtr != static_cast<void *>(geometryIndexes.data()))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                queue.enqueueUnmapMemObject(indexBuffer, static_cast<void *>(geometryIndexes.data()));
                vectorKernel.setArg(arg++, indexBuffer);

                // Add field buffers
                for (auto &n : fields) {
                    vectorKernel.setArg(arg++, getPropertyBuffer(n));
                }

                // Execute vector kernel
                clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(vectorKernel);
                clPaddedWorkgroupSize = (size_t)(gSize+((clWorkgroupSize-(gSize%clWorkgroupSize))%clWorkgroupSize));
                queue.enqueueNDRangeKernel(vectorKernel, cl::NullRange,
                    cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                // Unmap buffers
                indexBufferPtr = queue.enqueueMapBuffer(indexBuffer, CL_TRUE, CL_MAP_READ, 0, gSize*sizeof(cl_uint));
                if (indexBufferPtr != static_cast<void *>(geometryIndexes.data()))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

                // Apply filter
                geometryIndexes.erase(std::stable_partition(geometryIndexes.begin(), geometryIndexes.end(),
                    [](cl_uint v) {return v != getNullValue<cl_uint>();} ), std::end(geometryIndexes));
                if (gSize != geometryIndexes.size()) {

                    // Clear indexes
                    tree.clear();
                    pointIndexes.clear();
                    lineStringIndexes.clear();
                    polygonIndexes.clear();

                    // Rebuild
                    std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
                    for (std::size_t i = 0; i < geometryIndexes.size(); i++) {

                        // Update tree
                        tree.insert(geometry[i]);

                        // Update geometry index
                        geometry[i]->updateVector(*this, i);
                    }
                }

            } else {
                throw std::runtime_error("OpenCL not initialised");
            }


        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }

    }

    /**
    * Run script on %Vector with %Raster layers
    * @param script OpenCL script to run.
    * @param v %Vector to run script over.
    * @param rasterBaseRefs list of input raster references.
    * @param reductionType reduction type to apply to the Rasters.
    * @param parameters parameter flags for combination and aliasing.
    */
    template <typename RTYPE, typename CTYPE>
    void runVectorScript(
        std::string script,
        Vector<CTYPE> &v,
        std::vector<RasterBaseRef<CTYPE> > rasterBaseRefs,
        Reduction::Type reductionType) {

        // Check for no data
        if (!v.hasData()) {
            return;
        }

        try {

            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            uint8_t verbose = solver.getVerboseLevel();
            size_t clWorkgroupSize, clPaddedWorkgroupSize;
            if (solver.openCLInitialised()) {

                // Run script
                if (rasterBaseRefs.size() > 0) {

                    std::string fieldArgsList;
                    std::string variableList;
                    std::string post;

                    // Check for underscores in script, these are reserved for internal variables
                    if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                        throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                    }

                    // Search for fields
                    std::vector<std::string> fields;
                    auto &properties = v.getProperties();
                    for (auto n: properties.getPropertyNames()) {
                        if (std::regex_search(script, std::regex("\\b" + n + "\\b"))) {
                            auto type = properties.getPropertyType(n);

                            // Add fields for scalar reductions
                            if (reductionType != Reduction::None) {

                                #if defined(REAL_FLOAT)
                                if (type == PropertyType::Float) {
                                #elif defined(REAL_DOUBLE)
                                if (type == PropertyType::Double) {
                                #endif

                                    // Add index, integer and float fields
                                    fields.push_back(n);

                                } else if (type == PropertyType::Undefined) {

                                    // Create any undefined fields
                                    v.template setProperty<RTYPE>(0, n, 0.0);
                                    fields.push_back(n);

                                } else {

                                    // Throw error if property cannot be mapped
                                    throw std::runtime_error(std::string("Property '") + n +
                                        std::string("' cannot be used in script, this must a float type"));
                                }
                            } else {

                                if (type == PropertyType::Undefined) {
                                
                                    // Add to field list
                                    fields.push_back(n);

                                    // Redefine property as vector
                                    v.template setProperty<std::vector<RTYPE> >(0, n, std::vector<RTYPE>());


                                #if defined(REAL_FLOAT)

                                } else if (type == PropertyType::FloatVector) {

                                    auto &pv = v.template getPropertyVectors<std::vector<std::vector<float> > >(n);

                                #elif defined(REAL_DOUBLE)

                                } else if (type == PropertyType::DoubleVector) {

                                    auto &pv = v.template getPropertyVectors<std::vector<std::vector<double> > >(n);

                                #endif

                                    // Clear properties
                                    for (auto &v : pv) {
                                        v.clear();
                                    }

                                    // Add to field list
                                    fields.push_back(n);

                                } else {
                                    throw std::runtime_error("Cannot map non floating-point property type to floating-point property vector");
                                }
                            }
                        }
                    }

                    for (std::size_t i = 0; i < fields.size(); i++) {

                        std::string &fieldName = fields[i];

                        // Add to argument list
                        fieldArgsList += ",\n__global REAL *_" + fieldName;

                        // Update script for field value
                        variableList += "REAL " + fieldName + " = *(_" + fieldName + "+_index);\n";
                        post += "*(_" + fieldName + "+_index) = " + fieldName + ";\n";
                    }

                    // Check dimensions, this is currently restricted to 2D anchor layers
                    RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
                    auto dimFirst = rasterBaseFirst.getRasterDimensions();
                    if (dimFirst.d.nz > 1) {
                        throw std::runtime_error("Only 2D raster layers are supported for Vector scripting");
                    }

                    // Turn off all raster writes
                    std::vector<bool> rasterBaseNeedsWrite;
                    for (std::size_t i = 0; i < rasterBaseRefs.size(); i++) {
                        rasterBaseNeedsWrite.push_back(rasterBaseRefs[i].get().getNeedsWrite());
                        rasterBaseRefs[i].get().setNeedsWrite(false);
                    }

                    // Generate raster kernel script
                    auto rasterIndexedGenerator = Geostack::generateKernel<CTYPE>(script, Models::cl_raster_indexed_block_c, rasterBaseRefs);
                    if (rasterIndexedGenerator.script.size() == 0) {
                        throw std::runtime_error("Cannot generate script");
                    }

                    // Restore raster writes
                    for (std::size_t i = 0; i < rasterBaseRefs.size(); i++) {
                        rasterBaseRefs[i].get().setNeedsWrite(rasterBaseNeedsWrite[i]);
                    }

                    // Patch raster kernel template with additional code
                    fieldArgsList += ",";
                    rasterIndexedGenerator.script = std::regex_replace(rasterIndexedGenerator.script, std::regex("\\/\\*__ARGS2__\\*\\/"), fieldArgsList);
                    rasterIndexedGenerator.script = std::regex_replace(rasterIndexedGenerator.script, std::regex("\\/\\*__VARS2__\\*\\/"), variableList);
                    rasterIndexedGenerator.script = std::regex_replace(rasterIndexedGenerator.script, std::regex("\\/\\*__POST2__\\*\\/"), post);

                    // Build raster kernel
                    std::size_t rasterIndexedHash = buildRasterKernel(rasterIndexedGenerator);
                    if (rasterIndexedHash == solver.getNullHash()) {
                        throw std::runtime_error("Cannot get kernel for script");
                    }
                    auto &rasterIndexedKernel = solver.getKernel(rasterIndexedHash, "rasterIndexed");

                    // Create indexing kernel code
                    std::string indexKernelStr;
                    bool usingProjection = (rasterBaseFirst.getProjectionParameters() != v.getProjectionParameters());
                    if (usingProjection) {

                        // Update projection definitions
                        auto projStr = Models::cl_projection_head_c;
                        auto rproj = rasterBaseFirst.getProjectionParameters();
                        auto vproj = v.getProjectionParameters();
                        if (rproj.type == 1) {
                            std::string projDef = "USE_PROJ_TYPE_" + std::to_string(rproj.cttype);
                            projStr = std::regex_replace(projStr, std::regex(std::string("__") + projDef), projDef);
                        }
                        if (vproj.type == 1) {
                            std::string projDef = "USE_PROJ_TYPE_" + std::to_string(vproj.cttype);
                            projStr = std::regex_replace(projStr, std::regex(std::string("__") + projDef), projDef);
                        }

                        indexKernelStr = projStr+Models::cl_index_c;
                    } else {
                        indexKernelStr = Models::cl_index_c;
                    }
                    indexKernelStr = std::regex_replace(indexKernelStr, std::regex("TYPE"), Solver::getOpenCLTypeString<RTYPE>());

                    // Create indexing argument list
                    std::string indexingArgsList;
                    if (usingProjection) {
                        indexingArgsList += ",\nconst ProjectionParameters _rproj";
                        indexingArgsList += ",\nconst ProjectionParameters _vproj";
                    }

                    // Patch indexing script
                    indexKernelStr = std::regex_replace(indexKernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), indexingArgsList);
                    if (usingProjection) {
                        indexKernelStr = std::regex_replace(indexKernelStr, std::regex("\\/\\*__PROJECT__|__PROJECT__\\*\\/"), std::string(""));
                    }

                    // Build indexing kernel
                    std::size_t indexingHash = solver.buildProgram(indexKernelStr);
                    if (indexingHash == solver.getNullHash()) {
                        throw std::runtime_error("Cannot build program");
                    }

                    // Create reduction variables
                    auto geometrySize = v.getGeometryIndexes().size();
                    std::vector<std::vector<RTYPE> > fieldValuesReduce;
                    std::vector<std::vector<RTYPE> > fieldValuesCount;

                    if (reductionType != Reduction::None) {

                        // Initialise reduction
                        fieldValuesReduce.resize(fields.size());
                        fieldValuesCount.resize(fields.size());
                        for (std::size_t f = 0; f < fields.size(); f++) {

                            // Create reduction vector
                            fieldValuesReduce[f].resize(geometrySize);
                            if (reductionType == Reduction::Mean || reductionType == Reduction::Count)
                                fieldValuesCount[f].resize(geometrySize);

                            // Initialise reduction vector
                            switch (reductionType) {

                                case(Reduction::Maximum):
                                    std::fill(fieldValuesReduce[f].begin(), fieldValuesReduce[f].end(), getNullValue<RTYPE>()); // TODO update for other types
                                    break;

                                case(Reduction::Minimum):
                                    std::fill(fieldValuesReduce[f].begin(), fieldValuesReduce[f].end(), getNullValue<RTYPE>()); // TODO update for other types
                                    break;

                                case(Reduction::Sum):
                                case(Reduction::Mean):
                                    std::fill(fieldValuesReduce[f].begin(), fieldValuesReduce[f].end(), getNullValue<RTYPE>());
                                    std::fill(fieldValuesCount[f].begin(), fieldValuesCount[f].end(), 0.0);
                                    break;

                                case(Reduction::Count):
                                    std::fill(fieldValuesCount[f].begin(), fieldValuesCount[f].end(), 0.0);
                                    break;

                            }
                        }
                    }

                    // Create field vectors
                    std::vector<std::vector<RTYPE> > fieldValues;
                    std::vector<cl::Buffer > fieldBuffers;
                    fieldValues.resize(fields.size());
                    fieldBuffers.resize(fields.size());

                    // Loop over tiles
                    for (uint32_t tj = 0; tj < dimFirst.ty; tj++) {
                        for (uint32_t ti = 0; ti < dimFirst.tx; ti++) {

                            // Get indexes
                            auto tileVectorIndexes = rasterBaseFirst.getVectorTileIndexes(v, ti, tj, indexingHash);
                            auto iSize = tileVectorIndexes.size();

                            // Check for cells
                            if (iSize > 0) {

                                // Create array index buffer
                                AutoBuffer<RasterIndex<CTYPE> > tileIndexBuffer(context, queue, tileVectorIndexes);

                                // Set kernel arguments
                                cl_uint arg = 0;
                                rasterIndexedKernel.setArg(arg++, tileIndexBuffer.getBuffer());
                                rasterIndexedKernel.setArg(arg++, (cl_uint)iSize);

                                // Set field kernel arguments
                                for (std::size_t f = 0; f < fields.size(); f++) {

                                    // Create and populate values
                                    fieldValues[f].resize(iSize);
                                    if (reductionType != Reduction::None) {
                                        for (std::size_t j = 0; j < iSize; j++) {
                                            fieldValues[f][j] = v.template getProperty<RTYPE>(tileVectorIndexes[j].id, fields[f]);
                                        }
                                    }

                                    // Map memory
                                    fieldBuffers[f] = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, iSize*sizeof(RTYPE), fieldValues[f].data());
                                    void *fieldBufferPtr = queue.enqueueMapBuffer(fieldBuffers[f], CL_TRUE, CL_MAP_WRITE, 0, iSize*sizeof(RTYPE));
                                    if (fieldBufferPtr != static_cast<void *>(fieldValues[f].data()))
                                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                                    queue.enqueueUnmapMemObject(fieldBuffers[f], static_cast<void *>(fieldValues[f].data()));

                                    // Set buffer
                                    rasterIndexedKernel.setArg(arg++, fieldBuffers[f]);
                                }

                                // Set remaining arguments
                                Geostack::setTileKernelArguments<CTYPE>(ti, tj, rasterIndexedKernel, rasterBaseRefs, rasterIndexedGenerator.reqs, arg);

                                // Execute vector kernel
                                clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(rasterIndexedKernel);
                                clPaddedWorkgroupSize = (size_t)(iSize+((clWorkgroupSize-(iSize%clWorkgroupSize))%clWorkgroupSize));
                                queue.enqueueNDRangeKernel(rasterIndexedKernel, cl::NullRange,
                                    cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                                // Unmap data
                                for (std::size_t f = 0; f < fields.size(); f++) {
                                    void *fieldBufferPtr = queue.enqueueMapBuffer(fieldBuffers[f], CL_TRUE, CL_MAP_READ, 0, iSize*sizeof(RTYPE));
                                    if (fieldBufferPtr != static_cast<void *>(fieldValues[f].data()))
                                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

                                    if (reductionType != Reduction::None) {

                                        // Calculate reduction
                                        switch (reductionType) {

                                            case(Reduction::Maximum):
                                                for (std::size_t j = 0; j < iSize; j++) {
                                                    fieldValuesReduce[f][tileVectorIndexes[j].id] =
                                                        fmax(fieldValuesReduce[f][tileVectorIndexes[j].id], fieldValues[f][j]);
                                                }
                                                break;

                                            case(Reduction::Minimum):
                                                for (std::size_t j = 0; j < iSize; j++) {
                                                    fieldValuesReduce[f][tileVectorIndexes[j].id] =
                                                        fmin(fieldValuesReduce[f][tileVectorIndexes[j].id], fieldValues[f][j]);
                                                }
                                                break;

                                            case(Reduction::Sum):
                                                for (std::size_t j = 0; j < iSize; j++) {
                                                    auto v = fieldValuesReduce[f][tileVectorIndexes[j].id];
                                                    if (v == v) {
                                                        fieldValuesReduce[f][tileVectorIndexes[j].id] += fieldValues[f][j];
                                                    } else {
                                                        fieldValuesReduce[f][tileVectorIndexes[j].id] = fieldValues[f][j];
                                                    }
                                                }
                                                break;

                                            case(Reduction::Mean):
                                                for (std::size_t j = 0; j < iSize; j++) {
                                                    auto v = fieldValuesReduce[f][tileVectorIndexes[j].id];
                                                    if (v == v) {
                                                        fieldValuesReduce[f][tileVectorIndexes[j].id] += fieldValues[f][j];
                                                        fieldValuesCount[f][tileVectorIndexes[j].id] += 1.0;
                                                    } else {
                                                        fieldValuesReduce[f][tileVectorIndexes[j].id] = fieldValues[f][j];
                                                        fieldValuesCount[f][tileVectorIndexes[j].id] = 1.0;
                                                    }
                                                }
                                                break;

                                            case(Reduction::Count):
                                                for (std::size_t j = 0; j < iSize; j++) {
                                                    auto v = fieldValuesReduce[f][tileVectorIndexes[j].id];
                                                    if (v == v) {
                                                        fieldValuesCount[f][tileVectorIndexes[j].id] += 1.0;
                                                    }
                                                }
                                                break;
                                        }
                                    } else {

                                        // Get vector properties
                                        #if defined(REAL_FLOAT)
                                        auto &pv = v.template getPropertyVectors<std::vector<std::vector<float> > >(fields[f]);
                                        #elif defined(REAL_DOUBLE)
                                        auto &pv = v.template getPropertyVectors<std::vector<std::vector<double> > >(fields[f]);
                                        #endif

                                        // Append to properties
                                        for (std::size_t j = 0; j < iSize; j++) {
                                            pv[tileVectorIndexes[j].id].push_back(fieldValues[f][j]);
                                        }
                                    }
                                }
                            }
                        }

                        // Output progress
                        if (verbose <= Geostack::Verbosity::Info && (tj%10) == 0) {
                            std::cout << std::setprecision(2) << 100.0*(double)tj/(double)dimFirst.ty << "%" << std::endl;
                        }
                    }

                    if (reductionType != Reduction::None) {

                        // Write reduction values to vector
                        for (std::size_t f = 0; f < fields.size(); f++) {
                            for (std::size_t i = 0; i < geometrySize; i++) {

                                if (reductionType == Reduction::Count) {

                                    // Set property
                                    v.template setProperty<RTYPE>(i, fields[f], fieldValuesCount[f][i]);

                                } else {

                                    // Apply final reduction step
                                    if (reductionType == Reduction::Mean) {
                                        if (fieldValuesCount[f][i] > 0.0) {
                                            fieldValuesReduce[f][i]/=fieldValuesCount[f][i];
                                        }
                                    }

                                    // Set property
                                    v.template setProperty<RTYPE>(i, fields[f], fieldValuesReduce[f][i]);
                                }
                            }
                        }

                    } else {

                        // Sort data
                        for (std::size_t f = 0; f < fields.size(); f++) {
                            auto &pv = v.template getPropertyVectors<std::vector<std::vector<RTYPE> > >(fields[f]);
                            for (auto &pvv : pv) {

                                // Remove nodata
                                pvv.erase(std::remove_if(pvv.begin(), pvv.end(), [](const RTYPE &v) { return v != v; } ), pvv.end());

                                // Sort vector
                                std::sort(pvv.begin(), pvv.end());
                            }
                        }

                    }

                    if (verbose <= Geostack::Verbosity::Info) {
                        std::cout << "100%" << std::endl;
                    }

                } else {

                    // Run vector script only
                    v.runScript(script);
                }

            } else {
                throw std::runtime_error("OpenCL not initialised");
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }

    // Float type definitions
    template class VectorGeometry<float>;
    template class Point<float>;
    template class LineString<float>;
    template class Polygon<float>;
    template class Vector<float>;

    template void Vector<float>::pointSample<float>(Raster<float, float> &);
    template Raster<float, float> Vector<float>::mapDistance<float>(float, std::string, std::size_t, BoundingBox<float>);
    template Raster<float, float> Vector<float>::mapDistance<float>(const RasterBase<float> &, std::string, std::size_t);
    template Raster<float, float> Vector<float>::rasterise<float>(float, std::string, std::size_t, BoundingBox<float>);
    template void runVectorScript<float, float>(std::string, Vector<float> &, std::vector<std::reference_wrapper<RasterBase<float> > >, Reduction::Type);

    template void Vector<float>::setProperty<std::string>(cl_uint, std::string, std::string);
    template void Vector<float>::setProperty<int>(cl_uint, std::string, int v);
    template void Vector<float>::setProperty<float>(cl_uint, std::string, float v);
    template void Vector<float>::setProperty<cl_uint>(cl_uint, std::string, cl_uint v);
    template void Vector<float>::setProperty<std::vector<float> >(cl_uint, std::string, std::vector<float> v);

    template void Vector<float>::setProperty<std::vector<std::string> >(std::string, std::vector<std::string> v);
    template void Vector<float>::setProperty<std::vector<int> >(std::string, std::vector<int> v);
    template void Vector<float>::setProperty<std::vector<float> >(std::string, std::vector<float> v);
    template void Vector<float>::setProperty<std::vector<cl_uint> >(std::string, std::vector<cl_uint> v);

    template std::string Vector<float>::getProperty<std::string>(cl_uint, std::string);
    template int Vector<float>::getProperty<int>(cl_uint, std::string);
    template float Vector<float>::getProperty<float>(cl_uint, std::string);
    template cl_uint Vector<float>::getProperty<cl_uint>(cl_uint, std::string);
    template std::vector<float> Vector<float>::getProperty<std::vector<float> >(cl_uint, std::string);

    template std::vector<std::vector<float> > &Vector<float>::getPropertyVectors<std::vector<std::vector<float> > >(std::string);
    template std::vector<float> &Vector<float>::getPropertyVector<std::vector<float> >(cl_uint, std::string);

    template void Vector<float>::convertProperty<std::string>(std::string);
    template void Vector<float>::convertProperty<int>(std::string);
    template void Vector<float>::convertProperty<float>(std::string);
    template void Vector<float>::convertProperty<cl_uint>(std::string);

    // Double type definitions
    template class VectorGeometry<double>;
    template class Point<double>;
    template class LineString<double>;
    template class Polygon<double>;
    template class Vector<double>;

    template void Vector<double>::pointSample<double>(Raster<double, double> &);
    template Raster<double, double> Vector<double>::mapDistance<double>(double, std::string, std::size_t, BoundingBox<double>);
    template Raster<double, double> Vector<double>::mapDistance<double>(const RasterBase<double> &, std::string, std::size_t);
    template Raster<double, double> Vector<double>::rasterise<double>(double, std::string, std::size_t, BoundingBox<double>);
    template void runVectorScript<double, double>(std::string, Vector<double> &, std::vector<std::reference_wrapper<RasterBase<double> > >, Reduction::Type);

    template void Vector<double>::setProperty<std::string>(cl_uint, std::string, std::string v);
    template void Vector<double>::setProperty<int>(cl_uint, std::string, int v);
    template void Vector<double>::setProperty<double>(cl_uint, std::string, double v);
    template void Vector<double>::setProperty<cl_uint>(cl_uint, std::string, cl_uint v);
    template void Vector<double>::setProperty<std::vector<double> >(cl_uint, std::string, std::vector<double> v);

    template void Vector<double>::setProperty<std::vector<std::string> >(std::string, std::vector<std::string> v);
    template void Vector<double>::setProperty<std::vector<int> >(std::string, std::vector<int> v);
    template void Vector<double>::setProperty<std::vector<double> >(std::string, std::vector<double> v);
    template void Vector<double>::setProperty<std::vector<cl_uint> >(std::string, std::vector<cl_uint> v);

    template std::string Vector<double>::getProperty<std::string>(cl_uint, std::string);
    template int Vector<double>::getProperty<int>(cl_uint, std::string);
    template double Vector<double>::getProperty<double>(cl_uint, std::string);
    template cl_uint Vector<double>::getProperty<cl_uint>(cl_uint, std::string);
    template std::vector<double> Vector<double>::getProperty<std::vector<double> >(cl_uint, std::string);

    template std::vector<std::vector<double> > &Vector<double>::getPropertyVectors<std::vector<std::vector<double> > >(std::string);
    template std::vector<double> &Vector<double>::getPropertyVector<std::vector<double> >(cl_uint, std::string);

    template void Vector<double>::convertProperty<std::string>(std::string);
    template void Vector<double>::convertProperty<int>(std::string);
    template void Vector<double>::convertProperty<double>(std::string);
    template void Vector<double>::convertProperty<cl_uint>(std::string);
}
