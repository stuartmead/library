get_filename_component(Geostack_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(CMakeFindDependencyMacro)

if(NOT TARGET Geostack::Geostack)
    include("${Geostack_CMAKE_DIR}/GeostackTargets.cmake")
endif()