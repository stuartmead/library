/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_RASTER_H
#define GEOSTACK_RASTER_H

#include <atomic>
#include <memory>
#include <vector>
#include <list>
#include <set>
#include <unordered_map>
#include <iterator>
#include <functional>
#include <fstream>

#ifdef USE_TILE_CACHING
#include <thread>
#endif

#include "gs_opencl.h"
#include "gs_geometry.h"
#include "gs_variables.h"
#include "gs_property.h"
#include "gs_projection.h"

#define STRINGIFY(s) (#s)

/**
* Helper macro to create a named raster with the same name as the internal variable
*/
#define makeRaster(NAME, RTYPE, CTYPE) Geostack::Raster<RTYPE, CTYPE> NAME(STRINGIFY(NAME));

namespace Geostack
{
    // Forward declarations
    class Solver;

    template <typename CTYPE>
    class BoundingBox;

    template <typename CTYPE>
    class Vector;

    template <typename RTYPE, typename CTYPE>
    class Tile;
    
    template <typename CTYPE>
    class RasterBase;

    template <typename RTYPE, typename CTYPE>
    class Raster;
    
    // Definitions
    template <typename RTYPE, typename CTYPE>
    using TilePtr = std::shared_ptr<Tile<RTYPE, CTYPE> >;
    
    template <typename RTYPE, typename CTYPE>
    using RasterPtr = std::shared_ptr<Raster<RTYPE, CTYPE> >;

    template <typename CTYPE>
    using RasterBasePtr = std::shared_ptr<RasterBase<CTYPE> >;
    
    template <typename CTYPE>
    using RasterBaseRef = std::reference_wrapper<RasterBase<CTYPE> >;
    
    template <typename CTYPE>
    using RasterBaseRefs = std::vector<RasterBaseRef<CTYPE> >;

    using tileIndexSet = std::set<std::pair<uint32_t, uint32_t> >; 

    /**
    * Combination types
    */
    namespace RasterCombination {
        enum Type {
            Union        = 0,      ///< Combination is union of rasters
            Intersection = 1 << 0, ///< Combination is intersection of rasters
        };
    }

    /**
    * Resolution type
    */
    namespace RasterResolution {
        enum Type {
            Minimum = 0,      ///< Resolution is maximum resolution of rasters
            Maximum = 1 << 2, ///< Resolution is minimum resolution of rasters
        };
    }

    /**
    * Interpolation types.
    */
    namespace RasterInterpolation {
        enum Type {
            Nearest  = 0,      ///< Nearest neighbour interpolation
            Bilinear = 1 << 4, ///< Bilinear interpolation
            Bicubic  = 2 << 4, ///< Bicubic interpolation
        };
    }

    /**
    * Value for nodata in calculation.
    */
    namespace RasterNullValue {
        enum Type {
            Null = 0,      ///< Use null
            Zero = 1 << 6, ///< Use zero
            One  = 2 << 6, ///< Use one
        };
    }

    /**
    * Raster reduction type
    */
    namespace Reduction {
        enum Type {
            None       = 0,       ///< No reduction
            Maximum    = 1 << 8,  ///< Maximum over raster
            Minimum    = 2 << 8,  ///< Minimum over raster
            Sum        = 3 << 8,  ///< Sum over raster
            Count      = 4 << 8,  ///< Count of raster data values
            Mean       = 5 << 8,  ///< Mean over raster
            SumSquares = 6 << 8,  ///< Sum of squares of raster values
        };
    }

    /**
    * Raster debug type
    */
    namespace RasterDebug {
        enum Type {
            None       = 0 << 12, //< No debugging
            Enable     = 1 << 12, ///< Enable debugging
        };
    }

    /**
    * Raster neighbour type
    */
    namespace Neighbours {
        enum Type {
            None = 0,      ///< No neighbours
            N  = 1 << 0,   ///< North neighbour
            NE = 1 << 1,   ///< North-east neighbour
            E  = 1 << 2,   ///< East neighbour
            SE = 1 << 3,   ///< South-east neighbour
            S =  1 << 4,   ///< South neighbour
            SW = 1 << 5,   ///< South-west neighbour
            W =  1 << 6,   ///< West neighbour
            NW = 1 << 7,   ///< North-west neighbour
            Rook = 0x55,   ///< N, E, S and W neighbours
            Bishop = 0xAA, ///< NE, SW, SW and NW neighbours
            Queen = 0xFF,  ///< All neighbours
        };
    }

    namespace TileMetrics {
    
        // Tile size power
        const uint32_t tileSizePower = 8;

        // Tile size
        const uint32_t tileSize = 1 << tileSizePower;

        // Tile size squared
        const uint32_t tileSizeSquared = tileSize*tileSize;

        // Tile mask
        const uint32_t tileSizeMask = tileSize-1;
        
        // Tile ranges
        const cl::NDRange rangeTile2DGlobal = cl::NDRange(tileSize, tileSize);
        const cl::NDRange rangeTile2DLocal = cl::NDRange(tileSize, 1);

        // Tile reduction sizes
        const uint32_t tileReduceSize = (tileSize*tileSize)/(rangeTile2DLocal[0]*rangeTile2DLocal[1]);
    }
    
    /**
    * %VectorRasterIndexes class for indexes of %Raster in %Vector
    * with corresponding area weighting.
    */    
    template <typename RTYPE>
    struct alignas(8) RasterIndex {
        uint32_t id;
        uint16_t i;
        uint16_t j;
        RTYPE w;
    };

    /**
    * %General dimension structure
    */
    template <typename CTYPE>
    struct alignas(8) Dimensions {

        uint32_t nx; ///< Number of data cells in x dimension
        uint32_t ny; ///< Number of data cells in y dimension
        uint32_t nz; ///< Number of data cells in z dimension
        CTYPE hx;    ///< Spacing in x dimension
        CTYPE hy;    ///< Spacing in y dimension
        CTYPE hz;    ///< Spacing in z dimension
        CTYPE ox;    ///< Start coordinate in x dimension
        CTYPE oy;    ///< Start coordinate in y dimension
        CTYPE oz;    ///< Start coordinate in z dimension
        uint32_t mx; ///< Number of cells stored in memory for x dimension
        uint32_t my; ///< Number of cells stored in memory for y dimension
    };

    /**
    * %Tile dimension structure
    */
    template <typename CTYPE>
    struct alignas(8) TileDimensions {
    
        Dimensions<CTYPE> d; ///< General dimensions
        CTYPE ex;            ///< End coordinate in x dimension
        CTYPE ey;            ///< End coordinate in y dimension
        CTYPE ez;            ///< End coordinate in z dimension
        uint32_t ti;         ///< Tile index in x dimension
        uint32_t tj;         ///< Tile index in y dimension
    };

    /**
    * %Raster dimension structure
    */
    template <typename CTYPE>
    struct alignas(8) RasterDimensions {
    
        Dimensions<CTYPE> d; ///< General dimensions
        CTYPE ex;            ///< End coordinate in x dimension
        CTYPE ey;            ///< End coordinate in y dimension
        CTYPE ez;            ///< End coordinate in z dimension
        uint32_t tx;         ///< Number of tiles in x dimension
        uint32_t ty;         ///< Number of tiles in y dimension
    };
    
    /**
    * %RandomState structure
    */
    struct alignas(8) RandomState {
	    cl_uint a;
        cl_uint b;
        cl_uint c;
        cl_uint d;
    };

    class KernelRasterRequirement {
    public:
        /**
        * %KernelRequirements constructors
        */
        KernelRasterRequirement(): 
            used(true), 
            requiredNeighbours(0) {
        }
        KernelRasterRequirement(uint8_t _requiredNeighbours): 
            used(true),
            requiredNeighbours(_requiredNeighbours) {
        }      
        KernelRasterRequirement(const KernelRasterRequirement &r): 
            used(r.used),
            requiredNeighbours(r.requiredNeighbours) {
        }
        KernelRasterRequirement &operator=(const KernelRasterRequirement &r) {
            if (this != &r) {
                used = r.used;
                requiredNeighbours = r.requiredNeighbours;
            }
            return *this; 
        }

        bool used;                  // Whether Raster is used in the Kernel
        uint8_t requiredNeighbours; // Bitmask of required neighbours        
    };

    class KernelRequirements {    
    public:

        /**
        * %KernelRequirements constructors
        */
        KernelRequirements(): 
            usesRandom(false),
            usesBoundary(false) {
        }
        KernelRequirements(std::vector<KernelRasterRequirement> rasterRequirements_): 
            rasterRequirements(rasterRequirements_), 
            usesRandom(false),
            usesBoundary(false) {
        }        
        KernelRequirements(const KernelRequirements &r): 
            usesRandom(r.usesRandom),
            rasterRequirements(r.rasterRequirements),
            usesBoundary(r.usesBoundary) {
        }
        KernelRequirements &operator=(const KernelRequirements &r) {
            if (this != &r) {
                usesRandom = r.usesRandom;
                rasterRequirements = r.rasterRequirements;
                usesBoundary = r.usesBoundary;
            }
            return *this; 
        }

        bool usesRandom;            // Whether Raster random values are used in the Kernel
        bool usesBoundary;          // Whether tile boundary flag is required by the Kernel
        std::vector<KernelRasterRequirement> rasterRequirements;        
    };

    /**
    * %KernelGenerator class
    * Holds parsed script and flags for script building
    */
    class KernelGenerator {
    public:

        /**
        * %KernelGenerator constructors
        */
        KernelGenerator():script() {
            programHash = Solver::getNullHash();
        }      
        KernelGenerator(const KernelGenerator &r): 
            script(r.script), 
            projectionTypes(r.projectionTypes),
            reqs(r.reqs),
            programHash(r.programHash) {
        }
        KernelGenerator &operator=(const KernelGenerator &r) {
            if (this != &r) {
                script = r.script; 
                projectionTypes = r.projectionTypes;
                reqs = r.reqs;
                programHash = r.programHash;
            }
            return *this; 
        }

        std::string script;                 ///< Parsed script
        std::set<uint32_t> projectionTypes; ///< Projection types used in script
        
        /**
        * Vector of Raster requirements
        */
        KernelRequirements reqs;
        
        /**
        * Incoming script hash
        */
        std::size_t programHash;
    };
    
    /**
    * %KernelCache class
    * Contains program hashes and corresponding generators
    */
    class KernelCache {  

    public:

        KernelCache() { };
        ~KernelCache() { };

        // Singleton instance of OpenCL solver.
        static KernelCache &getKernelCache();

        // Prevent copy and assignment
        KernelCache(KernelCache const&) = delete;
        void operator=(KernelCache const&) = delete;        

        // Member functions
        void setKernelGenerator(std::size_t, KernelGenerator);
        KernelGenerator getKernelGenerator(std::size_t scriptHash);

    private:
        std::map<std::size_t, KernelGenerator> generatorMap; 
    };

    // Dimensions equality check.
    template <typename CTYPE>
    bool equalSpatialMetrics2D(const Dimensions<CTYPE> l, const Dimensions<CTYPE> r);

    template <typename CTYPE>
    bool equalSpatialMetrics(const Dimensions<CTYPE> l, const Dimensions<CTYPE> r);

    // Data handlers
    template <typename RTYPE, typename CTYPE>
    using dataHandlerReadFunction = std::function<void(TileDimensions<CTYPE>, std::vector<RTYPE> &, Raster<RTYPE, CTYPE> &)>;

    template <typename RTYPE, typename CTYPE>
    using dataHandlerWriteFunction = std::function<void(TileDimensions<CTYPE>, std::vector<RTYPE> &, Raster<RTYPE, CTYPE> &)>;

    // Raster dimensions output stream
    template <typename CTYPE>
    std::ostream &operator<<(std::ostream &os, const RasterDimensions<CTYPE> &r);

    template <typename CTYPE>
    void runScriptNoOut(
        std::string script, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        size_t parameters = 0);
        
    template <typename CTYPE>
    std::size_t createScriptHash(
        std::string script, 
        std::string kernelBlock,
        RasterBaseRefs<CTYPE> rasterBaseRefs);
        
    template <typename CTYPE>
    KernelGenerator generateKernel(
        std::string script, 
        std::string kernelBlock,
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        RasterNullValue::Type rasterNullValue = RasterNullValue::Null,
        VariablesBasePtr<std::string> variables = nullptr,
        bool useConst = true);

    template <typename CTYPE>
    bool setTileKernelArguments(
        uint32_t ti, 
        uint32_t tj, 
        cl::Kernel &kernel, 
        RasterBaseRefs<CTYPE> rasterBaseRefs,
        KernelRequirements reqs, 
        int startArg = 0,
        VariablesBasePtr<std::string> variables = nullptr,
        bool useDimensions = true);

    template <typename CTYPE>
    bool runTileKernel(
        uint32_t ti, 
        uint32_t tj, 
        cl::Kernel &kernel, 
        RasterBaseRefs<CTYPE> rasterBaseRefs,
        KernelRequirements reqs,
        int startArg = 0,
        VariablesBasePtr<std::string> variables = nullptr,
        size_t debug = 0);

    std::size_t buildRasterKernel(KernelGenerator &kernelGenerator);

    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runScript(
        std::string script, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        size_t parameters = 0);

    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runAreaScript(
        std::string script, 
        RasterBase<CTYPE> &r, 
        cl_int width);    
    
    /**
    * Run script on %Raster
    * Aliased runScript function
    * @param script OpenCL script to run.
    * @param rasterBaseRefs list of input raster references.
    * @param parameters parameter flags for combination and aliasing.
    * @return true if script is successful, false otherwise
    */
    template <typename CTYPE>
    void runScript(std::string script, RasterBaseRefs<CTYPE> rasterBaseRefs, size_t parameters = 0) {
        runScriptNoOut(script, rasterBaseRefs, parameters);
    }
    
    template <typename RTYPE, typename CTYPE>
    Vector<CTYPE> stipple(std::string script, RasterBaseRefs<CTYPE> rasterBaseRefs, 
        std::vector<std::string> fields = std::vector<std::string>(), uint32_t nPerCell = 1);
    
    template <typename CTYPE>
    void sortColumns(RasterBase<CTYPE> &rasterBase);

    // Comparison operators
    template <typename RTYPE, typename CTYPE>
    bool operator==(const Tile<RTYPE, CTYPE> &l, const Tile<RTYPE, CTYPE> &r);

    template <typename RTYPE, typename CTYPE>
    bool operator!=(const Tile<RTYPE, CTYPE> &l, const Tile<RTYPE, CTYPE> &r);

    template <typename RTYPE, typename CTYPE>
    bool operator==(const Raster<RTYPE, CTYPE> &l, const Raster<RTYPE, CTYPE> &r);
    
#ifdef USE_TILE_CACHING

    /**
    * %TileCacheManager class for %Tile data. 
    */
    template <typename RTYPE, typename CTYPE>
    class TileCacheManager {
    
    public:

        // Disable functions
        TileCacheManager(const TileCacheManager &) = delete;
        TileCacheManager(TileCacheManager &&) = delete;
        TileCacheManager& operator=(const TileCacheManager &) = delete;
        TileCacheManager& operator=(TileCacheManager &&) = delete;

        // Get singleton instance of %TileCacheManager.
        static TileCacheManager &instance();
        void update(Tile<RTYPE, CTYPE> *tile);
        void remove(Tile<RTYPE, CTYPE> *tile);
        const std::string &getCacheDirectory() { return cacheDirectory; }

    private:
        
        // Disable functions outside class
        TileCacheManager();
        ~TileCacheManager();
        
        // Maximum allocations for host and device
        uint64_t maxHostAllocationBytes;
        uint64_t maxDeviceAllocationBytes;
        std::atomic<std::uint64_t> cacheHostAllocationBytes;
        std::atomic<std::uint64_t> cacheDeviceAllocationBytes;

        // Cache variables
        std::list<Tile<RTYPE, CTYPE> *> cacheDeviceList;
        std::list<Tile<RTYPE, CTYPE> *> cacheHostList;
        std::unordered_map<Tile<RTYPE, CTYPE> *, typename std::list<Tile<RTYPE, CTYPE> *>::iterator> cacheDeviceMap;
        std::unordered_map<Tile<RTYPE, CTYPE> *, typename std::list<Tile<RTYPE, CTYPE> *>::iterator> cacheHostMap;

        // Temporary directory
        std::string cacheDirectory;
    };

#endif
    
    /**
    * %Tile class for one, two or three dimensional geospatial data.
    * This holds data for a square tile of a @Raster. 
    */
    template <typename RTYPE, typename CTYPE>
    class Tile : public GeometryBase<CTYPE> {

    public:

        // Constructor
        Tile();

        // Destructor
        ~Tile();
        
        // Disable Tile copy constructors.
        Tile(Tile const &) = delete;
        void operator=(Tile const &) = delete;

        friend bool operator==<RTYPE, CTYPE>(const Tile<RTYPE, CTYPE> &, const Tile<RTYPE, CTYPE> &);
        friend bool operator!=<RTYPE, CTYPE>(const Tile<RTYPE, CTYPE> &, const Tile<RTYPE, CTYPE> &);

        // Tile initialisation
        void createData();
        void clearData();
        void readData(Raster<RTYPE, CTYPE> &r, dataHandlerReadFunction<RTYPE, CTYPE> readDataHandler = nullptr);
        bool init(uint32_t ti_, uint32_t tj_, RasterDimensions<CTYPE> rdim_);

        // Tile deletion
        void deleteData(bool clearCacheFlag = false);

        // Get bounds and dimensions
        BoundingBox<CTYPE> getBounds() const override;

        /**
        * Get base dimensions.
        * @return base dimensions of %Tile.
        */
        Dimensions<CTYPE> getDimensions() { 
            return dim.d;
        }

        /**
        * Get data size dimensions.
        * @return data size in bytes of %Tile.
        */
        size_t getDataSize() const { 
            return dim.d.mx*dim.d.my*dim.d.nz;
        }

        /**
        * Get %Tile dimensions.
        * @return dimensions of %Tile.
        */
        TileDimensions<CTYPE> getTileDimensions() { 
            return dim;
        }
        
        /**
        * Get type of Geometry
        * @return Tile identifier.
        */
        bool isType(size_t typeMask) const override {
            return (bool)(typeMask & GeometryType::Tile);
        }

        // Buffer functions
        cl::Buffer &getDataBuffer();      ///< Get %Tile data buffer
        cl::Buffer &getReduceBuffer();    ///< Get %Tile reduction buffer
        cl::Buffer &getStatusBuffer();    ///< Get %Tile status buffer

        // Data handlers
        using tIterator = typename std::vector<RTYPE>::iterator;
        bool hasData();                                                       ///< Check %Tile data
        void setAllCellValues(RTYPE val);                                     ///< Set all tile values
        void getData(tIterator iTo, size_t start, size_t end);                ///< Copy %Tile data to vector
        void setData(tIterator iFrom, tIterator iFromEnd, size_t start);      ///< Copy vector to %Tile data vector
        void setIndex(uint32_t ti_, uint32_t tj_);                            ///< Set %Tile index
        inline RTYPE &operator()(uint32_t i, uint32_t j = 0, uint32_t k = 0); ///< Get %Tile value

        RTYPE max(); // Tile maximum value TODO remove
        RTYPE min(); // Tile minimum value TODO remove        

        // Reduction handlers
        RTYPE reduce(Reduction::Type type);        ///< %Tile reduction
        void resetReduction();                     ///< Clear reduction buffer

        // Status handlers
        cl_uint getStatus();                       ///< Get %Tile status
        void setStatus(cl_uint newStatus);         ///< Set %Tile status
        cl_uint getResetStatus(cl_uint newStatus); ///< %Tile status with reset
        
#ifdef USE_TILE_CACHING

        // Cache handler
        bool isCached();
        void writeToCache();
        void readFromCache();
        void clearDataBuffer();

#endif

        // Tile operations
        void mapVector(Vector<CTYPE> &v, std::size_t clHash, ProjectionParameters<double> rproj,
            size_t parameters = GeometryType::Point | GeometryType::LineString | GeometryType::Polygon,
            std::string widthPropertyName = std::string(), std::string levelPropertyName = std::string());

        void rasterise(Vector<CTYPE> &v, std::size_t clHash, std::vector<std::string> fields, ProjectionParameters<double> rproj,
            size_t parameters = GeometryType::Point | GeometryType::LineString | GeometryType::Polygon);

        void index(Vector<CTYPE> &v, std::vector<RasterIndex<CTYPE> > &rasterIndexes,
            std::size_t clHash, ProjectionParameters<double> rproj);

    private:
        
        // Tile data
        volatile bool dataInitialised; ///< Tile data initialised
        TileDimensions<CTYPE> dim;     ///< Tile dimensions
        std::vector<RTYPE> dataVec;    ///< Data vector
        std::vector<RTYPE> reduceVec;  ///< Reduction vector
        cl_uint status;                ///< Status bits

#ifdef USE_TILE_CACHING

        /**
        * %TileCache class for writing %Tile data to a temporary file on disk. 
        */
        class TileCache {

        public:
            
            TileCache();
            ~TileCache(); 

            void read(Tile<RTYPE, CTYPE> &tile);
            void write(Tile<RTYPE, CTYPE> &tile);
            bool getIsCached() { return isCached; }
            void setAsUncached() { isCached = false; }

        private:

            std::string cacheName;
            volatile bool isCached;
            bool hasCacheFile;
        };
        TileCache tileCache;
#endif

        // OpenCL data
        cl::Buffer dataBuffer;                          ///< Tile OpenCL data buffer
        mutable void *dataBufferMapPtr;                 ///< Holder for pointer returned from data buffer mapping
        mutable volatile bool dataBufferOnDevice;       ///< Flag indicating if data buffer is currently on device
        void mapDataBuffer(cl::CommandQueue &) const;   ///< Map OpenCL data buffer to host
        void unmapDataBuffer(cl::CommandQueue &);       ///< Unmap OpenCL data buffer from host
        void ensureDataBufferOnHost();                  ///< Ensure the tile data is on the host

        cl::Buffer reduceBuffer;                        ///< Tile OpenCL reduction buffer
        mutable void *reduceBufferMapPtr;               ///< Holder for pointer returned from reduction buffer mapping
        mutable volatile bool reduceBufferOnDevice;     ///< Flag indicating if reduction buffer is currently on device
        void mapReduceBuffer(cl::CommandQueue &);       ///< Map OpenCL reduction buffer to host
        void unmapReduceBuffer(cl::CommandQueue &);     ///< Unmap OpenCL reduction buffer from host
        void ensureReduceBufferOnHost();                ///< Ensure the tile reduction data is on the host

        cl::Buffer statusBuffer;                        ///< Tile OpenCL status buffer
        mutable void *statusBufferMapPtr;               ///< Holder for pointer returned from status buffer mapping
        mutable volatile bool statusBufferOnDevice;     ///< Flag indicating if status buffer is currently on device
        void mapStatusBuffer(cl::CommandQueue &) const; ///< Map OpenCL status buffer to host
        void unmapStatusBuffer(cl::CommandQueue &);     ///< Unmap OpenCL status buffer from host
        void ensureStatusBufferOnHost();                ///< Ensure the tile status data is on the host
    };

    /**
    * %RasterFileHandler class for %Raster file IO. 
    */
    template <typename RTYPE, typename CTYPE>
    class RasterFileHandler {

    public:
        
        // Constructor.
        RasterFileHandler();
        
        // Copy constructor.
        RasterFileHandler(const RasterFileHandler &); 
        
        // Disable assignment operator as reference cannot be initialised.
        void operator=(RasterFileHandler const &) = delete;
        
        /**
        * %RasterFileHandler destructor.
        */
        ~RasterFileHandler();

        virtual void read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig = "") = 0; ///< Open file for reading to %Raster
        virtual void write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig = "") = 0; ///< Write %Raster to file

        // Data handler callback function
        void registerReadDataHandler(dataHandlerReadFunction<RTYPE, CTYPE> dataHandler_);
        void registerWriteDataHandler(dataHandlerWriteFunction<RTYPE, CTYPE> dataHandler_);
        
        /**
        * Get input data handler function.
        */
        const dataHandlerReadFunction<RTYPE, CTYPE> &getReadDataHandler() const {
            return readDataHandler;
        }
        
        /**
        * Get output data handler function.
        */
        const dataHandlerWriteFunction<RTYPE, CTYPE> &getWriteDataHandler() const {
            return writeDataHandler;
        }

        std::shared_ptr<std::fstream> fileStream; ///< %Raster file handle

    private:

        // Callback function to read or write data
        dataHandlerReadFunction<RTYPE, CTYPE> readDataHandler;
        dataHandlerWriteFunction<RTYPE, CTYPE> writeDataHandler;

    };
    
    /**
    * %RasterBase class.
    * Base class for %Raster.
    **/
    template <typename CTYPE>
    class RasterBase : public PropertyMap {

    public:

        // Parent aliases
        using PropertyMap::properties;
    
        // Constructor
        RasterBase();

        // Copy constructor
        RasterBase(const RasterBase &rb);

        // Destructor
        ~RasterBase();
        
        /**
        * Get %RasterBase dimensions.
        * @return dimensions of %RasterBase.
        */
        RasterDimensions<CTYPE> getRasterDimensions() const { 
            return dim;
        }

        /**
        * Set %RasterBase %ProjectionParameters.
        */
        void setProjectionParameters(ProjectionParameters<double> proj_);

        /**
        * Get %RasterBase %ProjectionParameters.
        */
        ProjectionParameters<double> getProjectionParameters() {
            return proj;
        }

        /**
        * Set %RasterBase interpolation type.
        */
        void setInterpolationType(std::size_t interpolation_) {
            interpolation = interpolation_;
        }

        /**
        * Get %RasterBase interpolation type
        */
        std::size_t getInterpolationType() {
            return interpolation;
        }

        /**
        * Get bounds of %RasterBase.
        * @return bounds of %RasterBase.
        */
        BoundingBox<CTYPE> getBounds() {
            return BoundingBox<CTYPE>( { { dim.d.ox, dim.d.oy, dim.d.oz }, { dim.ex, dim.ey, dim.ez } } );
        }

        // Raster initialisation from dimensions
        virtual bool init(const Dimensions<CTYPE> &dim) = 0;

        // Get type strings
        virtual std::string getDataTypeString() = 0;
        virtual std::string getOpenCLTypeString() = 0;

        // Raster data check
        virtual bool hasData() = 0;

        // Raster data delete
        virtual void deleteRasterData() = 0;
        
        // Get tile data
        virtual cl::Buffer &getNullBuffer() = 0;
        virtual cl::Buffer &getRandomBuffer() = 0;
        virtual cl::Buffer &getTileDataBuffer(uint32_t ti, uint32_t tj) = 0;
        virtual cl::Buffer &getTileReduceBuffer(uint32_t ti, uint32_t tj) = 0;
        virtual void resetTileReduction(uint32_t ti, uint32_t tj) = 0;
        virtual cl::Buffer &getTileStatusBuffer(uint32_t ti, uint32_t tj) = 0;
        virtual cl_uint getTileStatus(uint32_t ti, uint32_t tj) = 0;
        virtual void setTileStatus(uint32_t ti, uint32_t tj, cl_uint newStatus) = 0;
        virtual TileDimensions<CTYPE> getTileDimensions(uint32_t ti, uint32_t tj) = 0;
        virtual BoundingBox<CTYPE> getTileBounds(uint32_t ti, uint32_t tj) = 0;
        virtual std::vector<RasterIndex<CTYPE> > getVectorTileIndexes(Vector<CTYPE> &v, 
            uint32_t ti, uint32_t tj, std::size_t clHash) = 0;

        // 2D raster resize
        virtual bool resize2D(uint32_t nx_, uint32_t ny_, uint32_t tox_, uint32_t toy_) = 0;
        
        // Buffer from region operation
        virtual bool createRegionBuffer(
            BoundingBox<CTYPE> bounds, 
            cl::Buffer &rasterRegionBuffer, 
            RasterDimensions<CTYPE> &rasterRegionDim) = 0;
        
        // Neighbour processing options
        virtual uint8_t getRequiredNeighbours() = 0;
        virtual void setRequiredNeighbours(uint8_t requiredNeighbours_) = 0;

        // Reduction processing options
        virtual Reduction::Type getReductionType() = 0;
        virtual void setReductionType(Reduction::Type reductionType_) = 0;

        // Status processing options
        virtual bool getNeedsStatus() = 0;
        virtual void setNeedsStatus(bool needsStatus_) = 0;
        
        // Read options
        virtual bool getNeedsRead() = 0;
        virtual void setNeedsRead(bool needsRead_) = 0;

        // Write options
        virtual bool getNeedsWrite() = 0;
        virtual void setNeedsWrite(bool needsWrite_) = 0;

        virtual void read(std::string fileName, std::string jsonConfig = "") = 0; //< Read file to %Raster
        virtual void write(std::string fileName, std::string jsonConfig = "") = 0; //< Write %Raster to file

        // Variable handling
        template <typename RTYPE>
        RTYPE getVariableData(std::string name); ///< Get variable data

        template <typename RTYPE>
        void setVariableData(std::string name, RTYPE value); ///< Set variable data
        
        const std::map<std::string, std::size_t> &getVariablesIndexes(); ///< Get indexes of variables

        cl::Buffer &getVariablesBuffer(); ///< Get buffer of variables

        std::string getVariablesType(); ///< Get type of variables
        
        /**
        * Check variables in %RasterBase.
        * @return variables of %RasterBase.
        */
        bool hasVariables() {
            return vars != nullptr && vars->hasData();
        }

        /**
        * Get %VariablesVector %Coordinate cache
        * @return %VariablesVector of %Coordinates.
        */
        VariablesVector<Coordinate<CTYPE> > &getCoordinateCache() {
            return coordsCache;
        }

        cl::Buffer &getCacheBuffer(std::size_t dataSize);

        // Get hash string
        std::string getHashString();

    protected:

        std::shared_ptr<Variables<CTYPE, std::string> > vars; ///% Variables data

        RTree<CTYPE> tree;                 ///< %RTree for %Tile geometry
        RasterDimensions<CTYPE> dim;       ///< %Raster dimensions
        ProjectionParameters<double> proj; ///< %Raster projection
        std::size_t interpolation;         ///< %Raster interpolation type
        std::string hashString;            ///< %String used for hashing Raster

        // Cache buffers
        VariablesVector<Coordinate<CTYPE> > coordsCache;

        // Data cache buffers
        std::map<std::size_t, cl::Buffer> dataCache;

        // Create hash
        void createHashString();
    };

    /**
    * %Raster class for one, two or three dimensional geospatial data.
    * This is the main class for gridded raster data. The raster holds a handle 
    * to a memory array as well as dimensions, cell sizes, an offset value from the 
    * origin (0, 0, 0) raster cell, projection information and a null value. 
    */
    template <typename RTYPE, typename CTYPE>
    class Raster : public RasterBase<CTYPE> {

    public:

        // Parent aliases
        using PropertyMap::properties;
        using RasterBase<CTYPE>::vars;
        using RasterBase<CTYPE>::tree;
        using RasterBase<CTYPE>::dim;
        using RasterBase<CTYPE>::proj;
        using RasterBase<CTYPE>::interpolation;
        using RasterBase<CTYPE>::hashString;

        // Constructors
        Raster();
        Raster(std::string name);

        // Destructor
        ~Raster();

        // Copy constructor
        Raster(const Raster &r);

        // Assignment operator
        Raster &operator=(const Raster &r);
        
        // Get type strings
        std::string getDataTypeString();
        std::string getOpenCLTypeString();
        
        // Raster initialisation
        bool init(
            uint32_t nx_, 
            uint32_t ny_, 
            uint32_t nz_, 
            CTYPE hx_, 
            CTYPE hy_, 
            CTYPE hz_, 
            CTYPE ox_ = 0.0, 
            CTYPE oy_ = 0.0,  
            CTYPE oz_ = 0.0);

        // Raster initialisation for 2D layer
        bool init2D(
            uint32_t nx_, 
            uint32_t ny_, 
            CTYPE hx_, 
            CTYPE hy_, 
            CTYPE ox_ = 0.0, 
            CTYPE oy_ = 0.0, 
            CTYPE oz_ = 0.0);

        // Raster initialisation from dimensions
        bool init(const Dimensions<CTYPE> &dim);

        // Raster initialisation from bounds
        bool init(const BoundingBox<CTYPE> bounds, CTYPE hx_, CTYPE hy_ = 0.0, CTYPE hz_ = 0.0);            
            
        // 2D raster resize
        bool resize2D(uint32_t nx_, uint32_t ny_, uint32_t tox_, uint32_t toy_);
        tileIndexSet resize2DIndexes(uint32_t nx_, uint32_t ny_, uint32_t tox_, uint32_t toy_);

        // Raster maximum reductions
        RTYPE max() const; // TODO remove
        RTYPE min() const; // TODO remove

        RTYPE reduce() const; // Raster reduction
        
        // Read and write operations
        RTYPE getCellValue(uint32_t i, uint32_t j = 0, uint32_t k = 0);
        RTYPE getNearestValue(CTYPE x, CTYPE y = 0.0, CTYPE z = 0.0);
        RTYPE getBilinearValue(CTYPE x, CTYPE y = 0.0, CTYPE z = 0.0);

        void setAllCellValues(RTYPE val);
        void setCellValue(RTYPE val, uint32_t i, uint32_t j = 0, uint32_t k = 0);
        
        TileDimensions<CTYPE> getTileDimensions(uint32_t ti, uint32_t tj);
        BoundingBox<CTYPE> getTileBounds(uint32_t ti, uint32_t tj);
        cl::Buffer &getTileDataBuffer(uint32_t ti, uint32_t tj);
        cl::Buffer &getTileReduceBuffer(uint32_t ti, uint32_t tj);
        RTYPE getTileReduction(uint32_t ti, uint32_t tj);
        void resetTileReduction(uint32_t ti, uint32_t tj);
        cl::Buffer &getTileStatusBuffer(uint32_t ti, uint32_t tj);
        void getTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE>& data);
        void getTileDataSlice(uint32_t ti, uint32_t tj, uint32_t k, std::vector<RTYPE>& data);
        void setTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE>& data);
        cl_uint getTileStatus(uint32_t ti, uint32_t tj);
        void setTileStatus(uint32_t ti, uint32_t tj, cl_uint newStatus);
        bool getResetTileStatus(uint32_t ti, uint32_t tj, cl_uint &status, cl_uint newStatus);
        bool setAllTileCellValues(uint32_t ti, uint32_t tj, RTYPE val);
        cl::Buffer &getNullBuffer();
        cl::Buffer &getRandomBuffer();

        /**
        * Check if %Raster has data.
        * @return true if Raster contains data.
        */
        bool hasData() { return !tiles.empty(); }

        // Delete tile data
        void deleteRasterData();

        // Raster region
        Raster<RTYPE, CTYPE> region(BoundingBox<CTYPE> bounds);

        // Map vector onto raster
        void mapVector(Vector<CTYPE> &v, 
            std::string script = std::string(), 
            size_t parameters = GeometryType::Point | GeometryType::LineString | GeometryType::Polygon,
            std::string widthPropertyName = std::string(), 
            std::string levelPropertyName = std::string());

        void mapVector(Vector<CTYPE> &v, 
            size_t parameters = GeometryType::Point | GeometryType::LineString | GeometryType::Polygon,
            std::string widthPropertyName = std::string(), 
            std::string levelPropertyName = std::string()) {
            mapVector(v, std::string(), parameters, widthPropertyName, levelPropertyName);
        }
            
        // Map vector onto raster tile
        void mapTileVector(uint32_t ti, uint32_t tj, Vector<CTYPE> &v, 
            std::string script = std::string(), 
            size_t parameters = GeometryType::Point | GeometryType::LineString | GeometryType::Polygon,
            std::string widthPropertyName = std::string(), 
            std::string levelPropertyName = std::string());

        void rasterise(Vector<CTYPE> &v, 
            std::string script = std::string(),
            size_t parameters = GeometryType::Point | GeometryType::LineString | GeometryType::Polygon);

        std::vector<RasterIndex<CTYPE> > getVectorTileIndexes(Vector<CTYPE> &v, 
            uint32_t ti, uint32_t tj, std::size_t clHash);

        // Vectorise raster
        Vector<CTYPE> cellCentres();
        Vector<CTYPE> vectorise(std::vector<RTYPE> countourValues, RTYPE noDataValue = getNullValue<RTYPE>());

        // Friend functions
        friend bool operator==<RTYPE, CTYPE>(const Raster<RTYPE, CTYPE> &, const Raster<RTYPE, CTYPE> &);

        void read(std::string fileName, std::string jsonConfig = ""); //< Read file to %Raster
        void write(std::string fileName, std::string jsonConfig = ""); //< Write %Raster to file

        /**
        * Directly set input file handler
        * @param fileHandler to set.
        */
        void setFileInputHandler(std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerIn_) {
            fileHandlerIn = fileHandlerIn_;
        }

        /**
        * Directly set output file handler
        * @param fileHandler to set.
        */
        void setFileOutputHandler(std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerOut_) {
            fileHandlerOut = fileHandlerOut_;
        }

        /**
        * Get whether %Raster needs continuous data at boundaries.
        * @return true if continuous data is needed, false otherwise.
        */
        uint8_t getRequiredNeighbours() { return requiredNeighbours; }

        /**
        * Set whether %Raster needs continuous data at boundaries.
        */
        void setRequiredNeighbours(uint8_t requiredNeighbours_) { requiredNeighbours = requiredNeighbours_; }

        /**
        * Get whether %Raster requires reductions.
        * @return true if reductions are needed, false otherwise.
        */
        Reduction::Type getReductionType() { return reductionType; }

        /**
        * Set whether %Raster requires reductions.
        */
        void setReductionType(Reduction::Type reductionType_) { reductionType = reductionType_; }

        /**
        * Get whether %Raster requires status.
        * @return true if status are needed, false otherwise.
        */
        bool getNeedsStatus() { return needsStatus; }

        /**
        * Set whether %Raster requires status.
        */
        void setNeedsStatus(bool needsStatus_) { needsStatus = needsStatus_; }
        
        /**
        * Get whether %Raster needs data initially read in kernel.
        * @return true if data is needed, false otherwise.
        */
        bool getNeedsRead() { return needsRead; }

        /**
        * Set whether %Raster requires data initially read in kernel.
        */
        void setNeedsRead(bool needsRead_) { needsRead = needsRead_; }

        /**
        * Get whether %Raster needs data written back in kernel.
        * @return true if write data is needed, false otherwise.
        */
        bool getNeedsWrite() { return needsWrite; }

        /**
        * Set whether %Raster requires write in kernel.
        */
        void setNeedsWrite(bool needsWrite_) { needsWrite = needsWrite_; }

    protected:

        // Tile data
        std::vector<TilePtr<RTYPE, CTYPE> > tiles; ///< %Tile array 
        uint8_t requiredNeighbours;                ///< Raster neighbours need to be taken into account during tile handling
        bool needsStatus;                          ///< Whether status bits need to be taken into account during tile handling
        bool needsRead;                            ///< Whether data needs to be initially read in kernel
        bool needsWrite;                           ///< Whether data needs to be written back to buffer in kernel
        std::shared_ptr<cl::Buffer> nullBuffer;    ///< Special buffer containing null values for boundaries
        std::shared_ptr<cl::Buffer> randomBuffer;  ///< Special buffer containing random number generator values
        Reduction::Type reductionType;             ///< Type of reduction required for %Raster

        
        // Tile access
        Tile<RTYPE, CTYPE> &getTile(uint32_t ti, uint32_t tj);

        // Raster file handlers
        std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerIn;
        std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerOut;

        // Buffer from region operation
        bool createRegionBuffer(BoundingBox<CTYPE> bounds, cl::Buffer &rasterRegionBuffer, RasterDimensions<CTYPE> &rasterRegionDim);
    };
}

#endif
