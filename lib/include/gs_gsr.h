/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_GSR_H
#define GEOSTACK_GSR_H

#include <string>

#include "gs_raster.h"

namespace Geostack
{
    template <typename RTYPE, typename CTYPE>
    class GsrHandler : public RasterFileHandler<RTYPE, CTYPE> {

    public:

        /**
        * %GsrHandler constructor.
        */
        GsrHandler(): RasterFileHandler<RTYPE, CTYPE>() { }
        
        void read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig); ///< Open file for reading to %Raster
        void write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig); ///< Write %Raster to file

    private:
    
        bool readData(uint32_t ti, uint32_t tj, std::vector<RTYPE> &v);                                     ///< Read data from gsr to tile
        void readDataFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r);  ///< Read %Raster tile
        bool writeData(uint32_t ti, uint32_t tj, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r);           ///< Write data to gsr from tile
        void writeDataFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r); ///< Write %Raster tile

        std::map<std::pair<uint32_t, uint32_t>, uint64_t> tileRecordPos; ///< Position of records in output data

        std::map<std::pair<uint32_t, uint32_t>, std::pair<uint64_t, uint32_t> > tileRecordTable; ///< Table of offsets from index pairs
    };
}

#endif
