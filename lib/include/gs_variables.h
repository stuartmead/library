/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_VARIABLES_H
#define GEOSTACK_VARIABLES_H

#include <memory>
#include <vector>
#include <map>

#include "gs_opencl.h"
#include "gs_solver.h"

namespace Geostack
{
    // Forward declarations
    template <typename KTYPE>
    class VariablesBase;

    // Definitions
    template <typename KTYPE>
    using VariablesBasePtr = std::shared_ptr<VariablesBase<KTYPE> >;

    /**
    * %VariablesBase class.
    * Base class for %Variables.
    **/
    template <typename KTYPE>
    class VariablesBase {

    public:

        // Data handlers
        virtual cl::Buffer &getBuffer() = 0;
        virtual const std::map<KTYPE, std::size_t> &getIndexes() = 0; 
        
        // Data check
        virtual bool hasData() = 0;

        // Get string for OpenCL scripts
        virtual std::string getOpenCLTypeString() = 0;

    protected:

        // Internal buffer
        cl::Buffer buffer;
    };

    /**
    * %Variables class for one dimensional named variables.
    */
    template <typename RTYPE, typename KTYPE>
    class Variables : public VariablesBase<KTYPE> {
    
    public:

        // Parent aliases
        using VariablesBase<KTYPE>::buffer;

        // Constructor
        Variables();

        // Destructor
        ~Variables();
        
        // Disable copy constructors
        Variables(Variables const &) = delete;
        void operator=(Variables const &) = delete;

        // Set and get operations
        void set(KTYPE key, RTYPE value);
        RTYPE get(KTYPE key);
        const std::map<KTYPE, std::size_t> &getIndexes();        
        
        // Data handlers
        cl::Buffer &getBuffer();

        // Get string for OpenCL scripts
        std::string getOpenCLTypeString() {
            return Solver::getTypeString<RTYPE>();
        }

        /**
        * Check %Variables data.
        * @return true if map contains data, false otherwise
        */
        bool hasData() { return !dataMap.empty(); }

    private:    
    
        // Internal variables
        std::map<KTYPE, RTYPE> dataMap;           ///< Data map
        std::map<KTYPE, std::size_t> dataIndexes; ///< Indexes of data in vector
        std::vector<RTYPE> dataVec;               ///< Data vector
        void *bufferMapPtr;                       ///< Holder for pointer returned from variable buffer mapping
        volatile bool bufferOnDevice;             ///< Flag indicating if variable buffer is currently on device
        
        // Internal functions
        void refreshData();                       ///< Copy data from map to vector
        void mapBuffer(cl::CommandQueue &);       ///< Map buffer to host
        void unmapBuffer(cl::CommandQueue &);     ///< Unmap buffer from host
        void ensureDataOnHost();                  ///< Ensure the %Variables data is on the host
    };

    /**
    * %VariablesVector class for data arrays.
    */
    template <typename RTYPE>
    class VariablesVector {    
    public:

        // Constructors
        VariablesVector();
        VariablesVector(VariablesVector const &r);
        VariablesVector &operator=(VariablesVector const &r);

        /**
        * Get data from %VariablesVector.
        */
        std::vector<RTYPE> &getData() {
            ensureBufferOnHost();
            return data;
        }

        /**
        * Get %Coordinate buffer from %Vector.
        */
        const cl::Buffer &getBuffer();

        /**
        * Clear %VariablesVector data.
        */
        void clear() {
            ensureBufferOnHost();
            data.clear();
        }
        
        /**
        * Get size of data vector.
        * @return size of data vector.
        */
        std::size_t size() const {
            return data.size();
        }

        // Clone %VariablesVector
        VariablesVector<RTYPE> *clone();

    private:

        // Data vector
        std::vector<RTYPE> data;

        // OpenCL handlers
        cl::Buffer buffer;
        mutable volatile bool bufferOnDevice;          ///< Flag indicating if data buffer is currently on device
        mutable volatile std::size_t bufferSize;       ///< Current buffer size
        void mapBuffer(cl::CommandQueue &queue) const; ///< Map buffer to host
        void unmapBuffer(cl::CommandQueue &queue);     ///< Unmap buffer from host
        void ensureBufferOnHost() const;               ///< Ensure data is on the host
    };
}

#endif
