/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

 #include <string>

// Link to external resources
extern const char R_cl_raster_head_c[];
extern const uint32_t R_cl_raster_head_c_size;

extern const char R_cl_raster_area_op2D_c[];
extern const uint32_t R_cl_raster_area_op2D_c_size;

extern const char R_cl_raster_block_c[];
extern const uint32_t R_cl_raster_block_c_size;

extern const char R_cl_map_distance_c[];
extern const uint32_t R_cl_map_distance_c_size;

extern const char R_cl_rasterise_c[];
extern const uint32_t R_cl_rasterise_c_size;

extern const char R_cl_random_c[];
extern const uint32_t R_cl_random_c_size;

extern const char R_cl_vectorise_c[];
extern const uint32_t R_cl_vectorise_c_size;

extern const char R_cl_projection_head_c[];
extern const uint32_t R_cl_projection_head_c_size;

extern const char R_cl_projection_c[];
extern const uint32_t R_cl_projection_c_size;

extern const char R_cl_index_c[];
extern const uint32_t R_cl_index_c_size;

extern const char R_cl_raster_indexed_block_c[];
extern const uint32_t R_cl_raster_indexed_block_c_size;

extern const char R_cl_sort_c[];
extern const uint32_t R_cl_sort_c_size;

extern const char R_cl_stipple_c[];
extern const uint32_t R_cl_stipple_c_size;

extern const char R_cl_vector_block_c[];
extern const uint32_t R_cl_vector_block_c_size;

namespace Geostack
{
    /**
    * %Models class for pre-defined scripts.
    */
    class Models {

    public:
        static const std::string cl_raster_head_c;
        static const std::string cl_raster_area_op2D_c;
        static const std::string cl_raster_block_c;
        static const std::string cl_map_distance_c;
        static const std::string cl_rasterise_c;
        static const std::string cl_random_c;
        static const std::string cl_vectorise_c;
        static const std::string cl_projection_c;
        static const std::string cl_projection_head_c;
        static const std::string cl_index_c;
        static const std::string cl_raster_indexed_block_c;
        static const std::string cl_sort_c;
        static const std::string cl_stipple_c;
        static const std::string cl_vector_block_c;
    };

}