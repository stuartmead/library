/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_SHAPEFILE_H
#define GEOSTACK_SHAPEFILE_H

#include <string>

#include "gs_geometry.h"
#include "gs_vector.h"

namespace Geostack
{
    // Forward declarations
    template <typename T>
    class Vector;

    template <typename T>
    class ShapeFile {

    public:

        // Reader
        static Geostack::Vector<T> shapefileToVector(std::string shapefileName);

        // Writer
        static bool vectorToShapefile(const Geostack::Vector<T> &v, std::string shapefileName, GeometryType::Type writeType);

    private:

        // Set projection
        //static void setProjection(Vector<T> &v);

    };
}

#endif
