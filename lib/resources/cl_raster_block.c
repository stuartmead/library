/**
* General 3D raster operator
* Calculations are run over _dim dimensions
*/
__kernel void raster(
/*__ARGS__*/
) {

    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);
    
    size_t _lid = get_local_id(0)+get_local_id(1)*get_local_size(0);
 
/*__CACHE__
__CACHE_HEAD__
if (_lid == 0) {
__CACHE_VARS__
}
barrier(CLK_LOCAL_MEM_FENCE);
__CACHE__*/

/*__REDUCE__
// Initialise reduction
size_t _lsize = get_local_size(0)*get_local_size(1);
__REDUCE_HEAD__
barrier(CLK_LOCAL_MEM_FENCE);
__REDUCE__*/

    // Check limits
    if (_i < _dim.nx && _j < _dim.ny && _k < _dim.nz) {

        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------

/*__POST__*/

    }

/*__REDUCE__
// Calculate reduction
barrier(CLK_LOCAL_MEM_FENCE);
for (size_t _step = _lsize>>1; _step >= 1; _step>>=1) {
    if (_lid < _step) {
        __REDUCE_CODE__
    }
    barrier(CLK_LOCAL_MEM_FENCE);
}
if (_lid == 0) {
    size_t gid = get_group_id(0)+get_num_groups(0)*(get_group_id(1)+get_num_groups(1)*get_group_id(2));
    __REDUCE_POST__
}
__REDUCE__*/

}

