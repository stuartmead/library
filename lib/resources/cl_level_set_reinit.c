/**
* Level set reinitialisation
*/
__kernel void reinitWeighted(
const parameters _p,
const REAL _CFL,
/*__ARGS__*/
) {

    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {

/*__VARS__*/

        // Calculate timestep
        REAL dtau = 0.25*_CFL*(_dim.hx+_dim.hy);

        // Take re-initialisation step
        _distance_update = _distance;
        if (fabs(_distance) > 0.5*_p.band_width) {

            // Reinitialise with pseudo time-step
            REAL _distance_abs = fabs(_distance);
            REAL _distance_s = sign(_distance);
            REAL dx = fmax(fmax(_distance_abs-_distance_s*_distance_W, _distance_abs-_distance_s*_distance_E)/_dim.hx, (REAL)0.0);
            REAL dy = fmax(fmax(_distance_abs-_distance_s*_distance_S, _distance_abs-_distance_s*_distance_N)/_dim.hy, (REAL)0.0);
            _distance_update -= _distance_s*dtau*(sqrt(dx*dx+dy*dy)-(REAL)1.0);

        } else {

            // Decrease re-initialisation amount linearly to zero under 10% of the averaged normalised speed
            REAL speed_weight = 1.0;
            REAL speed_normalised_average = _speed+_speed_N+_speed_S+_speed_W+_speed_E;
            speed_normalised_average/=5.0*(_p.max_speed+1.0E-6);
            if (speed_normalised_average < 0.1)
                speed_weight = 10.0*speed_normalised_average;
            
            REAL distance_N2 = _j < _dim.ny-2 ? _val2D_a(__distance, _i, _j+2) : _val2D_a(__distance_N, _i, _j+2-_dim.ny);
            REAL distance_S2 = _j >= 2 ? _val2D_a(__distance, _i, _j-2) : _val2D_a(__distance_S, _i, _j+_dim.ny-2);
            REAL distance_E2 = _i < _dim.nx-2 ? _val2D_a(__distance, _i+2, _j) : _val2D_a(__distance_E, _i+2-_dim.nx, _j);
            REAL distance_W2 = _i >= 2 ? _val2D_a(__distance, _i-2, _j) : _val2D_a(__distance_W, _i+_dim.nx-2, _j);

            // Second order method, see: 'Level Set Methods and Fast Marching Methods', Sethian, p. 66
            REAL dcx = _distance_E-2.0*_distance+_distance_W;
            REAL dcy = _distance_N-2.0*_distance+_distance_S;

            REAL A = _distance-_distance_W;
            REAL dmmx = _distance-2.0*_distance_W+distance_W2;
            if (dmmx*dcx >= 0.0) A += 0.5*(fabs(dmmx) > fabs(dcx) ? dcx : dmmx);

            REAL B = _distance_E-_distance;
            REAL dppx = _distance-2.0*_distance_E+distance_E2;
            if (dppx*dcx >= 0.0) B -= 0.5*(fabs(dppx) > fabs(dcx) ? dcx : dppx);

            REAL C = _distance-_distance_S;
            REAL dmmy = _distance-2.0*_distance_S+distance_S2;
            if (dmmy*dcy >= 0.0) C += 0.5*(fabs(dmmy) > fabs(dcy) ? dcy : dmmy);

            REAL D = _distance_N-_distance;
            REAL dppy = _distance-2.0*_distance_N+distance_N2;
            if (dppy*dcy >= 0.0) D -= 0.5*(fabs(dppy) > fabs(dcy) ? dcy : dppy);

            if (_distance > 0.0) {

                // Positive speed
                REAL max_x = A > 0.0 ? A: 0.0;
                REAL min_x = B < 0.0 ? B: 0.0;
                REAL max_y = C > 0.0 ? C: 0.0;
                REAL min_y = D < 0.0 ? D: 0.0;
                _distance_update -= speed_weight*dtau*(sqrt(
                    ((max_x*max_x+min_x*min_x)/(_dim.hx*_dim.hx))+
                    ((max_y*max_y+min_y*min_y)/(_dim.hy*_dim.hy)))-(REAL)1.0);

            } else if (_distance < 0.0) {

                // Negative speed
                REAL max_x = B > 0.0 ? B: 0.0;
                REAL min_x = A < 0.0 ? A: 0.0;
                REAL max_y = D > 0.0 ? D: 0.0;
                REAL min_y = C < 0.0 ? C: 0.0;
                _distance_update += speed_weight*dtau*(sqrt(
                    ((max_x*max_x+min_x*min_x)/(_dim.hx*_dim.hx))+
                    ((max_y*max_y+min_y*min_y)/(_dim.hy*_dim.hy)))-(REAL)1.0);
            }
        }
    
        // Ensure positive distances remain positive
        if (_distance > 0.0)
            _distance_update = fmax(_distance_update, (REAL)0.0);

/*__POST__*/

    }

}

