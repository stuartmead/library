/**
* Multigrid relaxation
*/
__kernel void relax(
const uint _tileBoundary,
const REAL _ihx2,
const REAL _ihy2,
__global REAL *_b,
__global REAL *_l0,
__global REAL *_l0_N,
__global REAL *_l0_E,
__global REAL *_l0_S,
__global REAL *_l0_W,
__global REAL *_l1
) {
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _ii = get_local_id(0);
    const size_t _jj = get_local_id(1);

    // Set boundary flags
    const bool _boundary_W = (_i == 0);
    const bool _boundary_S = (_j == 0); 
    const bool _boundary_E = (_i == /*__MSIZE__*/-1); 
    const bool _boundary_N = (_j == /*__MSIZE__*/-1);
    
    // Populate cache
    __local REAL cache[18][18];
    cache[_ii+1][_jj+1] = _val2D_a(_l0, _i, _j);
    if (_ii == 0) {
        cache[0][_jj+1] = _val2D_a_W(_l0, _i, _j);
    } else if (_ii == get_local_size(0)-1) {
        cache[_ii+2][_jj+1] = _val2D_a_E(_l0, _i, _j);
    }
    if (_jj == 0) {
        cache[_ii+1][0] = _val2D_a_S(_l0, _i, _j);
    } else if (_jj == get_local_size(1)-1) {
        cache[_ii+1][_jj+2] = _val2D_a_N(_l0, _i, _j);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    
    // Relax using weighted Jacobi with damping parameter 2/3
    REAL b = _val2D_a(_b, _i, _j);
    REAL w = 2.0/3.0;
    REAL l_diag = 2.0*(_ihx2+_ihy2);
    REAL l1 = (1.0-w)*cache[_ii+1][_jj+1]+w*((
        _ihx2*(cache[_ii+2][_jj+1]+cache[_ii  ][_jj+1])+
        _ihy2*(cache[_ii+1][_jj+2]+cache[_ii+1][_jj  ])-b)/l_diag);
    
    // Enforce Dirichlet condition on boundaries
    if (_tileBoundary & ((uint)_boundary_W | ((uint)_boundary_S << 1))) {
        l1 = 0.0;
    }

    // Write smoothed value
    _val2D_a(_l1, _i, _j) = l1;
}

/**
* Multigrid residual
*/
__kernel void residual(
const uint _tileBoundary,
const REAL _ihx2,
const REAL _ihy2,
__global REAL *_b,
__global REAL *_l0,
__global REAL *_l0_N,
__global REAL *_l0_E,
__global REAL *_l0_S,
__global REAL *_l0_W,
__global REAL *_l1
) {
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _ii = get_local_id(0);
    const size_t _jj = get_local_id(1);
    
    // Set boundary flags
    const bool _boundary_W = (_i == 0);
    const bool _boundary_S = (_j == 0); 
    const bool _boundary_E = (_i == /*__MSIZE__*/-1); 
    const bool _boundary_N = (_j == /*__MSIZE__*/-1);
    
    // Populate cache
    __local REAL cache[18][18];
    cache[_ii+1][_jj+1] = _val2D_a(_l0, _i, _j);
    if (_ii == 0) {
        cache[0][_jj+1] = _val2D_a_W(_l0, _i, _j);
    } else if (_ii == get_local_size(0)-1) {
        cache[_ii+2][_jj+1] = _val2D_a_E(_l0, _i, _j);
    }
    if (_jj == 0) {
        cache[_ii+1][0] = _val2D_a_S(_l0, _i, _j);
    } else if (_jj == get_local_size(1)-1) {
        cache[_ii+1][_jj+2] = _val2D_a_N(_l0, _i, _j);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    
    // Calculate residual
    REAL b = _val2D_a(_b, _i, _j);
    REAL l_diag = -2.0*(_ihx2+_ihy2)*cache[_ii+1][_jj+1];
    REAL l_val = 
        _ihx2*(cache[_ii+2][_jj+1]+cache[_ii  ][_jj+1])+
        _ihy2*(cache[_ii+1][_jj+2]+cache[_ii+1][_jj  ]);
    REAL l1 = b-(l_diag+l_val);
    
    // Enforce Dirichlet condition on boundaries
    if (_tileBoundary & ((uint)_boundary_W | ((uint)_boundary_S << 1))) {
        l1 = 0.0;
    }
    
    // Write restricted value
    _val2D_a(_l1, _i, _j) = l1;
}

