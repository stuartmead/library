/**
* Area based 2D raster operation
*/
__kernel void rasterArea2D(
const int width,
/*__ARGS__*/
) {

int _i = get_global_id(0);
int _j = get_global_id(1);

// Check limits
if (_i >= _dim.nx || _j >= _dim.ny) {
    return;
}

// Create output variables
REAL output = 0.0;
REAL sum = 0.0;
REAL __VARIABLE__ = noData_REAL;

// Run kernel over area
// Location bits are:
//
//         h
//   x x x 1
//   x x x 0
//   x x x 2
// v 1 0 2  
// 
// Stored as xyvvhh , where x and y are bits to indicate out-of-range in the x and y directions

for (int _jj = -width; _jj <= width; _jj++) {
    for (int _ii = -width; _ii <= width; _ii++) {

        REAL dist_sqr = _ii*_ii+_jj*_jj;

        __VARIABLE__ = _getValue2D(
            ___VARIABLE__, 
            ___VARIABLE___N, 
            ___VARIABLE___E, 
            ___VARIABLE___S, 
            ___VARIABLE___W, 
            ___VARIABLE___NE, 
            ___VARIABLE___SE, 
            ___VARIABLE___SW, 
            ___VARIABLE___NW, 
            _dim, _i, _j, _ii, _jj);

// ---------------------------------
// User defined code
/*__CODE__*/
// ---------------------------------

    }
}

// Average
if (sum != 0.0) {
    output/=sum;
}

// Set output
_val2D_a(_output, _i, _j) = output;

}

