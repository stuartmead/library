/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#define USING_RASTER

// Unaligned access
#define _val2D(R, i, j) (*((R)+(i)+(j)*_dim##R.mx))
#define _val3D(R, i, j, k) (*((R)+(i)+((j)+(k)*_dim##R.my)*_dim##R.mx))

// Aligned access
#define _val2D_a(R, i, j) (*((R)+(i)+(j)*_dim.mx))
#define _val3D_a(R, i, j, k) (*((R)+(i)+((j)+(k)*_dim.my)*_dim.mx))

// Neighbour access
#define _val2D_a_N(R, i, j) (_boundary_N ? _val2D_a(R##_N, i, 0) : _val2D_a(R, i, j+1))
#define _val2D_a_E(R, i, j) (_boundary_E ? _val2D_a(R##_E, 0, j) : _val2D_a(R, i+1, j))
#define _val2D_a_S(R, i, j) (_boundary_S ? _val2D_a(R##_S, i, _dim.my-1) : _val2D_a(R, i, j-1))
#define _val2D_a_W(R, i, j) (_boundary_W ? _val2D_a(R##_W, _dim.mx-1, j) : _val2D_a(R, i-1, j))

#define _val3D_a_N(R, i, j, k) (_boundary_N ? _val3D_a(R##_N, i, 0, k) : _val3D_a(R, i, j+1, k))
#define _val3D_a_E(R, i, j, k) (_boundary_E ? _val3D_a(R##_E, 0, j, k) : _val3D_a(R, i+1, j, k))
#define _val3D_a_S(R, i, j, k) (_boundary_S ? _val3D_a(R##_S, i, _dim.my-1, k) : _val3D_a(R, i, j-1, k))
#define _val3D_a_W(R, i, j, k) (_boundary_W ? _val3D_a(R##_W, _dim.mx-1, j, k) : _val3D_a(R, i-1, j, k))

#define _val2D_a_NE(R, i, j) ((_boundary_N && _boundary_E) ? _val2D_a(R##_NE, 0, 0) : \
    (_boundary_E ? _val2D_a(R##_E,  0, j+1) : (_boundary_N ? _val2D_a(R##_N, i+1, 0) : _val2D_a(R, i+1, j+1))))
#define _val2D_a_SE(R, i, j) ((_boundary_S && _boundary_E) ? _val2D_a(R##_SE, 0, _dim.my-1) : \
    (_boundary_E ? _val2D_a(R##_E,  0, j-1) : (_boundary_S ? _val2D_a(R##_S, i+1, _dim.my-1) : _val2D_a(R, i+1, j-1))))
#define _val2D_a_SW(R, i, j) ((_boundary_S && _boundary_W) ? _val2D_a(R##_SW, _dim.mx-1, _dim.my-1) : \
    (_boundary_W ? _val2D_a(R##_W, _dim.mx-1, j-1) : (_boundary_S ? _val2D_a(R##_S, i-1, _dim.my-1) : _val2D_a(R, i-1, j-1))))
#define _val2D_a_NW(R, i, j) ((_boundary_N && _boundary_W) ? _val2D_a(R##_NW, _dim.mx-1, 0) : \
    (_boundary_W ? _val2D_a(R##_W, _dim.mx-1, j+1) : (_boundary_N ? _val2D_a(R##_N, i-1, 0) : _val2D_a(R, i-1, j+1))))

#define _val3D_a_NE(R, i, j, k) ((_boundary_N && _boundary_E) ? _val3D_a(R##_NE, 0, 0, k) : \
    (_boundary_E ? _val3D_a(R##_E,  0, j+1, k) : (_boundary_N ? _val3D_a(R##_N, i+1, 0, k) : _val3D_a(R, i+1, j+1, k))))
#define _val3D_a_SE(R, i, j, k) ((_boundary_S && _boundary_E) ? _val3D_a(R##_SE, 0, _dim.my-1, k) : \
    (_boundary_E ? _val3D_a(R##_E,  0, j-1, k) : (_boundary_S ? _val3D_a(R##_S, i+1, _dim.my-1, k) : _val3D_a(R, i+1, j-1, k))))
#define _val3D_a_SW(R, i, j, k) ((_boundary_S && _boundary_W) ? _val3D_a(R##_SW, _dim.mx-1, _dim.my-1, k) : \
    (_boundary_W ? _val3D_a(R##_W, _dim.mx-1, j-1, k) : (_boundary_S ? _val3D_a(R##_S, i-1, _dim.my-1, k) : _val3D_a(R, i-1, j-1, k))))
#define _val3D_a_NW(R, i, j, k) ((_boundary_N && _boundary_W) ? _val3D_a(R##_NW, _dim.mx-1, 0, k) : \
    (_boundary_W ? _val3D_a(R##_W, _dim.mx-1, j+1, k) : (_boundary_N ? _val3D_a(R##_N, i-1, 0, k) : _val3D_a(R, i-1, j+1, k))))

// Gradient operators
#define _grad(R) getGradient2DREAL(_##R, x, y, _dim_##R)
#define _grad_a(R) (REALVEC2)(((R##_NE+2.0*R##_E+R##_SE)-(R##_NW+2.0*R##_W+R##_SW))/(8.0*_dim.hx), ((R##_SW+2.0*R##_S+R##_SE)-(R##_NW+2.0*R##_N+R##_NE))/(8.0*_dim.hy))

typedef struct DimensionsStruct {

    uint nx; ///< Number of cells in x dimension
    uint ny; ///< Number of cells in y dimension
    uint nz; ///< Number of cells in z dimension
    REAL hx; ///< Spacing in x dimension
    REAL hy; ///< Spacing in y dimension
    REAL hz; ///< Spacing in z dimension
    REAL ox; ///< Start coordinate in x dimension
    REAL oy; ///< Start coordinate in y dimension
    REAL oz; ///< Start coordinate in z dimension
    uint mx; ///< Number of cells stored in memory for x dimension
    uint my; ///< Number of cells stored in memory for y dimension

} __attribute__ ((aligned (8))) Dimensions;

REAL getZValueREAL(
    const __global REAL *_u, 
    const size_t _i, 
    const size_t _j, 
    const size_t _k,
    const size_t _mx,
    const size_t _my,
    const size_t _nz) {
    return *(_u+_i+(_j+min_uint_DEF(_k, _nz-1)*_my)*_mx);
}

REAL getZValueUINT(
    const __global UINT *_u, 
    const size_t _i, 
    const size_t _j, 
    const size_t _k,
    const size_t _mx,
    const size_t _my,
    const size_t _nz) {
    return *(_u+_i+(_j+min_uint_DEF(_k, _nz-1)*_my)*_mx);
}

REAL getNearestValue2DREAL(
    const  __global REAL *_u, 
    const REAL x, 
    const REAL y,
    const Dimensions _dim_u) {

    // Check for valid raster dimensions
    if (_dim_u.hx <= 0.0 || _dim_u.hy <= 0.0)
        return noData_REAL;

    // Convert to raster coordinates
    REAL rx = (x-_dim_u.ox)/_dim_u.hx;
    REAL ry = (y-_dim_u.oy)/_dim_u.hy;
        
    // Check bounds
    if (rx < 0.0 || ry < 0.0 || 
        rx >= (REAL)_dim_u.nx || ry >= (REAL)_dim_u.ny) {
        return noData_REAL;
    } else {
        
        // Convert to cell coordinates
        uint i = (uint)rx;
        uint j = (uint)ry;

        return _val2D(_u, i, j);
    }
}

UINT getNearestValue2DUINT(
    const  __global UINT *_u, 
    const REAL x, 
    const REAL y,
    const Dimensions _dim_u) {

    // Check for valid raster dimensions
    if (_dim_u.hx <= 0.0 || _dim_u.hy <= 0.0)
        return noData_UINT;

    // Convert to raster coordinates
    REAL rx = (x-_dim_u.ox)/_dim_u.hx;
    REAL ry = (y-_dim_u.oy)/_dim_u.hy;
        
    // Check bounds
    if (rx < 0.0 || ry < 0.0 || 
        rx >= (REAL)_dim_u.nx || ry >= (REAL)_dim_u.ny) {
        return noData_UINT;
    } else {
        
        // Convert to cell coordinates
        uint i = (uint)rx;
        uint j = (uint)ry;

        return _val2D(_u, i, j);
    }
}

REAL getBilinearValue2DREAL(
    const  __global REAL *_u, 
    const REAL x, 
    const REAL y,
    const Dimensions _dim_u) {

    // Check for valid raster dimensions
    if (_dim_u.hx <= 0.0 || _dim_u.hy <= 0.0)
        return noData_REAL;
    
    // Convert to raster coordinates
    REAL rx = (x-_dim_u.ox)/_dim_u.hx;
    REAL ry = (y-_dim_u.oy)/_dim_u.hy;
        
    // Check bounds
    if (rx < 0.0 || ry < 0.0 || 
        rx > (REAL)_dim_u.nx || ry > (REAL)_dim_u.ny) {
        return noData_REAL;
    } else {        
        
        // Offset to cell centre for interpolation
        rx-=0.5;
        ry-=0.5;
            
        // Offset within grid cell
        REAL a = 0.0;
        REAL b = 0.0;
            
        uint i, ip;
        if (rx < 0.0) {
            i = 0;
            ip = 0;
            a = 0.0;
        } else if (rx < (REAL)(_dim_u.nx-1)) {
            i = (uint)rx;
            ip = i+1;
            a = rx-(REAL)i;
        } else {
            i = _dim_u.nx-1;
            ip = _dim_u.nx-1;
            a = 0.0;
        }
            
        uint j, jp;
        if (ry < 0.0) {
            j = 0;
            jp = 0;
            b = 0.0;
        } else if (ry < (REAL)(_dim_u.ny-1)) {
            j = (uint)ry;
            jp = j+1;
            b = ry-(REAL)j;
        } else {
            j = _dim_u.ny-1;
            jp = _dim_u.ny-1;
            b = 0.0;
        }

        REAL u00 = _val2D(_u, i , j );
        REAL u10 = _val2D(_u, ip, j );
        REAL u01 = _val2D(_u, i , jp);
        REAL u11 = _val2D(_u, ip, jp);
            
        // Calculate interpolated value
        REAL v;
        v=  (REAL)(u11*     a *     b );
        v+= (REAL)(u01*(1.0-a)*     b );
        v+= (REAL)(u10*     a *(1.0-b));
        v+= (REAL)(u00*(1.0-a)*(1.0-b));

        return v;
    }
}

REAL getNearestValue3DREAL(
    const  __global REAL *_u, 
    const REAL x, 
    const REAL y,
    const REAL z,
    const Dimensions _dim_u) {

    // Check for valid raster dimensions
    if (_dim_u.hx <= 0.0 || _dim_u.hy <= 0.0 || _dim_u.hz <= 0.0)
        return noData_REAL;

    // Convert to raster coordinates
    REAL rx = (x-_dim_u.ox)/_dim_u.hx;
    REAL ry = (y-_dim_u.oy)/_dim_u.hy;
    REAL rz = (z-_dim_u.oz)/_dim_u.hz;
        
    // Check bounds
    if (rx < 0.0 || ry < 0.0 || rz < 0.0 || 
        rx >= (REAL)_dim_u.nx || ry >= (REAL)_dim_u.ny || rz >= (REAL)_dim_u.nz) {
        return noData_REAL;
    } else {
        
        // Convert to cell coordinates
        uint i = (uint)rx;
        uint j = (uint)ry;
        uint k = (uint)rz;

        return _val3D(_u, i, j, k);
    }
}

UINT getNearestValue3DUINT(
    const  __global UINT *_u, 
    const REAL x, 
    const REAL y,
    const REAL z,
    const Dimensions _dim_u) {

    // Check for valid raster dimensions
    if (_dim_u.hx <= 0.0 || _dim_u.hy <= 0.0 || _dim_u.hz <= 0.0)
        return noData_UINT;

    // Convert to raster coordinates
    REAL rx = (x-_dim_u.ox)/_dim_u.hx;
    REAL ry = (y-_dim_u.oy)/_dim_u.hy;
    REAL rz = (z-_dim_u.oz)/_dim_u.hz;
        
    // Check bounds
    if (rx < 0.0 || ry < 0.0 || rz < 0.0 || 
        rx >= (REAL)_dim_u.nx || ry >= (REAL)_dim_u.ny || rz >= (REAL)_dim_u.nz) {
        return noData_UINT;
    } else {
        
        // Convert to cell coordinates
        uint i = (uint)rx;
        uint j = (uint)ry;
        uint k = (uint)rz;

        return _val3D(_u, i, j, k);
    }
}

REAL getBilinearValue3DREAL(
    const  __global REAL *_u, 
    const REAL x, 
    const REAL y,
    const REAL z,
    const Dimensions _dim_u) {

    // Check for valid raster dimensions
    if (_dim_u.hx <= 0.0 || _dim_u.hy <= 0.0 || _dim_u.hz <= 0.0)
        return noData_REAL;
    
    // Convert to raster coordinates
    REAL rx = (x-_dim_u.ox)/_dim_u.hx;
    REAL ry = (y-_dim_u.oy)/_dim_u.hy;
    REAL rz = (z-_dim_u.oz)/_dim_u.hz;
        
    // Check bounds
    if (rx < 0.0 || ry < 0.0 || rz < 0.0 || 
        rx > (REAL)_dim_u.nx || ry > (REAL)_dim_u.ny || rz > (REAL)_dim_u.nz) {
        return noData_REAL;
    } else {        
        
        // Offset to cell centre for interpolation
        rx-=0.5;
        ry-=0.5;
        rz-=0.5;
            
        // Offset within grid cell
        REAL a = 0.0;
        REAL b = 0.0;
        REAL c = 0.0;
            
        uint i, ip;
        if (rx < 0.0) {
            i = 0;
            ip = 0;
            a = 0.0;
        } else if (rx < (REAL)(_dim_u.nx-1)) {
            i = (uint)rx;
            ip = i+1;
            a = rx-(REAL)i;
        } else {
            i = _dim_u.nx-1;
            ip = _dim_u.nx-1;
            a = 0.0;
        }
            
        uint j, jp;
        if (ry < 0.0) {
            j = 0;
            jp = 0;
            b = 0.0;
        } else if (ry < (REAL)(_dim_u.ny-1)) {
            j = (uint)ry;
            jp = j+1;
            b = ry-(REAL)j;
        } else {
            j = _dim_u.ny-1;
            jp = _dim_u.ny-1;
            b = 0.0;
        }
            
        uint k, kp;
        if (rz < 0.0) {
            k = 0;
            kp = 0;
            c = 0.0;
        } else if (rz < (REAL)(_dim_u.nz-1)) {
            k = (uint)rz;
            kp = k+1;
            c = rz-(REAL)k;
        } else {
            k = _dim_u.nz-1;
            kp = _dim_u.nz-1;
            c = 0.0;
        }

        REAL u000 = _val3D(_u, i , j , k );
        REAL u100 = _val3D(_u, ip, j , k );
        REAL u010 = _val3D(_u, i , jp, k );
        REAL u110 = _val3D(_u, ip, jp, k );
        REAL u001 = _val3D(_u, i , j , kp);
        REAL u101 = _val3D(_u, ip, j , kp);
        REAL u011 = _val3D(_u, i , jp, kp);
        REAL u111 = _val3D(_u, ip, jp, kp);
            
        // Calculate interpolated value
        REAL v;
        v = (REAL)(u111*     a *     b *     c );
        v+= (REAL)(u011*(1.0-a)*     b *     c );
        v+= (REAL)(u101*     a *(1.0-b)*     c );
        v+= (REAL)(u110*     a *     b *(1.0-c));
        v+= (REAL)(u001*(1.0-a)*(1.0-b)*     c );
        v+= (REAL)(u010*(1.0-a)*     b *(1.0-c));
        v+= (REAL)(u100*     a *(1.0-b)*(1.0-c));
        v+= (REAL)(u000*(1.0-a)*(1.0-b)*(1.0-c));

        return v;
    }
}

REALVEC2 getGradient2DREAL(
    const  __global REAL *_u, 
    const REAL x, 
    const REAL y,
    const Dimensions _dim_u) {

    // Check for valid raster dimensions
    if (_dim_u.hx <= 0.0 || _dim_u.hy <= 0.0 )
        return (REALVEC2)(0.0, 0.0);
    
    // Convert to raster coordinates
    REAL rx = (x-_dim_u.ox)/_dim_u.hx;
    REAL ry = (y-_dim_u.oy)/_dim_u.hy;
        
    // Check bounds
    if (rx < 1.0 || ry < 1.0 || 
        rx > (REAL)(_dim_u.nx-2) || ry > (REAL)(_dim_u.ny-2)) {
        return (REALVEC2)(0.0, 0.0);
    } else { 

        // Offset within grid cell
        uint i = (uint)rx;
        REAL a = rx-(REAL)i;

        uint j = (uint)ry;
        REAL b = ry-(REAL)j;

        // Get cell values
        REAL uimjm = _val2D(_u, i-1, j-1);
        REAL uimj0 = _val2D(_u, i-1, j  );
        REAL uimjp = _val2D(_u, i-1, j+1);
        REAL ui0jm = _val2D(_u, i  , j-1);
        REAL ui0j0 = _val2D(_u, i  , j  );
        REAL ui0jp = _val2D(_u, i  , j+1);
        REAL uipjm = _val2D(_u, i+1, j-1);
        REAL uipj0 = _val2D(_u, i+1, j  );
        REAL uipjp = _val2D(_u, i+1, j+1);

        // Calculate x-gradients
        REAL u00_x = ((ui0j0+ui0jm)-(uimj0+uimjm))/(2.0*_dim_u.hx);
        REAL u10_x = ((uipj0+uipjm)-(ui0j0+ui0jm))/(2.0*_dim_u.hx);
        REAL u01_x = ((ui0jp+ui0j0)-(uimjp+uimj0))/(2.0*_dim_u.hx);
        REAL u11_x = ((uipjp+uipj0)-(ui0jp+ui0j0))/(2.0*_dim_u.hx);
        
        // Calculate y-gradients
        REAL u00_y = ((uimj0+ui0j0)-(uimjm+ui0jm))/(2.0*_dim_u.hy);
        REAL u10_y = ((ui0j0+uipj0)-(ui0jm+uipjm))/(2.0*_dim_u.hy);
        REAL u01_y = ((uimjp+ui0jp)-(uimj0+ui0j0))/(2.0*_dim_u.hy);
        REAL u11_y = ((ui0jp+uipjp)-(ui0j0+uipj0))/(2.0*_dim_u.hy);
        
        // Calculate interpolated x-gradient
        REAL gx;
        gx =  (REAL)(u11_x*     a *     b );
        gx += (REAL)(u01_x*(1.0-a)*     b );
        gx += (REAL)(u10_x*     a *(1.0-b));
        gx += (REAL)(u00_x*(1.0-a)*(1.0-b));
        
        // Calculate interpolated y-gradient
        REAL gy;
        gy =  (REAL)(u11_y*     a *     b );
        gy += (REAL)(u01_y*(1.0-a)*     b );
        gy += (REAL)(u10_y*     a *(1.0-b));
        gy += (REAL)(u00_y*(1.0-a)*(1.0-b));
        
        return (REALVEC2)(gx, gy);
    }
}

/**
* Get general 2D value from neighbours
*/
REAL _getValue2D(
    const __global REAL *_u, 
    const __global REAL *_u_N, 
    const __global REAL *_u_E, 
    const __global REAL *_u_S, 
    const __global REAL *_u_W, 
    const __global REAL *_u_NE, 
    const __global REAL *_u_SE, 
    const __global REAL *_u_SW, 
    const __global REAL *_u_NW, 
    const Dimensions _dim,  
    const  size_t _i,
    const  size_t _j,
    const  int _ii,
    const  int _jj) {

    uint _lb = 0x30;
    int ri = _i+_ii;
    if (ri < 0) {
        ri = _dim.mx+ri;
        if (ri >= 0) {
            _lb &= 0x2F;
            _lb |= 0x01;
        }
    } else if (ri > _dim.mx-1) {
        ri = ri-_dim.mx;
        if (ri < _dim.mx) {
            _lb &= 0x2F;
            _lb |= 0x02;
        }
    } else {
        _lb &= 0x2F;
    }
        
    int rj = _j+_jj;
    if (rj < 0) {
        rj = _dim.my+rj;
        if (rj >= 0) {
            _lb &= 0x1F;
            _lb |= 0x08;
        }
    } else if (rj > _dim.my-1) {
        rj = rj-_dim.my;
        if (rj < _dim.my) {
            _lb &= 0x1F;
            _lb |= 0x04;
        }
    } else {
        _lb &= 0x1F;
    }

    switch(_lb) {
        case 0x00:
            return _val2D_a(_u,    ri, rj);
            break;

        case 0x05:
            return _val2D_a(_u_NW, ri, rj);
            break;

        case 0x04:
            return _val2D_a(_u_N,  ri, rj);
            break;

        case 0x06:
            return _val2D_a(_u_NE, ri, rj);
            break;

        case 0x02:
            return _val2D_a(_u_E,  ri, rj);
            break;

        case 0x0A:
            return _val2D_a(_u_SE, ri, rj);
            break;

        case 0x08:
            return _val2D_a(_u_S,  ri, rj);
            break;

        case 0x09:
            return _val2D_a(_u_SW, ri, rj);
            break;

        case 0x01:
            return _val2D_a(_u_W,  ri, rj);
            break;
    }

    return noData_REAL;
}

