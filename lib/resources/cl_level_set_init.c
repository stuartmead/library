/**
* Level set initialisation
*/
__kernel void init(
const parameters _p,
/*__ARGS__*/
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);
    
    // Sizes
    const size_t kn = get_global_size(2);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {
    
        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/

        // Set initial classification
        classbits = 3; // Default value of 3 for 10b (class 1) | 01b (state 1)

        // Cell state is given by lower bit of classification buffer 
        uint _class_state = 1; 

        // Cell classification is given by bits 1-24 bits of classification buffer 
        uint _class_lo = (classbits&0xFFFFFE)>>1;

        // Cell sub-classification is given by bits 24-32 of classification buffer 
        uint _class_sub_lo = classbits>>24; 

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------

        // Overwrite state if class is zero
        if (_class_lo == 0) _class_state = 0;

        // Set class
        classbits = ((_class_sub_lo&0xFF)<<24)|((_class_lo&0x7FFFFF)<<1)|(_class_state&0x1); 

/*__POST__*/

    }

}

