/**
* Level set step update
*/
__kernel void update(
const parameters _p,
uint _c,
__global uint *_status,
/*__ARGS__*/
) {
    
    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Sizes
    const size_t kn = get_global_size(2);
    
    // Local atomic variables
    __local uint _cell_count;
    __local uint _cell_max_speed;
    __local uint _tile_boundary;
    size_t _lid = get_local_id(0)+get_local_id(1)*get_local_size(0);
    if (_lid == 0) {
        _cell_count = 0;
        _cell_max_speed = 0;
        _tile_boundary = 0;
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {

        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/

        // Convert speed to fixed point integer for atomic maximum, note this limits the maximum speed to 65535
        atomic_max(&_cell_max_speed, (uint)65535.0*min(_speed, 65535.0));
    
        // Update forcing, set speed outside narrow band to 50% of maximum speed
        REAL _dx = fmax(fmax(_distance_update-_distance_update_W, _distance_update-_distance_update_E)/_dim.hx, (REAL)0.0);
        REAL _dy = fmax(fmax(_distance_update-_distance_update_S, _distance_update-_distance_update_N)/_dim.hy, (REAL)0.0);
        REAL _r2 = -(fabs(_distance_update) < _p.band_width ? _speed : 0.5*_p.max_speed)*hypot(_dx, _dy);
    
        // Time integrate with Runge-Kutta 2-step method
        _rate = 0.5*(_rate+_r2);
        _distance+=_p.dt*_rate;

        // Update arrival time
        if (_distance <= 0.0 && !isValid_REAL(_arrival)) {

            // Calculate corrected arrival time from level set and local rate
            if (_rate != 0.0) {
                REAL _tcorrect = fmin(_distance/_rate, _p.dt);
                _arrival = fmax(_p.time-_tcorrect, (REAL)0.0); 
            } else {
                _arrival = _p.time; 
            }

            //// Update start time
            //_start_time_update = _arrival;
            //if (isValid_REAL(_start_time_N))
            //    _start_time_update = min(_start_time_update, _start_time_N);
            //if (isValid_REAL(_start_time_NE))
            //    _start_time_update = min(_start_time_update, _start_time_NE);
            //if (isValid_REAL(_start_time_E))
            //    _start_time_update = min(_start_time_update, _start_time_E);
            //if (isValid_REAL(_start_time_SE))
            //    _start_time_update = min(_start_time_update, _start_time_SE);
            //if (isValid_REAL(_start_time_S))
            //    _start_time_update = min(_start_time_update, _start_time_S);
            //if (isValid_REAL(_start_time_SW))
            //    _start_time_update = min(_start_time_update, _start_time_SW);
            //if (isValid_REAL(_start_time_W))
            //    _start_time_update = min(_start_time_update, _start_time_W);
            //if (isValid_REAL(_start_time_NW))
            //    _start_time_update = min(_start_time_update, _start_time_NW);
        }
    
        // Run user code
        if (isValid_REAL(_arrival)) {

            // Cell state is given by lower bit of classification buffer 
            uint _class_state = classbits&1;

            // Cell classification is given by bits 1-24 bits of classification buffer 
            uint _class_lo = (classbits&0xFFFFFE)>>1;

            // Cell sub-classification is given by bits 24-32 of classification buffer 
            uint _class_sub_lo = classbits>>24;

            // Get speed
            const REAL speed = _speed;

            // ---------------------------------
            // User defined code
/*__CODE__*/
            // ---------------------------------

            // Update burnt cell count
            atomic_inc(&_cell_count);
        
            // Set state
            classbits = (classbits&~1)|(_class_state&1);
        }

        // Update boundary values to identify if a new tile needs to be created
        if (fabs(_distance) < _p.band_width) {

            // Set bits to represent interior cell
            atomic_or(&_tile_boundary, 0x10);

            // Set bits to represent boundary approach
            if (_j == _dim.ny-1) atomic_or(&_tile_boundary, 0x01); // Interface is near north boundary
            if (_i == _dim.nx-1) atomic_or(&_tile_boundary, 0x02); // Interface is near east boundary
            if (_j == 0        ) atomic_or(&_tile_boundary, 0x04); // Interface is near south boundary
            if (_i == 0        ) atomic_or(&_tile_boundary, 0x08); // Interface is near west boundary
        }

/*__POST__*/

    }

    // Update atomic variables
    barrier(CLK_LOCAL_MEM_FENCE);
    if (_lid == 0) {
        atomic_add(_status, _cell_count);
        atomic_max(_status+1, _cell_max_speed);
        atomic_or(_status+2+_c, _tile_boundary);
    }

}

/**
* Level set step update for inactive cells
*/
__kernel void updateInactive(
const parameters _p,
/*__ARGS__*/
) {
    
    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Sizes
    const size_t kn = get_global_size(2);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {

        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/
    
        // Run user code
        if (isValid_REAL(_arrival)) {

            // Cell state is given by lower bit of classification buffer 
            uint _class_state = classbits&1;

            // Cell classification is given by bits 1-24 bits of classification buffer 
            uint _class_lo = (classbits&0xFFFFFE)>>1;

            // Cell sub-classification is given by bits 24-32 of classification buffer 
            uint _class_sub_lo = classbits>>24;

            // Get speed
            const REAL speed = _speed;

            // ---------------------------------
            // User defined code
/*__CODE__*/
            // ---------------------------------

        }

/*__POST__*/

    }

}

