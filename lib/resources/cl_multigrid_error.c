/**
* Multigrid residual error
*/
__kernel void error(
const uint _tileBoundary,
const REAL _ihx2,
const REAL _ihy2,
/*__ARGS__*/
) {
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    
    size_t _lid = get_local_id(0)+get_local_id(1)*get_local_size(0);

/*__REDUCE__
// Initialise reduction
size_t _lsize = get_local_size(0)*get_local_size(1);
__REDUCE_HEAD__
barrier(CLK_LOCAL_MEM_FENCE);
__REDUCE__*/

/*__VARS__*/
    
    // Calculate residual
    REAL l_diag = -2.0*(_ihx2+_ihy2);
    REAL l_val = _ihx2*(l0_E+l0_W)+_ihy2*(l0_N+l0_S);
    l1 = b-(l_diag*l0+l_val);
    
    // Enforce Dirichlet condition on W and S boundaries
    if (_tileBoundary & ((uint)_boundary_W | ((uint)_boundary_S << 1))) {
        l1 = 0.0;
    }
    
/*__POST__*/

/*__REDUCE__
// Calculate reduction
barrier(CLK_LOCAL_MEM_FENCE);
for (size_t _step = _lsize>>1; _step >= 1; _step>>=1) {
    if (_lid < _step) {
        __REDUCE_CODE__
    }
    barrier(CLK_LOCAL_MEM_FENCE);
}
if (_lid == 0) {
    size_t gid = get_group_id(0)+get_num_groups(0)*(get_group_id(1)+get_num_groups(1)*get_group_id(2));
    __REDUCE_POST__
}
__REDUCE__*/

}

