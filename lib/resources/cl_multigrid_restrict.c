/**
* Restrict residual
*/
__kernel void rest(
const REAL _ihx2,
const REAL _ihy2,
__global REAL *_b,
__global REAL *_l0,
__global REAL *_l1,
__global REAL *_l1_N,
__global REAL *_l1_NE,
__global REAL *_l1_E,
__global REAL *_l1_SE,
__global REAL *_l1_S,
__global REAL *_l1_SW,
__global REAL *_l1_W,
__global REAL *_l1_NW
) {

    // Raster indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _iic = get_local_id(0)<<1;
    const size_t _jjc = get_local_id(1)<<1;
    size_t _if = _i<<1;
    size_t _jf = _j<<1;
    
    // Check quadrant and shift
    if (_if > /*__MSIZE__*/-1 && _jf > /*__MSIZE__*/-1) {

        // Shift north-east
        _if -= /*__MSIZE__*/;
        _jf -= /*__MSIZE__*/;

        _l1_W  = _l1_N;
        _l1_SW = _l1;
        _l1    = _l1_NE;
        _l1_S  = _l1_E;
    
    } else if (_if > /*__MSIZE__*/-1) {

        // Shift east
        _if -= /*__MSIZE__*/;

        _l1_W  = _l1;
        _l1_SW = _l1_S;
        _l1    = _l1_E;
        _l1_S  = _l1_SE;

    } else if (_jf > /*__MSIZE__*/-1) {

        // Shift north
        _jf -= /*__MSIZE__*/;

        _l1_SW = _l1_W;
        _l1_W  = _l1_NW;
        _l1_S  = _l1;
        _l1    = _l1_N;
    }
    
    // Populate cache
    __local REAL cache[33][33];
    cache[_iic+1][_jjc+1] = _val2D_a(_l1, _if, _jf);
    cache[_iic+2][_jjc+1] = _val2D_a(_l1, _if+1, _jf  );
    cache[_iic+1][_jjc+2] = _val2D_a(_l1, _if  , _jf+1);
    cache[_iic+2][_jjc+2] = _val2D_a(_l1, _if+1, _jf+1);
    if (_iic == 0) {
        cache[0][_jjc+1] = (_if == 0) ? _val2D_a(_l1_W, /*__MSIZE__*/-1, _jf) : _val2D_a(_l1, _if-1, _jf);
        cache[0][_jjc+2] = (_if == 0) ? _val2D_a(_l1_W, /*__MSIZE__*/-1, _jf+1) : _val2D_a(_l1, _if-1, _jf+1);
    }
    if (_jjc == 0) {
        cache[_iic+1][0] = (_jf == 0) ? _val2D_a(_l1_S, _if, /*__MSIZE__*/-1) : _val2D_a(_l1, _if, _jf-1);
        cache[_iic+2][0] = (_jf == 0) ? _val2D_a(_l1_S, _if+1, /*__MSIZE__*/-1) : _val2D_a(_l1, _if+1, _jf-1);
    }
    if (_iic == 0 && _jjc == 0) {
        cache[0][0] = ((_if == 0) && (_jf == 0)) ? _val2D_a(_l1_SW, /*__MSIZE__*/-1, /*__MSIZE__*/-1) :
            ((_if == 0) ? _val2D_a(_l1_W, /*__MSIZE__*/-1, _jf-1) : 
            ((_jf == 0) ? _val2D_a(_l1_S, _if-1, /*__MSIZE__*/-1) : _val2D_a(_l1, _if-1, _jf-1)));
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    // Calculate interpolated value
    REAL l_r = cache[_iic+1][_jjc+1]+
        0.5*(
            cache[_iic+2][_jjc+1]+  // E
            cache[_iic  ][_jjc+1]+  // W
            cache[_iic+1][_jjc+2]+  // N
            cache[_iic+1][_jjc  ])+ // S
        0.25*(
            cache[_iic+2][_jjc+2]+  // NE
            cache[_iic  ][_jjc+2]+  // NW
            cache[_iic+2][_jjc  ]+  // SE
            cache[_iic  ][_jjc  ]); // SW
    
    // Write to coarse layer
    REAL b = 0.25*l_r;
    *(_b+_i+(_j<</*__ROW_BSHIFT__*/)) = b;

    // Set error, this is the value of one Jacobi step with initial zero error
    REAL w = 2.0/3.0;
    REAL l_diag = 2.0*(_ihx2+_ihy2);
    *(_l0+_i+(_j<</*__ROW_BSHIFT__*/)) = -b*w/l_diag;
}

