/**
* Shallow water collision step
*/
__kernel void collision(
REAL c,
REAL ic2,
REAL ics2,
REAL itau,
uint _tid,
__global uint *_status,
/*__ARGS__*/
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    
    size_t _lid = get_local_id(0)+get_local_id(1)*get_local_size(0);

    __local uint _tile_boundary;
    if (_lid == 0) {
        _tile_boundary = 0;
    }
    
/*__REDUCE__
// Initialise reduction
size_t _lsize = get_local_size(0)*get_local_size(1);
__REDUCE_HEAD__
barrier(CLK_LOCAL_MEM_FENCE);
__REDUCE__*/

    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {

/*__VARS__*/        
            
        if (h > 0.0) {
        
            // Local variables
            const REAL g = 9.81;
            REAL u = uh/h;
            REAL v = vh/h;
            REAL u2 = u*u+v*v;
            REAL gh2 = g*h*h;

            const REAL i2 = (REAL)0.5;
            const REAL i3 = (REAL)(1.0/3.0);
            const REAL i6 = (REAL)(1.0/6.0);
            const REAL i8 = (REAL)(1.0/8.0);
            const REAL i9 = (REAL)(1.0/9.0);
            const REAL i12 = (REAL)(1.0/12.0);
            const REAL i24 = (REAL)(1.0/24.0);
            const REAL i36 = (REAL)(1.0/36.0);

            // Calculate dot product of lattice vectors and velocity
            REAL e_dot_u[8];
            e_dot_u[0] =  c*u;
            e_dot_u[1] =  c*v;
            e_dot_u[2] = -c*u;
            e_dot_u[3] = -c*v;
            e_dot_u[4] =  c*(u+v);
            e_dot_u[5] =  c*(v-u);
            e_dot_u[6] = -c*(u+v);
            e_dot_u[7] =  c*(u-v);

            // Calculate equilibrium values
            REAL fe[9];
            fe[0] = h-5.0*i6*gh2*ic2-2.0*i3*h*u2*ic2;
            fe[1] = i6*gh2*ic2+h*ic2*(e_dot_u[0]*(i3+i2*ic2*e_dot_u[0])-i6*u2);
            fe[2] = i6*gh2*ic2+h*ic2*(e_dot_u[1]*(i3+i2*ic2*e_dot_u[1])-i6*u2);
            fe[3] = i6*gh2*ic2+h*ic2*(e_dot_u[2]*(i3+i2*ic2*e_dot_u[2])-i6*u2);
            fe[4] = i6*gh2*ic2+h*ic2*(e_dot_u[3]*(i3+i2*ic2*e_dot_u[3])-i6*u2);
            fe[5] = i24*gh2*ic2+h*ic2*(e_dot_u[4]*(i12+i8*ic2*e_dot_u[4])-i24*u2);
            fe[6] = i24*gh2*ic2+h*ic2*(e_dot_u[5]*(i12+i8*ic2*e_dot_u[5])-i24*u2);
            fe[7] = i24*gh2*ic2+h*ic2*(e_dot_u[6]*(i12+i8*ic2*e_dot_u[6])-i24*u2);
            fe[8] = i24*gh2*ic2+h*ic2*(e_dot_u[7]*(i12+i8*ic2*e_dot_u[7])-i24*u2);

            // Relax
            for (uint k = 0; k < 9; k++) {
                REAL _fi = _val3D_a(__fi, _i, _j, k);
                _val3D_a(__fe, _i, _j, k) = fe[k];
                _val3D_a(__ft, _i, _j, k) = _fi-itau*(_fi-fe[k]);
            }

            // Add source terms
            _val3D_a(__ft, _i, _j, 1) -= i9 *ics2*g*0.5*(h_E+h)*(b_E-b);
            _val3D_a(__ft, _i, _j, 2) -= i9 *ics2*g*0.5*(h_N+h)*(b_N-b);
            _val3D_a(__ft, _i, _j, 3) -= i9 *ics2*g*0.5*(h_W+h)*(b_W-b);
            _val3D_a(__ft, _i, _j, 4) -= i9 *ics2*g*0.5*(h_S+h)*(b_S-b);
            _val3D_a(__ft, _i, _j, 5) -= i36*ics2*g*0.5*(h_NE+h)*(b_NE-b);
            _val3D_a(__ft, _i, _j, 6) -= i36*ics2*g*0.5*(h_NW+h)*(b_NW-b);
            _val3D_a(__ft, _i, _j, 7) -= i36*ics2*g*0.5*(h_SW+h)*(b_SW-b);
            _val3D_a(__ft, _i, _j, 8) -= i36*ics2*g*0.5*(h_SE+h)*(b_SE-b);
            
            // Set bits to represent interior cell
            atomic_or(&_tile_boundary, 0x10);

            // Set bits to represent boundary approach
            if (_j == _dim.ny-1) atomic_or(&_tile_boundary, 0x01); // Interface is near north boundary
            if (_i == _dim.nx-1) atomic_or(&_tile_boundary, 0x02); // Interface is near east boundary
            if (_j == 0        ) atomic_or(&_tile_boundary, 0x04); // Interface is near south boundary
            if (_i == 0        ) atomic_or(&_tile_boundary, 0x08); // Interface is near west boundary

        } else {            

            // Set all updated values to null
            for (uint k = 0; k < 9; k++) {
                _val3D_a(__fe, _i, _j, k) = 0.0;
                _val3D_a(__ft, _i, _j, k) = noData_REAL;
            }
        }

/*__POST__*/

    }

/*__REDUCE__
// Calculate reduction
barrier(CLK_LOCAL_MEM_FENCE);
for (size_t _step = _lsize>>1; _step >= 1; _step>>=1) {
    if (_lid < _step) {
        __REDUCE_CODE__
    }
    barrier(CLK_LOCAL_MEM_FENCE);
}
if (_lid == 0) {
    size_t gid = get_group_id(0)+get_num_groups(0)*(get_group_id(1)+get_num_groups(1)*get_group_id(2));
    __REDUCE_POST__
}
__REDUCE__*/

    // Update atomic variables
    if (_lid == 0) {
        atomic_or(_status+_tid, _tile_boundary);
    }

}

