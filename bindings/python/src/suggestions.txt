ipython
matplotlib
gdal>2.4.1
netcdf4
xarray
Cython
rasterio
shapely
geopandas
cftime
cfunits
requests
pydap
pyproj
