# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from __future__ import annotations
import os
import os.path as pth
import sys
import numpy as np
import json
import warnings
import numbers
from abc import ABCMeta, abstractmethod
from typing import Union, Tuple, List, Dict, Any, Optional
from datetime import datetime
from collections import deque
from .._version import version
from ..raster import raster
from .. import dataset
from ..dataset import supported_libs
from .. import utils
from .ncutils import Pydap2NC
from .timeutils import RasterTime

warnings.filterwarnings("ignore", category=UserWarning)

if supported_libs.HAS_GDAL:
    try:
        from osgeo import gdal, gdalconst, osr
        os.environ['GDAL_CACHEMAX'] = "100"
        os.environ['VSI_CACHE'] = "OFF"
    except ImportError:
        warnings.warn(
            "gdal is not correctly installed, check your installation.", RuntimeWarning)
        supported_libs.HAS_GDAL = False

if supported_libs.HAS_NCDF:
    import netCDF4 as nc
    if supported_libs.HAS_PYDAP:
        from pydap.client import open_url

if supported_libs.HAS_XARRAY:
    import xarray as xr

if supported_libs.HAS_RASTERIO:
    import rasterio as rio
    try:
        from rasterio.windows import Window
    except ImportError:
        warnings.warn(
            "rasterio is not correctly installed, check your installation.", RuntimeWarning)
        supported_libs.HAS_RASTERIO = False
    if supported_libs.HAS_RASTERIO and hasattr(rio, "_env"):
        rio._env.set_gdal_config("GDAL_CACHEMAX", 100)
        rio._env.set_gdal_config("VSI_CACHE", False, normalize=True)


__all__ = ["get_gdal_geotransform", "DataHandler", "GDAL_Handler",
           "NC_Handler", "XR_Handler", "RIO_Handler", "get_layers"]


def get_layers(arg, nz: numbers.Integral) -> List:
    """Get indices for 3d axis.

    Parameters
    ----------
    arg: numbers.Integral/slice/list/tuple
        index, list of indices, slice along 3rd axis to read 3d data from file.
    nz: numbers.Integral
        length along 3rd axis

    Returns
    -------
    levels: list
        list of indices along the 3rd axis
    """
    if arg is None:
        stride = slice(None)
    if isinstance(arg, numbers.Integral):
        if arg == -1:
            stride = slice(None)
        else:
            stride = slice(arg, arg + 1, None)
    elif isinstance(arg, (list, tuple, np.ndarray)):
        # get unique integer values from list
        stride = list(set([int(level) for level in arg]))
    elif isinstance(arg, slice):
        stride = arg

    levels = [i for i in range(nz)]

    if isinstance(stride, slice):
        levels = levels[stride]
    elif isinstance(stride, list):
        levels = list(filter(lambda x: x in stride, levels))
        if not len(levels) > 0:
            levels = get_layers(-1, nz)
    return levels


def get_gdal_geotransform(raster_dimensions: Union["raster.RasterDimensions", Dict]) -> Tuple:
    """Compute geotransform using raster dimensions.

    Parameters
    ----------
    raster_dimensions: raster.RasterDimensions/ dict
        RasterDimensions of a raster.Raster object

    Returns
    -------
    out: tuple
        gdal geotransform tuple representation of raster.RasterDimensions

    Raises
    ------
    TypeError: Input raster dimensions should contain key 'dim'
    """
    if isinstance(raster_dimensions, dict):
        if 'dim' in raster_dimensions:
            ox = raster_dimensions['dim']['ox']
            hx = raster_dimensions['dim']['hx']
            hy = raster_dimensions['dim']['hy']
            oy = raster_dimensions['oy']
            if hy > 0:
                hy = hy * -1
                oy = raster_dimensions['ey']
        else:
            raise TypeError("Input raster dimensions should contain key 'dim'")
    elif isinstance(raster_dimensions, raster.RasterDimensions):
        ox = raster_dimensions.ox
        hx = raster_dimensions.hx
        if raster_dimensions.hy > 0:
            hy = -1 * raster_dimensions.hy
            oy = raster_dimensions.ey
        elif raster_dimensions.hy < 0:
            hy = raster_dimensions.hy
            oy = raster_dimensions.oy
    out = (ox, hx, 0.0, oy, 0.0, hy)
    return out


@supported_libs.RequireLib("gdal")
def return_proj4(gdal_file):
    """Get GDAL Dataset projection as Proj4 string.

    Note
    ----
    Unable to use with gdal, possibly due to the thread safety
    https://gdal.org/development/rfc/rfc16_ogr_reentrancy.html

    Parameters
    ----------
    gdal_file: gdal.Dataset/str
        A GDAL Dataset projection as a proj4 string

    Examples
    --------
    >>> test = gdal.Dataset("test.tif")
    >>> out_str = return_proj4(test)
    """
    out = ""
    if isinstance(gdal_file, gdal.Dataset):
        gdal_dataset = gdal_file
    elif isinstance(gdal_file, str):
        gdal_dataset = gdal.OpenEx(gdal_file)

    if hasattr(gdal_dataset, "GetSpatialRef"):
        spatialRef = gdal_dataset.GetSpatialRef()
        if spatialRef is not None:
            out = spatialRef.ExportToProj4()
    else:
        proj = gdal_dataset.GetProjection()
        if len(proj) > 1:
            proj_ref = osr.SpatialReference()
            proj_ref.ImportFromWkt(proj)
            out = proj_ref.ExportToProj4()
    return out


class DataHandler(object, metaclass=ABCMeta):
    __slots__ = ()

    @abstractmethod
    def reader(self, *args, **kwargs):
        raise NotImplementedError("reader method is not implemented")

    @abstractmethod
    def setter(self, *args, **kwargs):
        raise NotImplementedError("setter method is not implemented")

    @abstractmethod
    def writer(self, *args, **kwargs):
        raise NotImplementedError("writer method is not implemented")

    @abstractmethod
    def time(self, *args, **kwargs):
        raise NotImplementedError("time method is not implemented")

    @abstractmethod
    def __exit__(self, *args):
        raise NotImplementedError("exit method is not implemented")


def get_tiles_count(xsize, ysize, tile_size):
    xtiles = divmod(xsize, tile_size)
    ytiles = divmod(ysize, tile_size)
    if xtiles[1] == 0:
        xtiles = xtiles[0]
    else:
        xtiles = xtiles[0] + 1

    if ytiles[1] == 0:
        ytiles = ytiles[0]
    else:
        ytiles = ytiles[0] + 1
    return xtiles, ytiles


class NC_Handler(DataHandler):

    def __init__(self,
                 fileName: Optional[Union[str, Any]] = None,
                 base_type: np.dtype = np.float32,
                 variable_map: Optional[Dict] = None,
                 data_type: np.dtype = np.float32):
        # if the filename is a path to a file, ensure it's an absolute path
        if fileName is not None:
            if isinstance(fileName, str) and 'dodsC' not in fileName:
                fileName = pth.abspath(
                    fileName) if pth.exists(fileName) else None
        self._file_name = fileName
        self._file_handler = None
        self.is_thredds = False
        self.use_pydap = False
        self.invert_y = False
        self.data_type = data_type
        self.base_type = base_type
        self.nullValue = raster.getNullValue(data_type)
        self._time_handler = None
        self._variable_map = variable_map
        self._dims = deque()
        self.layers = [0]

    @supported_libs.RequireLib("netcdf")
    def reader(self, thredds: bool = False, use_pydap: bool = False, **kwargs):
        # set thredds and use_pydap arguments
        self.is_thredds = thredds
        self.use_pydap = use_pydap

        if self._file_name is None:
            raise ValueError("file_name cannot be None")

        # check file_name and open file
        if not self.is_thredds and not self.use_pydap:
            if isinstance(self._file_name, (str, bytes)):
                # when file is on disc
                if isinstance(self._file_name, bytes):
                    self._file_handler = nc.Dataset(
                        self._file_name.decode(), mode='r')
                elif isinstance(self._file_name, str):
                    self._file_handler = nc.Dataset(self._file_name, mode='r')
            elif isinstance(self._file_name, list):
                _file_list = [item.decode() if isinstance(item, bytes) else item
                              for item in self._file_name]
                if len(_file_list) > 1:
                    self._file_handler = nc.MFDataset(_file_list)
                else:
                    self._file_handler = nc.Dataset(_file_list[0])
        elif self.is_thredds:
            # when using thredds
            if self.use_pydap and supported_libs.HAS_PYDAP:
                # open using pydap
                # pydap can be used when thredds server requires
                # authentication

                # Pydap2NC provides a netCDF4.Dataset like interface to
                # pydap.Dataset
                if isinstance(self._file_name, Pydap2NC):
                    self._file_handler = self._file_name
                elif isinstance(self._file_name, str):
                    self._file_handler = Pydap2NC(open_url(self._file_name))
                else:
                    self._file_handler = Pydap2NC(self._file_name)
            else:
                if isinstance(self._file_name, list):
                    _file_list = [item.decode() if isinstance(item, bytes) else item
                                  for item in self._file_name]
                    if len(_file_list) > 1:
                        self._file_handler = nc.MFDataset(_file_list)
                    else:
                        self._file_handler = nc.Dataset(_file_list[0])
                elif isinstance(self._file_name, (str, bytes)):
                    # open using netcdf4 library
                    if isinstance(self._file_name, str):
                        self._file_handler = nc.Dataset(self._file_name)
                    elif isinstance(self._file_name, bytes):
                        self._file_handler = nc.Dataset(
                            self._file_name.decode())

        if isinstance(self._file_name, (nc.Dataset, nc.MFDataset)):
            # assign nc.Dataset to file_handler when file_name is nc.Dataset
            self._file_handler = self._file_name
            # change file_name to path of file
            if isinstance(self._file_name, nc.Dataset):
                self._file_name = self._file_name.filepath()
            else:
                self._file_name = None

        if self._file_handler is None:
            raise ValueError("file_handler cannot be identified")

        if 'time' in self._file_handler.variables:
            self._time_handler = RasterTime(
                self._file_handler.variables['time'])
            self._time_handler.set_index_bounds()

        # use projection information when available in netcdf4 file
        projStr = ""
        if 'crs' in self._file_handler.variables:
            if hasattr(self._file_handler.variables['crs'], "crs_wkt"):
                projStr = utils.proj4_from_wkt(
                    self._file_handler.variables['crs'].getncattr("crs_wkt"))
            elif hasattr(self._file_handler.variables['crs'], "spatial_ref"):
                projStr = utils.proj4_from_wkt(
                    self._file_handler.variables['crs'].getncattr("spatial_ref"))

        # get dimensions from netcdf file
        if 'lon' in self._file_handler.dimensions:
            lon = self._file_handler.variables['lon']
            nx = self._file_handler.dimensions['lon'].size
            self._dims.append("lon")
        elif 'longitude' in self._file_handler.dimensions:
            lon = self._file_handler.variables['longitude']
            nx = self._file_handler.dimensions['longitude'].size
            self._dims.append("longitude")
        elif 'x' in self._file_handler.dimensions:
            lon = self._file_handler.variables['x']
            nx = self._file_handler.dimensions['x'].size
            self._dims.append("x")
        else:
            raise KeyError("lon/longitude/x not found in file dimensions")

        if 'lat' in self._file_handler.dimensions:
            lat = self._file_handler.variables['lat']
            ny = self._file_handler.dimensions['lat'].size
            self._dims.appendleft("lat")
        elif 'latitude' in self._file_handler.dimensions:
            lat = self._file_handler.variables['latitude']
            ny = self._file_handler.dimensions['latitude'].size
            self._dims.appendleft("latitude")
        elif 'y' in self._file_handler.dimensions:
            lat = self._file_handler.variables['y']
            ny = self._file_handler.dimensions['y'].size
            self._dims.appendleft("y")
        else:
            raise KeyError("lat/latitude/y not found in file dimensions")

        # add support for third dimension
        # get the third dimension to map
        layer_dim_name = kwargs.get("dims")
        if layer_dim_name is None:
            layer_dim_name = "time"
        else:
            current_size = len(self._dims)
            if not isinstance(layer_dim_name, (tuple, list)):
                raise TypeError(
                    f"Dimensions iterable {layer_dim_name} should be a tuple or list")

            if len(layer_dim_name) < current_size:
                raise ValueError(
                    f"Dimension tuple {layer_dim_name} should be atleast {tuple(self._dims)}")

            invalid_dims = []
            for i in range(current_size):
                offset = len(layer_dim_name) - current_size
                if layer_dim_name[i + offset] != self._dims[i]:
                    invalid_dims.append(layer_dim_name[i + offset])
            if len(invalid_dims) > 0:
                raise ValueError(
                    f"Dimension tuple {layer_dim_name} is not valid")
            for item in layer_dim_name:
                if item not in self._dims:
                    self._dims.appendleft(item)
            layer_dim_name = self._dims[0]

        layer_dim_size = self._file_handler.dimensions.get(layer_dim_name)
        if layer_dim_size is None:
            layer_dim_size = 1
        else:
            layer_dim_size = layer_dim_size.size
        self.layers = get_layers(kwargs.get("layers", -1), layer_dim_size)
        self.layers = sorted(self.layers)
        nz = len(self.layers)
        if nz > 1:
            hz = self.layers[1] - self.layers[0]
        else:
            hz = 1
        oz = float(self.layers[0])

        time = self.time(0)

        # compute dimension input for instantiating raster.Raster
        hx = lon[1] - lon[0]
        hy = lat[1] - lat[0]

        min_x = min(lon[0], lon[-1])
        if isinstance(min_x, np.ma.MaskedArray):
            ox = self.data_type(min_x.data)
        elif isinstance(min_x, np.ndarray):
            ox = self.data_type(min_x)
        elif isinstance(min_x, numbers.Real):
            ox = self.data_type(min_x)
        ox -= 0.5 * hx

        min_y = min(lat[0], lat[-1])
        if isinstance(min_y, np.ma.MaskedArray):
            oy = self.data_type(min_y)
        elif isinstance(min_y, np.ndarray):
            oy = self.data_type(min_y)
        elif isinstance(min_y, numbers.Real):
            oy = self.data_type(min_y)

        if hy < 0:
            self.invert_y = True
            hy = abs(hy)
        oy -= 0.5 * hy

        return nx, ny, nz, hx, hy, hz, ox, oy, oz, projStr.encode("utf-8"), time

    def time(self, tidx: numbers.Integral) -> numbers.Real:
        time = 0.0
        if self._time_handler is not None:
            time = self.time_from_index(tidx)
        return time

    def set_time_bounds(self, start_time: Optional[Union[numbers.Real, str]] = None,
                        end_time: Optional[Union[numbers.Real, str]] = None,
                        dt_format: Optional[str] = None):
        if self._time_handler is not None:
            self._time_handler.set_index_bounds(start_time=start_time,
                                                end_time=end_time,
                                                dt_format=dt_format)
        else:
            raise RuntimeError("No time handle has been set")

    def time_from_index(self, index: numbers.Integral) -> numbers.Real:
        if self._time_handler is not None:
            return self._time_handler.time_from_index(index)
        else:
            raise RuntimeError("No time handle has been set")

    def index_from_time(self, timestamp: Union[datetime, numbers.Real]) -> numbers.Integral:
        if self._time_handler is not None:
            return self._time_handler.get_index(timestamp)
        else:
            raise RuntimeError("No time handle has been set")

    def get_max_time_index(self):
        if self._time_handler is not None:
            return self._time_handler.get_max_time_index()
        else:
            raise RuntimeError("No time handle has been set")

    def get_left_index(self, timestamp: Union[datetime, numbers.Real]) -> numbers.Integral:
        if self._time_handler is not None:
            return self._time_handler.get_left_index(timestamp)
        else:
            raise RuntimeError("No time handle has been set")

    def get_right_index(self, timestamp: Union[datetime, numbers.Real]) -> numbers.Integral:
        if self._time_handler is not None:
            return self._time_handler.get_right_index(timestamp)
        else:
            raise RuntimeError("No time handle has been set")

    def writer(self, fileName, jsonConfig):
        writer_config = json.loads(jsonConfig)
        raise NotImplementedError("writer not yet implemented")

    @supported_libs.RequireLib("netcdf")
    def check_handler(self):
        if not isinstance(self, NC_Handler):
            raise TypeError("Unable to understand the input class instance")

        if self._file_handler is None:
            raise ValueError("file_handler cannot be identified")

        if not isinstance(self._file_handler, (nc.Dataset, nc.MFDataset, Pydap2NC)):
            raise TypeError("file_handler is of incorrect type")

        if not isinstance(self._file_handler, (Pydap2NC, nc.MFDataset)):
            if self._file_handler.filepath() != self._file_name:
                raise ValueError("Mismatch between filepath '{}' and filename '{}'".format(
                    self._file_handler.filepath(), self._file_name))

    @supported_libs.RequireLib("netcdf")
    def setter(self, ti: int, tj: int, tx: int, ty: int,
               varname: str, tidx: int) -> Tuple[np.ndarray, int, int]:

        # Check handle
        self.check_handler()

        # Create buffer
        tile_size = raster.TileSpecifications().tileSize
        buf_arr = np.full((len(self.layers), tile_size, tile_size), self.nullValue,
                          dtype=self.data_type)

        # Get dimensions
        if 'lon' in self._file_handler.dimensions:
            lon = self._file_handler.dimensions['lon']
        elif 'longitude' in self._file_handler.dimensions:
            lon = self._file_handler.dimensions['longitude']
        elif 'x' in self._file_handler.dimensions:
            lon = self._file_handler.dimensions['x']
        else:
            raise KeyError("lon/longitude/x doesn't exist in the file")

        if 'lat' in self._file_handler.dimensions:
            lat = self._file_handler.dimensions['lat']
        elif 'latitude' in self._file_handler.dimensions:
            lat = self._file_handler.dimensions['latitude']
        elif 'y' in self._file_handler.dimensions:
            lat = self._file_handler.dimensions['y']
        else:
            raise KeyError("lat/latitude/y doesn't exist in the file")

        x_start = ti * tile_size
        x_end = min(min((ti + 1), tx) * tile_size, lon.size)

        if self.invert_y:
            y_start = lat.size - min(min((tj + 1), ty) * tile_size, lat.size)
            y_end = lat.size - tj * tile_size
        else:
            y_start = tj * tile_size
            y_end = min(min((tj + 1), ty) * tile_size, lat.size)

        # handle reading 3d Data
        if len(self.layers) > 1:
            # when more than 1 layers to be read
            # here, tidx value is ignored
            z_slice = slice(self.layers[0],
                            self.layers[-1] + 1,
                            self.layers[1] - self.layers[0])
            # set value for axis to be flipped (when invert_y is true, and data is 3d)
            flip_axis = 1
        else:
            # when only one layer (spatial layer, stepping in time)
            z_slice = tidx
            # set value for axis to be flipped (when invert_y is true, and data is 2d)
            flip_axis = 0

        xtiles, _ = get_tiles_count(lon.size, lat.size, tile_size)
        tile_idx = ti + tj * xtiles
        # print(ti, tj, tile_idx)

        # Get variable name
        if isinstance(varname, str):
            var_name = varname
        elif isinstance(varname, bytes):
            var_name = varname.decode()

        var_name = self._variable_map.get(var_name, var_name)

        # Get data
        if var_name in self._file_handler.variables:
            var_shape = self._file_handler.variables[var_name].shape
            var_dims = self._file_handler.variables[var_name].dimensions
            # Check data dimensions
            if len(var_dims) == 3:
                # read 2D + time data
                if var_dims[-2:] == tuple(self._dims)[-2:][::-1]:
                    # rotate data when oriented time,x,y
                    temp = self._file_handler.variables[var_name][z_slice,
                                                                  x_start:x_end,
                                                                  y_start:y_end]
                    temp = np.swapaxes(temp, -1, -2)
                elif var_dims[-2:] == tuple(self._dims)[-2:]:
                    temp = self._file_handler.variables[var_name][z_slice,
                                                                  y_start:y_end,
                                                                  x_start:x_end]
            elif len(var_dims) == 2:
                # read 2D data
                if var_dims == tuple(self._dims)[::-1]:
                    # rotate data when oriented x,y
                    temp = self._file_handler.variables[var_name][x_start:x_end,
                                                                  y_start:y_end]
                    temp = np.swapaxes(temp, -1, -2)
                elif var_dims == tuple(self._dims):
                    temp = self._file_handler.variables[var_name][y_start:y_end,
                                                                  x_start:x_end]
            else:
                raise RuntimeError(
                    "Only 2D or 2D + time is currently supported")
            if any(filter(lambda s: s == 1, temp.shape)):
                temp = np.squeeze(temp)

            # Get missing value
            missing_value = None
            if hasattr(self._file_handler.variables[var_name], "missing_value"):
                missing_value = getattr(
                    self._file_handler.variables[var_name], "missing_value")
            elif hasattr(self._file_handler.variables[var_name], "_FillValue"):
                missing_value = getattr(
                    self._file_handler.variables[var_name], "_FillValue")

            # Fill missing values
            if isinstance(temp, np.ma.MaskedArray):
                temp = np.ma.filled(temp, fill_value=self.nullValue)
            else:
                if missing_value is not None:
                    if np.can_cast(missing_value, temp.dtype):
                        try:
                            missing_value = temp.dtype.type(missing_value)
                        except Exception as e:
                            print(f"Type Casting Error: {str(e)}")
                    else:
                        missing_value = None
                if missing_value is not None:
                    temp = np.where(temp == missing_value,
                                    self.nullValue, temp)
        else:
            raise KeyError(
                f"Item {var_name} not in NetCDF variables: {self._file_handler.variables.keys()}")

        # Copy data to buffer
        if temp.ndim == 2:
            zsize = 0
            ysize, xsize = temp.shape
        elif temp.ndim == 3:
            zsize, ysize, xsize = temp.shape
            zsize = slice(zsize)

        # temp = (temp / temp) * tile_idx

        if self.invert_y:
            buf_arr[zsize, :ysize, :xsize] = np.flip(
                temp, axis=flip_axis).astype(self.data_type)
            return buf_arr, ti, (ty - tj)
        else:
            buf_arr[zsize, :ysize, :xsize] = temp.astype(self.data_type)
            return buf_arr, ti, tj

    @supported_libs.RequireLib("netcdf")
    def __exit__(self, *args):

        if self._file_handler is None:
            raise ValueError("file_handler could not be located")
        if supported_libs.HAS_PYDAP:
            if not isinstance(self._file_handler, (nc.Dataset, nc.MFDataset, Pydap2NC)):
                raise TypeError(
                    "file_handler should be an instance of nc.Dataset or Pydap2NC")
        else:
            if not isinstance(self._file_handler, (nc.Dataset, nc.MFDataset)):
                raise TypeError(
                    "file_handler should be an instance of nc.Dataset")

        if not self.is_thredds and not self.use_pydap:
            if self._file_handler.isopen():
                self._file_handler.close()

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "<geostack.readers.%s>" % self.__class__.__name__


class GDAL_Handler(DataHandler):

    def __init__(self, fileName=None, base_type: np.dtype = np.float32,
                 data_type: np.dtype = np.float32):
        # if the filename is a path to a file, ensure it's an absolute path
        # doesn't check for a valid extension as gdal supports an extensive
        # list of file types
        if fileName is not None:
            if isinstance(fileName, str):
                if pth.exists(fileName):
                    fileName = pth.abspath(fileName)
        self._file_name = fileName
        self._file_handler = None
        self.is_thredds = False
        self.use_pydap = False
        self.invert_y = False
        self.data_type = data_type
        self.base_type = base_type
        self.nullValue = raster.getNullValue(data_type)
        self.layers = []

    @supported_libs.RequireLib("gdal")
    def reader(self, *args, **kwargs):
        if args:
            if isinstance(args[0], bool):
                self.is_thredds = args[0]
        elif kwargs:
            if 'thredds' in kwargs:
                self.is_thredds = kwargs['thredds']

        if self._file_name is None:
            raise ValueError("file_name cannot be None")

        if isinstance(self._file_name, str):
            # patch file path (when needed) for gdal virtual file system
            if self.is_thredds:
                if 'vsicurl' not in self._file_name:
                    self._file_name = f"/vsicurl/{self._file_name}"
                    if self._file_name.endswith('tar') or self._file_name.endswith('tgz'):
                        raise ValueError(
                            "Compressed archives require full path in the archive")
            else:
                if 'vsicurl' in self._file_name:
                    self.is_thredds = True
            self._file_handler = gdal.Open(self._file_name)
        elif isinstance(self._file_name, gdal.Dataset):
            # assign to file_handler, when file_name is gdal.Dataset
            self._file_handler = self._file_name
            # change file_name to path of file
            self._file_name = self._file_name.GetDescription()

        # checks if gdal managed to open file
        # handles cases where the file extension is not supported by gdal
        if self._file_handler is None or isinstance(self._file_handler, str):
            raise ValueError(f"Unable to open file {self._file_name}")

        # get geotransform
        geotransform = self._file_handler.GetGeoTransform()

        # get projection wkt string as proj4 string
        if kwargs.get("read_projection") is not None:
            if kwargs.get('read_projection'):
                try:
                    projStr = return_proj4(self._file_handler.GetDescription())
                except AttributeError:
                    projStr = return_proj4(self._file_handler)
            else:
                projStr = ""
        else:
            try:
                projStr = return_proj4(self._file_handler.GetDescription())
            except AttributeError:
                projStr = return_proj4(self._file_handler)

        if geotransform[5] < 0:
            self.invert_y = True
        else:
            self.invert_y = False

        # Dummy time variable
        time = self.time(None)

        nx = self._file_handler.RasterXSize
        ny = self._file_handler.RasterYSize

        self.layers = get_layers(kwargs.get("layers", -1),
                                 self._file_handler.RasterCount)
        # add 1 as bands in gdal starts from 1
        self.layers = [i + 1 for i in self.layers]

        nz = len(self.layers)
        hx = geotransform[1]
        hy = abs(geotransform[5])
        hz = 1
        ox = geotransform[0]
        if self.invert_y:
            oy = geotransform[3] + \
                self._file_handler.RasterYSize * geotransform[5]
        else:
            oy = geotransform[3]
        oz = 0.0
        return nx, ny, nz, hx, hy, hz, ox, oy, oz, projStr.encode("utf-8"), time

    def writer(self, fileName: str, jsonConfig: Union[str, Dict]):
        writer_config = json.loads(jsonConfig)
        raise NotImplementedError("writer not yet implemented")

    @staticmethod
    def compute_bounds(ti: int, tj: int, nx: int, ny: int, tileSize: int) -> Tuple[float, float, int, int]:
        xoff = min(ti * tileSize, nx)
        y_end = ny - min(tj * tileSize, ny)
        y_start = ny - min((tj + 1) * tileSize, ny)
        yoff = min(y_start, y_end)
        xsize = min((ti + 1) * tileSize, nx) - xoff
        ysize = abs(y_start - y_end)

        return xoff, yoff, xsize, ysize

    @supported_libs.RequireLib("gdal")
    def check_handler(self):
        if not isinstance(self, GDAL_Handler):
            raise TypeError("Unable to understand the input class instance")

        if self._file_handler is None:
            raise ValueError("file_handler could not be located")

        if not isinstance(self._file_handler, gdal.Dataset):
            raise TypeError("file_handler is not an instance of gdal.Dataset")

        if self._file_handler.GetDescription() != self._file_name:
            if self._file_name not in self._file_handler.GetDescription():
                raise ValueError(
                    "Mismatch between file_name and description of file in gdal.Dataset")

    @supported_libs.RequireLib("gdal")
    def setter(self, ti: numbers.Integral, tj: numbers.Integral,
               tx: numbers.Integral, ty: numbers.Integral,
               varname: str, raster_index: numbers.Integral) -> Tuple[np.ndarray, int, int]:

        byteorders = {"little": "<", "big": ">"}
        array_modes = {gdalconst.GDT_Int16: ("%si2" % byteorders[sys.byteorder]),
                       gdalconst.GDT_UInt16: ("%su2" % byteorders[sys.byteorder]),
                       gdalconst.GDT_Int32: ("%si4" % byteorders[sys.byteorder]),
                       gdalconst.GDT_UInt32: ("%su4" % byteorders[sys.byteorder]),
                       gdalconst.GDT_Float32: ("%sf4" % byteorders[sys.byteorder]),
                       gdalconst.GDT_Float64: ("%sf8" % byteorders[sys.byteorder]),
                       gdalconst.GDT_CFloat32: ("%sf4" % byteorders[sys.byteorder]),
                       gdalconst.GDT_CFloat64: ("%sf8" % byteorders[sys.byteorder]),
                       gdalconst.GDT_Byte: ("%sb" % byteorders[sys.byteorder])}

        # raster_index ignored as all data is read

        # Check handler
        self.check_handler()

        # Get type of band 1
        raster_band = self._file_handler.GetRasterBand(1)
        gdal_to_npy = dataset.gdal_dtype_to_numpy(raster_band.DataType)

        # Get missing value
        missing_value = raster_band.GetNoDataValue()
        if missing_value is not None:
            try:
                if not raster_band.DataType < 6:
                    missing_value = gdal_to_npy(missing_value)
                else:
                    if (missing_value - gdal_to_npy(missing_value)) == 0:
                        missing_value = gdal_to_npy(missing_value)
                    else:
                        missing_value = None
            except Exception as _:
                print(f"WARNING: GDAL missing value '{missing_value}'" +
                      f" cannot be converted to {gdal_to_npy.__name__}")
                missing_value = None

        # Map or upgrade gdal data to Raster type
        if gdal_to_npy not in [np.float32, np.float64, np.uint32]:
            _temp_type = GDAL_Handler.data_type_compliance(gdal_to_npy)
        else:
            _temp_type = gdal_to_npy

        if _temp_type != self.data_type:
            _temp_type = self.data_type
            # raise TypeError("Mismatch between data type of raster band and class instance")

        # Create empty buffer
        tile_size = raster.TileSpecifications().tileSize
        buf_arr = np.full((len(self.layers), tile_size, tile_size),
                          self.nullValue, dtype=self.data_type)

        # Get data bounds
        xoff, yoff, xsize, ysize = GDAL_Handler.compute_bounds(ti, tj,
                                                               self._file_handler.RasterXSize,
                                                               self._file_handler.RasterYSize,
                                                               tile_size)

        # Read tile from data set
        for idx, k in enumerate(self.layers, 0):
            raster_band = self._file_handler.GetRasterBand(k)

            # Read data
            temp = raster_band.ReadRaster(xoff=xoff,
                                          yoff=yoff,
                                          xsize=xsize,
                                          ysize=ysize,
                                          buf_type=raster_band.DataType)
            temp = np.frombuffer(
                temp, dtype=array_modes[raster_band.DataType]).reshape((ysize,
                                                                        xsize))

            # Check and patch nodata
            if missing_value is not None:
                temp = np.where(temp == missing_value, self.nullValue, temp)

            # Copy data to buffer
            ysize, xsize = temp.shape
            if self.invert_y:
                buf_arr[idx, :ysize, :xsize] = temp[::-1, :].astype(_temp_type)
            else:
                buf_arr[idx, :ysize, :xsize] = temp[:, :].astype(_temp_type)

        del(temp, raster_band)
        return buf_arr, ti, tj

    def time(self, *args):
        return 0.0

    @staticmethod
    def data_type_compliance(input_type: np.dtype) -> np.dtype:
        if input_type in [np.uint8, np.uint16]:
            return np.uint32
        elif input_type in [np.int16, np.int32]:
            return np.float32
        else:
            raise TypeError("No mapping for input data type")

    @supported_libs.RequireLib("gdal")
    def __exit__(self, *args):
        if self._file_handler is not None:
            if isinstance(self._file_handler, gdal.Dataset):
                del self._file_handler
            else:
                raise TypeError(
                    "file_handler is not an instance of gdal.Dataset")
        else:
            raise ValueError("file_handler could not be located")

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "<geostack.readers.%s>" % self.__class__.__name__


class XR_Handler(DataHandler):

    def __init__(self, fileName: Optional[Union[Any, str]] = None,
                 base_type: np.dtype = np.float32,
                 variable_map: Optional[Dict] = None,
                 data_type: np.dtype = np.float32):
        # if the filename is a file, ensure it's an absolute path
        if fileName is not None:
            if isinstance(fileName, str) and 'dodsC' not in fileName:
                fileName = pth.abspath(
                    fileName) if pth.exists(fileName) else None
        self._file_name = fileName
        self._file_handler = None
        self._time_handler = None
        self.is_thredds = False
        self.use_pydap = False
        self.invert_y = False
        self.data_type = data_type
        self.base_type = base_type
        self.nullValue = raster.getNullValue(data_type)
        self._dims = deque()

        # backward compatibility
        if version.major >= 0 and version.minor >= 1:
            if version.patch > 14:
                if variable_map is None:
                    raise ValueError("Mapping for variable should be provided")
            self._variable_map = variable_map

    @supported_libs.RequireLib("xarray")
    def reader(self, thredds: bool = False,
               use_pydap: bool = False, **kwargs) -> Tuple:
        # set thredds flag when provided
        self.is_thredds = thredds

        if self._file_name is None:
            raise ValueError("file_name cannot be None")
        # open file without decoding time
        if isinstance(self._file_name, str):
            self._file_handler = xr.open_dataset(self._file_name,
                                                 decode_cf=False,
                                                 decode_times=False,
                                                 decode_coords={"grid_mapping": "all"})
        elif isinstance(self._file_name, bytes):
            self._file_handler = xr.open_dataset(self._file_name.decode(),
                                                 decode_cf=False,
                                                 decode_times=False,
                                                 decode_coords={"grid_mapping": "all"})
        elif isinstance(self._file_name, list):
            _file_list = [item.decode() if isinstance(item, bytes) else item
                          for item in self._file_name]
            if len(_file_list) > 1:
                self._file_handler = xr.open_mfdataset(_file_list,
                                                       decode_coords={"grid_mapping": "all"})
            else:
                self._file_handler = xr.open_dataset(_file_list[0],
                                                     decode_coords={"grid_mapping": "all"})

        if isinstance(self._file_name, xr.Dataset):
            # assign file_name to file_handler when input is xr.Dataset
            self._file_handler = self._file_name
            # change file name to path of file
            try:
                source_file = self._file_handler._file_obj._filename
            except AttributeError:
                source_file = self._file_handler.encoding['source']

            self._file_name = source_file

        if self._file_handler is None:
            raise ValueError("file_handler cannot be identified")

        if hasattr(self._file_handler, 'time'):
            self._time_handler = RasterTime(
                getattr(self._file_handler, 'time'))

        # get projection information from the file if available
        projStr = ""
        if 'crs' in self._file_handler.variables:
            if hasattr(self._file_handler.variables['crs'], "crs_wkt"):
                projStr = utils.proj4_from_wkt(
                    self._file_handler.variables['crs'].crs_wkt)
            elif hasattr(self._file_handler.variables['crs'], "spatial_ref"):
                projStr = utils.proj4_from_wkt(
                    self._file_handler.variables['crs'].getncattr("spatial_ref"))

        # get dimensions of the file
        if 'lon' in self._file_handler.dims:
            lon = self._file_handler.variables['lon']
            nx = self._file_handler.dims['lon']
            self._dims.append("lon")
        elif 'longitude' in self._file_handler.dims:
            lon = self._file_handler.variables['longitude']
            nx = self._file_handler.dims['longitude']
            self._dims.append("longitude")
        elif 'x' in self._file_handler.dims:
            lon = self._file_handler.variables['x']
            nx = self._file_handler.dims['x']
            self._dims.append("x")
        else:
            raise KeyError("lon/longitude/x not found in file dimensions")

        if 'lat' in self._file_handler.dims:
            lat = self._file_handler.variables['lat']
            ny = self._file_handler.dims['lat']
            self._dims.appendleft("lat")
        elif 'latitude' in self._file_handler.dims:
            lat = self._file_handler.variables['latitude']
            ny = self._file_handler.dims['latitude']
            self._dims.appendleft("latitude")
        elif 'y' in self._file_handler.dims:
            lat = self._file_handler.variables['y']
            ny = self._file_handler.dims['y']
            self._dims.appendleft("y")
        else:
            raise KeyError("lat/latitude/y not found in file dimensions")

        # add support for third dimension
        # get the third dimension to map
        layer_dim_name = kwargs.get("dims")
        if layer_dim_name is None:
            layer_dim_name = "time"
        else:
            current_size = len(self._dims)
            if not isinstance(layer_dim_name, (tuple, list)):
                raise TypeError(
                    f"Dimensions iterable {layer_dim_name} should be a tuple or list")

            if len(layer_dim_name) < current_size:
                raise ValueError(
                    f"Dimension tuple {layer_dim_name} should be atleast {tuple(self._dims)}")

            invalid_dims = []
            for i in range(current_size):
                offset = len(layer_dim_name) - current_size
                if layer_dim_name[i + offset] != self._dims[i]:
                    invalid_dims.append(layer_dim_name[i + offset])
            if len(invalid_dims) > 0:
                raise ValueError(
                    f"Dimension tuple {layer_dim_name} is not valid")
            for item in layer_dim_name:
                if item not in self._dims:
                    self._dims.appendleft(item)
            layer_dim_name = self._dims[0]

        layer_dim_size = self._file_handler.dims.get(layer_dim_name)
        if layer_dim_size is None:
            layer_dim_size = 1
        self.layers = get_layers(kwargs.get("layers", -1), layer_dim_size)
        self.layers = sorted(self.layers)
        nz = len(self.layers)
        if nz > 1:
            hz = self.layers[1] - self.layers[0]
        else:
            hz = 1
        oz = float(self.layers[0])

        time = self.time(0)
        # compute dimension input for instantiating raster.Raster
        hx = float(lon[1] - lon[0])
        hy = float(lat[1] - lat[0])

        min_x = min(lon.values[0], lon.values[-1])
        if isinstance(min_x, np.ma.MaskedArray):
            ox = self.data_type(min.data)
        elif isinstance(min_x, np.ndarray):
            ox = self.data_type(min_x)
        elif isinstance(min_x, numbers.Real):
            ox = self.data_type(min_x)

        ox -= 0.5 * hx

        min_y = min(lat.values[0], lat.values[-1])
        if isinstance(min_y, np.ndarray):
            oy = self.data_type(min_y)
        elif isinstance(min_y, np.ma.MaskedArray):
            oy = self.data_type(min_y.data)
        elif isinstance(min_y, numbers.Real):
            oy = self.data_type(min_y)

        if hy < 0:
            self.invert_y = True
            hy = abs(hy)
        oy -= 0.5 * hy

        return nx, ny, nz, hx, hy, hz, ox, oy, oz, projStr.encode("utf-8"), time

    def time(self, tidx: numbers.Integral) -> numbers.Real:
        self.check_handler()
        time = 0.0
        if self._time_handler is not None:
            time = self.time_from_index(tidx)
        return time

    def set_time_bounds(self, start_time: Union[numbers.Real, str],
                        end_time: Union[numbers.Real, str],
                        dt_format: Optional[str] = None):
        if self._time_handler is not None:
            self._time_handler.set_bounds(start_time, end_time)
        else:
            raise RuntimeError("No time handle has been set")

    def time_from_index(self, index: numbers.Integral) -> numbers.Real:
        if self._time_handler is not None:
            return self._time_handler.time_from_index(index)
        else:
            raise RuntimeError("No time handle has been set")

    def index_from_time(self, timestamp: Union[datetime, numbers.Real]) -> numbers.Integral:
        if self._time_handler is not None:
            return self._time_handler.get_index(timestamp)
        else:
            raise RuntimeError("No time handle has been set")

    def get_max_time_index(self):
        if self._time_handler is not None:
            return self._time_handler.get_max_time_index()
        else:
            raise RuntimeError("No time handle has been set")

    def get_left_index(self, timestamp: Union[datetime, numbers.Real]) -> numbers.Integral:
        if self._time_handler is not None:
            return self._time_handler.get_left_index(timestamp)
        else:
            raise RuntimeError("No time handle has been set")

    def get_right_index(self, timestamp: Union[datetime, numbers.Real]) -> numbers.Integral:
        if self._time_handler is not None:
            return self._time_handler.get_right_index(timestamp)
        else:
            raise RuntimeError("No time handle has been set")

    @supported_libs.RequireLib("xarray")
    def writer(self, jsonConfig: Union[Dict, str]):
        writer_config = json.loads(jsonConfig)
        raise NotImplementedError("writer not yet implemented")

    @supported_libs.RequireLib("xarray")
    def check_handler(self):
        if not isinstance(self, XR_Handler):
            raise TypeError("Unable to understand the input class instance")

        if self._file_handler is None:
            raise ValueError("file_handler cannot be identified")

        if not isinstance(self._file_handler, xr.Dataset):
            raise TypeError("file_handler is of incorrect type")

        try:
            source_file = self._file_handler._file_obj._filename
        except AttributeError:
            source_file = self._file_handler.encoding['source']

        if source_file != self._file_name:
            raise ValueError("Mismatch between filepath and filename")

    @supported_libs.RequireLib("xarray")
    def setter(self, ti: numbers.Integral, tj: numbers.Integral,
               tx: numbers.Integral, ty: numbers.Integral,
               varname: str, tidx: numbers.Integral) -> Tuple[np.ndarray, int, int]:
        # Check handler
        self.check_handler()

        # Create empty buffer
        tile_size = raster.TileSpecifications().tileSize
        buf_arr = np.full((len(self.layers), tile_size, tile_size), self.nullValue,
                          dtype=self.data_type)

        # Get dimensions
        if 'lon' in self._file_handler.dims:
            lon = self._file_handler.dims['lon']
        elif 'longitude' in self._file_handler.dims:
            lon = self._file_handler.dims['longitude']
        elif 'x' in self._file_handler.dims:
            lon = self._file_handler.dims['x']
        else:
            raise KeyError("lon/longitude/x doesn't exist in the file")

        if 'lat' in self._file_handler.dims:
            lat = self._file_handler.dims['lat']
        elif 'latitude' in self._file_handler.dims:
            lat = self._file_handler.dims['latitude']
        elif 'y' in self._file_handler.dims:
            lat = self._file_handler.dims['y']
        else:
            raise KeyError("lat/latitude/y doesn't exist in the file")

        x_start = ti * tile_size
        x_end = min(min((ti + 1), tx) * tile_size, lon)

        if self.invert_y:
            y_start = lat - min(min((tj + 1), ty) * tile_size, lat)
            y_end = lat - tj * tile_size
        else:
            y_start = tj * tile_size
            y_end = min(min((tj + 1), ty) * tile_size, lat)

        # handle reading 3d Data
        if len(self.layers) > 1:
            # when more than 1 layers to be read
            # here, tidx value is ignored
            z_slice = slice(self.layers[0],
                            self.layers[-1] + 1,
                            self.layers[1] - self.layers[0])
            flip_axis = 1
        else:
            # when only one layer (spatial layer, stepping in time)
            z_slice = tidx
            flip_axis = 0

        # Get variable name
        if isinstance(varname, str):
            var_name = varname
        elif isinstance(varname, bytes):
            var_name = varname.decode()

        var_name = self._variable_map.get(var_name, var_name)

        # Get data
        if var_name in self._file_handler.data_vars:
            var_shape = self._file_handler.data_vars[var_name].shape
            var_dims = self._file_handler.data_vars[var_name].dims
            # Check dimensions
            if len(var_dims) == 3:
                if var_dims[-2:] == tuple(self._dims)[-2:][::-1]:
                    temp = self._file_handler.variables[var_name][z_slice,
                                                                  x_start:x_end,
                                                                  y_start:y_end].to_masked_array()
                    temp = np.swapaxes(temp, -1, -2)
                elif var_dims[-2:] == tuple(self._dims)[-2:]:
                    temp = self._file_handler.data_vars[var_name][z_slice,
                                                                  y_start:y_end,
                                                                  x_start:x_end].to_masked_array()
            elif len(var_dims) == 2:
                if var_dims == tuple(self._dims)[::-1]:
                    temp = self._file_handler.variables[var_name][x_start:x_end,
                                                                  y_start:y_end].to_masked_array()
                    temp = np.swapaxes(temp, -1, -2)
                elif var_dims == tuple(self._dims):
                    temp = self._file_handler.data_vars[var_name][y_start:y_end,
                                                                  x_start:x_end].to_masked_array()
            else:
                raise RuntimeError(
                    "Only 2D or 2D + time is currently supported")

            if temp.ndim > 2:
                temp = np.squeeze(temp)
            # Patch data
            temp = np.ma.filled(temp, fill_value=self.nullValue)
        else:
            raise KeyError(
                f"Item {var_name} not in NetCDF variables: {self._file_handler.data_vars.keys()}")

        # Copy data to buffer
        if temp.ndim == 2:
            zsize = 0
            ysize, xsize = temp.shape
        elif temp.ndim == 3:
            zsize, ysize, xsize = temp.shape
            zsize = slice(zsize)

        if self.invert_y:
            buf_arr[zsize, :ysize, :xsize] = np.flip(
                temp, axis=flip_axis).astype(self.data_type)
            return buf_arr, ti, (ty - tj)
        else:
            buf_arr[zsize, :ysize, :xsize] = temp[:, :].astype(self.data_type)
            return buf_arr, ti, tj

    @supported_libs.RequireLib("xarray")
    def __exit__(self, *args):
        if self._file_handler is None:
            raise ValueError("file_handler could not be located")

        if not isinstance(self._file_handler, xr.Dataset):
            raise TypeError("file_handler should be an instance of xr.Dataset")
        if not self.is_thredds and not self.use_pydap:
            self._file_handler.close()

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "<geostack.readers.%s>" % self.__class__.__name__


class RIO_Handler(DataHandler):

    def __init__(self, fileName=None, base_type: np.dtype = np.float32,
                 data_type: np.dtype = np.float32):
        # if the filename is a file, ensure it's an absolute path
        if fileName is not None:
            if isinstance(fileName, str):
                if pth.exists(fileName):
                    fileName = pth.abspath(fileName)
        self._file_name = fileName
        self._file_handler = None
        self.is_thredds = False
        self.use_pydap = False
        self.invert_y = False
        self.data_type = data_type
        self.base_type = base_type
        self.nullValue = raster.getNullValue(data_type)
        self.layers = []

    @supported_libs.RequireLib("rasterio")
    def reader(self, *args, **kwargs) -> Tuple:

        # Check if using remote file
        if args:
            if isinstance(args[0], bool):
                self.is_thredds = args[0]
        elif kwargs:
            if 'thredds' in kwargs:
                self.is_thredds = kwargs['thredds']

        if self._file_name is None:
            raise ValueError("file_name cannot be None")

        # Patch path (when needed) for gdal virtual file system
        if isinstance(self._file_name, (str, bytes)):
            if isinstance(self._file_name, bytes):
                _file_name = self._file_name.decode()
            elif isinstance(self._file_name, str):
                _file_name = self._file_name

            if self.is_thredds:
                if 'vsicurl' not in _file_name:
                    self._file_name = f"/vsicurl/{_file_name}"
                    if _file_name.endswith('tar') or _file_name.endswith('tgz'):
                        raise ValueError(
                            "Compressed archives require full path in the archive")
            else:
                if 'vsicurl' in _file_name:
                    is_thredds = True
            self._file_handler = rio.open(_file_name, mode='r')
        elif isinstance(self._file_name, rio.DatasetReader):
            # assign file_name to file_handler when input is DatasetReader object
            self._file_handler = self._file_name
            # change file_name to path of file
            self._file_name = self._file_handler.files[0]

        # Get projection information from the file
        projStr = ""
        if hasattr(self._file_handler, 'crs'):
            if getattr(self._file_handler, 'crs') is not None:
                projStr = self._file_handler.crs.to_proj4()
            # workaround for rasterio crs code. geostack projection can't handle
            # rasterio proj4 string when it only has epsg code
            if 'epsg' in projStr:
                # use pyproj to transform wkt to proj4
                try:
                    import pyproj
                    projStr = pyproj.CRS.from_wkt(
                        self._file_handler.crs.to_wkt()).to_proj4()
                except ImportError:
                    projStr = ""

        if self._file_handler is None:
            raise ValueError("Unable to open file %s" % self._file_name)

        # Get geostranform
        geotransform = self._file_handler.get_transform()
        if geotransform[5] < 0:
            self.invert_y = True
        else:
            self.invert_y = False

        time = self.time(None)

        # Compute dimension input for instantiating raster.Raster
        nx = self._file_handler.width
        ny = self._file_handler.height

        self.layers = get_layers(kwargs.get("layers", -1),
                                 self._file_handler.count)
        # add 1 as bands in gdal starts from 1
        self.layers = [i + 1 for i in self.layers]

        nz = len(self.layers)

        hx = geotransform[1]
        hy = abs(geotransform[5])
        hz = 1
        ox = geotransform[0]
        oy = geotransform[3] + ny * geotransform[5]
        oz = 0.0

        return nx, ny, nz, hx, hy, hz, ox, oy, oz, projStr.encode("utf-8"), time

    @staticmethod
    def compute_bounds(ti: numbers.Integral, tj: numbers.Integral,
                       nx: numbers.Integral, ny: numbers.Integral,
                       tileSize: numbers.Integral) -> Tuple[float, float, int, int]:
        xoff = min(ti * tileSize, nx)
        y_end = ny - min(tj * tileSize, ny)
        y_start = ny - min((tj + 1) * tileSize, ny)
        yoff = min(y_start, y_end)
        xsize = min((ti + 1) * tileSize, nx) - xoff
        ysize = abs(y_start - y_end)

        return xoff, yoff, xsize, ysize

    def writer(self):
        raise NotImplementedError()

    @supported_libs.RequireLib("rasterio")
    def check_handler(self):
        if not isinstance(self, RIO_Handler):
            raise TypeError("Unable to understand the input class instance")

        if self._file_handler is None:
            raise ValueError("file_handler could not be located")

        if not isinstance(self._file_handler, rio.DatasetReader):
            raise TypeError(
                "file_handler is not an instance of rio.DatasetReader")

        if self._file_handler.files[0] != self._file_name:
            if self._file_name not in self._file_handler.files[0]:
                raise ValueError(
                    "Mismatch between file_name and description of file in rio.DatasetReader")

    @supported_libs.RequireLib("rasterio")
    def setter(self, ti: numbers.Integral, tj: numbers.Integral,
               tx: numbers.Integral, ty: numbers.Integral,
               varname: str, raster_index: numbers.Integral) -> Tuple[np.ndarray, int, int]:

        # raster_index ignored as all data is read

        # Check handler
        self.check_handler()

        # Create empty buffer
        tile_size = raster.TileSpecifications().tileSize
        buf_arr = np.full((len(self.layers), tile_size, tile_size), self.nullValue,
                          dtype=self.data_type)

        # Get data bounds
        xoff, yoff, xsize, ysize = RIO_Handler.compute_bounds(ti, tj,
                                                              self._file_handler.width,
                                                              self._file_handler.height,
                                                              tile_size)

        # Get bands
        raster_band = self._file_handler.read(indexes=self.layers,
                                              window=Window(
                                                  xoff, yoff, xsize, ysize),
                                              out_dtype=self.data_type)

        # Get missing value
        missing_value = self._file_handler.nodatavals[0]

        # Get raster data type
        raster_dtype = np.dtype(self._file_handler.dtypes[0]).type
        if missing_value is not None:
            try:
                if raster_dtype in [np.float32, np.float64, np.complex_,
                                    np.complex64, np.complex128]:
                    missing_value = raster_dtype(missing_value)
                else:
                    if (missing_value - raster_dtype(missing_value)) == 0:
                        missing_value = raster_dtype(missing_value)
                    else:
                        missing_value = None
            except Exception as _:
                print(f"WARNING: Rasterio missing value '{missing_value}' " +
                      f"cannot be converted to {raster_dtype.__name__}",)
                missing_value = None

        # Check and patch data
        if missing_value is not None:
            raster_band = np.where(
                raster_band == missing_value, self.nullValue, raster_band)

        # Copy data to buffer
        zsize, ysize, xsize = raster_band.shape
        if self.invert_y:
            buf_arr[:zsize, :ysize, :xsize] = raster_band[:, ::-1, :]
            return buf_arr, ti, tj
        else:
            buf_arr[:zsize, :ysize, :xsize] = raster_band[:, :, :]
            return buf_arr, ti, tj

    @supported_libs.RequireLib("rasterio")
    def __exit__(self, *args):
        if self._file_handler is not None:
            if isinstance(self._file_handler, rio.DatasetReader):
                self._file_handler.close()
            else:
                raise TypeError(
                    "file_handler is not an instance of rio.DatasetReader")
        else:
            raise ValueError("file_handler could not be located")

    def time(self, *args):
        return 0.0

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "<geostack.readers.%s>" % self.__class__.__name__
