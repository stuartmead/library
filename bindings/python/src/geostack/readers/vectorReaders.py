# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from __future__ import annotations
import os
import sys
import os.path as pth
import numpy as np
from .. import dataset
from ..dataset import supported_libs
import warnings
import numbers

if supported_libs.HAS_GDAL:
    from osgeo import ogr
    os.environ['GDAL_CACHEMAX'] = "100"

if supported_libs.HAS_GPD:
    import geopandas as gpd
    import pandas as pd

if supported_libs.HAS_PYSHP:
    import shapefile

if supported_libs.HAS_FIONA:
    import fiona

from .. import core, io, vector
import time

__all__ = ['from_geopandas', "from_pyshp", "from_ogr", "from_fiona"]

@supported_libs.RequireLib("geopandas")
def get_column_names(this):
    if isinstance(this, pd.DataFrame):
        column_names = this.columns
    elif isinstance(this, pd.Series):
        column_names = this.keys()
    return column_names

@supported_libs.RequireLib("fiona")
def from_fiona(file_path: str, dtype: np.dtype = np.float32):
    """Create Vector object using fiona.

    Parameters
    ----------
    file_path: str/fiona.Collection
        path of the file to be read or a fiona Collection object
    dtype: np.dtype
        data type for the vector object to be created

    Returns
    -------
    vec: vector.Vector
        a vector object instantiated from fiona.Collection
    """

    def conform_type(inp):
        if isinstance(inp, float):
            if dtype == np.float64:
                return dtype(inp)
            else:
                return inp
        else:
            return inp

    if isinstance(file_path, str):
        datain = fiona.open(file_path, mode="r")
    elif isinstance(file_path, fiona.Collection):
        datain = file_path

    vec = vector.Vector(dtype=dtype)

    vec.setProjectionParameters(
        core.ProjectionParameters.from_wkt(datain.crs_wkt))

    for item in datain.keys():
        geom = datain[item]['geometry']
        props = datain[item]['properties']
        if geom['type'] == 'LineString':
            idx = vec.addLineString(geom['coordinates'])
            for prop in props:
                if props[prop] is None:
                    vec.setProperty(idx, prop, "")
                else:
                    vec.setProperty(idx, prop, conform_type(props[prop]))
        elif geom['type'] == "Point":
            idx = vec.addPoint(geom['coordinates'])
            for prop in props:
                if props[prop] is None:
                    vec.setProperty(idx, prop, "")
                else:
                    vec.setProperty(idx, prop, conform_type(props[prop]))
        elif geom['type'] in "Polygon":
            coords = np.squeeze(geom['coordinates'])
            if coords.ndim == 1:
                idx = vec.addPolygon([np.squeeze(_coords) for _coords in coords])
                for prop in props:
                    if props[prop] is None:
                        vec.setProperty(idx, prop, "")
                    else:
                        vec.setProperty(idx, prop, conform_type(props[prop]))
            else:
                idx = vec.addPolygon([coords])
                for prop in props:
                    if props[prop] is None:
                        vec.setProperty(idx, prop, "")
                    else:
                        vec.setProperty(idx, prop, conform_type(props[prop]))
        elif geom['type'] == "MultiPolygon":
            for coords in geom['coordinates']:
                _coords = np.squeeze(coords)
                if _coords.ndim == 1:
                    _multi_poly = []
                    for __coords in _coords:
                        _multi_poly.append(np.squeeze(__coords))
                    idx = vec.addPolygon(_multi_poly)
                    for prop in props:
                        if props[prop] is None:
                            vec.setProperty(idx, prop, "")
                        else:
                            vec.setProperty(idx, prop, conform_type(props[prop]))
                else:
                    idx = vec.addPolygon([_coords])
                    for prop in props:
                        if props[prop] is None:
                            vec.setProperty(idx, prop, "")
                        else:
                            vec.setProperty(idx, prop, conform_type(props[prop]))

    if isinstance(file_path, str):
        datain.close()
    return vec

@supported_libs.RequireLib("geopandas")
def from_geopandas(file_path: str, dtype: np.dtype=np.float32):
    """Create Vector object using geopandas.

    Parameters
    ----------
    file_path: str/geopandas.GeoDataFrame
        path of the file to be read or a geopandas GeoDataFrame object
    dtype: np.dtype
        data type for the vector object to be created

    Returns
    -------
    vec: vector.Vector
        a vector object created from geopandas GeoDataFrame
    """
    if isinstance(file_path, str):
        datain = gpd.read_file(file_path)
    elif isinstance(file_path, (gpd.GeoDataFrame, gpd.GeoSeries)):
        datain = file_path

    try:
        # try with the native wkt reader
        column_names = [item for item in datain.columns if item != "geometry"]
        vec = vector.Vector()
        idx = io.parseStrings(vec, map(lambda s: s.wkt, datain.geometry))
        props_map = map(lambda s: vec.setPropertyValues(s, datain[s].values), column_names)

        while next(props_map, False) == None:
            pass
    except RuntimeError:
        # handle cases which are not yet supported in native reader
        vec = _from_geopandas(file_path, dtype)

    return vec

@supported_libs.RequireLib("geopandas")
def _from_geopandas(file_path: str, dtype: np.dtype=np.float32):
    """Create Vector object using geopandas.

    Parameters
    ----------
    file_path: str/geopandas.GeoDataFrame
        path of the file to be read or a geopandas GeoDataFrame object
    dtype: np.dtype
        data type for the vector object to be created

    Returns
    -------
    vec: vector.Vector
        a vector object created from geopandas GeoDataFrame
    """
    prop_type_map = {"string": str,
                     "int": int,
                     "double": np.float64 if dtype == np.float64 else float}

    if isinstance(file_path, str):
        datain = gpd.read_file(file_path)
    elif isinstance(file_path, (gpd.GeoDataFrame, gpd.GeoSeries)):
        datain = file_path

    nrows = datain.shape[0]

    vec = vector.Vector(dtype=dtype)

    if isinstance(datain, gpd.GeoSeries):
        if datain.dtype.name != "geometry":
            raise TypeError("geoseries should be of dtype `geometry`")

    for i in range(nrows):
        if isinstance(datain, gpd.GeoDataFrame):
            _obj = datain.iloc[i]
            props = {'string':{}, 'int':{}, 'double':{}}
            for item in get_column_names(_obj):
                if item != "geometry":
                    if isinstance(getattr(_obj, item), str):
                        if _obj.get(item) is not None:
                            props['string'][item] = _obj.get(item)
                        else:
                            props['string'][item] = ""
                    elif isinstance(getattr(_obj, item), int):
                        if _obj.get(item) is not None:
                            props['int'][item] = _obj.get(item)
                        else:
                            props['string'][item] = ""
                    elif isinstance(getattr(_obj, item), float):
                        if _obj.get(item) is not None:
                            props['double'][item] = _obj.get(item)
                        else:
                            props['string'][item] = ""
            _geom = getattr(_obj, 'geometry')
        elif isinstance(datain, gpd.GeoSeries):
            _geom = datain.iloc[i]
        if _geom.type == 'MultiPolygon':
            for j, _geo_obj in enumerate(_geom, 0):
                _geo_coords = _geo_obj.__geo_interface__['coordinates']
                _coords = np.squeeze(_geo_coords)
                if _coords.ndim == 1:
                    _multi_poly = []
                    for k in range(len(_coords)):
                        __coords = np.squeeze(_coords[k])
                        _multi_poly.append(__coords)
                    poly_idx = vec.addPolygon(_multi_poly)
                    if isinstance(datain, gpd.GeoDataFrame):
                        for _item in props:
                            if len(props[_item]) > 0:
                                prop_type = prop_type_map.get(_item)
                                for _prop in props[_item]:
                                    vec.setProperty(poly_idx, _prop, props[_item][_prop], propType=prop_type)
                else:
                    poly_idx = vec.addPolygon([_coords])
                    if isinstance(datain, gpd.GeoDataFrame):
                        for _item in props:
                            if len(props[_item]) > 0:
                                prop_type = prop_type_map.get(_item)
                                for _prop in props[_item]:
                                    vec.setProperty(poly_idx, _prop, props[_item][_prop], propType=prop_type)
        elif _geom.type == 'Polygon':
            _geo_coords = _geo_obj.__geo_interface__['coordinates']
            _coords = np.squeeze(_geo_coords)
            if _coords.ndim == 1:
                _coords = [np.squeeze(item) for item in _coords]
            else:
                _coords = [_coords]
            poly_idx = vec.addPolygon(_coords)
            if isinstance(datain, gpd.GeoDataFrame):
                for _item in props:
                    if len(props[_item]) > 0:
                        prop_type = prop_type_map.get(_item)
                        for _prop in props[_item]:
                            vec.setProperty(poly_idx, _prop, props[_item][_prop], propType=prop_type)
        elif _geom.type == "Point":
            idx = vec.addPoint([_geom.x, _geom.y])
            if isinstance(datain, gpd.GeoDataFrame):
                for _item in props:
                    if len(props[_item]) > 0:
                        prop_type = prop_type_map.get(_item)
                        for _prop in props[_item]:
                            vec.setProperty(idx, _prop, props[_item][_prop], propType=prop_type)
        elif _geom.type == "LineString":
            idx = vec.addLineString(list(_geo_obj))
            if isinstance(datain, gpd.GeoDataFrame):
                for _item in props:
                    if len(props[_item]) > 0:
                        prop_type = prop_type_map.get(_item)
                        for _prop in props[_item]:
                            vec.setProperty(idx, _prop, props[_item][_prop], propType=prop_type)

    # get projection information
    if hasattr(datain, "crs") and datain.crs is not None:
        proj_str = datain.crs.to_proj4()
        proj_params = core.ProjectionParameters.from_proj4(proj_str)
        vec.setProjectionParameters(proj_params)

    return vec

@supported_libs.RequireLib("gdal")
def from_ogr(file_path: str, dtype: np.dtype=np.float32):
    """Create Vector object using GDAL's OGR.

    Parameters
    ----------
    file_path: str/fiona.Collection
        path of the file to be read or a ogr.DataSource/ogr.Layer object
    dtype: np.dtype
        data type for the vector object to be created

    Returns
    -------
    vec: vector.Vector
        a vector object created from a ogr.DataSource/ ogr.Layer
    """

    prop_type_map = {"string": str,
                     "int": int,
                     "double": np.float64 if dtype == np.float64 else float}

    if isinstance(file_path, str):
        if pth.splitext(file_path)[-1].lower() == ".shp":
            driver = ogr.GetDriverByName("ESRI Shapefile")
        elif pth.splitext(file_path)[-1].lower() in [".json", ".geojson"]:
            driver = ogr.GetDriverByName("GeoJSON")
        datain = driver.Open(file_path)
    elif isinstance(file_path, ogr.DataSource):
        datain = file_path
    elif isinstance(file_path, ogr.Layer):
        datain = None

    if datain is not None:
        layer_count = datain.GetLayerCount()
    else:
        layer_count = 1

    #this part converts the vector information into geostack Vector object
    vec = vector.Vector(dtype=dtype)

    for i in range(layer_count):
        if datain is not None:
            data_layer = datain.GetLayerByIndex(i)
        elif datain is None and isinstance(file_path, ogr.Layer):
            data_layer = file_path
        layer_type = ogr.GeometryTypeToName(data_layer.GetGeomType())
        spatial_ref = data_layer.GetSpatialRef()
        for j, data_feature in enumerate(data_layer, 0):
            field_count = data_feature.GetFieldCount()
            props = {'string':{}, 'int':{}, 'double':{}}
            for k in range(field_count):
                field_def = data_feature.GetFieldDefnRef(k)
                if ogr.GetFieldTypeName(field_def.GetType()) == 'String':
                    if data_feature.GetField(k) is not None:
                        props['string'][field_def.GetName()] = data_feature.GetField(k)
                    else:
                        props['string'][field_def.GetName()] = ""
                elif ogr.GetFieldTypeName(field_def.GetType()) == 'Integer':
                    if data_feature.GetField(k) is not None:
                        props['int'][field_def.GetName()] = data_feature.GetField(k)
                    else:
                        props['string'][field_def.GetName()] = ""
                elif ogr.GetFieldTypeName(field_def.GetType()) == 'Real':
                    if data_feature.GetField(k) is not None:
                        props['double'][field_def.GetName()] = data_feature.GetField(k)
                    else:
                        props['string'][field_def.GetName()] = ""
            geom = data_feature.GetGeometryRef()
            if layer_type == "Point":
                idx = vec.addPoint(geom.GetPoint_2D())
                for _item in props:
                    if len(props[_item]) > 0:
                        prop_type = prop_type_map.get(_item)
                        for _prop in props[_item]:
                            vec.setProperty(idx, _prop, props[_item][_prop], propType=prop_type)
            elif layer_type == "Line String":
                idx = vec.addLineString(geom.GetPoints())
                for _item in props:
                    if len(props[_item]) > 0:
                        prop_type = prop_type_map.get(_item)
                        for _prop in props[_item]:
                            vec.setProperty(idx, _prop, props[_item][_prop], propType=prop_type)
            elif layer_type in ["Polygon", "Multi Polygon", "3D Polygon"]:
                poly = []
                for k in range(geom.GetGeometryCount()):
                    _geom = geom.GetGeometryRef(k)
                    _geom_count = _geom.GetGeometryCount()
                    if _geom_count > 0:
                        _poly = []
                        for n in range(_geom_count):
                            __geom = _geom.GetGeometryRef(n)
                            points = np.array(__geom.GetPoints())
                            _poly.append(points)
                        poly.append(_poly)
                    else:
                        points = np.array(_geom.GetPoints())
                        poly.append(points)

                for item in poly:
                    if isinstance(item, list):
                        poly_idx = vec.addPolygon(item)
                    else:
                        poly_idx = vec.addPolygon([item])
                    for _item in props:
                        if len(props[_item]) > 0:
                            prop_type = prop_type_map.get(_item)
                            for _prop in props[_item]:
                                vec.setProperty(poly_idx, _prop, props[_item][_prop], propType=prop_type)

    vec.setProjectionParameters(
        core.ProjectionParameters.from_wkt(spatial_ref.ExportToWkt()))

    if isinstance(file_path, str):
        del(datain)
        datain = None
    return vec

@supported_libs.RequireLib("pyshp")
def from_pyshp(file_path: str, dtype: np.dtype=np.float32):
    """Create Vector object using pyshp's shapefile.

    Parameters
    ----------
    file_path: str/fiona.Collection
        path of the file to be read or a shapefile.Reader object
    dtype: np.dtype
        data type for the vector object to be created

    Returns
    -------
    vec: vector.Vector
        a vector object created from a shapefile.Reader

    """
    prop_type_map = {"string": str,
                     "int": int,
                     "double": np.float64 if dtype == np.float64 else float}

    if isinstance(file_path, str):
        datain = shapefile.Reader(pth.realpath(file_path))
    elif isinstance(file_path, shapefile.Reader):
        datain = file_path

    if pth.exists(f"{datain.shapeName}.prj"):
        with open(f"{datain.shapeName}.prj", "r") as inp:
            proj_param = inp.read()
        shape_proj = core.ProjectionParameters.from_wkt(proj_param)
    else:
        shape_proj = None

    n_shapes = datain.numRecords
    data_fields = datain.fields

    vec = vector.Vector(dtype=dtype)

    for i in range(n_shapes):
        props = {'string':{}, 'int':{}, 'double':{}}
        for j, item in enumerate(data_fields[1:], 0):
            if item[1] == 'C':
                props['string'][item[0]] = datain.record(i=i)[j]
            elif item[1] == "N":
                props['int'][item[0]] = datain.record(i=i)[j]
            elif item[1] == 'F':
                props['double'][item[0]] = datain.record(i=i)[j]
        geom = datain.shape(i=i)
        geom_points = geom.points
        if datain.shapeTypeName == "POINT":
            idx = vec.addPoint(geom_points[0])
            for _item in props:
                if len(props[_item]) > 0:
                    prop_type = prop_type_map.get(_item)
                    for _prop in props[_item]:
                        vec.setProperty(idx, _prop, props[_item][_prop], propType=prop_type)
        elif datain.shapeTypeName == "POLYLINE":
            idx = vec.addLineString(geom_points)
            for _item in props:
                if len(props[_item]) > 0:
                    prop_type = prop_type_map.get(_item)
                    for _prop in props[_item]:
                        vec.setProperty(idx, _prop, props[_item][_prop], propType=prop_type)
        elif datain.shapeTypeName in ["POLYGON", "POLYGONZ"]:
            geom_parts = geom.parts
            for k in range(len(geom_parts)):
                if k == len(geom_parts)-1:
                    _coords = np.squeeze(geom_points[geom_parts[k]:len(geom_points)])
                else:
                    _coords = np.squeeze(geom_points[geom_parts[k]:geom_parts[k+1]])
                poly_idx = vec.addPolygon([_coords])
                for _item in props:
                    if len(props[_item]) > 0:
                        prop_type = prop_type_map.get(_item)
                        for _prop in props[_item]:
                            vec.setProperty(poly_idx, _prop, props[_item][_prop], propType=prop_type)

    if shape_proj is not None:
        vec.setProjectionParameters(shape_proj)
    if isinstance(file_path, str):
        datain.close()
    return vec

if __name__ == "__main__":

    file_path = "./aus-states-09072015.shp"
    start = time.time()
    vec = from_geopandas(file_path)
    end = time.time()
    print(f"Time taken by geopandas to geostack = (end - start)")

    start = time.time()
    vec = from_ogr(file_path)
    end = time.time()
    print(f"Time taken by ogr to geostack = {(end - start)}")

    start = time.time()
    vec = from_pyshp(file_path)
    end = time.time()
    print(f"Time taken by pyshp to geostack = (end - start)")

    out = io.vectorToGeoJson(vec)
    core.Json11.load(out).dumps("./test2.geojson")

    print(f"Time taken to write to geojson = {(time.time() - end)}")
