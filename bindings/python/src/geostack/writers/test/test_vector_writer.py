# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os
import sys
sys.path.insert(0, os.path.realpath('../../../'))

import pytest
from geostack import vector
from geostack.utils import get_epsg
from geostack.writers import vectorWriters
from geostack.dataset import supported_libs


@pytest.mark.geopandas
@pytest.mark.skipif(not supported_libs.HAS_GPD, reason="geopandas library is not installed")
def test_vector_gpd():
    import geopandas as gpd
    vec = vector.Vector()
    vec.addPoint([0, 0])
    vec.addLineString([[0, 0], [1, 0]])
    vec.setProjectionParameters(get_epsg(4326))
    obj = vectorWriters.to_geopandas(vec)

    assert isinstance(obj, gpd.GeoDataFrame)
