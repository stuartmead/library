# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import os.path as pth
import sqlite3
import numpy as np
from typing import Optional, Union, List
from functools import partial

from ..dataset.supported_libs import HAS_GPD, RequireLib

if HAS_GPD:
    import geopandas as gpd
    from shapely import wkt
    try:
        from geopandas.geodataframe import CRS
    except ImportError:
        from pyproj.crs import CRS
    import pandas as pd

from ..vector import vector
from ..io import vectorItemToGeoWKT
from ..gs_enums import GeometryType

__all__ = ["to_geopandas", "to_database"]

try:
    from dataclasses import dataclass

    @dataclass
    class DbFlags:
        Polygon: int = 0
        Point: int = 0
        LineString: int = 0
except ImportError:
    class DbFlags:
        __slots__ = ()
        Polygon: int = 0
        Point: int = 0
        LineString: int = 0


@RequireLib("geopandas")
@RequireLib("shapely")
def from_wkt(input_vector: "vector.Vector") -> Union[List[str], np.ndarray]:
    """convert a vector object to a list (or ndarray) of geowkt strings.

    Parameters
    ----------
    input_vector : vector.Vector
        an instance of a vector object

    Returns
    -------
    Union[List[str], np.ndarray]
        a list (or ndarray) of geowkt strings.
    """
    item_to_wkt = partial(vectorItemToGeoWKT, input_vector)
    if hasattr(gpd.GeoSeries, "from_wkt"):
        method = getattr(gpd.GeoSeries, "from_wkt")
        geom_arr = list(map(item_to_wkt, input_vector.getGeometryIndexes()))
    else:
        method = getattr(gpd, "GeoSeries")
        geoms = map(wkt.loads, map(
            item_to_wkt, input_vector.getGeometryIndexes()))
        geom_arr = np.array(list(geoms), dtype=object)

    out = method(geom_arr)
    return out


@RequireLib("geopandas")
def to_geopandas(vector_object: "vector.Vector", file_path: Optional[str] = None):
    """Create geopandas object from Vector object.

    Parameters
    ----------
    vector_object: vector.Vector
        an instance of vector object
    file_path: str, Optional
        name of outfile file to write, default is None

    Returns
    -------
    out: geopandas.GeoDataFrame
        an instance of geopandas data from instantiated from vector object

    Examples
    --------
    >>> from geostack.vector import Vector
    >>> from geostack.utils import get_epsg

    >>> vec = Vector()
    >>> vec.addPoint([0, 0])
    >>> vec.setProjectionParameters(get_epsg(4326))
    >>> df = to_geopandas(vec)
    """
    _check_input_args(vector_object, file_path)

    # get projection parameters from the Vector object
    vec_proj = vector_object.getProjectionParameters().to_proj4()
    if len(vec_proj) > 0:
        crs = CRS.from_proj4(vec_proj)
    else:
        crs = None
    # convert geometries to GeoSeries
    vec_ds = from_wkt(vector_object)

    # get the properties
    prop_names = vector_object.properties.getPropertyNames()
    prop_values = {prop: vector_object.properties.getProperty(prop)
                   for prop in prop_names}
    prop_values.update({"geometry": vec_ds})

    # create geopandas GeoDataFrame
    out = gpd.GeoDataFrame(prop_values, crs=crs)

    if file_path is not None:
        if pth.splitext(file_path)[-1].lower()(".shp"):
            out.to_file(file_path)
        elif pth.splitext(file_path)[-1].lower() in ['.json', '.geojson']:
            out.to_file(file_path, driver="GeoJSON")
    else:
        return out


def to_database(inp_vector: "vector.Vector", file_name: str,
                epsg_code: int = 4283,
                geom_type: "GeometryType" = GeometryType.Polygon):
    """Create a SQLite database from Vector object.

    Parameters
    ----------
    input_vector: vector.Vector
        an instance of vector object
    file_name: str
        name of outfile file to write
    epsg_code: int
        epsg code for the projection of vector object,
        this is used to specify SRID in the database
    geom_type: GeometryType.Polygon
        geometry type to write to the database

    Returns
    -------
    Nil

    Examples
    --------
    >>> from geostack.vector import Vector
    >>> from geostack.utils import get_epsg

    >>> vec = Vector()
    >>> vec.addPoint([0, 0])
    >>> vec.setProjectionParameters(get_epsg(4283))

    >>> to_database(vec, "test.sqlite", epsg_code=4283, geom_type=GeometryType.Point)
    """
    _check_input_args(inp_vector, file_name)

    if pth.exists(file_name):
        raise RuntimeError(
            f"file {file_name} exists, specify a different name")

    db_flags = DbFlags()

    # get the properties
    prop_names = inp_vector.properties.getPropertyNames()
    prop_types = {prop: inp_vector.properties.getPropertyType(prop)
                  for prop in prop_names}

    # create column name string from property names
    prop_columns = []
    for prop in prop_names:
        prop_type = prop_types.get(prop)
        if prop_type == int:
            prop_columns.append(f"{prop} INTEGER NOT NULL")
        elif prop_type == float:
            prop_columns.append(f"{prop} FLOAT NOT NULL")
        elif prop_type == str:
            prop_columns.append(f"{prop} VARCHAR(2048) NOT NULL")
    prop_columns = ','.join(prop_columns)
    geom_idx = inp_vector.getGeometryIndexes()

    # name of the table for the DB
    db_name = pth.splitext(pth.basename(file_name))[0]
    db_name = db_name.replace("-", "_")

    # open database and write data
    with sqlite3.connect(file_name) as conn:
        conn.enable_load_extension(True)
        conn.load_extension("mod_spatialite")
        conn.execute("SELECT InitSpatialMetadata(1);")

        # ascertain the geometries to be written
        table_names = []
        for item, value in [["Point", inp_vector.getPointCount()],
                            ["LineString", inp_vector.getLineStringCount()],
                            ["Polygon", inp_vector.getPolygonCount()]]:
            if value > 0:
                table_names.append(item)

        for name in table_names:
            conn.execute(f"""CREATE TABLE {db_name.upper()}_{name}(
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, {prop_columns});""")

            if getattr(db_flags, name) != 1:
                if epsg_code is not None:
                    conn.execute(
                        f"SELECT AddGeometryColumn('{db_name.upper()}_{name}', 'geometry', {epsg_code}, '{name.upper()}', 2);")
                else:
                    conn.execute(
                        f"SELECT AddGeometryColumn('{db_name.upper()}_{name}', 'geometry', '{name.upper()}', 2);")
                conn.execute(
                    f"SELECT CreateMbrCache('{db_name.upper()}_{name}', 'geometry');")
                setattr(db_flags, name, 1)

        # iterate through the geometries
        for i, idx in enumerate(geom_idx, 0):
            geom_type = inp_vector.getGeometryType(idx)

            if geom_type == GeometryType.Polygon:
                name = "Polygon"
            elif geom_type == GeometryType.LineString:
                name = "LineString"
            elif geom_type == GeometryType.Point:
                name = "Point"

            # get the WKT string for the geometry
            geom_wkt = vectorItemToGeoWKT(inp_vector, idx)

            prop_values = [inp_vector.getProperty(
                idx, item) for item in prop_names]
            prop_values = ','.join([f"'{item}'" if isinstance(item, str) else f"{item}"
                                    for item in prop_values])

            # write the row information to the database
            cmd = f"INSERT INTO {db_name.upper()}_{name}({','.join(prop_names)},geometry)"
            if epsg_code is not None:
                cmd += f" VALUES({prop_values},GeomFromText('{geom_wkt}', {epsg_code}));"
            else:
                cmd += f" VALUES({prop_values},GeomFromText('{geom_wkt}'));"
            conn.execute(cmd)


def _check_input_args(vector_object: "vector.Vector", file_path: str = None):
    """internal function to check the arguments for geopandas writer

    Parameters
    ----------
    vector_object : vector.Vector
        an instance of vector object
    file_path : str, optional
        path of the file to be written, by default None

    Raises
    ------
    TypeError
        input vector should be an instance of Vector
    TypeError
        input file path should be a string
    ValueError
        path doesn't exist
    """
    if not isinstance(vector_object, vector.Vector):
        raise TypeError("Input vector object should be an instance of Vector")
    if file_path is not None:
        if not isinstance(file_path, str):
            raise TypeError("Input file path should be a string")
        if not pth.isdir(pth.dirname(file_path)):
            raise ValueError(f"path {pth.dirname(file_path)} doesn't exist")
