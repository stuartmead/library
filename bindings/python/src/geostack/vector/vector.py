# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from __future__ import annotations
import os
import os.path as pth
from io import StringIO
import json
import numpy as np
from copy import deepcopy
from typing import Union, List, Iterable, Dict, Hashable, Tuple, Optional
from itertools import accumulate, chain, repeat
import numbers

from ._cy_vector import (_Coordinate_d,
                         _Coordinate_f,
                         _Vector_d,
                         _Vector_f)
from ._cy_vector import IndexList as _IndexList
from ._cy_vector import _BoundingBox_d, _BoundingBox_f
from .. import io
from .. import core
from .. import gs_enums
from ..raster import raster
from .. import utils
from .. import writers
from .. import readers
from ..dataset import supported_libs

fiona, _ = supported_libs.import_or_skip("fiona")
shapefile, _ = supported_libs.import_or_skip("shapefile")
ogr, _ = supported_libs.import_or_skip("ogr", package="osgeo")

if supported_libs.HAS_GPD:
    import geopandas as gpd

__all__ = ["Coordinate", "Vector", "BoundingBox", "IndexList"]


class IndexList:
    def __init__(self, index_list):
        if not isinstance(index_list, _IndexList):
            raise TypeError(
                "index_list should be an instance of cython index list")
        self._handle = index_list

    @property
    def size(self) -> numbers.Integral:
        return self._handle.size

    def as_array(self) -> np.ndarray:
        return np.asanyarray(self._handle.as_array())

    def __contains__(self, idx) -> bool:
        return self._handle.contains(idx)

    def __iter__(self):
        return iter(self._handle)

    def __next__(self):
        return next(self._handle)

    def __getitem__(self, index):
        return self._handle[index]

    def __len__(self) -> numbers.Integral:
        return len(self._handle)

    def __repr__(self):
        return "<class 'geostack.vector.%s'>" % (self.__class__.__name__)


class BoundingBox:
    """BoundingBox class for cython wrapper to c++ object
    """

    def __init__(self, input_bbox=None, dtype: np.dtype = np.float32):
        """BoundingBox constructor.

        Parameters
        ----------
        input_bbox: None/BoundingBox/_BoundingBox_d/_BoundingBox_f/list
            input bounding box, either None for an empty box, an existing
            BoundingBox object or a list of coordinates
        dtype: np.dtype (np.float32/np.float64)
            data type of bounding box.

        Returns
        -------
        out: BoundingBox
            An instance of BoundingBox object.

        Examples
        --------
        >>> bbox = BoundingBox(dtype=np.float32)
        """
        if input_bbox is None:
            # Create empty bounding box
            self._handle = None
            self._dtype: np.dtype = None
            if dtype == np.float32:
                self._handle = _BoundingBox_f()
                self._dtype = np.float32
            elif dtype == np.float64:
                self._handle = _BoundingBox_d()
                self._dtype = np.float64
        elif isinstance(input_bbox, (BoundingBox, _BoundingBox_f, _BoundingBox_d)):
            # Create from bounding box
            new_bbox = BoundingBox.from_bbox(input_bbox)
            self._handle = new_bbox._handle
            self._dtype = new_bbox._dtype
        elif isinstance(input_bbox, list):
            # Create from list
            new_bbox = BoundingBox.from_list(input_bbox)
            self._handle = new_bbox._handle
            self._dtype = new_bbox._dtype
        else:
            raise TypeError(
                "input bounding box should be None, a BoundingBox or a list")

    @staticmethod
    def from_bbox(input_bbox: "BoundingBox") -> "BoundingBox":
        """Intantiate BoundingBox from cython instance.

        A static method to generate a bounding box from cython instances
        of BoundingBox. It is provided as a convenience function and should
        not be needed for most of purposes.

        Parameters
        ----------
        input_bbox: _BoundingBox_d/_BoundingBox_f
            An instances of boundingbox classes implemented in cython
        """
        out = BoundingBox()
        if isinstance(input_bbox, BoundingBox):
            out._handle = input_bbox._handle
            out.dtype = input_bbox._dtype
        else:
            out._handle = input_bbox
            out._dtype = np.float32 if isinstance(
                input_bbox, _BoundingBox_f) else np.float64
        return out

    @staticmethod
    def from_list(input_bbox: List, dtype: np.dtype = np.float32) -> "BoundingBox":
        """Instantiate a BoundingBox from a list of list of coordinate bounds.

        A static method to instantiate a bounding box from a list of list
        containing coordinate bounds of a box. This function is provided
        for convenience to generate a C++ instance of bounding box from
        python list.

        For instances, a bounding box can be *[[lower_left_x, lower_left_y],
        [upper_right_x, upper_right_y]]*, where *(lower_left_x, lower_left_y)*
        are the **x** and **y** coordinates of the lower left corner of the
        bounding box while *(upper_right_x, upper_right_y)* are the **x**, **y**
        coordinates of the upper right corner.


        Parameters
        ----------
        input_bbox: list
            input bounding box as a list of list

        dtype: np.dtype (np.float32/np.float64)
            Data type of bounding box
        """
        _input_bbox = deepcopy(input_bbox)
        for item in _input_bbox:
            item.extend([0] * (4 - len(item)))
        if dtype is None or dtype == np.float32:
            out = BoundingBox.from_bbox(_BoundingBox_f.from_list(_input_bbox))
        elif dtype == np.float64:
            out = BoundingBox.from_bbox(_BoundingBox_d.from_list(_input_bbox))
        return out

    def convert(self, dst_proj: "core.ProjectionParameters",
                src_proj: "core.ProjectionParameters") -> "BoundingBox":
        if not isinstance(src_proj, core.ProjectionParameters):
            raise TypeError(
                "src_proj should be an instance of ProjectionParameters")
        if not isinstance(dst_proj, core.ProjectionParameters):
            raise TypeError(
                "dst_proj should be an instance of ProjectionParameters")
        out = BoundingBox(dtype=self._dtype)
        out._handle = self._handle.convert(dst_proj._handle, src_proj._handle)
        return out

    def area2D(self) -> numbers.Real:
        """Compute 2D area of bounding box.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        out = self._handle.area2D()
        return out

    def centroidDistanceSqr(self, other: "BoundingBox") -> numbers.Real:
        """Compute squared distance between bounding box centroids.

        Parameters
        ----------
        other: BoundingBox
            An instance of BoundingBox class with a same dtype.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        if not isinstance(other, (BoundingBox, _BoundingBox_d, _BoundingBox_f)):
            raise TypeError(
                "Input bounding box should be an instance of BoundingBox class")
        _other = other._handle if isinstance(other, BoundingBox) else other
        if isinstance(_other, _BoundingBox_d):
            assert isinstance(
                self._handle, _BoundingBox_d), "datatype mismatch"
        elif isinstance(_other, _BoundingBox_f):
            assert isinstance(
                self._handle, _BoundingBox_f), "datatype mismatch"
        out = self._handle.centroidDistanceSqr(_other)
        return out

    def minimumDistanceSqr(self, other: "BoundingBox") -> numbers.Real:
        """Compute minimum squared distance between bounding boxes.

        Parameters
        ----------
        other: BoundingBox
            An instance of BoundingBox class with a same dtype.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        if not isinstance(other, (BoundingBox, _BoundingBox_d, _BoundingBox_f)):
            raise TypeError(
                "Input bounding box should be an instance of BoundingBox class")
        _other = other._handle if isinstance(other, BoundingBox) else other
        if isinstance(_other, _BoundingBox_d):
            assert isinstance(
                self._handle, _BoundingBox_d), "datatype mismatch"
        elif isinstance(_other, _BoundingBox_f):
            assert isinstance(
                self._handle, _BoundingBox_f), "datatype mismatch"
        out = self._handle.minimumDistanceSqr(_other)
        return out

    def extend(self, other: "BoundingBox"):
        """Extend the bounding box using input argument.

        Parameters
        ----------
        other: BoundingBox/float/Coordinate
            Input argument for extending the bounding box.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        if isinstance(other, (Coordinate, _Coordinate_d, _Coordinate_f)):
            _other = other._handle if isinstance(other, Coordinate) else other
            if isinstance(_other, _Coordinate_d):
                assert self._dtype == np.float64, "datatype mismatch"
            elif isinstance(_other, _Coordinate_f):
                assert self._dtype == np.float32, "datatype mismatch"
            self._handle.extend_with_coordinate(_other)
        elif isinstance(other, numbers.Real):
            self._handle.extend_with_value(float(other))
        elif isinstance(other, (BoundingBox, _BoundingBox_d, _BoundingBox_f)):
            _other = other._handle if isinstance(other, BoundingBox) else other
            if isinstance(_other, _BoundingBox_d):
                assert self._dtype == np.float64, "datatype mismatch"
            elif isinstance(_other, _BoundingBox_f):
                assert self._dtype == np.float32, "datatype mismatch"
            self._handle.extend_with_bbox(_other)
        else:
            raise TypeError(
                "other should be a float or an instance of Coordinate or BoundingBox")

    @property
    def centroid(self) -> "Coordinate":
        """Get the centroid of the bounding box.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        return Coordinate._from_coordinate(self._handle.centroid())

    @property
    def extent(self) -> "Coordinate":
        """Get the extent of the bounding box.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        return Coordinate._from_coordinate(self._handle.extent())

    @property
    def min(self) -> "Coordinate":
        assert self._handle is not None, "BoundingBox is not instantiated"
        return Coordinate._from_coordinate(self._handle.min_c)

    @property
    def max(self) -> "Coordinate":
        assert self._handle is not None, "BoundingBox is not instantiated"
        return Coordinate._from_coordinate(self._handle.max_c)

    def reset(self):
        """Reset the instantiation of the bounding box.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        self._handle.reset()

    def to_list(self) -> List:
        """Convert the bounding box to list of list of coordinates.
        """
        assert self._handle is not None, "BoundingBox is not instantiated"
        return self._handle.to_list()

    def contains(self, other) -> bool:
        if not isinstance(other, Coordinate):
            raise TypeError("input argument should be a Coordinate")
        if self._dtype != other._dtype:
            raise TypeError("Type mismatch between BoundingBox and Coordinate")
        return self._handle.contains(other._handle)

    @staticmethod
    def boundingBoxIntersects(this: "BoundingBox", other: "BoundingBox") -> bool:
        if not isinstance(this, BoundingBox) or isinstance(other, BoundingBox):
            raise TypeError("input arguments should be a BoundingBox")
        assert this._dtype == other._dtype, "Type mismatch between input argument"
        if this._dtype == np.float32:
            return _BoundingBox_f.boundingBoxIntersects(this._handle,
                                                        other._handle)
        elif this._dtype == np.float64:
            return _BoundingBox_d.boundingBoxIntersects(this._handle,
                                                        other._handle)

    @staticmethod
    def boundingBoxContains(this: "BoundingBox", other: "Coordinate") -> bool:
        if not isinstance(this, BoundingBox):
            raise TypeError("first argument should be a BoundingBox")
        if not isinstance(other, (Coordinate, BoundingBox)):
            raise TypeError(
                "second argument should be a Coordinate/BoundingBox")
        assert this._dtype == other._dtype, "Type mismatch between input argument"
        if this._dtype == np.float32:
            if isinstance(other, Coordinate):
                return _BoundingBox_f.bbox_contains_coordinate(this._handle,
                                                               other._handle)
            else:
                return _BoundingBox_f.bbox_contains_bbox(this._handle,
                                                         other._handle)
        elif this._dtype == np.float64:
            if isinstance(other, Coordinate):
                return _BoundingBox_d.bbox_contains_coordinate(this._handle,
                                                               other._handle)
            else:
                return _BoundingBox_d.bbox_contains_bbox(this._handle,
                                                         other._handle)

    def __getitem__(self, idx: Union[numbers.Integral, int]):
        if idx > 1:
            raise IndexError(f"Index {idx} should be in range [0,1]")
        return Coordinate._from_coordinate(self._handle[idx])

    def __str__(self):
        bounds_string = "    lower bound:   %s\n    upper bound:   %s" % tuple(
            map(str, self.to_list()))
        return bounds_string

    def __repr__(self):
        return "<class 'geostack.vector.%s'>\n%s" % (self.__class__.__name__, str(self))


class Coordinate:
    '''Coordinate class wrapper for c++ object.

    Parameters
    ----------
    p: x-axis coordinate, float value in degrees_east/ easting
    q: y-axis coordinate, float value in degrees_north/ northing

    Returns
    -------
    Instance of coordinate object


    Examples
    --------
    >>> c = Coordinate(p=130.0, q=-34.0, dtype=np.float32)
    '''

    def __init__(self, p: Optional[Union[numbers.Real, float]] = None,
                 q: Optional[Union[numbers.Real, float]] = None,
                 r: Optional[Union[numbers.Real, float]] = None,
                 s: Optional[Union[numbers.Real, float]] = None,
                 dtype: np.dtype = np.float32):
        if p is None and q is None:
            self._handle = None
            self._dtype = None
        else:
            if dtype not in [float, np.float32, np.float64]:
                raise TypeError("dtype should be np.float32/np.float64")
            self._dtype = np.float32 if dtype in [
                float, np.float32] else np.float64
            if self._dtype == np.float64:
                self._handle = _Coordinate_d(self._dtype(p),
                                             self._dtype(q))
            elif self._dtype == np.float32:
                self._handle = _Coordinate_f(self._dtype(p),
                                             self._dtype(q))

    @staticmethod
    def _from_coordinate(inp_coordinate):
        """Instantiate Coordinate object from a Cython object.

        Parameters
        ----------
        inp_coordinate : _Coordinate_f/ _Coordinate_d
            An instance of float or double type Coordinate object.

        Returns
        -------
        out: Coordinate
            An instance of Python Coordinate object
        """
        if isinstance(inp_coordinate, _Coordinate_d):
            out = Coordinate()
            out._dtype = np.float64
            out._handle = inp_coordinate
        elif isinstance(inp_coordinate, _Coordinate_f):
            out = Coordinate()
            out._dtype = np.float32
            out._handle = inp_coordinate
        return out

    @staticmethod
    def from_list(other: List, dtype: np.dtype = np.float32) -> "Coordinate":
        if not isinstance(other, list):
            raise TypeError("input argument should be a list")
        if len(other) < 2:
            raise ValueError("length of input list should be at least 2")
        _other = deepcopy(other)
        _other.extend([0] * (4 - len(other)))
        if dtype == np.float32:
            return Coordinate._from_coordinate(_Coordinate_f.from_list(_other))
        elif dtype == np.float64:
            return Coordinate._from_coordinate(_Coordinate_d.from_list(_other))

    def getCoordinate(self) -> Tuple[float, float]:
        '''get the coordinates of the object

        Parameters
        ----------
        Nil

        Returns
        -------
        out: tuple of coordinate (p, q)

        Examples
        --------
        >>> c = Coordinate(p=130.0, q=-34.0)
        >>> c.getCoordinate()
        (130.0, -34.0)
        '''
        if hasattr(self, "_handle"):
            return self._handle.getCoordinate()
        else:
            raise AttributeError("Coordinate class is not yet initialized")

    def maxCoordinate(self, this: "Coordinate", other: "Coordinate") -> "Coordinate":
        """Instantiate Coordinate object with the maximum of two input coordinates.

        Parameters
        ----------
        this : Coordinate
            An instance of Coordinate object
        other : Coordinate
            An instance of Coordinate object

        Returns
        -------
        out : Coordinate
            A Coordinate instance with the maximum of the input Coordinates

        Raises
        ------
        TypeError
            Type mismatch between two input coordinates
        RuntimeError
            Input Coordinate not initialized
        TypeError
            Input arguments should be instance of coordinates
        """
        assert self._handle is not None, "Coordinate is not instantiated"
        if isinstance(this, Coordinate) and isinstance(other, Coordinate):
            if this._handle is not None or other._handle is not None:
                raise RuntimeError("input coordinates are not initialized")
            assert self._dtype == this._dtype == other._dtype, "data type mismatch"
            return Coordinate._from_coordinate(self._handle.maxCoordinate(this._handle,
                                                                          other._handle))
        else:
            raise TypeError("Input argument should be instance of Coordinate")

    def minCoordinate(self, this: "Coordinate", other: "Coordinate") -> "Coordinate":
        """Instantiate Coordinate object with the minimum of two input coordinates.

        Parameters
        ----------
        this : Coordinate
            An instance of Coordinate object
        other : Coordinate
            An instance of Coordinate object

        Returns
        -------
        out : Coordinate
            A Coordinate instance with the minimum of the input Coordinates

        Raises
        ------
        TypeError
            Type mismatch between two input coordinates
        RuntimeError
            Input Coordinate not initialized
        TypeError
            Input arguments should be instance of coordinates
        """
        assert self._handle is not None, "Coordinate is not instantiated"
        if isinstance(this, Coordinate) and isinstance(other, Coordinate):
            if this._handle is not None or other._handle is not None:
                raise RuntimeError("input coordinates are not initialized")
            assert self._dtype == this._dtype == other._dtype, "datatype mismatch"
            return Coordinate._from_coordinate(self._handle.minCoordinate(this._handle,
                                                                          other._handle))
        else:
            raise TypeError("Input argument should be instance of Coordinate")

    def centroid(self, this: "Coordinate", other: "Coordinate") -> "Coordinate":
        """Instantiate Coordinate object with the centroid of two input coordinates

        Parameters
        ----------
        this : Coordinate
            An instance of Coordinate object
        other : Coordinate
            An instance of Coordinate object

        Returns
        -------
        out : Coordinate
            A Coordinate instance with the centroid of the input Coordinates

        Raises
        ------
        TypeError
            Type mismatch between two input coordinates
        RuntimeError
            Input Coordinate not initialized
        TypeError
            Input arguments should be instance of coordinates
        """
        assert self._handle is not None, "Coordinate is not instantiated"
        if isinstance(this, Coordinate) and isinstance(other, Coordinate):
            if this._handle is not None or other._handle is not None:
                raise RuntimeError("input coordinates are not initialized")
            assert self._dtype == this._dtype == other._dtype, "datatype mismatch"
            return Coordinate._from_coordinate(
                self._handle.centroid(this._handle, other._handle))
        else:
            raise TypeError("Input argument should be instance of Coordinate")

    def magnitudeSquared(self) -> numbers.Real:
        if self._handle is not None:
            return self._handle.magnitudeSquared()

    @property
    def p(self) -> numbers.Real:
        return self._handle.get_p()

    @p.setter
    def p(self, other: numbers.Real):
        self._handle.set_p(other)

    @property
    def q(self) -> numbers.Real:
        return self._handle.get_q()

    @q.setter
    def q(self, other: numbers.Real):
        self._handle.set_q(other)

    @property
    def r(self) -> numbers.Real:
        return self._handle.get_r()

    @r.setter
    def r(self, other: numbers.Real):
        self._handle.set_r(other)

    @property
    def s(self) -> numbers.Real:
        return self._handle.get_s()

    @s.setter
    def s(self, other: numbers.Real):
        self._handle.set_s(other)

    def to_list(self) -> List:
        return self._handle.to_list()

    def _check_type(self, other):
        if not isinstance(other, Coordinate):
            raise TypeError(
                "input argument should be an instance of Coordinate")
        assert type(self._handle) == type(other._handle), "datatype mismatch"

    def getGeoHash(self) -> str:
        return self._handle.getGeoHash()

    def __eq__(self, other: "Coordinate") -> bool:
        self._check_type(other)
        out = self._handle == other._handle
        return out

    def __ne__(self, other: "Coordinate") -> bool:
        self._check_type(other)
        out = self._handle != other._handle
        return out

    def __add__(self, other: "Coordinate"):
        self._check_type(other)
        out = self._handle + other._handle
        return Coordinate._from_coordinate(out)

    def __iadd__(self, other: "Coordinate"):
        self._check_type(other)
        self._handle = self._handle + other._handle

    def __sub__(self, other: "Coordinate"):
        self._check_type(other)
        out = self._handle - other._handle
        return Coordinate._from_coordinate(out)

    def __isub__(self, other: "Coordinate"):
        self._check_type(other)
        self._handle = self._handle - other._handle

    def __getitem__(self, idx: Union[List, numbers.Integral]) -> Union[List, numbers.Real]:
        if isinstance(idx, int):
            if idx > 3:
                raise IndexError(f"Index {idx} should be in range[0, 3]")
            return self._handle[idx]
        elif isinstance(idx, (list, tuple)):
            return [self._handle[item] for item in filter(lambda s: s < 3 and s >= 0, idx)]
        elif isinstance(idx, slice):
            return self.to_list()[idx]

    def __setitem__(self, idx: numbers.Integral, val: numbers.Real):
        idx_map = {i: prop for i, prop in enumerate(['p', 'q', 'r', 's'])}
        if isinstance(idx, int):
            if idx > 3 or idx < 0:
                raise IndexError(f"Index {idx} should be in range[0, 3]")
            setattr(self, idx_map[idx], val)
        else:
            raise NotImplementedError(
                "set item is only implemented for integer indices")

    def __repr__(self):
        return "<class 'geostack.vector.%s'>\n    %s>" % (self.__class__.__name__,
                                                          str(self.to_list()))


class Vector:
    """Vector class python object around C++ Vector.

    Parameters
    ----------
    dtype : numpy.dtype
        Data type for instantiating Cython Vector object, (np.float32/np.float64).

    Attributes
    ----------
    _dtype : np.dtype
        Data type of Vector class object.

    _handle : Cython object
        Handle to Cython Vector object.

    """

    def __init__(self, dtype: np.dtype = np.float32):
        self._dtype: np.dtype = None
        self._handle = None
        if dtype not in [float, np.float32, np.float64]:
            raise TypeError("dtype should be np.float32/ np.float64")
        self._dtype = np.float32 if dtype in [
            float, np.float32] else np.float64
        if self._dtype == np.float32:
            self._handle = _Vector_f()
        elif self._dtype == np.float64:
            self._handle = _Vector_d()

    @classmethod
    def _from_vector(cls, vec_obj):
        out = cls()
        _vec_obj = vec_obj._handle if isinstance(vec_obj, cls) else vec_obj
        if isinstance(_vec_obj, _Vector_d):
            out._dtype = np.float64
            out._handle = _vec_obj
        elif isinstance(_vec_obj, _Vector_f):
            out._dtype = np.float32
            out._handle = _vec_obj
        else:
            raise TypeError("vector_object can be an instance of Vector")
        return out

    def addGeometry(self, other: "Vector", idx_: numbers.Integral) -> numbers.Integral:
        """add a geometry at a given index from the given vector object

        Parameters
        ----------
        other : Vector
            an instance of the source vector object
        idx_ : numbers.Integral
            index of geometry in the source vector object

        Returns:
        numbers.Integral
            index of the geometry in the destination vector object

        Raises
        ------
        TypeError
            input object should an instance of vector object
        """
        if not isinstance(other, Vector):
            raise TypeError("other should be an instance of Vector object")

        # if isinstance(other, Vector):
        #     geom_type = other.getGeometryType(idx_)

        #     get_method_map = {gs_enums.GeometryType.Point.value: other.getPoint,
        #                       gs_enums.GeometryType.LineString.value: other.getLineString,
        #                       gs_enums.GeometryType.Polygon.value: other.getPolygon}

        # elif isinstance(other, (_Vector_f, _Vector_d)):
        #     geom_type = gs_enums.GeometryType(other.get_geometry_type(idx_))

        # if not self.hasData():
        #     self += get_method_map.get(geom_type.value)(idx_)
        #     out = next(self.getGeometryIndexes())
        # else:
        #     if isinstance(other, Vector):
        #         assert other._dtype == self._dtype, "mismatch in datatype of vector object"
        #         out = self._handle.addGeometry(other._handle, idx_)
        #     else:
        #         out = self._handle.addGeometry(other, idx_)

        geom_type = other.getGeometryType(idx_)

        get_method_map = {gs_enums.GeometryType.Point.value: other.getPoint,
                          gs_enums.GeometryType.LineString.value: other.getLineString,
                          gs_enums.GeometryType.Polygon.value: other.getPolygon}

        self += get_method_map.get(geom_type.value)(idx_)
        out = self.getGeometryIndexes()
        out = out[out.size - 1]
        return out

    def addPoint(self, other: Union[List[numbers.Real], Tuple[numbers.Real], Coordinate]) -> numbers.Integral:
        """add point to a vector object

        Parameters
        ----------
        other : Union[List[numbers.Real], Tuple[numbers.Real], Coordinate]
            the coordinates of the point

        Returns
        -------
        numbers.Integral
            index of the point in the vector object

        Raises
        ------
        RuntimeWarning
            vector class is not yet initialised
        TypeError
            point should be a list/ tuple of maximum length 4
        TypeError
            vales in point should of float type
        TypeError
            point is not of a valid type
        """
        if self._dtype is None or self._handle is None:
            raise RuntimeWarning('Vector class is not yet initialized')
        if isinstance(other, (list, tuple)):
            if len(other) > 4:
                raise TypeError(
                    "point should be a list or tuple of maximum length 4")
            else:
                if not isinstance(other[0], numbers.Real):
                    raise TypeError("values in point should be of float type")
                else:
                    out = self._handle.addPoint(Coordinate.from_list(other,
                                                dtype=self._dtype)._handle)
                    return out
        elif isinstance(other, Coordinate):
            out = self._handle.addPoint(other._handle)
            return out
        else:
            raise TypeError("point is not of a valid type")

    def addPoints(self, other: Union[np.ndarray, List[List[numbers.Real]]]) -> List[numbers.Integral]:
        """add a list of points to the vector object

        Parameters
        ----------
        other : Union[np.ndarray, List[List[numbers.Real]]]
            a ndarray or list of list containing coordinates of points

        Returns
        -------
        List[numbers.Integral]
            a list of indices of the points in the vector object

        Raises
        ------
        TypeError
            input should be a list/ ndarray
        TypeError
            if a numpy array is provided, it should be of shape npoints x 2
        """
        if isinstance(other, np.ndarray):
            points = other.astype(self._dtype)
        elif isinstance(other, list):
            points = np.array(other, dtype=self._dtype)
        else:
            raise TypeError("Input should be list/ np.ndarray")
        if points.ndim == 2 and points.shape[1] >= 2:
            _points = points.copy()
            for _ in range(4 - points.shape[1]):
                _points = np.c_[_points, np.zeros(
                    points.shape[0], dtype=self._dtype)]
        else:
            raise TypeError("If a numpy array is provided, it should be of" +
                            " shape npoints x 2 is expected")
        out = self._handle.addPoints(_points)
        return np.asanyarray(out)

    def addLineString(self, other: Union[np.ndarray, List[List[numbers.Real]]]) -> numbers.Integral:
        """add line string to a vector object

        Parameters
        ----------
        other : Union[np.ndarray, List[List[numbers.Real]]]
            a ndarray or list of list with the coordinates for a line string

        Returns
        -------
        numbers.Integral
            index of linestring in the vector object

        Raises
        ------
        RuntimeWarning
            vector class is not yet initialised
        TypeError
            if a list is provided, it should ba list of list
        TypeError
            if a list is provided, ti should contain points as list of length atleast 2
        TypeError
            if a numpy array is provided, it should be of shape npoints x 2
        """
        if self._dtype is None or self._handle is None:
            raise RuntimeWarning("Vector class is not yet initialized")
        if isinstance(other, list):
            if not isinstance(other[0], (list, tuple)):
                raise TypeError(
                    "If a list is provided, it should be a list of list/ or tuple")
            if len(other[0]) < 2:
                raise TypeError('If a list is provided, it should contain points' +
                                ' as list of length at least 2')
            _other = np.array(other).astype(self._dtype)
            for _ in range(4 - _other.shape[1]):
                _other = np.c_[_other, np.zeros(
                    _other.shape[0], dtype=self._dtype)]
            out = self._handle.addLineString(_other)
        elif isinstance(other, np.ndarray):
            if other.ndim == 2 and other.shape[1] >= 2:
                _other = other.copy().astype(self._dtype)
                for _ in range(4 - other.shape[1]):
                    _other = np.c_[_other, np.zeros(
                        _other.shape[0], dtype=self._dtype)]
                out = self._handle.addLineString(_other)
            else:
                raise TypeError("If a numpy array if provided, it should be of" +
                                " shape npoints x 2 is expected")
        return out

    def addPolygon(self, other: List[Union[List[List[numbers.Real]], np.ndarray]]) -> numbers.Integral:
        """add a polygon to a Vector object

        Parameters
        ----------
        other : List[Union[List[List[numbers.Real]], np.ndarray]]
            a list of lists/ np.ndarray with the coordinates for the polygon boundaries

        Returns
        -------
        numbers.Integral
            index of the polygon geometry in the vector object

        Raises
        ------
        RuntimeWarning
            Vector class is not yet initialized
        TypeError
            Polygon object should be a list of numpy array or list
        TypeError
            Polygon object should be a list of numpy array or list
        TypeError
            if a list of list is provided, the inner list should contain points as list of length atleast 2
        TypeError
            if a list of ndarray is provided, the inner ndarray should be of shape npoints x 2
        """
        if self._dtype is None or self._handle is None:
            raise RuntimeWarning("Vector class is not yet initialized")
        if not isinstance(other, list):
            raise TypeError(
                "Polygon object should be a list of numpy array or list")
        elif not isinstance(other[0], (list, np.ndarray)):
            raise TypeError(
                "Polygon object should be a list of numpy array or list")

        if isinstance(other[0], list):
            if len(other[0]) < 2:
                raise TypeError('If a list is provided, it should contain' +
                                ' points as list of length atleast 2')
        elif isinstance(other[0], np.ndarray):
            if other[0].ndim != 2 and other[0].shape[1] >= 2:
                raise TypeError("If a numpy array if provided, it should be" +
                                " of shape npoints x 2 is expected")

        _poly_item = []
        for item in other:
            if isinstance(item, list):
                _other = np.atleast_2d(item).astype(self._dtype)
                for _ in range(4 - _other.shape[1]):
                    _other = np.c_[_other,
                                   np.zeros(shape=(_other.shape[0], 1),
                                            dtype=self._dtype)]
            elif isinstance(item, np.ndarray):
                _other = item.copy().astype(self._dtype)
                for _ in range(4 - item.shape[1]):
                    _other = np.c_[_other,
                                   np.zeros(shape=(_other.shape[0], 1),
                                            dtype=self._dtype)]
            _poly_item.append(_other)
        out = self._handle.addPolygon(_poly_item)
        return out

    def updatePointIndex(self, index: numbers.Integral):
        self._handle.updatePointIndex(index)

    def updateLineStringIndex(self, index: numbers.Integral):
        self._handle.updateLineStringIndex(index)

    def updatePolygonIndex(self, index: numbers.Integral):
        self._handle.updatePolygonIndex(index)

    def clear(self):
        self._handle.clear()

    def buildTree(self):
        self._handle.buildTree()

    def getVertexSize(self) -> numbers.Integral:
        """get the size of vertices in the vector object

        Returns
        -------
        numbers.Integral
            number of vertices in the vector object
        """
        return self._handle.getVertexSize()

    def getGeometryIndexes(self) -> IndexList:
        """get the indices of geometries in the vector object

        Returns
        -------
        IndexList
            an instance of IndexList object
        """
        return IndexList(self._handle.getGeometryIndexes())

    def getPointIndexes(self) -> IndexList:
        """get the indices of point geometries in the vector object

        Returns
        -------
        IndexList
            an instance of IndexList object
        """
        return IndexList(self._handle.getPointIndexes())

    def getLineStringIndexes(self) -> IndexList:
        """get the indices of line string geometries in the vector object

        Returns
        -------
        IndexList
            an instance of IndexList object
        """
        return IndexList(self._handle.getLineStringIndexes())

    def getPolygonIndexes(self) -> IndexList:
        """get the indices of polygon geometries in the vector object

        Returns
        -------
        IndexList
            an instance of IndexList object
        """
        return IndexList(self._handle.getPolygonIndexes())

    def getPolygonSubIndexes(self, idx: int) -> IndexList:
        """get the sub-indices of polygon geometries in the vector object

        Returns
        -------
        IndexList
            an instance of IndexList object
        """
        return IndexList(self._handle.getPolygonSubIndexes(idx))

    def getCoordinate(self, idx: numbers.Integral, safe=utils.is_ipython()) -> "Coordinate":
        """method to get the coordinates of a vertex

        Parameters
        ----------
        idx : numbers.Integral
            index of a vertex in a vector object
        safe : False, optional
            flag to set the method to safe, by default is_ipython()

        Returns
        -------
        Coordinate
            an instance of Coordinate object with the coordinates of a vertex

        Raises
        ------
        IndexError
            index is not valid
        """
        if safe:
            vertex_size = self.getVertexSize()
            if idx < vertex_size:
                out = self._handle.getCoordinate(int(idx))
                return Coordinate._from_coordinate(out)
            else:
                raise IndexError(f"index {idx} not valid")
        else:
            if self._handle is not None:
                return Coordinate._from_coordinate(self._handle.getCoordinate(int(idx)))

    def getPointCoordinate(self, idx: numbers.Integral, safe: bool = utils.is_ipython()) -> "Coordinate":
        """get the coordinates of a point geometry

        Parameters
        ----------
        idx : numbers.Integral
            index of the point geometry
        safe : bool, optional
            flag to check in the index is valid for a point, by default is_ipython()

        Returns
        -------
        Coordinate
            The coordinates of the point geometry at the given index

        Raises
        ------
        IndexError
            point index is not valid
        """
        if safe:
            point_idx = self.getPointIndexes()
            if idx in point_idx:
                out = self._handle.getPointCoordinate(int(idx))
                return Coordinate._from_coordinate(out)
            else:
                raise IndexError(f"point index {idx} not valid")
        else:
            if self._handle is not None:
                return Coordinate._from_coordinate(self._handle.getPointCoordinate(int(idx)))

    def getLineStringCoordinates(self, idx: numbers.Integral,
                                 safe: bool = utils.is_ipython()) -> np.ndarray:
        """method to get the coordinates of the line string

        Parameters
        ----------
        idx : numbers.Integral
            index of the line string geometry
        safe : bool, optional
            flag to check whether the index is valid for a line string, by default is_ipython()

        Returns
        -------
        np.ndarray
            a ndarray with the coordinates of the line string

        Raises
        ------
        IndexError
            line string index is not valid
        """
        if safe:
            line_str_idx = self.getLineStringIndexes()
            if idx in line_str_idx:
                out = self._handle.getLineStringCoordinates(int(idx))
                return np.asanyarray(out)
            else:
                raise IndexError(f"line string index {idx} not valid")
        else:
            if self._handle is not None:
                out = self._handle.getLineStringCoordinates(int(idx))
                return np.asanyarray(out)

    def getPolygonCoordinates(self, idx: numbers.Integral,
                              safe: bool = utils.is_ipython()) -> List[np.ndarray]:
        """method to get the coordinates of the polygon

        Parameters
        ----------
        idx : numbers.Integral
            index of the polygon geometry
        safe : bool, optional
            flag to check whether the index is valid for a polygon, by default is_ipython()

        Returns
        -------
        List[np.ndarray]
            a list of ndarray with polygon coordinates

        Raises
        ------
        IndexError
            polygon index is not valid
        """
        if safe:
            poly_idx = self.getPolygonIndexes()
            if idx in poly_idx:
                out = np.asanyarray(
                    self._handle.getPolygonCoordinates(int(idx)))
            else:
                raise IndexError(f"polygon index {idx} not valid")
        else:
            if self._handle is not None:
                out = np.asanyarray(
                    self._handle.getPolygonCoordinates(int(idx)))

        # get the sub indices
        sub_idx = self.getPolygonSubIndexes(idx)
        # convert array to list of array
        out = list(map(lambda start, end: out[end - start:end],
                       list(sub_idx), accumulate(sub_idx)))
        return out

    def removeProperty(self, name: Union[str, bytes]) -> None:
        """remove a property to the vector object

        Parameters
        ----------
        name : Union[str, bytes]
            name of the property

        Raises
        ------
        TypeError
            property name should be of str/ bytes type
        """
        if not isinstance(name, (str, bytes)):
            raise TypeError("property name should be of str/ bytes type")
        if isinstance(name, str):
            self._handle.removeProperty(name.encode("UTF-8"))
        else:
            self._handle.removeProperty(name)

    def addProperty(self, name: Union[str, bytes]) -> None:
        """add a property to the vector object

        Parameters
        ----------
        name : Union[str, bytes]
            name of the property

        Raises
        ------
        TypeError
            property name should be of str/ bytes type
        """
        if not isinstance(name, (str, bytes)):
            raise TypeError("property name should be of str/ bytes type")
        if isinstance(name, str):
            self._handle.addProperty(name.encode("UTF-8"))
        else:
            self._handle.addProperty(name)

    def setPropertyValues(self, name: Union[str, bytes],
                          propValue: Union[Iterable, np.ndarray]) -> None:
        """set all the values of a property in a Vector object

        Parameters
        ----------
        name : Union[str, bytes]
            name of the property defined in a vector object
        propValue : Union[Iterable, np.ndarray]
            the values of the property

        Examples
        --------
        vec = Vector()
        vec.addPoint([1.0, 2.0])
        vec.addPoint([2.0, 2.0])
        vec.addPoint([3.0, 2.0])
        vec.addProperty("dummy")
        vec.setPropertyValues("dummy", [0.0, 0.0, 0.0])
        """

        # create a method map
        method_map = {"int": self._handle.setPropertyValues_int,
                      "str": self._handle.setPropertyValues_str}
        if self._dtype == np.float64:
            method_map['double'] = self._handle.setPropertyValues_dbl
            method_map['float64'] = self._handle.setPropertyValues_dbl
            method_map['float'] = self._handle.setPropertyValues_dbl
        elif self._dtype == np.float32:
            method_map['float'] = self._handle.setPropertyValues_flt
            method_map['float64'] = self._handle.setPropertyValues_flt
            method_map['float32'] = self._handle.setPropertyValues_flt

        assert isinstance(propValue, (list, tuple)) | isinstance(
            propValue, np.ndarray)

        # get the geometry base count
        geom_base_count = self.getGeometryBaseCount()
        data_type = type(propValue[0])

        if data_type.__name__.startswith('float') or data_type.__name__ == "double":
            data_type = self._dtype

        # create a map for padding values
        pad_value = {"int": raster.getNullValue(int),
                     "str": "",
                     "float": raster.getNullValue(float),
                     "float64": raster.getNullValue(float),
                     "float32": raster.getNullValue(float)}

        if self._dtype == np.float64:
            for key in pad_value:
                if key.startswith("float"):
                    pad_value[key] = raster.getNullValue(np.float64)

        # pad values when the input values have a length smaller than
        # the length in the base vector
        diff = geom_base_count - len(propValue)
        if not isinstance(propValue, np.ndarray):
            if diff > 0:
                values = list(chain.from_iterable((propValue,
                                                  repeat(pad_value.get(data_type.__name__), diff))))
            else:
                values = propValue

            if data_type != str:
                values = np.array(list(values), dtype=data_type)
        else:
            if diff > 0:
                values = np.pad(propValue, (0, diff), mode='constant',
                                constant_values=(pad_value.get(data_type.__name__),
                                                 pad_value.get(data_type.__name__)))
            else:
                if data_type == str:
                    values = propValue.tolist()
                else:
                    values = propValue.astype(data_type)

        # set values
        method_map.get(data_type.__name__)(core.str2bytes(name), values)

    def setProperty(self, idx: int, propName: Union[str, bytes],
                    propValue: Union[str, int, float],
                    propType: type = None) -> None:
        """set a property and value to the vector object.

        Parameters
        ----------
        idx : int
            index of vector geometry in the vector object
        propName : Union[str, bytes]
            name of the property
        propValue : Union[str, int, float]
            value of the property
        propType : type, optional
            data type for the value of the property, by default None

        Raises
        ------
        AssertionError
            Vector class is not yet initialized
        ValueError
            propType is not valid
        """
        assert self._handle is not None, "Vector class is not yet initialized"

        method_map = {"int": self._handle.setProperty_int,
                      "str": self._handle.setProperty_str}
        if self._dtype == np.float64:
            method_map['float64'] = self._handle.setProperty_dbl
            method_map['float'] = self._handle.setProperty_dbl
        elif self._dtype == np.float32:
            method_map['float'] = self._handle.setProperty_flt
            method_map['float64'] = self._handle.setProperty_flt

        if propValue is not None:
            if np.isscalar(propValue):
                if propType is None:
                    propType = type(propValue)
            else:
                # work with non-scalar values
                if self._dtype == np.float64:
                    method_map['float64'] = self._handle.setPropertyVector_dbl
                    method_map['float'] = self._handle.setPropertyVector_dbl
                elif self._dtype == np.float32:
                    method_map['float'] = self._handle.setPropertyVector_flt
                    method_map['float64'] = self._handle.setPropertyVector_flt

                if propType is None:
                    propType = type(propValue[0])
                    # map integer type to float
                    if propType in [int, np.int32, np.uint32]:
                        propType = self._dtype

            method = method_map.get(propType.__name__)
            if propType.__name__ == "float32":
                method = method_map.get("float")
            if method is None:
                raise ValueError(f"propType {propType.__name__} is not valid")

            if propType == str:
                method(np.uint32(idx), propName.encode('UTF-8'),
                       propType(propValue).encode('UTF-8'))
            else:
                # work is vector values
                if np.isscalar(propValue):
                    method(np.uint32(idx), propName.encode(
                        'UTF-8'), propType(propValue))
                else:
                    method(np.uint32(idx), propName.encode('UTF-8'),
                           core.PropertyType.dtype2vec[propType](propValue))
        else:
            if not self._handle.hasProperty(propName.encode('UTF-8')):
                self._handle.addProperty(propName.encode('UTF-8'))

    def getProperty(self, idx: int, propName: Union[str, bytes],
                    propType: type = None) -> Union[int, float, str]:
        """get the value of a property from the vector object

        Parameters
        ----------
        idx : int
            index of vector geometry in the vector object
        propName : Union[str, bytes]
            name of the property
        propType : type, optional
            data type for the value of property, by default None

        Returns
        -------
        Union[int, float, str]
            value of the property

        Raises
        ------
        AssertionError
            Vector class is not yet initialized
        ValueError
            propType is not valid
        """
        assert self._handle is not None, "Vector class is not yet initialized"

        method_map = {"int": self._handle.getProperty_int,
                      "str": self._handle.getProperty_str}
        _prop_type = self.properties.getPropertyType(propName)

        if self._dtype == np.float64:
            if _prop_type == np.float64:
                method_map['float64'] = self._handle.getProperty_dbl
            elif _prop_type == core.DoubleVector:
                method_map['float64'] = self._handle.getPropertyVector_dbl
                method_map['DoubleVector'] = self._handle.getPropertyVector_dbl
        elif self._dtype == np.float32:
            if _prop_type == np.float32 or _prop_type == float:
                method_map['float'] = self._handle.getProperty_flt
            elif _prop_type == core.FloatVector:
                method_map['float'] = self._handle.getPropertyVector_flt
                method_map['FloatVector'] = self._handle.getPropertyVector_flt

        if propType is None:
            if _prop_type is not None:
                out = method_map[_prop_type.__name__](np.uint32(idx),
                                                      propName.encode('UTF-8'))
            else:
                out = _prop_type
        else:
            method = method_map.get(propType.__name__)
            if method is None:
                raise ValueError(f"propType {propType.__name__} is not valid")
            out = method(np.uint32(idx), propName.encode('UTF-8'))
        if _prop_type in [core.DoubleVector, core.FloatVector]:
            out = np.asanyarray(out)
        return out

    def hasProperty(self, propName: Union[str, bytes]) -> bool:
        """check if the property is defined for the vector object

        Parameters
        ----------
        propName : Union[str, bytes]
            name of the property

        Returns
        -------
        bool
            True is property is defined, False otherwise

        Raises
        ------
        RuntimeWarning
            Vector class is not yet initialized
        """
        if self._handle is not None:
            out = self._handle.hasProperty(propName.encode('UTF-8'))
            return out
        else:
            raise RuntimeWarning("Vector class is not yet initialized")

    def convertProperty(self, propName: Union[str, bytes], propType: type) -> None:
        """Convert data type of the property of an object.

        Parameters
        ----------
        propName : Union[str, bytes]
            name of the property
        prop_type: type
            data type to cast the value of property

        Returns
        -------
        None

        Raises
        ------
        RuntimeWarning
            Vector class is not yet initialized
        """
        if hasattr(self, '_handle'):
            cy_obj = "_handle"
        else:
            raise AttributeError("Raster or Vector has not been created")

        method_map = {"int": getattr(self, cy_obj).convertProperty_int,
                      "float": getattr(self, cy_obj).convertProperty_flt,
                      "float64": getattr(self, cy_obj).convertProperty_flt,
                      "str": getattr(self, cy_obj).convertProperty_str}

        if self._dtype == np.float64:
            method_map['float'] = getattr(self, cy_obj).convertProperty_dbl
            method_map["float64"] = getattr(self, cy_obj).convertProperty_dbl

        assert propType.__name__ in method_map, f"propType {propType} is not valid"
        method_map.get(propType.__name__)(core.str2bytes(propName))

    def getGeometryType(self, idx: int) -> gs_enums.GeometryType:
        """method to get the geometry type for a given index.

        Parameters
        ----------
        idx : int
            index of a geometry

        Returns
        -------
        gs_enums.GeometryType
            geometry type for the given index of geometry
        """
        geom_type = self._handle.get_geometry_type(idx)
        out = gs_enums.GeometryType(geom_type)
        return out

    @property
    def properties(self) -> "core.PropertyMap":
        """return all of the properties

        Returns
        -------
        PropertyMap
            an instance of PropertyMap object
        """
        return self.getProperties()

    def getProperties(self) -> "core.PropertyMap":
        """get all the properties defined for the vector object

        Returns
        -------
        PropertyMap
            an instance of PropertyMap object

        Raises
        ------
        RuntimeWarning
            Vector object is not initialized
        """
        if self._handle is not None:
            obj = core.PropertyMap(other=self._handle.getProperties())
        else:
            raise RuntimeWarning("Vector object is not initialized")
        return obj

    def setProjectionParameters(self, other: "core.ProjectionParameters"):
        if not isinstance(other, core.ProjectionParameters):
            raise TypeError(
                "dst_proj should be an instance of ProjectionParameters")
        self._handle.setProjectionParameters(other._handle)

    def getProjectionParameters(self) -> "core.ProjectionParameters":
        return core.ProjectionParameters.from_proj_param(self._handle.getProjectionParameters())

    def convert(self, dst_proj: "core.ProjectionParameters") -> "Vector":
        if not isinstance(dst_proj, core.ProjectionParameters):
            raise TypeError(
                "dst_proj should be an instance of ProjectionParameters")
        return Vector._from_vector(self._handle.convert(dst_proj._handle))

    def getPoint(self, idx: numbers.Integral) -> "Vector":
        out = Vector(dtype=self._dtype)
        _idx = out.addPoint(self.getPointCoordinate(idx))
        props = self.getProperties()
        for name in props.getPropertyNames():
            out.setProperty(_idx, name, self.getProperty(idx, name))
        return out

    def getLineString(self, idx: numbers.Integral) -> "Vector":
        out = Vector(dtype=self._dtype)
        _idx = out.addLineString(self.getLineStringCoordinates(idx))
        props = self.getProperties()
        for name in props.getPropertyNames():
            out.setProperty(_idx, name, self.getProperty(idx, name))
        return out

    def getPolygon(self, idx: numbers.Integral) -> "Vector":
        out = Vector(dtype=self._dtype)
        poly_coords = self.getPolygonCoordinates(idx)
        if not isinstance(poly_coords, list):
            poly_coords = [poly_coords]
        _idx = out.addPolygon(poly_coords)
        props = self.getProperties()
        for name in props.getPropertyNames():
            out.setProperty(_idx, name, self.getProperty(idx, name))
        return out

    def region(self, other: "BoundingBox",
               geom_type: Union[numbers.Integral, "gs_enums.GeometryType"] = 7) -> "Vector":
        """Return vector geometries within a region.

        Parameters
        ----------
        other : BoundingBox
            input region of type BoundingBox
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of vector geometry to return

        Returns
        -------
        Vector
            Vector object containing vector geometries within the
            given bounding box.

        Raises
        ------
        TypeError
            input argument should be an instance of BoundingBox
        TypeError
            geom_type should be int/ gs_enums.GeometryType
        """
        if not isinstance(other, (BoundingBox, _BoundingBox_d, _BoundingBox_f)):
            raise TypeError("other should be an instance of BoundingBox")

        _bbox = other._handle if isinstance(other, BoundingBox) else other
        if isinstance(_bbox, _BoundingBox_d):
            assert self._dtype == np.float64
        else:
            assert self._dtype == np.float32

        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ gs_enums.GeometryType")

        if isinstance(geom_type, gs_enums.GeometryType):
            _geom_type = geom_type.value
        else:
            _geom_type = geom_type
        out = Vector._from_vector(self._handle.region(_bbox, _geom_type))
        return out

    def nearest(self, other: "BoundingBox",
                geom_type: Union[numbers.Integral, "gs_enums.GeometryType"] = 7) -> "Vector":
        """Return vector geometries nearest to a region.

        Parameters
        ----------
        other : BoundingBox
            input region of type BoundingBox
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of vector geometry to return

        Returns
        -------
        Vector
            Vector object containing vector geometries within the
            given bounding box.

        Raises
        ------
        TypeError
            input argument should be an instance of BoundingBox
        TypeError
            geom_type should be int/ gs_enums.GeometryType
        """
        if not isinstance(other, (BoundingBox, _BoundingBox_d, _BoundingBox_f)):
            raise TypeError("other should be an instance of BoundingBox")
        _bbox = other._handle if isinstance(other, BoundingBox) else other
        if isinstance(_bbox, _BoundingBox_d):
            assert self._dtype == np.float64
        else:
            assert self._dtype == np.float32

        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ gs_enums.GeometryType")

        if isinstance(geom_type, gs_enums.GeometryType):
            geom_type = geom_type.value
        else:
            geom_type = geom_type

        out = Vector._from_vector(self._handle.nearest(_bbox, geom_type))
        return out

    def attached(self, inp_coordinate: "Coordinate",
                 geom_type: Union[numbers.Integral, "gs_enums.GeometryType"] = 7) -> "Vector":
        """Return vector geometries attached to the input coordinate.

        Parameters
        ----------
        inp_coordinate : Coordinate
            Input coordinate to obtain the attached vector geometry
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of vector geometry to return

        Returns
        -------
        Vector
            Vector object containing vector geometries attached to the
            input coordinate

        Raises
        ------
        TypeError
            inp_coordinate should be of type Coordinate
        TypeError
            geom_type should be int/ gs_enums.GeometryType
        AssertionError
            Datatype mismatch b/w Vector and input coordinate
        """
        if not isinstance(inp_coordinate, Coordinate):
            raise TypeError("inp_coordinate should be Coordinate")
        assert self._dtype == inp_coordinate._dtype, "DataType mismatch"
        if isinstance(geom_type, gs_enums.GeometryType):
            _geom_type = geom_type.value
        elif isinstance(geom_type, numbers.Integral):
            _geom_type = geom_type
        else:
            raise TypeError("geom_type should be int/ gs_enums.GeometryType")
        out = Vector._from_vector(self._handle.attached(inp_coordinate,
                                                        _geom_type))
        return out

    def deduplicateVertices(self):
        assert self._handle is not None, "Vector is not instantiated"
        self._handle.deduplicateVertices()

    def mapDistance(self, resolution: numbers.Real = None,
                    script: str = "",
                    geom_type: Union[numbers.Integral,
                                     "gs_enums.GeometryType"] = 7,
                    bounds: Optional['BoundingBox'] = None,
                    inp_raster: Optional[Union['raster.Raster', 'raster.RasterFile']]=None) -> "raster.Raster":
        """Return a distance map for the vector object.

        Parameters
        ----------
        resolution : numbers.Real
            resolution of the output raster
        script : str
            script to use for rasterise operation
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of geometry use for distance map.
        bounds : BoundingBox, optional
            bounding box to subset vector before creating
            distance map, by default None
        inp_raster: raster.Raster, optional
            a raster object to use for creating distance map, by default None

        Returns
        -------
        raster.Raster
            distance map raster

        Raises
        ------
        AssertionError
            Vector is not instantiated
        TypeError
            resolution should be numeric
        TypeError
            bounds should be an instance of boundingbox
        TypeError
            geom_type should be int/ gs_enums.GeometryType
        """
        if inp_raster is None:
            assert self._handle is not None, "Vector is not instantiated"
            if not isinstance(resolution, numbers.Real):
                raise TypeError("resolution should be numeric")
            _resolution = self._dtype(resolution)

            if bounds is None:
                _bounds = BoundingBox()
            else:
                if not isinstance(bounds, BoundingBox):
                    raise TypeError(
                        "bounds should be an instance of boundingbox")
                _bounds = bounds
        else:
            # handle case when a raster is provided
            if not isinstance(inp_raster, (raster.Raster, raster.RasterFile)):
                raise TypeError(
                    "inp_raster should be an instance of Raster/RasterFile")
            if not inp_raster.hasData():
                raise RuntimeError("Input raster cannot be empty")

        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ gs_enums.GeometryType")

        if isinstance(geom_type, gs_enums.GeometryType):
            if inp_raster is None:
                _out = self._handle.mapDistanceOnBounds(_resolution,
                                                        script=script, geom_type=geom_type.value, bounds=_bounds._handle)
            else:
                if isinstance(inp_raster, raster.Raster):
                    _out = self._handle.mapDistanceOnRaster(inp_raster._handle,
                                                            script=script, geom_type=geom_type.value)
                elif isinstance(inp_raster, raster.RasterFile):
                    _out = self._handle.mapDistanceOnRaster(inp_raster._handle.cy_raster_obj,
                                                            script=script, geom_type=geom_type.value)
        else:
            if inp_raster is None:
                _out = self._handle.mapDistanceOnBounds(_resolution,
                                                        script=script, geom_type=geom_type, bounds=_bounds._handle)
            else:
                if isinstance(inp_raster, raster.Raster):
                    _out = self._handle.mapDistanceOnRaster(inp_raster._handle,
                                                            script=script, geom_type=geom_type)
                elif isinstance(inp_raster, raster.RasterFile):
                    _out = self._handle.mapDistanceOnRaster(inp_raster._handle.cy_raster_obj,
                                                            script=script, geom_type=geom_type)
        out = raster.Raster.copy("rasterised", _out)
        out.setProjectionParameters(self.getProjectionParameters())
        return out

    def rasterise(self, resolution: numbers.Real,
                  script: str = "",
                  geom_type: Union[numbers.Integral,
                                   "gs_enums.GeometryType"] = 7,
                  bounds: Optional["BoundingBox"] = None) -> "raster.Raster":
        """Return a raster after rasterising the vector object.

        Parameters
        ----------
        resolution : numbers.Real
            resolution of the output raster
        script : str
            script to use for rasterise operation
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of geometry use for rasterise operation
        bounds : BoundingBox, optional
            bounding box to subset vector before creating
            distance map, by default None

        Returns
        -------
        raster.Raster
            raster generated from rasterise operation.

        Raises
        ------
        AssertionError
            Vector is not instantiated
        TypeError
            bounds should be an instance of boundingbox
        TypeError
            geom_type should be int/ gs_enums.GeometryType
        """
        assert self._handle is not None, "Vector is not instantiated"
        _resolution = self._dtype(resolution)

        if bounds is None:
            _bounds = BoundingBox(dtype=np.float32)
        else:
            if not isinstance(bounds, BoundingBox):
                raise TypeError("bounds should be an instance of boundingbox")
            _bounds = bounds

        if isinstance(script, str):
            _script = script.encode("UTF-8")
        elif isinstance(script, bytes):
            _script = script

        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ gs_enums.GeometryType")

        if isinstance(geom_type, gs_enums.GeometryType):
            _out = self._handle.rasterise(_resolution,
                                          _script, geom_type.value, _bounds._handle)
        else:
            _out = self._handle.rasterise(_resolution,
                                          _script, geom_type, _bounds._handle)
        out = raster.Raster.copy("rasterised", _out)
        out.setProjectionParameters(self.getProjectionParameters())
        return out

    def pointSample(self, other: "raster.Raster") -> bool:
        """Sample raster at the location of vector geometries.

        Parameters
        ----------
        other : raster.Raster
            input raster dataset for sampling.

        Returns
        -------
        bool
            True if raster was sampled False otherwise

        Raises
        ------
        TypeError
            Input argument should be of type Raster
        AssertionError
            Datatype mismatch
        """
        if not isinstance(other, (raster._cyRaster_d,
                                  raster._cyRaster_f,
                                  raster.Raster)):
            raise TypeError(
                "Input argument should be an instance of Raster class")
        _bbox = other._handle if isinstance(other, raster.Raster) else other

        if isinstance(_bbox, raster._cyRaster_d):
            assert self._dtype == np.float64, "Datatype mismatch"
        else:
            assert self._dtype == np.float32, "Datatype mismatch"
        return self._handle.pointSample(_bbox)

    def runScript(self, script: Union[str, bytes]):
        """
        Method to run script on the Vector object.

        Parameters
        ----------
        script: Union[str, bytes]
             Script to run on the Vector object over GPU.

        Returns
        -------
        Nil
        """
        if isinstance(script, str):
            self._handle.runScript(script.encode('utf-8'))
        elif isinstance(script, bytes):
            self._handle.runScript(script)
        else:
            raise TypeError(
                f"Invalid data type {type(script)} for script, it should be of type str/bytes")

    def getBounds(self) -> "BoundingBox":
        """get the bounds of the Vector object

        Returns
        -------
        BoundingBox
            an instance of BoundingBox object
        """
        if self._handle is not None:
            return BoundingBox.from_bbox(self._handle.getBounds())

    @property
    def bounds(self) -> "BoundingBox":
        return self.getBounds()

    def hasData(self) -> bool:
        """check if the Vector object has data.

        Returns
        -------
        bool
            True if the Vector object has data, False otherwise
        """
        return self._handle.hasData()

    @staticmethod
    def from_geojson(fileName: Union[Dict, str], dtype: np.dtype = np.float32) -> "Vector":
        """read a geojson to a Vector object

        Parameters
        ----------
        fileName : Union[Dict, str]
            name of the file or a dictionary
        dtype : np.dtype, optional
            data type for the vector object, by default np.float32

        Returns
        -------
        Vector
            a vector object initialized from the geojson
        """
        return io.geoJsonToVector(fileName, dtype=dtype)

    def to_geojson(self, fileName: Union[str, StringIO] = None,
                   enforceProjection: bool = True, writeNullProperties: bool = True) -> Union[None, str]:
        """get a geojson representation of a Vector object

        Parameters
        ----------
        fileName : Union[str, StringIO], optional
            name and path of the file, by default None
        enforceProjection : bool, optional
            flag to enforce projection of geojson to EPSG:4326, by default True
        writeNullProperties : bool, optional
            flag to write properties as null when the property is not present for a geometry, by default True

        Returns
        -------
        Union[None, str]
            a geojson string or None (when written to a file)
        """
        if fileName is None:
            out = json.loads(io.vectorToGeoJson(self,
                             enforceProjection=enforceProjection,
                             writeNullProperties=writeNullProperties))
            return out
        else:
            if isinstance(fileName, str):
                with open(fileName, 'w') as out:
                    out.write(io.vectorToGeoJson(self,
                              enforceProjection=enforceProjection,
                              writeNullProperties=writeNullProperties))
            else:
                fileName.write(io.vectorToGeoJson(self,
                                                  enforceProjection=enforceProjection,
                                                  writeNullProperties=writeNullProperties))

    def to_csv(self, filename: str = None) -> Union[StringIO, None]:
        """write a Vector object to a csv object

        Returns
        -------
        Union[None, StringIO]
            a csv containing the geometeries from the Vector object
        """
        return io.vectorToCSV(self, filename=filename)

    def to_geowkt(self) -> str:
        """get a geowkt representation of the Vector object

        Returns
        -------
        str
            a geoWKT containing the geometeries from the Vector object
        """
        return io.vectorToGeoWKT(self)

    @staticmethod
    def from_shapefile(fileName: str, dtype: np.dtype = np.float32):
        return io.shapefileToVector(fileName, dtype=dtype)

    def to_shapefile(self, fileName: str, geom_type: Optional["gs_enums.GeometryType"]=None):
        out = io.vectorToShapefile(self, fileName, geom_type=geom_type)
        return out

    def getPointCount(self) -> numbers.Integral:
        return self._handle.getPointCount()

    def getLineStringCount(self) -> numbers.Integral:
        return self._handle.getLineStringCount()

    def getPolygonCount(self) -> numbers.Integral:
        return self._handle.getPolygonCount()

    def getGeometryBaseCount(self) -> numbers.Integral:
        return self._handle.getGeometryBaseCount()

    @property
    def __geo_interface__(self, enforceProjection: bool = True):
        return io.vectorToGeoJson(self,
                                  enforceProjection=enforceProjection)

    def __eq__(self, other):
        if not isinstance(other, Vector):
            raise TypeError("input argument should be a Vector")
        assert self._dtype == other._dtype, "Type mismatch for Vectors"
        return self._handle == other._handle

    def __ne__(self, other):
        if not isinstance(other, Vector):
            raise TypeError("input argument should be a Vector")
        assert self._dtype == other._dtype, "Type mismatch for Vectors"
        return self._handle != other._handle

    @staticmethod
    def from_geopandas(other, dtype: np.dtype = np.float32):
        obj = readers.vectorReaders.from_geopandas(other, dtype=dtype)
        return obj

    @staticmethod
    def from_fiona(other, dtype: np.dtype = np.float32):
        obj = readers.vectorReaders.from_fiona(other, dtype=dtype)
        return obj

    @staticmethod
    def from_pyshp(other, dtype: np.dtype = np.float32):
        obj = readers.vectorReaders.from_pyshp(other, dtype=dtype)
        return obj

    @staticmethod
    def from_ogr(other, dtype: np.dtype = np.float32):
        obj = readers.vectorReaders.from_ogr(other, dtype=dtype)
        return obj

    def to_geopandas(self, **kwargs):
        obj = writers.vectorWriters.to_geopandas(self, **kwargs)
        return obj

    def __iadd__(self, other):
        if isinstance(other, Vector):
            assert self._dtype == other._dtype, "Type mismatch for Vectors"
            self._handle += other._handle
        elif any([supported_libs.HAS_GDAL, supported_libs.HAS_GPD,
                  supported_libs.HAS_FIONA, supported_libs.HAS_PYSHP]):
            obj = None
            if supported_libs.HAS_GDAL:
                if isinstance(other, (ogr.DataSource, ogr.Layer)):
                    obj = Vector.from_ogr(other, dtype=self._dtype)
            elif supported_libs.HAS_FIONA:
                if isinstance(other, fiona.Collection):
                    obj = Vector.from_ogr(other, dtype=self._dtype)
            elif supported_libs.HAS_PYSHP:
                if isinstance(other, shapefile.Reader):
                    obj = Vector.from_shapefile(other, dtype=self._dtype)
            elif supported_libs.HAS_GPD:
                if isinstance(other, (gpd.GeoDataFrame, gpd.GeoSeries)):
                    obj = Vector.from_geopandas(other, dtype=self._dtype)
            if obj is not None:
                if self.getProjectionParameters() == other.getProjectionParameters():
                    self._handle += obj._handle
                else:
                    raise ValueError(
                        "Mismatch in projection parameters of vector objects")
        else:
            raise TypeError("input argument should be a Vector")
        return self

    def __repr__(self):
        bounds_string = "\nBounding Box:\n%s" % str(self.getBounds())
        proj_string = "\nProjection Parameters:\n%s" % str(
            self.getProjectionParameters())
        geom_string = f"\nGeometry:\n    {'Points':10s}:{max(0, self.getPointCount()):4d}\n"
        geom_string += f"    {'LineString':10s}:{max(0, self.getLineStringCount()):4d}\n"
        geom_string += f"    {'Polygon':10s}:{max(0, self.getPolygonCount()):4d}"

        return "<class 'geostack.vector.%s'>%s%s%s" % (self.__class__.__name__,
                                                       geom_string,
                                                       bounds_string,
                                                       proj_string)
