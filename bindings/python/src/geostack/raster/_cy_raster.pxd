# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3

from cython.operator import dereference as deref
from libcpp.string cimport string
from libcpp cimport bool
from libcpp.map cimport map as cpp_map
from libcpp.set cimport set as cpp_set
from libcpp.set cimport pair as cpp_pair
from libcpp.vector cimport vector
from libc.stdint cimport uint8_t, uint32_t, int32_t, uint64_t, int64_t
from libc.stdio cimport printf
from libcpp.functional cimport function
from libcpp.iterator cimport iterator
from cpython.pycapsule cimport PyCapsule_IsValid, PyCapsule_GetPointer, PyCapsule_New
import numpy as np
cimport cython
cimport numpy as np
from numpy cimport npy_intp, PyArray_SimpleNewFromData
from libcpp.memory cimport shared_ptr, unique_ptr, make_shared, default_delete
from ..core._cy_property cimport PropertyType, PropertyStructure, PropertyMap, _PropertyMap
from ..vector._cy_vector cimport (_Vector_d, _Vector_f,
                                  _BoundingBox_d, _BoundingBox_f)
from ..core._cy_projection cimport _ProjectionParameters_d

np.import_array()

ctypedef fused float_t:
    double
    float

ctypedef fused int_t:
    int32_t
    int

cdef extern from "utils.h":
    void cy_copy[T](T& a, T& b) nogil

cdef extern from "<iostream>" namespace "std" nogil:
    cdef cppclass ostream:
        ostream& write(const char*, int) except +
    cdef cppclass istream:
        istream& read(const char*, int) except +
    cdef ostream cout
    cdef istream cin

cdef extern from "<iostream>" namespace "std::ios_base" nogil:
    cdef cppclass openmode:
        pass
    cdef cppclass seekdir:
        pass
    cdef openmode ios_binary "std::ios::binary"
    cdef openmode ios_in "std::ios::in"
    cdef openmode ios_out "std::ios::out"
    cdef openmode ios_app "std::ios::app"

    cdef seekdir seek_beg "std::ios::beg"
    cdef seekdir seek_end "std::ios::end"
    cdef seekdir seek_cur "std::ios::cur"

    bool sync_with_stdio(bool) except+

cdef extern from "<fstream>" namespace "std" nogil:
    cdef cppclass ofstream(ostream):
        # constructors
        ofstream(const char*) except+
        ofstream(const char*, openmode) except+
        ofstream& write(char*, int n) except+
        ofstream& flush() except+
        open(const char*, openmode) except+
        void close() except+
        bool is_open() except+
        # using except+ for operator<< gives invalid c++ code
        ofstream& operator<< (int& val)
        ofstream& operator<< (float& val)
        ofstream& operator<< (void*& val)
        bint operator bool() except+
        bint operator!() except+

    cdef cppclass ifstream(istream):
        ifstream(const char*) except+
        ifstream(const char*, openmode) except+
        open(const char*, openmode) except+
        void close() except+
        bool is_open() except+
        # using except+ for operator>> gives invalid c++ code
        ifstream& operator>> (int& val)
        ifstream& operator>> (float& val)
        ifstream& operator>> (void*& val)
        bint operator bool() except+
        bint operator!() except+

    cdef cppclass fstream:
        fstream(const char*) except+
        fstream(const char*, openmode) except+
        open(const char*, openmode) except+
        bool is_open() except+
        fstream& operator=(const fstream&&) except+
        ifstream& read(char*, int n) except+
        # using except+ for operator>> gives invalid c++ code
        ifstream& operator>> (int& val)
        ifstream& operator>> (float& val)
        ifstream& operator>> (void*& val)
        ofstream& write(char*, int n) except+
        # using except+ for operator<< gives invalid c++ code
        ofstream& operator<< (int& val)
        ofstream& operator<< (float& val)
        ofstream& operator<< (void*& val)
        ofstream& flush() except+
        void close() except+
        bool good() except+
        bool eof() except+
        bint operator bool() except+
        bint operator!() except+

cdef extern from "<functional>" namespace "std" nogil:
    cdef cppclass reference_wrapper[T]:
        # reference_wrapper() except +
        reference_wrapper(T& ref) except +
        reference_wrapper(T&&) except +
        reference_wrapper(reference_wrapper[T]&) except +
        reference_wrapper& get() except +
        T& get() except +
        reference_wrapper operator=(reference_wrapper&) except +
        reference_wrapper& operator[]() except +
    reference_wrapper[T] cpp_ref "std::ref" [T](T& t) except +
    reference_wrapper[T] cpp_ref "std::ref" [T](reference_wrapper[T] t) except +

cdef extern from "gs_projection.h" namespace "Geostack":
    cdef cppclass ProjectionParameters[T]:
        pass

cdef extern from "gs_solver.h" namespace "Geostack":
    T getNullValue[T]() nogil

cdef extern from "gs_vector.h" namespace "Geostack":
    cdef cppclass Vector[T]:
        pass

    cdef cppclass Coordinate[T]:
        Coordinate() except +
        Coordinate(Coordinate[T] &c) except +
        Coordinate(T p, T q) except +
        T magnitudeSquared() except +
        Coordinate[T] max_c "max"(Coordinate[T] &a, Coordinate[T] &b) except +
        Coordinate[T] min_c "min"(Coordinate[T] &a, Coordinate[T] &b) except +
        Coordinate[T] centroid(Coordinate[T] &a, Coordinate[T] &b) except +
        Coordinate[T]& operator=(Coordinate[T] &c) except +
        T p, q, r, s

    bool operator==[T](Coordinate[T] &a, Coordinate[T] &b) except +
    bool operator!=[T](Coordinate[T] &a, Coordinate[T] &b) except +
    Coordinate[T]& operator+[T](Coordinate[T] &a, Coordinate[T] &b) except +
    Coordinate[T]& operator-[T](Coordinate[T] &a, Coordinate[T] &b) except +

    cdef cppclass BoundingBox[T]:
        BoundingBox() except +
        BoundingBox(Coordinate[T], Coordinate[T]) except +
        Coordinate[T] min_c "min"
        Coordinate[T] max_c "max"
        void reset()
        void extend(T)
        void extend(Coordinate[T])
        void extent(const BoundingBox[T] &b)
        T area()
        T diameterSqr()
        T centroidDistanceSqr(const BoundingBox[T] &b)
        Coordinate[T] centroid()
        Coordinate[T] extent()
        uint64_t createZIndex(Coordinate[T] c)

    cdef cppclass GeometryBase[T]:
        BoundingBox[T] getBounds()
        bool isContainer()

cdef extern from "gs_raster.h" namespace "Geostack":

    cdef cppclass dataHandlerFunction[R, C]:
        pass

    cdef cppclass Tile[R, C](GeometryBase[C]):
        Tile() nogil except +
        Tile(const Tile &r) nogil except +
        bool init(int32_t ti_, int32_t tj_, RasterDimensions[C] rdim_)
        TileDimensions[C] getDimensions()
        BoundingBox[C] getBounds()
        bool isType(size_t typeMask)
        bool operator==[R, C](const Tile[R, C] &l, const Tile[R, C] &r)
        bool operator!=[R, C](const Tile[R, C] &l, const Tile[R, C] &r)
        R& operator[](uint32_t i, uint32_t j, uint32_t k)
        bool hasData()
        void setAllCellValues(R val)
        R max()
        R min()

    cdef cppclass RasterFileHandler[R, C]:
        RasterFileHandler() nogil except +
        RasterFileHandler(RasterFileHandler &) nogil except +
        void read(string fileName, Raster[R, C] &r, string jsonConfig) except+
        void write(string fileName, Raster[R, C] &r, string jsonConfig) except+
        const dataHandlerFunction[R, C]& getDataHandler()
        Raster[R, C] &r
        shared_ptr[fstream] fileStream

    cdef cppclass RasterBase[C](PropertyMap):
        RasterBase() nogil except +
        RasterBase(const RasterBase &Rb) nogil except +
        RasterDimensions[C] getRasterDimensions() except+
        BoundingBox[C] getBounds() except+
        bool hasData() except+
        void deleteRasterData() nogil except +
        void setProjectionParameters(ProjectionParameters[double] proj_)
        void setInterpolationType(size_t interpolation_)
        ProjectionParameters[double] getProjectionParameters()
        R getVariableData[R](string name) except+
        void setVariableData[R](string name, R value) except+
        cpp_map[string, size_t]& getVariablesIndexes() except+
        string getVariablesType() except+
        bool hasVariables() except+
        string getDataTypeString() except+

    cdef cppclass Dimensions[C]:
        uint32_t nx
        uint32_t ny
        uint32_t nz
        C hx
        C hy
        C hz
        C ox
        C oy
        C oz
        uint32_t mx
        uint32_t my

    cdef cppclass TileDimensions[C]:
        Dimensions[C] d
        C ex
        C ey
        C ez
        uint32_t ti
        uint32_t tj

    cdef cppclass RasterDimensions[C]:
        Dimensions[C] d
        C ex
        C ey
        C ez
        uint32_t tx
        uint32_t ty

    bool operator==[R, C](const Tile[R, C] &l, const Tile[R, C] &r)
    bool operator!=[R, C](const Tile[R, C] &l, const Tile[R, C] &r)
    bool equalSpatialMetrics[T](const Dimensions[T] l, const Dimensions[T] r)

cdef extern from "gs_raster.h" namespace "Geostack::RasterCombination":
    cdef enum RasterCombinationType "Geostack::RasterCombination::Type":
        Union = 0
        Intersection = 1 << 0

cdef extern from "gs_raster.h" namespace "Geostack::RasterResolution":
    cdef enum RasterResolutionType "Geostack::RasterResolution::Type":
        Resolution_MIN "Minimum" = 0
        Resolution_MAX "Maximum" = 1 << 2

cdef extern from "gs_raster.h" namespace "Geostack::RasterInterpolation":
    cdef enum RasterInterpolationType "Geostack::RasterInterpolation::Type":
        Nearest = 0
        Bilinear = 1 << 4
        Bicubic = 2 << 4

cdef extern from "gs_raster.h" namespace "Geostack::RasterDebug":
    cdef enum RasterDebugType "Geostack::RasterDebug::Type":
        NoDebug "Geostack::RasterDebug::Type::None" = 0 << 12
        Enable = 1 << 12

cdef extern from "gs_raster.h" namespace "Geostack::RasterNullValue":
    cdef enum RasterNullValueType "Geostack::RasterNullValue::Type":
        Null = 0
        Zero = 1 << 6
        One = 2 << 6

cdef extern from "gs_raster.h" namespace "Geostack::GeometryType":
    cdef enum GeometryType "Geostack::GeometryType::Type":
        NoType "Geostack::GeometryType::Type::None" = 0
        Point = 1
        LineString = 1 << 1
        Polygon = 1 << 2
        TileType "Geostack::GeometryType::Type::Tile" = 1 << 3

cdef extern from "gs_raster.h" namespace "Geostack::Neighbours":
    cdef enum NeighboursType "Geostack::Neighbours::Type":
        NoNeighbour "None" = 0x00
        N  = 1 << 0
        NE = 1 << 1
        E  = 1 << 2
        S =  1 << 4
        SW = 1 << 5
        W =  1 << 6
        NW = 1 << 7
        Rook = 0x01
        Bishop = 0x02
        Queen = 0x03

cdef extern from "gs_raster.h" namespace "Geostack::Reduction":
    cdef enum ReductionType "Geostack::Reduction::Type":
        NoReduction "None" = 0
        Reduction_MAX "Maximum" = 1 << 8
        Reduction_MIN "Minimum" = 2 << 8
        Reduction_SUM "Sum" = 3 << 8
        Reduction_COUNT "Count" = 4 << 8
        Reduction_MEAN "Mean" = 5 << 8
        Reduction_SumSquares "SumSquares" = 6 << 8

ctypedef cpp_set[cpp_pair[uint32_t, uint32_t]] tileIndexSet

cdef extern from "gs_raster.h" namespace "Geostack":

    cdef cppclass Raster[R, C](RasterBase[C]):
        Raster() nogil except +
        Raster(string) nogil except +
        Raster(const Raster &r) nogil except+
        Raster& operator=(const Raster &r)
        bool init(Dimensions[C] &dim) except +
        bool init(BoundingBox[C] bounds, C hx, C hy, C hz) except +
        bool init2D(uint32_t nx_, uint32_t ny_, C hx_, C hy_,
                    C ox_, C oy_) nogil except +
        bool init(uint32_t nx_, uint32_t ny_, uint32_t nz_,
                  C hx_, C hy_, C hz_, C ox_, C oy_, C oz_) nogil except +
        bool resize2D(uint32_t nx, uint32_t ny, uint32_t tox, uint32_t toy) nogil except +
        tileIndexSet resize2DIndexes(uint32_t nx, uint32_t ny, uint32_t tox, uint32_t toy) nogil except +
        R max() nogil
        R min() nogil
        R reduce() nogil except +
        R getCellValue(uint32_t i, uint32_t j, uint32_t k) nogil
        R getNearestValue(C x, C y, C z) nogil
        R getBilinearValue(C x, C y, C z) nogil
        void setAllCellValues(R c) nogil
        void setCellValue(R v, uint32_t i, uint32_t j, uint32_t k) nogil
        TileDimensions[C] getTileDimensions(uint32_t i, uint32_t j) nogil
        void setTileData(uint32_t ti, uint32_t tj, vector[R] &data) nogil except+
        void getTileData(uint32_t ti, uint32_t tj, vector[R] &data) nogil except+
        void mapVector(Vector[C] &v, string script, size_t parameters,
            string widthPropertyName, string levelPropertyName) nogil except+
        void rasterise(Vector[C] &v, string script, size_t parameters) nogil except+
        Vector[C] cellCentres() nogil
        Vector[C] vectorise(vector[R] contourValue, R noDataValue) nogil
        bool operator==[R, C](Raster[R, C] &l, Raster[R, C] &r)
        Raster[R, C] region(BoundingBox[C] bounds)
        void read(string fileName, string jsonConfig) except+
        void write(string fileName, string jsonConfig) except+
        uint8_t getRequiredNeighbours()
        void setRequiredNeighbours(uint8_t requiredNeighbours)
        ReductionType getReductionType()
        void setReductionType(ReductionType requiredReduction)
        bool getNeedsStatus()
        void setNeedsStatus(bool needsStatus_)
        bool getNeedsWrite()
        void setNeedsWrite(bool needsWrite_)
        void setFileInputHandler(shared_ptr[pyFileHandler[R, C]]) except +
        void setFileOutputHandler(shared_ptr[pyFileHandler[R, C]]) except +
        void setFileInputHandler(shared_ptr[pyFileHandler_i[C]]) except +
        void setFileOutputHandler(shared_ptr[pyFileHandler_i[C]]) except +

    bool operator==[R, C](const Raster[R, C] &l, const Raster[R, C] &r)
    void sortColumns[R](RasterBase[R] &r) nogil except+


cdef extern from "pyFileHandler.h" namespace "Geostack::pyGeostack":

    ctypedef struct pyException:
        int rc
        string errMessage

    cdef cppclass pyFileHandler[R, C]:
        ctypedef pyException (*data_reader) (void*, TileDimensions[C], vector[R]&)
        pyFileHandler(data_reader py_f, void* py_class, string filename) nogil except +
        void dataFunction(TileDimensions[C] tdim, vector[R] &v) nogil except +
        string getFileName() nogil except +

    cdef cppclass pyFileHandler_i[C]:
        ctypedef pyException (*data_reader_i) (void*, TileDimensions[C], vector[uint32_t]&)
        pyFileHandler_i(data_reader_i py_f, void* py_class, string filename) nogil except +
        void dataFunction(TileDimensions[C] tdim, vector[uint32_t] &v) nogil except +
        string getFileName() nogil except +

    void add_ref_to_vec[T](vector[reference_wrapper[RasterBase[T]]]&,
        RasterBase[T]&) except+
    RasterBase[T]& get_raster_ref_from_vec[T](vector[reference_wrapper[RasterBase[T]]]&, size_t i) except+
    void add_raster_ptr_to_vec[R, C](vector[shared_ptr[RasterBase[C]]]&,
        shared_ptr[Raster[R, C]]&) except+
    shared_ptr[RasterBase[C]] get_raster_ptr_from_vec[C](vector[shared_ptr[RasterBase[C]]]&, size_t i) except+

cdef extern from "gs_raster.h" namespace "Geostack::TileMetrics":
    cdef uint32_t tileSize
    cdef uint32_t tileSizePower
    cdef uint32_t tileSizeSquared
    cdef uint32_t tileSizeMask

ctypedef enum RasterKind:
    Raster1D = 1
    Raster2D = 2
    Raster3D = 3

ctypedef reference_wrapper[RasterBase[double]] RasterBaseRef_d
ctypedef reference_wrapper[RasterBase[float]] RasterBaseRef_f
ctypedef shared_ptr[RasterBase[double]] RasterBasePtr_d
ctypedef shared_ptr[RasterBase[float]] RasterBasePtr_f
ctypedef vector[RasterBaseRef_d] raster_base_list_d
ctypedef vector[RasterBaseRef_f] raster_base_list_f
ctypedef vector[RasterBasePtr_d] raster_ptr_list_d
ctypedef vector[RasterBasePtr_f] raster_ptr_list_f
ctypedef RasterDimensions[double] raster_dimensions_d
ctypedef RasterDimensions[float] raster_dimensions_f
ctypedef TileDimensions[double] tile_dimensions_d
ctypedef TileDimensions[float] tile_dimensions_f

ctypedef pyException (*data_reader_f) (void*, TileDimensions[float], vector[float]&)
ctypedef pyException (*data_reader_f_i) (void*, TileDimensions[float], vector[uint32_t]&)
ctypedef pyException (*data_reader_d) (void*, TileDimensions[double], vector[double]&)
ctypedef pyException (*data_reader_d_i) (void*, TileDimensions[double], vector[uint32_t]&)

cpdef double getNullValue_dbl()
cpdef float getNullValue_flt()
cpdef uint32_t getNullValue_uint32()
cpdef uint64_t getNullValue_uint64()
cpdef int32_t getNullValue_int32()
cpdef int64_t getNullValue_int64()

cdef class _Dimensions_d:
    cdef Dimensions[double] *thisptr
    cdef void c_copy(self, Dimensions[double] inp_dims)

cdef class _Dimensions_f:
    cdef Dimensions[float] *thisptr
    cdef void c_copy(self, Dimensions[float] inp_dims)

cdef class _RasterDimensions_d:
    cdef raster_dimensions_d *thisptr
    cdef void c_copy(self, raster_dimensions_d inp_dims)

cdef class _RasterDimensions_f:
    cdef raster_dimensions_f *thisptr
    cdef void c_copy(self, raster_dimensions_f inp_dims)

cdef class _TileDimensions_d:
    cdef tile_dimensions_d *thisptr
    cdef uint32_t n, nz, lnx, lny
    cdef uint32_t ti, tj
    cdef double hx, hy, hz, ox, oy, oz, ex, ey, ez
    cdef void c_copy(self, tile_dimensions_d inp_dims)

cdef class _TileDimensions_f:
    cdef tile_dimensions_f *thisptr
    cdef uint32_t n, nz, lnx, lny
    cdef uint32_t ti, tj
    cdef float hx, hy, hz, ox, oy, oz, ex, ey, ez
    cdef void c_copy(self, tile_dimensions_f inp_dims)

cdef class _cyRasterBase_d(_PropertyMap):
    cdef RasterBase[double] *baseptr
    cpdef _BoundingBox_d getBounds(self) except+
    cpdef _RasterDimensions_d getRasterDimensions(self) except+
    cpdef void setProjectionParameters(self, _ProjectionParameters_d proj_)
    cpdef void setInterpolationType(self, size_t interpolation_)
    cpdef _ProjectionParameters_d getProjectionParameters(self)
    cpdef int getRasterKind(self)
    cpdef double getVariableData(self, string name) except+
    cpdef void setVariableData(self, string name, double value) except+
    cpdef dict getVariablesIndexes(self) except+
    cpdef string getVariablesType(self) except+
    cpdef bool hasVariables(self) except+
    cpdef void deleteRasterData(self) except+
    cpdef string getDataTypeString(self) except+

cdef class _cyRasterBase_f(_PropertyMap):
    cdef RasterBase[float] *baseptr
    cpdef _BoundingBox_f getBounds(self) except+
    cpdef _RasterDimensions_f getRasterDimensions(self) except+
    cpdef void setProjectionParameters(self, _ProjectionParameters_d proj_)
    cpdef void setInterpolationType(self, size_t interpolation_)
    cpdef _ProjectionParameters_d getProjectionParameters(self)
    cpdef int getRasterKind(self)
    cpdef float getVariableData(self, string name) except+
    cpdef void setVariableData(self, string name, float value) except+
    cpdef dict getVariablesIndexes(self) except+
    cpdef string getVariablesType(self) except+
    cpdef bool hasVariables(self) except+
    cpdef void deleteRasterData(self) except+
    cpdef string getDataTypeString(self) except+

cdef class _cyRaster_f(_cyRasterBase_f):
    cdef shared_ptr[Raster[float, float]] sh_ptr
    cdef float[:] data1D
    cdef float[:, :] data2D
    cdef float[:, :, :] data3D
    cdef uint32_t nx_, ny_, nz_
    cdef float hx_, hy_, hz_
    cdef float ox_, oy_, oz_
    cdef void init_with_raster_dimensions(self, _RasterDimensions_f d)  except *
    cdef void init_with_bbox(self, _BoundingBox_f b, float hx, float hy, float hz)  except *
    cpdef void init1D(self, uint32_t nx_, float hx_, float ox_=?)  except *
    cpdef void init2D(self, uint32_t nx_, uint32_t ny_, float hx_, float hy_,
                      float ox_=?, float oy_=?)  except *
    cpdef void init3D(self, uint32_t nx_, uint32_t ny_, uint32_t nz_,
                      float hx_, float hy_, float hz_, float ox_=?, float oy_=?,
                      float oz_=?)  except *
    cdef void c_rastercopy(self, Raster[float, float] &other) except*
    cpdef float getCellValue(self, uint32_t i, uint32_t j=?, uint32_t k=?) except+
    cpdef float getNearestValue(self, float x, float y=?, float z=?) except+
    cpdef float getBilinearValue(self, float x, float y=?, float z=?) except+
    cpdef void setCellValue(self, float value, uint32_t i, uint32_t j=?,
                            uint32_t k=?) except+
    cpdef void setAllCellValues(self, float c) except+
    cpdef void set1D(self, float[:, :] inp, int ti=?, int tj=?) except+
    cpdef void set2D(self, float[:, :] inp, int ti=?, int tj=?) except+
    cpdef void set3D(self, float[:, :, :] inp, int ti=?, int tj=?) except+
    cpdef float[:] get_data_1d(self, uint32_t ti=?, uint32_t tj=?) except+
    cpdef float[:, :] get_data_2d(self, uint32_t ti=?, uint32_t tj=?) except+
    cpdef float[:, :, :] get_data_3d(self, uint32_t ti=?, uint32_t tj=?) except+
    cdef void get_tile_data(self, uint32_t ti, uint32_t tj, vector[float]& vec) except+
    cpdef float maxVal(self) except+
    cpdef float minVal(self) except+
    cpdef float reduceVal(self) except+
    cpdef void mapVector(self, _Vector_f v, string script, size_t parameters,
        string widthPropertyName, string levelPropertyName) except+
    cpdef void rasterise(self, _Vector_f v, string script, size_t parameters) except+
    cpdef _Vector_f cellCentres(self) except+
    cpdef _Vector_f vectorise(self, float[:] contourValue, object noDataValue=?) except+
    cpdef void read(self, string fileName, string jsonConfig) except+
    cpdef void write(self, string fileName, string jsonConfig) except+
    cpdef bool hasData(self) except+
    cpdef uint8_t getRequiredNeighbours(self) except+
    cpdef void setRequiredNeighbours(self, uint8_t requiredNeighbours) except+
    cpdef ReductionType getReductionType(self) except+
    cpdef void setReductionType(self, ReductionType other) except+
    cpdef bool getNeedsStatus(self) except+
    cpdef void setNeedsStatus(self, bool other) except+
    cpdef bool getNeedsWrite(self) except+
    cpdef void setNeedsWrite(self, bool other) except+
    cpdef bool resize2D(self, uint32_t nx, uint32_t ny, uint32_t tox, uint32_t toy)
    cpdef _cyRaster_f region(self, _BoundingBox_f bounds)
    cpdef _TileDimensions_f getTileDimensions(self, uint32_t ti=?, uint32_t tj=?) except+

cdef class _cyRaster_f_i(_cyRasterBase_f):
    cdef shared_ptr[Raster[uint32_t, float]] sh_ptr
    cdef uint32_t[:] data1D
    cdef uint32_t[:, :] data2D
    cdef uint32_t[:, :, :] data3D
    cdef uint32_t nx_, ny_, nz_
    cdef float hx_, hy_, hz_
    cdef float ox_, oy_, oz_
    cdef void init_with_raster_dimensions(self, _RasterDimensions_f d)  except *
    cdef void init_with_bbox(self, _BoundingBox_f b, float hx, float hy, float hz)  except *
    cpdef void init1D(self, uint32_t nx_, float hx_, float ox_=?)  except *
    cpdef void init2D(self, uint32_t nx_, uint32_t ny_, float hx_, float hy_,
                      float ox_=?, float oy_=?)  except *
    cpdef void init3D(self, uint32_t nx_, uint32_t ny_, uint32_t nz_, float hx_,
                      float hy_, float hz_, float ox_=?, float oy_=?, float oz_=?)  except *
    cdef void c_rastercopy(self, Raster[uint32_t, float] &other) except*
    cpdef uint32_t maxVal(self) except+
    cpdef uint32_t minVal(self) except+
    cpdef uint32_t reduceVal(self) except+
    cpdef void set1D(self, uint32_t[:, :] inp, int ti=?, int tj=?) except+
    cpdef void set2D(self, uint32_t[:, :] inp, int ti=?, int tj=?) except+
    cpdef void set3D(self, uint32_t[:, :, :] inp, int ti=?, int tj=?) except+
    cpdef uint32_t[:] get_data_1d(self, uint32_t ti=?, uint32_t tj=?) except+
    cpdef uint32_t[:, :] get_data_2d(self, uint32_t ti=?, uint32_t tj=?) except+
    cpdef uint32_t[:, :, :] get_data_3d(self, uint32_t ti=?, uint32_t tj=?) except+
    cdef void get_tile_data(self, uint32_t ti, uint32_t tj, vector[uint32_t]& vec) except+
    cpdef void mapVector(self, _Vector_f v, string script, size_t parameters,
        string widthPropertyName, string levelPropertyName) except+
    cpdef void rasterise(self, _Vector_f v, string script, size_t parameters) except+
    cpdef _Vector_f cellCentres(self) except+
    cpdef _Vector_f vectorise(self, uint32_t[:] contourValue, object noDataValue=?) except+
    cpdef uint32_t getCellValue(self, uint32_t i, uint32_t j=?, uint32_t k=?) except+
    cpdef uint32_t getNearestValue(self, float x, float y=?, float z=?) except+
    # cpdef uint32_t getBilinearValue(self, float x, float y=?, float z=?)
    cpdef void setCellValue(self, uint32_t value, uint32_t i, uint32_t j=?,
                            uint32_t k=?) except+
    cpdef void setAllCellValues(self, uint32_t c) except+
    cpdef void read(self, string fileName, string jsonConfig) except+
    cpdef void write(self, string fileName, string jsonConfig) except+
    cpdef bool hasData(self) except+
    cpdef uint8_t getRequiredNeighbours(self) except+
    cpdef void setRequiredNeighbours(self, uint8_t requiredNeighbours) except+
    cpdef ReductionType getReductionType(self) except+
    cpdef void setReductionType(self, ReductionType other) except+
    cpdef bool getNeedsStatus(self) except+
    cpdef void setNeedsStatus(self, bool other) except+
    cpdef bool getNeedsWrite(self) except+
    cpdef void setNeedsWrite(self, bool other) except+
    cpdef bool resize2D(self, uint32_t nx, uint32_t ny, uint32_t tox, uint32_t toy)
    cpdef _cyRaster_f_i region(self, _BoundingBox_f bounds)
    cpdef _TileDimensions_f getTileDimensions(self, uint32_t ti=?, uint32_t tj=?) except+

cdef class _cyRaster_d(_cyRasterBase_d):
    cdef shared_ptr[Raster[double, double]] sh_ptr
    cdef double[:] data1D
    cdef double[:, :] data2D
    cdef double[:, :, :] data3D
    cdef uint32_t nx_, ny_, nz_
    cdef double hx_, hy_, hz_
    cdef double ox_, oy_, oz_
    cdef void init_with_raster_dimensions(self, _RasterDimensions_d d)  except *
    cdef void init_with_bbox(self, _BoundingBox_d b, double hx, double hy, double hz)  except *
    cpdef void init1D(self, uint32_t nx_, double hx_, double ox_=?)  except *
    cpdef void init2D(self, uint32_t nx_, uint32_t ny_, double hx_, double hy_,
                      double ox_=?, double oy_=?)  except *
    cpdef void init3D(self, uint32_t nx_, uint32_t ny_, uint32_t nz_, double hx_,
                      double hy_, double hz_, double ox_=?, double oy_=?,
                      double oz_=?)  except *
    cdef void c_rastercopy(self, Raster[double, double] &other) except*
    cpdef double getCellValue(self, uint32_t i, uint32_t j=?, uint32_t k=?) except+
    cpdef double getNearestValue(self, double x, double y=?, double z=?) except+
    cpdef double getBilinearValue(self, double x, double y=?, double z=?) except+
    cpdef void setCellValue(self, double value, uint32_t i, uint32_t j=?,
                            uint32_t k=?) except+
    cpdef void setAllCellValues(self, double c) except+
    cpdef void set1D(self, double[:, :] inp, int ti=?, int tj=?) except+
    cpdef void set2D(self, double[:, :] inp, int ti=?, int tj=?) except+
    cpdef void set3D(self, double[:, :, :] inp, int ti=?, int tj=?) except+
    cpdef double[:] get_data_1d(self, uint32_t ti=?, uint32_t tj=?) except+
    cpdef double[:, :] get_data_2d(self, uint32_t ti=?, uint32_t tj=?) except+
    cpdef double[:, :, :] get_data_3d(self, uint32_t ti=?, uint32_t tj=?) except+
    cdef void get_tile_data(self, uint32_t ti, uint32_t tj, vector[double]& vec) except+
    cpdef double maxVal(self) except+
    cpdef double minVal(self) except+
    cpdef double reduceVal(self) except+
    cpdef void mapVector(self, _Vector_d v, string script, size_t parameters,
        string widthPropertyName, string levelPropertyName) except+
    cpdef void rasterise(self, _Vector_d v, string script, size_t parameters) except+
    cpdef _Vector_d cellCentres(self) except+
    cpdef _Vector_d vectorise(self, double[:] contourValue, object noDataValue=?) except+
    cpdef void read(self, string fileName, string jsonConfig) except+
    cpdef void write(self, string fileName, string jsonConfig) except+
    cpdef bool hasData(self) except+
    cpdef uint8_t getRequiredNeighbours(self) except+
    cpdef void setRequiredNeighbours(self, uint8_t requiredNeighbours) except+
    cpdef ReductionType getReductionType(self) except+
    cpdef void setReductionType(self, ReductionType other) except+
    cpdef bool getNeedsStatus(self) except+
    cpdef void setNeedsStatus(self, bool other) except+
    cpdef bool getNeedsWrite(self) except+
    cpdef void setNeedsWrite(self, bool other) except+
    cpdef bool resize2D(self, uint32_t nx, uint32_t ny, uint32_t tox, uint32_t toy)
    cpdef _cyRaster_d region(self, _BoundingBox_d bounds)
    cpdef _TileDimensions_d getTileDimensions(self, uint32_t ti=?, uint32_t tj=?) except+

cdef class _cyRaster_d_i(_cyRasterBase_d):
    cdef shared_ptr[Raster[uint32_t, double]] sh_ptr
    cdef uint32_t[:] data1D
    cdef uint32_t[:, :] data2D
    cdef uint32_t[:, :, :] data3D
    cdef uint32_t nx_, ny_, nz_
    cdef double hx_, hy_, hz_
    cdef double ox_, oy_, oz_
    cdef void c_rastercopy(self, Raster[uint32_t, double] &other) except*
    cdef void init_with_raster_dimensions(self, _RasterDimensions_d d) except *
    cdef void init_with_bbox(self, _BoundingBox_d b, double hx, double hy, double hz) except *
    cpdef void init1D(self, uint32_t nx_, double hx_, double ox_=?) except *
    cpdef void init2D(self, uint32_t nx_, uint32_t ny_, double hx_, double hy_,
                      double ox_=?, double oy_=?) except *
    cpdef void init3D(self, uint32_t nx_, uint32_t ny_, uint32_t nz_, double hx_,
                      double hy_, double hz_, double ox_=?, double oy_=?,
                      double oz_=?) except *
    cpdef uint32_t maxVal(self) except+
    cpdef uint32_t minVal(self) except+
    cpdef uint32_t reduceVal(self) except+
    cpdef void set1D(self, uint32_t[:, :] inp, int ti=?, int tj=?) except+
    cpdef void set2D(self, uint32_t[:, :] inp, int ti=?, int tj=?) except+
    cpdef void set3D(self, uint32_t[:, :, :] inp, int ti=?, int tj=?) except+
    cpdef uint32_t[:] get_data_1d(self, uint32_t ti=?, uint32_t tj=?) except+
    cpdef uint32_t[:, :] get_data_2d(self, uint32_t ti=?, uint32_t tj=?) except+
    cpdef uint32_t[:, :, :] get_data_3d(self, uint32_t ti=?, uint32_t tj=?) except+
    cdef void get_tile_data(self, uint32_t ti, uint32_t tj, vector[uint32_t]& vec) except+
    cpdef void mapVector(self, _Vector_d v, string script, size_t parameters,
        string widthPropertyName, string levelPropertyName) except+
    cpdef void rasterise(self, _Vector_d v, string script, size_t parameters) except+
    cpdef _Vector_d cellCentres(self) except+
    cpdef _Vector_d vectorise(self, uint32_t[:] contourValue, object noDataValue=?) except+
    cpdef void read(self, string fileName, string jsonConfig) except+
    cpdef void write(self, string fileName, string jsonConfig) except+
    cpdef uint32_t getCellValue(self, uint32_t i, uint32_t j=?, uint32_t k=?) except+
    cpdef uint32_t getNearestValue(self, double x, double y=?, double z=?) except+
    # cpdef uint32_t getBilinearValue(self, double x, double y=?, double z=?)
    cpdef void setCellValue(self, uint32_t value, uint32_t i, uint32_t j=?,
                            uint32_t k=?) except+
    cpdef void setAllCellValues(self, uint32_t c) except+
    cpdef bool hasData(self) except+
    cpdef uint8_t getRequiredNeighbours(self) except+
    cpdef void setRequiredNeighbours(self, uint8_t requiredNeighbours) except+
    cpdef ReductionType getReductionType(self) except+
    cpdef void setReductionType(self, ReductionType other) except+
    cpdef bool getNeedsStatus(self) except+
    cpdef void setNeedsStatus(self, bool other) except+
    cpdef bool getNeedsWrite(self) except+
    cpdef void setNeedsWrite(self, bool other) except+
    cpdef bool resize2D(self, uint32_t nx, uint32_t ny, uint32_t tox, uint32_t toy)
    cpdef _cyRaster_d_i region(self, _BoundingBox_d bounds)
    cpdef _TileDimensions_d getTileDimensions(self, uint32_t ti=?, uint32_t tj=?) except+

cdef class _RasterPtrList_d:
    cdef raster_ptr_list_d *thisptr
    cdef int n_raster
    cpdef int get_number_of_rasters(self)
    cpdef void get_raster_from_vec(self, size_t i)

cdef class _RasterPtrList_f:
    cdef raster_ptr_list_f *thisptr
    cdef int n_raster
    cpdef int get_number_of_rasters(self)
    cpdef void get_raster_from_vec(self, size_t i)

cdef class _RasterBaseList_d:
    cdef raster_base_list_d *thisptr
    cdef int n_raster
    cpdef int get_number_of_rasters(self)
    cdef void _add_raster(self, RasterBase[double]* other)
    cdef void _add_df_handler(self, RasterBase[double]* other)
    cpdef void get_raster_from_vec(self, size_t i)

cdef class _RasterBaseList_f:
    cdef raster_base_list_f *thisptr
    cdef int n_raster
    cpdef int get_number_of_rasters(self)
    cdef void _add_raster(self, RasterBase[float]* other)
    cdef void _add_df_handler(self, RasterBase[float]* other)
    cpdef void get_raster_from_vec(self, size_t i)

cdef class DataFileHandler_f:
    cdef public _cyRaster_f cy_raster_obj
    cdef shared_ptr[pyFileHandler[float, float]] thisptr
    cdef float[:, :, :] buf_arr
    cdef int tidx
    cdef double time
    cdef public object class_obj, _file_handler_obj, _file_name
    cdef object cls_capsule, func_capsule
    cpdef void read(self, bool thredds=?, bool use_pydap=?,
                    str modelProjection=?, bool read_projection=?,
                    object layers=?, object dims=?)
    cpdef void write(self, object fileName, string jsonConfig)
    cpdef void update_time(self, int tidx)
    cpdef double time_from_index(self, int tidx)
    cpdef void set_time_bounds(self, object start_time=?, object end_time=?,
                               object dt_str=?)
    cpdef int index_from_time(self, double timestamp)
    cpdef int get_left_index(self, double timestamp)
    cpdef int get_right_index(self, double timestamp)
    cpdef double get_time(self)
    cpdef int get_time_index(self)
    cpdef int get_max_time_index(self)
    cdef pyException setDataFunction(self, TileDimensions[float] tdim,
        vector[float] &v) except+ with gil
    cpdef void setFileInputHandler(self)
    cpdef void setFileOutputHandler(self)
    cpdef void setProjectionParameters(self, _ProjectionParameters_d proj_)
    cpdef void setInterpolationType(self, size_t interpolation_)
    cpdef _ProjectionParameters_d getProjectionParameters(self)

cdef class DataFileHandler_f_i:
    cdef public _cyRaster_f_i cy_raster_obj
    cdef shared_ptr[pyFileHandler_i[float]] thisptr
    cdef uint32_t[:, :, :] buf_arr
    cdef int tidx
    cdef double time
    cdef public object class_obj, _file_handler_obj, _file_name
    cdef object cls_capsule, func_capsule
    cpdef void read(self, bool thredds=?, bool use_pydap=?,
                    str modelProjection=?, bool read_projection=?,
                    object layers=?, object dims=?)
    cpdef void write(self, object fileName, string jsonConfig)
    cpdef void update_time(self, int tidx)
    cpdef double time_from_index(self, int tidx)
    cpdef void set_time_bounds(self, object start_time=?, object end_time=?,
                               object dt_str=?)
    cpdef int index_from_time(self, double timestamp)
    cpdef int get_left_index(self, double timestamp)
    cpdef int get_right_index(self, double timestamp)
    cpdef double get_time(self)
    cpdef int get_time_index(self)
    cpdef int get_max_time_index(self)
    cdef pyException setDataFunction(self, TileDimensions[float] tdim,
                                     vector[uint32_t] &v) except+ with gil
    cpdef void setFileInputHandler(self)
    cpdef void setFileOutputHandler(self)
    cpdef void setProjectionParameters(self, _ProjectionParameters_d proj_)
    cpdef void setInterpolationType(self, size_t interpolation_)
    cpdef _ProjectionParameters_d getProjectionParameters(self)

cdef class DataFileHandler_d:
    cdef public _cyRaster_d cy_raster_obj
    cdef shared_ptr[pyFileHandler[double, double]] thisptr
    cdef double[:, :, :] buf_arr
    cdef int tidx
    cdef double time
    cdef public object class_obj, _file_handler_obj, _file_name
    cdef object cls_capsule, func_capsule
    cpdef void read(self, bool thredds=?, bool use_pydap=?,
                    str modelProjection=?, bool read_projection=?,
                    object layers=?, object dims=?)
    cpdef void write(self, object fileName, string jsonConfig)
    cpdef void update_time(self, int tidx)
    cpdef double time_from_index(self, int tidx)
    cpdef void set_time_bounds(self, object start_time=?, object end_time=?,
                               object dt_str=?)
    cpdef int index_from_time(self, double timestamp)
    cpdef int get_left_index(self, double timestamp)
    cpdef int get_right_index(self, double timestamp)
    cpdef double get_time(self)
    cpdef int get_time_index(self)
    cpdef int get_max_time_index(self)
    cdef pyException setDataFunction(self, TileDimensions[double] tdim,
                                     vector[double] &v) except+ with gil
    cpdef void setFileInputHandler(self)
    cpdef void setFileOutputHandler(self)
    cpdef void setProjectionParameters(self, _ProjectionParameters_d proj_)
    cpdef void setInterpolationType(self, size_t interpolation_)
    cpdef _ProjectionParameters_d getProjectionParameters(self)

cdef class DataFileHandler_d_i:
    cdef public _cyRaster_d_i cy_raster_obj
    cdef shared_ptr[pyFileHandler_i[double]] thisptr
    cdef uint32_t[:, :, :] buf_arr
    cdef int tidx
    cdef double time
    cdef public object class_obj, _file_handler_obj, _file_name
    cdef object cls_capsule, func_capsule
    cpdef void read(self, bool thredds=?, bool use_pydap=?,
                    str modelProjection=?, bool read_projection=?,
                    object layers=?, object dims=?)
    cpdef void write(self, object fileName, string jsonConfig)
    cpdef void update_time(self, int tidx)
    cpdef double time_from_index(self, int tidx)
    cpdef void set_time_bounds(self, object start_time=?, object end_time=?,
                               object dt_str=?)
    cpdef int index_from_time(self, double timestamp)
    cpdef int get_left_index(self, double timestamp)
    cpdef int get_right_index(self, double timestamp)
    cpdef double get_time(self)
    cpdef int get_time_index(self)
    cpdef int get_max_time_index(self)
    cdef pyException setDataFunction(self, TileDimensions[double] tdim,
                                     vector[uint32_t] &v) except+ with gil
    cpdef void setFileInputHandler(self)
    cpdef void setFileOutputHandler(self)
    cpdef void setProjectionParameters(self, _ProjectionParameters_d proj_)
    cpdef void setInterpolationType(self, size_t interpolation_)
    cpdef _ProjectionParameters_d getProjectionParameters(self)

cpdef bool equalSpatialMetrics_d(_Dimensions_d l, _Dimensions_d r) except+
cpdef bool equalSpatialMetrics_f(_Dimensions_f l, _Dimensions_f r) except+
cpdef void sortColumns_d(_cyRaster_d r) except+
cpdef void sortColumns_f(_cyRaster_f r) except+
cpdef void sortColumns_d_i(_cyRaster_d_i r) except+
cpdef void sortColumns_f_i(_cyRaster_f_i r) except+