# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from __future__ import annotations
import sys
import json
from enum import Enum, unique
import warnings
import numbers
from collections import Counter
from typing import Union, List, Tuple, Dict, Optional
from itertools import product
import numpy as np
from .. import core
from .. import gs_enums
from . import raster
from ..vector import vector, _cy_vector
from ._cy_raster import (DataFileHandler_f, DataFileHandler_d,
                         DataFileHandler_d_i, DataFileHandler_f_i,
                         TileSpecifications)
from ._cy_raster import (_cyRaster_d, _cyRaster_f, _cyRaster_d_i,
                         _cyRaster_f_i, _TileDimensions_f, _TileDimensions_d)
from ..runner import runScript, runAreaScript


@unique
class RasterKind(Enum):
    Raster1D: numbers.Integral = 1
    Raster2D: numbers.Integral = 2
    Raster3D: numbers.Integral = 3

    def __eq__(self, other: Union[numbers.Integral, 'RasterKind']):
        if isinstance(other, numbers.Integral):
            return self.value == other
        else:
            return self == other

    def __req__(self, other: Union[numbers.Integral, 'RasterKind']):
        if isinstance(other, numbers.Integral):
            return other == self.value
        else:
            return other == self


class _RasterBase(core.PropertyMap, np.lib.mixins.NDArrayOperatorsMixin):
    def __init__(self: "_RasterBase", base_type: np.dtype, data_type: np.dtype):
        self._dtype = None
        self._handle = None
        self._has_input_handler = False
        self.tileNum = 0
        self.base_type = base_type
        self.data_type = data_type
        self._properties = {}
        self._tmp_script = []
        self._tmp_var_count = None
        self._tmp_var_vector = []

    def getVariableData(self: "_RasterBase",
                        name: Union[str, bytes, np.uint32]) -> Union[np.float32, np.float64, np.uint32]:
        """get the value of the variable

        Parameters
        ----------
        var_name : Union[str, bytes, np.uint32]
            name of the variable

        Returns
        -------
        Union[np.float32,np.float64,np.uint32]
            value of the variable
        """
        # Set variable data
        _name = name
        if isinstance(_name, str):
            _name = _name.encode('utf-8')

        # Get handle
        if isinstance(self, raster.RasterFile):
            return self._handle.cy_raster_obj.getVariableData(_name)
        else:
            return self._handle.getVariableData(_name)

    def setVariableData(self: "_RasterBase", name: Union[str, bytes, np.uint32],
                        value: Union[np.float32, np.float64, np.uint32]) -> None:
        """set a value for a given variable.

        Parameters
        ----------
        var_name : Union[str, bytes, np.uint32]
            name of the variable
        var_value : Union[np.float32, np.float64, np.uint32]
            value of the variable

        Returns
        -------
        Nil
        """
        # Set variable data
        _name = name
        if isinstance(_name, str):
            _name = _name.encode('utf-8')

        # Get handle
        if isinstance(self, raster.RasterFile):
            self._handle.cy_raster_obj.setVariableData(
                _name, self.data_type(value))
        else:
            self._handle.setVariableData(_name, self.data_type(value))

    def getVariablesType(self) -> str:
        """get the type of Variables object

        Returns
        -------
        str
            float/ double
        """
        # Get handle
        if isinstance(self, raster.RasterFile):
            return self._handle.cy_raster_obj.getVariablesType()
        else:
            return self._handle.getVariablesType()

    def hasVariables(self) -> bool:
        """check if variables object has data.

        Returns
        -------
        bool
            True if Variables has data else False
        """
        # Get handle
        if isinstance(self, raster.RasterFile):
            return self._handle.cy_raster_obj.hasVariables()
        else:
            return self._handle.hasVariables()

    def getVariablesIndexes(self) -> Dict:
        """get the indices of the variable

        Returns
        -------
        Dict
            a dictionary of variable and indices in the Variables object.
        """
        # Get handle
        if isinstance(self, raster.RasterFile):
            return self._handle.cy_raster_obj.getVariablesIndexes()
        else:
            return self._handle.getVariablesIndexes()

    @property
    def name(self) -> str:
        out = self.getProperty('name')
        return out

    @name.setter
    def name(self, other: str) -> None:
        self.setProperty("name", other, prop_type=str)

    @property
    def nullValue(self) -> Union[numbers.Integral, numbers.Real]:
        return raster.getNullValue(self.data_type)

    def getPropertyType(self, propName: Union[str, bytes]) -> type:
        return super().getPropertyType(propName)

    def setProjectionParameters(self, other: "core.ProjectionParameters") -> None:
        """Set projection parameters for the raster.

        Parameters
        ----------
        other : ProjectionParameters object
            An instance of projection parameters obtained from a wkt or proj4.

        Returns
        -------
        Nil
        """
        if not isinstance(other, core.ProjectionParameters):
            raise TypeError(
                "Argument should be an instance of ProjectionParameters")
        if isinstance(self, raster.Raster):
            self._handle.setProjectionParameters(other._handle)
        elif isinstance(self, raster.RasterFile):
            self._handle.cy_raster_obj.setProjectionParameters(other._handle)

    def getProjectionParameters(self) -> "core.ProjectionParameters":
        """Get projection parameters from the raster.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : ProjectionParameters
            Returns Raster projection parameters as an instance of ProjectionParameters
        """
        if isinstance(self, raster.Raster):
            out = core.ProjectionParameters.from_proj_param(
                self._handle.getProjectionParameters())
        elif isinstance(self, raster.RasterFile):
            out = core.ProjectionParameters.from_proj_param(
                self._handle.cy_raster_obj.getProjectionParameters())
        return out

    def setInterpolationType(self, interpolation: Union[numbers.Integral,
                                                        gs_enums.RasterInterpolationType]) -> None:
        """Set interpolation type for the raster.

        Parameters
        ----------
        interpolation : Interpolation type
            A RasterInterpolationType enum.

        Returns
        -------
        Nil
        """

        if not isinstance(interpolation, (numbers.Integral,
                                          gs_enums.RasterInterpolationType)):
            raise TypeError("interpolation type should be int / gs_enums")
        elif isinstance(interpolation, numbers.Integral):
            # assign raw value
            _interpolation = interpolation
        else:
            # assign value from enum.Enum
            _interpolation = interpolation.value

        if isinstance(self, raster.Raster):
            self._handle.setInterpolationType(_interpolation)
        elif isinstance(self, raster.RasterFile):
            self._handle.cy_raster_obj.setInterpolationType(_interpolation)

    def region(self, other: 'vector.BoundingBox') -> "raster.Raster":
        """Subset a raster within a region given by a BoundingBox.

        Parameters
        ----------
        other : BoundingBox
            a BoundingBox object to subset Raster

        Returns
        -------
        Raster
            a Raster subset with a given BoundingBox

        Raises
        ------
        TypeError
            Input argument should be an instance of BoundingBox
        TypeError
            Type mismatch between Raster instance and input BoundingBox
        """
        if not isinstance(other, (vector.BoundingBox, _cy_vector._BoundingBox_d,
                                  _cy_vector._BoundingBox_f)):
            raise TypeError(
                "Input argument should be an instance of BoundingBox")
        if self.base_type != other._dtype:
            raise TypeError(
                "Type mismatch between Raster instance and input BoundingBox")
        if isinstance(self, raster.Raster):
            out = raster.Raster.copy("", self._handle.region(other._handle))
        elif isinstance(self, raster.RasterFile):
            if self._has_input_handler:
                temp = self._handle.cy_raster_obj.region(other._handle)
                out = raster.Raster.copy("", temp)
                return out
        return out

    def reproject(self, projParams: "core.ProjectionParameters", **kwargs) -> "raster.Raster":
        """Reproject raster to a given projection parameters.

        Parameters
        ----------
        projParams: ProjectionParameters object
            projection parameters for output raster
        resolution: integer/float, optional
            resolution of output raster
        hx: integer/float, optional
            resolution in x-direction
        hy: integer/float, optional
            resolution in y-direction
        hz: integer/float, optional
            resolution in z-direction
        method: RasterInterpolationType, optional
            method to use for projecting

        Returns
        -------
        out : Raster object
        """
        # get horizonal and vertical spacing
        hx = kwargs.get("resolution", kwargs.get("hx"))
        if hx is None:
            hx = self.dimensions.hx
        hy = kwargs.get("resolution", kwargs.get("hy"))
        if hy is None:
            hy = self.dimensions.hy
        hz = kwargs.get("resolution", kwargs.get("hz"))
        if hy is None:
            hy = self.dimensions.hy

        # get interpolation type
        interp_type = kwargs.get(
            "method", gs_enums.RasterInterpolationType.Nearest)

        # project raster bounds
        bounds_proj = self.bounds.convert(projParams,
                                          self.getProjectionParameters())

        # create an empty raster
        out = raster.Raster(name="dst", data_type=self.data_type)
        out.init_with_bbox(bounds_proj, hx, hy, hz)
        out.setProjectionParameters(projParams)

        raster_name = self.name
        # change name in src raster
        self.name = "src"

        # run script to project raster
        runScript("dst = src;", [out, self], parameter=interp_type,
                  output_type=self.data_type)

        # update name (or revert to original name)
        self.name = raster_name
        out.name = raster_name
        return out

    def clip(self, other: Union['raster.Raster', 'raster.RasterFile',
                                'vector.Vector', 'vector.BoudingBox'],
             projParams: Optional["core.ProjectionParameters"] = None) -> "raster.Raster":
        """Clip a raster with a vector/ raster object.

        Parameters
        ----------
        other : Raster/RasterFile/BoundingBox,Vector
            a vector or raster object used to clip the raster
        projParams : ProjectionParameters, optional
            projection parameters for use with BoundingBox, by default None

        Returns
        -------
        Raster
            a raster subset with a given vector/ raster object

        Raises
        ------
        ValueError
            ProjectionParameters must be provided for BoundingBox
        TypeError
            projParams should be an instance of ProjectionParameter
        TypeError
            input argument should be an instance of Raster/RasterFile/BoundingBox
        """
        if isinstance(other, (raster.Raster, raster.RasterFile)):
            bbox = other.getBounds()
            if self.getProjectionParameters() != other.getProjectionParameters():
                _bbox = bbox.convert(self.getProjectionParameters(),
                                     other.getProjectionParameters())
            else:
                _bbox = bbox
        elif isinstance(other, vector.BoundingBox):
            if projParams is None:
                raise ValueError(
                    "ProjectionParameters must be provided for BoundingBox")
            if not isinstance(projParams, core.ProjectionParameters):
                raise TypeError(
                    "projParams should be an instance of ProjectionParameters")
            if self.getProjectionParameters() != projParams:
                _bbox = other.convert(
                    self.getProjectionParameters(), projParams)
            else:
                _bbox = other
        elif isinstance(other, vector.Vector):
            if self.getProjectionParameters() != other.getProjectionParameters():
                _bbox = other.convert(self.getProjectionParameters(),
                                      other.getProjectionParameters())
            else:
                _bbox = other
        else:
            raise TypeError("Argument 'other' should be an instance of" +
                            " Raster/RasterFile/BoundingBox")

        out = self.region(_bbox)
        out.setProperty("name", self.getProperty(
            "name", prop_type=str), prop_type=str)
        out.setProjectionParameters(self.getProjectionParameters())
        return out

    def get_tile_idx_bounds(self, idx: int) -> Tuple[int]:
        tileSize = TileSpecifications().tileSize
        tj, ti = divmod(idx, self.dimensions.tx)
        idx_s = ti * tileSize
        idx_e = min((ti + 1) * tileSize, self.dimensions.nx)
        jdx_s = tj * tileSize
        jdx_e = min((tj + 1) * tileSize, self.dimensions.ny)
        return idx_s, idx_e, jdx_s, jdx_e

    def _slice_to_tiles(self, *args) -> List:
        """internal method to build list of tiles for given slices.
        """
        # handle when slice start or stop is None or -1
        def handle_none(s, r): return r if s is None or s == -1 else s
        # get the tile size used in core library
        tileSize = TileSpecifications().tileSize
        tiles = []
        # start from backwards i.e., (x -> y -> z)
        for i, _slice in enumerate(args[0][::-1]):
            # only return tile index in x and y direction
            if isinstance(_slice, slice) and i < min(len(*args), 2):
                # compute start index for tiles
                start = divmod(handle_none(_slice.start, 0), tileSize)
                # compute end index for tiles
                # also handle cases when stop is larger than number of cells in raster
                if i == 0:
                    end = divmod(min(handle_none(_slice.stop, self.dimensions.nx),
                                     self.dimensions.nx), tileSize)
                elif i == 1:
                    end = divmod(min(handle_none(_slice.stop, self.dimensions.ny),
                                     self.dimensions.ny), tileSize)
                # create a list of tiles
                _tiles = list(
                    range(start[0], end[0] + 1 if end[1] > 0 else end[0], 1))
                # handle case when slice.stop < tileSize
                if not len(_tiles) > 0:
                    _tiles = [0]
                tiles.append(_tiles)
            elif isinstance(_slice, numbers.Integral):
                # handle case when input is a integer
                tiles.append([_slice // tileSize])
        return tiles[::-1]

    def get_full_data(self, *args) -> np.ndarray:
        tileSize = TileSpecifications().tileSize
        if self._handle is None:
            return
        elif isinstance(self, raster.RasterFile):
            if self._handle.cy_raster_obj.hasData() == False:
                raise RuntimeError("RasterFile object is not initialized")

        if len(args) > 0:
            # throw an error if input argument is not tuple
            if not isinstance(args[0], tuple):
                raise TypeError("Input argument should be a tuple")

            # check if input argument is a tuple of integer, if so, use the method getCellValue
            # instead of reading whole array
            if all([not isinstance(item, slice) for item in args[0]]):
                if len(args[0]) != self.ndim:
                    raise ValueError(
                        f"Number of Input arguments should be {self.ndim}")
                return self.getCellValue(*args[0][::-1])

            # create array to store data
            if self.ndim == RasterKind.Raster1D:
                out_data = np.full((self.dimensions.nx), self.nullValue,
                                   dtype=self.data_type)
            elif self.ndim == RasterKind.Raster2D:
                out_data = np.full((self.dimensions.ny, self.dimensions.nx),
                                   self.nullValue, dtype=self.data_type)
            elif self.ndim == RasterKind.Raster3D:
                out_data = np.full((self.dimensions.nz, self.dimensions.ny,
                                    self.dimensions.nx), self.nullValue,
                                   dtype=self.data_type)

            # read data as tiles
            # get tile indices when
            tile_idx = self._slice_to_tiles(*args)
            # now build an iterator for tiles and read data
            for ti, tj in product(*tile_idx):
                idx = tj + ti * self.dimensions.tx
                idx_s, idx_e, jdx_s, jdx_e = self.get_tile_idx_bounds(idx)
                tile_ny = jdx_e - jdx_s
                tile_nx = idx_e - idx_s
                if self.ndim == RasterKind.Raster1D:
                    out_data[idx_s:idx_e] = self.get_tile(idx)[:tile_nx]
                elif self.ndim == RasterKind.Raster2D:
                    out_data[jdx_s:jdx_e, idx_s:idx_e] = self.get_tile(idx)[
                        :tile_ny, :tile_nx]
                elif self.ndim == RasterKind.Raster3D:
                    nz = self.dimensions.nz
                    out_data[:, jdx_s:jdx_e, idx_s:idx_e] = self.get_tile(idx)[
                        :, :tile_ny, :tile_nx]
            # slice output array for the given arguments
            out_data = out_data[args[0]]
        else:
            # read all the data
            if self.ndim == RasterKind.Raster1D:
                data_slice = [slice(self.dimensions.nx)]
                out_data = np.full((self.dimensions.nx), self.nullValue,
                                   dtype=self.data_type)
            elif self.ndim == RasterKind.Raster2D:
                data_slice = [slice(self.dimensions.ny),
                              slice(self.dimensions.nx)]
                out_data = np.full((self.dimensions.ny, self.dimensions.nx),
                                   self.nullValue, dtype=self.data_type)
            elif self.ndim == RasterKind.Raster3D:
                data_slice = [slice(self.dimensions.nz), slice(self.dimensions.ny),
                              slice(self.dimensions.nx)]
                out_data = np.full((self.dimensions.nz, self.dimensions.ny,
                                    self.dimensions.nx), self.nullValue,
                                   dtype=self.data_type)
            else:
                raise NotImplementedError("only upto 3D rasters are handled")

            numTiles = self.dimensions.tx * self.dimensions.ty
            for idx in range(numTiles):
                ny, nx = self.dimensions.ny, self.dimensions.nx
                idx_s, idx_e, jdx_s, jdx_e = self.get_tile_idx_bounds(idx)
                tile_dim = self.get_tile_dimensions(idx)
                nx, ny = tile_dim.nx, tile_dim.ny
                if self.ndim == RasterKind.Raster1D:
                    out_data[idx_s:idx_e, ] = self.get_tile(idx)[:ny, :nx]
                elif self.ndim == RasterKind.Raster2D:
                    out_data[jdx_s:jdx_e, idx_s:idx_e, ] = self.get_tile(idx)[
                        :ny, :nx]
                elif self.ndim == RasterKind.Raster3D:
                    nz = self.dimensions.nz
                    out_data[:, jdx_s:jdx_e, idx_s:idx_e] = self.get_tile(idx)[
                        :nz, :ny, :nx]
        return out_data

    def resize2D(self, nx: int, ny: int, tox: int, toy: int) -> "raster.Raster":
        if isinstance(self, raster.Raster):
            out = self._handle.resize2D(np.uint32(nx), np.uint32(ny),
                                        np.uint32(tox), np.uint32(toy))
        elif isinstance(self, raster.RasterFile):
            out = self._handle.cy_raster_obj.resize2D(np.uint32(nx), np.uint32(ny),
                                                      np.uint32(tox), np.uint32(toy))
        return out

    @property
    def tiles(self) -> np.ndarray:
        num_tiles = self.dimensions.tx * self.dimensions.ty
        for tileNum in range(num_tiles):
            yield self.get_tile(tileNum)

    def get_tile(self, tileNum: int) -> np.ndarray:
        num_tiles = self.dimensions.tx * self.dimensions.ty
        if tileNum > num_tiles or tileNum < 0:
            raise ValueError("Request tile number of out of bounds")
        tj, ti = divmod(tileNum, self.dimensions.tx)
        if isinstance(self, raster.Raster):
            return self.writeData(ti=ti, tj=tj)
        elif isinstance(self, raster.RasterFile):
            return self.getData(ti=ti, tj=tj)

    def get_tile_dimensions(self, tileNum: int) -> "raster.TileDimensions":
        num_tiles = self.dimensions.tx * self.dimensions.ty
        if tileNum > num_tiles or tileNum < 0:
            raise ValueError("Request tile number of out of bounds")
        tj, ti = divmod(tileNum, self.dimensions.tx)
        if isinstance(self, raster.Raster):
            tile_dim = self._handle.getTileDimensions(ti, tj)
        if isinstance(self, raster.RasterFile):
            tile_dim = self._handle.cy_raster_obj.getTileDimensions(ti, tj)
        return raster.TileDimensions.copy(tile_dim)

    def getReductionType(self) -> "gs_enums.ReductionType":
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return gs_enums.ReductionType(self._handle.getReductionType())
            elif isinstance(self, raster.RasterFile):
                return gs_enums.ReductionType(self._handle.cy_raster_obj.getReductionType())
        else:
            raise RuntimeError("Raster is not yet initialized")

    def setReductionType(self, other: "gs_enums.ReductionType"):
        if not isinstance(other, gs_enums.ReductionType):
            raise RuntimeError(f"Invalid reduction type '{other}'")
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                self._handle.setReductionType(other.value)
            elif isinstance(self, raster.RasterFile):
                self._handle.cy_raster_obj.setReductionType(other.value)
        else:
            raise RuntimeError("Raster is not yet initialized")

    def getRequiredNeighbours(self) -> "gs_enums.NeighboursType":
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return gs_enums.NeighboursType(self._handle.getRequiredNeighbours())
            elif isinstance(self, raster.RasterFile):
                return gs_enums.NeighboursType(self._handle.cy_raster_obj.getRequiredNeighbours())
        else:
            raise RuntimeError("Raster is not yet initialized")

    def setRequiredNeighbours(self, other: "gs_enums.NeighboursType"):
        if self._handle is not None:
            if not isinstance(other, gs_enums.NeighboursType):
                raise TypeError("input argument should be of NeighboursType")
            if isinstance(self, raster.Raster):
                self._handle.setRequiredNeighbours(other.value)
            elif isinstance(self, raster.RasterFile):
                self._handle.cy_raster_obj.setRequiredNeighbours(other.value)
        else:
            raise RuntimeError("Raster is not yet initialized")

    def max(self) -> Union[int, float]:
        """Get maximum value of the Raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : int/float32/float64
            return the maximum value from the Raster.
        """
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return self._handle.maxVal()
            elif isinstance(self, raster.RasterFile):
                if not self._handle.cy_raster_obj.hasData():
                    raise RuntimeError("RasterFile object is not initialized")
                return self._handle.cy_raster_obj.maxVal()

    def min(self) -> Union[int, float]:
        """Get minimum value of the Raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : int/float32/float64
            return the minimum value from the Raster.
        """
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return self._handle.minVal()
            elif isinstance(self, raster.RasterFile):
                if not self._handle.cy_raster_obj.hasData():
                    raise RuntimeError("RasterFile object is not initialized")
                return self._handle.cy_raster_obj.minVal()

    def getCellValue(self, i: numbers.Integral, j: numbers.Integral = 0,
                     k: numbers.Integral = 0) -> numbers.Real:
        """Get cell value for a location within the Raster.

        Parameters
        ----------
        i : int
            grid cell index along x-axis
        j : int (optional)
            grid cell index along y-axis, default is 0
        k : int (optional)
            grid cell index along z-axis, default is 0

        Returns
        -------
        out : float32/uint32/float64
            Raster value at a given index within the raster.
        """
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return self._handle.getCellValue(i, j=j, k=k)
            elif isinstance(self, raster.RasterFile):
                return self._handle.cy_raster_obj.getCellValue(i, j=j, k=k)
        else:
            raise RuntimeError("Raster is not yet initialized")

    def getNearestValue(self, x: numbers.Real, y: numbers.Real = 0.0,
                        z: numbers.Real = 0.0) -> numbers.Real:
        """Get a nearest value for a location within the spatial extent of Raster.

        Parameters
        ----------
        x : int/float
            Spatial location along x-axis
        y : int/float (optional)
            Spatial location along y-axis
        z : int/float (optional)
            Spatial location along z-axis

        Returns
        -------
        out : float32/uint32/float64
            Raster value a given location within the spatial extent of the raster.
        """
        if self._handle is not None:
            if isinstance(self, raster.Raster):
                return self._handle.getNearestValue(x, y=y, z=z)
            elif isinstance(self, raster.RasterFile):
                return self._handle.cy_raster_obj.getNearestValue(x, y=y, z=z)
        else:
            raise RuntimeError("Raster is not yet initialized")

    def getBilinearValue(self, x: numbers.Real, y: numbers.Real = 0.0,
                         z: numbers.Real = 0.0) -> numbers.Real:
        """Get a bilinearly interpolated value for a location within the spatial extent of Raster.

        Parameters
        ----------
        x : int/float
            Spatial location along x-axis
        y : int/float (optional)
            Spatial location along y-axis
        z : int/float (optional)
            Spatial location along z-axis

        Returns
        -------
        out : float32/uint32/float64
            Raster value a given location within the spatial extent of the raster.
        """
        if isinstance(self._handle, (_cyRaster_d_i, _cyRaster_f_i,
                                     DataFileHandler_d_i, DataFileHandler_f_i)):
            raise NotImplementedError("Bilinear value cannot be computed for" +
                                      " Raster of datatype uint32 ")
        else:
            if self._handle is not None:
                if isinstance(self, raster.Raster):
                    return self._handle.getBilinearValue(x, y=y, z=z)
                elif isinstance(self, raster.RasterFile):
                    return self._handle.cy_raster_obj.getBilinearValue(x, y=y, z=z)
            else:
                raise RuntimeError("Raster is not yet initialized")

    def getBounds(self) -> "vector.BoundingBox":
        """Get bounding box of the raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : BoundingBox object
            Return bounding box of the raster object.
        """
        if self._handle is not None:
            if isinstance(self, raster.RasterFile):
                if self._handle.cy_raster_obj.hasData() == False:
                    raise RuntimeError("RasterFile object is not initialized")
                return vector.BoundingBox.from_bbox(self._handle.cy_raster_obj.getBounds())
            elif isinstance(self, raster.Raster):
                return vector.BoundingBox.from_bbox(self._handle.getBounds())
        else:
            return

    def getDimensions(self) -> "raster.RasterDimensions":
        """Get dimensions of the raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : RasterDimensions object
            Return dimensions of the raster object.
        """
        return self.getRasterDimensions()

    def getRasterDimensions(self) -> "raster.RasterDimensions":
        """Get raster dimensions of the raster object.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : RasterDimensions object
            An instance of RasterDimensions containing dimensions of the Raster object.
        """
        if self._handle is None:
            return

        if isinstance(self, raster.Raster):
            return raster.RasterDimensions.copy(self._handle.getRasterDimensions())
        elif isinstance(self, raster.RasterFile):
            if self._handle.cy_raster_obj.hasData() == False:
                raise RuntimeError("RasterFile object is not initialized")
            return raster.RasterDimensions.copy(self._handle.cy_raster_obj.getRasterDimensions())

    def cellCentres(self) -> "vector.Vector":
        """Create a vector of points from Raster cell centres.

        Returns
        -------
        out : Vector object
            Return a vector object containing points.
        """

        if isinstance(self, raster.Raster):
            _raster_handle = self._handle
        elif isinstance(self, raster.RasterFile):
            _raster_handle = self._handle.cy_raster_obj

        _out = _raster_handle.cellCentres()

        out = vector.Vector._from_vector(_out)
        return out

    def vectorise(self, contourValue: numbers.Real,
                  noDataValue: Union[int, float] = None) -> "vector.Vector":
        """Get a vector from the Raster object.

        Parameters
        ----------
        contourValue : int/float/list(int/float),np.ndarray
            A constant value or a list of values to compute contour from the Raster object.

        Returns
        -------
        out : Vector object
            Return a vector object obtained from the raster for a given contour value.
        """

        if (not isinstance(contourValue, (float, list, np.ndarray)) and
                not isinstance(contourValue, numbers.Real)):
            raise TypeError("contourValue should be of numeric type or" +
                            " list/ndarray of numeric types")

        if isinstance(self, raster.Raster):
            _raster_handle = self._handle
        elif isinstance(self, raster.RasterFile):
            _raster_handle = self._handle.cy_raster_obj

        if not isinstance(contourValue, np.ndarray):
            if isinstance(contourValue, list):
                _contour_value = contourValue
            else:
                _contour_value = [contourValue]
            _out = _raster_handle.vectorise(np.array(_contour_value,
                                                     dtype=self.data_type),
                                            noDataValue)
        elif isinstance(contourValue, np.ndarray):
            _out = _raster_handle.vectorise(contourValue.astype(self.data_type),
                                            noDataValue)
        out = vector.Vector._from_vector(_out)
        return out

    def mapVector(self, inp_vector: "vector.Vector",
                  script: str = "",
                  geom_type: Union[numbers.Integral,
                                   gs_enums.GeometryType] = 7,
                  widthPropertyName: str = "",
                  levelPropertyName: str = "") -> None:
        """Convert an input vector to a raster

        Parameters
        ----------
        inp_vector : vector.Vector
            A vector object to be converted to a raster object
        script : str, optional
            the mapping script
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of the vector geometries
        widthPropertyName : str, optional
            vector property used for width mapping, by default ""
        levelPropertyName: str, optional
            vector property used for level mapping, by default ""

        Returns
        -------
        Nil

        Raises
        ------
        TypeError
            inp_vector should be an instance of Vector class
        TypeError
            geom_type should be int/ GeometryType
        """
        if not isinstance(inp_vector, (vector.Vector,
                                       vector._Vector_d,
                                       vector._Vector_f)):
            raise TypeError("inp_vector should be an instance of Vector class")

        if isinstance(inp_vector, vector.Vector):
            assert self.data_type == inp_vector._dtype
            _inp_vector = inp_vector._handle
        else:
            if isinstance(inp_vector, vector._Vector_d):
                assert self.data_type == np.float64
            elif isinstance(inp_vector, vector._Vector_f):
                assert self.data_type == np.float32
            _inp_vector = inp_vector

        if isinstance(script, str):
            _script = script.encode("UTF-8")
        elif isinstance(script, bytes):
            _script = script

        if isinstance(widthPropertyName, str):
            _width = widthPropertyName.encode("UTF-8")
        elif isinstance(widthPropertyName, bytes):
            _width = widthPropertyName

        if isinstance(levelPropertyName, str):
            _level = levelPropertyName.encode("UTF-8")
        elif isinstance(levelPropertyName, bytes):
            _level = levelPropertyName

        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ GeometryType")

        if isinstance(geom_type, gs_enums.GeometryType):
            _geom_type = geom_type.value
        else:
            _geom_type = geom_type

        self._handle.mapVector(_inp_vector, _script,
                               _geom_type, _width, _level)

    def rasterise(self, inp_vector: "vector.Vector", script: str = None,
                  geom_type: Union[numbers.Integral, "gs_enums.GeometryType"] = 7) -> None:
        """Convert an input vector to a raster.

        Parameters
        ----------
        inp_vector : vector.Vector
            A vector object to be converted to a raster object
        script : str
            script used to assign values to raster cells.
        geom_type : Union[numbers.Integral, gs_enums.GeometryType]
            type of the vector geometries

        Returns
        -------
        Nil

        Raises
        ------
        TypeError
            inp_vector should be an instance of Vector class
        AssertionError
            datatype mismatch
        TypeError
            geom_type should be int/ GeometryType
        """
        if not isinstance(inp_vector, (vector.Vector,
                                       vector._Vector_d,
                                       vector._Vector_f)):
            raise TypeError("inp_vector should be an instance of Vector class")

        if isinstance(inp_vector, vector.Vector):
            _inp_vector = inp_vector._handle
        else:
            _inp_vector = inp_vector

        if isinstance(inp_vector, vector._Vector_d):
            assert self.data_type == np.float64, "datatype mismatch"
        elif isinstance(inp_vector, vector._Vector_f):
            assert self.data_type == np.float32, "datatype mismatch"

        if isinstance(script, str):
            _script = script.encode("UTF-8")
        elif isinstance(script, bytes):
            _script = script
        else:
            _script = ""

        if not isinstance(geom_type, (numbers.Integral, gs_enums.GeometryType)):
            raise TypeError("geom_type should be int/ GeometryType")

        if isinstance(geom_type, gs_enums.GeometryType):
            _geom_type = geom_type.value
        else:
            _geom_type = geom_type

        self._handle.rasterise(_inp_vector, _script, _geom_type)

    def hasData(self) -> bool:
        """Check if raster object has data.

        Parameters
        ----------
        Nil

        Returns
        -------
        out : bool
            Return true if raster object has data else false.
        """
        if isinstance(self, raster.Raster):
            return self._handle.hasData()
        elif isinstance(self, raster.RasterFile):
            return self._handle.cy_raster_obj.hasData()

    def getDataTypeString(self) -> np.dtype:
        """get raster datatype as string

        Returns
        -------
        numpy.dtype
            raster data type as numpy dtype object
        """
        assert self._handle is not None
        dtype = self._handle.getDataTypeString().replace("_t", "")
        if dtype == "float":
            dtype = f"{dtype}32"
        return np.dtype(dtype)

    def write(self, fileName: str,
              jsonConfig: Optional[Union[Dict, str]] = None) -> None:
        """Write raster data to a output file.

        Parameters
        ----------
        fileName : str
            Path of the file to write raster data.
        jsonConfig : Union[str, dict]
            A string or dictionary containing configuration for the output file.

        Returns
        -------
        Nil
        """
        if jsonConfig is None:
            _json_config = "".encode("UTF-8")
        else:
            if isinstance(jsonConfig, str):
                _json_config = jsonConfig.encode("UTF-8")
            elif isinstance(jsonConfig, bytes):
                _json_config = jsonConfig
            elif isinstance(jsonConfig, dict):
                _json_config = json.dumps(jsonConfig).encode("UTF-8")

        self._handle.write(fileName, _json_config)

    @property
    def dtype(self) -> np.dtype:
        """get raster data type.

        Returns
        -------
        np.dtype
            raster data type as numpy datatype object
        """
        return self.getDataTypeString()

    @staticmethod
    def _check_inputs(inp) -> bool:
        if not isinstance(inp, (raster.Raster, raster.RasterFile)):
            if not isinstance(inp, numbers.Real):
                warnings.warn("Input argument should be numeric",
                              RuntimeWarning)
                return False
            else:
                return True
        if not len(inp.name) >= 1:
            warnings.warn(
                "Length of Input raster name should be greater than 1", RuntimeWarning)
            return False
        else:
            return True

    def _check_input_args(self, other):
        assert self.getDimensions() == other.getDimensions(), "Raster size should be same"
        assert self.name != other.name, "Input raster can't have same name"

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):
        if method != "__call__":
            raise NotImplementedError()
        else:
            inputs = tuple(x.get_full_data()
                           if isinstance(x, (raster.Raster, raster.RasterFile)) else x
                           for x in inputs)
            result = getattr(ufunc, method)(*inputs, **kwargs)
            return result

    def __array__(self) -> np.ndarray:
        return self.get_full_data()

    def __getitem__(self, val):
        return self.get_full_data(val)

    @property
    def ndim(self) -> int:
        if isinstance(self, raster.RasterFile):
            raster_kind = self._handle.cy_raster_obj.getRasterKind()
        else:
            raster_kind = self._handle.getRasterKind()
        return raster_kind

    @property
    def shape(self) -> Tuple[int]:
        dimensions = self.getDimensions()
        shape = (dimensions.nz, dimensions.ny, dimensions.nx)
        return shape[-1 * (self.ndim):]

    def __str__(self):
        raster_name = f"{self.name} ({self.base_type.__name__}, {self.data_type.__name__})\n"
        proj_str = f"Projection Parameters: \n{str(self.getProjectionParameters())}\n"
        dim_str = str(self.dimensions)
        if self.hasVariables():
            var_data = self.getVariablesIndexes()
            var_str = "Variables:\n"
            var_str += '\n'.join(
                [f"    {item}[{var_data[item]}]:  {self.getVariableData(item)}" for item in var_data])
            raster_name += f"{var_str}\n{proj_str}{dim_str}"
        else:
            raster_name += proj_str + dim_str
        return raster_name

    def __repr__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__


class _Raster_list:
    def __init__(self, dtype: np.dtype, handle=None):
        self._dtype = dtype
        self._handle = handle

    def _from_list(self, other):
        if not isinstance(other, list):
            raise TypeError('Input argument should be a list')
        else:
            self._from_iterable(other)

    def _from_tuple(self, other: Tuple[Union["raster.Raster", "raster.RasterFile"]]):
        if not isinstance(other, tuple):
            raise TypeError('Input argument should be a tuple')
        else:
            self._from_iterable(other)

    def _from_iterable(self, other: Tuple[Union["raster.Raster", "raster.RasterFile"]]):
        n_items = len(other)
        n_rasters = 0
        n_df_handler = 0
        for item in other:
            if isinstance(item, raster.Raster):
                n_rasters += 1
            elif isinstance(item, raster.RasterFile):
                n_df_handler += 1
        if n_rasters > 0:
            if n_rasters != n_items:
                raise ValueError(
                    "All element of tuple should be instances of Raster")
            else:
                for i, item in enumerate(other, 0):
                    if item.base_type != self._dtype:
                        raise TypeError(
                            "Mismatch between Raster datatype and class instance")
                    self._add_raster(item)
        elif n_df_handler > 0:
            if n_df_handler != n_items:
                raise ValueError(
                    "All element of tuple should be instances of DataFileHandler")
            else:
                for i, item in enumerate(other, 0):
                    if item._dtype != self._dtype:
                        raise TypeError(
                            "Mismatch between RasterFile datatype and class instance")
                    self._add_data_handler(item)

    def _append(self, other: Union["raster.Raster", "raster.RasterFile"]):
        if isinstance(other, raster.RasterFile):
            self._add_data_handler(other)
        elif isinstance(other, raster.Raster):
            self._add_raster(other)

    def _add_raster(self, other: "raster.Raster"):
        if isinstance(other, raster.Raster):
            if self._dtype != other.base_type:
                raise TypeError(
                    "mismatch between datatype of input raster and class instance")
            self._add_raster(other._handle)
        elif isinstance(other, (_cyRaster_d, _cyRaster_d_i)):
            if self._dtype != np.float64:
                raise TypeError(
                    "Cannot add input raster of double type to class instance of single precision")
            if isinstance(other, _cyRaster_d):
                self._handle.add_dbl_raster(other)
            elif isinstance(other, _cyRaster_d_i):
                self._handle.add_int_raster(other)
        elif isinstance(other, (_cyRaster_f, _cyRaster_f_i)):
            if self._dtype != np.float32:
                raise TypeError(
                    "Cannot add input raster of single type to class instance of double precision")
            if isinstance(other, _cyRaster_f):
                self._handle.add_flt_raster(other)
            elif isinstance(other, _cyRaster_f_i):
                self._handle.add_int_raster(other)
        else:
            raise TypeError("input argument should be an instance of Raster")

    def _add_data_handler(self, other: "raster.RasterFile"):
        if isinstance(other, raster.RasterFile):
            if self._dtype != other.base_type:
                raise TypeError(
                    "mismatch between datatype of input RasterFile and class instance")
            self._add_data_handler(other._handle)
        elif isinstance(other, (DataFileHandler_d, DataFileHandler_d_i)):
            if self._dtype != np.float64:
                raise TypeError(
                    "Cannot add input RasterFile of double type to class instance of single precision")
            if isinstance(other, DataFileHandler_d):
                self._handle.add_dbl_df_handler(other)
            if isinstance(other, DataFileHandler_d_i):
                self._handle.add_int_df_handler(other)
        elif isinstance(other, (DataFileHandler_f, DataFileHandler_f_i)):
            if self._dtype != np.float32:
                raise TypeError(
                    "Cannot add input RasterFile of single type to class instance of double precision")
            if isinstance(other, DataFileHandler_f):
                self._handle.add_flt_df_handler(other)
            elif isinstance(other, DataFileHandler_f_i):
                self._handle.add_int_df_handler(other)
        else:
            raise TypeError(
                "input argument should be an instance of RasterFile")

    @property
    def _size(self) -> int:
        if self._handle is not None:
            return self._handle.get_number_of_rasters()

    def __len__(self) -> int:
        if self._handle is not None:
            return self._size

    def __getitem__(self, other):
        raise NotImplementedError(
            "Get item operation is not supported on RasterBaseList/RasterPtrList")
        # if isinstance(other, numbers.Integral):
        #     if other >= 0 and other < self._size:
        #         self._handle[other]
        # else:
        #     raise TypeError("input argument should an integer")

    def __setitem__(self, other, value):
        raise NotImplementedError(
            "Set item operation is not supported on RasterBaseList/RasterPtrList")

    def __repr__(self):
        return "<class 'geostack.raster.%s'>" % self.__class__.__name__
