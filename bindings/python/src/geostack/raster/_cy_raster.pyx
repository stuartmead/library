# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=ascii
#cython: c_string_type=unicode

from cython.operator cimport dereference as deref, preincrement as preinc
from libcpp.string cimport string
from libcpp cimport nullptr
from libcpp.memory cimport shared_ptr, unique_ptr
from libcpp.map cimport map as cpp_map
from libcpp.vector cimport vector
from libc.stdio cimport printf
from libc.stdint cimport uint32_t
from libc.string cimport memcpy, memset
from libc.math cimport fmax
import numpy as np
cimport numpy as np
from numpy cimport PyArray_SimpleNewFromData
from ..vector._cy_vector cimport Vector
from ..core._cy_projection cimport _parsePROJ4_d

np.import_array()

cdef class TileSpecifications:
    def __cinit__(self):
        pass

    @property
    def tileSize(self):
        return tileSize

    @property
    def tileSizePower(self):
        return tileSizePower

    @property
    def tileSizeSquared(self):
        return tileSizeSquared

    @property
    def tileSizeMask(self):
        return tileSizeMask

include "_cy_raster.pxi"

cpdef int32_t getNullValue_int32():
    cdef int32_t out
    out = getNullValue[int32_t]()
    return out

cpdef int64_t getNullValue_int64():
    cdef int64_t out
    out = getNullValue[int64_t]()
    return out

cpdef uint64_t getNullValue_uint64():
    cdef uint64_t out
    out = getNullValue[uint64_t]()
    return out

def test_write():
    cdef fstream* f_out = new fstream(b"test.txt", ios_out)
    f_out.write(b"Hello world\n", 12)
    f_out.close()
    del f_out

def test_read(filename):
    cdef int a=0,b=0
    cdef ifstream* f_in = new ifstream(b"test.txt")
    try:
        while (f_in[0] >> a >> b):
            print(a, b)
    finally: # don't forget to call destructor!
        del f_in
