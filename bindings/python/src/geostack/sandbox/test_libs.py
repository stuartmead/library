# -*- coding: utf-8 -*-
from __future__ import annotations
import os
import os.path as pth

global HAS_NCDF, HAS_XARRAY, HAS_GDAL, HAS_RASTERIO
try:
    import netCDF4 as nc
    HAS_NCDF = 1
except ImportError:
    HAS_NCDF = 0

try:
    import xarray as xr
    HAS_XARRAY = 1
except ImportError:
    HAS_XARRAY = 0

try:
    from osgeo import gdal, gdalconst
    HAS_GDAL = 1
except ImportError:
    HAS_GDAL = 0

try:
    import rasterio
    HAS_RASTERIO = 1
except ImportError:
    HAS_RASTERIO = 0


class requires:
    @staticmethod
    def requires_netcdf4(f):
        if HAS_NCDF == 1:
            return f
        else:
            def dummy_function(*args, **kwargs):
                raise ImportWarning("NetCDF is not installed")
            return dummy_function

    @staticmethod
    def requires_gdal(f):
        if HAS_GDAL == 1:
            return f
        else:
            def dummy_function(*args, **kwargs):
                raise ImportWarning("gdal is not installed")
            return dummy_function

    @staticmethod
    def requires_rasterio(f):
        if HAS_RASTERIO == 1:
            return f
        else:
            def dummy_function(*args, **kwargs):
                raise ImportWarning("rasterio is not installed")
            return dummy_function

    @staticmethod
    def requires_xarray(f):
        if HAS_XARRAY == 1:
            return f
        else:
            def dummy_function(*args, **kwargs):
                raise ImportWarning("xarray is not installed")
            return dummy_function


@requires.requires_netcdf4
def read_netcdf(*args, **kwargs):
    '''read netcdf file using netcdf4-python
    '''
    if not isinstance(filein, str):
        raise TypeError("filein argument should be of string type")

    if pth.exists(filein):
        if pth.isfile(filein):
            test = nc.Dataset(filein, mode='r')
            return test
        else:
            raise TypeError("%s is not a file" % filein)
    else:
        raise FileNotFoundError("file %s is not present" % filein)


@requires.requires_gdal
def read_gdal(*args, **kwargs):
    '''read raster data using gdal
    '''
    if not isinstance(filein, str):
        raise TypeError("filein argument should be of string type")

    if pth.exists(filein):
        if pth.isfile(filein):
            test = gdal.Open(filein)
            return test
        else:
            raise TypeError("%s is not a file" % filein)
    else:
        raise FileNotFoundError("file %s is not present" % filein)


@requires.requires_xarray
def read_xarray(*args, **kwargs):
    '''read netcdf data using xarray
    '''
    if not isinstance(filein, str):
        raise TypeError("filein argument should be of string type")

    if pth.exists(filein):
        if pth.isfile(filein):
            test = xr.open_dataset(filein)
            return test
        else:
            raise TypeError("%s is not a file" % filein)
    else:
        raise FileNotFoundError("file %s is not present" % filein)


@requires.requires_rasterio
def read_rasterio(*args, **kwargs):
    '''read raster data using rasterio
    '''
    if not isinstance(filein, str):
        raise TypeError("filein argument should be of string type")

    if pth.exists(filein):
        if pth.isfile(filein):
            test = rasterio.open(filein)
            return test
        else:
            raise TypeError("%s is not a file" % filein)
    else:
        raise FileNotFoundError("file %s is not present" % filein)
