# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
# cython: language_level=3

from libcpp.string cimport string
from libcpp.vector cimport vector
from ..raster._cy_raster cimport (_RasterBaseList_d, _RasterBaseList_f,
    _cyRaster_d, _cyRaster_f, _cyRaster_d_i, _cyRaster_f_i)
from ..vector._cy_vector cimport Vector, _Vector_d, _Vector_f
from ..raster._cy_raster cimport Raster, RasterBase, ReductionType, RasterDebugType
from libc.stdint cimport uint32_t
from libcpp cimport bool

cdef extern from "utils.h":
    void cy_copy[T](T& a, T& b) nogil

cdef extern from "<functional>" namespace "std" nogil:
    cdef cppclass reference_wrapper[T]:
        # reference_wrapper() except +
        reference_wrapper(T& ref) except +
        reference_wrapper(T&&) except +
        reference_wrapper(reference_wrapper[T]&) except +
        reference_wrapper& get() except +
        T& get() except +
        reference_wrapper operator=(reference_wrapper&) except +
        reference_wrapper& operator[]() except +
    reference_wrapper[T] cpp_ref "std::ref" [T](T& t) except +
    reference_wrapper[T] cpp_ref "std::ref" [T](reference_wrapper[T] t) except +

cdef extern from "gs_raster.h" namespace "Geostack":
    void runScriptNoOut[C](string script,
                           vector[reference_wrapper[RasterBase[C]]] inputRasters,
                           size_t parameters) nogil except +

    Raster[R, C] runScript[R, C](string script,
                                 vector[reference_wrapper[RasterBase[C]]] inputRasters,
                                 size_t parameters) nogil except +

    Raster[R, C] runAreaScript[R, C](string script,
                                     RasterBase[C] inputRaster,
                                     int width) nogil except +

    Vector[C] stipple[C](string script, vector[reference_wrapper[RasterBase[C]]] inputRasters,
                         vector[string] fields, uint32_t nPerCell) nogil except+

cdef extern from "gs_vector.h" namespace "Geostack":
    void runVectorScript[C](string script,
                            Vector[C] &v,
                            vector[reference_wrapper[RasterBase[C]]] inputRasters,
                            ReductionType r) nogil except+

ctypedef reference_wrapper[RasterBase[double]] RasterBaseRef_d
ctypedef reference_wrapper[RasterBase[float]] RasterBaseRef_f

cdef class RasterScript_d:
    @staticmethod
    cdef void _run_script_noout(bytes script,
                                _RasterBaseList_d input_rasters,
                                size_t parameters) except+
    @staticmethod
    cdef _cyRaster_d _run_script(bytes script,
                                 _RasterBaseList_d input_rasters,
                                 size_t parameters) except+
    @staticmethod
    cdef _cyRaster_d _run_areascript(bytes script,
                                     _cyRaster_d input_raster,
                                     int width) except+

    @staticmethod
    cdef _Vector_d c_stipple(bytes script, _RasterBaseList_d input_rasters,
                             vector[string] fields, uint32_t nPerCell) except+

    @staticmethod
    cdef _cyRaster_d_i _run_script_i(bytes script,
                                     _RasterBaseList_d input_rasters,
                                     size_t parameters) except+
    @staticmethod
    cdef _cyRaster_d_i _run_areascript_i(bytes script,
                                         _cyRaster_d_i input_raster,
                                         int width) except+

cdef class RasterScript_f:
    @staticmethod
    cdef void _run_script_noout(bytes script,
                                _RasterBaseList_f input_rasters,
                                size_t parameters) except+
    @staticmethod
    cdef _cyRaster_f _run_script(bytes script,
                                 _RasterBaseList_f input_rasters,
                                 size_t parameters) except+
    @staticmethod
    cdef _cyRaster_f _run_areascript(bytes script,
                                     _cyRaster_f input_raster,
                                     int width) except+

    @staticmethod
    cdef _Vector_f c_stipple(bytes script, _RasterBaseList_f input_rasters,
                             vector[string] fields, uint32_t nPerCell) except+

    @staticmethod
    cdef _cyRaster_f_i _run_script_i(bytes script,
                                     _RasterBaseList_f input_rasters,
                                     size_t parameters) except+
    @staticmethod
    cdef _cyRaster_f_i _run_areascript_i(bytes script,
                                         _cyRaster_f_i input_raster,
                                         int width) except+

cdef class VectorScript_d:
    @staticmethod
    cdef void _run_script_noout(bytes script,
                                _Vector_d v,
                                _RasterBaseList_d input_rasters,
                                ReductionType r) except +

cdef class VectorScript_f:
    @staticmethod
    cdef void _run_script_noout(bytes script,
                                _Vector_f v,
                                _RasterBaseList_f input_rasters,
                                ReductionType r) except +