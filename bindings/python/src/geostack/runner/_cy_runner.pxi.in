"""
Template for each `dtype` helper function for series

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# name, x_name, y_name

rtypes = [('d', 'double', 'double'),
         ('f', 'float', 'float'),]
}}

{{for x_name, py_type, x_type in rtypes}}

cdef class RasterScript_{{x_name}}:
    @staticmethod
    cdef void _run_script_noout(bytes script,
                                _RasterBaseList_{{x_name}} input_rasters,
                                size_t parameters) except+:
        cdef string c_script = <string> script
        with nogil:
            runScriptNoOut[{{x_type}}](c_script,
                deref(input_rasters.thisptr), parameters)

    @staticmethod
    def run_script_noout(bytes script,
                         _RasterBaseList_{{x_name}} input_rasters, size_t parameters):
        RasterScript_{{x_name}}._run_script_noout(script, input_rasters, parameters)

    @staticmethod
    cdef _cyRaster_{{x_name}} _run_script(bytes script,
                                          _RasterBaseList_{{x_name}} input_rasters,
                                          size_t parameters) except +:
        cdef Raster[{{x_type}}, {{x_type}}] out
        cdef string c_script = <string> script
        rout = _cyRaster_{{x_name}}()
        with nogil:
            out = runScript[{{x_type}}, {{x_type}}](c_script,
                    deref(input_rasters.thisptr), parameters)
        rout.c_rastercopy(out)
        return rout

    @staticmethod
    def run_script(bytes script,
                   _RasterBaseList_{{x_name}} input_rasters,
                   size_t parameters):
        return RasterScript_{{x_name}}._run_script(script, input_rasters, parameters)

    @staticmethod
    cdef _Vector_{{x_name}} c_stipple(bytes script, _RasterBaseList_{{x_name}} input_rasters,
                                      vector[string] fields, uint32_t nPerCell) except+:
        cdef Vector[{{x_type}}] out
        cdef string c_script = <string> script
        vout = _Vector_{{x_name}}()
        with nogil:
            out = stipple[{{x_type}}](c_script, deref(input_rasters.thisptr), fields, nPerCell)
        vout.c_copy(out)
        return vout

    @staticmethod
    def _stipple(bytes script, _RasterBaseList_{{x_name}} input_rasters,
                 list fields, size_t nPerCell):
        cdef vector[string] c_fields
        if len(fields) > 0:
            for item in fields:
                c_fields.push_back(<string> item)
        return RasterScript_{{x_name}}.c_stipple(script, input_rasters, c_fields, nPerCell)

    @staticmethod
    cdef _cyRaster_{{x_name}}_i _run_script_i(bytes script,
                                              _RasterBaseList_{{x_name}} input_rasters,
                                              size_t parameters) except +:
        cdef Raster[uint32_t, {{x_type}}] out
        cdef string c_script = <string> script
        rout = _cyRaster_{{x_name}}_i()
        with nogil:
            out = runScript[uint32_t, {{x_type}}](c_script,
                    deref(input_rasters.thisptr), parameters)
        rout.c_rastercopy(out)
        return rout

    @staticmethod
    def run_script_i(bytes script,
                     _RasterBaseList_{{x_name}} input_rasters,
                     size_t parameters):
        return RasterScript_{{x_name}}._run_script_i(script,
                                                     input_rasters,
                                                     parameters)

    @staticmethod
    cdef _cyRaster_{{x_name}} _run_areascript(bytes script,
                                              _cyRaster_{{x_name}} input_raster,
                                              int width) except +:
        cdef Raster[{{x_type}}, {{x_type}}] out
        cdef string c_script = <string> script
        rout = _cyRaster_{{x_name}}()
        with nogil:
            out = runAreaScript[{{x_type}}, {{x_type}}](c_script,
                                                        deref(input_raster.baseptr),
                                                        width)
        rout.c_rastercopy(out)
        return rout

    @staticmethod
    def run_areascript(bytes script,
                       _cyRaster_{{x_name}} input_raster,
                       int width):
        return RasterScript_{{x_name}}._run_areascript(script, input_raster, width)

    @staticmethod
    cdef _cyRaster_{{x_name}}_i _run_areascript_i(bytes script,
        _cyRaster_{{x_name}}_i input_raster, int width) except +:
        cdef Raster[uint32_t, {{x_type}}] out
        cdef string c_script = <string> script
        rout = _cyRaster_{{x_name}}_i()
        with nogil:
            out = runAreaScript[uint32_t, {{x_type}}](c_script,
                                                      deref(input_raster.baseptr),
                                                      width)
        rout.c_rastercopy(out)
        return rout

    @staticmethod
    def run_areascript_i(bytes script,
                         _cyRaster_{{x_name}}_i input_raster,
                         int width):
        return RasterScript_{{x_name}}._run_areascript_i(script, input_raster, width)


cdef class VectorScript_{{x_name}}:
    @staticmethod
    cdef void _run_script_noout(bytes script,
                                _Vector_{{x_name}} v,
                                _RasterBaseList_{{x_name}} input_rasters,
                                ReductionType r) except +:
        cdef string c_script = <string> script
        with nogil:
            runVectorScript[{{x_type}}](c_script,
                                        <Vector[{{x_type}}]&>deref(v.thisptr),
                                        <vector[reference_wrapper[RasterBase[{{x_type}}]]]>deref(input_rasters.thisptr),
                                        <ReductionType>r)

    @staticmethod
    def run_script_noout(bytes script,
                         _Vector_{{x_name}} v,
                         _RasterBaseList_{{x_name}} input_rasters,
                         size_t reduction_type):
        VectorScript_{{x_name}}._run_script_noout(script,
                                                  v,
                                                  input_rasters,
                                                  <ReductionType>reduction_type)

{{endfor}}
