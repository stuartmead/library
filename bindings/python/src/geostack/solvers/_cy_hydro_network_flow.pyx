# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=ascii
#cython: c_string_type=unicode

from cython.operator cimport dereference as deref
from libcpp.memory cimport unique_ptr, shared_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport nullptr_t, nullptr
from libc.stdint cimport uint32_t, uint64_t
from libcpp cimport bool
#from libcpp cimport double
import numpy as np
cimport numpy as np
from ..vector._cy_vector cimport Vector
from ..core._cy_json cimport Json
from ..vector._cy_vector cimport _Vector_d, _Vector_f

np.import_array()

cdef extern from "gs_hydro_network.h" namespace "Geostack":
    ctypedef enum HydroNetworkNodeType "HydroNetworkNodeType::Type":
        Junction
        Terminator
        Inflow

    ctypedef enum HydroNetworkSegmentType "HydroNetworkSegmentType::Type":
        Undefined
        HazenWilliams
        MannigOpenChannel
        Logarithmic
        SqrtExp
        DynamicWave

    cdef cppclass HydroNetworkFlowSolver[T]:
        bool init(Vector[T] &network, string jsonConfig) except+
        bool run(T runtime, T writeStep, uint32_t mode) except+
        double getArea(uint32_t id) except+
        double getPerimeter(uint32_t id) except+
        double getChannelWidth(uint32_t id) except+
        Vector[T]& getNetwork() except+

include "_cy_hydro_network_flow.pxi"
