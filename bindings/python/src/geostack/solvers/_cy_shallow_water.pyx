# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=ascii
#cython: c_string_type=unicode

from cython.operator cimport dereference as deref
from libcpp.memory cimport unique_ptr, shared_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport nullptr_t, nullptr
from libc.stdint cimport uint32_t, uint64_t
from libcpp cimport bool
import numpy as np
cimport numpy as np
from ..raster._cy_raster cimport _cyRaster_d, _cyRaster_f, Raster, RasterBase
from ..raster._cy_raster cimport _RasterPtrList_d, _RasterPtrList_f
from ..core._cy_property cimport PropertyMap

np.import_array()

cdef extern from "gs_shallow_water.h" namespace "Geostack":
    cdef cppclass ShallowWater[T]:
        ShallowWater() except+
        bool init(string jsonConfig,
                  Raster[T, T] &h_in,
                  Raster[T, T] &b_in) nogil except+
        bool step() nogil except+

include "_cy_shallow_water.pxi"