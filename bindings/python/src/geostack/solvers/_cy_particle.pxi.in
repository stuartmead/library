"""
Template for each `dtype` helper function

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# f_name, f_type

rtypes = [('d', 'double'),
          ('f', 'float'),]

}}

{{for f_name, f_type in rtypes}}

cdef class cyParticle_{{f_name}}:
    cdef unique_ptr[Particle[{{f_type}}]] thisptr

    def __cinit__(self):
        self.thisptr.reset(new Particle[{{f_type}}]())

    cpdef bool init(self, string jsonConfig,
                    _Vector_{{f_name}} particles,
                    _Variables_{{f_name}} variables,
                    _RasterPtrList_{{f_name}} inp_layers) except +:
        cdef bool out = False
        if self.thisptr != nullptr:
            with nogil:
                out = deref(self.thisptr).init(jsonConfig,
                                               deref(particles.thisptr),
                                               variables.thisptr,
                                               deref(inp_layers.thisptr))
        return out

    cpdef bool step(self) except+:
        cdef bool out = False
        with nogil:
            out = deref(self.thisptr).step()
        return out

    cpdef bool setTimeStep(self, {{f_type}} dt) except+:
        cdef {{f_type}} dt_ = dt
        with nogil:
            deref(self.thisptr).setTimeStep(dt_)

    cpdef void addParticles(self, _Vector_{{f_name}} particles) except +:
        if self.thisptr != nullptr:
            with nogil:
                deref(self.thisptr).addParticles(deref(particles.thisptr))

    cpdef _Vector_{{f_name}} getParticles(self) except+:
        cdef Vector[{{f_type}}] out
        vout = _Vector_{{f_name}}()
        if self.thisptr != nullptr:
            with nogil:
                out = deref(self.thisptr).getParticles()
        vout.c_copy(out)
        return vout

    cpdef cl_uint getSamplePlaneIndexCount(self) except+:
        cdef cl_uint out = 0;
        with nogil:
            out = deref(self.thisptr).getSamplePlaneIndexCount()
        return out

    cpdef cl_uint[:] getSamplePlaneIndexes(self) except+:
        cdef cl_uint[:] out
        cdef vector[cl_uint] vout
        with nogil:
            vout = deref(self.thisptr).getSamplePlaneIndexes()
        out = np.zeros(shape=(vout.size(),), dtype=np.uint32) # TODO 32-bit assumption for cl_uint
        memcpy(&out[0], vout.data(), vout.size()*sizeof(cl_uint))
        return out

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if self.thisptr != nullptr:
            if type(self) is cyParticle_{{f_name}}:
                self.thisptr.reset()

    def __exit__(self):
        self.__dealloc__()


{{endfor}}