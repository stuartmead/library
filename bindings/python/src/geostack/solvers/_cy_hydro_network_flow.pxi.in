"""
Template for each `dtype` helper function

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# f_name, f_type

rtypes = [('d', 'double'),
          ('f', 'float'),]

}}

{{for f_name, f_type in rtypes}}


cdef class cyHydroNetworkFlowSolver_{{f_name}}:
    cdef unique_ptr[HydroNetworkFlowSolver[{{f_type}}]] thisptr

    def __cinit__(self):
        self.thisptr.reset(new HydroNetworkFlowSolver[{{f_type}}]())

    cdef bool init(self, Vector[{{f_type}}]& network, string jsonConfig) except+:
        cdef bool out
        out = deref(self.thisptr).init(network, jsonConfig)
        return out

    cpdef bool init_solver(self, _Vector_{{f_name}} input_network, string jsonConfig) except *:
        cdef bool out
        out = self.init(deref(input_network.thisptr), jsonConfig)
        return out

    cpdef _Vector_{{f_name}} getNetwork(self) except+:
        out = _Vector_{{f_name}}()
        cdef Vector[{{f_type}}] _out
        _out = deref(self.thisptr).getNetwork()
        out.c_copy(_out)
        return out

    cpdef bool run(self, {{f_type}} runtime, {{f_type}} writeStep, uint32_t mode) except+:
        cdef bool out
        out = deref(self.thisptr).run(runtime, writeStep, mode)
        return out

    cpdef double getArea(self, uint32_t id) except+:
        cdef double out
        out = deref(self.thisptr).getArea(id)
        return out
    
    cpdef double getPerimeter(self, uint32_t id) except+:
        cdef double out
        out = deref(self.thisptr).getPerimeter(id)
        return out
        
    cpdef double getChannelWidth(self, uint32_t id) except+:
        cdef double out
        out = deref(self.thisptr).getChannelWidth(id)
        return out

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if self.thisptr != nullptr:
            if type(self) is cyHydroNetworkFlowSolver_{{f_name}}:
                self.thisptr.reset()

    def __exit__(self):
        self.__dealloc__()

{{endfor}}
