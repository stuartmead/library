"""
Template for each `dtype` helper function for series

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# name, x_name, y_name

xtypes = [('dbl', '"float64"', 'double'),
         ('int', '"int64"', 'int64_t'),]
ytypes = [('dbl', '"float64"', 'double'),
         ('flt', '"float32"', 'float'),]
}}

{{for x_name, x_np, x_type in xtypes}}
{{for y_name, y_np, y_type in ytypes}}

cdef class Series_{{x_name}}_{{y_name}}:
    def __cinit__(self):
        self.thisptr.reset(new Series[{{x_type}}, {{y_type}}]())

    cpdef void clear(self):
        if self.thisptr != nullptr:
            deref(self.thisptr).clear()

    cpdef {{x_type}} get_xMax(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).get_xMax()

    cpdef {{x_type}} get_xMin(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).get_xMin()

    cpdef {{y_type}} get_yMax(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).get_yMax()

    cpdef {{y_type}} get_yMin(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).get_yMin()

    cpdef bool isInitialised(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).isInitialised()

    cpdef bool isConstant(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).isConstant()

    cpdef string getName(self):
        cdef string out
        if self.thisptr != nullptr:
            out = deref(self.thisptr).getName()
            return out
        else:
            raise RuntimeError("Series is not yet initialised")

    cpdef void setName(self, string name):
        if self.thisptr != nullptr:
            deref(self.thisptr).setName(name)

    cpdef bool inRange(self, {{x_type}} x):
        if self.thisptr != nullptr:
            return deref(self.thisptr).inRange(x)

    cpdef void update(self):
        if self.thisptr != nullptr:
            deref(self.thisptr).update()

    cpdef void updateLimits(self):
        if self.thisptr != nullptr:
            deref(self.thisptr).updateLimits()

    cpdef void setBounds(self, {{y_type}} lowerLimit, {{y_type}} upperLimit):
        if self.thisptr != nullptr:
            deref(self.thisptr).setBounds(lowerLimit, upperLimit)

    cdef {{y_type}} get(self, {{x_type}} x):
        if self.thisptr != nullptr:
            return deref(self.thisptr)(x)

    cpdef void from_series(self, Series_{{x_name}}_{{y_name}} other):
        self.thisptr.reset(new Series[{{x_type}}, {{y_type}}](deref(other.thisptr)))

    cpdef void add_string_value(self, string xval, {{y_type}} yval):
        deref(self.thisptr).addValue(xval, yval)
        
    cpdef void add_value(self, {{x_type}} xval, {{y_type}} yval):
        deref(self.thisptr).addValue(xval, yval)

    cpdef void add_values(self, {{x_type}}[:] xvals, {{y_type}}[:] yvals):
        cdef size_t nx, i
        nx = <size_t> xvals.shape[0]
        for i in range(nx):
            deref(self.thisptr).addValue(xvals[i], yvals[i])

    cpdef void setInterpolation(self, int interp_type):
        if self.thisptr != nullptr:
            if interp_type == SeriesInterpolationType.Linear:
                deref(self.thisptr).setInterpolation(SeriesInterpolationType.Linear)
            elif interp_type == SeriesInterpolationType.MonotoneCubic:
                deref(self.thisptr).setInterpolation(SeriesInterpolationType.MonotoneCubic)
            elif interp_type == SeriesInterpolationType.BoundedLinear:
                deref(self.thisptr).setInterpolation(SeriesInterpolationType.BoundedLinear)
            else:
                raise ValueError("interp_type should be 0,1,2")

    def __call__(self, x):
        return self.get(x)

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if self.thisptr != nullptr:
            self.thisptr.reset()

    def __exit__(self):
        self.__dealloc__()

{{endfor}}
{{endfor}}
