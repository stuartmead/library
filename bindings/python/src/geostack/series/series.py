# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from __future__ import annotations
import os
import os.path as pth
import calendar
import numbers
import numpy as np
from datetime import datetime, timedelta
from functools import partial
from typing import List, Dict, Union, Optional
from fnmatch import fnmatch

from ._cy_series import (Series_dbl_flt,
                         Series_int_flt,
                         Series_dbl_dbl,
                         Series_int_dbl)  # noqa
from .. import gs_enums
from ..dataset.supported_libs import import_or_skip

global HAS_PYTZ

pytz, HAS_PYTZ = import_or_skip('pytz')

__all__ = ["Series"]


def parse_date_string(inp_str: str, timezone=None,
                      dt_format: str = "%Y-%m-%dT%H:%M:%SZ") -> datetime:
    """parse a date string to datetime object

    Parameters
    ----------
    inp_str : str
        datetime string
    timezone : pytz.BaseTzInfo, optional
        time zone as a pytz object, by default None
    dt_format : str, optional
        string format for parsing datetime string, by default "%Y-%m-%dT%H:%M:%SZ"

    Returns
    -------
    datetime
        a datetime object from the datetime string

    Raises
    ------
    TypeError
        timezone should be an instance of pytz.BaseTzInfo
    """
    time_zone = None
    try:
        out_date = datetime.strptime(inp_str, dt_format)
    except ValueError:
        if fnmatch(inp_str, "*+??:??"):
            out_date = datetime.fromisoformat(inp_str)
        else:
            raise RuntimeError("Unable to parse datetime")

    if HAS_PYTZ:
        if timezone is None:
            time_zone = pytz.UTC
        else:
            if isinstance(timezone, pytz.BaseTzInfo):
                time_zone = timezone
            else:
                raise TypeError(
                    "timezone should be an instance of pytz.BaseTzInfo")

        out_date = out_date.replace(tzinfo=time_zone)
    return out_date


class Series:
    def __init__(self, *args, **kwargs):
        self.obj_types = {"index_type": np.int64,
                          "value_type": np.float32}

        if args:
            self._parse_args(args)
        if kwargs:
            self._parse_kwargs(kwargs)

        if self.obj_types['value_type'] == np.float32:
            if self.obj_types['index_type'] == np.int64:
                self._handle = Series_int_flt()
            else:
                self._handle = Series_dbl_flt()
        else:
            if self.obj_types['index_type'] == np.int64:
                self._handle = Series_int_dbl()
            else:
                self._handle = Series_dbl_dbl()

    def _parse_args(self, args: List):
        arg_len = min(len(args), 2)
        for item, arg in zip(['index_type', 'value_type'], range(arg_len)):
            if item == "index_type":
                if args[arg] in [int, np.int32, np.int64]:
                    self.obj_types[item] = np.int64
                elif args[arg] in [float, np.float32, np.float64]:
                    self.obj_types[item] = np.float64
                else:
                    raise ValueError(f"{item} type can be only int64/float64")
            else:
                if args[arg] in [float, np.float32, np.float32]:
                    self.obj_types[item] = np.float32
                elif args[arg] in [np.float64]:
                    self.obj_types[item] = np.float64
                else:
                    raise ValueError(
                        f"{item} type can be only float32/float64")

    def _parse_kwargs(self, kwargs: Dict):
        arg_len = min(len(kwargs), 2)
        for item in ['index_type', 'value_type']:
            if item == "index_type":
                if kwargs.get(item) in [int, np.int32, np.int64]:
                    self.obj_types[item] = np.int64
                elif kwargs.get(item) in [float, np.float32, np.float64]:
                    self.obj_types[item] = np.float64
                else:
                    raise ValueError("Index type can be only int64/float64")
            else:
                if kwargs.get(item) in [float, np.float32, np.float32]:
                    self.obj_types[item] = np.float32
                elif kwargs.get(item) in [np.float64]:
                    self.obj_types[item] = np.float64
                else:
                    raise ValueError("Value type can be only float32/float64")

    @staticmethod
    def read_csv_file(filepath: str, **kwargs) -> "Series":
        """parse a csv file or csv data into a Series object.

        Parameters
        ----------
        filepath : str
            path of the csv file or csv data as string delimited by newline character

        Returns
        -------
        Series
            a series object
        """
        if kwargs.get("index_type"):
            x_type = kwargs.get("index_type")
            kwargs.pop('index_type')
        else:
            x_type = np.int64
        if kwargs.get('value_type'):
            y_type = kwargs.get("value_type")
            kwargs.pop('value_type')
        else:
            y_type = np.float32
        out = Series(index_type=x_type, value_type=y_type)
        out.from_csvfile(filepath, **kwargs)
        return out

    def from_csvfile(self, other: str, index_col: int = 0,
                     usecols: Optional[List] = None, parse_date: bool = False,
                     dt_format: Optional[str] = None, time_factor: Optional[numbers.Real] = None,
                     timezone=None, **kwargs):
        """method to parse csv file or csv data string.

        Parameters
        ----------
        other : str
            path to a csv file or csv data as string
        index_col : int, optional
            column number to use as index, by default 0
        usecols : List, optional
            a list of column indices to use for parsing, by default None
        parse_date : bool, optional
            flag to parse date, by default False
        dt_format : str, optional
            a string representation of datetime format, by default None
        time_factor : numbers.Real, optional
            a factor to apply to the time index, by default None
        timezone : pytz.BaseTzInfo, optional
            time zone offset, by default None

        Raises
        ------
        TypeError
            input argument should be of str type
        FileNotFoundError
            file is not present
        AssertionError
            Series object only support two columns
        ValueError
            index column should be in usecols
        """
        if not isinstance(other, str):
            raise TypeError("input argument should be of str type")

        if '\n' not in other:
            if not pth.exists(other):
                raise FileNotFoundError(f"{other} is not present")

        if parse_date:
            if dt_format is None:
                datetime_format = "%Y-%m-%dT%H:%M:%SZ"
            else:
                datetime_format = dt_format

            if time_factor is None:
                _time_factor = 1.0
            else:
                _time_factor = time_factor

        if usecols is not None:
            assert len(usecols) == 2, "Series object only support two columns"
        else:
            usecols = [index_col, 1]

        if not kwargs.get("dtype"):
            if not parse_date:
                if self.obj_types['index_type'] == np.int64:
                    x_type = 'i8'
                elif self.obj_types['index_type'] == np.float64:
                    x_type = 'f8'
            else:
                x_type = "U25"
            if self.obj_types['value_type'] == np.float32:
                y_type = 'f4'
            elif self.obj_types['value_type'] == np.float64:
                y_type = 'f8'
            dtype = f'{x_type},{y_type}'
        else:
            dtype = kwargs.get('dtype')
            kwargs.pop('dtype')

        if kwargs.get('names') and isinstance(kwargs.get("names"), list):
            col_names = kwargs['names']
            kwargs.pop('names')
        else:
            if '\n' not in other:
                with open(other, 'r') as inp:
                    row = inp.readline()
            else:
                row = other.split('\n')[0]
            col_names = [row.split(',')[item] for item in usecols]

        if '\n' not in other:
            datainp = np.recfromcsv(other, dtype=dtype, usecols=usecols,
                                    names=col_names, **kwargs)
        else:
            datainp = np.recfromcsv(filter(lambda s: len(s.strip()) > 0, other.split('\n')),
                                    dtype=dtype, usecols=usecols, names=col_names, **kwargs)

        if kwargs.get('names'):
            var_names = kwargs.get('names')
        else:
            var_names = datainp.dtype.names

        if index_col not in usecols:
            raise ValueError("index column should be in usecols")
        for item in usecols:
            if item != index_col:
                var_col = item

        nx = len(datainp)

        if index_col < var_col:
            var_name = var_names[1]
            index_name = var_names[0]
        else:
            var_name = var_names[0]
            index_name = var_names[1]

        if parse_date:
            date_parser = partial(parse_date_string, timezone=timezone,
                                  dt_format=datetime_format)
            dt_stamp = [date_parser(item).timestamp()
                        for item in datainp[var_names[index_col]]]
            dt_stamp = np.array(
                dt_stamp, dtype=self.obj_types['index_type']) * _time_factor
        else:
            dt_stamp = datainp[index_name]
        self.add_values(dt_stamp, datainp[var_name])
        self.setName(var_name)

    def from_list(self, other: List):
        """add data to series object from a list.

        Parameters
        ----------
        other : List
            a list with index and values
        """
        assert len(other[0]) == 2, "input argument should be list of list"
        self.from_ndarray(np.array(other))

    def from_ndarray(self, other: np.ndarray):
        """add data to series object from a ndarray.

        Parameters
        ----------
        other : np.ndarray
            a ndarray with index and values
        """
        assert isinstance(
            other, np.ndarray), "input argument should be np.ndarray"
        if isinstance(other, np.recarray):
            self.from_recarray(other)
        else:
            assert other.shape[1] == 2, "np.ndarray should be (N,2) shape"
            self.add_values(other[:, 0].astype(self.obj_types['index_type']),
                            other[:, 1].astype(self.obj_types['var_type']))

    def from_recarray(self, other: np.recarray):
        """add data to series object from a np.recarray.

        Parameters
        ----------
        other : np.recarray
            a numpy record array with index and values
        """
        assert isinstance(
            other, np.recarray), "input argument should be np.recarray"
        assert len(other.dtype) == 2, "np.recarray should have two records"
        var_names = other.dtype.names
        index_name = var_names[0]
        var_name = var_names[1]
        self.setName(var_name)
        self.add_values(other[index_name].astype(self.obj_types['index_type']),
                        other[var_name].astype(self.obj_types['var_type']))

    def add_value(self, x: Union[str, numbers.Real], y: numbers.Real):
        """add a index and value to series object.

        Parameters
        ----------
        x : numbers.Real
            index for the value in series object
        y : numbers.Real
            value to add in the series object
        """
        x_type = self.obj_types['index_type']
        y_type = self.obj_types['value_type']
        assert x_type and y_type, "index/ value type not set"
        if isinstance(x, str):
            self._handle.add_string_value(x.encode("UTF-8"), y_type(y))
        else:
            self._handle.add_value(x_type(x), y_type(y))

    def add_values(self, x: np.ndarray, y: np.ndarray):
        """add multiple values in a series object.

        Parameters
        ----------
        x : np.ndarray
            a numpy ndarray of indices
        y : np.ndarray
            a numpy ndarray of values
        """
        assert isinstance(x, np.ndarray) and isinstance(
            y, np.ndarray), "input should be np.ndarray"
        x_type = self.obj_types['index_type']
        y_type = self.obj_types['value_type']
        assert x_type and y_type, "index/ value type not set"
        self._handle.add_values(x.astype(x_type), y.astype(y_type))

    def setInterpolation(self, interp_type: Union[gs_enums.SeriesInterpolationType,
                                                  numbers.Integral]):
        """set interpolation scheme for a Series object.

        Parameters
        ----------
        interp_type : Union[gs_enums.SeriesInterpolationType, numbers.Integral]
            the interpolation scheme to use for the series object

        Raises
        ------
        TypeError
            series interpolation type should be int/ SeriesInterpolationType
        """
        if not isinstance(interp_type, (gs_enums.SeriesInterpolationType,
                                        numbers.Integral)):
            raise TypeError(
                "series interpolation type should be int/ SeriesInterpolationType")
        if isinstance(interp_type, numbers.Integral):
            self._handle.setInterpolation(interp_type)
        elif isinstance(interp_type, gs_enums.SeriesInterpolationType):
            self._handle.setInterpolation(interp_type.value)

    def setName(self, other: Union[str, bytes]):
        """method to set name for a series object.

        Parameters
        ----------
        other : Union[str, bytes]
            name of the series object

        Raises
        ------
        TypeError
            name should be of str/bytes type
        """
        if isinstance(other, str):
            self._handle.setName(other.encode('utf-8'))
        elif isinstance(other, bytes):
            self._handle.setName(other)
        else:
            raise TypeError("name should be of str/ bytes type")

    def setBounds(self, lowerLimit: numbers.Real, upperLimit: numbers.Real):
        """method to set bounds for a series object.

        Parameters
        ----------
        lowerLimit : numbers.Real
            lower limit of the series bounds
        upperLimit : numbers.Real
            upper limit of the series bounds

        Raises
        ------
        RuntimeError
            value type is not set
        """
        y_type = self.obj_types['value_type']
        if y_type:
            self._handle.setBounds(y_type(lowerLimit), y_type(upperLimit))
        else:
            raise RuntimeError("value type is not set")

    def get(self, x: numbers.Real) -> numbers.Real:
        """method to get value from a Series object.

        Parameters
        ----------
        x : numbers.Real
            index location

        Returns
        -------
        Union[np.float32, np.float64]
            value of a series object at a given index location

        Raises
        ------
        RuntimeError
            index type is not set
        """
        x_type = self.obj_types['index_type']
        if x_type:
            return self._handle(x_type(x))
        else:
            raise RuntimeError("index type is not set")

    @classmethod
    def from_series(cls, other: "Series") -> "Series":
        """method to shallow copy a Series object.

        Parameters
        ----------
        other : Series
            an instance of python/ cython Series object

        Returns
        -------
        Series
            an instance of Series object with a copy of the input Series object

        Raises
        ------
        TypeError
            input object should be of Series type
        """
        if isinstance(other, cls):
            out = cls(**other.obj_type)
            out._handle.from_series(other._handle)
        else:
            obj_type = {Series_dbl_dbl: dict(index_type=np.float64,
                                             value_type=np.float64),
                        Series_int_dbl: dict(index_type=np.int64,
                                             value_type=np.float64),
                        Series_int_flt: dict(index_type=np.int64,
                                             value_type=np.float32),
                        Series_dbl_flt: dict(index_type=np.float64,
                                             value_type=np.float32)}
            if not obj_type.get(other):
                raise TypeError("input object should be of Series type")
            out = cls(**obj_type[other])
            out._handle.from_series(other)
        return out

    def getName(self) -> str:
        """method to get name of series object.

        Returns
        -------
        str
            name of the series object
        """
        return self._handle.getName()

    def clear(self):
        """clear the contents of Series object.
        """
        self._handle.clear()

    def get_xMax(self) -> numbers.Real:
        """get maximum of Series indices.

        Returns
        -------
        numbers.Real
            maximum index
        """
        return self._handle.get_xMax()

    def get_xMin(self) -> numbers.Real:
        """get minimum of Series indices.

        Returns
        -------
        numbers.Real
            minimum index
        """
        return self._handle.get_xMin()

    def get_yMax(self) -> numbers.Real:
        """get maximum of Series values.

        Returns
        -------
        numbers.Real
            maximum value
        """
        return self._handle.get_yMax()

    def get_yMin(self) -> numbers.Real:
        """get minimum of Series values.

        Returns
        -------
        numbers.Real
            minimum value
        """
        return self._handle.get_yMin()

    def isInitialised(self) -> bool:
        """method to check if Series object is initialized.

        Returns
        -------
        bool
            True if initialized, False otherwise
        """
        return self._handle.isInitialised()

    def isConstant(self) -> bool:
        """method to check if values in Series object are constant.

        Returns
        -------
        bool
            True if constant, False otherwise
        """
        return self._handle.isConstant()

    def inRange(self, x: numbers.Real) -> bool:
        """method to check if index is within the range of Series indices.

        Parameters
        ----------
        x : numbers.Real
            index location to check in the Series object

        Returns
        -------
        bool
            True if `x` is in range of Series indices, False otherwise

        Raises
        ------
        RuntimeError
            index type is not specified
        """
        x_type = self.obj_types['index_type']
        if x_type:
            return self._handle.inRange(x_type(x))
        else:
            raise RuntimeError("index type is not specified")

    def update(self):
        """method to update Series object.
        """
        self._handle.update()

    def updateLimits(self):
        """method to update the limits of Series object.
        """
        self._handle.updateLimits()

    def __call__(self, x: numbers.Real):
        x_type = self.obj_types['index_type']
        return self._handle(x_type(x))

    def __str__(self):
        x_col = "Index:   %s " % (self.obj_types['index_type'].__name__)
        x_col += f"{self.get_xMin()} ... {self.get_xMax()}\n"
        y_col = "Series variables:\n"
        y_col += "    %s (%s):   " % (self.getName(),
                                      self.obj_types['value_type'].__name__)
        y_col += f"{self.get_yMin()} ... {self.get_yMax()}"
        return x_col + y_col

    def __repr__(self):
        return "<class 'geostack.series.%s'>\n%s" % (self.__class__.__name__,
                                                     str(self))
