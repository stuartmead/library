"""
Template for each `dtype` helper function for series

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# name, x_name, y_name

rtypes = [('dbl', 'double', 'double'),
          ('flt', 'float', 'float'),
          ('int', 'int', 'int'),
          ("str", "bytes", "string"),
          ("idx", "uint32_t", "cl_uint"),]
}}

cdef class _PropertyMap:
    def __cinit__(self):
        if type(self) is _PropertyMap:
            with nogil:
                self.thisptr = new PropertyMap()

    @staticmethod
    def copy(_PropertyMap other):
        if isinstance(other, _PropertyMap):
            return _PropertyMap.c_copy(deref(other.thisptr))
        else:
            raise TypeError("input argument should be a _PropertyMap object")

    @staticmethod
    cdef _PropertyMap c_copy(PropertyMap other) except+:
        obj = _PropertyMap()
        del obj.thisptr
        obj.thisptr = new PropertyMap(other)
        return obj

    cpdef void addProperty(self, string name) except+:
        deref(self.thisptr).addProperty(name)

    cpdef set getUndefinedProperties(self) except+:
        cdef cpp_set[string] prop_names
        cdef cpp_set[string].iterator it
        cdef set out_names = set()
        prop_names = deref(self.thisptr).getUndefinedProperties()

        it = prop_names.begin()

        while it != prop_names.end():
            out_names.add(deref(it))
            preinc(it)
        return out_names

    cpdef PropertyType getPropertyType(self, string name) except+:
        cdef PropertyType out
        out = deref(self.thisptr).getPropertyType(name)
        return out

    cpdef PropertyStructure getPropertyStructure(self, string name) except+:
        cdef PropertyStructure out
        out = deref(self.thisptr).getPropertyStructure(name)
        return out

    cpdef string toJsonString(self) except+:
        return deref(self.thisptr).toJsonString()

    cpdef void removeProperty(self, string name) except+:
        deref(self.thisptr).removeProperty(name)

    cpdef void clear(self) except+:
        deref(self.thisptr).clear()

    cpdef void resize(self, size_t n) except+:
        deref(self.thisptr).resize(n)

    cpdef set getPropertyNames(self) except+:
        cdef cpp_set[string] prop_names
        cdef cpp_set[string].iterator it
        cdef set out_names = set()
        prop_names = deref(self.thisptr).getPropertyNames()
        it = prop_names.begin()
        while it != prop_names.end():
            out_names.add(deref(it))
            preinc(it)
        return out_names

    cpdef set getPropertyVectorNames(self) except+:
        cdef cpp_set[string] prop_names
        cdef cpp_set[string].iterator it
        cdef set out_names = set()
        prop_names = deref(self.thisptr).getPropertyVectorNames()
        it = prop_names.begin()
        while it != prop_names.end():
            out_names.add(deref(it))
            preinc(it)
        return out_names

    cpdef bool hasProperty(self, string name) except+:
        return deref(self.thisptr).hasProperty(name)

{{for x_name, py_type, x_type in rtypes}}

    cpdef void setProperty_{{x_name}}(self, string name, {{x_type}} v) except+:
        deref(self.thisptr).setProperty[{{x_type}}](name, v)

    cdef void _setProperty_{{x_name}}_vector(self, string name, vector[{{x_type}}] v) except+:
        deref(self.thisptr).setProperty[vector[{{x_type}}]](name, v)

    cpdef {{x_type}} getProperty_{{x_name}}(self, string name) except+:
        return deref(self.thisptr).getProperty[{{x_type}}](name)

    cpdef {{x_type}} getPropertyFromVector_{{x_name}}(self, string name, size_t index) except+:
        return deref(self.thisptr).getPropertyFromVector[{{x_type}}](name, index)

    {{if x_name == "str"}}
    def setProperty_{{x_name}}_vector(self, string name, list other):
    {{else}}
    def setProperty_{{x_name}}_vector(self, string name, {{py_type}}[:] other):
    {{endif}}
        cdef size_t vec_size
        cdef vector[{{x_type}}] vec
        {{if x_name == "str"}}
        for item in other:
            vec.push_back(<{{x_type}}>item)
        {{else}}
        vec_size = other.shape[0]
        vec.reserve(vec_size)
        vec.assign(&other[0], &other[0]+vec_size)
        {{endif}}
        self._setProperty_{{x_name}}_vector(name, vec)

    cpdef void convertProperty_{{x_name}}(self, string name) except+:
        deref(self.thisptr).convertProperty[vector[{{x_type}}]](name)

    cpdef dict getProperties_{{x_name}}(self) except+:
        cdef cpp_map[string, {{x_type}}] _out
        cdef cpp_map[string, {{x_type}}].iterator it
        cdef dict out = {}

        _out = deref(self.thisptr).getProperties[{{x_type}}]()
        it = _out.begin()
        while it != _out.end():
            out[deref(it).first] = deref(it).second
            preinc(it)
        return out

    cdef vector[{{x_type}}] getPropertyRef_{{x_name}}_vector(self, string name) except+:
        cdef vector[{{x_type}}] out = deref(self.thisptr).getPropertyRef[vector[{{x_type}}]](name)
        return out

    def getPropertyVector_{{x_name}}(self, string name):
        cdef vector[{{x_type}}] _out
        cdef vector[{{x_type}}].iterator it
        cdef size_t vec_size
        {{if x_name == "str"}}
        cdef list out = []
        {{else}}
        cdef {{x_type}}[:] out
        {{endif}}
        _out = self.getPropertyRef_{{x_name}}_vector(name)
        {{if x_name == "str"}}
        it = _out.begin()
        while it != _out.end():
            out.append(deref(it))
            preinc(it)
        {{else}}
        vec_size = _out.size()
        {{if x_name == "flt"}}
        out = np.zeros(shape=vec_size, dtype=np.float32)
        {{elif x_name == "dbl"}}
        out = np.zeros(shape=vec_size, dtype=np.float64)
        {{elif x_name == "int"}}
        out = np.zeros(shape=vec_size, dtype=np.int32)
        {{elif x_name == "idx"}}
        out = np.zeros(shape=vec_size, dtype=np.uint32)
        {{endif}}
        memcpy(&out[0], _out.data(), sizeof({{py_type}})*vec_size)
        {{endif}}
        return out
{{endfor}}

    def getProperties(self):
        cdef dict out = {}
{{for x_name, py_type, x_type in rtypes}}
        out.update(self.getProperties_{{x_name}}())
{{endfor}}
        return out

    def __repr__(self):
        return self.__class__.__name__

    def __exit__(self):
        if type(self) is _PropertyMap:
            del self.thisptr

    def __dealloc__(self):
        self.__exit__()