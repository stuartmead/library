# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=ascii
#cython: c_string_type=unicode

cimport cython
from cython.operator cimport dereference as deref, preincrement as preinc
from libcpp cimport nullptr
from libcpp.string cimport string
from libc.string cimport memcpy
from libcpp.map cimport map as cpp_map
from libcpp.vector cimport vector
from libcpp.iterator cimport iterator
import numpy as np
cimport numpy as np

np.import_array()

include "_cy_property.pxi"

def get_geostack_version():
    out = {"major": GEOSTACK_VER_MAJOR,
           "minor": GEOSTACK_VER_MINOR,
           "patch": GEOSTACK_VER_PATCH}
    return out