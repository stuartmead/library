# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from __future__ import annotations
import logging
from ._cy_solver import cySolver
from numbers import Integral
import numpy as np

__all__ = ["Solver"]


class Solver:
    def __init__(self):
        self._handle = cySolver()

    def setVerbose(self, verbose: bool):
        """set verbose flag for logger

        Parameters
        ----------
        verbose : bool
            True to set solver to verbose, false otherwise

        Raises
        ------
        TypeError
            input argument should be a bool
        """
        if not isinstance(verbose, bool):
            raise TypeError("input argument should be bool")
        self._handle.setVerbose(verbose)

        # update python logger
        logger = logging.getLogger()
        if verbose and logger.hasHandlers():
            logger.setLevel(self._handle.getVerboseLevel())

    def setVerboseLevel(self, verbose: Integral):
        """set verbosity level of the solver

        Parameters
        ----------
        verbose : Integral
            verbosity level to control logging and verbosity

        Raises
        ------
        TypeError
            verbose level should be an integer
        """
        if not isinstance(verbose, Integral):
            raise TypeError("input argument should be an integer")
        self._handle.setVerboseLevel(verbose)

        # update python logger
        logger = logging.getLogger()
        if logger.hasHandlers():
            logger.setLevel(verbose)

    def getVerboseLevel(self) -> Integral:
        """get the verbosity level of the solver (logger)

        Returns
        -------
        Integral
            the verbosity level of the logger (solver)
        """
        return self._handle.getVerboseLevel()

    def setHostMemoryLimit(self, hostMemoryLimit_: Integral):
        """set host memory limit in bytes

        Parameters
        ----------
        hostMemoryLimit: Integral
            host memory limit in bytes
        """

        self._handle.setHostMemoryLimit(np.uint64(hostMemoryLimit_))

    def getHostMemoryLimit(self) -> Integral:
        """get host memory limit in bytes

        Returns
        -------
        Integral
            get host memory limit in bytes
        """

        return self._handle.getHostMemoryLimit()

    def setDeviceMemoryLimit(self, deviceMemoryLimit_: Integral):
        """set device memory limit in bytes

        Parameters
        ----------
        deviceMemoryLimit: Integral
            set device memory limit in bytes
        """

        self._handle.setDeviceMemoryLimit(np.uint64(deviceMemoryLimit_))

    def getDeviceMemoryLimit(self) -> Integral:
        """get device memory limit in bytes

        Returns
        -------
        Integral
            get device memory limit in bytes
        """

        return self._handle.getDeviceMemoryLimit()

    @staticmethod
    def get_verbose_level() -> Integral:
        """get verbose level from the solve instance.

        Returns
        -------
        Integral
            the verbosity level from the solver instance
        """
        obj = Solver()
        return obj.getVerboseLevel()

    @staticmethod
    def set_verbose(verbose: bool):
        """set verbosity of the solver.

        Parameters
        ----------
        verbose : bool
            True to enable verbosity, False otherwise.
        """
        obj = Solver()
        obj.setVerbose(verbose)

    @staticmethod
    def set_verbose_level(verbose: Integral):
        """set verbosity level for the solver

        Parameters
        ----------
        verbose : Integral
            the level of verbosity for the solver.
        """
        obj = Solver()
        obj.setVerboseLevel(verbose)

    @staticmethod
    def set_host_memory_limit(hostMemoryLimit_: Integral):
        """set host memory limit

        Parameters
        ----------
        hostMemoryLimit: Integral
            set host memory limit in bytes
        """
        obj = Solver()
        obj.setHostMemoryLimit(hostMemoryLimit_)

    @staticmethod
    def get_host_memory_limit() -> Integral:
        """get host memory limit in bytes

        Returns
        -------
        Integral
            get host memory limit
        """
        obj = Solver()
        return obj.getHostMemoryLimit()

    @staticmethod
    def set_device_memory_limit(deviceMemoryLimit_: Integral):
        """set device memory limit in bytes

        Parameters
        ----------
        deviceMemoryLimit: Integral
            set device memory limit
        """
        obj = Solver()
        obj.setDeviceMemoryLimit(deviceMemoryLimit_)

    @staticmethod
    def get_device_memory_limit() -> Integral:
        """get device memory limit in bytes

        Returns
        -------
        Integral
            get device memory limit
        """
        obj = Solver()
        return obj.getDeviceMemoryLimit()

    def __repr__(self):
        return self.__repr__()

    def __str__(self):
        return "<class 'geostack.core.%s'>" % self.__class__.__name__
