# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from __future__ import annotations
import os
import os.path as pth
import numpy as np
from ._cy_variables import _Variables_f, _Variables_d, _Variables_i
from typing import Union, Dict

__all__ = ["Variables"]


class Variables:
    def __init__(self, dtype: Union[np.float32,
                                    np.float64,
                                    np.uint32] = np.float32):
        self._handle = None
        if dtype == np.float32:
            self._handle = _Variables_f()
            self._dtype = np.float32
        elif dtype == np.float64:
            self._handle = _Variables_d()
            self._dtype = dtype
        elif dtype == np.uint32:
            self._handle = _Variables_i()
            self._dtype = dtype

    def set(self, var_name: Union[str, bytes, np.uint32], var_value) -> None:
        """set a value for a given variable.

        Parameters
        ----------
        var_name : Union[str, bytes, np.uint32]
            name of the variable
        var_value : [type]
            value of the variable

        Returns
        -------
        Nil
        """
        assert self._handle is not None, "Variables object is not initialized"
        if self._dtype == np.uint32:
            assert type(var_name) in [np.uint32, int]
            self._handle.set(var_name, self._dtype(var_value))
        else:
            if isinstance(var_name, str):
                self._handle.set(var_name.encode('utf-8'),
                                 self._dtype(var_value))
            elif isinstance(var_name, bytes):
                self._handle.set(var_name, self._dtype(var_value))

    def get(self, var_name: Union[str, bytes, np.uint32]) -> Union[np.float32,
                                                                   np.float64,
                                                                   np.uint32]:
        """get the value of the variable

        Parameters
        ----------
        var_name : Union[str, bytes, np.uint32]
            name of the variable

        Returns
        -------
        Union[np.float32,np.float64,np.uint32]
            value of the variable
        """
        assert self._handle is not None, "Variables object is not initialized"
        if self._dtype == np.uint32:
            assert type(var_name) in [np.uint32, int]
            return self._handle.get(var_name)
        else:
            if isinstance(var_name, str):
                return self._handle.get(var_name.encode('utf-8'))
            elif isinstance(var_name, bytes):
                return self._handle.get(var_name)

    def getIndexes(self) -> Dict:
        """get the indices of the variable

        Returns
        -------
        Dict
            a dictionary of variable and indices in the Variables object.
        """
        assert self._handle is not None, "Variable object is not initialised"
        return self._handle.getIndexes()

    def hasData(self) -> bool:
        """check if variables object has data.

        Returns
        -------
        bool
            True if Variables has data else False
        """
        assert self._handle is not None, "Variable object is not initialised"
        return self._handle.hasData()

    @staticmethod
    def from_dict(other, **kwargs):
        if not isinstance(other, dict):
            raise TypeError("input argument should be a dictionary")
        dtype = kwargs.get("dtype", np.float32)
        obj = Variables(dtype=dtype)
        for item in other:
            obj[item] = other[item]
        return obj

    def update(self, other):
        if isinstance(other, dict):
            for item in other:
                self[item] = other[item]
        elif isinstance(other, Variables):
            assert other._dtype == self._dtype, "data type mismatch between the input and current object"
            idx = other.getIndexes()
            for item in idx:
                self[item] = other[item]

    @property
    def indexes(self):
        return self.getIndexes()

    def __iter__(self):
        for item in self.indexes:
            yield item

    def __setitem__(self, var_name: Union[str, bytes, np.uint32], var_value):
        self.set(var_name, var_value)

    def __getitem__(self, var_name: Union[str, bytes, np.uint32]):
        return self.get(var_name)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        var_data = self.getIndexes()
        var_string = '\n'.join(
            [f"    {item}[{var_data[item]}]:  {self.get(item)}" for item in var_data])
        return "<class 'geostack.core.%s'>\n%s" % (self.__class__.__name__, var_string)
