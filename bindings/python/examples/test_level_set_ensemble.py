# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
 
import json
import numpy as np

from geostack.raster import Raster, RasterPtrList
from geostack.vector import Vector
from geostack.runner import runScript
from geostack.solvers import LevelSet
from geostack.io import vectorToGeoJson

# Set parameters
N = 20 # Number of levels
time = 200.0 # Maximum time

# Create initial conditions
v = Vector()
for i in range(0, N):
    point_id = v.addPoint( [np.random.normal(0, 20), np.random.normal(0, 20)] )
    v.setProperty(point_id, "radius", 10)
    v.setProperty(point_id, "level", i)

# Create random map
base = Raster(name = "base")
base.init(nx = 100, ny = 100, nz = N, hx = 10.0, hy = 10.0, hz = 1.0, ox = -500, oy = -500)
runScript("base = random+1.0;", [base])
base.write('./_out_level_set_base.tiff')

# Create input list
inputLayers = RasterPtrList()
inputLayers.append(base)

# Create solver
config = {
    "resolution": 1.0,
    "levels": N,
    "advectionScript": '''advect_x = 0.0; advect_y = 1.0;''',
    "buildScript": '''speed = base*(0.25+advect_dot_normal);'''
}
solver = LevelSet()
solver.init(json.dumps(config), v, inputLayers = inputLayers)
    
# Run solver
while solver.getParameters()['time'] < time:
    solver.step()
      
# Reduce into single raster
arrival = solver.getArrival()
arrival.name = "arrival"
dims = arrival.getDimensions()
reduce = Raster(name = "reduce")
reduce.init(nx = dims.nx, ny = dims.ny, hx = dims.hx, hy = dims.hy, ox = dims.ox, oy = dims.oy)
runScript(f'''

reduce = 0.0;
for (uint k = 0; k < arrival_layers; k++) {{
    reduce += isValid_REAL(arrival[k]) ? arrival[k]/{time} : 1.0;
}}
reduce /= (REAL)arrival_layers;

''', [reduce, arrival])
reduce.write('./_out_level_set_reduce.tiff')

# Write arrival time 
solver.getArrival().write('./_out_level_set_arrival.tiff')
        
# Generate isochrones
#isochroneVector = solver.getArrival().vectorise(
#    np.arange(0, 100, 10), 100)

# Write isochrone file
#with open('./_out_level_set_iso.json', 'w') as outfile:
#    outfile.write(vectorToGeoJson(isochroneVector, enforceProjection = False))

print("Done")
