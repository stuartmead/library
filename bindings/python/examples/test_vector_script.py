# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from geostack.raster import Raster
from geostack.vector import Vector
from geostack.runner import runScript, runVectorScript
from geostack.io import vectorToGeoJson
from geostack.gs_enums import ReductionType

# Create raster layer
A = Raster(name = "A")

# Initialize Raster
A.init(nx = 10, ny = 10, hx = 10.0, hy = 10.0)

runScript("A = x*y;", [A])

# Write Raster
A.write('./_out_vector_script_raster.tiff')

# Create vector
v = Vector()
v.addPolygon( [ [ [2.5, 2.5], [2.5, 37.5], [37.5, 37.5], [37.5, 2.5] ] ] )
v.addPolygon( [ [ [97.5, 97.5], [97.5, 62.5], [62.5, 62.5], [62.5, 97.5] ] ] )
v.addProperty("max")
v.addProperty("min")

# Run script
runVectorScript("max = A;", v, [A], ReductionType.Maximum)
runVectorScript("min = A;", v, [A], ReductionType.Minimum)

# Write vector
with open('./_out_vector_script.json', 'w') as outfile:
    outfile.write(vectorToGeoJson(v, enforceProjection = False))
    
print("Done")

