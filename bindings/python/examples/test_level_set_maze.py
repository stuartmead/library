# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
 
import json
import numpy as np
import imageio

from geostack.raster import Raster, RasterPtrList
from geostack.vector import Vector
from geostack.solvers import LevelSet

# Read red channel from maze and normalise
maze_image = imageio.imread('maze_1.png')[:,:,0]/255

# Create maze raster layer
maze = Raster(name = "maze")
maze.init(nx = maze_image.shape[0], ny = maze_image.shape[1], hx = 1.0, hy = 1.0)

# Set maze data
maze.data = maze_image

# Create input list
inputLayers = RasterPtrList()
inputLayers.append(maze)

# Create output list
outputLayers = RasterPtrList()
test = Raster(name = 'test')
outputLayers.append(test)

# Create vector layer
v = Vector()
point_id = v.addPoint( [15, 15] )
v.setProperty(point_id, "radius", 5)

# Create solver configuration
config = {
    "resolution": 1.0,
    "initialisationScript": "class = maze;",
    "buildScript": "speed = 1.0;",
    "updateScript": "test = sin(arrival/(M_PI*10.0));"
}

# Create solver
solver = LevelSet()
solver.init(json.dumps(config), v, inputLayers = inputLayers, outputLayers = outputLayers)

# Run solver
while solver.getParameters()['time'] <= 2500.0:
    solver.step()
        
# Print shortest path distance
print(solver.getArrival().getNearestValue(maze_image.shape[0]-15, maze_image.shape[1]-15))

solver.getArrival().write('./_out_level_set_maze.tiff')
solver.getClassification().write('./_out_level_set_maze_class.tiff')
test.write('./_out_level_set_test.tiff')

print("Done")