from geostack.raster import Raster
from geostack.runner import runScript

# Create raster
r = Raster(name = 'r')
r.init(nx = 20, ny = 20, hx = 1, hy = 1)
r.setAllCellValues(0)

# Glider
r.setCellValue(1, 2, 4)
r.setCellValue(1, 3, 4)
r.setCellValue(1, 4, 4)
r.setCellValue(1, 4, 3)
r.setCellValue(1, 3, 2)

# Run cellular automaton
script = '''
    if (!isBoundary_N && !isBoundary_S && !isBoundary_W && !isBoundary_E) {
        int neighbours = r_N+r_NW+r_W+r_SW+r_S+r_SE+r_E+r_NE;
        
        if (r == 1 && (neighbours < 2 || neighbours > 3)) {
        
            // Any live cell less than two or more than live neighbours dies
            r = 0;
            
        } else if (r == 0 && neighbours == 3) {
        
            // Any dead cell with exactly three live neighbours becomes a live cell
            r = 1;
        }
    }
'''

r.write(f'game_of_life_00.png')
for i in range(1, 50):

    # Run step
    runScript(script, [r])

    # Write
    r.write(f'game_of_life_{i:02d}.png')
